$(document).ready(function(){
    $('#mobile-navbar-language-button').click(function(){
        //$('#mobile-navbar').hide(); // isn't enough
        $('#mobile-navbar .btn-close').click(); // simulates a touch outside the mobile navbar
        var languageModal = new bootstrap.Modal(document.getElementById('language-modal'));
        languageModal.toggle();
    });

    $('#fab').click(function(){
        var $fabButton = $('#fab button');
        var enterDesignMode = $fabButton.hasClass('btn-fab-view');
        $fabButton.removeClass(enterDesignMode ? 'btn-fab-view' : 'btn-fab-edit');
        $fabButton.addClass(enterDesignMode ? 'btn-fab-edit' : 'btn-fab-view');
        $fabButton.html('<i class="fas fa-' + (enterDesignMode ? 'edit' : 'eye') + '"></i>');
        document.designMode = enterDesignMode ? 'on' : 'off';
    });

    // Bootstrap's tables are opt-in
    // https://stackoverflow.com/a/39703599/1657502
    $('table:not(.rouge-table)').addClass('table');

    // Bootstrap's blockquotes are opt-in as well
    $('blockquote').addClass('blockquote py-2 px-4 mb-4 border-0 border-start border-5');
});

// https://www.w3schools.com/howto/howto_js_copy_clipboard.asp
function copyToClipboard(inputTextId, buttonId = "", tooltipText = "") {
    var inputText = document.getElementById(inputTextId);
    inputText.select();
    inputText.setSelectionRange(0, 99999);
    document.execCommand('copy');
    if ((buttonId != "") && (tooltipText != "")) {
        var button = document.getElementById(buttonId);
        var tooltip = new bootstrap.Tooltip(button, {
            title: tooltipText
        });
        tooltip.show(); // TODO Why does the page scroll?
        setTimeout(function() { tooltip.hide(); }, 3000);
    }
}
