---
date: 2023-12-08 11:00:00 GMT-3
layout: page
nickname: 'privacy-policy'
title: 'Política de Privacidade'
---

A sua privacidade é importante para nós. É política do **{{ site.title }}** respeitar a sua privacidade em relação a qualquer informação sua que possamos coletar no _site_ **[{{ site.title }}]({{ site.baseurl | prepend: site.url }})**, e outros _sites_ que possuímos e operamos.

Solicitamos informações pessoais apenas quando realmente precisamos delas para lhe fornecer um serviço. Fazemo-lo por meios justos e legais, com o seu conhecimento e consentimento. Também informamos por que estamos coletando e como será usado.

Apenas retemos as informações coletadas pelo tempo necessário para fornecer o serviço solicitado. Quando armazenamos dados, protegemos dentro de meios comercialmente aceitáveis ​​para evitar perdas e roubos, bem como acesso, divulgação, cópia, uso ou modificação não autorizados.

Não compartilhamos informações de identificação pessoal publicamente ou com terceiros, exceto quando exigido por lei.

O nosso _site_ pode ter _links_ para _sites_ externos que não são operados por nós. Esteja ciente de que não temos controle sobre o conteúdo e práticas desses _sites_ e não podemos aceitar responsabilidade por suas respectivas políticas de privacidade.

Você é livre para recusar a nossa solicitação de informações pessoais, entendendo que talvez não possamos fornecer alguns dos serviços desejados.

O uso continuado de nosso _site_ será considerado como aceitação de nossas práticas em torno de privacidade e informações pessoais. Se você tiver alguma dúvida sobre como lidamos com dados do usuário e informações pessoais, entre em contato conosco.

- O serviço Google AdSense que usamos para veicular publicidade usa um _cookie_ DoubleClick para veicular anúncios mais relevantes em toda a _web_ e limitar o número de vezes que um determinado anúncio é exibido para você.
- Para mais informações sobre o Google AdSense, consulte as FAQs oficiais sobre privacidade do Google AdSense.
- Utilizamos anúncios para compensar os custos de funcionamento deste _site_ e fornecer financiamento para futuros desenvolvimentos. Os _cookies_ de publicidade comportamental usados ​​por este _site_ foram projetados para garantir que você forneça os anúncios mais relevantes sempre que possível, rastreando anonimamente seus interesses e apresentando coisas semelhantes que possam ser do seu interesse.
- Vários parceiros anunciam em nosso nome e os _cookies_ de rastreamento de afiliados simplesmente nos permitem ver se nossos clientes acessaram o _site_ através de um dos _sites_ de nossos parceiros, para que possamos creditá-los adequadamente e, quando aplicável, permitir que nossos parceiros afiliados ofereçam qualquer promoção que pode fornecê-lo para fazer uma compra.

## Compromisso do usuário

O usuário se compromete a fazer uso adequado dos conteúdos e da informação que o **{{ site.title }}** oferece no _site_ e com caráter enunciativo, mas não limitativo:

A) Não se envolver em atividades que sejam ilegais ou contrárias à boa fé a à ordem pública;

B) Não difundir propaganda ou conteúdo de natureza racista, xenofóbica, jogos de apostas ou azar, qualquer tipo de pornografia ilegal, de apologia ao terrorismo ou contra os direitos humanos;

C) Não causar danos aos sistemas físicos (_hardwares_) e lógicos (_softwares_) do **{{ site.title }}**, de seus fornecedores ou terceiros, para introduzir ou disseminar vírus informáticos ou quaisquer outros sistemas de _hardware_ ou _software_ que sejam capazes de causar danos anteriormente mencionados.

## Mais informações

Esperamos que esteja esclarecido e, como mencionado anteriormente, se houver algo que você não tem certeza se precisa ou não, geralmente é mais seguro deixar os _cookies_ ativados, caso interaja com um dos recursos que você usa em nosso _site_.

Esta política é efetiva a partir de {{ page.date | dataEmPortugues }} às {{ page.date | date: "%R" }}.
