---
layout: contribute
nickname: contribute
title: Contribuir
permalink: /pt/contribuir

intro: 'É muito fácil contribuir com o Linux Kamarada. Dependendo das suas habilidades e da sua disponibilidade de tempo e de dinheiro, você pode ajudar o projeto de uma (ou mais de uma) das seguintes formas:'

help_other_users_title: 'Ajude outros usuários'
help_other_users_desc: 'Se você tiver algum tempo livre e estiver disposto a ajudar outros usuários com problemas técnicos, poderá entrar nos canais de <a href="/pt/ajuda">Ajuda</a> e auxiliar outros usuários do Linux Kamarada a resolver os problemas que você sabe corrigir.'

test_bugs_title: 'Teste e reporte bugs'
test_bugs_desc: 'Se você perceber algo que não funciona corretamente, solucione o problema e, se encontrar um <em>bug</em>, por favor, reporte. Veja formas de entrar em contato na página <a href="/pt/ajuda">Ajuda</a>.'

financial_title: 'Ajuda financeira'
financial_desc: 'O Linux Kamarada é gratuito e não gera nenhum tipo de receita direta. É financiado pela exibição de anúncios no <em>site</em><!--, patrocínios--> e doações. Você pode ajudar fazendo uma contribuição financeira para o projeto.'

donations_title: 'Doações'
donations_desc: '<p>Qualquer quantia ajudará e será muito bem-vinda!</p>
<p>Você pode fazer uma doação única rapidamente usando o PayPal:</p>'
---
