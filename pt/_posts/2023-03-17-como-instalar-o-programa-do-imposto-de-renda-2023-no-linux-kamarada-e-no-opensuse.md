---
date: '2023-03-17 19:35:00 UTC-3'
image: '/files/2017/02/irpf-2017.jpg'
layout: post
published: true
nickname: 'irpf-2023'
title: 'Como instalar o programa do Imposto de Renda 2023 no Linux Kamarada (e no openSUSE)'
---

No dia [09 de março][noticias], a [Receita Federal] liberou para os contribuintes brasileiros o programa do [Imposto de Renda Pessoa Física 2023 (IRPF 2023)][irpf-2023]. Já é possível enviar a declaração desde anteontem, dia 15 de março, e o prazo de entrega vai até o dia 31 de maio.&nbsp;

Veja neste _post_ como baixar e instalar o programa do IRPF 2023 no [Linux Kamarada], que, caso você ainda não conheça, é uma [distribuição Linux][linux] brasileira baseada no [openSUSE Leap][leap-vs-tumbleweed].

As instruções a seguir também servem para o openSUSE Leap e o [openSUSE Tumbleweed][leap-vs-tumbleweed].

{% include image.html src="/files/2017/02/irpf-2017.jpg" %}

**Observação:** não está no escopo deste _post_ ensinar a preencher e enviar a declaração. Caso tenha dúvidas com relação a esses assuntos, consulte a ajuda do próprio programa ou o [_site_ do IRPF 2023][irpf-2023].

## Java não é mais um pré-requisito

Versões anteriores do programa do IRPF, a exemplo do [IRPF 2020], precisavam que a [máquina virtual Java][java] da [Oracle] estivesse instalada no computador.

O programa do IRPF 2023 **não** requer que uma máquina virtual Java esteja instalada no computador, isso porque ele já vem com a própria máquina virtual Java embutida.

## Download

Acesse o _site_ da Receita Federal em:

- [www.gov.br/receitafederal][Receita Federal]

Na página inicial, clique em **Imposto de Renda**:

{% include image.html src="/files/2023/03/irpf-2023-01.jpg" %}

Na página seguinte, clique no _link_ **Baixar o programa do imposto de renda**:

{% include image.html src="/files/2023/03/irpf-2023-02.jpg" %}

Na seção **Programa IRPF 2023**, clique no *link* **Linux** para baixar o instalador do programa:

{% include image.html src="/files/2023/03/irpf-2023-03.jpg" %}

## Permissão

Por questões de segurança, o Linux não deixa que arquivos binários (executáveis, aplicativos, programas) sejam executados assim que são baixados. A menos que você, usuário, permita explicitamente que o arquivo seja executado.

Para permitir a execução do instalador, abra a pasta **Downloads**, onde o instalador foi baixado, clique com o botão direito do *mouse* no instalador e clique em **Propriedades**:

{% include image.html src="/files/2023/03/irpf-2023-04.jpg" %}

Mude para a aba **Permissões** e marque a opção **Permitir execução do arquivo como um programa**:

{% include image.html src="/files/2023/03/irpf-2023-05.jpg" %}

Em seguida, pode fechar a caixa de diálogo **Propriedades**.

## Instalação

Agora, nos esbarramos em outra questão de segurança: o gerenciador de [arquivos] da área de trabalho [GNOME] não abre arquivos executáveis (ou seja, não inicia programas) diretamente com um duplo clique. Abrir esses arquivos difere um pouco de abrir um documento do [LibreOffice Writer], por exemplo.

De volta à pasta **Downloads**, clique com o botão direito do _mouse_ no instalador e clique em **Executar como programa**:

{% include image.html src="/files/2023/03/irpf-2023-06.jpg" %}

O instalador é iniciado:

{% include image.html src="/files/2023/03/irpf-2023-07.jpg" %}

Você pode simplesmente **Avançar** pelas telas do instalador e, na última, clicar em **Terminar**:

{% include image.html src="/files/2023/03/irpf-2023-08.jpg" %}
{% include image.html src="/files/2023/03/irpf-2023-09.jpg" %}
{% include image.html src="/files/2023/03/irpf-2023-10.jpg" %}

Pronto! O programa do Imposto de Renda 2023 já está instalado. Agora é só usar!

## Iniciando o programa do IRPF 2023

Para iniciar o programa do IRPF 2023, abra o menu **Atividades**, no canto superior esquerdo da tela, digite `irpf` e clique no ícone **IRPF 2023**:

{% include image.html src="/files/2023/03/irpf-2023-11.jpg" %}

{% include image.html src="/files/2023/03/irpf-2023-12.jpg" %}

Agora é só fazer a declaração e enviar.

Espero que essa dica tenha sido útil. Se ficou alguma dúvida em relação à instalação, não deixe de comentar! Até a próxima!

[noticias]:             https://www.gov.br/receitafederal/pt-br/assuntos/noticias/2023/marco/receita-libera-nesta-quinta-feira-9-3-o-programa-do-imposto-de-renda-2023
[Receita Federal]:      https://www.gov.br/receitafederal/pt-br
[irpf-2023]:            https://www.gov.br/receitafederal/pt-br/assuntos/meu-imposto-de-renda
[Linux Kamarada]:       {% post_url pt/2023-02-27-linux-kamarada-15-4-mais-funcional-bonito-polido-e-elegante-do-que-nunca %}
[linux]:                http://www.vivaolinux.com.br/linux/
[leap-vs-tumbleweed]:   {% post_url pt/2020-12-06-opensuse-leap-e-opensuse-tumbleweed-qual-e-a-diferenca %}
[IRPF 2020]:            {% post_url pt/2020-03-02-como-instalar-o-programa-do-imposto-de-renda-2020-no-linux-kamarada-e-no-opensuse %}
[java]:                 {% post_url pt/2017-02-28-como-instalar-o-java-da-oracle-no-linux-opensuse %}
[Oracle]:               https://www.oracle.com/br/
[arquivos]:             https://apps.gnome.org/pt-BR/app/org.gnome.Nautilus/
[GNOME]:                https://br.gnome.org/
[LibreOffice Writer]:   https://pt-br.libreoffice.org/descubra/writer/
