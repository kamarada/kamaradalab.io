---
date: '2023-02-27 11:10:00 GMT-3'
image: '/files/2023/02/kamarada-15.3-eol.jpg'
layout: post
nickname: 'kamarada-15.3-eol'
title: 'Linux Kamarada 15.3 e openSUSE Leap 15.3 tiveram seu suporte encerrado'
excerpt: 'O suporte do openSUSE Leap 15.3, lançado em 02 de junho de 2021, foi encerrado em 31 de dezembro de 2022, totalizando 18 meses de correções de bugs de funcionalidades e falhas de segurança. Houve um primeiro anúncio antecipado na lista de discussão por e-mail em 30 de novembro, e depois um segundo anúncio antecipado no site de notícias do openSUSE em 12 de dezembro. Na verdade, ainda em 16 de janeiro houve uma atualização para o Leap 15.3 lançada pela SUSE, depois sim a manutenção do openSUSE Leap 15.3 foi encerrada em definitivo. Desde então, o openSUSE Leap 15.3 não recebe mais atualizações de qualquer tipo.'
---

{% include image.html src='/files/2023/02/kamarada-15.3-eol.jpg' %}

## openSUSE Leap 15.3

O suporte do [openSUSE Leap 15.3][leap-15.3], lançado em 02 de junho de 2021, foi encerrado em 31 de dezembro de 2022, totalizando 18 meses de correções de _bugs_ de funcionalidades e falhas de segurança.

Houve um primeiro anúncio antecipado na [lista de discussão por _e-mail_][mailing list] em [30 de novembro][list-1], e depois um segundo anúncio antecipado no _site_ de notícias do openSUSE em [12 de dezembro][news]. Na verdade, ainda em 16 de janeiro houve uma atualização para o Leap 15.3 lançada pela [SUSE], depois sim [a manutenção do openSUSE Leap 15.3 foi encerrada em definitivo][list-2]. Desde então, o openSUSE Leap 15.3 não recebe mais atualizações de qualquer tipo.

O [Projeto openSUSE][opensuse] recomenda que usuários do openSUSE Leap 15.3 atualizem para o [openSUSE Leap 15.4][leap-15.4], lançado em 08 de junho de 2022 e que deve receber atualizações até o fim de novembro de 2023, de acordo com a [_wiki_ do openSUSE][lifetime].

Até lá, muito provavelmente já teremos o openSUSE Leap 15.5, cujo lançamento está [previsto][leap-15.5-roadmap] para 07 de junho de 2023. Portanto, quando o Leap 15.5 for lançado, os usuários do Leap 15.4 terão aproximadamente 6 meses para fazer a atualização da distribuição.

## Linux Kamarada 15.3

Como o Linux Kamarada é baseado no [openSUSE Leap][leap], usuários do Linux Kamarada são, por tabela, usuários do openSUSE Leap também e recebem essas mesmas atualizações.

O fim do suporte de todas as versões do Linux Kamarada normalmente deve ocorrer junto do fim do suporte da versão equivalente do openSUSE Leap.

Como o [Linux Kamarada 15.4][kamarada-15.4] foi lançado apenas hoje, o suporte do [Linux Kamarada 15.3][kamarada-15.3], lançado em 30 de dezembro de 2021, foi estendido até hoje, mas se encerra hoje, 27 de fevereiro de 2023.

Se você usa o Linux Kamarada 15.3, eu recomendo que atualize para o [Linux Kamarada 15.4][kamarada-15.4], que deve receber atualizações pelo mesmo período que o openSUSE Leap 15.4, ou seja, até novembro de 2023.

## Como atualizar

Se você usa o openSUSE Leap (ou o Linux Kamarada) 15.3, veja como atualizar para o 15.4 em:

- [Linux Kamarada e openSUSE Leap: como atualizar da versão 15.3 para a 15.4][upgrade]

As instruções no tutorial acima se aplicam a ambas as distribuições.

Se tiver alguma dúvida, comente neste texto ou no tutorial acima. Você também pode conferir outras opções na página [Ajuda].

[leap-15.3]:            {% post_url pt/2021-06-02-opensuse-leap-153-constroi-um-caminho-para-a-versao-empresarial %}
[mailing list]:         https://en.opensuse.org/openSUSE:Mailing_lists_subscription
[list-1]:               https://lists.opensuse.org/archives/list/announce@lists.opensuse.org/thread/OCJDZIP63AUG4TW4W5JKR6TVWZ6N2TMT/
[news]:                 https://news.opensuse.org/2022/12/12/leap-153-to-reach-eol/
[SUSE]:                 https://www.suse.com/pt-br/
[list-2]:               https://lists.opensuse.org/archives/list/announce@lists.opensuse.org/thread/57Z3SCL5R7ZW7UUFZDR6I27WD545YD2V/
[opensuse]:             https://www.opensuse.org/
[leap-15.4]:            {% post_url pt/2022-06-08-leap-15-4-oferece-novos-recursos-com-a-estabilidade-de-sempre %}
[lifetime]:             https://en.opensuse.org/Lifetime
[leap-15.5-roadmap]:    https://en.opensuse.org/openSUSE:Roadmap#Schedule_for_openSUSE_Leap_15.5
[leap]:                 {% post_url pt/2020-12-06-opensuse-leap-e-opensuse-tumbleweed-qual-e-a-diferenca %}
[kamarada-15.4]:        {% post_url pt/2023-02-27-linux-kamarada-15-4-mais-funcional-bonito-polido-e-elegante-do-que-nunca %}
[kamarada-15.3]:        {% post_url pt/2021-12-30-linux-kamarada-15-3-ano-novo-distribuicao-nova %}
[upgrade]:              {% post_url pt/2022-12-28-linux-kamarada-e-opensuse-leap-como-atualizar-da-versao-153-para-a-154 %}
[Ajuda]:                /pt/ajuda
