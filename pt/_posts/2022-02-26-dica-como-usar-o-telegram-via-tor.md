---
date: '2022-02-26 00:30:00 GMT-3'
image: '/files/2022/02/telegram-tor.png'
layout: post
nickname: 'telegram-tor'
title: 'Dica: como usar o Telegram via Tor no Linux'
---

{% include image.html src='/files/2022/02/telegram-tor.png' %}

Se por qualquer motivo você for impedido de usar o [Telegram] -- por exemplo, se você está de mudança ou viajando para um dos [países que censuram o Telegram][telegram-censorship] (felizmente, o Brasil não está nessa lista, e espero que continue assim) -- veja aqui como você pode usá-lo no [Linux] por meio da rede [Tor].

{% capture brasil %}

Infelizmente, o Brasil chegou a ser adicionado à [lista de países que censuram o Telegram][telegram-censorship], porque houve uma ordem judicial para bloquear o Telegram na sexta-feira [18 de março de 2022][tecmundo-1]. Mas, felizmente, essa ordem foi revogada 2 dias depois, no [domingo 20][tecmundo-2]. No momento em que atualizo esta dica, o uso do Telegram é, legalmente falando, permitido no Brasil, e espero que continue assim.

[telegram-censorship]:  https://en.wikipedia.org/wiki/Government_censorship_of_Telegram_Messenger
[tecmundo-1]: https://www.tecmundo.com.br/software/235647-telegram-ministro-alexandre-moraes-determina-banimento-app.htm
[tecmundo-2]: https://www.tecmundo.com.br/internet/235684-telegram-liberado-brasil-cumprir-ordens-moraes.htm

{% endcapture %}

{% include update.html date="29/03/2022" message=brasil %}

Eu já falei [aqui][Navegador Tor] brevemente sobre o [Navegador Tor] (_Tor Browser_), você também pode se beneficiar dessa dica se usa esse navegador.

Como referência, vou usar o [Linux Kamarada 15.3], que já vem com o [Flatpak] e o Tor instalados "de fábrica".

{% capture outros_sos %}

Se, por um acaso, você procurava instruções para outro sistema operacional, mas caiu nessa página, consulte os _links_ a seguir:

- Android: [Como usar o Telegram via Tor no Android - Antônio Medeiros][android]
- Windows: [Como usar o Telegram via Tor no Windows - Antônio Medeiros][windows]
- iOS (iPhone e iPad): [Como usar o Telegram via Tor no iOS (iPhone e iPad) - Antônio Medeiros][ios]

[android]: https://antoniomedeiros.dev/blog/2022/03/24/como-usar-o-telegram-via-tor-no-android/
[windows]: https://antoniomedeiros.dev/blog/2022/03/23/como-usar-o-telegram-via-tor-no-windows/
[ios]: https://antoniomedeiros.dev/blog/2022/03/29/como-usar-o-telegram-via-tor-no-ios-iphone-e-ipad/

{% endcapture %}

{% include update.html date="29/03/2022" message=outros_sos %}

O Telegram tem um aplicativo para Linux que pode ser instalado em qualquer distribuição por meio do Flatpak. Se você já tem o Flatpak instalado no seu sistema, pode facilmente instalar o aplicativo do Telegram pela loja de aplicativos, ou executando o comando a seguir no terminal:

```
# flatpak install org.telegram.desktop
```

Se precisar de mais informações sobre o Flatpak, consulte:

- [Flatpak: tudo o que você precisa saber sobre o gerenciador de pacotes independente de distribuição][Flatpak]

E se precisar de mais informações sobre o Telegram, consulte:

- [O mensageiro Telegram][Telegram]

Certifique-se de que o Tor esteja instalado, ativo e sendo executado com o comando:

```
# systemctl status tor
```

{% include image.html src='/files/2022/02/telegram-tor-01.jpg' %}

Perceba na tela as palavras _active_ ("ativo") e _running_ ("rodando") em verde. Elas devem aparecer em verde para você também.

Caso o comando acima apresente um resultado diferente para você, instale, ative e inicie o Tor executando os comandos a seguir:

```
# zypper in tor
# systemctl enable tor
# systemctl start tor
```

Agora inicie o Telegram e abra o menu clicando no ícone com três linhas horizontais, no canto superior esquerdo da janela:

{% include image.html src='/files/2022/02/telegram-tor-02-pt.jpg' %}

No menu, clique em **Configurações**:

{% include image.html src='/files/2022/02/telegram-tor-03-pt.jpg' %}

Nas configurações, clique em **Avançado**:

{% include image.html src='/files/2022/02/telegram-tor-04-pt.jpg' %}

Na seção **Rede e proxy**, clique em **Tipo de conexão**:

{% include image.html src='/files/2022/02/telegram-tor-05-pt.jpg' %}

Nas **Configurações de proxy**, selecione **Usar proxy customizado**:

{% include image.html src='/files/2022/02/telegram-tor-06-pt.jpg' %}

Na tela seguinte, preencha as configurações dessa forma:

- Certifique-se de que a opção **SOCKS5** esteja selecionada;
- Em **Nome do host**, informe `localhost`;
- Em **Porta**:
  - Se você está usando o Tor, informe `9050`; ou
  - Se você está usando o Navegador Tor (_Tor Browser_), informe `9150`.

{% include image.html src='/files/2022/02/telegram-tor-07-pt.jpg' %}

Por fim, clique em **Salvar**.

De volta às **Configurações de proxy**, certifique-se de que o Telegram conseguiu se conectar ao _proxy_ (note o **online**):

{% include image.html src='/files/2022/02/telegram-tor-08-pt.jpg' %}

Pronto. Pode fechar todas as configurações e seguir usando o Telegram normalmente.

{% capture atualizacao %}

Um ícone de escudo no canto inferior esquerdo da janela indica que o Telegram está conectado ao _proxy_ (nesse caso, à Rede Tor):

{% include image.html src='/files/2022/03/telegram-tor-09.jpg' %}

Clicando nele, você pode facilmente acessar as configurações de _proxy_.

{% endcapture %}

{% include update.html date="09/03/2022" message=atualizacao %}

Espero que essa dica tenha sido útil. Se ficou com alguma dúvida, não deixe de comentar. Sugestões de outras dicas como essa também são bem-vindas.

[Telegram]:             {% post_url pt/2021-05-09-o-mensageiro-telegram %}
[telegram-censorship]:  https://en.wikipedia.org/wiki/Government_censorship_of_Telegram_Messenger
[Linux]:                https://www.vivaolinux.com.br/linux/
[Tor]:                  https://www.torproject.org/
[Navegador Tor]:        {% post_url pt/2019-07-12-20-aplicativos-que-voce-pode-usar-do-mesmo-jeito-no-linux-e-no-windows-parte-2 %}#18-navegador-tor
[Linux Kamarada 15.3]:  {% post_url pt/2021-12-30-linux-kamarada-15-3-ano-novo-distribuicao-nova %}
[Flatpak]:              {% post_url pt/2022-01-17-flatpak-tudo-o-que-voce-precisa-saber-sobre-o-gerenciador-de-pacotes-independente-de-distribuicao %}
