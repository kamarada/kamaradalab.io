---
date: '2022-01-17 21:30:00 GMT-3'
image: '/files/2022/01/flatpak.jpg'
layout: post
nickname: 'flatpak'
title: 'Flatpak: tudo o que você precisa saber sobre o gerenciador de pacotes independente de distribuição'
excerpt: 'O Flatpak é um gerenciador de pacotes independente de distribuição. Ele trouxe uma alternativa mais simples para instalar programas em diferentes distribuições: contanto que o Flatpak esteja instalado no sistema, o mesmo pacote Flatpak pode ser instalado em qualquer distribuição. Um pacote Flatpak contém não só o aplicativo, como também a maioria das bibliotecas das quais ele precisa para funcionar. Os aplicativos são executados pelo Flatpak em um ambiente de sandbox (“caixa de areia”) que cria um isolamento entre os aplicativos e entre estes e o sistema, aumentando a segurança como um todo.'
---

Até pouco tempo atrás, diferentes distribuições [Linux] usavam diferentes formatos de pacotes, geralmente incompatíveis entre si. Por exemplo, [Debian] e [Ubuntu] usam pacotes [DEB], enquanto [Red Hat], [Fedora], [openSUSE] e [Linux Kamarada][kamarada-15.3] usam pacotes [RPM]. Esses formatos tradicionais de empacotamento traziam alguns inconvenientes para todos -- usuários, desenvolvedores de _software_ e mantenedores de distribuições e de pacotes:

- **Trabalho duplicado empacotando programas:** como desenvolvedores tinham que empacotar o mesmo programa em diferentes formatos para diferentes distribuições, era comum escolher apenas uma ou algumas distribuições para suportar e ignorar todas as outras;
- Isso leva ao segundo ponto, **usuário limitado às distribuições que têm aplicativos:** apenas algumas distribuições tinham uma vasta gama de aplicativos empacotados para fácil instalação e uso, de modo que as distribuições que se podia considerar para uso diário eram limitadas;
- **Usuário limitado aos programas já empacotados:** nem todos os aplicativos estão disponíveis nativamente para todas as distribuições, se o usuário precisa de um programa que não está empacotado para a sua distribuição, precisará baixá-lo e, muitas vezes, compilá-lo manualmente;
- **Pacotes antigos e desatualizados:** distribuições cujo ciclo de vida é organizado em versões, que são suportadas por muito tempo (LTS, _Long Term Support_), geralmente oferecem versões muito antigas de aplicativos. Isso é um problema principalmente em distribuições como o [openSUSE Leap] ou o Debian, nem tanto em distribuições com atualizações frequentes (_rolling release_) como o [openSUSE Tumbleweed] ou o [Arch Linux]; e
- **Dificuldade de oferecer suporte:** como cada distribuição empacota os programas de um jeito, e cada distribuição tem diferentes versões dos programas e de suas dependências, os desenvolvedores não têm controle sobre o que está no computador do usuário, dificultando a solução de problemas (ou mesmo reduzir a chance que eles ocorram, em primeiro lugar).

Buscando resolver esses problemas, alguns formatos alternativos de pacotes surgiram, a exemplo do [Snap], do [Flatpak] e do [AppImage]. Hoje, vamos falar do Flatpak.

{% include image.html src='/files/2022/01/flatpak.jpg' %}

O **[Flatpak]** é um gerenciador de pacotes independente de distribuição. Ele trouxe uma alternativa mais simples para instalar programas em diferentes distribuições: contanto que o Flatpak esteja instalado no sistema, o mesmo pacote Flatpak pode ser instalado em qualquer distribuição. Um pacote Flatpak contém não só o aplicativo, como também a maioria das bibliotecas das quais ele precisa para funcionar. Os aplicativos são executados pelo Flatpak em um ambiente de _sandbox_ ("caixa de areia") que cria um isolamento entre os aplicativos e entre estes e o sistema, aumentando a segurança como um todo.

O Flatpak foi criado originalmente por [Alexander Larsson][alexl] enquanto trabalhava com contêineres na Red Hat. Embora tenha recebido muitas contribuições de desenvolvedores da Red Hat e do [GNOME], o Flatpak não está amarrado nem a essa distribuição, nem a essa área de trabalho, tendo sido desenvolvido como um projeto de [_software_ livre][free-sw] independente desde o início. [Antes][flatpak-history] chamado de xdg-app, teve sua primeira versão lançada em março de 2015 e depois renomeado para Flatpak em maio de 2016. No mês seguinte, o concorrente Snap, que [até então só suportava o Ubuntu][snap-wikipedia], foi portado para outras distribuições.

No momento, o Flatpak é suportado por pelo menos [33 grandes distribuições][flatpak-setup].

Assim como as distribuições possuem o conceito de repositórios, o Flatpak também permite obter pacotes de um ou mais repositórios, sendo o principal deles o [Flathub] em [flathub.org][Flathub]. Na [terminologia do Flatpak][flatpak-concepts], repositórios são chamados de **remotos** (_remotes_), de modo semelhante ao [Git].

Veja a seguir como instalar e configurar o Flatpak no seu sistema, como usá-lo para instalar aplicativos e outras informações que podem ser úteis.

## Como instalar o Flatpak

O [Linux Kamarada 15.3][kamarada-15.3] já traz o Flatpak instalado e o Flathub pré-configurado por padrão. Se você está usando um [LiveDVD/USB] do Linux Kamarada 15.3 ou uma instalação nova feita a partir de um, pode pular esta seção. Se você [atualizou a partir de uma versão anterior][how-to-upgrade], convém passar pelos passos a seguir, pra se certificar de que está tudo no lugar.

O Flatpak está disponível nos [repositórios padrão][opensuse-repos] de todas as versões atualmente suportadas do openSUSE Leap e do openSUSE Tumbleweed. Nessas distribuições, você pode instalar o Flatpak de duas formas: pela interface gráfica, usando a instalação com 1 clique (*1-Click Install*), ou pelo terminal, usando o gerenciador de pacotes **[zypper]**. Escolha a que prefere.

Para instalar o Flatpak usando a instalação com 1 clique, clique no botão abaixo:

<p class='text-center'><a class='btn btn-sm btn-outline-primary' href='/downloads/flatpak.ymp'><i class='fas fa-bolt'></i> Instalação com 1 clique</a></p>

Para instalar o Flatpak usando o terminal, execute o comando a seguir:

```
# zypper in flatpak
```

Depois, adicione o repositório Flathub executando:

```
# flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
```

Por fim, reinicie o computador (sei que isso não é usual em se tratando de Linux, mas esse é o último passo das [instruções de instalação do Flatpak no openSUSE][flatpak-setup-opensuse]).

Se você usa outra distribuição, consulte como instalar o Flatpak no [_site_ do Flatpak][flatpak-setup].

De agora em diante, focarei no Linux Kamarada 15.3 e nas distribuições do openSUSE.

## Como instalar aplicativos com o Flatpak

Agora você tem tudo o que precisa para instalar aplicativos Flatpak no Linux Kamarada usando o aplicativo **Programas** do GNOME, o terminal e/ou o _site_ do Flathub.

### 1) Pelo aplicativo Programas do GNOME

Você pode pesquisar aplicativos Flatpak para instalar usando o _app_ **Programas**. Para isso, abra o menu **Atividades**, digite `programas` e clique no ícone do aplicativo:

{% include image.html src='/files/2022/01/flatpak-01-pt.jpg' %}

No _app_ **Programas**, pesquise pelo programa que deseja instalar, seja navegando pelas categorias, seja por meio do botão com ícone de **lupa** no canto superior esquerdo:

{% include image.html src='/files/2022/01/flatpak-02-pt.jpg' %}

Quando encontrar o programa, clique nele:

{% include image.html src='/files/2022/01/flatpak-03-pt.jpg' %}

A tela seguinte mostra várias informações sobre o programa. Você pode conferi-las e clicar no botão **Instalar**:

{% include image.html src='/files/2022/01/flatpak-04-pt.jpg' %}

A senha do administrador (usuário _root_) será exigida. Você deve fornecê-la para instalar o programa.

Você pode conferir que o programa vem do Flathub observando o campo **Fonte**, mais abaixo na tela:

{% include image.html src='/files/2022/01/flatpak-05-pt.jpg' %}

Existem programas que podem ser instalados tanto a partir do Flathub quanto a partir de um repositório da distribuição. Nesses casos, um menu **Fonte**, na parte superior da janela, permite selecionar de onde obter o programa:

{% include image.html src='/files/2022/01/flatpak-06-pt.jpg' %}

Conforme você seleciona uma fonte ou outra, as informações sobre o programa mudam, você pode compará-las:

<div class="row">
    <div class="col-md">
        {% include image.html src='/files/2022/01/flatpak-07-pt.jpg' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2022/01/flatpak-08-pt.jpg' %}
    </div>
</div>

### 2) Pelo site do Flathub

Você também pode procurar por aplicativos Flatpak e instalá-los usando seu navegador preferido. Basta acessar o _site_ do Flathub em [flathub.org][Flathub] e navegar pelas categorias ou pesquisar pelo nome do programa que deseja instalar:

{% include image.html src='/files/2022/01/flatpak-09.jpg' %}

Quando encontrar o programa, clique nele. A página seguinte mostra várias informações sobre o programa. Você pode conferi-las e clicar no botão **Install** (instalar):

{% include image.html src='/files/2022/01/flatpak-10-pt.jpg' %}

O navegador vai baixar um arquivo com extensão terminada em `.flatpakref`. Informe que quer abri-lo usando o aplicativo **Programas**, como na imagem acima.

Aberto o aplicativo **Programas**, clique no botão **Instalar**:

{% include image.html src='/files/2022/01/flatpak-11-pt.jpg' %}

### 3) Pelo terminal

[**flatpak**][flatpak-man] é o comando principal do Flatpak, ao qual comandos específicos são adicionados.

Para pesquisar um aplicativo, use o comando **flatpak search**. Por exemplo:

```
$ flatpak search spotify
```

{% include image.html src='/files/2022/01/flatpak-12-pt.jpg' %}

O resultado traz, dentre outras informações, o **ID do aplicativo**, que é usado para as demais operações com o Flatpak. Nesse exemplo, `com.spotify.Client`.

Para instalar o aplicativo, use o comando **flatpak install**. Por exemplo:

```
$ flatpak install com.spotify.Client
```

Uma dica é usar o _site_ do Flathub para pesquisar o aplicativo e descobrir seu ID, que é a última parte do endereço da página:

{% include image.html src='/files/2022/01/flatpak-13.jpg' %}

Ou copiar o comando para instalação, que aparece ao final da página:

{% include image.html src='/files/2022/01/flatpak-14.jpg' %}

## Como iniciar um aplicativo Flatpak

Depois de instalado, o programa pode ser iniciado pelo menu **Atividades**, como de costume.

Você também pode iniciar um aplicativo Flatpak pelo terminal, usando o comando **flatpak run**. Por exemplo:

```
$ flatpak run com.spotify.Client
```

## Como atualizar aplicativos Flatpak

Você pode atualizar os aplicativos Flatpak instalados pelo _app_ **Programas**. Para isso, use a aba **Atualizações**:

{% include image.html src='/files/2022/01/flatpak-15-pt.jpg' %}

Você pode atualizar todos os aplicativos de uma só vez clicando em **Atualizar tudo**.

Ou, se quiser atualizar apenas um aplicativo, clique no botão **Atualizar** correspondente.

Pelo terminal, para atualizar todos os aplicativos de uma só vez, execute:

```
$ flatpak update
```

Para atualizar apenas um aplicativo, forneça seu ID ao comando **flatpak update**. Por exemplo:

```
$ flatpak update io.lbry.lbry-app
```

## Como desinstalar um aplicativo Flatpak

Você pode desinstalar um aplicativo Flatpak pelo _app_ **Programas**. Para isso, pesquise pelo aplicativo, clique nele na lista e, na tela seguinte, que mostra suas informações, clique em **Remover**:

{% include image.html src='/files/2022/01/flatpak-16-pt.jpg' %}

Você também pode desinstalar um aplicativo Flatpak pelo terminal, usando o comando **flatpak uninstall**. Por exemplo:

```
$ flatpak uninstall com.spotify.Client
```

## Limitações

Para não dizer que só falei bem do Flatpak, listo a seguir alguns de seus problemas. Ao usar o Flatpak, esteja atento a eles:

- O Flatpak não foi projetado para ser usado em servidores, apenas em _desktops_ Linux;
- Um pacote Flatpak, uma vez que reúne aplicativo e dependências, consome mais espaço em disco do que pacotes RPM tradicionais; e
- Como aplicativos Flatpak são executados em ambientes isolados, isso pode limitar algumas funcionalidades de alguns aplicativos. Por exemplo, os aplicativos Flatpak podem aparecer com o tema padrão do GNOME, em vez do seu [tema GTK personalizado][gtk-themes], ou podem ter acesso a apenas algumas pastas, como Documentos e Downloads.

## Conclusão

Eu acredito que a praticidade, a facilidade de usar o Flatpak e a conveniência proporcionada pela grande quantidade de aplicativos disponíveis no Flathub mais que sobressaem as limitações do Flatpak, que amplia as possibilidades dos usuários do Linux Kamarada.

Eu sugiro usar o Flatpak da mesma forma que eu uso: eu dou preferência a instalar aplicativos a partir dos repositórios da própria distribuição. Mas se um aplicativo não está nesses repositórios, ou até está, mas eu preciso de uma versão mais nova que tem no Flathub, então eu instalo esse aplicativo usando o Flatpak.

Espero que esse guia tenha sido útil. Comente se tiver dúvidas ou sugestões. Até a próxima!

## Referências

- [Flatpak documentation][flatpak-docs]
- [How to Install Flatpak & Flathub on Ubuntu (Complete Guide) - OMG! Ubuntu!][omgubuntu]
- [openSUSE Quick Setup - Flatpak][flatpak-setup-opensuse]
- [Flatpak - openSUSE Wiki][opensuse-wiki]
- [What is Flatpak in Linux? - It's FOSS][itsfoss]

[Linux]:                    https://www.vivaolinux.com.br/linux/
[debian]:                   https://www.debian.org/index.pt.html
[ubuntu]:                   https://ubuntu.com/
[deb]:                      https://pt.wikipedia.org/wiki/.deb
[Red Hat]:                  https://www.redhat.com/pt-br/technologies/linux-platforms/enterprise-linux
[fedora]:                   https://getfedora.org/pt_BR/
[opensuse]:                 https://www.opensuse.org/
[kamarada-15.3]:            {% post_url pt/2021-12-30-linux-kamarada-15-3-ano-novo-distribuicao-nova %}
[rpm]:                      https://pt.wikipedia.org/wiki/RPM_(software)
[openSUSE Leap]:            {% post_url pt/2020-12-06-opensuse-leap-e-opensuse-tumbleweed-qual-e-a-diferenca %}
[openSUSE Tumbleweed]:      {% post_url pt/2020-12-06-opensuse-leap-e-opensuse-tumbleweed-qual-e-a-diferenca %}
[Arch Linux]:               https://archlinux.org/
[Snap]:                     https://snapcraft.io/about
[Flatpak]:                  https://flatpak.org/
[AppImage]:                 https://appimage.org/
[alexl]:                    https://blogs.gnome.org/alexl/2018/06/20/flatpak-a-history/
[GNOME]:                    https://br.gnome.org/
[free-sw]:                  https://www.gnu.org/philosophy/free-sw.pt-br.html
[flatpak-history]:          https://flatpak.org/about/
[snap-wikipedia]:           https://en.wikipedia.org/wiki/Snap_(package_manager)#Adoption
[flatpak-setup]:            https://flatpak.org/setup/
[Flathub]:                  https://flathub.org/
[flatpak-concepts]:         https://docs.flatpak.org/en/latest/basic-concepts.html#repositories
[Git]:                      https://git-scm.com/
[LiveDVD/USB]:              {% post_url pt/2015-11-25-o-que-e-um-livecd-um-livedvd-um-liveusb %}
[how-to-upgrade]:           {% post_url pt/2021-12-23-linux-kamarada-e-opensuse-leap-como-atualizar-da-versao-152-para-a-153 %}
[opensuse-repos]:           https://en.opensuse.org/Package_repositories
[zypper]:                   https://en.opensuse.org/Portal:Zypper
[flatpak-setup-opensuse]:   https://flatpak.org/setup/openSUSE
[flatpak-man]:              https://www.man7.org/linux/man-pages/man1/flatpak.1.html
[gtk-themes]:               {% post_url pt/2019-05-10-personalize-o-gnome-com-temas %}
[flatpak-docs]:             https://docs.flatpak.org/
[omgubuntu]:                https://www.omgubuntu.co.uk/how-to-install-flatpak-on-ubuntu
[opensuse-wiki]:            https://en.opensuse.org/Flatpak
[itsfoss]:                  https://itsfoss.com/what-is-flatpak/
