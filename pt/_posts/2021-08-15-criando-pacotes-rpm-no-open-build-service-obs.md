---
date: 2021-08-15 01:20:00 GMT-3
image: '/files/2021/04/kick-off.jpg'
layout: post
published: true
nickname: 'obs-rpm'
title: 'Criando pacotes RPM no Open Build Service (OBS)'
---

{% include image.html src='/files/2021/04/kick-off.jpg' %}

Dando sequência à nossa série de _posts_ sobre [como fazer uma distribuição Linux baseada no openSUSE][making-of-1], hoje eu vou mostrar pra você como criar um [pacote RPM][rpm] no [Open Build Service (OBS)][obs]. Lembrando que eu uso o openSUSE Build Service ([build.opensuse.org][buildoo]), que é o servidor de referência público e gratuito do OBS.&nbsp;

Na verdade, já criamos um pacote no [primeiro _post_][making-of-1], mas eu "coloquei o carro na frente dos bois" e expliquei como criar um pacote usando o conjunto completo de [Git] mais OBS. Você deve se lembrar que eu disse que o OBS pode ser usado para todo o processo de armazenar e gerenciar o código-fonte, mas também compilar e empacotar. Ou seja, o OBS não é apenas uma ferramenta que cria pacotes, ele também tem um [sistema de controle de versões][scm] próprio. Hoje vamos ver como um pacote pode nascer no OBS.

Se você ainda não leu o _post_ anterior, recomendo que comece por ele:

- [Como fazer uma distro baseada no openSUSE (bastidores do Linux Kamarada): começando][making-of-1]

Para o que vamos fazer hoje, o ideal seria já termos noções sobre empacotamento RPM com **[rpmbuild]**, que podem ser adquiridas lendo o [Fedora RPM Guide][fedora-rpm-guide] e outras referências. Mas vou tentar mostrar uma visão mais "mão na massa" (_hands on_) de como fazer um pacote RPM usando o OBS. Caso você tenha alguma dúvida, pode comentar ou consultar as referências no final da página. Além da documentação do openSUSE, eu costumo consultar também a documentação do [Fedora], porque é outra distribuição que usa o formato RPM.

Aliás, [o Fedora é a versão comunitária do Red Hat Enterprise Linux][fedora-vs-red-hat] (assim como o [openSUSE] é a versão comunitária do [SUSE Linux Enterprise][sle]) e o [Red Hat] é o "pai" do RPM (já viu o que significa a sigla [RPM]?) Portanto, a documentação do Fedora é uma fonte valiosa de informação.

## O que vamos empacotar

Dentre os pacotes que criei para a versão atual da distribuição, a [15.2], selecionei um pacote com um arquivo _spec_ pequeno, básico e que ilustre bem o básico que deve vir nesse arquivo. Sendo assim, vamos recriar para a versão 15.3 o pacote [**sound-theme-materia**][sound-theme-materia-15.2].

Esse pacote permite a fácil instalação nas [distribuições do openSUSE][leap-tumbleweed] (Leap e Tumbleweed) e no Linux Kamarada do [tema de som Materia][materia-sound-theme] (na verdade, no Linux Kamarada, ele já vem instalado por padrão). Esse tema de som acompanha o [tema GTK Materia][materia-theme], que visa trazer a experiência do [Material Design] do [Google] (o mesmo _design_ do [Android]) para a área de trabalho [GNOME] e outras áreas de trabalho baseadas na biblioteca [GTK].

O código-fonte do tema de som Materia, assim como as instruções para instalá-lo a partir desse código-fonte, estão disponíveis no [GitHub][materia-sound-theme]:

- [https://github.com/nana-4/materia-sound-theme][materia-sound-theme]

{% include image.html src='/files/2021/08/rpm-obs-01.jpg' %}

Antes de criar propriamente o pacote, geralmente é uma boa ideia tentar compilá-lo e instalá-lo na própria máquina ou em uma [máquina virtual], até para descobrir as dependências e se familiarizar com o processo de compilação e instalação do programa.

Embora os desenvolvedores tenham chamado o repositório original de [materia-sound-theme], e geralmente é uma boa ideia dar o mesmo nome ao pacote (na verdade, você pode nomear o pacote como quiser), eu preferi chamar o pacote de **[sound-theme-materia]** para seguir um padrão que eu percebi no openSUSE. Veja como são chamados outros pacotes de temas de sons: **[sound-theme-freedesktop]**, **[sound-theme-yaru]**...

## Criando o pacote no OBS

Uma vez logado no OBS, acesse o projeto onde deseja criar o pacote (no meu caso, o projeto de desenvolvimento da distribuição, o [**home:kamarada:15.3:dev**](https://build.opensuse.org/project/show/home:kamarada:15.3:dev)).

Mude para a aba **Overview** (visão geral) e clique em **Create Package** (criar pacote):

{% include image.html src='/files/2021/08/rpm-obs-02.jpg' %}

Dê pelo menos um nome (**Name**) para o pacote (no meu caso, `sound-theme-materia`) e clique em **Create** (criar):

{% include image.html src='/files/2021/08/rpm-obs-03.jpg' %}

O pacote é criado no OBS. Por enquanto, não possui arquivos:

{% include image.html src='/files/2021/08/rpm-obs-04.jpg' %}

## Fazendo checkout do pacote

Criado o pacote no OBS, até poderíamos trabalhar nele usando apenas a interface _web_. Mas vamos trazê-lo para o computador para trabalhar nele localmente, de modo análogo ao que fazemos ao clonar um repositório do Git.

Como vimos no [_post_ anterior][making-of-1], podemos criar uma cópia local ("fazer _checkout_") do projeto inteiro, caso ainda não tenhamos feito isso:

```
$ osc checkout home:kamarada:15.3:dev
```

Se já temos uma cópia local do projeto (e esse é o caso), para baixar esse novo pacote, temos duas opções.

Uma delas é entrar na pasta do projeto e fazer _checkout_ apenas do pacote:

```
$ cd home:kamarada:15.3:dev
$ osc co sound-theme-materia
```

(você pode abreviar a opção **checkout** do comando **[osc]** como apenas **co**)

A outra é entrar na pasta do projeto e atualizar (_update_) a cópia local, o que vai baixar não apenas o pacote novo, como também quaisquer alterações feitas em outros pacotes e que ainda não tenham sido sincronizadas para a cópia local:

```
$ cd home:kamarada:15.3:dev
$ osc up
```

(aqui também você pode usar **update** ou a abreviação **up**)

Seja qual for a opção escolhida, depois entre na pasta do pacote:

```
$ cd sound-theme-materia
```

## Baixando o código-fonte do upstream

A palavra _upstream_ poderia ser traduzida, em uma tradução literal, como "rio acima" ou "fluxo acima". No desenvolvimento de _software_, chamamos de **_[upstream]_** o autor original do _software_. Esse termo é comumente usado por empacotadores ou desenvolvedores que fazem _forks_ no Git.

Por exemplo, pode acontecer de o [Projeto openSUSE][opensuse] identificar um _bug_ no [LibreOffice] e corrigir esse _bug_ primeiro no seu pacote, para depois enviar a correção para o _upstream_ (o Projeto LibreOffice). Ou – o que acredito ser mais comum – o _bug_ ser corrigido primeiro no _upstream_ para então a atualização ser baixada pela distribuição. De uma forma ou de outra, quando a correção é liberada pelo _upstream_, todas as distribuições que empacotam aquele programa são beneficiadas, todos saem ganhando.

Outro exemplo é o que o Projeto openSUSE está fazendo ao tentar introduzir o suporte ao [Raspberry Pi 4][rpi4] primeiro no _upstream_ (o [_kernel_ Linux][kernel]), como já expliquei em [outro texto][opensuse-rpi4].

Voltando ao nosso pacote... como eu disse, seu código-fonte é hospedado no [GitHub][materia-sound-theme]. Então, vamos acessar a [página do projeto no GitHub][materia-sound-theme] e clicar em **Releases** (lançamentos, versões):

{% include image.html src='/files/2021/08/rpm-obs-05.jpg' %}

Só houve uma versão lançada, a `v0.1`. Baixe o código-fonte dessa versão clicando no _link_ **Source code**, e dê preferência ao formato `.tar.gz`, por ser mais amigável ao [Linux]:

{% include image.html src='/files/2021/08/rpm-obs-06.jpg' %}

Quando o _download_ terminar, mova o arquivo `materia-sound-theme-0.1.tar.gz` para a pasta do projeto.

## Escrevendo o arquivo spec

O arquivo _spec_ (do inglês _spec file_, abreviação de _specification file_, arquivo de especificação) contém informações sobre o pacote (como nome, descrição, versão, autor, _site_, licença de uso, etc), os comandos necessários para compilá-lo e instalá-lo, sua lista de arquivos, histórico de alterações (_changelog_) e possivelmente outras informações.

Todo pacote RPM tem um arquivo _spec_ correspondente. O arquivo _spec_ é um arquivo de texto e normalmente é nomeado com o nome do pacote.

Para o nosso pacote **[sound-theme-materia]**, vamos criar um arquivo de texto vazio chamado `sound-theme-materia.spec`:

```
$ touch sound-theme-materia.spec
```

Abra esse arquivo com seu editor de texto preferido. Pode ser o **Editor de texto** padrão do GNOME (o [Gedit]), ou, se você é mais sinistro e prefere editar o arquivo no terminal mesmo, o **[nano]**, o **[joe]** ou o **[vim]**. O Linux Kamarada vem "de fábrica" com essas quatro opções.

Como veremos, o arquivo _spec_ tem uma sintaxe própria, que deve ser obedecida para que as informações possam ser entendidas pelo RPM, pelo OBS e por outros programas.

## Seção introdutória do arquivo spec

Comece copiando e colando para o arquivo _spec_ sua seção introdutória:

```
Name:           sound-theme-materia
Version:        0.1
Release:        0
Summary:        Materia Sound Theme
License:        GPL-3.0
Group:          System/Libraries
Url:            https://github.com/nana-4/materia-sound-theme
Source:         https://github.com/nana-4/materia-sound-theme/archive/v%{version}.tar.gz#/materia-sound-theme-%{version}.tar.gz
BuildRequires:  fdupes
BuildRequires:  meson
BuildRequires:  ninja
BuildArch:      noarch
```

Essa seção contém informações gerais sobre o pacote. Explico cada linha a seguir.

- **Name:** o nome do pacote. Não deve conter espaços ou outros caracteres de espaço em branco, como tabulações ou novas linhas. Use caracteres válidos para nomes de arquivo. Sugiro não usar muita criatividade aqui. Pense em quem vai digitar um comando como este para instalar seu pacote:

```
# zypper in sound-theme-materia
```

Você também pode pesquisar pacotes semelhantes já existentes na distribuição e nomear o seu de acordo. Fiz assim no exemplo: **sound-theme-freedesktop**, **sound-theme-yaru**...

- **Version:** versão do pacote, de preferência no formato de números separados por pontos (x.y.z). Aqui usamos a versão informada pelo próprio _upstream_ (`0.1`). Tenha em mente que esse número de versão será usado pelo sistema para comparar a versão instalada com a versão disponível no repositório, para determinar se o pacote deve ser atualizado. No nosso exemplo, a próxima versão desse pacote pode ser `0.1.1` ou `0.2`, mas não `0.0.1`. Procure se informar sobre [versionamento semântico][semver].

- **Release:** poderíamos traduzir para "lançamento". É uma espécie de versão do próprio pacote. Se esse número mudou, mas o da versão permaneceu, indica que o pacote mudou, ainda que o programa empacotado não tenha mudado. Uma boa prática é começar com `0` ou `1` e incrementar esse número a cada modificação do arquivo _spec_. Então, por exemplo, estamos criando agora o pacote 0 da versão 0.1. Suponhamos que eu lancei o pacote, mas depois percebi um erro de digitação no arquivo _spec_. Então eu corrijo esse erro, incremento o _release_ para 1 e lanço a atualização.

- **Summary:** resumo. Uma breve descrição do pacote em uma linha. Procure não usar muito mais que 50 caracteres.

- **License:** licença de uso do _software_. Essa informação também deve ser obtida do _upstream_. A licença deve ser declarada usando o formato de nome curto [SPDX]. Exemplos: `GPL-3.0`, `MIT`, `LGPL-2.1-or-later`, `GPL-2.0+`, `GPL-2.0-only`, `Apache-2.0`, etc.

- **Group:** grupo ao qual pertence o pacote. Permite uma espécie de classificação dos pacotes. Exemplos: `System/Libraries`, `Productivity/Office/Suite`, `Hardware/Printing`, veja mais [aqui][group-1] e [aqui][group-2]. Vale observar que esse campo não é obrigatório e está caindo em desuso. A [documentação do Fedora][fedora-packaging-guidelines] explicitamente desaconselha o seu uso. Caso você queira usá-lo (por enquanto, eu tenho usado), dê preferência a informar um nome de grupo listado em um dos _links_ anteriores.

- **Url:** o _site_ oficial do pacote ou uma página na qual pode-se obter mais informações sobre ele. Por exemplo, a `Url` do pacote **[chromium]** é [`https://www.chromium.org/`](https://www.chromium.org/). No caso do pacote que estou criando, como aparentemente ele não tem um _site_ oficial, informei a [página do projeto no GitHub][materia-sound-theme].

- **Source:** código-fonte. A maioria dos pacotes tem um ou mais arquivos de código-fonte, que devem ser listados no arquivo _spec_. Muitas vezes (e esse é o caso aqui), há um arquivo `.tar.gz` que contém os arquivos do código-fonte compactados. Se o arquivo foi baixado da Internet, você deve fornecer o _link_ para _download_. Isso é apenas para documentação e conveniência, o arquivo não será baixado na hora de criar o pacote. Nós já baixamos esse arquivo antes.

Se você precisa referenciar mais de um arquivo de código-fonte, use uma _tag_ `Source` para cada arquivo, numerando todas as _tags_ a partir do zero: `Source0`, `Source1`, `Source2`... isso é útil para separar o código fornecido pelo _upstream_, que deve ser mantido intacto, como veio, e personalizações feitas pela distribuição, que podem ser listadas como _patches_.

Também é comum o arquivo ser obtido de um sistema de controle de versão, como o Git (e esse também é o caso aqui). Nesses casos, as URLs de alguns serviços já são conhecidas, há modelos que você pode reutilizar na [documentação do Fedora][source-url].

É oportuno falar sobre as [**macros do RPM**][macros-1], que podem ser entendidas como variáveis, funções e em alguns casos até mesmo _scripts_ providos pelo sistema de empacotamento RPM para facilitar a escrita do arquivo _spec_ e torná-lo mais legível. Aqui usamos a macro `%{version}`, que será substituída pela versão do pacote, declarada em `Version`.

Você pode consultar listas de macros predefinidas, e também como definir suas próprias macros, nos _links_ a seguir:

- [openSUSE:Packaging Conventions RPM Macros - openSUSE Wiki][macros-2]
- [RPM Macros - Fedora Packaging Guidelines][macros-3]
- [9.7. Defining Spec File Macros - Fedora RPM Guide][macros-1]
- [rpm.org - Documentation - Packager Documentation - Macro syntax][macros-4]

- **BuildRequires:** dependência para compilar o pacote. Comumente, você precisa que outros programas estejam previamente instalados no sistema até para que você consiga compilar o código-fonte do programa. Essa informação é fornecida pelo _upstream_, normalmente nas instruções de compilação. Nesse exemplo, precisamos dos pacotes **[fdupes]**, **[meson][meson-softwareoo]** e **[ninja]** para compilar o tema de som. Você pode listar todas as dependências em uma única linha, separadas por espaço ou vírgula, ou pode usar uma linha para cada dependência (o que é mais legível e também mais comum).

O tema de som que estamos empacotando não tem dependências adicionais. Mas os pacotes também podem declarar dependências para funcionar (**Requires**) e dependências para instalar (**PreReq**), além de outros tipos de dependências e/ou conflitos com outros pacotes (**Conflicts**). Para mais informações, consulte as referências no final dessa página.

- **BuildArch:** arquitetura a que se destina o pacote. Se você estiver empacotando arquivos que são independentes da arquitetura do computador (por exemplo, _shell scripts_, arquivos de dados ou temas de som, como esse exemplo), informe `noarch`. Exemplos de arquiteturas comuns incluem: `i586`, `i686`, `x86_64` e `aarch64`.

Caso você não saiba a arquitetura do seu computador, use o comando `uname -m` para descobrir. No meu _notebook_ (PC de 64 _bits_), esse comando retorna `x86_64`. No meu Raspberry Pi 4, esse comando retorna `aarch64`.

Note que na verdade o campo `BuildArch` se refere ao computador onde o pacote será instalado, que não necessariamente é da mesma arquitetura que o seu próprio computador. Por meio do OBS, você pode, por exemplo, usar seu PC para criar um pacote que será instalado em um Raspberry Pi. Nesse caso, você deve usar `BuildArch: aarch64`.

## Demais seções do arquivo spec

A seguir, apresento as demais seções do arquivo _spec_. Copie e cole os trechos a seguir no seu arquivo _spec_.

```
%description
Materia Sound Theme is a freedesktop sound theme using Google's Material sound
resources. It follows the Material sound guidelines.
```

Na seção `%description`, você deve fornecer uma descrição mais longa do pacote. Aqui você pode usar uma linha ou mais, tantas quantas necessárias, mas todas devem ter no máximo 80 caracteres (como se fosse a margem de uma página). Linhas em branco separam parágrafos.

```
%prep
%autosetup -n materia-sound-theme-%{version}
```

A seção `%prep` lista comandos, em formato de _script_, para preparar o pacote para compilação. Normalmente o código-fonte vem compactado dentro de um arquivo `.tar.gz` e é na seção `%prep` que ele é extraído. Nessa seção também podem ser aplicados _patches_, seja para corrigir erros nos programas, seja para aplicar personalizações da distribuição.

Nesse exemplo, é usada a macro [`%autosetup`][autosetup], que faz essas duas tarefas: descompactar o código-fonte (nesse caso, somente um arquivo `.tar.gz`) e aplicar eventuais _patches_ (nesse caso, nenhum). A opção `-n` informa a pasta que é criada quando o conteúdo do arquivo compactado é extraído. Outras macros comuns na seção `%prep` são [`%setup`][setup] e [`%patch`][patch].

```
%build
%meson
%meson_build
```

A seção `%build` lista comandos, também em formato de _script_, para compilar o pacote, ou seja, deixá-lo pronto para instalação. Nessa seção, são comuns a macro `%configure` e o comando **[make]**.

Nesse exemplo, são usadas as macros [`%meson`][meson-macros] e [`%meson_build`][meson-macros], relacionadas ao sistema de compilação [Meson], usado para compilar o tema de som que estamos empacotando.

```
%install
%meson_install
%fdupes %{buildroot}%{_datadir}/sounds/
```

A seção `%install` lista comandos, também em formato de _script_, para instalar o pacote. Esses comandos são executados no computador do empacotador, simulando a instalação. Comumente essa seção é responsável por copiar os arquivos compilados do diretório de compilação (representado pela macro `%{_builddir}`) para o diretório que simula a raiz do computador onde o pacote será instalado (macro `%{buildroot}`).

É preferível, sempre que possível, usar macros em vez de caminhos fixos. Por exemplo, a macro `%{_datadir}` por padrão equivale ao caminho `/usr/share`. Para uma lista de macros para caminhos, consulte a página [RPM Macros][macros-3] na documentação do Fedora.

No nosso arquivo _spec_ de exemplo, aparece a macro [`%fdupes`][fdupes-macro], que é relativamente comum e é usada para substituir eventuais arquivos duplicados por _links_ simbólicos.

```
%files
%defattr(-,root,root)
%{_datadir}/sounds/*
```

A seção `%files` contém uma lista de todos os arquivos do pacote, um arquivo por linha. Você pode usar o caractere curinga (`*`) para se referir a vários arquivos. Além de caminhos para arquivos, você também pode informar caminhos para pastas, declarando que todo o conteúdo dessas pastas pertence ao pacote.

```
%changelog
```

A seção `%changelog` normalmente listaria um histórico de mudanças do pacote. Mas o Open Build Service requer que declaremos essa seção, mas a deixemos em branco, e façamos o histórico de mudanças em um arquivo à parte, que é o que veremos a seguir.

Terminamos de analisar o arquivo _spec_ de exemplo. Aqui, foquei no necessário para entender esse exemplo de arquivo _spec_, mas muitas outras coisas podem aparecer em um arquivo _spec_. Para mais informações, consulte as referências no final da página.

## Escrevendo o arquivo changes

Cada vez que fizer mudanças no pacote, você deve adicionar uma entrada no _changelog_ (histórico de mudanças) descrevendo essa mudança. Isso é importante não apenas para ter uma ideia sobre a história de um pacote, mas também para permitir que usuários, outros empacotadores e testadores identifiquem facilmente as alterações feitas.

O Open Build Service usa um arquivo separado para o _changelog_. Esse arquivo tem o mesmo nome do arquivo _spec_, mas a extensão é `.changes` em vez de `.spec`.

Para adicionar uma entrada ao _changelog_, use o comando:

```
$ osc vc
```

(o arquivo _changes_ será criado, caso ainda não exista)

O editor de texto padrão é usado para abrir o arquivo _changes_. A data atual é sugerida:

```
-------------------------------------------------------------------
Sun Aug 15 01:30:00 UTC 2021 - Antônio Vinícius Menezes Medeiros Medeiros <linuxkamarada@gmail.com>

-
```

Como não fiz nenhuma alteração nesse pacote do Linux Kamarada 15.2 para o 15.3, em vez de criar um novo arquivo _changes_, vou copiar e colar o anterior. Veja o exemplo:

```
-------------------------------------------------------------------
Sun Jul  5 01:25:41 UTC 2020 - Antônio Vinícius Menezes Medeiros Medeiros <linuxkamarada@gmail.com> - 0.1

- First packaging, initial release (v0.1)
```

Para mais informações sobre o arquivo _changes_, consulte:

- [openSUSE:Creating a changes file (RPM) - openSUSE Wiki][opensuse-changes]

## Compilando o pacote localmente

Caso o pacote não seja de uma arquitetura diferente do seu computador, pode ser uma boa ideia compilar o pacote antes de enviá-lo para o OBS. Com isso, se houver algo errado, você pode corrigir o erro antes do envio. Para compilar seu pacote localmente, execute:

```
$ osc build
```

Se houver ambiguidade, pode ser necessário informar a distribuição e a arquitetura:

```
$ osc build openSUSE_Leap_15.3 x86_64
```

O **osc** vai precisar baixar pacotes e pergunta se pode confiar no repositório:

```
Updating cache of required packages

The build root needs packages from project 'SUSE:SLE-15:Update'.
Note that malicious packages can compromise the build result or even your system.
Would you like to ...
0 - quit (default)
1 - always trust packages from 'SUSE:SLE-15:Update'
2 - trust packages just this time
?
```

Digite `1` para sempre confiar nesse repositório e tecle **Enter**.

Note que o OBS (e o **osc**) não compila o pacote diretamente no sistema do computador. Ele baixa todos os pacotes necessários e faz uma instalação mínima do Linux "enjaulada" (**[chroot]**), para então compilar o pacote nessa instalação. Com isso, o OBS garante que o arquivo _spec_ foi escrito corretamente e que o pacote não está sendo compilado com sucesso apenas porque é no seu computador. Isso garante que a compilação do pacote pode ser reproduzida (_[reproducible build]_) por qualquer um, em qualquer computador.

Quando terminar de baixar os pacotes, o **osc** vai exigir a senha do _root_ para criar o ambiente **chroot** e começar a compilação do pacote.

Ao final, se tudo der certo, você terá uma saída parecida com esta:

```
[   28s] RPMLINT report:
[   28s] ===============
[   28s] 2 packages and 0 specfiles checked; 0 errors, 0 warnings.
[   28s]
[   28s]
[   28s] kamarada-pc finished "build sound-theme-materia.spec" at Sun Aug 15 01:53:33 UTC 2021.
[   28s]

/var/tmp/build-root/openSUSE_Leap_15.3-x86_64/home/abuild/rpmbuild/SRPMS/sound-theme-materia-0.1-0.src.rpm

/var/tmp/build-root/openSUSE_Leap_15.3-x86_64/home/abuild/rpmbuild/RPMS/noarch/sound-theme-materia-0.1-0.noarch.rpm
```

As últimas linhas indicam onde você pode encontrar o pacote RPM fonte (`.src.rpm`) e o pacote RPM em si (`.rpm`). Como mais um teste, você pode instalá-lo no seu computador ou máquina virtual (dica: para copiar e colar no terminal, use **Ctrl + Shift + C** e **Ctrl + Shift + V**):

```
$ sudo rpm -Uvh /var/tmp/build-root/openSUSE_Leap_15.3-x86_64/home/abuild/rpmbuild/RPMS/noarch/sound-theme-materia-0.1-0.noarch.rpm
```

## Enviando o pacote para o OBS

Assim que o pacote estiver como deseja, use os comandos a seguir para enviar seu trabalho para o OBS (se você já tiver familiaridade com algum sistema de controle de versões, como o Git, verá que a lógica é parecida).

Confira o _status_ da pasta:

```
$ osc status
?    materia-sound-theme-0.1.tar.gz
?    sound-theme-materia.changes
?    sound-theme-materia.spec
```

Nesse caso, nenhum dos 3 arquivos está no controle de versões do OBS. Para adicioná-los, use:

```
$ osc add *
A    materia-sound-theme-0.1.tar.gz
A    sound-theme-materia.changes
A    sound-theme-materia.spec
```

Por fim, para enviar as alterações para o OBS, execute:

```
$ osc commit
```

Tal como no Git, o editor de texto é aberto para que você forneça uma mensagem para o _commit_ (alteração). A entrada mais recente do _changelog_ é sugerida. Você pode só salvar o arquivo e fechar o editor de texto.

```
Sending    materia-sound-theme-0.1.tar.gz
Sending    sound-theme-materia.changes
Sending    sound-theme-materia.spec
Transmitting file data ..
Committed revision 1.
```

Se você abrir a página do seu pacote no OBS, verá que está sendo compilado (_building_):

- [https://build.opensuse.org/package/show/home:kamarada:15.3:dev/sound-theme-materia](https://build.opensuse.org/package/show/home:kamarada:15.3:dev/sound-theme-materia)

{% include image.html src='/files/2021/08/rpm-obs-07.jpg' %}

Como a compilação local deu certo, é provável que a compilação pelo OBS também dê certo:

{% include image.html src='/files/2021/08/rpm-obs-08.jpg' %}

Você pode clicar no resultado (**succeeded**, "com sucesso") para visualizar o relatório da compilação:

{% include image.html src='/files/2021/08/rpm-obs-09.jpg' %}

Esse relatório é útil quando algo dá errado na compilação.

Voltando à página do pacote, você pode usar o _link_ **Download package** para baixar o pacote RPM para o seu computador.

Note que também é possível encontrá-lo e instalá-lo a partir de [software.opensuse.org](https://software.opensuse.org/).

## Conclusão

Espero que esse tutorial possa ter te ajudado caso você esteja procurando como fazer pacotes RPM com o auxílio do Open Build Service. Aqui, mostrei uma visão "mão na massa" a partir de um exemplo de pacote RPM, mas não esgotei o assunto. Você pode encontrar mais informações sobre o OBS e sobre empacotamento RPM nos _links_ a seguir.

## Referências

- [openSUSE:Build Service Tutorial - openSUSE Wiki][obs-tutorial]
- [openSUSE:Packaging guidelines - openSUSE Wiki][opensuse-packaging]
- [openSUSE:Creating a changes file (RPM) - openSUSE Wiki][opensuse-changes]
- [Fedora RPM Guide][fedora-rpm-guide]
- [Fedora Packager's Guide][fedora-packagers-guide]
- [Fedora Packaging Guidelines][fedora-packaging-guidelines]
- [Maximum RPM][maximum-rpm]

[making-of-1]:                  {% post_url pt/2021-04-12-como-fazer-uma-distro-baseada-no-opensuse-bastidores-do-linux-kamarada-comecando %}
[rpm]:                          https://pt.wikipedia.org/wiki/RPM_(software)
[obs]:                          https://openbuildservice.org/
[buildoo]:                      https://build.opensuse.org
[git]:                          https://git-scm.com/
[scm]:                          https://pt.wikipedia.org/wiki/Sistema_de_controle_de_versões
[rpmbuild]:                     https://man7.org/linux/man-pages/man8/rpmbuild.8.html
[fedora-rpm-guide]:             http://docs.fedoraproject.org/en-US/Fedora_Draft_Documentation/0.1/html/RPM_Guide/index.html
[fedora]:                       https://getfedora.org/
[Red Hat]:                      https://www.redhat.com/pt-br
[fedora-vs-red-hat]:            https://www.redhat.com/pt-br/topics/linux/fedora-vs-red-hat-enterprise-linux
[openSUSE]:                     https://www.opensuse.org/
[sle]:                          https://www.suse.com/pt-br/products/server/

[15.2]:                         {% post_url pt/2020-09-11-linux-kamarada-15.2-venha-para-o-lado-verde-elegante-e-moderno-da-forca %}
[sound-theme-materia-15.2]:     https://build.opensuse.org/package/show/home:kamarada:15.2:dev/sound-theme-materia
[leap-tumbleweed]:              {% post_url pt/2020-12-06-opensuse-leap-e-opensuse-tumbleweed-qual-e-a-diferenca %}
[materia-sound-theme]:          https://github.com/nana-4/materia-sound-theme
[materia-theme]:                https://github.com/nana-4/materia-theme
[Material Design]:              https://material.io/
[Google]:                       https://www.google.com.br/
[Android]:                      https://www.android.com/
[GNOME]:                        https://br.gnome.org/
[GTK]:                          https://www.gtk.org/
[máquina virtual]:              {% post_url pt/2019-10-08-virtualbox-a-forma-mais-facil-de-conhecer-o-linux-sem-precisar-instala-lo %}
[sound-theme-freedesktop]:      https://software.opensuse.org/package/sound-theme-freedesktop
[sound-theme-yaru]:             https://software.opensuse.org/package/sound-theme-yaru

[osc]:                          https://linux.die.net/man/1/osc

[upstream]:                     https://pt.wikipedia.org/wiki/Upstream_(desenvolvimento_de_software)
[LibreOffice]:                  https://pt-br.libreoffice.org/
[rpi4]:                         {% post_url pt/2019-09-20-primeiros-passos-no-raspberry-pi-com-noobs-e-raspbian %}
[kernel]:                       https://www.kernel.org/
[opensuse-rpi4]:                {% post_url pt/2020-10-07-opensuse-no-raspberry-pi-4-parte-1-download-e-instalacao %}
[linux]:                        https://www.vivaolinux.com.br/linux/

[sound-theme-materia]:          https://build.opensuse.org/package/show/home:kamarada:15.3:dev/sound-theme-materia
[Gedit]:                        https://wiki.gnome.org/Apps/Gedit
[nano]:                         https://www.nano-editor.org/
[joe]:                          https://joe-editor.sourceforge.io/
[vim]:                          https://www.vim.org/

[semver]:                       https://semver.org/lang/pt-BR/
[SPDX]:                         https://spdx.org/licenses/
[group-1]:                      https://en.opensuse.org/openSUSE:Package_group_guidelines
[group-2]:                      https://fedoraproject.org/wiki/RPMGroups
[fedora-packaging-guidelines]:  https://docs.fedoraproject.org/en-US/packaging-guidelines/
[chromium]:                     https://build.opensuse.org/package/view_file/openSUSE:Leap:15.3/chromium/chromium.spec
[source-url]:                   https://docs.fedoraproject.org/en-US/packaging-guidelines/SourceURL/#_git_hosting_services
[macros-1]:                     https://docs.fedoraproject.org/en-US/Fedora_Draft_Documentation/0.1/html/RPM_Guide/ch09s07.html
[macros-2]:                     https://en.opensuse.org/openSUSE:Packaging_Conventions_RPM_Macros
[macros-3]:                     https://docs.fedoraproject.org/en-US/packaging-guidelines/RPMMacros/
[macros-4]:                     http://rpm.org/user_doc/macros.html
[fdupes]:                       https://software.opensuse.org/package/fdupes
[meson-softwareoo]:             https://software.opensuse.org/package/meson
[ninja]:                        https://software.opensuse.org/package/ninja
[autosetup]:                    https://rpm.org/user_doc/autosetup.html
[setup]:                        http://ftp.rpm.org/max-rpm/s1-rpm-inside-macros.html#S2-RPM-INSIDE-SETUP-MACRO
[patch]:                        http://ftp.rpm.org/max-rpm/s1-rpm-inside-macros.html#S2-RPM-INSIDE-PATCH-MACRO
[make]:                         https://man7.org/linux/man-pages/man1/make.1.html
[meson-macros]:                 https://docs.fedoraproject.org/en-US/packaging-guidelines/Meson/
[meson]:                        https://mesonbuild.com/
[fdupes-macro]:                 https://en.opensuse.org/openSUSE:Packaging_Conventions_RPM_Macros#.25fdupes

[opensuse-changes]:             https://en.opensuse.org/openSUSE:Creating_a_changes_file_(RPM)

[chroot]:                       https://man7.org/linux/man-pages/man2/chroot.2.html
[reproducible build]:           https://www.suse.com/c/reproducible-builds-in-opensuse-and-sle/

[obs-tutorial]:                 https://en.opensuse.org/openSUSE:Build_Service_Tutorial
[opensuse-packaging]:           https://en.opensuse.org/openSUSE:Packaging_guidelines
[fedora-packagers-guide]:       https://docs.fedoraproject.org/en-US/Fedora_Draft_Documentation/0.1/html/Packagers_Guide/index.html
[maximum-rpm]:                  http://ftp.rpm.org/max-rpm/
