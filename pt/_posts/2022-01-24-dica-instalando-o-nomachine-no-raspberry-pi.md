---
date: '2022-01-24 11:30:00 GMT-3'
image: '/files/2022/01/nomachine-rpi.jpg'
layout: post
nickname: 'nomachine-rpi'
title: 'Dica: instalando o NoMachine no Raspberry Pi'
---

{% include image.html src='/files/2022/01/nomachine-rpi.jpg' %}

Se você instalou o [openSUSE no Raspberry Pi][opensuse-rpi4] e pretende usá-lo como servidor, instalar também o [NoMachine] pode ser útil para fornecer acesso remoto à área de trabalho. Se você pretende usar o Raspberry Pi como _desktop_, o NoMachine também pode ser útil para acessar a área de trabalho de algum servidor com o NoMachine instalado.&nbsp;

[opensuse-rpi4]: {% post_url pt/2020-10-07-opensuse-no-raspberry-pi-4-parte-1-download-e-instalacao %}
[NoMachine]: {% post_url pt/2020-03-23-acesso-remoto-com-nomachine %}

Instalar o NoMachine no Raspberry Pi é tão fácil quanto instalar no _desktop_ ou servidor com arquitetura IBM PC de 64 _bits_. Você só precisa se atentar que, no caso do Raspberry Pi 4, a arquitetura é diferente (ARMv8 de 64 _bits_). Por isso, o pacote RPM que deve ser baixado estará em um local um pouco diferente. E é para mostrar isso que fiz essa dica.

<!--more-->

Comece seguindo o tutorial do NoMachine normalmente até chegar na seção **Download**:

- [Acesso remoto com NoMachine][NoMachine]

Chegando nessa seção, volte para essa página e siga as instruções a seguir.

## Download

Para baixar o NoMachine, acesse seu _site_ em [nomachine.com](https://www.nomachine.com/pt-pt/). Como você está usando o Raspberry Pi, clique em **Outros sistemas operativos** (_Other operating systems_):

{% include image.html src='/files/2022/01/nomachine-rpi-01.jpg' %}

Dentre as opções de _download_, clique no _link_ de **Download** referente ao **NoMachine para Raspberry Pi** (_NoMachine for Raspberry Pi_):

{% include image.html src='/files/2022/01/nomachine-rpi-02.jpg' %}

Na página seguinte, escolha o pacote RPM para ARMv8 na seção do Raspberry Pi 4:

{% include image.html src='/files/2022/01/nomachine-rpi-03.jpg' %}

Na página seguinte, clique em **Download**:

{% include image.html src='/files/2022/01/nomachine-rpi-04.jpg' %}

Se seu navegador perguntar o que fazer com o arquivo (abrir ou salvar), escolha **Abrir com** (_Open with_) o [YaST]:

[YaST]: http://yast.opensuse.org/

{% include image.html src='/files/2022/01/nomachine-rpi-05.jpg' %}

## Instalação

Quando o _download_ terminar, forneça a senha do administrador (usuário _root_) para proceder à instalação:

{% include image.html src='/files/2022/01/nomachine-rpi-06.jpg' %}

A instalação é semelhante, clique em **Aceitar** (_Accept_) para começar:

{% include image.html src='/files/2022/01/nomachine-rpi-07.jpg' %}

Você pode voltar a seguir aquele tutorial a partir da seção de **[Instalação]**.

[Instalação]: {% post_url pt/2020-03-23-acesso-remoto-com-nomachine %}#instalação

## Verificando o endereço IP do servidor

Perceba que o NoMachine adicionou um ícone na bandeja de estado (_status tray_) do [XFCE], no canto inferior direito da tela. Ele já está rodando e pronto para ser usado:

[XFCE]: https://www.xfce.org/

{% include image.html src='/files/2022/01/nomachine-rpi-08.jpg' %}
