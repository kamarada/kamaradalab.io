---
date: '2024-07-19 10:00:00 GMT-3'
image: '/files/2024/07/vlc-remote-pt.jpg'
layout: post
nickname: 'vlc-remote'
title: 'Use seu celular como controle remoto do VLC'
---

Você conectou o _notebook_ à TV, colocou um filme pra rodar, sentou no sofá e não quer ter que ficar levantando pra pausar o filme, aumentar o volume, etc? Já pensou que legal seria usar seu celular como controle remoto? Pois saiba que se você usa o [**reprodutor de mídias VLC**][vlc] isso é possível! Veja a seguir como.

{% include image.html src='/files/2024/07/vlc-remote-pt.jpg' %}

Eu uso e recomendo o VLC porque o considero um "canivete suíço" multimídia. Não apenas ele suporta praticamente todos os formatos de mídia existentes -- o que já seria bom o bastante -- como também ele possui muitas funcionalidades, algumas das quais seus usuários talvez nem imaginem que ele tem, a exemplo desta: é possível [controlar o VLC remotamente][vlc-control] por meio de [aplicativos para Android][vlc-android] ou [iPhone][vlc-iphone]. Eu testei alguns desses e indico o **[VLC Mobile Remote]**.

Como referência para este tutorial, vou usar um _notebook_ com [Linux], mais especificamente a distribuição [Linux Kamarada 15.5] (na qual o VLC já vem instalado por padrão), e um _smartphone_ com [Android 14]. Mas note que o VLC está disponível também para outros sistemas, a exemplo do [Windows][vlc] e do [macOS]. O VLC Mobile Remote também pode ser instalado no [iPhone][vlcmobileremote-iphone].

## Instalando o aplicativo no celular

Comece instalando o aplicativo VLC Mobile Remote no seu celular:

<div class="row">
    <div class="col-md">
        <div class="image no-ads-here text-center mb-3">
            <a href="https://play.google.com/store/apps/details?id=adarshurs.android.vlcmobileremote" title="">
                <img src="/assets/img/google-play-pt.png" alt="" class="img-fluid" style="width: 200px;">
            </a>
        </div>
    </div>
    <div class="col-md">
        <div class="image no-ads-here text-center mb-3">
            <a href="https://apps.apple.com/br/app/remote-for-vlc-pc-mac/id1140931401?ls=1" title="">
                <img src="/assets/img/app-store-pt.svg" alt="" class="img-fluid" style="width: 200px;">
            </a>
        </div>
    </div>
</div>

Quando terminar de baixar e instalar o aplicativo, abra-o.

No primeiro uso, ele já pergunta qual é o sistema operacional do computador:

{% include image.html src='/files/2024/07/vlcmobileremote-01-pt.png' %}

Escolha o sistema operacional do seu computador (no meu caso, **Linux**).

Nas telas seguintes, ele vai apresentar um tutorial de como configurar o VLC no computador, mas você já está acompanhando este tutorial. Passe as telas, deslizando da direita pra esquerda, até chegar na que tem o botão **Conexão Automática**:

<div class="row">
    <div class="col-md">
        {% include image.html src='/files/2024/07/vlcmobileremote-02-pt.jpg' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2024/07/vlcmobileremote-03-pt.jpg' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2024/07/vlcmobileremote-04-pt.jpg' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2024/07/vlcmobileremote-05-pt.jpg' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2024/07/vlcmobileremote-06-pt.jpg' %}
    </div>
</div>

Por enquanto, deixemos o celular assim e vamos para o computador.

## Ativando o controle remoto no VLC

No computador, abra o VLC. Vá em **Ferramentas > Preferências**:

{% include image.html src='/files/2024/07/vlcmobileremote-07-pt.jpg' %}

As preferências do VLC são abertas no modo **Simplificado**. No canto inferior esquerdo da tela, mude para **Detalhado**:

{% include image.html src='/files/2024/07/vlcmobileremote-08-pt.jpg' %}

Nas seções à esquerda, selecione **Interface > Interfaces principais**. À direita, em **Módulos adicionais de interface**, ative o módulo **Web**:

{% include image.html src='/files/2024/07/vlcmobileremote-09-pt.jpg' %}

Nas seções à esquerda, expanda **Interfaces principais** e selecione **Lua**. À direita, em **HTTP Lua**, defina uma **Senha** (recomendo `1234`, para que a conexão automática do aplicativo funcione) e, por fim, clique em **Salvar**:

{% include image.html src='/files/2024/07/vlcmobileremote-10-pt.jpg' %}

Agora feche o VLC e abra-o novamente. Você já pode até abrir o vídeo que deseja reproduzir.

## Finalizando a configuração no celular

Certifique-se de que o computador e o celular estão conectados à mesma rede Wi-Fi.

No celular, toque no botão **Conexão Automática**.

O aplicativo VLC Mobile Remote passa a exibir os controles do VLC do computador. Opcionalmente, você pode **Permitir** que ele exiba notificações:

{% include image.html src='/files/2024/07/vlcmobileremote-11-pt.jpg' %}

## Faça bom proveito!

Agora você já pode controlar o VLC pelo seu celular, como na imagem que abre este tutorial. Você não precisa fazer essas configurações de novo. Sempre que quiser usar seu celular como controle do VLC: conecte o computador e o celular à mesma rede Wi-Fi, abra o VLC no computador e o VLC Mobile Remote no celular (nessa ordem).

Divirta-se bastante! (_have a lot of fun!_)

A título de curiosidade, o filme que estou usando como exemplo se chama [Wing It!] e foi produzido em 2023 usando o [Blender], um [_software_ livre][free-sw] de computação gráfica 3D. É um filme de [código aberto] (você pode baixar os [arquivos que foram usados para produzi-lo][wing-it-production files]). Você também pode [baixar o filme][wing-it-download] pra assistir _offline_ (licença [CC BY 4.0]) ou simplesmente assisti-lo no [YouTube]:

{% include youtube.html id="u9lj-c29dxI" %}

Confira também outros [filmes do Blender Studio][blender-studio].

[vlc]:                      {% post_url pt/2019-07-05-18-aplicativos-que-voce-pode-usar-do-mesmo-jeito-no-linux-e-no-windows-parte-1 %}#7-vlc
[vlc-control]:              https://wiki.videolan.org/Category:Control_VLC/
[vlc-android]:              https://wiki.videolan.org/Control_VLC_from_an_Android_Phone/
[vlc-iphone]:               https://wiki.videolan.org/Control_VLC_from_an_iPhone/
[VLC Mobile Remote]:        https://vlcmobileremote.com/
[Linux]:                    http://www.vivaolinux.com.br/linux/
[Linux Kamarada 15.5]:      {% post_url pt/2024-05-27-linux-kamarada-15-5-mais-alinhado-com-o-opensuse-leap-e-com-outras-distribuicoes %}
[Android 14]:               https://www.android.com/intl/pt_br/android-14/
[macOS]:                    https://www.videolan.org/vlc/download-macosx.html
[vlcmobileremote-iphone]:   https://apps.apple.com/us/app/remote-for-vlc-pc-mac/id1140931401?ls=1
[Wing It!]:                 https://studio.blender.org/films/wing-it/
[Blender]:                  https://www.blender.org/
[free-sw]:                  https://www.gnu.org/philosophy/free-sw.pt-br.html
[código aberto]:            https://pt.wikipedia.org/wiki/Software_de_c%C3%B3digo_aberto
[wing-it-production files]: https://studio.blender.org/films/wing-it/3c308f54ee719e/?asset=6966
[wing-it-download]:         https://studio.blender.org/films/wing-it/3c402f7c9ab362/?asset=7023
[CC BY 4.0]:                https://creativecommons.org/licenses/by/4.0/
[YouTube]:                  https://www.youtube.com/watch?v=u9lj-c29dxI
[blender-studio]:           https://studio.blender.org/films/
