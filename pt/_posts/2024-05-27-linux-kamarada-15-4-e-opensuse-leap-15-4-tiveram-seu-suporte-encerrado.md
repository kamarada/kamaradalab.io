---
date: '2024-05-27 23:58:00 GMT-3'
image: '/files/2024/05/kamarada-15.4-eol.jpg'
layout: post
nickname: 'kamarada-15.4-eol'
title: 'Linux Kamarada 15.4 e openSUSE Leap 15.4 tiveram seu suporte encerrado'
excerpt: 'O suporte do openSUSE Leap 15.4, lançado em 08 de junho de 2022, foi encerrado em 31 de dezembro de 2023, totalizando 18 meses de correções de bugs de funcionalidades e falhas de segurança. O Projeto openSUSE recomenda que usuários do openSUSE Leap 15.4 atualizem para o openSUSE Leap 15.5, lançado em 07 de junho de 2023 e que deve receber atualizações até o fim de dezembro de 2024, de acordo com a wiki do openSUSE.'
---

{% include image.html src='/files/2024/05/kamarada-15.4-eol.jpg' %}

## openSUSE Leap 15.4

O suporte do [openSUSE Leap 15.4][leap-15.4], lançado em 08 de junho de 2022, foi encerrado em 31 de dezembro de 2023, totalizando 18 meses de correções de _bugs_ de funcionalidades e falhas de segurança.

O [Projeto openSUSE][opensuse] recomenda que usuários do openSUSE Leap 15.4 atualizem para o [openSUSE Leap 15.5][leap-15.5], lançado em 07 de junho de 2023 e que deve receber atualizações até o fim de dezembro de 2024, de acordo com a [_wiki_ do openSUSE][lifetime].

Até lá, muito provavelmente já teremos o openSUSE Leap 15.6, cujo lançamento está [previsto][leap-15.6-roadmap] para 12 de junho de 2024. Portanto, quando o Leap 15.6 for lançado, os usuários do Leap 15.5 terão aproximadamente 6 meses para fazer a atualização da distribuição.

## Linux Kamarada 15.4

Como o Linux Kamarada é baseado no [openSUSE Leap][leap], usuários do Linux Kamarada são, por tabela, usuários do openSUSE Leap também e recebem essas mesmas atualizações.

O fim do suporte de todas as versões do Linux Kamarada normalmente deve ocorrer junto do fim do suporte da versão equivalente do openSUSE Leap.

Como o [Linux Kamarada 15.5][kamarada-15.5] foi lançado apenas hoje, o suporte do [Linux Kamarada 15.4][kamarada-15.4], lançado em 27 de fevereiro de 2023, foi estendido até hoje, mas se encerra hoje, 27 de maio de 2024.

Se você usa o Linux Kamarada 15.4, eu recomendo que atualize para o [Linux Kamarada 15.5][kamarada-15.5], que deve receber atualizações pelo mesmo período que o openSUSE Leap 15.5, ou seja, até dezembro de 2024.

## Como atualizar

Se você usa o openSUSE Leap (ou o Linux Kamarada) 15.4, veja como atualizar para o 15.5 em:

- [Linux Kamarada e openSUSE Leap: como atualizar da versão 15.4 para a 15.5][upgrade]

As instruções no tutorial acima se aplicam a ambas as distribuições.

Se tiver alguma dúvida, comente neste texto ou no tutorial acima. Você também pode conferir outras opções na página [Ajuda].

[leap-15.4]:            {% post_url pt/2022-06-08-leap-15-4-oferece-novos-recursos-com-a-estabilidade-de-sempre %}
[opensuse]:             https://www.opensuse.org/
[leap-15.5]:            {% post_url pt/2023-06-07-lancamento-do-opensuse-leap-15-5-amadurece-a-distribuicao-e-estabelece-transicao-tecnologica %}
[lifetime]:             https://en.opensuse.org/Lifetime
[leap-15.6-roadmap]:    https://en.opensuse.org/openSUSE:Roadmap#Schedule_for_openSUSE_Leap_15.6
[leap]:                 {% post_url pt/2020-12-06-opensuse-leap-e-opensuse-tumbleweed-qual-e-a-diferenca %}
[kamarada-15.5]:        {% post_url pt/2024-05-27-linux-kamarada-15-5-mais-alinhado-com-o-opensuse-leap-e-com-outras-distribuicoes %}
[kamarada-15.4]:        {% post_url pt/2023-02-27-linux-kamarada-15-4-mais-funcional-bonito-polido-e-elegante-do-que-nunca %}
[upgrade]:              {% post_url pt/2023-11-26-linux-kamarada-15-4-e-opensuse-leap-15-4-como-atualizar-para-a-versao-15-5 %}
[Ajuda]:                /pt/ajuda
