---
date: '2021-06-02 21:20:00 GMT-3'
image: '/files/2021/05/im-wire-pt.jpg'
layout: post
nickname: 'wire'
title: 'O mensageiro Wire'
---

{% include image.html src='/files/2021/05/im-wire-pt.jpg' %}

O [Wire] é um [aplicativo de mensagens] seguro e focado em privacidade, totalmente criptografado de ponta a ponta. Ele é diferente dos mensageiros mostrados aqui antes porque, na verdade, é uma suíte de colaboração corporativa. Assim como outros mensageiros, o Wire permite enviar mensagens de texto, de voz e de vídeo, fotos, vídeos e arquivos e também fazer chamadas de voz e de vídeo. Mas o Wire também tem recursos para empresas, como [compartilhamento de tela][screen sharing], [salas de reuniões com convidados][guest rooms], gerenciamento de permissões, chamadas de voz em grupo (com até 25 participantes), videoconferência (com até 12 participantes) e conformidade com regulamentações (GDPR e CCPA) e certificações (ISO e SOX).

Assim como acontece com o [Element], Wire é o nome tanto do aplicativo quanto da empresa por trás dele.

A Wire vende planos para empresas: [Pro], [Enterprise] e [Red]. Tem também o [Wire Personal][personal] para uso pessoal, que é mais simples, mas totalmente gratuito. Curioso que é difícil achar um _link_ para o Wire Personal no [_site_ da Wire][wire], mas se você pesquisar (usando, por exemplo, o [DuckDuckGo] ou o [Google]) ou [clicar aqui][personal], verá que ele está lá. Ou você pode simplesmente [baixar o Wire][download]. Todas essas opções usam o mesmo aplicativo, a diferença fica por conta dos recursos disponíveis e do preço.

O Wire Personal não exibe anúncios e não traça perfis dos usuários: não vende relatórios analíticos nem de uso do aplicativo para empresas de publicidade ou qualquer outra pessoa. E não precisa fazer isso graças à renda obtida com as versões corporativas.

Assim como o [WhatsApp], [Telegram] e [Signal], e diferente de outros mensageiros mostrados aqui antes, o Wire possui servidores centralizados (embora permita a você [hospedar seu próprio servidor][self-hosting] e ofereça uma [extensa documentação][self-hosting] sobre isso). O benefício disso na prática é permitir que o Wire seja usado em vários dispositivos, como o Telegram. Mas, diferente do Telegram, o Wire é totalmente criptografado de ponta a ponta: nenhuma mensagem ou arquivo é armazenado nos servidores sem criptografia. O Wire permite que uma conta seja sincronizada em até 8 dispositivos e as mensagens são criptografadas para cada um deles.

Você precisa informar um endereço de _e-mail_ ou número de telefone para criar uma conta. De fato, [o Wire registra alguns metadados dos usuários][metadados].

Como outros mensageiros, o Wire permite que você envie [mensagens e arquivos efêmeros][ephemeral]. Além disso, o aplicativo para dispositivos móveis oferece a possibilidade de bloquear o aplicativo. Se você ativar esse recurso, para desbloquear o aplicativo deve usar a mesma senha, padrão ou impressão digital que normalmente usa para desbloquear o dispositivo.

O Wire oferece um recurso de _[backup]_ que facilita você fazer _backup_ das conversas.

Quanto à empresa Wire, [ela foi fundada por algumas das mesmas pessoas que fundaram o Skype][skype] e hoje tem mais de 1300 clientes corporativos. Quanto à sua localização, as informações presentes no _site_ são um pouco confusas: [uma página][location-1] diz que a sede da empresa fica na Suíça, enquanto [outra página][location-2] diz que está sediada na Alemanha, com alguns escritórios pelo mundo. Talvez eles possam nos esclarecer isso e atualizar o _site_ também. Seus [termos de uso] dizem que a empresa segue as leis dos Estados Unidos e do estado da Califórnia (para usuários nos EUA) e as leis da Suíça (para usuários fora dos EUA).

O Wire é totalmente código-aberto: os códigos-fonte de todos os clientes e do servidor estão disponíveis no [GitHub], permitindo uma auditoria independente.

O Wire usa o protocolo Proteus para realizar a criptografia de ponta a ponta das mensagens. O protocolo Proteus é um _fork_ do código que viria a se tornar o protocolo do Signal, _fork_ esse feito no início do desenvolvimento. O [protocolo Proteus][audit-1] e [todos os clientes Wire][audit-2] foram auditados publicamente pela Kudelski Security em conjunto com a X41 D-Sec GmbH.

Existe outro protocolo em desenvolvimento para criptografar as mensagens em grupo, chamado de [Messaging Layer Security][mls] (MLS). Está sendo desenvolvido por um grupo de trabalho da IETF iniciado pela Wire em 2016, junto com a Mozilla e a Cisco. Desde então, se juntaram grupo de trabalho: Universidade de Oxford, Facebook, INRIA, Google e Twitter.

Por último, mas não menos importante, o Wire possui uma interface bem bonita e distinta.

O Wire já foi recomendado uma vez pelo especialista em privacidade [Edward Snowden]:

<blockquote class="twitter-tweet" data-conversation="none" data-lang="pt"><p lang="en" dir="ltr">I would not (and do not) use email, except as throwaways for registration. Email is a fundamentally insecure protocol that, in 2019, can and should be abandoned for the purposes of any meaningful communication. Email is unsafe. I&#39;d use <a href="https://twitter.com/signalapp?ref_src=twsrc%5Etfw">@Signalapp</a> or <a href="https://twitter.com/wire?ref_src=twsrc%5Etfw">@Wire</a> as a safer alternative.</p>&mdash; Edward Snowden (@Snowden) <a href="https://twitter.com/Snowden/status/1175437588129308672?ref_src=twsrc%5Etfw">21 de setembro de 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Mas alguns meses depois, ele mostrou preocupação com a aquisição da empresa suíça Wire por uma _holding_ norte-americana:

<blockquote class="twitter-tweet" data-lang="pt"><p lang="en" dir="ltr">If you&#39;re a tech journalist, you should be digging into the story behind what&#39;s going on behind the curtain here. This is not appropriate for a company claiming to provide a secure messenger -- claims a large number of human rights defenders relied on -- and we need facts. <a href="https://t.co/iV4tRZwgDR">https://t.co/iV4tRZwgDR</a></p>&mdash; Edward Snowden (@Snowden) <a href="https://twitter.com/Snowden/status/1194396764293550080?ref_src=twsrc%5Etfw">12 de novembro de 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" data-lang="pt"><p lang="en" dir="ltr">“Wire was always for profit and planned to follow the typical venture backed route.&quot; [<a href="https://twitter.com/wire?ref_src=twsrc%5Etfw">@Wire</a> CEO] Brogger... describes individual consumers as “not part of our strategy.&quot;<br><br>This is a grim turn for a once-promising app, and a window for <a href="https://twitter.com/signalapp?ref_src=twsrc%5Etfw">@Signalapp</a> to exploit. <a href="https://t.co/HWuqwc9GIp">https://t.co/HWuqwc9GIp</a></p>&mdash; Edward Snowden (@Snowden) <a href="https://twitter.com/Snowden/status/1194805615023050752?ref_src=twsrc%5Etfw">14 de novembro de 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

De qualquer forma, se você procura um aplicativo para substituir o WhatsApp, pode ser que o Wire Personal seja uma opção. Ou, se você gerencia uma empresa que precisa de conformidade com regulamentações e certificações, pode contratar os serviços da Wire.

No Android, o Wire pode ser obtido do [Google Play] ou do [próprio _site_][download] (como APK).

O _app_ do Wire para Linux em formato [AppImage] pode ser obtido do [_site_ do Wire][download], que também tem pacotes para Ubuntu e Debian. Eu achei mais fácil instalar a partir do [Flathub]:

```
# flatpak install com.wire.WireDesktop
```

Também existem versões do Wire para outros sistemas. Veja mais em: [wire.com][download].

O _site_ do Wire possui uma abrangente seção de [suporte] com muitos artigos que podem ser pesquisados. O Wire também tem uma [extensa documentação sobre o servidor][self-hosting] para administradores de sistemas que querem rodar seu próprio servidor do Wire em _datacenters_ próprios ou na nuvem, ou mesmo pessoas curiosas em entender como o servidor funciona.

Esse texto faz parte da série:

- [Aplicativos de mensagens livres e focados em privacidade alternativos ao WhatsApp][aplicativo de mensagens]

[wire]:                     https://wire.com/en/
[aplicativo de mensagens]:  {% post_url pt/2021-05-08-aplicativos-de-mensagens-livres-e-focados-em-privacidade-alternativos-ao-whatsapp %}
[screen sharing]:           https://wire.com/en/blog/encrypted-screen-sharing/
[guest rooms]:              https://wire.com/en/features/encrypted-guest-rooms/
[element]:                  {% post_url pt/2021-05-21-o-mensageiro-element-e-o-padrao-matrix %}
[pro]:                      https://wire.com/en/products/pro-secure-team-collaboration/
[enterprise]:               https://wire.com/en/products/technology/
[red]:                      https://wire.com/en/products/red-crisis-communication-software/
[personal]:                 https://wire.com/en/products/personal-secure-messenger/
[duckduckgo]:               https://duckduckgo.com/?q=wire+personal
[google]:                   https://www.google.com/search?q=wire+personal
[download]:                 https://wire.com/en/download/
[whatsapp]:                 https://www.whatsapp.com/
[telegram]:                 {% post_url pt/2021-05-09-o-mensageiro-telegram %}
[signal]:                   {% post_url pt/2021-05-11-o-mensageiro-signal %}
[self-hosting]:             https://docs.wire.com/
[metadados]:                https://wire-docs.wire.com/download/Wire+Privacy+Whitepaper.pdf
[ephemeral]:                https://wire.com/en/blog/ephemeral-messaging-workplace/
[backup]:                   https://support.wire.com/hc/en-us/articles/360000824805-Back-up-your-conversation-history
[skype]:                    https://canaltech.com.br/apps/Criador-do-Skype-lanca-novo-app-de-chat-chamado-Wire/
[location-1]:               https://wire.com/en/security/
[location-2]:               https://wire.com/en/about/
[termos de uso]:            https://wire.com/en/legal/#terms
[github]:                   https://github.com/wireapp
[audit-1]:                  https://wireapp.medium.com/wires-independent-security-review-61f37a1762a8
[audit-2]:                  https://wireapp.medium.com/wire-application-level-security-audits-98324d1f211b
[mls]:                      https://messaginglayersecurity.rocks/
[edward snowden]:           https://pt.wikipedia.org/wiki/Edward_Snowden
[google play]:              https://play.google.com/store/apps/details?id=com.wire
[appimage]:                 https://appimage.org/
[flathub]:                  https://flathub.org/apps/details/com.wire.WireDesktop
[suporte]:                  https://support.wire.com/hc/en-us
