---
date: '2024-12-18 15:00:00 GMT-3'
image: '/files/2024/12/15.6-beta.jpg'
layout: post
nickname: 'kamarada-15.6-beta'
title: 'Linux Kamarada 15.6 Beta: ajude a testar a melhor versão da distribuição'
---

{% include image.html src='/files/2024/12/15.6-beta.jpg' %}

O Projeto Linux Kamarada anuncia o lançamento da versão 15.6 Beta da [distribuição Linux][linux] de mesmo nome, baseada no [openSUSE Leap 15.6][leap-15.6]. Ela já está disponível para _[download]_.

<!--more-->

A página [Download] foi atualizada e agora oferece principalmente duas versões para _download_:

- a versão 15.5 Final, que você pode instalar no computador de casa ou do trabalho; e
- a versão 15.6 Beta, que você pode testar, se quiser ajudar no desenvolvimento.

O openSUSE Leap 15.6 foi lançado em [junho][leap-15.6] e desde então já recebeu diversas atualizações e correções. Portanto, o Linux Kamarada 15.6, embora ainda esteja em fase [beta], deve apresentar desde já uma estabilidade considerável. Ainda assim, pode ser que essa versão tenha _bugs_ e não esteja pronta para o uso diário. Se você encontrar um _bug_, por favor, reporte. Veja formas de entrar em contato na página [Ajuda]. Claro, _bugs_ podem ser encontrados e corrigidos a qualquer momento, mas quanto antes, melhor!

A forma mais fácil de testar o Linux é usando o [VirtualBox]. Para mais informações, leia:

- [VirtualBox: a forma mais fácil de conhecer o Linux sem precisar instalá-lo][VirtualBox]

Mas, se você quiser testar o Linux Kamarada no seu próprio computador, a forma mais fácil de fazer isso é usando um _pendrive_ inicializável que você pode criar com o [Ventoy]:

- [Ventoy: crie um pendrive multiboot simplesmente copiando imagens ISO para ele][Ventoy]

Se você deseja instalar o Linux Kamarada no computador para usar no dia-a-dia, o recomendado é que você instale a versão 15.5 Final, que já está pronta, e depois, quando a versão 15.6 estiver pronta, atualize. Para mais informações sobre a versão 15.5 Final, leia:

- [Linux Kamarada 15.5: mais alinhado com o openSUSE Leap e com outras distribuições][kamarada-15.5]

## Novidades

As novidades da versão 15.6 se resumem a:

- os aplicativos da versão 15.5 foram mantidos em sua maioria, porém atualizados, em novas versões, com novas funcionalidades e _bugs_ corrigidos;

- a atualização mais notável foi da área de trabalho [GNOME], que no [Linux Kamarada 15.4][kamarada-15.4] e [15.5][kamarada-15.5] estava na versão 41, e agora está na versão 45;

- a extensão do GNOME [GSConnect] agora vem instalada e habilitada por padrão, ela é uma implementação do protocolo [KDE Connect] para a área de trabalho GNOME, que permite integrar o celular ao computador para exibir notificações do celular no computador e vice-versa, compartilhar arquivos e _links_, copiar e colar entre os dispositivos, entre várias outras possibilidades;

- agora você pode mudar para um tema escuro, em vez do tema padrão claro;

- e, como de costume, novos belos papéis de parede (ainda é possível usar os antigos ou o padrão do openSUSE, se você preferir).

## Atualizando para o 15.6 Beta

Se você já usa o Linux Kamarada 15.5 e confia na sua experiência como usuário, pode querer atualizar para o Linux Kamarada 15.6 Beta para ajudar a testar o processo de atualização. Em linhas gerais, deve ser suficiente seguir esse tutorial de atualização, substituindo `15.5` por `15.6`, onde houver:

- [Linux Kamarada e openSUSE Leap: como atualizar da versão 15.4 para a 15.5][upgrade-howto]

Como o Linux Kamarada 15.5 já configurava os repositórios com a variável `$releasever`, deve ser bem fácil atualizar para a versão 15.6. Exemplos de comandos do tutorial acima, já adaptados para a nova versão:

```
# zypper --releasever=15.6 ref
# zypper --releasever=15.6 dup --download-only --allow-vendor-change
```

Se você não sentir segurança de atualizar para a versão 15.6 Beta com essas dicas, não tem problema. Em breve, devo estar publicando uma versão atualizada do tutorial de atualização.

## Ficha técnica

Para que seja possível comparar esta versão com as anteriores e as futuras, segue um resumo do _software_ contido no Linux Kamarada 15.6 Beta Build 4.2:

- _kernel_ Linux 6.4.0
- servidor gráfico X.Org 21.1.11 (com Wayland 1.22.0 habilitado por padrão)
- área de trabalho GNOME 45.3 acompanhada de seus principais aplicativos (_core apps_), como Arquivos (antigo Nautilus), Calculadora, Terminal, Editor de texto (gedit), Captura de tela e outros
- suíte de aplicativos de escritório LibreOffice 24.8.1.2
- navegador Mozilla Firefox 128.5.1 ESR (navegador padro)
- navegador Chromium 131 (navegador alternativo disponível)
- reprodutor multimídia VLC 3.0.21
- cliente de _e-mail_ Evolution 3.50.3
- centro de controle do YaST
- Brasero 3.12.3
- CUPS 2.2.7
- Drawing 1.0.2
- Firewalld 2.0.1
- GParted 1.5.0
- HPLIP 3.23.8
- Java (OpenJDK) 11.0.25
- KeePassXC 2.7.9
- Linphone 5.0.16
- PDFsam Basic 5.2.9
- Pidgin 2.14.8
- Python 3.6.15 e 3.11.10
- Samba 4.19.8
- Tor 0.4.8.11
- Transmission 4.0.5
- Vim 9.1.0836
- Wine 9.0
- instalador Calamares 3.2.62
- Flatpak 1.14.6
- jogos: AisleRiot (Paciência), Mahjongg, Minas (Campo minado), Nibbles (Cobras), Quadrapassel (Tetris), Reversi, Sudoku, Xadrez

Essa lista não é exaustiva, mas já dá uma noção do que se encontra na distribuição.

[linux]:            https://www.vivaolinux.com.br/linux/
[leap-15.6]:        {% post_url pt/2024-06-12-leap-15-6-revela-opcoes-para-usuarios %}
[download]:         /pt/download
[beta]:             https://pt.wikipedia.org/wiki/Ciclo_de_vida_de_liberação_de_software#Beta
[Ajuda]:            /pt/ajuda
[VirtualBox]:       {% post_url pt/2019-10-08-virtualbox-a-forma-mais-facil-de-conhecer-o-linux-sem-precisar-instala-lo %}
[Ventoy]:           {% post_url pt/2020-07-29-ventoy-crie-pendrives-multiboot-simplesmente-copiando-imagens-iso-para-ele %}
[kamarada-15.5]:    {% post_url pt/2024-05-27-linux-kamarada-15-5-mais-alinhado-com-o-opensuse-leap-e-com-outras-distribuicoes %}
[GNOME]:            https://br.gnome.org/
[kamarada-15.4]:    {% post_url pt/2023-02-27-linux-kamarada-15-4-mais-funcional-bonito-polido-e-elegante-do-que-nunca %}
[GSConnect]:        https://extensions.gnome.org/extension/1319/gsconnect/
[KDE Connect]:      https://userbase.kde.org/KDEConnect
[upgrade-howto]:    {% post_url pt/2023-11-26-linux-kamarada-15-4-e-opensuse-leap-15-4-como-atualizar-para-a-versao-15-5 %}
