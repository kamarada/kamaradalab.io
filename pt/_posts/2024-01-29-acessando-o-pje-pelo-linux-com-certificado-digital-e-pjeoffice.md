---
date: '2024-01-29 21:20:00 GMT-3'
layout: post
published: true
title: 'Acessando o PJe pelo Linux com certificado digital e PJeOffice'
image: /files/2024/01/pje-office.jpg
nickname: 'pjeoffice'
---

{% include image.html src='/files/2024/01/pje-office.jpg' %}

Se você é advogado ou juiz, provavelmente já conhece o sistema de [**Processo Judicial Eletrônico (PJe)**][PJe], que é desenvolvido pelo [Conselho Nacional de Justiça (CNJ)][CNJ] em parceria com diversos tribunais. Há vários [tribunais no Brasil todo][pje-navegador] que usam esse sistema para tramitar os processos judiciais.&nbsp;

Embora o PJe seja um sistema _web_ e para usá-lo você precisa, a princípio, apenas do navegador, para usufruir de todas as funcionalidades do sistema pode ser necessário instalar programas adicionais, como o **[PJeOffice]** para entrar no sistema usando [certificado digital][how-to-token] e assinar documentos eletronicamente.

Há vários tutoriais na Internet, feitos tanto por tribunais quanto por diversas pessoas, mostrando como fazer o PJeOffice funcionar no [Windows]. Quem usa [Linux] acaba recebendo menos atenção nesse sentido e, não à toa, surgiu a dúvida no [grupo de usuários do Linux Kamarada][grupo] no [Telegram].

Como eu consegui fazer o PJeOffice funcionar no Linux, resolvi responder a essa pergunta escrevendo esse tutorial, mostrando como você pode fazer o mesmo.

Como referência, vou usar a distribuição [Linux Kamarada 15.5]. Se você usa outra distribuição Linux, o procedimento para fazer o PJeOffice funcionar nela pode ser parecido. Em caso de dúvida, consulte a documentação e/ou o suporte da sua distribuição.

## Pré-requisitos

Antes de usar o PJeOffice, você deve se certificar de que seu sistema atenda a alguns pré-requisitos:

- O certificado da autoridade certificadora raiz (no caso do Brasil, a [ICP-Brasil]) deve estar instalado. O Linux Kamarada já traz esse certificado instalado "de fábrica". Se você usa outra distribuição Linux, consulte instruções de como instalá-lo [nesse tutorial][how-to-install-certificates];
- Seu certificado digital precisa estar configurado e você deve ser capaz de usá-lo com seu navegador preferido para acessar sistemas do governo. Consulte instruções de como conseguir isso [nesse tutorial][how-to-token] (se você usa o [Google Chrome], leia [esse tutorial][chrome-token] **também**);
- O PJeOffice é feito em [Java]. Isso significa que você precisa ter instalado no seu sistema o ambiente de execução do Java (do inglês _Java Runtime Environment_, JRE). O Linux Kamarada já vem com o Java instalado também. Se você usa o [openSUSE Leap ou Tumbleweed][leap-vs-tumbleweed], execute o comando a seguir (como usuário _root_) para instalar o Java:

```
# zypper in java
```

## Baixando o PJeOffice

Se você [pesquisar PJeOffice no Google][pjeoffice-google], o primeiro resultado é essa página da _wiki_ oficial do PJe:

- <https://www.pje.jus.br/wiki/index.php/PJeOffice>

Na seção **Aplicativo PJeOffice para instalação**, clique no _link_ para baixar a versão para **Unix**:

{% include image.html src='/files/2024/01/pje-office-01.jpg' %}

O navegador deve baixar o arquivo compactado para a pasta **Downloads**. Abra essa pasta, clique com o botão direito do _mouse_ no arquivo compactado e clique na opção **Extrair aqui**:

{% include image.html src='/files/2024/01/pje-office-02.jpg' %}

O conteúdo do arquivo compactado é extraído para uma pasta com o mesmo nome dele. Dentro desta, existe uma pasta chamada `pje-office`. Mova essa pasta para algum local que seja fácil de você lembrar.

Por exemplo, eu criei uma pasta `programas` dentro da minha pasta pessoal (`/home/vinicius/`) e movi a pasta `pje-office` para lá. Portanto, o caminho para o PJeOffice no meu computador ficou sendo:

`/home/vinicius/programas/pje-office`

## Iniciando o PJeOffice

Recomendo que você conecte seu _token_ ou _smart card_ ao computador antes de iniciar o PJeOffice.

Para iniciar o PJeOffice, vá até a pasta onde ele se encontra, clique com o botão direito do _mouse_ no _script_ `pjeOffice.sh` e clique na opção **Executar como programa**:

{% include image.html src='/files/2024/01/pje-office-03.jpg' %}

Note que é aberta uma janela de **Terminal** que exibe mensagens do PJeOffice (você pode minimizá-la), mas também o ícone do PJeOffice passa a aparecer junto de outros ícones na bandeja do sistema, no canto superior direito da tela:

{% include image.html src='/files/2024/01/pje-office-04.jpg' %}

## Configurando o PJeOffice no primeiro uso

Na primeira vez que você usa o PJeOffice, precisa configurar seu certificado digital. Para isso, clique com o botão direito do _mouse_ no ícone do PJeOffice na bandeja do sistema e clique na opção **Configuração de Certificado**:

{% include image.html src='/files/2024/01/pje-office-05.jpg' %}

Na janela que aparece, selecione a aba **Avançado**. Seu _token_ ou _smart card_ deve aparecer listado em **Providers** (provedores). Clique nele:

{% include image.html src='/files/2024/01/pje-office-06.jpg' %}

Digite a senha PIN do _token_ e clique em **OK**:

{% include image.html src='/files/2024/01/pje-office-07.jpg' %}

Feito isso, o seu certificado digital passa a aparecer na **Lista de certificados**:

{% include image.html src='/files/2024/01/pje-office-08.jpg' %}

E está pronto para ser usado com o PJeOffice.

## Usando o PJeOffice para acessar o PJe

Abra seu navegador preferido (como exemplo, vou usar o [Mozilla Firefox], mas também deve funcionar no Google Chrome ou em qualquer outro navegador) e acesse o PJe do tribunal desejado (como exemplo, vou usar o [PJe 1º grau do TRT4][trt4]). Na dúvida, acesse essa página do PJe e selecione o tribunal desejado:

- <https://www.pje.jus.br/navegador/>

Perceba que em **Modo de assinatura** está selecionado o **Shodō**. Clique nesse _link_ para mudar o aplicativo assinador:

{% include image.html src='/files/2024/01/pje-office-09.jpg' %}

Clique no botão do **PJeOffice**:

{% include image.html src='/files/2024/01/pje-office-10.jpg' %}

Perceba que agora em **Modo de assinatura** está selecionado o **PJeOffice**. Agora clique no botão **Certificado Digital**:

{% include image.html src='/files/2024/01/pje-office-11.jpg' %}

O PJeOffice pergunta se você deseja autorizar o acesso dessa instância do PJe. Para que ele autorize e não faça essa pergunta novamente, clique em **Sempre**:

{% include image.html src='/files/2024/01/pje-office-12.jpg' %}

Se é a primeira vez que você acessa o PJe, ele vai usar seu CPF, presente no certificado digital, para obter seus dados da Receita Federal e pedir que você faça seu cadastro:

{% include image.html src='/files/2024/01/pje-office-13.jpg' %}

Se você já possui cadastro no PJe, o sistema deve apresentar sua tela inicial.

Em ambos os casos, isso indica que você configurou corretamente seu certificado digital e o PJeOffice e a partir de agora pode usá-los sempre que quiser acessar o PJe.

## Tutoriais relacionados

Dentro do tema certificado digital, também pode ser do seu interesse os seguintes tutoriais:

- [Assinando documentos ODF e PDF com o LibreOffice][libreoffice-signing]
- [Assinando e-mails enviados com certificado digital no Evolution][evolution-token]
- [Assinando e-mails enviados com certificado digital no Thunderbird][how-to-thunderbird-token]

[PJe]:                          https://www.cnj.jus.br/programas-e-acoes/processo-judicial-eletronico-pje/
[CNJ]:                          https://www.cnj.jus.br/
[pje-navegador]:                https://www.pje.jus.br/navegador/
[PJeOffice]:                    https://www.pje.jus.br/wiki/index.php/PJeOffice
[how-to-token]:                 {% post_url pt/2018-04-16-configurando-certificado-digital-no-linux-opensuse %}
[Windows]:                      https://www.microsoft.com/pt-br/windows/
[Linux]:                        https://www.vivaolinux.com.br/linux/
[grupo]:                        https://t.me/LinuxKamarada/4102
[Telegram]:                     {% post_url pt/2021-05-09-o-mensageiro-telegram %}
[Linux Kamarada 15.5]:          {% post_url pt/2023-11-01-linux-kamarada-15-5-beta-ajude-a-testar-a-melhor-versao-da-distribuicao %}
[ICP-Brasil]:                   https://www.gov.br/iti/pt-br/assuntos/icp-brasil
[how-to-install-certificates]:  {% post_url pt/2017-08-29-como-instalar-certificados-de-seguranca-no-linux %}
[Google Chrome]:                https://www.google.com/chrome/
[chrome-token]:                 {% post_url pt/2019-09-26-configurando-certificado-digital-no-navegador-google-chrome-chromium %}
[Java]:                         {% post_url pt/2019-07-05-18-aplicativos-que-voce-pode-usar-do-mesmo-jeito-no-linux-e-no-windows-parte-1 %}#9-java
[leap-vs-tumbleweed]:           {% post_url pt/2020-12-06-opensuse-leap-e-opensuse-tumbleweed-qual-e-a-diferenca %}
[pjeoffice-google]:             https://www.google.com/search?q=pjeoffice
[Mozilla Firefox]:              https://www.mozilla.org/pt-BR/firefox/new/
[trt4]:                         https://www.trt4.jus.br/portais/trt4/pje#acesso-rapido
[libreoffice-signing]:          {% post_url pt/2018-12-16-assinando-documentos-odf-e-pdf-com-o-libreoffice %}
[evolution-token]:              {% post_url pt/2020-07-06-assinando-emails-enviados-com-certificado-digital-no-evolution %}
[how-to-thunderbird-token]:     {% post_url pt/2018-09-24-assinando-emails-enviados-com-certificado-digital-no-thunderbird %}
