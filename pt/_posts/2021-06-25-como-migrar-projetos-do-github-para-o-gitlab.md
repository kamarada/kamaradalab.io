---
date: 2021-06-25 11:35:00 GMT-3
image: /files/2021/03/moving-to-gitlab.jpg
layout: post
nickname: 'github-to-gitlab'
title: 'Como migrar projetos do GitHub para o GitLab'
---

{% include image.html src='/files/2021/03/moving-to-gitlab.jpg' %}

O texto de hoje é para os desenvolvedores e projetistas de _software_ livre. Se você acompanha meu trabalho, provavelmente já sabe que o [Linux Kamarada está migrando para o GitLab][moving-to-gitlab]. Se você também pensa em migrar seus repositórios de código-fonte do [GitHub] para o [GitLab], preparei o passo a passo a seguir mostrando como você pode fazer isso.&nbsp;

Esse tutorial segue a ideia de compartilhar os [bastidores do Linux Kamarada][making-of]. Sendo assim, como exemplo, mostrarei como migrei o repositório [Linux-Kamarada-GNOME][repo-github], que contém a _appliance_ do [Kiwi] usada para gerar a imagem ISO da distribuição.

## Como funciona

Você pode importar seus repositórios do GitHub para o [GitLab.com][GitLab] ou para seu próprio servidor do GitLab (_self-hosted_).

Praticamente tudo dos projetos é importado, como: descrição, dados do Git (_commits_, _branches_, _tags_, etc), _issues_ e _pull requests_ (e seus comentários), páginas da _wiki_ e [mais][gitlab-docs].

Observe que o que o GitHub chama de _pull requests_, o GitLab chama de _merge requests_.

Referências a _issues_ e _pull requests_ são preservadas. Ao importar _issues_ e _pull requests_, o importador tenta encontrar seus autores e _assignees_ (designados) do GitHub no GitLab. Para que essa associação funcione, cada autor e _assignee_ no repositório do GitHub deve atender a uma das seguintes condições antes da importação:

- Ter feito _login_ anteriormente em uma conta do GitLab usando o ícone do GitHub.
- Ter uma conta no GitHub com um [endereço de _e-mail_ visível ao público][github-docs-1] que corresponda ao endereço de _e-mail_ da sua conta no GitLab.

Se um usuário referenciado em uma _issue_ não for encontrado no GitLab, o criador do projeto (normalmente o usuário que iniciou o processo de importação) é referenciado em seu lugar, mas uma observação é adicionada à _issue_ informando o usuário original no GitHub.

Se quiser mais detalhes sobre o processo, consulte a [documentação do GitLab][gitlab-docs].

Agora, vamos ver como migrar projetos do GitHub para o GitLab na prática!

## Importando o repositório para o GitLab

Entre (faça _login_) na sua conta do [GitLab].

Em seguida, na barra de navegação ao topo, vá em **Menu** > **Projects** (projetos) > **Create new project** (criar novo projeto):

{% include image.html src='/files/2021/06/github-to-gitlab-01.jpg' %}

Na tela seguinte, escolha **Import project/repository** (importar projeto/repositório):

{% include image.html src='/files/2021/06/github-to-gitlab-02.jpg' %}

E então, clique no **GitHub**:

{% include image.html src='/files/2021/06/github-to-gitlab-03.jpg' %}

Você será redirecionado para uma página no GitHub para autorizar o GitLab. Faça _login_ na sua conta do GitHub:

{% include image.html src='/files/2021/06/github-to-gitlab-04.jpg' %}

Você será redirecionado de volta para o GitLab, que listará todos os seus repositórios do GitHub.

Procure na lista o repositório que você quer importar (ex.: [Linux-Kamarada-GNOME][repo-github]):

{% include image.html src='/files/2021/06/github-to-gitlab-05.jpg' %}

Selecione para qual grupo ou usuário o repositório será importado (ex.: o grupo [kamarada][kamarada-gitlab], em vez da minha conta de usuário [antoniomedeiros]):

Você também pode mudar o nome do repositório, se quiser (é opcional).

Quando terminar, clique no botão **Import** (importar) na mesma linha do repositório.

A coluna **Status** mostra o _status_ da importação de cada repositório. Você pode deixar a página aberta que ela se atualiza sozinha ou pode navegar normalmente, fazer outras coisas e voltar para ela depois.

Concluída a importação do repositório, você pode clicar no botão **Go to project** (ir para o projeto):

{% include image.html src='/files/2021/06/github-to-gitlab-06.jpg' %}

Confira se está tudo no lugar. Por exemplo, os _branches_ (ramificações) foram importados:

{% include image.html src='/files/2021/06/github-to-gitlab-07.jpg' %}

As _issues_ tanto abertas (_open_) quanto fechadas (_closed_) também foram importadas:

{% include image.html src='/files/2021/06/github-to-gitlab-08.jpg' %}

Note que a importação do GitLab traduziu corretamente meu usuário [vinyanalista] do GitHub para meu usuário [antoniomedeiros] do GitLab, mas não encontrou equivalente no GitLab para o usuário [kevinsmia1939] do GitHub, importando os comentários dele com a minha conta e adicionando observações no início para esclarecer que são, na verdade, dele:

{% include image.html src='/files/2021/06/github-to-gitlab-09.jpg' %}

Talvez ele não tenha conta no GitLab ou, se tem, se registrou com outro _e-mail_.

## Atualizando o remote do clone local

Agora que seu repositório está no GitLab, se você tem um clone dele no seu computador, deve apontar sua cópia local para o novo repositório remoto.

Para isso, abra um terminal na pasta do repositório local.

Liste os repositórios remotos configurados:

```
$ git remote -v
origin	git@github.com:kamarada/Linux-Kamarada-GNOME.git (fetch)
origin	git@github.com:kamarada/Linux-Kamarada-GNOME.git (push)
```

O remoto padrão `origin` está apontando para o GitHub. Vamos apontá-lo para o GitLab.

Abra o projeto importado no GitLab e copie sua URL como se fosse cloná-lo:

{% include image.html src='/files/2021/06/github-to-gitlab-10.jpg' %}

De volta ao terminal, mude a URL do remoto `origin`:

```
$ git remote set-url origin git@gitlab.com:kamarada/Linux-Kamarada-GNOME.git
```

Se você listar os remotos de novo, verá que agora o padrão `origin` aponta para o GitLab:

```
$ git remote -v
origin	git@gitlab.com:kamarada/Linux-Kamarada-GNOME.git (fetch)
origin	git@gitlab.com:kamarada/Linux-Kamarada-GNOME.git (push)
```

De agora em diante, todo **git pull** e **git push** irá baixar e enviar _commits_ para o GitLab.

Opcionalmente, se você ainda vai precisar fazer alterações no repositório que está no GitHub (como, por exemplo, eu precisei alterar arquivos no [kamarada-website][kamarada-website-github] que está no GitHub, que divergiu do equivalente [kamarada.gitlab.io][kamarada-website-gitlab] no GitLab), você pode adicionar a URL do antigo repositório no GitHub como um novo remoto, com outro nome (ex.: `github`):

```
$ git remote add github git@github.com:kamarada/Linux-Kamarada-GNOME.git

$ git remote -v
github	git@github.com:kamarada/Linux-Kamarada-GNOME.git (fetch)
github	git@github.com:kamarada/Linux-Kamarada-GNOME.git (push)
origin	git@gitlab.com:kamarada/Linux-Kamarada-GNOME.git (fetch)
origin	git@gitlab.com:kamarada/Linux-Kamarada-GNOME.git (push)
```

Com isso, você pode fazer alterações nos dois repositórios sem precisar fazer outro clone (aqui, não vou entrar em detalhes de como gerenciar esse arranjo).

## Avisando a todos a mudança

É recomendado encerrar todas as _issues_ e _pull requests_ no GitHub, assim como atualizar o arquivo README (leia-me) e a descrição do repositório, informando a todos que o repositório mudou de endereço para o GitLab. Talvez você queira avisar às pessoas também nas redes sociais, no _site_ do projeto, etc.

Como exemplo, veja como ficou o repositório [Linux-Kamarada-GNOME][repo-github] após os avisos e o arquivamento (falarei sobre isso na sequência):

{% include image.html src='/files/2021/06/github-to-gitlab-11.jpg' caption='Arquivo README e descrição do repositório' %}

{% include image.html src='/files/2021/06/github-to-gitlab-12.jpg' caption='Comentário na issue, tornada somente leitura pelo arquivamento' %}

Na verdade, eu não fechei as _issues_: as que estavam abertas, deixei abertas, apenas fiz comentários com _links_ para o novo endereço no GitLab (a numeração é mantida). Como eu tinha poucas _issues_ abertas, saí comentando em cada uma (mas fiz isso só nas abertas). Se você souber alguma forma de automatizar esses comentários — pro caso de um repositório com muitas _issues_ abertas — por favor, compartilhe comigo nos comentários.

## Arquivando o repositório do GitHub

Se você não vai mais fazer alterações no repositório antigo no GitHub, você pode [arquivá-lo][github-docs-2] a fim de torná-lo somente leitura e indicar que não recebe mais manutenção. Caso você mude de ideia no futuro, o GitHub permite desarquivar repositórios que foram arquivados.

Para arquivar o repositório no GitHub, acesse a página principal do repositório.

Abaixo do nome do repositório, clique em **Settings** (configurações):

{% include image.html src='/files/2021/06/github-to-gitlab-13.jpg' %}

Em **Danger Zone** (zona de perigo), clique em **Archive this repository** (arquivar este repositório):

{% include image.html src='/files/2021/06/github-to-gitlab-14.jpg' %}

Leia os avisos com atenção:

{% include image.html src='/files/2021/06/github-to-gitlab-15.jpg' %}

Depois, digite o nome do repositório que você deseja arquivar e clique em **I understand the consequences, archive this repository** (entendo as consequências, arquive este repositório).

## Conclusão

Pronto! Agora você pode conferir o novo repositório no GitLab: [Linux-Kamarada-GNOME][repo-gitlab]. E o repositório antigo arquivado no GitHub: [Linux-Kamarada-GNOME][repo-github].

Durante os próximos dias estarei migrando mais repositórios usando esse meu próprio roteiro. Espero que ele possa ser útil para você também.

## Referências

- [Import your project from GitHub to GitLab \| GitLab][gitlab-docs]
- [How to Change a Git Remote's URL \| Linuxize][linuxize]
- [Managing remote repositories - GitHub Docs][github-docs-3]
- [Arquivar repositórios - GitHub Docs][github-docs-4]

[moving-to-gitlab]:         {% post_url pt/2021-03-10-linux-kamarada-esta-migrando-para-o-gitlab %}
[GitHub]:                   https://github.com/
[GitLab]:                   https://gitlab.com/
[making-of]:                https://linuxkamarada.com/pt/2021/04/12/como-fazer-uma-distro-baseada-no-opensuse-bastidores-do-linux-kamarada-comecando/
[repo-github]:              https://github.com/kamarada/Linux-Kamarada-GNOME
[Kiwi]:                     https://osinside.github.io/kiwi/
[gitlab-docs]:              https://docs.gitlab.com/ee/user/project/import/github.html
[github-docs-1]:            https://docs.github.com/pt/github/setting-up-and-managing-your-github-user-account/managing-email-preferences/setting-your-commit-email-address
[kamarada-gitlab]:          https://gitlab.com/kamarada
[antoniomedeiros]:          https://gitlab.com/antoniomedeiros
[vinyanalista]:             https://github.com/vinyanalista
[kevinsmia1939]:            https://github.com/kevinsmia1939
[kamarada-website-github]:  https://github.com/kamarada/kamarada-website
[kamarada-website-gitlab]:  https://gitlab.com/kamarada/kamarada.gitlab.io
[github-docs-2]:            https://docs.github.com/pt/github/creating-cloning-and-archiving-repositories/archiving-a-github-repository/about-archiving-repositories
[repo-gitlab]:              https://gitlab.com/kamarada/Linux-Kamarada-GNOME
[linuxize]:                 https://linuxize.com/post/how-to-change-git-remote-url/
[github-docs-3]:            https://docs.github.com/en/github/getting-started-with-github/getting-started-with-git/managing-remote-repositories#adding-a-remote-repository
[github-docs-4]:            https://docs.github.com/pt/github/creating-cloning-and-archiving-repositories/archiving-a-github-repository/archiving-repositories
