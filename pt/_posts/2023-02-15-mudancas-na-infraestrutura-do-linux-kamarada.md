---
date: '2023-02-15 23:30:00 GMT-3'
image: '/files/2023/02/kamarada-infra.jpg'
layout: post
nickname: 'packages.linuxkamarada.com'
title: 'Mudanças na infraestrutura do Linux Kamarada'
---

{% include image.html src='/files/2023/02/kamarada-infra.jpg' %}

O Linux Kamarada 15.4 Final está pronto para ser lançado: houve apenas uma pequena correção após a versão [15.4 RC], a imagem ISO já está pronta e a notícia de lançamento já foi redigida. No entanto, na semana passada, não consegui atualizar o repositório da versão 15.4 na [OSDN]. O repositório ainda está disponível, os arquivos que já estavam lá ainda podem ser acessados, mas não consigo substituí-los por novos arquivos.

Entrei em contato com o suporte da OSDN, mas ainda não obtive resposta ([OSDN User Support Ticket #47329][ticket-47329]). Outros projetos de _software_ livre hospedados pela OSDN estão enfrentando o mesmo problema ([Ticket #47324][ticket-47324]).

Para resolver esse impasse, fiz algumas mudanças na infraestrutura do Linux Kamarada.

Antes, tanto os pacotes [RPM] quanto as imagens ISO do Linux Kamarada estavam hospedados na [OSDN]. Esse repositório não será mais atualizado.

A partir de agora, os pacotes RPM do Linux Kamarada estão disponíveis em [packages.linuxkamarada.com](https://packages.linuxkamarada.com/), uma instância do [GitLab Pages][gitlab], o mesmo serviço que hospeda o _site_ [linuxkamarada.com](https://linuxkamarada.com/).

Já as imagens ISO do Linux Kamarada estão hospedadas no [SourceForge].

Não usei o mesmo serviço para hospedar pacotes e imagens ISO, como era antes com a OSDN, porque o GitLab Pages limita o espaço de armazenamento e a largura de banda (o que o torna inviável para hospedar as imagens ISO) e o SourceForge não permite o _download_ direto de arquivos (o que o torna inviável para hospedar o repositório de pacotes RPM).

A título de curiosidade, esta não é a primeira vez que o Linux Kamarada usa a hospedagem do SourceForge. A primeira versão do Linux Kamarada lançada oficialmente (com notícia de lançamento) foi a [15.1]. Mas existiram versões do Linux Kamarada baseadas no openSUSE Leap 42.2 que nunca foram lançadas oficialmente. Essas imagens ISO antigas podem ser encontradas no [SourceForge][sf-42.2]. Para minha surpresa, elas tiveram 850 _downloads_ de 2017 até agora.

## Onde baixar as imagens ISO

A página [Download] foi atualizada com os _links_ para as imagens ISO no SourceForge.

## Como usar o novo repositório

Se você já usa o Linux Kamarada, deve ajustar a [URL] do repositório oficial do Linux Kamarada no seu sistema.

Você pode fazer isso de duas formas: pela interface gráfica ou pelo terminal.

### Pelo terminal

A solução pelo terminal usa o gerenciador de pacotes **[zypper]**. Como ela é mais resumida, vou começar mostrando ela. Nesse caso, é mais fácil remover o repositório do Linux Kamarada e adicioná-lo novamente já com a nova URL. Para fazer isso, execute os comandos a seguir como usuário administrador (_root_):

```
# zypper rr kamarada
# zypper addrepo -f -K -n "Linux Kamarada" -p 95 "https://packages.linuxkamarada.com/\$releasever/openSUSE_Leap_\$releasever/" kamarada
```

### Pela interface gráfica

Embora seja mais amigável, a solução pela interface gráfica requer alguns cliques e passa por mais telas. Mas a facilidade mais que compensa esse caminho mais longo.

Abra o [Centro de Controle do YaST][yast]. Para isso, clique em **Mostrar aplicativos**, no canto inferior esquerdo da tela, e clique no ícone do **YaST**, no final da lista de aplicativos:

{% include image.html src="/files/2022/12/upgrade-to-15.4-04-pt.jpg" %}

Será solicitada a você a senha do usuário *root*. Digite-a e clique em **Continuar**:

{% include image.html src="/files/2022/12/upgrade-to-15.4-05-pt.jpg" %}

Dentro da categoria **Software**, a primeira, clique no item **Repositórios de software**:

{% include image.html src="/files/2022/12/upgrade-to-15.4-06-pt.jpg" %}

Você será apresentado à lista de repositórios do seu sistema:

{% include image.html src="/files/2023/02/packages-01-pt.jpg" %}

Selecione o repositório oficial do **Linux Kamarada** na lista e clique em **Editar**.

Na tela que aparece, substitua a **URL do repositório** por:

```
https://packages.linuxkamarada.com/$releasever/openSUSE_Leap_$releasever/
```

{% include image.html src="/files/2023/02/packages-02-pt.jpg" %}

E clique em **OK**.

De volta à lista de repositórios, clique em **OK** para gravar as alterações, e feche o YaST.

## Quando a versão 15.4 Final será lançada?

Adiarei o lançamento da versão 15.4 Final em alguns dias a fim de que a nova infraestrutura seja testada. Novas instalações do Linux Kamarada feitas com a imagem ISO da versão 15.4 Final já usarão o novo repositório de pacotes em [packages.linuxkamarada.com](https://packages.linuxkamarada.com/).

[15.4 RC]:      {% post_url pt/2022-12-19-linux-kamarada-libera-versao-candidata-a-lancamento-15-4-rc %}
[OSDN]:         https://osdn.net/projects/kamarada/
[ticket-47329]: https://osdn.net/projects/support/ticket/47329
[ticket-47324]: https://osdn.net/projects/support/ticket/47324
[RPM]:          https://pt.wikipedia.org/wiki/RPM_(software)
[gitlab]:       {% post_url pt/2021-03-10-linux-kamarada-esta-migrando-para-o-gitlab %}
[SourceForge]:  https://sourceforge.net/projects/kamarada/
[15.1]:         {% post_url pt/2020-02-24-kamarada-15.1-vem-com-tudo-que-voce-precisa-para-usar-o-linux-no-dia-a-dia %}
[sf-42.2]:      https://sourceforge.net/projects/kamarada/files/distribution/leap/42.2/
[Download]:     /pt/download
[URL]:          https://pt.wikipedia.org/wiki/URL
[zypper]:       https://pt.opensuse.org/Portal:Zypper
[yast]:         http://yast.opensuse.org/
