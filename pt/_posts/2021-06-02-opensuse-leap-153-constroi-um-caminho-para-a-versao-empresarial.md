---
date: '2021-06-02 23:35:00 GMT-3'
image: '/files/2021/06/opensuse-leap-yin-yang.png'
layout: post
nickname: 'opensuse-leap-15.3'
title: 'openSUSE Leap 15.3 constrói um caminho para a versão empresarial'
excerpt: 'O openSUSE Leap 15.3 foi lançado! Baixe já em get.opensuse.org/leap! A versão 15.3 é a &quot;minor release&quot; mais recente do openSUSE Leap. Ela traz toda a solidez da série 15.x do openSUSE 15.x, mantendo todos os atributos positivos de seus predecessores. Mas temos uma grande novidade em relação às versões anteriores do Leap: a partir de agora, o openSUSE Leap 15.3 é construído não apenas a partir do código-fonte do SUSE Linux Enterprise, como nas versões anteriores, mas ele também é construído exatamente com os mesmos pacotes binários, fornecendo compatibilidade total entre o Leap e o SLE como um yin yang.'
---

{% capture nota1 %}
Esta é uma cópia da notícia originalmente escrita colaborativamente por várias pessoas e publicada em [pt.opensuse.org](https://pt.opensuse.org/Anuncio_de_lançamento_15.3).
{% endcapture %}

{% include note.html text=nota1 %}

{% include image.html src='/files/2021/06/opensuse-leap-yin-yang.png' %}

NUREMBERGUE, Alemanha – O [openSUSE Leap 15.3](https://www.opensuse.org/) foi lançado! Baixe já em [get.opensuse.org/leap](https://get.opensuse.org/leap)!

A versão 15.3 é a "_minor release_" mais recente do [openSUSE Leap](https://en.opensuse.org/Portal:Leap). Ela traz toda a solidez da série 15.x do openSUSE 15.x, mantendo todos os atributos positivos de seus predecessores. Mas temos uma grande novidade em relação às versões anteriores do Leap: a partir de agora, o openSUSE Leap 15.3 é construído não apenas a partir do código-fonte do [SUSE Linux Enterprise](https://www.suse.com/products/server/), como nas versões anteriores, mas ele também é construído exatamente com os mesmos pacotes binários, fornecendo compatibilidade total entre o Leap e o SLE como um [yin yang](https://youtu.be/ezmR9Attpyc).

"Os recursos presentes nesta versão fazem com que o openSUSE Leap seja uma distribuição desejável para servidor, estação de trabalho, _desktop_ e _container_ para profissionais de TI, empresários, hobbistas, pequenas empresas e profissionais da educação", disse o gerente de lançamento Lubos Kocman.

Esta versão é extremamente benéfica para projetos de migração e testes de aceitação do usuário. Grandes equipes de desenvolvimento ganham valor agregado usando o openSUSE Leap 15.3 para executar e testar cargas de trabalho que possam ser aumentadas e transferidas para o SUSE Linux Enterprise Linux 15 SP3 para manutenção de longo prazo.

O openSUSE Leap oferece uma vantagem clara para os servidores, fornecendo pelo menos 18 meses de atualizações para cada versão. A comunidade apoia e se envolve com pessoas que usam versões anteriores do Leap por meio de canais da comunidade, como [listas de discussão](https://lists.opensuse.org), [Matrix](https://app.element.io/#/room/#project:opensuse.org), [Discord](https://discord.com/invite/opensuse), [Telegram (canal oficial em português)](https://t.me/opensusebr), [Facebook](https://www.facebook.com/groups/opensuseproject), etc.

Essa relação próxima e de apoio mútuo entre a distribuição comunitária Leap e a distribuição corporativa SLE torna este um lançamento empolgante para desenvolvedores, administradores de sistema, testadores de distros, fornecedores independentes de software e usuários e clientes da SUSE.

A sinergia entre o openSUSE Leap 15.3 e SLE 15 Service Pack 3 permite que os usuários tenham opções de milhares de pacotes com suporte da comunidade. Esses pacotes da comunidade são construídos em um projeto openSUSE chamado ["Backports"](https://en.opensuse.org/openSUSE:Packaging_for_Leap#How_do_packages_from_SUSE_Linux_Enterprise_get_to_openSUSE_Leap.3F) sobre os pacotes-base do SLE. Os _backports_ são publicados no [SUSE Package Hub](https://packagehub.suse.com/), tornando as migrações do Leap para o SLE uniformes e rápidas. Utilizando os recursos do sistema de arquivos [btrfs](https://btrfs.wiki.kernel.org/index.php/Main_Page), os usuários podem testar novos recursos no Leap, implantar no SLE e até mesmo reverter _snapshots_ caso algum problema ocorra.

O Leap permite que seus usuários utilizem quantas CPUs e máquinas virtuais quiserem, sem limitações. Os usuários do Leap podem migrar o servidor, máquina virtual ou containers existentes para o SUSE Linux Enterprise, caso seja necessário "ativar" o suporte corporativo posteriormente.

Muitos dos pacotes no [openSUSE Leap 15.3](https://get.opensuse.org) permanecem os mesmos da [versão anterior](https://en.opensuse.org/Portal:15.2). O Leap 15.3 vem com correções de bugs e backports de segurança para um [kernel de suporte de longo prazo](http://kroah.com/log/blog/2018/08/24/what-stable-kernel-should-i-use/#Distribution%20kernels). Isso garante que os usuários obtenham um sistema de servidor estável, enquanto também fornece aos usuários drivers para hardware mais recente.

## Atualizando a partir de versões anteriores do Leap

Os usuários que estão atualizando para o openSUSE Leap 15.3 precisam estar cientes de que não é recomendado atualizar diretamente de versões anteriores ao openSUSE Leap 15.2. É recomendável atualizar para o Leap 15.2 antes de atualizar para o Leap 15.3. A versão suporta apenas a atualização do openSUSE Leap 15.2 para 15.3, conforme destacado nas [notas da versão](https://doc.opensuse.org/release-notes/x86_64/openSUSE/Leap/15.3/RELEASE-NOTES.pt_BR.html#upgrade). É recomendado que os usuários leiam [esta seção antes de migrar](https://doc.opensuse.org/release-notes/x86_64/openSUSE/Leap/15.3/RELEASE-NOTES.pt_BR.html#sec.upgrade.152). Também é recomendado que não usem o comando **zypper patch** até a próxima semana.

## O que há de novo

Há novos grandes recursos no [Xfce](https://www.xfce.org) [4.16](https://www.xfce.org/about/news/?post=1608595200). Há uma nova identidade visual neste lançamento do Xfce. Com novos ícones e paleta, o Xfce brilha um pouco mais fora da caixa. O Gerenciador de configurações recebeu uma atualização visual de sua caixa de filtros, que agora pode ser ocultada permanentemente. Os recursos de pesquisa da caixa de filtros foram aprimorados pesquisando a parte descritiva dos 'Comentários' do arquivo de inicialização de cada caixa de diálogo (também conhecido como .desktop). A caixa de diálogo de configurações do gerenciador de energia está mais limpa e mostra as configurações 'na bateria' ou 'conectado', em oposição a ambas em uma frase enorme.

O GNU Health, o premiado sistema de gestão e informação de saúde e hospitalar, vem na versão 3.8 com um novo módulo odontológico e Odontogram. Como primeira distribuição, o openSUSE terá o MyGNUHealth, um Gerente de Saúde Médica Pessoal (Personal Medical Health Manager), que foi desenvolvido em cooperação entre o GNU Health e o projeto KDE. Ele é executado no PinePhone e na área de trabalho do Plasma e oferece ao usuário total propriedade e controle sobre seus dados.

O gerenciador de pacotes DNF deve ser incluído em uma atualização de manutenção e dará aos usuários a versão 4.7.0 que fornece novos recursos em toda a pilha e melhorias esperadas. A API DNF Python é estável e compatível. Um _container_ experimental de base "opensuse / leap-dnf" e "opensuse / leap-microdnf" agora está disponível. Uma leve implementação C de DNF chamada "Micro DNF" está incluída. Ele foi projetado para ser usado para realizar ações simples de gerenciamento de pacotes quando os usuários não precisam de um DNF completo e desejam os menores ambientes úteis possíveis. Isso é útil para o caso de _containers_ e _appliances_. O Micro DNF foi rebaseado para 3.8.0, o que traz muitas correções e melhorias. Finalmente, um _backend_ PackageKit alternativo experimental para usar DNF também está disponível.

O openSUSE Leap funciona perfeitamente em várias arquiteturas e a novidade neste lançamento é o suporte para sistemas [IBM Z e LinuxONE (s390x)](https://en.wikipedia.org/wiki/Linux_on_IBM_Z). A distribuição da comunidade ganhou acesso à arquitetura s390x a partir dos esforços para torná-la compatível com o SLE.

Nas versões anteriores do Leap, as arquiteturas [PowerPC](https://en.wikipedia.org/wiki/PowerPC) e [aarch64](https://en.wikipedia.org/wiki/AArch64) faziam parte do projeto "ports" e eram mantidos por equipes comunitárias separadas com recursos limitados. Agora, o openSUSE Leap usa diretamente pacotes binários do lado corporativo para aarch64, powerpc64 e x86_64. As imagens para estas arquiteturas estão disponíveis em [get.opensuse.org](https://get.opensuse.org). Pessoas interessadas na arquitetura armv7 e outras devem ler o anúncio sobre o [openSUSE Step](https://news.opensuse.org/2021/02/11/opensuse-new-project-looks-to-build-sle-on-more-architectures).

### Tecnologias de Containers

Os pacotes em "_technology preview_" mantém todas as mesmas versões do Leap 15.2, mas há atualizações de segurança para todos os pacotes como [containerd](https://containerd.io/), [podman](https://podman.io/), [kubeadm](https://github.com/kubernetes/kubeadm) e [cri-o](https://cri-o.io/).

Os usuários do Leap 15.3 terão mais poder para desenvolver, enviar e implantar aplicativos em _containers_ usando as tecnologias de _container_ mais novas que estão sendo mantidas na distribuição. O [Kubernetes](https://kubernetes.io/) dá um grande impulso aos recursos de orquestração de _container_, permitindo que os usuários automatizem implantações, escalonem e gerenciem aplicativos em containeres. O Helm, o gerenciador de pacotes do Kubernetes, ajuda os desenvolvedores e administradores de sistema a gerenciar a complexidade, definindo, instalando e atualizando os aplicativos Kubernetes mais complexos de forma rápida e fácil.

O Container Runtime Interface (CRI), que implementa o motor de "_runtime_" em conformidade com a Open Container Initiative (OCI) (CRI-O) também está incluído nesta versão. CRI-O é uma alternativa leve e totalmente compatível ao Docker, e permite que o Kubernetes use runtime compatível com OCI para executar _containers_ e _pods_ em um _cluster_.

Caso deseje, a opção de usar o [Docker](https://www.docker.com/) continua disponível.

### Containers

Os usuários do Leap podem migrar o servidor, máquina virtual ou _container_ existente para o SUSE Linux Enterprise em minutos, caso seja necessário "ativar" o suporte corporativo posteriormente.

Não há nenhuma restrição quanto a quantas CPUs podem ser executadas, quantas máquinas virtuais podem ser hospedadas, por quanto tempo a máquina pode ser executada ou outras restrições encontradas em algumas distribuições de nível empresarial gratuitas. Use à vontade!

### Inteligência Artificial (IA) e Aprendizado de Máquina

[Tensorflow](https://www.tensorflow.org/): uma estrutura para aprendizado profundo que pode ser usada por cientistas de dados, fornece cálculos numéricos e gráficos de fluxo de dados. Sua arquitetura flexível permite que os usuários implantem cálculos em uma ou mais CPUs em um desktop, servidor ou dispositivo móvel sem reescrever o código.

[PyTorch](https://pytorch.org/tutorials/): feita para servidores e recursos de computação de alta performance, esta biblioteca de aprendizado de máquina acelera a capacidade dos usuários avançados de prototipar um projeto e movê-lo para uma implantação de produção.

[ONNX](https://onnx.ai/): um formato aberto desenvolvido para representar modelos de aprendizado de máquina, fornece interoperabilidade no espaço de ferramentas de IA. Ele permite que os desenvolvedores de IA usem modelos com uma variedade de estruturas, ferramentas, tempos de execução e compiladores.

[Grafana](https://grafana.com/) e [Prometheus](https://prometheus.io/) são altamente úteis para especialistas analíticos. Grafana fornece aos usuários finais a capacidade de criar análises visuais interativas. Pacotes de modelagem de dados ricos em recursos: Graphite, Elastic e Prometheus dão aos usuários do openSUSE maior latitude para construir, computar e decifrar dados de forma mais inteligível.

### Desktop Environment

A versão de suporte de longo prazo (LTS) do Plasma 5.18 do KDE está mais uma vez disponível no Leap 15.3. O LTS tem uma quantidade significativa de recursos de polimento e qualidade. As notificações são mais claras, as configurações são simplificadas e a aparência geral é mais atraente. O GNOME 3.34 fornece uma quantidade considerável de atualizações visuais para vários aplicativos. Mais fontes de dados no sysprof tornam o perfil de desempenho de um aplicativo ainda mais fácil e há várias melhorias no Builder, incluindo um inspetor D-Bus integrado. Com um novo padrão para o Cinnamon, o Leap 15.3 oferece um total de 8 desktops atraentes para instalação (paralela), para atender às preferências pessoais e recursos de hardware.

### Imagens de nuvem (Cloud Images), hardware e arquiteturas

As imagens da nuvem Linode do Leap estão disponíveis hoje e prontas para todas as necessidades de infraestrutura. Os serviços de hospedagem em nuvem oferecerão imagens do Leap 15.3 nas próximas semanas, como Amazon Web Services, Azure, Google Compute Engine e OpenStack. O Leap 15 é continuamente otimizado para cenários de uso da nuvem como um host e convidado de virtualização.

Computadores TUXEDO e notebooks Linux podem ser adquiridos com o Leap 15.2 pré-instalado. O Leap 15.3 também pode ser encomendado pré-instalado com Slimbooks.

### Servidores e desktops

O Leap é ideal para ambientes de desktop e servidor. Administradores de sistema e pequenas empresas podem usar o Leap para hospedar servidores de web e de e-mail. Os administradores de sistemas podem aproveitar todas as vantagens do protocolo de gerenciamento de rede DHCP (Dynamic Host Configuration Protocol), alocar recursos usando Domain Name System (DNS) ou oferecer aos computadores clientes acesso a arquivos em um Network FileSystem (NFS). Pacotes de compartilhamento de arquivos e hosts como NextCloud também estão disponíveis e o pacote de aplicativos de groupware Kopano faz parte do lançamento oficial do Leap 15.3.

As arquiteturas disponíveis para [teste](https://get.opensuse.org/testing/) incluem [x86_64](https://en.wikipedia.org/wiki/X86-64), [aarch64](https: // en.wikipedia.org/wiki/AArch64), [PowerPC](https://en.wikipedia.org/wiki/PowerPC) e [s390x](https://en.wikipedia.org/wiki/Linux_on_IBM_Z). Caso esteja interessado na arquitetura Armv7, leia nosso anúncio sobre o [openSUSE Step](https://news.opensuse.org/2021/02/11/opensuse-new-project-looks-to-build-sle-on-more-architectures/).

Você pode encontrar mais informações sobre a imagem do openSUSE Leap 15.3 para o sub-sistema de Linux para Windows (WSL) [aqui](https://en.opensuse.org/openSUSE:WSL).

## Fim de Ciclo

O openSUSE Leap 15.2 terá seu [End of Life (EOL)](https://en.wikipedia.org/wiki/End-of-life_product) seis meses após o lançamento de hoje. Os usuários devem atualizar para o openSUSE Leap 15.3 para continuar a receber atualizações de segurança e manutenção.

## Download Leap 15.3

Para baixar a imagem ISO, visite: [https://get.opensuse.org/leap/](https://get.opensuse.org/leap/)

## Perguntas

Se você tiver alguma dúvida sobre o lançamento ou acha que encontrou um _bug_, adoraríamos ouvir de você em:

- [https://t.me/opensusebr](https://t.me/opensusebr)
- [https://lists.opensuse.org/opensuse-support/](https://lists.opensuse.org/opensuse-support/)
- [https://discordapp.com/invite/openSUSE](https://discordapp.com/invite/openSUSE)
- [https://www.facebook.com/groups/opensuseproject](https://www.facebook.com/groups/opensuseproject)

## Envolva-se

O Projeto openSUSE é uma comunidade mundial que promove o uso do Linux em todos os lugares. Ele cria duas das melhores distribuições de Linux do mundo: a versão Tumbleweed, com melhorias contínuas, e a Leap, a distribuição híbrida corporativa/comunidade. O openSUSE está continuamente trabalhando em conjunto de uma maneira aberta, transparente e amigável como parte da comunidade mundial de Software Livre e de Código Aberto. O projeto é controlado por sua comunidade e conta com a contribuição de indivíduos, trabalhando como testadores, escritores, tradutores, especialistas em usabilidade, artistas e embaixadores ou desenvolvedores. O projeto abrange uma ampla variedade de tecnologias, pessoas com diferentes níveis de conhecimento, que falam línguas diferentes e têm origens culturais diferentes. Saiba mais sobre isso em [opensuse.org](https://www.opensuse.org/).
