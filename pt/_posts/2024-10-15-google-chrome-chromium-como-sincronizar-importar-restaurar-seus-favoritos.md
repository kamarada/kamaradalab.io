---
date: '2024-10-15 21:00:00 GMT-3'
image: '/files/2024/10/chrome-sync-bookmarks.png'
layout: post
nickname: 'chrome-sync-bookmarks'
title: 'Google Chrome / Chromium: como sincronizar / importar / restaurar seus favoritos'
excerpt: 'Formatou o computador? Mudou de computador? Migrando de um navegador para outro? Ou do Windows para o Linux? Seja qual for sua situação, você deseja carregar seus favoritos com você. Veja dicas que podem te ajudar se você usa o navegador Google Chrome, o mais usado no mundo, ou sua base livre, o Chromium.'
---

{% include image.html src='/files/2024/10/chrome-sync-bookmarks.png' %}

Formatou o computador? Mudou de computador? Migrando de um navegador para outro? Ou do [Windows] para o [Linux]? Seja qual for sua situação, você deseja carregar seus favoritos com você. Veja a seguir dicas que podem te ajudar se você usa o navegador [Google Chrome], o [navegador mais usado no mundo][statcounter].

Quase todas as dicas a seguir também servem para o [Chromium], que é a base de [_software_ livre][free-sw] para o [proprietário] Google Chrome. Junto com o [Mozilla Firefox], o Chromium é um dos navegadores que vem no [Linux Kamarada 15.5].

## O jeito mais fácil: sincronizar

Se você está migrando de sistema ou computador, ou usa o Google Chrome em vários dispositivos (computador, _smartphone_, _tablet_, etc.), o jeito mais fácil de carregar seus favoritos aonde for é mantê-los sincronizados [ativando a sincronização do Google Chrome][sync] em todos os seus dispositivos. Fazendo isso, toda vez que você criar, alterar ou excluir um favorito em um dispositivo, isso será aplicado nos demais quando você abrir o Google Chrome neles.

De quebra, o Google Chrome sincroniza, além dos favoritos, várias outras configurações do navegador, incluindo senhas, histórico, extensões, entre outras. Para isso, você precisará criar uma [Conta do Google], caso ainda não tenha uma.

Observe que a sincronização é uma funcionalidade exclusiva do Google Chrome, [não presente no Chromium][omgubuntu].

Para ativar a sincronização do Google Chrome no computador, no canto superior direito da janela, clique em **Perfil**, e depois em **Ativar a sincronização**:

{% include image.html src='/files/2024/10/chrome-sync-01-pt.jpg' %}

Digite seu _e-mail_ e senha para entrar na sua Conta do Google, ou crie uma, caso ainda não tenha:

{% include image.html src='/files/2024/10/chrome-sync-02-pt.jpg' %}

Em **Ativar a sincronização**, clique em **Sim**:

{% include image.html src='/files/2024/10/chrome-sync-03-pt.jpg' %}

Pronto! A sincronização do Google Chrome com sua Conta do Google está ativada.

Se antes você já tinha conectado outro navegador à sua Conta do Google, agora o Chrome baixará seus favoritos e os exibirá na barra de favoritos. Note que se você já tinha favoritos neste navegador antes de sincronizá-lo, eles serão mesclados com os que já existiam na sua Conta do Google.

## Mudando de navegador?

Se você está apenas mudando de navegador no mesmo sistema -- digamos que você costumava usar outro navegador, como o Mozilla Firefox, e deseja passar a usar o Google Chrome -- é possível importar favoritos de outros navegadores para o Google Chrome (e outras configurações como histórico e senhas).

Primeiro, feche o outro navegador.

Depois, abra o **menu do Chrome**, no canto superior direito da janela, aponte para **Favoritos e listas** e clique em **Importar favoritos e configurações**:

{% include image.html src='/files/2024/10/chrome-import-other-browser-01-pt.jpg' %}

Selecione na lista o navegador cujos dados devem ser importados e clique em **Importar**:

{% include image.html src='/files/2024/10/chrome-import-other-browser-02-pt.jpg' %}

Por fim, clique em **Concluir**:

{% include image.html src='/files/2024/10/chrome-import-other-browser-03-pt.jpg' %}

Você já deve perceber os favoritos do outro navegador no seu Chrome.

Perceba que o Chrome põe os favoritos importados em uma pasta separada na barra de favoritos. Você pode organizá-los como preferir.

## Exportando os favoritos do Chrome

Você pode exportar seus favoritos para um arquivo HTML, que pode ser guardado como um _backup_ dos seus favoritos, ou importado em outro navegador como o Mozilla Firefox ou até o próprio Google Chrome.

Para isso, abra o **menu do Chrome**, aponte para **Favoritos e listas** e clique em **Gerenciador de favoritos**:

{% include image.html src='/files/2024/10/chrome-export-01-pt.jpg' %}

No canto superior direito, abra o menu **Organizar** e clique em **Exportar favoritos**:

{% include image.html src='/files/2024/10/chrome-export-02-pt.jpg' %}

Informe um local e um nome para o arquivo (o Chrome sugere, por padrão, um nome que contém a data atual, como `favoritos_15_10_2024.html`).

Seus favoritos foram exportados com sucesso. O arquivo HTML que você salvou agora pode ser importado em outro navegador.

Se quiser instruções de como importá-lo no Firefox, confira [esse tutorial][Mozilla Firefox].

## Importando favoritos para o Chrome

Você pode importar para o Chrome favoritos exportados de outro navegador como o Firefox ou até do próprio Chrome. Esses favoritos exportados precisam estar no formato de um arquivo HTML.

Para isso, abra o **menu do Chrome**, aponte para **Favoritos e listas** e clique em **Importar favoritos e configurações**.

Selecione na lista **Arquivo HTML com favoritos** e clique em **Escolher arquivo**:

{% include image.html src='/files/2024/10/chrome-import-01-pt.jpg' %}

Informe a localização do arquivo HTML contendo os favoritos a serem importados.

Os favoritos no arquivo HTML selecionado são adicionados à barra de favoritos do Chrome em uma pasta separada:

{% include image.html src='/files/2024/10/chrome-import-02-pt.jpg' %}

Você pode organizá-los como preferir.

## Restaurando os favoritos manualmente

Se você formatou o computador, ou mudou de computador, e não consegue mais abrir o Chrome no sistema anterior, mas consegue acessar os arquivos desse sistema, pode ser que sua única saída seja restaurar os favoritos manualmente, sem o auxílio de uma tela do próprio Chrome.

Todas as personalizações que você faz no Chrome, como sua página inicial, extensões, senhas salvas e favoritos, são gravadas em uma pasta especial chamada de [**perfil** (_profile_)][profiles]. A pasta do perfil é armazenada na sua pasta pessoal, em um lugar separado do aplicativo Chrome, de modo que, se algo der errado com o Chrome, suas informações ainda estejam lá.

Antes de começar, feche todas as janelas do Chrome e do Chromium abertas.

Usando o aplicativo **Arquivos**, procure a pasta do perfil antigo, cujos favoritos você quer restaurar, e também a pasta do perfil atual, que você está usando no momento e para o qual você deseja importar os favoritos.

Por padrão, o Google Chrome armazena os perfis:

- No Windows, em: `C:\Users\SeuNomeDeUsuario\AppData\Local\Google\Chrome\User Data\`; e
- No Linux, em: `/home/seunomedeusuario/.config/google-chrome/` (ou `chromium`).

Nessas pastas, podem existir pastas chamadas `Default`, `Profile 2`, `Profile 3`, etc. correspondentes aos perfis.

Para chegar nessas pastas, pode ser que você precise ativar a exibição de arquivos ocultos. Para isso, abra o **menu do Arquivos**, no canto superior direito da janela, e marque a opção **Mostrar arquivos ocultos**:

{% include image.html src='/files/2024/09/firefox-profile-03-pt.jpg' %}

Abra as pastas dos perfis e localize nelas o arquivo chamado `Bookmarks` (que quer dizer favoritos, em inglês):

{% include image.html src='/files/2024/10/chrome-profile-pt.jpg' %}

Esse arquivo contém seus favoritos.

Na pasta do perfil atual, renomeie esse arquivo para algo como `Bookmarks-original`. Você também pode fazer uma cópia desse arquivo para algum outro lugar, por precaução.

Copie o arquivo `Bookmarks` da pasta do perfil antigo para a pasta do perfil atual.

Abra o Chrome e perceba que os favoritos antigos agora aparecem na barra de favoritos.

Exporte esses favoritos seguindo as instruções que vimos anteriormente.

Feche mais uma vez o Chrome. De volta ao Arquivos, na pasta do perfil novo, exclua o arquivo `Bookmarks` e renomeie o arquivo `Bookmarks-original` de volta para `Bookmarks`.

Abra mais uma vez o Chrome. Perceba que agora aparecem os favoritos que você tinha antes.

Finalmente, você pode importar os favoritos do perfil antigo que você acabou de exportar.

Alternativamente, observe que os arquivos `Bookmarks` são arquivos de texto no formato [JSON]. Se você compreende esse formato, pode abrir os arquivos `Bookmarks` do perfil antigo e do perfil novo lado a lado um do outro e copiar e colar os favoritos de um para o outro, manualmente:

{% include image.html src='/files/2024/10/chrome-bookmarks-json-pt.jpg' %}

## Referências

Espero que essas dicas possam ter te ajudado a recuperar seus favoritos no Chrome após formatar ou mudar de computador ou de sistema. Este artigo foi uma compilação de informações encontradas em várias páginas da documentação oficial do Chrome e de mais alguns _sites_. Caso precise de mais informações, você pode consultá-las:

- [Acessar favoritos, senhas e mais em todos os seus dispositivos - Computador - Ajuda do Google Chrome](https://support.google.com/chrome/answer/165139?hl=pt-BR)
- [Login e sincronização no Chrome - Computador - Ajuda do Google Chrome][sync]
- [Importar favoritos e configurações do Chrome - Ajuda do Google Chrome](https://support.google.com/chrome/answer/96816?hl=pt-br)
- [Usar o Chrome com vários perfis - Computador - Ajuda do Google Chrome][profiles]
- [Use Chromium? Sync Features Will Stop Working on March 15 - OMG! Ubuntu][omgubuntu]
- [Where Are Google Chrome Bookmarks Stored? - Alphr](https://www.alphr.com/where-are-google-chrome-bookmarks-stored/)

[Windows]:              https://www.microsoft.com/pt-br/windows/
[Linux]:                https://www.vivaolinux.com.br/linux/
[Google Chrome]:        {% post_url pt/2019-07-05-18-aplicativos-que-voce-pode-usar-do-mesmo-jeito-no-linux-e-no-windows-parte-1 %}#2-google-chrome
[statcounter]:          https://gs.statcounter.com/
[Chromium]:             https://www.chromium.org/
[free-sw]:              https://www.gnu.org/philosophy/free-sw.pt-br.html
[proprietário]:         https://pt.wikipedia.org/wiki/Software_propriet%C3%A1rio
[Mozilla Firefox]:      {% post_url pt/2024-09-20-firefox-como-sincronizar-importar-restaurar-seus-favoritos %}
[Linux Kamarada 15.5]:  {% post_url pt/2024-05-27-linux-kamarada-15-5-mais-alinhado-com-o-opensuse-leap-e-com-outras-distribuicoes %}
[sync]:                 https://support.google.com/chrome/answer/185277?hl=pt-BR
[Conta do Google]:      https://support.google.com/accounts/answer/27441?hl=pt-BR
[omgubuntu]:            https://www.omgubuntu.co.uk/2021/01/chromium-sync-google-api-removed
[profiles]:             https://support.google.com/chrome/answer/2364824?hl=pt-BR
[JSON]:                 https://pt.wikipedia.org/wiki/JSON
