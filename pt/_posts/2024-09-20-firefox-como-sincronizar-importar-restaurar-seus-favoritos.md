---
date: '2024-09-20 00:40:00 GMT-3'
image: '/files/2024/09/firefox-sync-bookmarks.png'
layout: post
nickname: 'firefox-sync-bookmarks'
title: 'Firefox: como sincronizar / importar / restaurar seus favoritos'
excerpt: 'Formatou o computador? Mudou de computador? Migrando de um navegador para outro? Ou do Windows para o Linux? Seja qual for sua situação, você deseja carregar seus favoritos com você. Veja nesse artigo dicas que podem te ajudar se você usa o navegador Mozilla Firefox, o navegador padrão do Linux Kamarada 15.5.'
---

{% include image.html src='/files/2024/09/firefox-sync-bookmarks.png' %}

Formatou o computador? Mudou de computador? Migrando de um navegador para outro? Ou do [Windows] para o [Linux]? Seja qual for sua situação, você deseja carregar seus favoritos com você. Veja a seguir dicas que podem te ajudar se você usa o navegador [Mozilla Firefox], o navegador padrão do [Linux Kamarada 15.5].

## O jeito mais fácil: sincronizar

Se você está migrando de sistema ou computador, ou usa o Firefox em vários dispositivos (computador, _smartphone_, _tablet_, etc.), o jeito mais fácil de carregar seus favoritos aonde for é mantê-los sincronizados entre os vários dispositivos por meio do [Firefox Sync][sync]. Configure-o em todos os seus dispositivos e toda vez que você criar, alterar ou excluir um favorito em um dispositivo, isso será aplicado nos demais quando você abrir o Firefox neles.

De quebra, o Firefox Sync sincroniza, além dos favoritos, várias outras configurações do navegador, incluindo senhas, histórico, extensões, entre outras. Para usá-lo, você precisará criar uma [Conta Mozilla], caso ainda não tenha uma.

Para habilitar o Firefox Sync no computador, abra o **menu do Firefox** no canto superior direito da janela e, ao lado de **Sincronizar e salvar dados**, clique em **Entrar**:

{% include image.html src='/files/2024/09/firefox-sync-01-pt.jpg' %}

Digite seu _e-mail_ e senha para entrar na sua Conta Mozilla, ou criar uma, caso ainda não tenha:

{% include image.html src='/files/2024/09/firefox-sync-02-pt.jpg' %}

Se tudo der certo, você deve ver esta mensagem: **Você está conectado no Firefox.**

{% include image.html src='/files/2024/09/firefox-sync-03-pt.jpg' %}

Se antes você já tinha conectado outro navegador à sua Conta Mozilla, agora o Firefox Sync baixará seus favoritos e os exibirá na barra de favoritos. Note que se você já tinha favoritos neste navegador antes de sincronizá-lo, eles serão mesclados com os que já existiam na sua Conta Mozilla.

## Mudando de navegador?

Se você está apenas mudando de navegador no mesmo sistema -- digamos que você costumava usar outro navegador, como o [Google Chrome], e deseja passar a usar o Firefox -- é possível importar favoritos de outros navegadores para o Firefox (e outras configurações como histórico e senhas).

Primeiro, feche o outro navegador.

Depois, abra o **menu do Firefox** e clique em **Configurações**:

{% include image.html src='/files/2024/09/firefox-import-other-browser-01-pt.jpg' %}

Na seção **Importar dados de navegador**, clique no botão **Importar dados**:

{% include image.html src='/files/2024/09/firefox-import-other-browser-02-pt.jpg' %}

Selecione na lista o navegador cujos dados devem ser importados e clique em **Importar**:

{% include image.html src='/files/2024/09/firefox-import-other-browser-03-pt.jpg' %}

Por fim, clique em **Concluído**:

{% include image.html src='/files/2024/09/firefox-import-other-browser-04-pt.jpg' %}

Você já deve perceber os favoritos do outro navegador no seu Firefox.

## Exportando os favoritos do Firefox

Você pode exportar seus favoritos para um arquivo HTML, que pode ser guardado como um _backup_ dos seus favoritos, ou importado em outro navegador como o Google Chrome ou até o próprio Firefox.

Para isso, abra o **menu do Firefox**, clique em **Favoritos** e, na sequência, em **Gerenciar favoritos**, ao final:

{% include image.html src='/files/2024/09/firefox-export-01-pt.jpg' %}

Na janela **Biblioteca**, abra o menu **Importar e backup** e clique em **Exportar favoritos para HTML**:

{% include image.html src='/files/2024/09/firefox-export-02-pt.jpg' %}

Informe um local e um nome para o arquivo (o Firefox sugere, por padrão, `bookmarks.html`).

De volta à janela **Biblioteca**, você pode fechá-la.

Seus favoritos foram exportados com sucesso. O arquivo HTML que você salvou agora pode ser importado em outro navegador.

Se quiser instruções de como importá-lo no Chrome, confira [esse tutorial][Google Chrome].

## Importando favoritos para o Firefox

Você pode importar para o Firefox favoritos exportados de outro navegador como o Google Chrome ou até do próprio Firefox. Esses favoritos exportados precisam estar no formato de um arquivo HTML.

Para isso, abra o **menu do Firefox**, clique em **Favoritos** e, na sequência, em **Gerenciar favoritos**.

Na janela **Biblioteca**, abra o menu **Importar e backup** e clique em **Importar favoritos de HTML**.

Informe a localização do arquivo HTML contendo os favoritos a serem importados.

Os favoritos no arquivo HTML selecionado são adicionados aos favoritos do Firefox na pasta **Menu de favoritos**.

Se você esperava ver os favoritos importados na barra de favoritos e eles não apareceram lá, note, na janela **Biblioteca**, que pode haver uma confusão entre a pasta importada **Barra de favoritos** dentro da pasta **Menu de favoritos** e a pasta **Barra de favoritos** que já existia dentro de **Favoritos**:

{% include image.html src='/files/2024/09/firefox-import-pt.jpg' %}

Os favoritos que aparecem na barra de favoritos do Firefox estão na pasta **Barra de favoritos** que já existia em **Favoritos**. Pode ser que você precise arrumar os favoritos manualmente para que eles apareçam onde você deseja. Aproveite que a janela **Biblioteca** está aberta para fazer isso. Quando terminar, pode fechá-la.

## Restaurando os favoritos manualmente

Se você formatou o computador, ou mudou de computador, e não consegue mais abrir o Firefox no sistema anterior, mas consegue acessar os arquivos desse sistema, pode ser que sua única saída seja restaurar os favoritos manualmente, sem o auxílio de uma tela do próprio Firefox.

Todas as personalizações que você faz no Firefox, como sua página inicial, extensões, senhas salvas e favoritos, são gravadas em uma pasta especial chamada de [**perfil** (_profile_)][profiles]. A pasta do perfil é armazenada na sua pasta pessoal, em um lugar separado do aplicativo Firefox, de modo que, se algo der errado com o Firefox, suas informações ainda estejam lá.

Para abrir a pasta do perfil que você está usando no momento na sua atual instalação do Firefox, abra o **menu do Firefox**, clique em **Ajuda** e depois em **Informações técnicas**.

Na página que se abre, ao lado de **Pasta do perfil**, clique em **Abrir pasta**:

{% include image.html src='/files/2024/09/firefox-profile-01-pt.jpg' %}

A pasta do perfil é aberta no aplicativo **Arquivos**.

Localize nessa pasta o arquivo chamado `places.sqlite`:

{% include image.html src='/files/2024/09/firefox-profile-02-pt.jpg' %}

Esse arquivo é um banco de dados que contém seus favoritos e as listas de todos os arquivos que você baixou e _sites_ que você visitou.

Feche o Firefox e renomeie esse arquivo para algo como `places.sqlite-original`. Você também pode fazer uma cópia desse arquivo para algum outro lugar, por precaução.

Abra uma nova aba ou janela no Arquivos e localize o arquivo `places.sqlite` correspondente na pasta do perfil antigo.

Por padrão, o Firefox armazena os perfis:

- No Windows, em: `C:\Users\SeuNomeDeUsuario\AppData\Roaming\Mozilla\Firefox\Profiles\`; e
- No Linux, em: `/home/seunomedeusuario/.mozilla/firefox/`.

Para encontrar o perfil antigo, pode ser que você precise ativar a exibição de arquivos ocultos. Para isso, abra o **menu do Arquivos**, no canto superior direito da janela, e marque a opção **Mostrar arquivos ocultos**:

{% include image.html src='/files/2024/09/firefox-profile-03-pt.jpg' %}

Copie o arquivo `places.sqlite` da pasta do perfil antigo para a pasta do perfil atual.

Abra o Firefox e perceba que os favoritos antigos agora aparecem na barra de favoritos.

Exporte esses favoritos seguindo as instruções que vimos anteriormente.

Feche mais uma vez o Firefox. De volta ao Arquivos, na pasta do perfil novo, exclua o arquivo `places.sqlite` e renomeie o arquivo `places.sqlite-original` de volta para `places.sqlite`.

Abra mais uma vez o Firefox. Perceba que agora aparecem os favoritos que você tinha antes.

Finalmente, você pode importar os favoritos do perfil antigo que você acabou de exportar.

## Referências

Espero que essas dicas possam ter te ajudado a recuperar seus favoritos no Firefox após formatar ou mudar de computador ou de sistema. Este artigo foi uma compilação de informações encontradas em várias páginas da documentação oficial do Firefox. Caso precise de mais informações, você pode consultá-las:

- [Favoritos no Firefox \| Ajuda do Firefox](https://support.mozilla.org/pt-BR/kb/favoritos-no-firefox)
- [Como configurar a sincronização no meu computador? \| Suporte Mozilla](https://support.mozilla.org/pt-BR/kb/como-configurar-o-sync-no-meu-computador)
- [Sincronização de dados no Firefox \| Suporte Mozilla][sync]
- [Importe dados de outro navegador \| Ajuda do Firefox](https://support.mozilla.org/pt-BR/kb/importe-dados-de-outro-navegador)
- [Como exportar favoritos do Firefox para arquivo HTML para backup ou transferência \| Ajuda do Firefox](https://support.mozilla.org/pt-BR/kb/como-exportar-os-favoritos-do-firefox-em-html)
- [Importe favoritos de um arquivo HTML \| Ajuda do Firefox](https://support.mozilla.org/pt-BR/kb/importe-favoritos-de-um-arquivo-html)
- [Restaure favoritos de um backup ou mova-os para outro computador \| Ajuda do Firefox](https://support.mozilla.org/pt-BR/kb/restaure-favoritos-backup-ou-mova-outro-computador)
- [Perfis - Onde o Firefox armazena seus favoritos, senhas e outros dados de usuário \| Ajuda do Firefox][profiles]

[Windows]:              https://www.microsoft.com/pt-br/windows/
[Linux]:                https://www.vivaolinux.com.br/linux/
[Mozilla Firefox]:      {% post_url pt/2019-07-05-18-aplicativos-que-voce-pode-usar-do-mesmo-jeito-no-linux-e-no-windows-parte-1 %}#1-mozilla-firefox
[Linux Kamarada 15.5]:  {% post_url pt/2024-05-27-linux-kamarada-15-5-mais-alinhado-com-o-opensuse-leap-e-com-outras-distribuicoes %}
[sync]:                 https://support.mozilla.org/pt-BR/kb/sincronizacao
[Conta Mozilla]:        https://www.mozilla.org/pt-BR/account/
[Google Chrome]:        {% post_url pt/2024-10-15-google-chrome-chromium-como-sincronizar-importar-restaurar-seus-favoritos %}
[profiles]:             https://support.mozilla.org/pt-BR/kb/perfis-onde-firefox-armazena-dados-usuario
