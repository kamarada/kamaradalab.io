---
date: '2021-10-26 22:40:00 GMT-3'
image: '/files/2021/10/postfix-smtp-relay.jpg'
layout: post
published: true
nickname: 'postfix-smtp-relay'
title: 'Postfix como relay SMTP no openSUSE com auxílio do YaST'
---

{% include image.html src='/files/2021/10/postfix-smtp-relay.jpg' %}

Eu estava pesquisando [como enviar _e-mails_ usando _shell scripts_][email-shell-script] e fiquei impressionado com a facilidade com que o [openSUSE] permite instalar e configurar um servidor de _e-mail_ simples por meio do [Centro de Controle do YaST][yast], enquanto outras distribuições demandam mexer em vários arquivos de configuração e executar vários comandos.

[email-shell-script]: {% post_url pt/2021-10-26-como-enviar-e-mails-a-partir-de-shell-scripts-com-o-comando-mailx %}
[openSUSE]: https://www.opensuse.org/
[yast]: https://yast.opensuse.org/

Se você já usou algum cliente de _e-mail_ como o [Evolution] ou o [Thunderbird], sabe que o recebimento de _e-mails_ é feito por protocolos como o [IMAP] e o [POP3], enquanto o envio de _e-mails_ é feito pelo protocolo [SMTP].

[Evolution]: {% post_url pt/2017-09-05-conecte-o-evolution-ao-office-365 %}
[Thunderbird]: {% post_url pt/2018-09-12-sincronizando-o-gmail-no-thunderbird %}
[IMAP]: https://pt.wikipedia.org/wiki/IMAP
[POP3]: https://pt.wikipedia.org/wiki/POP3
[SMTP]: https://pt.wikipedia.org/wiki/Simple_Mail_Transfer_Protocol

Há principalmente dois tipos de programas em todo o trâmite necessário para enviar, transmitir e receber _e-mails_, que são:

- o **[MUA]** (_Mail User Agent_, "agente do usuário de _e-mail_", em uma tradução livre), que é o cliente de _e-mail_, o programa que o usuário usa para receber e/ou enviar _e-mails_, a exemplo dos já citados Evolution e Thunderbird e de outros aplicativos conhecidos, como [KMail], [Claws Mail], [Geary], [Microsoft Outlook] e dos utilitários de linha de comando **[mailx]** e [Mutt], dentre outros; e

[MUA]: https://pt.wikipedia.org/wiki/Cliente_de_e-mail
[KMail]: https://apps.kde.org/kmail2/
[Claws Mail]: https://www.claws-mail.org/
[Geary]: https://wiki.gnome.org/Apps/Geary
[Microsoft Outlook]: https://www.microsoft.com/outlook
[mailx]: http://heirloom.sourceforge.net/mailx.html
[Mutt]: http://www.mutt.org

- o **[MTA]** (_Mail Transfer Agent_, agente de transporte de _e-mail_), que é o servidor de _e-mail_, que tanto pode responder por um domínio como apenas encaminhar _e-mails_ para outros MTAs, exemplos incluem [Postfix], [Sendmail], [Exim] e outros.

[MTA]: https://pt.wikipedia.org/wiki/Agente_de_transporte_de_correio_eletrônico
[Postfix]: http://www.postfix.org/
[Sendmail]: http://www.sendmail.org/
[Exim]: https://www.exim.org/

Para enviar _e-mails_ usando _shell scripts_, podemos fazer um MUA como o **mailx** conectar diretamente no servidor SMTP de algum serviço de _e-mail_ como o [Gmail]. Mas usar um MTA local pode nos trazer a vantagem de desacoplar o envio do _e-mail_ da execução do _script_. Caso não seja possível conectar ao servidor SMTP (durante uma queda de conexão com a Internet, por exemplo), o MTA guarda o _e-mail_ em uma fila e tenta enviá-lo de novo depois, enquanto o MUA simplesmente falha porque não consegue enviar o _e-mail_ na mesma hora.

[Gmail]: https://www.google.com/gmail/

E para instalar e configurar um MTA localmente não precisamos subir todo um serviço de _e-mail_ novo com um domínio personalizado: podemos simplesmente configurar o MTA local para fazer **retransmissão SMTP** (_SMTP relay_). Dessa forma, a única função do MTA local é atuar como ponte entre o MUA, invocado pelo _script_, e o MTA remoto, do serviço de _e-mail_, retransmitindo os _e-mails_ e fazendo fila quando necessário.

Nesse tutorial, você verá como instalar e configurar o Postfix para atuar como _SMTP relay_ no openSUSE. Depois, veremos como enviar _e-mails_ a partir do terminal usando o **mailx**.

O [Postfix] é um MTA [_software_ livre][free-sw] que pode ser instalado em sistemas baseados no [Unix], como o [Linux], e é empacotado para fácil instalação por várias distribuições Linux.

[free-sw]: https://www.gnu.org/philosophy/free-sw.html
[Unix]: https://pt.wikipedia.org/wiki/Unix
[Linux]: https://www.vivaolinux.com.br/linux/

O passo a passo a seguir foi testado no [openSUSE Leap 15.3][leap-15.3] e no [Linux Kamarada 15.2][kamarada-15.2] (lembrando que o Linux Kamarada é focado em _desktops_, não em servidores, mas é possível fazer coisas assim nele também).

[leap-15.3]: {% post_url pt/2021-06-02-opensuse-leap-153-constroi-um-caminho-para-a-versao-empresarial %}
[kamarada-15.2]: {% post_url pt/2020-09-11-linux-kamarada-15.2-venha-para-o-lado-verde-elegante-e-moderno-da-forca %}

Escolhi como MTA e MUA, respectivamente, o Postfix e o **mailx** porque já vêm instalados por padrão no openSUSE Leap e podem ser facilmente instalados no Linux Kamarada.

Para variar, vou usar um [serviço de _e-mail_ alternativo ao Gmail][gmail-alternatives], o [Zoho Mail]. Criei uma conta nesse serviço só para enviar _e-mails_ a partir de _scripts_ para meu _e-mail_ pessoal.

As configurações a seguir podem variar conforme o serviço de _e-mail_ que você for usar. Verifique junto ao seu provedor de serviço como configurar o envio de _e-mails_ por SMTP.

[gmail-alternatives]: https://pt.vpnmentor.com/blog/5-otimas-alternativas-para-o-gmail/
[Zoho Mail]: https://www.zoho.com/pt-br/mail/

## Instalando os pacotes necessários

Para configurar o Postfix, vamos usar um módulo do YaST que não vem instalado por padrão. Para garantir que estamos todos na mesma página, vamos instalar todos os pacotes de que vamos precisar:

```
# zypper in yast2-mail postfix mailx
```

## Configurando o Postfix pelo YaST

Abra o Centro de Controle do YaST. Aqui vou mostrar a interface gráfica só porque é mais bonita, mas é possível usá-lo pelo terminal também, no caso de servidores.

Se você estiver usando a área de trabalho [GNOME], para iniciar o YaST, abra o menu **Atividades**, no canto superior esquerdo da tela, digite `yast` e clique no ícone correspondente.

[GNOME]: https://br.gnome.org/

Se você estiver usando o terminal, para iniciar o YaST, execute:

```
# yast
```

Na tela principal do YaST, na categoria **Serviços de rede**, clique em **Servidor de e-mail**:

{% include image.html src='/files/2021/10/postfix-smtp-relay-01-pt.jpg' %}

Na primeira tela do assistente de configuração, **Configurações gerais**, deixe as opções como estão marcadas por padrão e clique em **Próximo**:

{% include image.html src='/files/2021/10/postfix-smtp-relay-02-pt.jpg' %}

Na tela seguinte, no campo **Servidor de correio eletrônico de saída**, informe o endereço e a porta do servidor SMTP do serviço de _e-mail_ remoto, separados por dois pontos (no formato `endereco:porta`). Para o [Zoho Mail][zoho-smtp], por exemplo, informe `smtp.zoho.com:587`:

[zoho-smtp]: https://www.zoho.com/pt-br/mail/help/zoho-smtp.html

{% include image.html src='/files/2021/10/postfix-smtp-relay-03-pt.jpg' %}

Em **Criptografia TLS**, certifique-se de que a opção **Usar** esteja marcada (isso também pode variar de acordo com o serviço de _e-mail_, mas [geralmente o uso de TLS é recomendado][tls]).

[tls]: https://blog.validcertificadora.com.br/ssl-ou-tls-quais-sao-as-diferencas-entre-esses-protocolos/

Depois, clique em **Autenticação**.

Informe o **Nome de usuário** e **Senha** da sua conta no serviço de _e-mail_:

{% include image.html src='/files/2021/10/postfix-smtp-relay-04-pt.jpg' %}

(para o nome de usuário, use o formato `seunomedeusuario@zohomail.com`)

Clique **OK** e, na tela anterior, clique em **Próximo**.

Nessa última tela do assistente de configuração, **Entrada de e-mail**, também podemos deixar as opções como estão marcadas por padrão e clicar em **Concluir**:

{% include image.html src='/files/2021/10/postfix-smtp-relay-05-pt.jpg' %}

Feito isso, o Postfix está pronto para ser usado como _SMTP relay_.

## Testando o envio pelo Postfix

Para testar o funcionamento do Postfix, veja o tutorial:

- [Como enviar e-mails a partir de shell scripts com o comando mailx][email-shell-script]

## Dicas para gerenciar o Postfix

Para verificar se há _e-mails_ na fila para serem enviados, use o comando **[mailq]**.

[mailq]: http://www.postfix.org/mailq.1.html

Ele pode informar que a fila de _e-mails_ está vazia (_mail queue is empty_):

```
$ mailq
Mail queue is empty
```

Ou mostrar quais _e-mails_ estão na fila para serem enviados:

```
$ mailq
-Queue ID-  --Size-- ----Arrival Time---- -Sender/Recipient-------
CDE65BFC14B*     454 Tue Oct 26 11:35:27  seunomedeusuario@zohomail.com
                                         emaildodestinatario@exemplo.com
```

O comando **[postqueue]** produz a mesma saída, mas precisa ser executado como _root_:

[postqueue]: http://www.postfix.org/postqueue.1.html

```
# postqueue -p
Mail queue is empty
```

Para tentar enviar os _e-mails_ que estão na fila e, assim, esvaziá-la, use o comando **[postfix]**:

[postfix]: http://www.postfix.org/postfix.1.html

```
# postfix flush
```

Ou:

```
# postqueue -f
```

Para esvaziar a fila excluindo os _e-mails_ sem enviá-los, use o comando **[postsuper]**:

[postsuper]: http://www.postfix.org/postsuper.1.html

```
# postsuper -d ALL
```

Se precisar reiniciar o serviço do Postfix:

```
# systemctl restart postfix
```

Normalmente, o Postfix escreve seu _log_ em `/var/log/mail`. Você pode monitorá-lo com:

```
# tail -f /var/log/mail
```

Ou, se esse arquivo não existir no seu sistema, pode ser que o sistema esteja usando o sistema de _log_ do [systemd], chamado de _[journal]_, como é o caso do Linux Kamarada. Nesse caso, o comando para monitorar o _log_ é:

[systemd]: https://doc.opensuse.org/documentation/leap/reference/html/book-reference/cha-systemd.html
[journal]: https://doc.opensuse.org/documentation/leap/reference/html/book-reference/cha-systemd.html#sec-boot-systemd-advanced-logging

```
# journalctl -fu postfix
```

## Servidor de e-mail avançado

Nesse tutorial, abordei como configurar um servidor SMTP simples para retransmitir (fazer _relay_) de mensagens para um servidor SMTP maior, de um provedor de serviço. Mas, caso seu interesse seja saber como configurar um servidor SMTP para um domínio, sugiro que consulte a _wiki_ do openSUSE, que explica como configurar o Postfix e outros componentes:

- [Mail server HOWTO - openSUSE Wiki][opensuse-wiki]

[opensuse-wiki]: https://en.opensuse.org/Mail_server_HOWTO

Ou talvez você se interesse por uma suíte como o [Zimbra], que também inclui o Postfix.

[Zimbra]: https://www.zimbra.com/

## Referências

- [linux - Shell Script for Sending output or log via email - Server Fault][serverfault]
- [email - Sending a mail from a linux shell script - Stack Overflow][stackoverflow]
- [Zoho Mail -- Configurações SMTP][zoho-smtp]
- [How To: Postfix Flush the Mail Queue Using CLI - nixCraft][cyberciti]

[serverfault]: https://serverfault.com/a/340383
[stackoverflow]: https://stackoverflow.com/a/5156999/1657502
[cyberciti]: https://www.cyberciti.biz/tips/howto-postfix-flush-mail-queue.html
