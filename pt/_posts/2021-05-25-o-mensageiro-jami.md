---
date: '2021-05-25 23:55:00 GMT-3'
image: '/files/2021/05/im-jami-pt.jpg'
layout: post
nickname: 'jami'
title: 'O mensageiro Jami'
---

{% include image.html src='/files/2021/05/im-jami-pt.jpg' %}

O [Jami] é um [mensageiro instantâneo] _[peer-to-peer]_ (ponto-a-ponto ou P2P). Diferente de outros mensageiros mais comuns, como [WhatsApp], [Telegram] ou [Signal], que são centralizados, ou de outros mensageiros que vimos aqui, como [Session] e [Element], que são descentralizados, o Jami usa uma abordagem **distribuída** e não requer um servidor para transmitir mensagens entre usuários. Duas pessoas conectadas à mesma rede (pode ser até uma rede local sem acesso à Internet) podem se comunicar pelo Jami, ao menos na teoria (explicarei a seguir).

{% include image.html src='/files/2021/05/network-topologies.png' %}

Todas as comunicações no Jami são criptografadas de ponta a ponta. Sua conta não é armazenada em nenhum servidor, apenas no seu dispositivo. Você não precisa fornecer nenhuma informação pessoal, como número de telefone ou endereço de _e-mail_, para criar uma conta. As informações que você pode fornecer (se quiser) são todas opcionais. Você pode criar um nome de usuário para facilitar que outras pessoas te encontrem. E também pode definir uma senha, que é usada apenas para criptografar o arquivo da sua conta, protegendo suas mensagens de alguém que consiga acesso físico ao seu dispositivo.

Quando você usa o Jami pela primeira vez, ele apresenta um assistente de configuração. O aplicativo pede que você crie um nome de usuário, uma senha, um nome de exibição e um avatar. Você pode criar uma conta sem nenhum desses dados, se quiser, e configurá-los mais tarde. Depois dessa configuração inicial, que pode ser feita mesmo que você não tenha acesso à Internet, o Jami gera uma **JamiID** aleatória (única) para você. É um número hexadecimal comprido, escrito com 40 caracteres, que tecnicamente é a impressão digital (_fingerprint_) da sua chave pública. E é isso, agora você tem uma conta do Jami.

Eu disse "ao menos na teoria" antes porque, nos meus testes com o Jami, eu não consegui fazê-lo entregar mensagens sem usar uma conexão com a Internet. Eu conectei um _notebook_ e um _smartphone_ Android à mesma rede Wi-Fi. Baixei e instalei o Jami em ambos os dispositivos e, em seguida, desconectei o cabo do provedor do meu roteador Wi-Fi. Portanto, os dois dispositivos ficaram na mesma rede local, mas sem acesso à Internet. Em seguida, configurei o Jami em ambos os dispositivos. É bem verdade que a configuração inicial funcionou _offline_. Mas então, quando tentei enviar mensagens de um dispositivo para o outro, elas não foram entregues. Só foram depois que eu devolvi o cabo do provedor.

Isso mostra a desvantagem de mensageiros _peer-to-peer_ como o Jami: embora o P2P seja bom para a privacidade, já que a ausência de um servidor central significa ausência de um ponto central de falha ou interceptação, as mensagens só podem ser transmitidas quando remetente e destinatário estão ambos _online_. No entanto, o aplicativo pode guardar as mensagens localmente para esperar que o contato se conecte novamente, o que o Jami faz.

Provavelmente meu teste de mensagens _offline_ não funcionou porque, [na verdade, o Jami depende de alguns servidores][servidores]. Mas você pode levantar servidores semelhantes próprios (_self-hosted_) e configurar seus aplicativos cliente para usá-los. Os principais servidores usados pelo Jami são o que hospeda a [DHT] (tabela _hash_ distribuída, mesma técnica usada pelo [BitTorrent] e pelo [IPFS] para ajudar os nós na rede a se conectarem uns aos outros) e o servidor de nomes, usado para resolver nomes de usuários. Por padrão, o Jami vem configurado para usar os servidores `dhtproxy.jami.net` e `ns.jami.net`, respectivamente.

Você pode ter a mesma conta associada a mais de um dispositivo. Para isso, você deve abrir o Jami em um dispositivo que já possui a conta e escolher associar um novo dispositivo. O Jami gera um PIN aleatório e disponibiliza sua conta na DHT por 10 minutos. Em seguida, no outro dispositivo, abra o Jami e escolha associá-lo a uma conta existente. Eu testei esse recurso também e apenas os contatos foram copiados, não as mensagens. Além disso, depois que a conta estava associada a ambos os dispositivos e ambos estavam _online_, as mensagens recebidas foram exibidas em ambos os dispositivos, mas as enviadas apareciam apenas no dispositivo que as enviou de fato. Portanto, embora esse recurso funcione, não é tão transparente quanto em outros mensageiros, como o Telegram.

Associar a conta a outro dispositivo também é uma das formas de fazer _backup_ de uma conta do Jami. Como ela só existe nos dispositivos associados, a outra maneira de fazer _backup_ é copiar os arquivos da conta do dispositivo para outro lugar. Nas versões para Linux e Android, existe uma opção nas configurações da conta que facilita essa cópia.

Além disso, como sua conta existe somente nos seus dispositivos, não é possível redefinir a senha se você criar uma senha e depois esquecê-la. Não há a quem recorrer ou pedir ajuda. Se você tem medo de esquecer sua senha, pode usar um gerenciador de senhas para anotá-la com segurança, como o [KeePassXC], que vem com o [Linux Kamarada][kamarada-15.2].

Ao usar o Jami, você é o único responsável pelos seus dados. Por isso, tome cuidado. Se não quiser perder sua conta, faça _backup_ dos seus arquivos e da sua senha. Por outro lado, se você deseja excluir sua conta, basta desinstalar o Jami e excluir seus arquivos em todos os dispositivos associados. Fazendo isso, sua conta não existirá mais e ninguém poderá restaurá-la. Jamais.

O Jami é desenvolvido pela [Savoir-faire Linux][sfl] — uma empresa canadense que oferece consultoria em GNU/Linux — mas também é considerado um projeto do [GNU] apoiado pela [Free Software Foundation][fsf]. O código-fonte do Jami está disponível no [GitLab do projeto][fonte] e o aplicativo é distribuído sob a [licença GPL versão 3][gplv3] ou mais recente.

O Jami sempre foi desenvolvido pela Savoir-faire Linux, mas nem sempre se chamou assim. Lançado em 2004, no início era um _softphone_ SIP chamado [SFLphone]. Depois, em [2015], ele ganhou recursos de _chat_ e criptografia e foi renomeado para Ring. Em [2016], passou a fazer parte do Projeto GNU. Finalmente, em [2019], foi renomeado para Jami para evitar confusão com produtos comerciais (alguns até com recursos semelhantes). O nome Jami é inspirado em uma palavra [suaíli] que significa "comunidade". Por causa desse histórico relacionado principalmente a chamadas, alguns [_sites_][techradar] consideram o Jami uma alternativa livre ao [Skype].

O Jami oferece a maioria dos recursos comuns aos mensageiros: permite enviar mensagens de texto, de voz e de vídeo, assim como fotos, vídeos e arquivos, fazer chamadas de voz e de vídeo, conferências e transmitir a tela. O Jami também mantém o suporte ao SIP de quando era SFLphone e permite que você adicione contas SIP ao aplicativo além das contas Jami. Com isso, você pode fazer chamadas tanto para usuários do Jami quanto para usuários de SIP.

Grupos ainda não são suportados pelo Jami, mas esse é um recurso [em desenvolvimento][grupos].

Eu não recomendaria o Jami para o público em geral, porque acredito que a maioria das pessoas não quer se preocupar com fazer _backup_ das mensagens por conta própria e precisará recorrer a algum tipo de recuperação de conta algum dia. Mas, para usuários preocupados com a privacidade, o Jami certamente é uma ótima opção.

O _site_ do Jami fornece instruções sobre [como instalá-lo em algumas distribuições Linux][jami-linux], inclusive o openSUSE. Por exemplo, no [openSUSE Leap 15.2][leap-15.2] (e no Linux Kamarada 15.2):

```
# zypper addrepo https://dl.jami.net/nightly/opensuse-leap_15.2/ring-nightly.repo
# zypper install jami
```

Eu, pessoalmente, achei mais fácil instalar o Jami usando Flatpak a partir do [Flathub]:

```
# flatpak install net.jami.Jami
```

O _app_ do Jami para Android pode ser instalado a partir do [Google Play] ou do [F-Droid].

Também existem versões do Jami para outros sistemas. Veja mais em: [jami.net](https://jami.net/download/).

O Jami tem duas páginas muito explicativas de perguntas frequentes no seu [_site_][faq-1] e no [GitLab do projeto][faq-2].

Esse texto faz parte da série:

- [Aplicativos de mensagens livres e focados em privacidade alternativos ao WhatsApp][mensageiro instantâneo]

[jami]:                     https://jami.net/
[mensageiro instantâneo]:   {% post_url pt/2021-05-08-aplicativos-de-mensagens-livres-e-focados-em-privacidade-alternativos-ao-whatsapp %}
[peer-to-peer]:             https://pt.wikipedia.org/wiki/Peer-to-peer
[whatsapp]:                 https://www.whatsapp.com/
[telegram]:                 {% post_url pt/2021-05-09-o-mensageiro-telegram %}
[signal]:                   {% post_url pt/2021-05-11-o-mensageiro-signal %}
[session]:                  {% post_url pt/2021-05-18-o-mensageiro-session %}
[element]:                  {% post_url pt/2021-05-21-o-mensageiro-element-e-o-padrao-matrix %}
[servidores]:               https://jami.net/why-is-jami-truly-distributed/
[DHT]:                      https://pt.wikipedia.org/wiki/Distributed_hash_table
[BitTorrent]:               https://pt.wikipedia.org/wiki/BitTorrent
[IPFS]:                     https://pt.wikipedia.org/wiki/Sistema_de_Arquivos_Interplanet%C3%A1rio
[KeePassXC]:                https://keepassxc.org/
[kamarada-15.2]:            {% post_url pt/2020-09-11-linux-kamarada-15.2-venha-para-o-lado-verde-elegante-e-moderno-da-forca %}
[sfl]:                      https://savoirfairelinux.com/en/
[GNU]:                      https://www.gnu.org/
[fsf]:                      https://www.fsf.org/?set_language=pt-br
[fonte]:                    https://git.jami.net/savoirfairelinux/ring-project
[gplv3]:                    https://www.gnu.org/licenses/gpl-3.0.html
[SFLphone]:                 https://en.wikipedia.org/wiki/Jami_(software)#History
[2004]:                     https://en.wikipedia.org/wiki/Savoir-faire_Linux#History
[2015]:                     https://blog.savoirfairelinux.com/en-ca/2015/ring-ultimate-privacy-and-control-for-your-voice-video-and-chat-communications/
[2016]:                     https://lists.gnu.org/archive/html/info-gnu/2016-11/msg00001.html
[2019]:                     https://jami.net/ring-becomes-jami/
[suaíli]:                   https://pt.wikipedia.org/wiki/Língua_suaíli
[techradar]:                https://www.techradar.com/best/best-alternatives-to-skype
[Skype]:                    https://www.skype.com/
[grupos]:                   https://git.jami.net/savoirfairelinux/ring-project/-/wikis/technical/2.3.-Swarm
[jami-linux]:               https://jami.net/download-jami-linux/
[leap-15.2]:                {% post_url pt/2020-07-02-versao-15.2-do-opensuse-leap-traz-novos-e-empolgantes-pacotes-de-inteligencia-artificial-aprendizagem-de-maquina-e-containers %}
[flathub]:                  https://flathub.org/apps/details/net.jami.Jami
[google play]:              https://play.google.com/store/apps/details?id=cx.ring
[f-droid]:                  https://f-droid.org/repository/browse/?fdid=cx.ring
[faq-1]:                    https://jami.net/help/
[faq-2]:                    https://git.jami.net/savoirfairelinux/ring-project/-/wikis/tutorials/Frequently-Asked-Questions
