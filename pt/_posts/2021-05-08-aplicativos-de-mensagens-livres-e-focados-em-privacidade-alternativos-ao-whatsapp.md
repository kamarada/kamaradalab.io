---
date: 2021-05-08 23:59:00 GMT-3
image: '/files/2021/05/messaging-apps.jpg'
layout: post
nickname: 'messaging-apps'
title: 'Aplicativos de mensagens livres e focados em privacidade alternativos ao WhatsApp'
---

{% include image.html src='/files/2021/05/messaging-apps.jpg' %}

Nos últimos 20 anos, houve um aumento significativo no uso de [mensageiros instantâneos][im], com os mais populares sendo usados ativamente por mais de 1 bilhão de pessoas cada.

{% include youtube.html id="pdZ179PmCPk" %}

Hoje, o aplicativo de mensagens mais usado no mundo é o [WhatsApp], que tem mais de 2 bilhões de usuários em mais de 180 países. O fato de muitas pessoas usarem o mesmo aplicativo pode por um lado ser conveniente, mas por outro lado gerou preocupações quanto à privacidade e segurança das conversas.

Especialmente depois que [o Facebook comprou o WhatsApp em 2014 por 19 bilhões de dólares][fb-whatsapp], valor que chamou atenção para um aplicativo gratuito sem nenhuma forma aparente de monetização, como exibição de anúncios, assinatura ou funcionalidades pagas. Era especulado que o [Facebook] tinha interesse no WhatsApp para [mineração de dados][data-mining].

Na época, o Facebook prometeu que manteria os dados do WhatsApp privados e não os compartilharia com outros serviços. No entanto, desde então o WhatsApp já anunciou duas mudanças nos seus termos de serviço e política de privacidade, de modo a compartilhar cada vez mais dados com o Facebook: uma em [2016] e outra agora em [2021].

A primeira mudança parece ter passado despercebida, mas a recente ocasionou uma [debandada de usuários][debandada] para outros aplicativos como [Telegram] e [Signal]. O êxodo foi tão grande que [o WhatsApp adiou as mudanças][adiou], antes previstas para 8 de fevereiro, para 15 de maio de 2021, e tem recorrido a [campanhas publicitárias][debandada] para melhor explicá-las.

{% include image.html src='/files/2021/05/whatsapp-pt.png' %}

Talvez você queira aproveitar esse prazo para procurar outro aplicativo de mensagens. Se esse é o seu caso, a seguir você encontrará uma lista de possíveis alternativas ao WhatsApp. Eu pesquisei e testei alguns mensageiros instantâneos focados em segurança e privacidade e compilei essa lista. Todos são [multiplataforma] e tem versões para [Linux] e [Android] — que foram as que eu testei — mas podem também suportar outros sistemas.

## O que torna um mensageiro seguro?

A principal medida que aplicativos de mensagens tem usado para garantir a privacidade dos usuários é criptografar o conteúdo das mensagens.

**[Criptografia]** é uma técnica que usa matemática para cifrar ("embaralhar") o conteúdo de uma mensagem de forma que somente pode ser decifrada e lida por alguém que tenha determinada informação, como uma senha ou chave de criptografia (uma espécie de "segredo"). Se você pesquisar pela [história da criptografia][historia], verá que é usada há muito tempo: [o imperador romano Júlio César já usava criptografia][cesar] para se comunicar com seus generais.

É possível usar criptografia de diversas maneiras. Mais especificamente, os mensageiros mais seguros fazem [**criptografia de ponta-a-ponta**][e2ee]: quando você envia uma mensagem ou arquivo, o conteúdo é criptografado pelo aplicativo **antes** de deixar seu dispositivo (computador ou celular), e permanece assim criptografado até chegar no(s) dispositivo(s) da(s) outra(s) pessoa(s) com quem você está conversando. Somente o remetente e o(s) destinatário(s) possuem as chaves necessárias para decifrar — e, portanto, ler — as mensagens, de modo que ninguém além dos envolvidos na conversa consegue saber do que se fala, mesmo quando a conversa atravessa um meio inseguro como a Internet.

Se atente também às **configurações padrão** dos aplicativos de mensagens: alguns mensageiros até oferecem criptografia de ponta-a-ponta, mas não a trazem ativada por padrão, de modo que você precisa acessar as configurações do aplicativo e realmente habilitá-la para usá-la. Outros mensageiros criptografam apenas alguns tipos de mensagens. Não presuma que o aplicativo é seguro só porque sua descrição menciona "criptografia de ponta-a-ponta": procure saber se ou quando suas mensagens são criptografadas por ele.

O aplicativo WhatsApp diz que usa criptografia de ponta-a-ponta. Em 2015 e 2016 ocorreram [alguns episódios em que tribunais de justiça brasileiros solicitaram conteúdos de conversas ao WhatsApp][whatsapp-justica] e não foram atendidos, no que puniram a empresa com suspensão temporária do serviço. Por um lado, isso pode ser visto como uma forte evidência de que o WhatsApp usa mesmo criptografia de ponta-a-ponta: o serviço não conseguiria fornecer as conversas dos usuários aos tribunais mesmo se quisesse. Se ele armazena mensagens criptografadas, isso seria de fato tecnicamente impossível. Por outro lado, não dá pra afirmar isso com total certeza, porque o [código-fonte] do WhatsApp não é aberto, o que nos leva ao próximo ponto.

[**_Softwares_ de código aberto**][oss] são aqueles em que os desenvolvedores disponibilizam o código-fonte para quem tiver interesse (e o conhecimento técnico necessário) poder analisar como o _software_ funciona internamente. Os mensageiros considerados mais seguros são os de código aberto, porque podem ser auditados de forma mais fácil e independente, assim como erros (_bugs_) que sejam eventualmente encontrados podem ser corrigidos de forma mais fácil e rápida. De modo mais amplo, quando o _software_ trata de privacidade, segurança e criptografia, revelar seu código-fonte é amplamente considerado um indicador da integridade desse _software_: é possível ver que ele de fato faz o que diz que faz.

Mais seguros ainda são os aplicativos feitos com **compilação determinística** ([_reproducible builds_][reproducible-builds]), que, de forma simplificada, quer dizer que qualquer pessoa pode compilar a partir do código-fonte um binário igual _bit_ a _bit_ ao disponibilizado oficialmente para _download_. Ou seja, o aplicativo baixado da loja de fato foi construído com o código-fonte disponibilizado.

Outro ponto a ser analisado em um aplicativo de mensagens é se, embora ele use criptografia de ponta-a-ponta, protegendo, assim, o conteúdo das mensagens, ainda coleta outras informações sobre os usuários, como endereço IP, número e modelo do celular, lista de contatos, frequência e horário das mensagens, etc. Esses tipos de "informações sobre as informações" são chamados de **[metadados]**. A existência e a análise desses metadados representam um risco para jornalistas, manifestantes e ativistas de direitos humanos.

Também acho importante analisar quem são as pessoas por trás do aplicativo, tanto **quem desenvolve**, quanto **quem financia**. Dependendo do tipo de mensageiro, pode ser que ele precise de toda uma infraestrutura para armazenar e transmitir as mensagens. Se há um custo envolvido, é importante saber como é financiado. Afinal de contas, tem um ditado que diz "se o serviço parece ser de graça, pode ser que o produto seja, na verdade, você".

Todos os aplicativos apresentados a seguir usam criptografia de ponta-a-ponta, quase todos por padrão. Todos possuem código aberto (ainda que só do cliente, nem todos disponibilizam o código-fonte do servidor) e coletam poucos metadados ou nenhum.

## Lista de mensageiros livres

Vamos analisar os aplicativos de mensagens a seguir:

<div class="media mb-3">
    <img src="/files/2021/05/telegram.svg" class="mr-3" style="max-width: 48px;">
    <div class="media-body align-self-center">
        <h3 class="mt-0 mb-0"><a href="{% post_url pt/2021-05-09-o-mensageiro-telegram %}">Telegram</a></h3>
    </div>
</div>

<div class="media mb-3">
    <img src="/files/2021/05/signal.svg" class="mr-3" style="max-width: 48px;">
    <div class="media-body align-self-center">
        <h3 class="mt-0 mb-0"><a href="{% post_url pt/2021-05-11-o-mensageiro-signal %}">Signal</a></h3>
    </div>
</div>

<div class="media mb-3">
    <img src="/files/2021/05/session.png" class="mr-3" style="max-width: 48px;">
    <div class="media-body align-self-center">
        <h3 class="mt-0 mb-0"><a href="{% post_url pt/2021-05-18-o-mensageiro-session %}">Session</a></h3>
    </div>
</div>

<div class="media mb-3">
    <img src="/files/2021/05/element.svg" class="mr-3" style="max-width: 48px;">
    <div class="media-body align-self-center">
        <h3 class="mt-0 mb-0"><a href="{% post_url pt/2021-05-21-o-mensageiro-element-e-o-padrao-matrix %}">Element (e Matrix)</a></h3>
    </div>
</div>

<div class="media mb-3">
    <img src="/files/2021/05/jami.svg" class="mr-3" style="max-width: 48px;">
    <div class="media-body align-self-center">
        <h3 class="mt-0 mb-0"><a href="{% post_url pt/2021-05-25-o-mensageiro-jami %}">Jami</a></h3>
    </div>
</div>

<div class="media mb-3">
    <img src="/files/2021/05/briar.svg" class="mr-3" style="max-width: 48px;">
    <div class="media-body align-self-center">
        <h3 class="mt-0 mb-0"><a href="{% post_url pt/2021-05-30-o-mensageiro-briar %}">Briar</a></h3>
    </div>
</div>

<div class="media mb-3">
    <img src="/files/2021/05/wire.svg" class="mr-3" style="max-width: 48px;">
    <div class="media-body align-self-center">
        <h3 class="mt-0 mb-0"><a href="{% post_url pt/2021-06-02-o-mensageiro-wire %}">Wire</a></h3>
    </div>
</div>

## Referências

Me antecipo em compartilhar a lista de textos que consultei para escrever esta série:

- [Mantenha sua comunicação digital privada - Security-in-a-Box][securityinabox]
- [Comunicação em Tempo Real \| Privacidade.Digital][privacytools]
- [The Most Secure Messaging Apps for Android & iOS 2021 \| AVG][avg]
- [5 Better Privacy Alternatives to Dump WhatsApp - It's FOSS][itsfoss]
- [7 Best WhatsApp Alternatives In 2021: Privacy-Focused Messaging Apps - Fossbytes][fossbytes]
- [WhatsApp alternatives: the best messaging apps for Android and iPhone \| TechRadar][techradar]
- [Top 5 WhatsApp Alternatives in 2021 \[Best For Privacy & Safety\] - vpnMentor][vpnmentor]
- [5 alternatives to using WhatsApp in 2021 \| TechSpot][techspot]
- [5 of the Best Alternatives to WhatsApp that Actually Respect Your Privacy - Make Tech Easier][maketecheasier]
- [Best WhatsApp alternatives that respect your privacy - ProtonMail Blog][protonmail]

[im]:                   https://pt.wikipedia.org/wiki/Mensageiro_instantâneo
[whatsapp]:             https://www.whatsapp.com/
[fb-whatsapp]:          https://www.tecmundo.com.br/facebook/51564-bomba-facebook-compra-o-whatsapp-por-us-16-bilhoes.htm
[facebook]:             https://pt-br.facebook.com/
[data-mining]:          https://tecnoblog.net/151635/potencial-whatsapp-mineracao-de-dados/
[2016]:                 https://tecnoblog.net/200144/whatsapp-novos-termos-privacidade-mudancas/
[2021]:                 https://www.tecmundo.com.br/seguranca/209001-whatsapp-obrigar-usuario-compartilhar-dados-facebook.htm
[debandada]:            https://www.tecmundo.com.br/seguranca/209532-whatsapp-usa-jornais-stories-amenizar-debandada-usuarios.htm
[telegram]:             {% post_url pt/2021-05-09-o-mensageiro-telegram %}
[signal]:               {% post_url pt/2021-05-11-o-mensageiro-signal %}
[adiou]:                https://www.tecmundo.com.br/seguranca/211325-whatsapp-anuncia-prazo-aceite-nova-politica-privacidade.htm
[multiplataforma]:      https://pt.wikipedia.org/wiki/Multiplataforma
[linux]:                http://www.vivaolinux.com.br/linux/
[android]:              https://www.android.com/

[criptografia]:         https://securityinabox.org/pt/guide/secure-communication/#criptografia
[historia]:             https://pt.wikipedia.org/wiki/Criptografia#História
[cesar]:                https://pt.wikipedia.org/wiki/Cifra_de_César
[e2ee]:                 https://pt.wikipedia.org/wiki/Criptografia_de_ponta-a-ponta
[whatsapp-justica]:     https://pt.wikipedia.org/wiki/Bloqueio_do_WhatsApp_no_Brasil
[código-fonte]:         https://pt.wikipedia.org/wiki/Código-fonte
[oss]:                  https://pt.wikipedia.org/wiki/Software_de_código_aberto
[reproducible-builds]:  https://www.suse.com/c/reproducible-builds-in-opensuse-and-sle/
[metadados]:            https://securityinabox.org/pt/guide/secure-communication/#metadados

[securityinabox]:       https://securityinabox.org/pt/guide/secure-communication/
[privacytools]:         https://www.privacidade.digital/software/comunicacao-tempo-real/
[avg]:                  https://www.avg.com/en/signal/secure-message-apps
[itsfoss]:              https://itsfoss.com/private-whatsapp-alternatives/
[fossbytes]:            https://fossbytes.com/best-whatsapp-alternatives-privacy-focused-messaging-apps/
[techradar]:            https://www.techradar.com/best/whatsapp-alternatives
[vpnmentor]:            https://www.vpnmentor.com/blog/best-secure-alternatives-whatsapp/
[techspot]:             https://www.techspot.com/news/88291-5-alternatives-using-whatsapp-2021.html
[maketecheasier]:       https://www.maketecheasier.com/alternatives-to-whatsapp/
[protonmail]:           https://protonmail.com/blog/whatsapp-alternatives/
