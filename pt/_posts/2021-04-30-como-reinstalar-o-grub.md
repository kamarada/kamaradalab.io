---
date: 2021-04-30 19:45:00 GMT-3
image: '/files/2021/04/grub-error.jpg'
layout: post
published: true
nickname: 'reinstall-grub'
title: 'Como reinstalar o GRUB'
---

{% include image.html src="/files/2021/04/grub.png" %}

O [GRUB] é o carregador de inicialização (_boot loader_), que é o primeiro programa a ser carregado do disco, responsável por carregar o _kernel_ e iniciar o sistema operacional. Ele é conhecido pelo menu que apresenta quando o computador é ligado, especialmente útil em computadores com _dual boot_, ou seja, em que dois (ou mais) sistemas operacionais estão instalados (por exemplo, [Linux] e [Windows]) e o usuário precisa escolher qual sistema iniciar.

Em algumas situações, pode ser que esse menu deixe de funcionar. Como aconteceu comigo, enquanto testava o [openSUSE Leap 15.3 Beta][leap-15.3-beta]. Após [baixar atualizações para o sistema][how-to-update] usando o comando **zypper up** e reiniciar, o menu do GRUB não apareceu, apresentando a seguinte mensagem de erro:

{% include image.html src="/files/2021/04/grub-error.jpg" %}

```
Welcome to GRUB!

error: symbol 'grub_verify_string' not found.
Entering rescue mode...
grub rescue>
```

O que me fez reportar o _bug_ [boo#1183884][bugzillaoo], no qual ainda trabalho junto ao [Projeto openSUSE][opensuse].

Isso me fez pesquisar como reinstalar o GRUB. Encontrei duas possíveis soluções — uma delas funcionou pra mim — e as compartilho a seguir, caso sejam úteis para mais alguém.

Para tentar ambas as situações, já que você não consegue acesso ao sistema operacional instalado no seu computador, vai precisar de uma [imagem Live][live] gravada em um DVD ou em um _pendrive_. Você pode baixar o [Linux Kamarada 15.2][kamarada-15.2] na página [Download].

Comece dando o _boot_ pelo sistema Live e identificando as partições do seu disco. Você pode fazer isso, por exemplo, com o comando **[fdisk]**:

```
# fdisk -l
Disco /dev/sda: 447,1 GiB, 480103981056 bytes, 937703088 setores
Modelo de disco: KINGSTON SA400S3
Unidades: setor de 1 * 512 = 512 bytes
Tamanho de setor (lógico/físico): 512 bytes / 512 bytes
Tamanho E/S (mínimo/ótimo): 512 bytes / 512 bytes
Tipo de rótulo do disco: gpt
Identificador do disco: C5E68EC3-1350-4E0C-A335-252FF0FDD675

Dispositivo    Início       Fim   Setores Tamanho Tipo
/dev/sda1        2048    206847    204800    100M Sistema EFI
/dev/sda2      206848    239615     32768     16M Microsoft reservado
/dev/sda3      239616 208864604 208624989   99,5G Microsoft dados básico
/dev/sda4   208865280 209919999   1054720    515M Windows ambiente de recuperaçã
/dev/sda5   209920000 419635199 209715200    100G Linux sistema de arquivos
/dev/sda6   902049792 937701375  35651584     17G Linux sistema de arquivos
/dev/sda7   419635200 902049791 482414592    230G Linux sistema de arquivos

Partições lógicas fora da ordem do disco.
```

No meu caso, as informações que interessam são:

- o sistema operacional está instalado na partição `/dev/sda5`
- a partição EFI é a `/dev/sda1`
- portanto, o disco com que estamos trabalhando é o `/dev/sda`

Nos exemplos a seguir, eu considero essas partições. Se atente para mudar os comandos conforme a realidade do seu computador.

## 1) Sistema instalado e Live coincidem

A solução um pouco menos trabalhosa (que requer a execução de menos comandos) deve funcionar quando você dispõe da imagem Live referente ao sistema que está instalado no computador. Por exemplo, quando você instalou o Linux Kamarada 15.2 no seu computador, e agora o GRUB está com problema, e você tem a imagem Live do Linux Kamarada 15.2.

Crie um ponto de montagem para a partição do sistema e monte-a:

```
# mkdir -p /mnt/opensuse
# mount /dev/sda5 /mnt/opensuse -o subvol=@
```

Depois, monte dentro dela a partição EFI, caso você tenha uma:

```
# mount /dev/sda1 /mnt/opensuse/boot/efi/
```

Finalmente, execute o comando a seguir, que vai reinstalar o GRUB:

```
# grub2-install --root-directory=/mnt/opensuse /dev/sda
Instalando para a plataforma x86_64-efi.
Instalação terminada. Sem erros reportados.
```

(aqui, estou considerando Linux Kamarada e openSUSE, caso seu cenário seja outro — por exemplo, você está usando uma imagem Live do [Ubuntu] para reinstalar o GRUB em uma instalação do Ubuntu — o comando acima pode ser diferente)

Por fim, desmonte as partições e reinicie o computador:

```
# umount /mnt/opensuse/boot/efi/
# umount /mnt/opensuse/
# reboot
```

## 2) Sistema instalado diferente do Live

Essa solução, na teoria, funciona até mesmo entre distribuições Linux diferentes. Por exemplo, se o sistema instalado no computador é o Linux Kamarada mas a imagem Live que você dispõe é do Ubuntu (ou vice-versa). Isso funciona porque essa solução usa o comando **[chroot]** para entrar no sistema instalado. Então o GRUB que será reinstalado é o GRUB da distribuição instalada, não importando, para isso, qual é a distribuição da imagem Live.

Crie um ponto de montagem para a partição do sistema e monte-a:

```
# mkdir -p /mnt/opensuse
# mount /dev/sda5 /mnt/opensuse -o subvol=@
```

Depois, monte dentro dela a partição EFI, caso você tenha uma:

```
# mount /dev/sda1 /mnt/opensuse/boot/efi/
```

Agora vamos "emprestar" alguns sistemas de arquivos do sistema Live para o sistema instalado, usando a opção **-B** (ou **\--bind**) do comando **[mount]**:

```
# mount -B /dev /mnt/opensuse/dev
# mount -B /dev/pts /mnt/opensuse/dev/pts
# mount -B /proc /mnt/opensuse/proc
# mount -B /run /mnt/opensuse/run
# mount -B /sys /mnt/opensuse/sys
```

Então, entre no sistema instalado usando o comando **chroot**:

```
# chroot /mnt/opensuse
```

Com isso, todos os comandos a partir de agora serão executados no sistema instalado, não no sistema Live. A partição a que antes você se referia como `/mnt/opensuse` agora é a própria `/`.

Finalmente, execute o comando a seguir, que vai reinstalar o GRUB:

```
# grub2-install /dev/sda
Instalando para a plataforma x86_64-efi.
Instalação terminada. Sem erros reportados.
```

(aqui, estou considerando que o sistema instalado é o Linux Kamarada ou o openSUSE, não importando qual é o sistema Live usado, caso seu cenário seja outro — por exemplo, você está usando uma imagem Live do Linux Kamarada para reinstalar o GRUB em uma instalação do Ubuntu — o comando acima pode ser diferente)

Aproveite para recriar o initrd, que pode ser parte do problema também:

```
# mkinitrd
Creating initrd: /boot/initrd-5.3.18-47-default
dracut: Executing: /usr/bin/dracut --logfile /var/log/YaST2/mkinitrd.log --force /boot/initrd-5.3.18-47-default 5.3.18-47-default

[...]

dracut: *** Store current command line parameters ***
dracut: Stored kernel commandline:
dracut:  root=UUID=d9dd0940-6312-456a-b769-ccbea177fdc2 rootfstype=btrfs rootflags=rw,relatime,ssd,space_cache,subvolid=256,subvol=/@,subvol=@
dracut: *** Creating image file '/boot/initrd-5.3.18-lp152.63-preempt' ***
dracut: *** Creating initramfs image file '/boot/initrd-5.3.18-lp152.63-preempt' done ***
```

(esse comando pode demorar alguns minutos)

Por fim, saia do sistema instalado, desmonte as partições e reinicie o computador:

```
# exit
# umount /mnt/opensuse/sys
# umount /mnt/opensuse/run
# umount /mnt/opensuse/proc
# umount /mnt/opensuse/dev/pts
# umount /mnt/opensuse/dev
# umount /mnt/opensuse/boot/efi/
# umount /mnt/opensuse/
# reboot
```

## Conclusão

Remova a mídia Live para que o sistema seja iniciado a partir do disco do próprio computador e _voilà_: o menu do GRUB volta a aparecer.

No meu caso, dado que eu tinha o openSUSE Leap 15.3 Beta no computador, mas só dispunha da imagem Live do Linux Kamarada 15.2, na teoria eu deveria resolver meu problema com essa solução 2. Foi a primeira que eu tentei, mas não funcionou. Daí eu tentei a solução 1 e funcionou. É bem verdade que agora eu estou com todos os pacotes, _kernel_, etc. do openSUSE Leap 15.3 Beta e só o GRUB do [Leap 15.2][leap-15.2] (até o pacote RPM grub é o do Leap 15.3, mas o GRUB efetivamente instalado na partição EFI é o do Leap 15.2). Mas está funcionando. Desconfio que o problema esteja no GRUB do Leap 15.3, que não está reconhecendo minha configuração, ou no instalador do Linux Kamarada 15.2, que na época da instalação fez algo diferente do que deveria.

Seja como for, espero que consigamos descobrir onde está o problema, e que ele seja corrigido para que não aconteça nas próximas versões.

## Referências

- [How to setup a chroot environment using the rescue system in order to re-create the initrd \| Support \| SUSE][suse1]
- [How to reinstall the GRUB boot loader \| Support \| SUSE][suse2]
- [Fix grub on btrfs partition \| panticz.de][panticz]
- [How to Create and Mount Btrfs Subvolumes &#8211; Linux Hint][linuxhint]
- [GRUB rescue on UEFI based systems \| by Arnab Satapathi \| Medium][medium]

[grub]:             https://www.gnu.org/software/grub/
[linux]:            http://www.vivaolinux.com.br/linux/
[windows]:          https://www.microsoft.com/pt-br/windows/
[leap-15.3-beta]:   https://news.opensuse.org/2021/03/03/opensuse-leap-153-reaches-beta-build-phase/
[how-to-update]:    {% post_url pt/2019-05-14-como-obter-atualizacoes-para-o-linux-opensuse %}
[bugzillaoo]:       https://bugzilla.opensuse.org/show_bug.cgi?id=1183884
[opensuse]:         https://www.opensuse.org/
[live]:             {% post_url pt/2015-11-25-o-que-e-um-livecd-um-livedvd-um-liveusb %}
[kamarada-15.2]:    {% post_url pt/2020-09-11-linux-kamarada-15.2-venha-para-o-lado-verde-elegante-e-moderno-da-forca %}
[download]:         /pt/download/
[fdisk]:            https://man7.org/linux/man-pages/man8/fdisk.8.html
[ubuntu]:           https://ubuntu.com/
[chroot]:           https://www.man7.org/linux/man-pages/man1/chroot.1.html
[mount]:            https://www.man7.org/linux/man-pages/man8/mount.8.html
[leap-15.2]:        {% post_url pt/2020-07-02-versao-15.2-do-opensuse-leap-traz-novos-e-empolgantes-pacotes-de-inteligencia-artificial-aprendizagem-de-maquina-e-containers %}
[suse1]:            https://www.suse.com/support/kb/doc/?id=000018801
[suse2]:            https://www.suse.com/support/kb/doc/?id=000016528
[panticz]:          http://www.panticz.de/Fix-grub-on-btrfs-partition
[linuxhint]:        https://linuxhint.com/create-mount-btrfs-subvolumes/
[medium]:           https://medium.com/@Arnab_Sat/grub-rescue-on-uefi-based-systems-7bf520e414aa
