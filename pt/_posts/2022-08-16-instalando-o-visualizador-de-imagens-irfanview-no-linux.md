---
date: '2022-08-16 10:40:00 GMT-3'
image: '/files/2022/08/irfanview-00-pt.jpg'
layout: post
nickname: 'irfanview1'
title: 'Instalando o visualizador de imagens IrfanView no Linux'
---

Meu programa preferido para fazer edições simples de imagem no [Linux], por incrível que pareça, é um programa feito para [Windows]. Estou falando do **[IrfanView]**, um visualizador de imagens que conta com recursos básicos de edição, é pequeno, leve, simples e [existe desde a época do Windows 95][changelog].

<!--more-->

{% include image.html src='/files/2022/08/irfanview-logo.jpg' %}

{% include image.html src='/files/2022/08/irfanview-00-pt.jpg' caption='Tela inicial do IrfanView com uma imagem aberta' %}

Hoje, praticamente todos os gerenciadores de arquivos, a exemplo do [Explorador de Arquivos][explorer] do Windows e do [Arquivos] do [GNOME], exibem miniaturas de imagens. Mas o IrfanView é de uma época em que eles não faziam isso. Por isso, desde cedo ele traz um gerenciador de arquivos embutido que é próprio para imagens, e exibe miniaturas das imagens que ele consegue abrir, o **IrfanView Miniaturas** (_IrfanView Thumbnails_).

{% include image.html src='/files/2022/08/irfanview-01-pt.jpg' caption='IrfanView Miniaturas (IrfanView Thumbnails)' %}

Você pode estar se perguntando como é possível usar esse programa feito para Windows no Linux. Para isso, você pode usar o [Wine], que é uma camada de tradução que torna o Linux capaz de executar aplicativos Windows como se fossem aplicativos nativos do Linux, sem perda de desempenho ou visual.

O Wine já vem pré-instalado no [Linux Kamarada], de modo que você já pode simplesmente baixar, instalar e usar o IrfanView, da mesma forma como faria no Windows. Mas não é difícil instalar o Wine em outras distribuições Linux.

## Instalando o Wine

Se você usa o [openSUSE Leap][leap-vs-tumbleweed] ou o [openSUSE Tumbleweed][leap-vs-tumbleweed], instalar o Wine é tão simples quanto abrir o terminal e executar, como administrador (usuário _root_):

```
# zypper in wine
```

Se você usa outra distribuição Linux, consulte a documentação da sua distribuição para saber como instalar o Wine nela.

## Baixando o IrfanView

Acesse o _site_ do IrfanView em [irfanview.com](https://www.irfanview.com/) e clique no _link_ para _download_ da versão de 64 _bits_:

{% include image.html src='/files/2022/08/irfanview-02.jpg' %}

O IrfanView, por padrão, vem apenas no idioma Inglês. A instalação das traduções é opcional, mas eu recomendo que você instale a tradução para o Português do Brasil. Para baixar essa tradução, use o _link_ **Languages** (idiomas):

{% include image.html src='/files/2022/08/irfanview-03.jpg' %}

Na página seguinte, clique no _link_ para o instalador (_Installer_) correspondente:

{% include image.html src='/files/2022/08/irfanview-04.jpg' %}

## Instalando o IrfanView

Seu navegador deve ter baixado tanto o instalador do IrfanView quanto o instalador da tradução para a pasta **Downloads**. No Linux Kamarada, você pode simplesmente dar um duplo-clique no instalador do IrfanView, da mesma forma como faria no Windows. Uma alternativa é clicar com o botão direito no instalador e mandar **Abrir com Wine**:

{% include image.html src='/files/2022/08/irfanview-05-pt.jpg' %}

Inclusive a instalação você pode fazer como faria no Windows (famoso **Avançar**, **Avançar**, **Avançar**...):

{% include image.html src='/files/2022/08/irfanview-06-pt.jpg' %}

Na última tela, desmarque as duas opções para que o instalador não abra janelas por enquanto, e clique em **Done** (feito):

{% include image.html src='/files/2022/08/irfanview-07-pt.jpg' %}

De volta ao gerenciador de arquivos e à pasta **Downloads**, abra o instalador da tradução e clique em **Install** (instalar):

{% include image.html src='/files/2022/08/irfanview-08.jpg' %}

Feito isso, o IrfanView e sua tradução já estão prontos para uso.

## Iniciando o IrfanView

O Wine adiciona um ícone do IrfanView ao menu **Atividades** do GNOME, da mesma forma que o Windows adicionaria um ícone do IrfanView ao [menu Iniciar]:

{% include image.html src='/files/2022/08/irfanview-09-pt.jpg' %}

Iniciando o IrfanView pelo menu **Atividades**, ele começa sem imagem aberta:

{% include image.html src='/files/2022/08/irfanview-10.png' caption='Tela inicial do IrfanView sem nenhuma imagem aberta' %}

Normalmente, eu inicio o IrfanView de outra forma, que vou mostrar a seguir.

## Traduzindo a interface

Para aplicar a tradução, abra o menu **Options** (Opções) e clique em **Change language** (Mudar idioma):

{% include image.html src='/files/2022/08/irfanview-11.jpg' %}

Selecione a tradução para o Português e clique em **OK**:

{% include image.html src='/files/2022/08/irfanview-12.jpg' %}

Perceba que, ao fazer isso, a interface é traduzida na mesma hora.

## Abrindo uma imagem com o IrfanView

Normalmente, eu inicio o IrfanView a partir do gerenciador de arquivos, já abrindo a imagem que quero visualizar e/ou editar. Para fazer isso, localize a imagem que deseja abrir, clique com o botão direito nela e use **Abrir com outro aplicativo**:

{% include image.html src='/files/2022/08/irfanview-13-pt.jpg' %}

Dê um duplo-clique no IrfanView e a imagem será aberta com ele:

{% include image.html src='/files/2022/08/irfanview-14-pt.jpg' %}

## Continua...

No próximo artigo, veremos como usar o IrfanView para edição básica de imagens: cortar, girar, redimensionar, ajustar cores, desenhar, salvar, etc.

Siga o Linux Kamarada nas [redes sociais][social] para saber assim que esse artigo for liberado!

[Linux]:                http://www.vivaolinux.com.br/linux/
[Windows]:              https://www.microsoft.com/pt-br/windows/
[IrfanView]:            https://www.irfanview.com/
[changelog]:            https://www.irfanview.com/history_old.htm
[explorer]:             https://pt.wikipedia.org/wiki/Windows_Explorer
[Arquivos]:             https://wiki.gnome.org/Apps/Files
[GNOME]:                https://br.gnome.org/
[Wine]:                 https://www.winehq.org/
[Linux Kamarada]:       {% post_url pt/2021-12-30-linux-kamarada-15-3-ano-novo-distribuicao-nova %}
[leap-vs-tumbleweed]:   {% post_url pt/2020-12-06-opensuse-leap-e-opensuse-tumbleweed-qual-e-a-diferenca %}
[menu Iniciar]:         https://pt.wikipedia.org/wiki/Menu_Iniciar
[social]:               #about
