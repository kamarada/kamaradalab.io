---
date: 2021-03-15 11:30:00 GMT-3
image: '/files/2021/03/obs-and-gitlab.jpg'
layout: post
nickname: 'obs-and-gitlab'
title: 'Integrando Open Build Service com GitLab'
excerpt: 'Seu software compilado, empacotado e disponível para download logo após um git push!'
---

{% include image.html src="/files/2021/03/obs-and-gitlab.jpg" %}

Como vimos no texto anterior, [o Linux Kamarada está migrando para o GitLab][moving-to-gitlab]. Se você me acompanha há algum tempo, sabe que o desenvolvimento da distribuição ocorria não apenas no [GitHub], mas também no [Open Build Service (OBS)][obs]. Enquanto o [Git] gerencia o código-fonte, o OBS compila os pacotes e imagens ISO da distribuição. Eu já fiz um texto aqui explicando [como integrar o Open Build Service com o GitHub][obs-and-github]. Com a mudança do GitHub para o [GitLab], surge a dúvida: é possível integrar o Open Build Service com o GitLab?

Na verdade, antes de iniciar a migração, eu já havia feito esse teste e a resposta é: sim!

Escrevo esse texto para compartilhar como fazer isso, caso mais alguém tenha essa mesma dúvida. Vou repetir algumas informações do [texto sobre OBS e GitHub][obs-and-github] para que esse fique autocontido. Se você não leu o outro texto, não precisa lê-lo, pode ler apenas esse que vai entender tudo. Se já leu o outro texto, fique à vontade para pular o que te parece familiar.

## Conheça o Open Build Service (OBS)

Se você desenvolve _software_ para [Linux], deveria conhecer o [**Open Build Service (OBS)**][obs]. O que o torna uma ferramenta tão interessante? Extraído do seu [_site_][obs] (tradução livre minha):

> O Open Build Service (OBS) é um sistema genérico para compilar e distribuir pacotes binários a partir de códigos-fonte de forma automática, consistente e reprodutível. Você pode distribuir pacotes, assim como atualizações, _add-ons_, _appliances_ e distribuições Linux inteiras para uma vasta gama de sistemas operacionais e arquiteturas de _hardware_.

Na prática, você desenvolvedor pode enviar o código-fonte do seu _software_ para o OBS e obter pacotes prontos para instalar no [openSUSE], [Fedora], [Debian], [Ubuntu] e outras distribuições, assim como pacotes para [i586], [x86\_64], [ARM] e outras arquiteturas. Os usuários do _software_ podem baixar esses pacotes diretamente do OBS. Ele também é capaz de fazer [sistemas Live][what-is-a-livecd-dvd-usb]: você pode oferecer aos seus usuários a possibilidade de testar seu _software_ em um sistema limpo e controlado sem necessidade de instalação.

O OBS é um [_software_ livre][free-software], de modo que você pode baixá-lo, instalá-lo em um servidor próprio e usá-lo localmente. Ou (mais facil) você pode usar o servidor de referência, disponível pública e gratuitamente em [build.opensuse.org][buildoo]. Esse servidor é usado principalmente para o desenvolvimento da distribuição openSUSE, mas também hospeda vários projetos comunitários. Dentre eles, a distribuição [Linux Kamarada][kamarada-15.2].

O OBS oferece um [sistema de controle de versões][vcs] próprio, tal qual o Git, e você pode armazenar e gerenciar seu código-fonte diretamente no OBS. Ou, se você já usa o Git em um serviço como o GitLab, pode configurar o OBS para obter o código-fonte desse serviço, de modo que seu _software_ seja sempre compilado, empacotado e disponibilizado para _download_ logo após um `git push`. Você verá a seguir como configurar essa integração.

## Começando a usar o OBS

Não vou falar sobre os conceitos básicos do OBS aqui, seria assunto para um _post_ inteiro. Para aprender o básico do OBS, consulte as páginas a seguir (em inglês):

- [Documentation - Open Build Service][obs-help]
- [openSUSE:Build Service Tutorial - openSUSE Wiki][obs-tutorial]

Vou assumir que você já consegue entrar ("fazer _login_") com sua conta do openSUSE em [build.opensuse.org][buildoo]. Se você ainda não tem uma conta, crie uma clicando no _link_ **Sign Up** (inscrever-se) no topo da página.

Crie um pacote vazio para o seu _software_ no OBS. Aqui vou usar como exemplo o repositório [patterns] do Linux Kamarada no GitLab, que no momento possui apenas uma imagem PNG e um arquivo _spec_.

Se você não sabe o que é um arquivo _spec_ e/ou é novo no empacotamento [RPM], leia (em inglês):

- [Fedora RPM Guide][fedora-rpm-guide]
- [openSUSE:Packaging guidelines - openSUSE Wiki][opensuse-packaging]

Também vou assumir que você instalou o **[osc]** (o cliente de linha de comando do OBS) no seu computador. Se você usa openSUSE, pode instalar o **osc** executando:

```
# zypper install osc
```

Crie uma cópia local ("faça _checkout_") do seu projeto pessoal (_home project_) do OBS:

```
$ osc checkout home:seu_nome_de_usuario
```

Entre na pasta do pacote:

```
$ cd home:seu_nome_de_usuario/nome_do_pacote
```

Agora vamos integrar o OBS com o GitLab.

## OBS: obtendo o código-fonte do GitLab

Usando o **osc**, configure o serviço de código-fonte ([_source service_][obs-user-guide-3]) do OBS para obter o código-fonte do GitLab:

```
$ osc add https://gitlab.com/kamarada/patterns.git
```

É criado na pasta um arquivo `_service`. Abra-o e substitua seu conteúdo pelo seguinte:

```xml
<services>
    <service name="obs_scm">
        <param name="scm">git</param>
        <param name="url">https://gitlab.com/kamarada/patterns.git</param>
        <param name="revision">15.3-dev</param>
        <param name="extract">pattern-kamarada-gnome.png</param>
        <param name="extract">patterns-kamarada-gnome.spec</param>
    </service>
</services>
```

Note que estou usando o ramo (_branch_) `15.3-dev`. Se você pretende usar o ramo `master` para o seu pacote, pode simplesmente omitir o parâmetro `revision` (revisão).

Comite as alterações ("faça o _commit_" das alterações):

```
$ osc commit
```

Comitar para o OBS automaticamente dispara o serviço de código-fonte e o processo de compilação e empacotamento. Se você acessar [build.opensuse.org][buildoo], verá que seu projeto está compilando (_building_):

{% include image.html src="/files/2021/03/obs-and-gitlab-1.jpg" %}

Para que o GitLab possa disparar essas mesmas ações, você precisa gerar um _token_ de autorização ([_authorization token_][obs-user-guide-2]) usando o **osc**:

```
$ osc token --create --operation runservice home:seu_nome_de_usuario nome_do_pacote
```

O comando retornará o _token_ e sua identificação (_id_):

```xml
<status code="ok">
  <summary>Ok</summary>
  <data name="token">UmAsTrInGeNoRmEcOmSeUtOkEn</data>
  <data name="id">4321</data>
</status>
```

Como teste, você pode disparar manualmente a criação do seu pacote usando esse _token_:

```
$ osc token --trigger UmAsTrInGeNoRmEcOmSeUtOkEn
```

Se você acessar [build.opensuse.org][buildoo], verá que seu projeto está compilando.

## GitLab: avisando novos commits ao OBS

Queremos atualizar o código-fonte do nosso pacote no OBS toda vez que ele mudar no GitLab. Podemos fazer com que o GitLab dispare automaticamente essa atualização configurando um [_webhook_ do GitLab][gitlab-webhook]. Para fazer isso, execute os passos a seguir.

Entre (_sign in_) no GitLab e vá até seu repositório (no meu caso, [https://gitlab.com/kamarada/patterns](https://gitlab.com/kamarada/patterns)). Aponte para **Settings** (configurações) e clique em **Webhooks**:

{% include image.html src="/files/2021/03/obs-and-gitlab-2.jpg" %}

Forneça os seguintes parâmetros:

- **URL**: `https://build.opensuse.org/trigger/runservice?project=home:seu_nome_de_usuario&package=nome_do_pacote`
- **Secret token** (_token_ secreto): `UmAsTrInGeNoRmEcOmSeUtOkEn`
- Em **Trigger**, marque **Push events** (na verdade, já vem marcado por padrão) e informe o nome do _branch_ `15.3-dev`

{% include image.html src="/files/2021/03/obs-and-gitlab-3.jpg" %}

No final, em **SSL verification**, marque **Enable SSL verification** (na verdade, já vem marcado por padrão). Para terminar, clique no botão verde **Add webhook** (adicionar _webhook_):

{% include image.html src="/files/2021/03/obs-and-gitlab-4.jpg" %}

De volta à página anterior, você pode ver o _webhook_ adicionado:

{% include image.html src="/files/2021/03/obs-and-gitlab-5.jpg" %}

Você pode testar o _webhook_ clicando em **Test** e depois em **Push events**:

{% include image.html src="/files/2021/03/obs-and-gitlab-6.jpg" %}

Se você acessar [build.opensuse.org][buildoo], verá que seu projeto está compilando.

A partir de agora, cada `git push` para o GitLab irá disparar automaticamente uma reconstrução do pacote no OBS. Seu nome de usuário aparecerá no histórico do código-fonte do OBS, que pode ser verificado com o comando `osc log`.

Divirta-se muito! (_Have a lot of fun!_)

## Referências

- [How to integrate external SCM sources - OBS User Guide][obs-user-guide-1]
- [Authorization - OBS User Guide][obs-user-guide-2]
- [Using Source Services - OBS User Guide][obs-user-guide-3]

[moving-to-gitlab]:         {% post_url pt/2021-03-10-linux-kamarada-esta-migrando-para-o-gitlab %}
[github]:                   https://github.com/
[obs]:                      https://openbuildservice.org/
[git]:                      https://git-scm.com/
[obs-and-github]:           {% post_url pt/2019-07-23-integrando-open-build-service-com-github %}
[GitLab]:                   https://gitlab.com/
[linux]:                    http://www.vivaolinux.com.br/linux/
[opensuse]:                 https://www.opensuse.org/
[fedora]:                   https://getfedora.org/
[debian]:                   https://www.debian.org
[ubuntu]:                   https://www.ubuntu.com/
[i586]:                     https://pt.wikipedia.org/wiki/X86
[x86_64]:                   https://pt.wikipedia.org/wiki/AMD64
[arm]:                      https://pt.wikipedia.org/wiki/Arquitetura_ARM
[what-is-a-livecd-dvd-usb]: {% post_url pt/2015-11-25-o-que-e-um-livecd-um-livedvd-um-liveusb %}
[free-software]:            https://www.gnu.org/philosophy/free-sw.pt-br.html
[buildoo]:                  https://build.opensuse.org
[kamarada-15.2]:            {% post_url pt/2020-09-11-linux-kamarada-15.2-venha-para-o-lado-verde-elegante-e-moderno-da-forca %}
[vcs]:                      https://pt.wikipedia.org/wiki/Sistema_de_controle_de_versões
[obs-help]:                 https://openbuildservice.org/help/
[obs-tutorial]:             https://en.opensuse.org/openSUSE:Build_Service_Tutorial
[patterns]:                 https://gitlab.com/kamarada/patterns/-/tree/15.3-dev
[rpm]:                      https://pt.wikipedia.org/wiki/RPM_(software)
[fedora-rpm-guide]:         http://docs.fedoraproject.org/en-US/Fedora_Draft_Documentation/0.1/html/RPM_Guide/index.html
[opensuse-packaging]:       https://en.opensuse.org/openSUSE:Packaging_guidelines
[osc]:                      https://linux.die.net/man/1/osc
[gitlab-webhook]:           https://docs.gitlab.com/ee/user/project/integrations/webhooks.html
[obs-user-guide-1]:         https://openbuildservice.org/help/manuals/obs-user-guide/cha.obs.best-practices.scm_integration.html
[obs-user-guide-2]:         https://openbuildservice.org/help/manuals/obs-user-guide/cha.obs.authorization.token.html
[obs-user-guide-3]:         https://openbuildservice.org/help/manuals/obs-user-guide/cha.obs.source_service.html
