---
date: '2021-10-26 22:41:00 GMT-3'
image: '/files/2021/10/postfix-smtp-relay.jpg'
layout: post
published: true
nickname: 'mailx'
title: 'Como enviar e-mails a partir de shell scripts com o comando mailx'
---

{% include image.html src='/files/2021/10/postfix-smtp-relay.jpg' %}

O _e-mail_ é um meio de comunicação bastante comum nos dias de hoje. A maioria das pessoas tem conta em algum serviço de _e-mail_ gratuito como [Gmail], [Yahoo], [Outlook] ou [alternativos][gmail-alternatives] e os usa a partir do navegador (pelo _webmail_) ou de algum aplicativo pra _smartphone_. Há quem use clientes de _e-mail_ para _desktop_, como o [Evolution] ou o [Thunderbird]. Mas há também cliente de _e-mail_ para o terminal, como o **[mailx]** e o [Mutt].

[Gmail]: https://www.google.com/gmail/
[Yahoo]: https://br.mail.yahoo.com/
[Outlook]: https://outlook.live.com/
[gmail-alternatives]: https://pt.vpnmentor.com/blog/5-otimas-alternativas-para-o-gmail/
[Evolution]: {% post_url pt/2017-09-05-conecte-o-evolution-ao-office-365 %}
[Thunderbird]: {% post_url pt/2018-09-12-sincronizando-o-gmail-no-thunderbird %}
[mailx]: http://heirloom.sourceforge.net/mailx.html
[Mutt]: http://www.mutt.org

Não imagino que alguém curta digitar comandos para enviar _e-mails_ (embora o Mutt com sua interface [ncurses] possa quebrar um galho quando, por qualquer motivo, a interface gráfica não está disponível). Mas comandos como o **mailx** podem ser úteis quando você quer enviar um _e-mail_ durante a execução de um _script_, geralmente agendado.

[ncurses]: https://pt.wikipedia.org/wiki/Ncurses

Há vários exemplos de usos: avisar que um _script_ foi executado com sucesso ou não, e informar o resultado ou uma mensagem de erro; alertar que um _script_ de monitoramento detectou algo (como que o espaço livre em disco está pouco); informar que um _script_ de _backup_ foi concluído, possivelmente até mesmo enviando algum arquivo em anexo; etc.

Nesse tutorial, você verá algumas formas possíveis de enviar _e-mails_ pelo terminal (e a partir de _shell scripts_) usando o **mailx**, que já vem instalado por padrão no [openSUSE Leap][leap] e pode ser facilmente instalado no [Linux Kamarada][kamarada-15.2].

[leap]: {% post_url pt/2020-12-06-opensuse-leap-e-opensuse-tumbleweed-qual-e-a-diferenca %}
[kamarada-15.2]: {% post_url pt/2020-09-11-linux-kamarada-15.2-venha-para-o-lado-verde-elegante-e-moderno-da-forca %}

O [Heirloom **mailx**][mailx] (antes chamado de **nail**) é um cliente de _e-mail_ com interface de linha de comando para sistemas baseados no [Unix], como o [Linux]. Ele mantém compatibilidade em grande parte (mas não totalmente) com o cliente de _e-mail_ original do Unix, o **mail**, ao mesmo tempo em que adiciona algumas funcionalidades. É [_software_ livre][free-sw] e é empacotado para fácil instalação por várias distribuições Linux.

[Unix]: https://pt.wikipedia.org/wiki/Unix
[Linux]: https://www.vivaolinux.com.br/linux/
[free-sw]: https://www.gnu.org/philosophy/free-sw.html

O passo a passo a seguir foi testado no [openSUSE Leap 15.3][leap-15.3] e no [Linux Kamarada 15.2][kamarada-15.2].

[leap-15.3]: {% post_url pt/2021-06-02-opensuse-leap-153-constroi-um-caminho-para-a-versao-empresarial %}

Para variar, vou usar um [serviço de _e-mail_ alternativo ao Gmail][gmail-alternatives], o [Zoho Mail]. Criei uma conta nesse serviço só para enviar _e-mails_ a partir de _scripts_ para meu _e-mail_ pessoal.

[Zoho Mail]: https://www.zoho.com/pt-br/mail/

## 1) Envio de e-mail com MTA local

A forma recomendada de enviar _e-mails_ a partir de _shell scripts_ é com o auxílio de um MTA local, como explicado no tutorial:

- [Postfix como relay SMTP no openSUSE com auxílio do YaST][postfix-smtp-relay]

[postfix-smtp-relay]: {% post_url pt/2021-10-26-postfix-como-relay-smtp-no-opensuse-com-auxilio-do-yast %}

Os exemplos a seguir consideram que o Postfix foi configurado conforme esse tutorial.

O mínimo que você precisa digitar para invocar o comando [**mailx**][mailx-man] para enviar um _e-mail_ é:

[mailx-man]: http://heirloom.sourceforge.net/mailx/mailx.1.html

```
$ mailx -r "seunomedeusuario@zohomail.com" "emaildodestinatario@exemplo.com"
```

A opção `-r` deve vir acompanhada do endereço de _e-mail_ do remetente (o seu) e o endereço de _e-mail_ do destinatário deve ser o último argumento.

O **mailx** pergunta o assunto (_subject_) da mensagem:

```
Subject:
```

Digite o assunto da mensagem e tecle **Enter**. Na sequência, digite o corpo da mensagem:

```
Subject: Assunto do e-mail
Corpo do e-mail
```

Aqui você pode digitar à vontade, inclusive teclando **Enter** para iniciar novas linhas quantas vezes precisar. Quando terminar de digitar o _e-mail_, tecle **Ctrl + D**. Com isso, você vai enviar o sinal de [EOT] (_end-of-transmission_, "fim da transmissão") para o **mailx**, que encerrará.

[EOT]: https://en.wikipedia.org/wiki/End-of-Transmission_character

Seu terminal agora deve estar assim:

```
$ mailx -r "seunomedeusuario@zohomail.com" "emaildodestinatario@exemplo.com"
Subject: Assunto do e-mail
Corpo do e-mail
EOT
```

Se tudo deu certo, dentro de alguns instantes o destinatário já terá recebido o _e-mail_:

{% include image.html src='/files/2021/10/mailx-01-pt.png' %}

Você pode conseguir o mesmo efeito com apenas um comando, combinando **[echo]** para informar o corpo da mensagem e a opção `-s` para informar o assunto:

[echo]: https://man7.org/linux/man-pages/man1/echo.1.html

```
$ echo "Corpo do e-mail" | mailx -r "seunomedeusuario@zohomail.com" -s "Assunto do e-mail" "emaildodestinatario@exemplo.com"
```

Com esse comando, o **mailx** executa imediatamente e não espera você digitar mais nada. Esse comando já pode ser inserido em um _shell script_.

Mais algumas opções que você pode adicionar ao comando **mailx**:

- **Cópia:** opção `-c`
- **Cópia oculta:** opção `-b`
- **Anexo:** opção `-a` (pode usar mais de uma vez, se precisar enviar mais de um anexo)

Exemplo:

```
$ echo "Corpo do e-mail" | mailx -r "seunomedeusuario@zohomail.com" -s "Assunto do e-mail" -c "emaildacopia@exemplo.com" -b "emaildacopiaoculta@exemplo.com" -a backup.zip "emaildodestinatario@exemplo.com"
```

Veja a seguir um exemplo de _script_ que você pode fazer com o comando **mailx**:

```sh
#!/bin/bash

DE="seunomedeusuario@zohomail.com"
PARA="emaildodestinatario@exemplo.com"
CC="emaildacopia@exemplo.com"
CCO="emaildacopiaoculta@exemplo.com"
ASSUNTO="Assunto do e-mail"
CORPO="Corpo do e-mail"
ANEXO="backup.zip"

echo "$CORPO" | mailx -r "$DE" -s "$ASSUNTO" -c "$CC" -b "$CCO" -a "$ANEXO" "$PARA"
```

Se você executar esse _script_ e der tudo certo, dentro de alguns instantes os destinatários já terão recebido o _e-mail_:

{% include image.html src='/files/2021/10/mailx-02-pt.png' %}

## 2) Envio de e-mail sem MTA local

Você pode usar o **mailx** apenas, sem auxílio de nenhum outro _software_.

Nesse caso, você deve passar as configurações da conexão SMTP para o próprio **mailx**.

Uma alternativa seria acrescentar essas configurações no _script_:

```sh
#!/bin/bash

SERVIDOR="smtp://smtp.zoho.com:587"
SENHA="SuaSenha"

DE="seunomedeusuario@zohomail.com"
PARA="emaildodestinatario@exemplo.com"
CC="emaildacopia@exemplo.com"
CCO="emaildacopiaoculta@exemplo.com"
ASSUNTO="Assunto do e-mail"
CORPO="Corpo do e-mail"
ANEXO="backup.zip"

echo "$CORPO" | mailx -S smtp="$SERVIDOR" \
-S smtp-auth-user="$DE" \
-S smtp-auth-password="$SENHA" \
-S smtp-use-starttls \
-r "$DE" -s "$ASSUNTO" -c "$CC" -b "$CCO" -a "$ANEXO" "$PARA"
```

(note que eu dividi o comando **mailx** em algumas linhas para melhor legibilidade)

Mas não é recomendado deixar uma senha em um arquivo de texto simples como um _script_, principalmente se você vai expor esse arquivo publicamente, por exemplo, em um [sistema de controle de versão][scm], como o [Git].

[scm]: https://pt.wikipedia.org/wiki/Sistema_de_controle_de_versões
[Git]: https://git-scm.com/

O melhor é escrever as configurações da conexão SMTP no arquivo de configuração do **mailx**, o `.mailrc`, na sua pasta pessoal. Crie-o com o seu editor de texto preferido, por exemplo:

```
$ vim ~/.mailrc
```

Ou:

```
$ gedit ~/.mailrc
```

E copie e cole nele o seguinte conteúdo:

```sh
set smtp=smtp://smtp.zoho.com:587
set smtp-auth-user=seunomedeusuario@zohomail.com
set smtp-auth-password=SuaSenha
set smtp-use-starttls
```

Note que esse arquivo também é um arquivo de texto simples. Para torná-lo um pouco mais seguro, você pode ajustar suas permissões para que apenas seu usuário possa acessá-lo:

```
$ chmod 600 ~/.mailrc
```

E não exponha esse arquivo publicamente, mantenha-o apenas na sua pasta pessoal.

Feito isso, você pode voltar o _script_ à versão anterior, sem as configurações de conexão.

## Conclusão

{% capture atualizacao %}

Quando você envia _e-mails_ usando um cliente de _e-mail_, como estamos fazendo nesse tutorial, o Zoho Mail guarda uma cópia dos _e-mails_ enviados na pasta **Enviados**, mas ele permite desativar isso. Caso você não queira guardar cópias dos _e-mails_ enviados por _scripts_, faça o seguinte:

1. Faça _login_ no [Zoho Mail](https://www.zoho.com/pt-br/mail/login.html)
2. Acesse as **Configurações**
3. Vá em **Contas de e-mail**, depois selecione a aba **SMTP**
4. Desmarque a opção **Salvar uma cópia dos e-mails enviados**
5. Clique em **Salvar**

Execute o _script_ novamente e verá que o _e-mail_ será entregue ao destinatário, mas uma cópia dele não será mais salva na pasta **Enviados** do seu Zoho Mail.

{% endcapture %}

{% include update.html date="29/11/2021" message=atualizacao %}

É isso! Espero que esse tutorial possa ter te ajudado. Se você é administrador de sistemas e/ou de redes, certamente vai fazer muitos _scripts_ como esses.

O **mailx** não é o único programa que pode ser usado para enviar _e-mails_ a partir de _shell scripts_. Existem [algumas alternativas][tecadmin]. Como eu disse, decidi usá-lo pela conveniência, por já vir instalado no [openSUSE].

[tecadmin]: https://tecadmin.net/ways-to-send-email-from-linux-command-line/
[openSUSE]: https://www.opensuse.org/

Se ficou com alguma dúvida ou tiver algo a sugerir, escreva nos comentários.

Até a próxima!

## Referências

- [Sending Email Using mailx/s-nail in Linux Through Gmail SMTP - SysTutorials][systutorials]
- [How to send email with attachments from command line \| SUSEGEEK][susegeek]
- [email - Difference between mail and mailx? - Unix & Linux Stack Exchange][stackexchange]

[systutorials]: https://www.systutorials.com/sending-email-from-mailx-command-in-linux-using-gmails-smtp/
[susegeek]: http://www.susegeek.com/internet-browser/how-to-send-email-with-attachments-from-command-line/
[stackexchange]: https://unix.stackexchange.com/a/89531
