---
date: '2021-12-23 00:15:00 GMT-3'
layout: post
published: true
title: 'Linux Kamarada e openSUSE Leap: como atualizar da versão 15.2 para a 15.3'
image: /files/2021/12/upgrade-to-15.3.jpg
nickname: 'upgrade-to-15.3'
---

{% include image.html src="/files/2021/12/upgrade-to-15.3.jpg" %}

Se você acompanha esse _site_ há um tempo, sabe que eu sempre faço um tutorial mostrando como atualizar a distribuição [openSUSE Leap][opensuse-leap] de uma versão para outra.&nbsp;

Da [última vez][upgrade-to-15.2], tivemos a estreia do [Linux Kamarada][kamarada-15.3-beta], que, caso você ainda não conheça, é uma distribuição [Linux] baseada no openSUSE Leap. Na [última edição desse tutorial][upgrade-to-15.2], eu mostrei como atualizar do [openSUSE Leap 15.1] para o [openSUSE Leap 15.2], e ao mesmo tempo como atualizar do [Linux Kamarada 15.1] para o [Linux Kamarada 15.2]. Os mesmos números de versão indicam o alinhamento entre as distribuições e o procedimento para atualizá-las é bem parecido, tanto que ambas cabem no mesmo tutorial.

Hoje, o openSUSE Leap está na versão [15.3][leap-15.3], lançada em [junho de 2021][leap-15.3]. A versão anterior, a [15.2][openSUSE Leap 15.2], foi lançada em [julho de 2020][openSUSE Leap 15.2] e, de acordo com a [_wiki_ do openSUSE][lifetime], terá seu suporte encerrado em 31 de dezembro de 2021. Portanto, é recomendado a usuários do openSUSE Leap 15.2 atualizarem para a versão 15.3.

O Linux Kamarada está na versão [15.2][Linux Kamarada 15.2], lançada em [setembro de 2020][Linux Kamarada 15.2]. O Linux Kamarada 15.3 será lançado nos próximos dias. Por enquanto, temos a versão [15.3 Beta][kamarada-15.3-beta], que já apresenta uma boa estabilidade. Nos meus testes, a atualização do Linux Kamarada 15.2 Final para o 15.3 Beta ocorreu sem problemas.

Como o Linux Kamarada 15.3 ainda não foi finalizado, não recomendo a atualização para todos os usuários. Mas, se você se sente "corajoso" e quer ajudar no desenvolvimento, testando a atualização e reportando quaisquer problemas, por favor, sinta-se à vontade.

Atualizar (fazer o _upgrade_) de uma versão para outra no openSUSE Leap é um processo fácil e tranquilo. Eu sigo a "receita de bolo" que aprendi na [_wiki_ do openSUSE][system-upgrade]. É praticamente a mesma "receita" desde 2012, quando comecei a usar o openSUSE. Como o Linux Kamarada é baseado no openSUSE Leap, sua atualização segue a mesma "receita", a única diferença é a configuração de mais um repositório, que é o do Linux Kamarada.

Caso você tenha curiosidade de conferir as edições anteriores desse tutorial:

- [Como atualizar do openSUSE Leap 42.1 para o 42.2][upgrade-to-42.2]
- [Como atualizar do openSUSE Leap 42.2 para o 42.3][upgrade-to-42.3]
- Eu não escrevi sobre a atualização do openSUSE Leap 42.3 para o 15.0
- [Como atualizar do openSUSE Leap 15.0 para o 15.1][upgrade-to-15.1]
- [Linux Kamarada e openSUSE Leap: como atualizar da versão 15.1 para a 15.2][upgrade-to-15.2]

A seguir, você verá como atualizar o openSUSE Leap e o Linux Kamarada da versão 15.2 para a 15.3.

**Observação:** para não ficar repetitivo, o que eu falo sobre o openSUSE Leap se aplica às duas distribuições, o que eu falo sobre o Linux Kamarada é específico deste.

## Algumas recomendações

A atualização da distribuição (_upgrade_) no openSUSE Leap é um procedimento seguro, que deve funcionar para a maioria dos casos, mas que requer alguns cuidados. É sempre bom relembrar as recomendações a seguir.

**Você deve estar usando a versão imediatamente anterior do openSUSE Leap.** Isso quer dizer que se você pretende atualizar para a versão 15.3, deve estar usando agora a versão 15.2. Saltos de versões não são suportados. Podem funcionar, mas não há garantia. Portanto, se agora você está usando a versão 15.1, por exemplo, é melhor que primeiro [atualize para a versão 15.2][upgrade-to-15.2], para então atualizar para a versão 15.3.

**Sua instalação do openSUSE Leap deve estar atualizada** com as versões mais recentes dos pacotes lançadas até o momento. Observe que há dois tipos de atualização: neste *post*, você vai ver como atualizar a distribuição (*upgrade*), mas antes você deve atualizar os pacotes (*update*). Para mais informações sobre os dois tipos de atualizações e como atualizar os pacotes, consulte o *post*:

- [Como obter atualizações para o Linux openSUSE][howto-update]

**Você deve se informar sobre a versão do openSUSE Leap que vai instalar.** Consulte as [notas de lançamento][release-notes], que descrevem o que muda na nova versão e listam alguns *bugs* conhecidos, como preveni-los e/ou solucioná-los.

**Você deve fazer uma cópia de segurança (*backup*) de arquivos importantes.** apesar de a atualização do openSUSE ser segura (fazendo lembrar, para quem conhece, a atualização do [Debian]), não quer dizer que seja perfeita. Ganhar na loteria é difícil, mas uma hora alguém ganha. Algo dar errado na atualização é difícil, especialmente se você toma todos os cuidados, mas pode acontecer. Eu, particularmente, já atualizei algumas vezes sem problema. Nunca é demais fazer uma cópia dos seus arquivos pessoais por precaução, especialmente se a pasta `/home` não está em uma partição reservada. Se o computador não pode ficar parado por muito tempo (um servidor, por exemplo), considere fazer uma cópia de todo o sistema, a fim de restaurá-la imediatamente caso algo não funcione como esperado.

**Você não deve usar repositórios não oficiais (de terceiros) durante a atualização.** Antes de começar a atualização (*upgrade*), removeremos os [repositórios de terceiros][third-party-repos]. É possível que isso ocasione a remoção também de programas de terceiros. Você pode devolvê-los depois. Isso não quer dizer que não seja possível atualizar com esses repositórios presentes, mas usar apenas os [repositórios oficiais][official-repos] é mais garantido. A base do sistema será atualizada para uma nova base estável, sólida, confiável. Depois, sobre essa base, você poderá instalar o que quiser. Faça diferente dessa recomendação apenas se souber o que está fazendo.

Recomendo também que você leia toda essa página antes de começar de fato a agir. Para organizá-la melhor, vou dividi-la em 6 etapas. Conhecendo todo o processo, você terá condição de agendá-lo melhor. Você pode até fazer outras coisas usando o computador enquanto segue o passo a passo, mas depois que começar a atualização (*upgrade*), só poderá usar o computador quando ela for concluída.

## 1) Atualização dos pacotes (*update*)

Certifique-se de que sua instalação do openSUSE Leap está atualizada. Para isso, execute o comando `zypper up` como usuário _root_ (administrador). Ele deve informar que não há o que fazer:

{% include image.html src="/files/2021/12/upgrade-01-pt.jpg" %}

Note que, no caso do Linux Kamarada 15.2, temos um problema ao atualizar com o pacote **[virtualbox-kmp-default]**, que apresenta conflito, impedindo a atualização completa.

Esse problema já foi reportado por alguns usuários no [grupo de usuários do Telegram][telegram] e também pelo [SlackJeff], na [_review_ que ele fez do Linux Kamarada 15.2][SlackJeff].

**Apenas** se você **não** está usando o Linux Kamarada em uma máquina virtual do [VirtualBox], é seguro remover esse pacote. Para isso, você pode executar o seguinte comando:

```
# zypper rm virtualbox-kmp-default
```

Mas seja qual for o seu caso, **recomendo deixar** esse pacote instalado e seguir normalmente com a atualização (_upgrade_). Nos meus testes, isso resolveu o problema.

Para mais informações sobre atualização de pacotes (_update_), consulte o *post*:

- [Como obter atualizações para o Linux openSUSE][howto-update]

## 2) *Backup* dos repositórios atuais

Essa etapa, na verdade, é opcional.

Talvez você queira fazer *backup* da sua lista atual de repositórios para devolvê-la depois da atualização. O openSUSE Leap guarda as configurações de repositórios na pasta `/etc/zypp/repos.d`, sendo um arquivo de texto para cada repositório.

Vamos copiar a pasta `repos.d`, em `/etc/zypp`, para outra no mesmo lugar, chamada `repos.d.antigos`. Para isso, usaremos a interface de linha de comando.

Se não já o fez, alterne do seu usuário para o usuário *root* executando o comando:

```
$ su
```

Forneça a senha do usuário *root* para continuar.

Se você já fez alguma atualização de versão seguindo alguma edição anterior desse mesmo tutorial, talvez ainda tenha o *backup* antigo guardado. Se esse é o seu caso, execute o comando a seguir para excluir a pasta `repos.d.antigos`:

```
# rm -rf /etc/zypp/repos.d.antigos
```

Então, ordene a cópia:

```
# cp -Rv /etc/zypp/repos.d /etc/zypp/repos.d.antigos
```

{% include image.html src="/files/2021/12/upgrade-02-pt.jpg" %}

Se você ainda não fez *backup* dos seus arquivos pessoais e/ou do seu sistema, considere fazê-lo agora.

## 3) Faxina dos repositórios

Como expliquei antes, durante a atualização usaremos apenas os repositórios oficiais. Mais especificamente, apenas dois deles, no caso do openSUSE Leap:

- **Main Repository** (repositório principal): contém apenas [*softwares* de código aberto][oss] (do inglês *open source sofware*, OSS).

URL: `http://download.opensuse.org/distribution/leap/$releasever/repo/oss/`

URL calculada para o 15.2: [http://download.opensuse.org/distribution/leap/15.2/repo/oss/](http://download.opensuse.org/distribution/leap/15.2/repo/oss/)

- **Main Update Repository** (repositório de atualização principal): contém atualizações (*updates*) oficiais para os pacotes do repositório principal.

URL: `http://download.opensuse.org/update/leap/$releasever/oss/`

URL calculada para o 15.2: [http://download.opensuse.org/update/leap/15.2/oss/](http://download.opensuse.org/update/leap/15.2/oss/)

Note que o sistema automaticamente substitui a variável `$releasever` pela versão da distribuição sendo usada no momento. Essa variável já é usada por padrão em novas instalações do Linux Kamarada desde a versão 15.2. Se você já usava a versão 15.1, mas atualizou para a versão 15.2 seguindo as orientações do [tutorial][upgrade-to-15.2], também configurou seu sistema para usá-la.

No caso do Linux Kamarada, além dos repositórios acima, usaremos também o repositório oficial do **Linux Kamarada**, que é gentilmente hospedado pela [OSDN] em espelhos (_mirrors_) em diversos países. Se você está no Brasil, use o espelho da [C3SL (UFPR)][c3sl].

URL: `http://c3sl.dl.osdn.jp/storage/g/k/ka/kamarada/$releasever/openSUSE_Leap_$releasever/`

URL calculada para o 15.2: [http://c3sl.dl.osdn.jp/storage/g/k/ka/kamarada/15.2/openSUSE_Leap_15.2/](http://c3sl.dl.osdn.jp/storage/g/k/ka/kamarada/15.2/openSUSE_Leap_15.2/)

Se você está em outro país, consulte a [lista de espelhos na _wiki_ do Linux Kamarada no GitLab][mirrors].

Vamos fazer uma faxina nos repositórios, excluindo quaisquer outros repositórios configurados (vamos excluir inclusive quaisquer repositórios de terceiros).

Abra o [Centro de Controle do YaST][yast]. Para isso, clique em **Mostrar aplicativos**, no canto inferior esquerdo da tela, e clique no ícone do **YaST**, no final da lista de aplicativos:

{% include image.html src="/files/2021/12/upgrade-03-pt.jpg" %}

Será solicitada a você a senha do usuário *root*. Digite-a e clique em **Continuar**:

{% include image.html src="/files/2021/12/upgrade-04-pt.jpg" %}

Dentro da categoria **Software**, a primeira, clique no item **Repositórios de software**:

{% include image.html src="/files/2021/12/upgrade-05-pt.jpg" %}

Você será apresentado à lista de repositórios do seu sistema:

{% include image.html src="/files/2021/12/upgrade-06-pt.jpg" %}

Para cada repositório que não seja um dos listados acima, selecione o repositório na lista e clique em **Remover**.

Aparecerá uma mensagem de confirmação. Clique em **Sim**:

{% include image.html src="/files/2021/12/upgrade-07-pt.jpg" %}

Faça isso com todos os repositórios até que só sobrem os repositórios listados acima:

{% include image.html src="/files/2021/12/upgrade-08-pt.jpg" %}

Esses repositórios podem estar com nomes diferentes no seu computador. Se esse for o caso, você pode se orientar pela [URL] em vez do nome. Se não encontrá-los, não tem problema: adicione-os, informando as URLs acima.

## 4) Repositórios da nova versão

Nessa etapa, em edições anteriores desse tutorial, editávamos os repositórios um a um, substituindo a versão anterior da distribuição (15.2) pela versão nova (15.3). Agora que temos a variável `$releasever`, isso não é mais necessário.

Apenas selecione cada um dos seus repositórios e confira se seu **URL Bruto** contém a variável `$releasever`:

{% include image.html src="/files/2021/12/upgrade-09-pt.jpg" %}

Caso a versão da distribuição (no momento, `15.2`) apareça em algum trecho do **URL Bruto**, clique em **Editar**. Na tela que aparece, substitua essa versão por `$releasever` e clique em **OK**:

{% include image.html src="/files/2021/12/upgrade-10-pt.jpg" %}

No caso do **Linux Kamarada**, observe que a variável `$releasever` aparece duas vezes:

{% include image.html src="/files/2021/12/upgrade-11-pt.jpg" %}

Ao final, sua lista de repositórios deve estar assim:

{% include image.html src="/files/2021/12/upgrade-12-pt.jpg" %}

Clique em **OK** para gravar as alterações. Pode fechar o YaST.

## 5) *Download* dos pacotes

Já podemos começar a baixar os pacotes da nova versão do openSUSE Leap.

De volta à interface de linha de comando, baixe a lista de pacotes dos novos repositórios (note que vamos usar a opção `--releasever=15.3` para indicar o uso da versão nova):

```
# zypper --releasever=15.3 ref
```

Depois, execute o comando:

```
# zypper --releasever=15.3 dup --download-only --allow-vendor-change
```

(*distribution upgrade*, atualização da distribuição, *download only*, apenas baixar, *allow vendor change*, permitir [mudanças de fornecedor][vendor-change])

{% include image.html src="/files/2021/12/upgrade-13-pt.jpg" %}

Se você já se guiou por edições anteriores desse tutorial, vai perceber a adição da opção `--allow-vendor-change`. Isso se deve ao fato de o openSUSE Leap 15.3 compartilhar pacotes RPM com o [SUSE Linux Enterprise 15 SP3][sle-15-sp3] (não apenas o código-fonte, como nas versões anteriores, mas até mesmo os pacotes binários, já compilados). Por isso, o gerenciador de pacotes **[zypper]** acusaria muitas [mudanças de fornecedor][vendor-change] durante a atualização. Para aceitá-las de antemão é que adicionamos a opção `--allow-vendor-change`.

O gerenciador de pacotes **zypper** passa um tempo "pensando" e logo depois mostra o que precisa fazer para atualizar o openSUSE Leap para a próxima versão.

É possível que ele precise mudar alguns pacotes, como na imagem abaixo:

{% include image.html src="/files/2021/12/upgrade-14-pt.jpg" %}

Se isso acontecer, leia atentamente as opções apresentadas e escolha uma delas.

Nesse caso, o **zypper** deve substituir o pacote **[distribution-logos-openSUSE-Leap]** pelo pacote **[distribution-logos-kamarada]**. Portanto, nesse caso, você deve escolher a **Solução 1**. Para isso, digite `1` e tecle **Enter**.

O **zypper** lista as alterações que fará no sistema (quais pacotes serão atualizados, quais serão removidos, quais mudarão de fornecedor, etc) e pergunta se você deseja continuar:

{% include image.html src="/files/2021/12/upgrade-15-pt.jpg" %}

Note que essa lista de ações é semelhante à produzida pelo comando `zypper up`, apresentado no [*post* sobre atualização de pacotes][howto-update]. No entanto, como agora estamos fazendo uma atualização de distribuição, a lista do que precisa ser feito é bem maior.

Confira essa lista com cuidado. Você pode ir para cima ou para baixo usando a barra de rolagem à direita da tela ou a roda (*scroll*) do *mouse*, se utiliza o Terminal, ou as combinações de teclas **Shift + Page Up** ou **Shift + Page Down**, se está em uma interface puramente textual (elas também funcionam no Terminal).

A opção padrão é continuar (**s**). Se você concorda, pode apenas teclar **Enter**.

Aqui temos mais um detalhe da atualização do Linux Kamarada 15.2 para o 15.3:

{% include image.html src="/files/2021/12/upgrade-16-pt.jpg" %}

Como o Linux Kamarada 15.3 inclui o _driver_ **[nouveau]**, o **zypper** apresenta sua licença de uso em um paginador estilo o **[less]**. Use a tecla **Enter** ou a **barra de espaço** para ler a licença até o final. Quando terminar, tecle **q** para sair do paginador:

{% include image.html src="/files/2021/12/upgrade-17-pt.jpg" %}

Perguntado se você concorda com os termos da licença, digite `sim` e tecle **Enter**:

{% include image.html src="/files/2021/12/upgrade-18-pt.jpg" %}

Feito isso, o *download* dos novos pacotes começará. Enquanto aguarda, você pode usar seu computador normalmente. A atualização propriamente dita ainda não começou.

Observe que o **zypper** apenas baixa os pacotes, ele não inicia a atualização:

{% include image.html src="/files/2021/12/upgrade-19-pt.jpg" %}

Isso porque ordenamos a ele por ora apenas baixar os pacotes (opção `--download-only`).

Termine o que está fazendo e salve os arquivos abertos para iniciarmos a atualização da distribuição. Note que ela pode levar alguns (vários) minutos e você só poderá voltar a usar o computador quando ela for concluída.

## 6) Atualização da distribuição (*upgrade*)

Agora que já baixamos os pacotes necessários para atualizar o openSUSE Leap da versão 15.2 para a 15.3, vamos sair da interface gráfica e usar a linha de comando para realizar a instalação. Aqui, você não poderá usar o Terminal.

Isso é necessário porque inclusive a própria interface gráfica será atualizada. Se estivermos usando a interface gráfica durante a atualização, pode ser que o sistema trave no meio da processo e as consequências disso são imprevisíveis.

Considere abrir essa página em outro computador, em um *smartphone* ou *tablet*, imprimi-la ou anotar o que vai fazer.

Se o sistema que você vai atualizar está em um *notebook*, certifique-se de que ele esteja com a bateria completamente carregada e conectado à fonte de alimentação. Não desconecte a bateria ou a fonte durante a atualização. Vamos prevenir qualquer possibilidade de problema.

Encerre sua sessão (*logout*). Para isso, clique no **menu do sistema**, no canto superior direito da tela, clique no seu nome de usuário e escolha a opção **Encerrar sessão**:

{% include image.html src="/files/2021/12/upgrade-20-pt.jpg" %}

Você será apresentado à tela de *login*, onde você poderia informar seu nome de usuário e senha caso quisesse iniciar uma nova sessão na interface gráfica:

{% include image.html src="/files/2021/12/upgrade-21-pt.jpg" %}

Mas não é isso que queremos. Aperte a combinação de teclas **Ctrl + Alt + F1** para alternar para uma interface puramente textual:

{% include image.html src="/files/2021/12/upgrade-22.png" %}

Caso isso seja novidade para você, saiba que o Linux disponibiliza seis [consoles][terminal] (terminais) além da interface gráfica. Você pode usar as teclas de **F1** a **F6** na mesma combinação (**Ctrl + Alt + F1**, **Ctrl + Alt + F2** e assim por diante) para alternar entre os consoles, assim como pressionar **Ctrl + Alt + F7** para retornar à interface gráfica.

Vamos permanecer no primeiro console.

Entre com o usuário *root*. Para isso, digite `root` e tecle **Enter**. Depois, digite a senha do usuário *root* e tecle **Enter**.

Vamos alternar do nível de execução (_[runlevel]_) 5, nível padrão, no qual o sistema nos provê interface gráfica, para o nível de execução 3, no qual temos apenas a interface de linha de comando e conexão de rede.

Para [alterar o nível de execução][switch-runlevel] para 3, execute o comando:

```
# init 3
```

Finalmente, vamos realizar a atualização de distribuição propriamente dita. Para isso, execute o comando (se atente, também aqui, às mesmas opções que usamos antes):

```
# zypper --no-refresh --releasever=15.3 dup --allow-vendor-change
```

{% include image.html src="/files/2021/12/upgrade-23.png" %}

A opção `--no-refresh` faz com que o **zypper** não atualize a lista de pacotes dos repositórios. Com isso, garantimos que ele não tente baixar mais algum pacote além dos que nós já baixamos. Isso pode ser útil especialmente em *notebooks*, que podem perder a conexão com a rede Wi-Fi ao sair da interface gráfica.

Como antes, o **zypper** vai processar a atualização (se ele fizer as mesmas perguntas sobre mudanças e licenças, responda da mesma forma) e mostrar o que vai fazer:

{% include image.html src="/files/2021/12/upgrade-24-pt.png" %}

Note que todos os pacotes que ele precisa já foram baixados: ao final ele mostra "Tamanho total do download: 0 B. Já em cache: 2,23 GiB."

Apenas tecle **Enter** para que a atualização comece.

O **zypper** pula o _download_ dos pacotes e já vai direto para a instalação:

{% include image.html src="/files/2021/12/upgrade-25-pt.png" %}

A atualização da distribuição pode demorar alguns (vários) minutos.

Quando ela terminar, o **zypper** vai sugerir reiniciar. É o que vamos fazer executando:

```
# reboot
```

{% include image.html src="/files/2021/12/upgrade-26-pt.png" %}

## Pronto

Se você conseguiu fazer tudo até aqui, seu computador já está com o Linux Kamarada 15.3 (ou com o openSUSE Leap 15.3) e ele já está pronto para uso:

{% include image.html src="/files/2021/12/upgrade-27-pt.jpg" %}

Verifique se está tudo no lugar.

Agora você pode devolver os repositórios que excluiu, lembrando de substituir a versão do openSUSE Leap, onde aparecer, pela variável `$releasever`. Pela interface de linha de comando, fazer isso é fácil:

```
# sed -i 's/15.2/$releasever/g' /etc/zypp/repos.d.antigos/*
# mv /etc/zypp/repos.d.antigos/* /etc/zypp/repos.d/
```

Feito isso, você pode excluir o *backup* da sua antiga lista de repositórios:

```
# rm -rf /etc/zypp/repos.d.antigos/
```

Você também pode tentar instalar qualquer programa que por ventura tenha sido removido durante a atualização.

Se lembre de regularmente [verificar se há atualizações (*updates*)][howto-update] para sua nova distribuição. Note que no uso cotidiano do **zypper** você não precisa usar a opção `--releasever`.

Se você encontrou alguma dificuldade durante a atualização ou possui alguma dúvida, não deixe de comentar!

Até a próxima!

[opensuse-leap]:                    {% post_url pt/2020-12-06-opensuse-leap-e-opensuse-tumbleweed-qual-e-a-diferenca %}
[upgrade-to-15.2]:                  {% post_url pt/2020-08-31-linux-kamarada-e-opensuse-leap-como-atualizar-da-versao-151-para-a-152 %}
[kamarada-15.3-beta]:               {% post_url pt/2021-12-11-linux-kamarada-15-3-beta-ajude-a-testar-a-melhor-versao-da-distribuicao %}
[linux]:                            https://www.vivaolinux.com.br/linux/
[openSUSE Leap 15.1]:               {% post_url pt/2019-05-22-comunidade-opensuse-lanca-a-versao-15-1-da-distribuicao-leap %}
[openSUSE Leap 15.2]:               {% post_url pt/2020-07-02-versao-15.2-do-opensuse-leap-traz-novos-e-empolgantes-pacotes-de-inteligencia-artificial-aprendizagem-de-maquina-e-containers %}
[Linux Kamarada 15.1]:              {% post_url pt/2020-02-24-kamarada-15.1-vem-com-tudo-que-voce-precisa-para-usar-o-linux-no-dia-a-dia %}
[Linux Kamarada 15.2]:              {% post_url pt/2020-09-11-linux-kamarada-15.2-venha-para-o-lado-verde-elegante-e-moderno-da-forca %}
[leap-15.3]:                        {% post_url pt/2021-06-02-opensuse-leap-153-constroi-um-caminho-para-a-versao-empresarial %}
[lifetime]:                         https://en.opensuse.org/Lifetime
[system-upgrade]:                   https://en.opensuse.org/SDB:System_upgrade
[upgrade-to-42.2]:                  {% post_url pt/2016-10-31-como-atualizar-do-opensuse-leap-421-para-o-422 %}
[upgrade-to-42.3]:                  {% post_url pt/2017-08-01-como-atualizar-do-opensuse-leap-422-para-o-423 %}
[upgrade-to-15.1]:                  {% post_url pt/2019-05-26-como-atualizar-do-opensuse-leap-150-para-o-151 %}
[howto-update]:                     {% post_url pt/2019-05-14-como-obter-atualizacoes-para-o-linux-opensuse %}
[release-notes]:                    https://doc.opensuse.org/release-notes/x86_64/openSUSE/Leap/15.3/RELEASE-NOTES.pt_BR.html
[debian]:                           https://www.debian.org/
[third-party-repos]:                https://en.opensuse.org/Additional_package_repositories
[official-repos]:                   https://en.opensuse.org/Package_repositories#Official_Repositories
[virtualbox-kmp-default]:           https://software.opensuse.org/package/virtualbox-kmp-default
[telegram]:                         https://t.me/LinuxKamarada
[SlackJeff]:                        https://www.youtube.com/watch?v=VlSCytf7Tns
[VirtualBox]:                       {% post_url pt/2019-10-08-virtualbox-a-forma-mais-facil-de-conhecer-o-linux-sem-precisar-instala-lo %}
[oss]:                              https://pt.wikipedia.org/wiki/Software_de_código_aberto
[osdn]:                             https://pt.osdn.net/
[c3sl]:                             https://www.c3sl.ufpr.br/
[mirrors]:                          https://gitlab.com/kamarada/Linux-Kamarada-GNOME/-/wikis/Mirrors
[yast]:                             http://yast.opensuse.org/
[url]:                              https://pt.wikipedia.org/wiki/URL
[vendor-change]:                    https://en.opensuse.org/SDB:Vendor_change_update
[sle-15-sp3]:                       https://www.suse.com/c/introducing-suse-linux-enterprise-15-sp3/
[zypper]:                           https://pt.opensuse.org/Portal:Zypper
[distribution-logos-openSUSE-Leap]: https://software.opensuse.org/package/distribution-logos-openSUSE-Leap
[distribution-logos-kamarada]:      https://software.opensuse.org/package/distribution-logos-kamarada
[nouveau]:                          https://nouveau.freedesktop.org/
[less]:                             https://man7.org/linux/man-pages/man1/less.1.html
[terminal]:                         http://www.hardware.com.br/livros/linux/usando-terminal.html
[runlevel]:                         http://www.hardware.com.br/termos/runlevel
[switch-runlevel]:                  https://en.opensuse.org/SDB:Switch_runlevel
