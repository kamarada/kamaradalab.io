---
date: 2023-02-27 11:20:00 GMT-3
image: '/files/2023/02/kamarada-15.4.jpg'
layout: post
published: true
nickname: 'kamarada-15.4-final'
title: 'Linux Kamarada 15.4: mais funcional, bonito, polido e elegante do que nunca'
---

{% include image.html src='/files/2023/02/kamarada-15.4.jpg' %}

É com enorme satisfação que venho a público anunciar que o **Linux Kamarada 15.4** está pronto para ser usado por todos!&nbsp;

A distribuição Linux Kamarada é baseada no [openSUSE Leap] e se destina ao uso em _desktops_ em casa e no trabalho, seja em empresas privadas ou em órgãos públicos. Contém os programas essenciais a qualquer instalação do [Linux] e uma área de trabalho com visual moderno e agradável.

O Linux Kamarada 15.4 não é uma distribuição Linux criada "do nada", mas sim baseada em outra distribuição maior, o [openSUSE Leap 15.4]. Os números de versões iguais (15.4) refletem o alinhamento entre as distribuições. Enquanto o openSUSE Leap tem um propósito mais geral, fornecendo um sistema operacional estável para computadores pessoais e servidores, assim como ferramentas para desenvolvedores e administradores de sistemas, o Linux Kamarada é focado em computadores pessoais e em usuários iniciantes.

Novos usuários podem encontrar a nova versão do Linux Kamarada na página [Download]. Se você já usa o Linux Kamarada, confira orientações sobre como atualizar para a versão nova mais adiante.

Confira a seguir o que mudou no Linux Kamarada da versão anterior ([15.3]) para a versão atual (15.4).

[openSUSE Leap]:        {% post_url pt/2020-12-06-opensuse-leap-e-opensuse-tumbleweed-qual-e-a-diferenca %}
[Linux]:                https://www.vivaolinux.com.br/linux/
[openSUSE Leap 15.4]:   {% post_url pt/2022-06-08-leap-15-4-oferece-novos-recursos-com-a-estabilidade-de-sempre %}
[Download]:             /pt/download
[15.3]:                 {% post_url pt/2021-12-30-linux-kamarada-15-3-ano-novo-distribuicao-nova %}

<div class="d-flex align-items-center my-3">
    <div class="flex-shrink-0">
        <img src="/files/2020/02/desktop-environment-gnome.svg" style="max-width: 64px;">
    </div>
    <div class="flex-grow-1 ms-3">
        <h2 class="mb-0">Melhorias no GNOME</h2>
    </div>
</div>

A atualização mais notável foi da área de trabalho [GNOME], que no [Linux Kamarada 15.2][kamarada-15.2] e 15.3 estava na versão 3.34, e agora está na versão 41. O Projeto GNOME adotou uma nova forma mais simples de numerar as versões. Na sequência da versão 3.34 tivemos as versões: [3.36][gnome-3.36], [3.38][gnome-3.38], [40][gnome-40] e [41][gnome-41].

De modo geral, o visual do GNOME está mais suave e polido.

O panorama de **Atividades** recebeu melhorias no visual e no funcionamento. O menu de aplicativos, que antes era dividido em **Frequente** e **Todos**, agora apresenta uma lista única. O usuário pode reordenar os aplicativos e organizá-los em pastas, que podem, inclusive, ser renomeadas.

<div class="row">
    <div class="col-md">
        {% include image.html src='/files/2023/02/kamarada-15.4-activities-pt.jpg' caption='Panorama de Atividades' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2023/02/kamarada-15.4-apps-pt.jpg' caption='Menu de aplicativos' %}
    </div>
</div>

Uma chave **Não perturbe** foi adicionada à janela sobreposta de notificações. Enquanto ativada, as notificações não aparecem, sendo possível conferi-las manualmente, abrindo a janela de notificações. Para que as notificações voltem a aparecer, é só desligar essa chave.

<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        {% include image.html src='/files/2023/02/kamarada-15.4-do-not-disturb-pt.jpg' caption='Não perturbe' %}
    </div>
    <div class="col-md-3"></div>
</div>

As experiências do início e encerramento da sessão e do bloqueio e desbloqueio da tela foram melhoradas. A nova tela de bloqueio é mais funcional, mais fácil de usar e elegante em sua simplicidade.

<div class="row">
    <div class="col-md">
        {% include image.html src='/files/2023/02/kamarada-15.4-system-menu-pt.jpg' caption='Menu do sistema' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2023/02/kamarada-15.4-lock-screen-pt.jpg' caption='Tela de bloqueio' %}
    </div>
</div>

<div class="d-flex">
    <div class="flex-grow-1">
        {% markdown %}

As extensões do GNOME, que antes eram gerenciadas por meio do aplicativo **[Ajustes]**, agora são gerenciadas por meio do novo aplicativo **[Extensões]**, que pode ser usado para atualizar, configurar, remover ou desativar extensões.

[Ajustes]:          https://wiki.gnome.org/Apps/Tweaks
[Extensões]:        https://apps.gnome.org/pt-BR/app/org.gnome.Extensions/

        {% endmarkdown %}
    </div>
    <div class="flex-shrink-0 ms-3">
        <img src="/files/2023/02/org.gnome.Extensions.svg" style="max-width: 48px;">
    </div>
</div>

O aplicativo **[Meteorologia]** foi redesenhado e agora apresenta visualizações separadas para a previsão horária (para as próximas 24 horas) e a previsão diária (para os próximos 10 dias). Outros aplicativos também tiveram suas interfaces redesenhadas para fornecer uma experiência mais polida e elegante, a exemplo da **[Captura de tela]**, **[Gravador de som]** e **[Programas]**.

<div class="row">
    <div class="col-md">
        {% include image.html src='/files/2023/02/kamarada-15.4-weather-pt.jpg' caption='Meteorologia' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2023/02/kamarada-15.4-screenshot-pt.jpg' caption='Captura de tela' %}
    </div>
</div>

Essas foram apenas algumas das novidades que mais me chamaram a atenção. Se quiser saber mais sobre a evolução do GNOME da versão 3.34 até a versão 41, recomendo a leitura das notas de lançamento:

- [Notas de lançamento do GNOME 3.36][gnome-3.36]
- [Notas de lançamento do GNOME 3.38][gnome-3.38]
- [Notas de lançamento do GNOME 40][gnome-40]
- [GNOME 41 Release Notes][gnome-41] (em inglês, apenas)

[GNOME]:            https://br.gnome.org/
[kamarada-15.2]:    {% post_url pt/2020-09-11-linux-kamarada-15.2-venha-para-o-lado-verde-elegante-e-moderno-da-forca %}
[gnome-3.36]:       https://help.gnome.org/misc/release-notes/3.36/index.html.pt_BR
[gnome-3.38]:       https://help.gnome.org/misc/release-notes/3.38/index.html.pt_BR
[gnome-40]:         https://help.gnome.org/misc/release-notes/40.0/index.html.pt_BR
[gnome-41]:         https://help.gnome.org/misc/release-notes/41.0/
[Meteorologia]:     https://apps.gnome.org/pt-BR/app/org.gnome.Weather/
[Captura de tela]:  https://en.wikipedia.org/wiki/GNOME_Screenshot
[Gravador de som]:  https://wiki.gnome.org/Apps/SoundRecorder
[Programas]:        https://apps.gnome.org/pt-BR/app/org.gnome.Software/

<div class="d-flex align-items-center my-3">
    <div class="flex-shrink-0">
        <img src="/files/2023/02/system-software-install.svg" style="max-width: 64px;">
    </div>
    <div class="flex-grow-1 ms-3">
        <h2 class="mb-0">Mudanças nos aplicativos</h2>
    </div>
</div>

Os aplicativos da versão 15.3 foram mantidos em sua maioria, porém atualizados, em novas versões, com novas funcionalidades e _bugs_ corrigidos.

O aplicativo **[Jogos]** deixou de ser um dos principais aplicativos do GNOME (_[GNOME Core Apps]_). Ele não foi empacotado para o openSUSE Leap 15.4 e, portanto, também foi removido do Linux Kamarada 15.4. Ainda é possível instalá-lo por meio do [Flathub], se desejado.

O aplicativo de desenho **[KolourPaint]** foi substituído pelo **[Drawing]**, que se integra melhor com a área de trabalho GNOME, e o aplicativo **[Mapas]** foi incluído.

<div class="row">
    <div class="col-md">
        {% include image.html src='/files/2023/02/kamarada-15.4-drawing-pt.jpg' caption='Drawing' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2023/02/kamarada-15.4-maps-pt.jpg' caption='Mapas' %}
    </div>
</div>

[Jogos]:            https://wiki.gnome.org/Apps/Games
[GNOME Core Apps]:  https://apps.gnome.org/pt-BR/#core
[Flathub]:          https://flathub.org/apps/details/org.gnome.Games
[KolourPaint]:      https://apps.kde.org/pt-br/kolourpaint/
[Drawing]:          https://apps.gnome.org/pt-BR/app/com.github.maoschanz.drawing/
[Mapas]:            https://apps.gnome.org/pt-BR/app/org.gnome.Maps/

<div class="d-flex align-items-center my-3">
    <div class="flex-shrink-0">
        <img src="/files/2023/02/preferences-desktop-theme.svg" style="max-width: 64px;">
    </div>
    <div class="flex-grow-1 ms-3">
        <h2 class="mb-0">Novo tema</h2>
    </div>
</div>

O tema [GTK] usado no Linux Kamarada 15.3, o tema [Materia], não tem recebido mais atualizações, e por isso foi substituído pelo tema [Orchis] (que inclusive é baseado no Materia), mantendo o alinhamento visual da distribuição com o [Material Design] do [Google] (o mesmo _design_ do [Android]).

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        {% include image.html src='/files/2023/02/kamarada-15.4-orchis-theme-pt.jpg' caption='Tema Orchis' %}
    </div>
    <div class="col-md-2"></div>
</div>

[GTK]:              https://www.gtk.org/
[Materia]:          https://github.com/nana-4/materia-theme
[Orchis]:           https://github.com/vinceliuice/Orchis-theme
[Material Design]:  https://material.io/
[Google]:           https://www.google.com.br/
[Android]:          https://www.android.com/

<div class="d-flex align-items-center my-3">
    <div class="flex-shrink-0">
        <img src="/files/2021/12/multimedia-photo-viewer.svg" style="max-width: 64px;">
    </div>
    <div class="flex-grow-1 ms-3">
        <h2 class="mb-0">Novos planos de fundo</h2>
    </div>
</div>

Como de costume, essa nova versão vem com novos planos de fundo, com fotos de belas praias da cidade de [Florianópolis]:

<div class="row">
    <div class="col-md">
        {% include image.html src='/files/2023/02/kamarada-15.4-praia-do-meio-pt.jpg' caption='Praia do Meio (padrão)' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2023/02/kamarada-15.4-lagoa-da-conceicao-pt.jpg' caption='Lagoa da Conceição' %}
    </div>
</div>

Nessa nova versão do GNOME, a tela de bloqueio usa o mesmo plano de fundo da área de trabalho. Mesmo assim, mantendo a tradição das versões anteriores do Linux Kamarada, na versão 15.4 eu incluí dois novos planos de fundo.

Se preferir, você também pode usar os planos de fundo de versões anteriores do Linux Kamarada ou o padrão do openSUSE Leap (ou definir suas próprias imagens como planos de fundo, claro).

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        {% include image.html src='/files/2023/02/kamarada-15.4-backgrounds-pt.jpg' caption='Planos de fundo inclusos no Linux Kamarada 15.4' %}
    </div>
    <div class="col-md-2"></div>
</div>

[Florianópolis]:    https://pt.wikipedia.org/wiki/Florian%C3%B3polis

<div class="d-flex align-items-center my-3">
    <div class="flex-shrink-0">
        <img src="/files/2023/02/network-server.svg" style="max-width: 64px;">
    </div>
    <div class="flex-grow-1 ms-3">
        <h2 class="mb-0">Mudanças na infraestrutura</h2>
    </div>
</div>

As imagens ISO do Linux Kamarada agora são hospedadas pelo [SourceForge]. A página [Download] foi atualizada com os novos _links_.

E o repositório oficial de pacotes do Linux Kamarada foi movido para [packages.linuxkamarada.com](https://packages.linuxkamarada.com/). A mídia Live do Linux Kamarada 15.4 Final já vem configurada com o novo endereço do repositório. Novas instalações feitas a partir dessa mídia também já acessarão o repositório no novo endereço. Mas se você já usa o Linux Kamarada, deve ajustar a configuração do seu sistema.

Para mais informações, leia:

- [Mudanças na infraestrutura do Linux Kamarada][changes-infrastructure]

[SourceForge]:              https://sourceforge.net/projects/kamarada/
[changes-infrastructure]:   {% post_url pt/2023-02-15-mudancas-na-infraestrutura-do-linux-kamarada %}

## Onde baixo o Linux Kamarada?

A página [Download] foi atualizada com o _link_ para _download_ da versão 15.4 Final.

**Observação:** não é recomendado usar o Linux Kamarada em servidores, embora seja possível. Nesse caso, a distribuição openSUSE Leap é mais adequada, por fornecer uma opção de instalação própria para servidores, sem área de trabalho.

## O que faço em seguida?

Quando o _download_ estiver concluído, verifique a integridade da imagem ISO calculando sua soma de verificação (_checksum_), que deve coincidir com a soma que aparece na página [Download]. Se a soma não corresponder, faça o _download_ novamente.

Se não estiver com pressa, é recomendado verificar também a autenticidade da imagem ISO.

A impressão digital (_fingerprint_) da chave pública do Projeto Linux Kamarada é:

```
6b18 52e7 764f b302 b805 a4a0 a575 bcce 1737 8ecc
```

Para importá-la, execute os comandos a seguir:

```
$ wget https://build.opensuse.org/projects/home:kamarada:15.4/public_key
$ gpg --import public_key
```

Para mais informações sobre essas verificações, leia:

- [Verificação de integridade e autenticidade com SHA-256 e GPG][how-to-verify-iso]

Com a imagem ISO baixada e as verificações feitas e bem-sucedidas, você tem 3 opções:

<div class="d-flex">
    <div class="flex-shrink-0">
        <img src="/files/2020/02/disk-burner.svg" style="max-width: 48px;">
    </div>
    <div class="flex-grow-1 ms-3">
        <h6>1) Gravar a imagem ISO em um DVD (gerando, assim, um LiveDVD)</h6>

        <p>Use um aplicativo como o <a href="https://cdburnerxp.se/">CDBurnerXP</a> (no Windows), o <a href="http://www.k3b.org/">K3b</a> (em um Linux com KDE) ou o <a href="https://wiki.gnome.org/Apps/Brasero">Brasero</a> (em um Linux com GNOME) para gravar a imagem ISO em um DVD. Ligue o computador com o LiveDVD na unidade de DVD para iniciar o Linux Kamarada.</p>

        <p>Esse <em>post</em> pode te ajudar com a gravação do DVD:</p>

        <ul><li><a href="{% post_url pt/2016-04-22-como-gravar-um-livedvd %}">Como gravar um LiveDVD</a></li></ul>
    </div>
</div>

<div class="d-flex">
    <div class="flex-shrink-0">
        <img src="/files/2020/02/usb-creator.svg" style="max-width: 48px;">
    </div>
    <div class="flex-grow-1 ms-3">
        <h6>2) Gravar a imagem ISO em um <em>pendrive</em> (gerando, assim, um LiveUSB)</h6>

        <p>Use o <a href="https://www.ventoy.net/">Ventoy</a> (disponível para Windows e Linux) para preparar o <em>pendrive</em> e copie a imagem ISO para ele. Ligue o computador com o LiveUSB para iniciar o Linux Kamarada.</p>

        <p>Esse <em>post</em> pode te ajudar com a preparação do LiveUSB:</p>

        <ul><li><a href="{% post_url pt/2020-07-29-ventoy-crie-pendrives-multiboot-simplesmente-copiando-imagens-iso-para-ele %}">Ventoy: crie um pendrive multiboot simplesmente copiando imagens ISO para ele</a></li></ul>
    </div>
</div>

<div class="d-flex">
    <div class="flex-shrink-0">
        <img src="/files/2020/02/virtualbox.svg" style="max-width: 48px;">
    </div>
    <div class="flex-grow-1 ms-3">
        <h6>3) Criar uma máquina virtual e usar a imagem ISO para iniciá-la</h6>

        <p>Essa opção te possibilita testar o Linux Kamarada sem precisar reiniciar o computador e sair do sistema operacional que você já usa.</p>

        <p>Para mais informações, leia:</p>

        <ul><li><a href="{% post_url pt/2019-10-08-virtualbox-a-forma-mais-facil-de-conhecer-o-linux-sem-precisar-instala-lo %}">VirtualBox: a forma mais fácil de conhecer o Linux sem precisar instalá-lo</a></li></ul>
    </div>
</div>

<div class="d-flex mb-3">
    <div class="flex-grow-1">
        Depois de testar o Linux Kamarada, caso queira instalá-lo no seu computador ou na máquina virtual, inicie o instalador fazendo um duplo-clique nesse ícone, situado na Área de Trabalho.
    </div>
    <div class="flex-shrink-0 ms-3">
        <img src="/files/2021/12/calamares.svg" style="max-width: 48px;">
    </div>
</div>

O instalador fará o particionamento, a cópia do sistema para o computador e todas as configurações iniciais (idioma, leiaute de teclado, fuso horário, usuário e senha, etc.). Ao final, reinicie o computador para começar a usar o sistema instalado.

[how-to-verify-iso]:    {% post_url pt/2018-10-06-verificacao-de-integridade-e-autenticidade-com-sha-256-e-gpg %}

## E se eu já uso o Linux Kamarada?

A imagem ISO não se destina a atualizações, apenas para testes e instalações novas.

Se você já usa o Linux Kamarada 15.3, pode atualizar para o 15.4 seguindo o tutorial:

- [Linux Kamarada e openSUSE Leap: como atualizar da versão 15.3 para a 15.4][upgrade-to-15.4]

Se você já usa o [Linux Kamarada 15.4 RC][15.4-rc], atente-se à mudança de endereço do repositório de pacotes. Você precisa ajustar a configuração do seu sistema. Se ainda não fez isso, veja como fazer em:

- [Mudanças na infraestrutura do Linux Kamarada][changes-infrastructure]

Depois, basta obter as atualizações para os pacotes, como de costume:

- [Como obter atualizações para o Linux openSUSE][howto-update]

[upgrade-to-15.4]:  {% post_url pt/2022-12-28-linux-kamarada-e-opensuse-leap-como-atualizar-da-versao-153-para-a-154 %}
[15.4-rc]:          {% post_url pt/2022-12-19-linux-kamarada-libera-versao-candidata-a-lancamento-15-4-rc %}
[howto-update]:     {% post_url pt/2019-05-14-como-obter-atualizacoes-para-o-linux-opensuse %}

## E se eu já uso o openSUSE Leap?

Você já usa o openSUSE Leap e quer transformá-lo no Linux Kamarada? Simples!

Basta adicionar o repositório do Linux Kamarada e instalar o pacote **[patterns-kamarada-gnome]**.

Você pode fazer isso de duas formas: pela interface gráfica, usando a instalação com 1 clique (*1-Click Install*), ou pelo terminal, usando o gerenciador de pacotes **zypper**. Escolha a que prefere.

Para instalar usando a instalação com 1 clique, clique no botão abaixo:

<p class='text-center'>
    <a class='btn btn-primary' href='/downloads/patterns-kamarada-gnome-pt.ymp'>
        <i class='fas fa-bolt'></i> Instalação com 1 clique
    </a>
</p>

Para instalar usando o terminal, primeiro adicione o repositório do Linux Kamarada:

```
# zypper addrepo -f -K -n "Linux Kamarada" -p 95 "https://packages.linuxkamarada.com/\$releasever/openSUSE_Leap_\$releasever/" kamarada
```

Depois, instale o pacote **patterns-kamarada-gnome**:

```
# zypper in patterns-kamarada-gnome
```

Se você já usa a área de trabalho GNOME, provavelmente será necessário baixar poucos pacotes. Se você usa outra área de trabalho, o tamanho do _download_ será maior.

Quando a instalação terminar, se você criar um novo usuário, perceberá que ele receberá as configurações padrão do Linux Kamarada (como tema, papel de parede, etc.). Usuários já existentes podem seguir usando suas personalizações ou ajustar a aparência do sistema (por exemplo, usando os aplicativos **Ajustes** e **Configurações**).

[patterns-kamarada-gnome]:  https://software.opensuse.org/package/patterns-kamarada-gnome

## Onde obtenho ajuda?

A página [Ajuda] indica alguns lugares onde é possível obter auxílio com o Linux Kamarada, o [openSUSE] ou distribuições Linux em geral (incluindo essas duas).

O canal de suporte preferido pelos usuários tem sido o grupo [@LinuxKamarada](https://t.me/LinuxKamarada) no [Telegram], que é um aplicativo de mensagens que pode ser instalado ou usado a partir do navegador.

[Ajuda]:    /pt/ajuda
[openSUSE]: https://www.opensuse.org/
[Telegram]: {% post_url pt/2021-05-09-o-mensageiro-telegram %}

## Até quando receberá suporte?

De acordo com a [_wiki_ do openSUSE][lifetime], o openSUSE Leap 15.4 deve receber atualizações até o fim de novembro de 2023.

Como o Linux Kamarada é baseado no openSUSE Leap, usuários do Linux Kamarada são, por tabela, usuários do openSUSE Leap também, e recebem essas mesmas atualizações.

Portanto, o Linux Kamarada 15.4 também receberá suporte até o fim de novembro de 2023.

[lifetime]: https://en.opensuse.org/Lifetime

## Onde obtenho os códigos-fonte?

Como todo projeto de _software_ livre, o Linux Kamarada disponibiliza seus códigos-fonte para quem quiser estudá-los, adaptá-los ou contribuir com o projeto.

O desenvolvimento do Linux Kamarada ocorre no [GitLab] e no [Open Build Service][obs]. Lá podem ser obtidos os códigos-fonte dos pacotes desenvolvidos especificamente para esse projeto (até mesmo [o código-fonte deste _site_][kamarada-website] que você lê está disponível).

Os códigos-fonte de pacotes herdados do openSUSE Leap podem ser obtidos diretamente dessa distribuição. Se precisar de ajuda para fazer isso, entre em contato, posso ajudar.

[GitLab]:           https://gitlab.com/kamarada
[obs]:              https://build.opensuse.org/project/subprojects/home:kamarada
[kamarada-website]: https://gitlab.com/kamarada/kamarada.gitlab.io

## Sobre o Linux Kamarada

O Projeto Linux Kamarada visa divulgar e promover o Linux como um sistema operacional robusto, seguro, versátil e fácil de usar, adequado para o uso diário seja em casa, no trabalho ou no servidor. O projeto começou como um _blog_ sobre o openSUSE, que é a distribuição Linux que eu uso há 10 anos (desde [abril de 2012][antoniomedeiros], na época eu instalei o openSUSE 11.4 e depois atualizei para o 12.1). Agora o projeto disponibiliza sua própria distribuição Linux, que traz vários programas apresentados no _blog_ já instalados e prontos para uso.

[antoniomedeiros]:  https://antoniomedeiros.dev/blog/2012/04/21/problemas-envolvendo-bootloaders-mbr-e-tabela-de-particoes/

## Ficha técnica

Para que seja possível comparar esta versão com a anterior e as futuras, segue um resumo do _software_ contido no Linux Kamarada 15.4 Final Build 5.6:

- _kernel_ Linux 5.14.21
- servidor gráfico X.Org 1.20.3 (sem Wayland)
- área de trabalho GNOME 41.8 acompanhada de seus principais aplicativos (_core apps_), como Arquivos (antigo Nautilus), Calculadora, Terminal, Editor de texto (gedit), Captura de tela e outros
- suíte de aplicativos de escritório LibreOffice 7.4.3.2
- navegador Mozilla Firefox 102.8.0 ESR (navegador padrão)
- navegador Chromium 110 (navegador alternativo disponível)
- reprodutor multimídia VLC 3.0.18
- cliente de _e-mail_ Evolution 3.42.4
- centro de controle do YaST
- Brasero 3.12.3
- CUPS 2.2.7
- Drawing 1.0.1
- Firewalld 0.9.3
- GParted 1.3.1
- HPLIP 3.21.10
- Java (OpenJDK) 11.0.17
- KeePassXC 2.7.4
- Linphone 4.3.2
- PDFsam Basic 4.3.4
- Pidgin 2.14.8
- Python 2.7.18 e 3.6.15
- Samba 4.15.13
- Tor 0.4.7
- Transmission 3.00
- Vim 9.0
- Wine 7.0
- instalador Calamares 3.2.36
- Flatpak 1.12.5
- jogos: Aisleriot (Paciência), Copas, Iagno (Reversi), Mahjongg, Minas (Campo minado), Nibbles (Cobras), Quadrapassel (Tetris), Sudoku, Xadrez

Essa lista não é exaustiva, mas já dá uma noção do que se encontra na distribuição.
