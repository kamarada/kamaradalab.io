---
date: '2021-05-30 08:30:00 GMT-3'
image: '/files/2021/05/im-briar-pt.jpg'
layout: post
nickname: 'briar'
title: 'O mensageiro Briar'
---

{% include image.html src='/files/2021/05/im-briar-pt.jpg' %}

O [Briar] é um [mensageiro instantâneo] _[peer-to-peer]_ totalmente criptografado de ponta a ponta, assim como o [Jami]. Ele armazena mensagens de forma segura no dispositivo do usuário, em vez de na nuvem. O Briar foi projetado para ativistas, jornalistas e qualquer pessoa que precise de um meio de comunicação seguro, fácil e robusto. Oferece conversas com contatos, grupos privados (nos quais somente o criador do grupo pode convidar novos membros), fóruns (grupos públicos, nos quais qualquer membro pode convidar outros) e _blogs_ (onde você pode postar notícias sobre você, como nos _blogs_ tradicionais ou em perfis de redes sociais).

O Briar não depende de um servidor central — as mensagens são sincronizadas diretamente entre os dispositivos dos usuários. Se a Internet cair, o Briar pode sincronizar por meio de redes baseadas em proximidade, onde conexões são estabelecidas por Wi-Fi ou Bluetooth. Tecnologias como esta já se provaram úteis quando a disponibilidade de Internet é um problema, como em tempos de crise: em situações assim, o Briar permite que seus usuários mantenham a informação fluindo. Por outro lado, se há conexão com a Internet, o Briar pode sincronizar por meio da rede [Tor], protegendo os usuários e seus contatos contra vigilância.

{% include image.html src='/files/2021/05/briar-sharing.png' %}

Lembra do teste que eu fiz com o Jami? Repeti o mesmo teste com o Briar: sem acesso à Internet, criei duas contas do Briar em dois dispositivos e enviei mensagens de um pro outro. Com o Briar, esse teste foi bem-sucedido: o envio de mensagens _offline_ funcionou.

Você não fornece nenhuma informação pessoal, como telefone ou _e-mail_, para criar uma conta no Briar, apenas um apelido (que não entendi pra que serve) e uma senha (para criptografar o arquivo da sua conta). O aplicativo cria um _link_ Briar para você, que é basicamente sua chave pública precedida por `briar://`. É por meio desse _link_ que outras pessoas podem te adicionar como contato. Na verdade, você e a outra pessoa precisam se adicionar mutuamente como contatos. Portanto, você também precisa do _link_ dela.

Para facilitar a troca de _links_, o aplicativo oferece um QR Code, de modo que vocês podem escanear o QR Code um do outro. Vocês também podem copiar e colar o _link_ um do outro.

Assim como o Jami, o Briar armazena sua conta apenas no seu dispositivo: se você desinstalar o aplicativo ou esquecer sua senha, não há como recuperar sua conta. Portanto, tome cuidado!

No momento, não é possível [fazer _backup_ da conta no Briar][backup], mas esse é um recurso planejado para o futuro.

Assim como o [Signal] e o [Session], o Briar oferece um [bloqueio de tela] que usa a mesma senha, padrão ou impressão digital que você normalmente usa para desbloquear o aparelho.

Além de mensagens de texto, o Briar não oferece nenhum outro modal de comunicação, como mensagens de voz ou de vídeo, arquivos ou chamadas de voz ou de vídeo. Mas os planos pro longo prazo dos desenvolvedores vão muito além das mensagens: eles querem usar a capacidade de sincronização de dados do Briar para fornecer suporte a aplicativos seguros e distribuídos, como mapeamento de crises e edição colaborativa de documentos.

O Projeto Briar é formado por uma [equipe] de programadores, cientistas da computação, _hackers_ e ativistas pela liberdade. Seu objetivo é permitir que pessoas em qualquer país criem espaços seguros onde possam debater qualquer assunto, planejar eventos e organizar movimentos sociais.

O projeto é dirigido por um [conselho] voluntário e tem recebido [financiamento] de fundações que lutam contra vigilância e censura e a favor dos direitos humanos. Existem também algumas maneiras de doar para o projeto listadas na sua [página inicial][briar].

No momento, o Briar está disponível oficialmente apenas para Android. Você pode instalar o aplicativo a partir do [Google Play], do [F-Droid] ou [baixando o APK diretamente do _site_][briar-apk].

O Briar é um [_software_ livre e de código aberto][foss]. Você pode encontrar seu código-fonte no [GitLab próprio do projeto][fonte]. O processo de compilação do Briar produz [_reproducible builds_][reproducer].

O Briar foi auditado de forma independente em 2017 pela empresa de cibersegurança alemã [Cure53]. Todos os problemas identificados por essa auditoria foram corrigidos antes do lançamento da primeira versão do aplicativo. Outra auditoria é planejada para 2023.

Há uma versão do Briar para Linux sendo desenvolvida no [GitLab do projeto][briar-gtk]. Por enquanto, é possível instalá-la usando Flatpak, mas a partir de um repositório próprio, não do Flathub:

```
$ flatpak install --user https://flatpak.dorfbrunnen.eu/repo/appstream/app.briar.gtk.flatpakref
```

O Briar conta com um [manual] _online_ abrangente que descreve todas as funcionalidades disponíveis e explica como usar o aplicativo, assim como uma _[wiki]_ para detalhes técnicos. Além disso, a _wiki_ possui uma página de perguntas frequentes ([FAQ]).

Esse texto faz parte da série:

- [Aplicativos de mensagens livres e focados em privacidade alternativos ao WhatsApp][mensageiro instantâneo]

[briar]:                    https://briarproject.org/
[mensageiro instantâneo]:   {% post_url pt/2021-05-08-aplicativos-de-mensagens-livres-e-focados-em-privacidade-alternativos-ao-whatsapp %}
[peer-to-peer]:             https://pt.wikipedia.org/wiki/Peer-to-peer
[jami]:                     {% post_url pt/2021-05-25-o-mensageiro-jami %}
[tor]:                      https://www.torproject.org/
[backup]:                   https://code.briarproject.org/briar/briar/-/wikis/FAQ#how-do-i-backup-my-account
[signal]:                   {% post_url pt/2021-05-11-o-mensageiro-signal %}
[session]:                  {% post_url pt/2021-05-18-o-mensageiro-session %}
[bloqueio de tela]:         https://briarproject.org/manual/#screen-lock
[equipe]:                   https://briarproject.org/about-us/
[conselho]:                 https://briarproject.org/governance
[financiamento]:            https://briarproject.org/about-us/#funding
[google play]:              https://play.google.com/store/apps/details?id=org.briarproject.briar.android
[f-droid]:                  https://briarproject.org/installing-briar-via-f-droid/
[briar-apk]:                https://briarproject.org/installing-briar-via-direct-download/
[foss]:                     https://pt.wikipedia.org/wiki/Software_livre_e_de_código_aberto
[fonte]:                    https://code.briarproject.org/briar/briar/
[reproducer]:               https://code.briarproject.org/briar/briar-reproducer/-/blob/master/README.md
[Cure53]:                   https://briarproject.org/raw/BRP-01-report.pdf
[briar-gtk]:                https://code.briarproject.org/briar/briar-gtk
[manual]:                   https://briarproject.org/manual/
[wiki]:                     https://code.briarproject.org/briar/briar/wikis
[faq]:                      https://code.briarproject.org/briar/briar/-/wikis/FAQ
