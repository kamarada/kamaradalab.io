---
date: '2021-05-21 01:20:00 GMT-3'
image: '/files/2021/05/im-element-pt.jpg'
layout: post
nickname: 'element-matrix'
title: 'O mensageiro Element (e o padrão Matrix)'
---

{% include image.html src='/files/2021/05/im-element-pt.jpg' %}

O [Element] é um [aplicativo de mensagens] que é a implementação de referência de um cliente para a rede [Matrix]. Diferente de outros mensageiros mais comuns, como [WhatsApp], [Telegram] ou [Signal], que são centralizados, o aplicativo Element e o protocolo Matrix são descentralizados, como o [Session]. Mas também são descentralizados de uma forma diferente do Session: eles foram projetados sobre o conceito de **federação** (do inglês _[federation]_).

Como falar sobre o Element é também falar sobre o Matrix, vamos analisar ambos aqui.

**Sistemas federados** (_federated systems_) usam vários servidores independentes que são capazes de conversar uns com os outros.

O _e-mail_ pode ser visto como um serviço federado. Por exemplo, suponha que você tenha uma conta do [Gmail]. Você pode enviar um _e-mail_ para outro usuário do Gmail. Quando você faz isso, sua mensagem não sai do servidor do Gmail, que simplesmente a copia para a caixa de entrada do destinatário. Mas você também pode enviar um _e-mail_ para um usuário do [Hotmail]. Nesse caso, o servidor do Gmail envia sua mensagem para o servidor do Hotmail, que, por sua vez, a entrega ao destinatário. Além disso, ao enviar um _e-mail_, você não precisa se preocupar com qual aplicativo o destinatário está usando: vocês podem estar usando um _webmail_ ou um aplicativo cliente de _e-mail_ como o [Thunderbird] ou o [Evolution].

Assim como o _e-mail_, o Matrix é um protocolo: um [padrão aberto] para comunicação interoperável, segura, descentralizada e em tempo real sobre o IP. Ele pode ser usado para mensagens instantâneas, chamadas VoIP/WebRTC, comunicação entre dispositivos da Internet das Coisas — ou qualquer coisa que precise de uma forma padronizada de enviar e receber dados ao mesmo tempo em que o histórico das conversas deve ser mantido. O protocolo Matrix é desenvolvido pela [Matrix.org Foundation][foundation], uma ONG sediada no Reino Unido que também fornece [implementações de referência com código aberto][matrix-source] de servidores compatíveis com o protocolo Matrix e SDKs para desenvolver clientes compatíveis.

O padrão aberto Matrix permite que você rode seu próprio servidor Matrix ou use um servidor público, seja ele gratuito ou pago, mantido por outra pessoa ou empresa. Você pode enviar mensagens entre usuários do mesmo servidor Matrix, bem como usuários de outros servidores Matrix. Por exemplo, existe um servidor Matrix público em [matrix.org] que é fornecido pela Matrix.org Foundation de graça para quem quiser usá-lo.

{% include image.html src='/files/2021/05/matrix.png' %}

Existem muitos [clientes Matrix] disponíveis. E o Element é um deles.

O Element é um aplicativo gratuito desenvolvido pela empresa de mesmo nome que oferece produtos e serviços personalizados sobre o padrão Matrix. No [passado], o aplicativo se chamava Riot e a empresa, New Vector. Alguns dos fundadores dessa empresa são também fundadores da Matrix.org Foundation. Eles criaram o Element para popularizar o Matrix.

Quanto à empresa Element, sua receita vem de duas fontes: Element Matrix Services — uma hospedagem para armazenar instalações do Element (e de outras soluções baseadas no Matrix) — e serviços de consultoria — grandes organizações geralmente precisam de conhecimento técnico especializado e de suporte para essas grandes instalações.

Quanto ao aplicativo Element, além de sua natureza federada, ele possui a maioria dos recursos que você encontra em mensageiros comuns: permite o envio de mensagens de texto, fotos, vídeos e arquivos, em conversas com contatos ou em grupo, além de fazer chamadas de voz e vídeo.

O Element oferece criptografia ponta a ponta, mas ela não é habilitada por padrão: você precisa habilitá-la manualmente para cada conversa nas configurações da conversa.

Você precisa de um endereço de _e-mail_ para se registrar no servidor [matrix.org], mas você pode desassociá-lo da sua conta mais tarde, se quiser (o que não é recomendado pela Element, já já você vai entender). Opcionalmente, você pode depois associar sua conta a um número de telefone. Você pode ter quantos _e-mails_ e telefones quiser associados à sua conta — inclusive nenhum. Você cria um nome de usuário durante o registro. Com ele, você pode manter seus dados pessoais confidenciais, se preferir: outras pessoas podem te encontrar no Element por esse nome de usuário. Você também pode escolher, para cada _e-mail_ e número de telefone registrado, se deseja poder ser encontrado por eles.

Além disso, ter um _e-mail_ associado à sua conta permite que você redefina sua senha caso a esqueça. Caso você tenha esquecido sua senha e não tenha associado um _e-mail_ à sua conta, não será possível recuperá-la. Daí a recomendação para ter um _e-mail_ associado.

Assim como o Telegram, você pode usar o Element em vários dispositivos — computadores, _smartphones_ e _tablets_ — ao mesmo tempo, suas mensagens serão sincronizadas entre eles. Você pode até usar o Element pelo navegador. Isso é possível porque as mensagens são armazenadas em servidores Matrix.

Também porque as mensagens são armazenadas em servidores, o aplicativo não oferece um recurso de _backup_ de mensagens. O que ele oferece é fazer _backup_ das suas chaves de criptografia. Se você ativar o _backup_ de chaves, seu dispositivo manterá uma cópia segura das suas chaves no servidor Matrix. Para garantir que essas chaves só possam ser acessadas por você, elas são criptografadas no seu dispositivo antes de serem enviadas ao servidor, de modo que o servidor nunca as veja descriptografadas. Elas podem ser criptografadas com uma chave que você já possui ou por uma frase de segurança.

Assim como outros mensageiros, o Element permite que você restrinja o acesso ao aplicativo usando um PIN ou biometria.

Os [números] do Element e do Matrix são impressionantes: mais de 28 milhões de usuários, que incluem indivíduos, empresas e governos, espalhados por mais de 60.000 implantações em todo o mundo. A maior implantação pública tem mais de 7 milhões de usuários. Já a maior implantação comercial atende a mais de 500.000 usuários.

O Element é usado por alguns projetos de código aberto bem conhecidos, como [Mozilla], [KDE], [GNOME] e [Gitter], porque possui recursos para [desenvolvedores], que incluem realce de sintaxe para trechos de código, suporte a Markdown e robôs (_bots_) programáveis.

A interoperabilidade é uma ideia chave tão presente no protocolo Matrix que o servidor pode ser integrado a outros serviços por meio de **_[bridges]_** (pontes). Existem _bridges_ Matrix para WhatsApp, Facebook Messenger, Skype, Telegram, Signal, Slack, Rocket.Chat, Discord, IRC, só pra citar alguns exemplos. As _bridges_ permitem que você transforme seu cliente Matrix em um mensageiro "tudo em um". Mas lembre-se que esses serviços ainda podem rastreá-lo de alguma forma. Portanto, se manter-se anônimo é uma preocupação para você, talvez integrá-los não seja uma boa ideia.

Todas as versões do aplicativo Element têm seus códigos abertos, você pode encontrar seus códigos-fonte no [GitHub].

A [criptografia ponta a ponta no Matrix][e2ee] (e, portanto, no Element) é baseada na implementação Olm do [algoritmo Double Ratchet][Double Ratchet algorithm], popularizado pelo Signal. A implementação Megolm é usada para comunicações em grupo. Embora os aplicativos do Element não tenham sido auditados formalmente, a Olm e a Megolm foram auditadas de forma independente pelo [NCC Group].

O _app_ do Element para Android pode ser instalado a partir do [Google Play] ou do [F-Droid].

Com relação ao Linux, o Element só disponibiliza oficialmente pacotes para [Debian] e [Ubuntu]. Mas é possível instalá-lo no openSUSE usando Flatpak a partir do [Flathub]:

```
# flatpak install im.riot.Riot
```

Para usar o Element pelo navegador, sem instalar aplicativo, acesse: [app.element.io](https://app.element.io/).

O Matrix possui uma página de perguntas frequentes ([FAQ]) muito explicativa no seu _site_. O Element também possui uma página de [ajuda] muito explicativa.

Também existem versões do Element para outros sistemas. Veja mais em: [element.io](https://element.io/get-started).

Esse texto faz parte da série:

- [Aplicativos de mensagens livres e focados em privacidade alternativos ao WhatsApp][aplicativo de mensagens]

[element]:                  https://element.io/
[aplicativo de mensagens]:  {% post_url pt/2021-05-08-aplicativos-de-mensagens-livres-e-focados-em-privacidade-alternativos-ao-whatsapp %}
[matrix]:                   https://matrix.org/
[whatsapp]:                 https://www.whatsapp.com/
[telegram]:                 {% post_url pt/2021-05-09-o-mensageiro-telegram %}
[signal]:                   {% post_url pt/2021-05-11-o-mensageiro-signal %}
[session]:                  {% post_url pt/2021-05-18-o-mensageiro-session %}
[federation]:               https://en.wikipedia.org/wiki/Federation_(information_technology)
[gmail]:                    https://gmail.com/
[hotmail]:                  https://www.hotmail.com/
[thunderbird]:              https://www.thunderbird.net/
[evolution]:                https://wiki.gnome.org/Apps/Evolution
[padrão aberto]:            https://matrix.org/docs/spec/
[foundation]:               https://matrix.org/foundation/
[matrix-source]:            https://github.com/matrix-org
[matrix.org]:               https://matrix.org/
[clientes Matrix]:          https://matrix.org/clients/
[passado]:                  https://element.io/blog/the-world-is-changing/
[números]:                  https://element.io/about
[Mozilla]:                  https://chat.mozilla.org/
[KDE]:                      http://webchat.kde.org/
[GNOME]:                    https://gnome.riot.im/
[Gitter]:                   https://gitter.im/
[desenvolvedores]:          https://element.io/developers
[bridges]:                  https://matrix.org/bridges/
[github]:                   https://github.com/vector-im/
[e2ee]:                     https://matrix.org/docs/guides/end-to-end-encryption-implementation-guide
[Double Ratchet algorithm]: https://en.wikipedia.org/wiki/Double_Ratchet_Algorithm
[NCC Group]:                https://www.nccgroup.trust/globalassets/our-research/us/public-reports/2016/november/ncc_group_olm_cryptogrpahic_review_2016_11_01.pdf
[google play]:              https://play.google.com/store/apps/details?id=im.vector.app
[f-droid]:                  https://f-droid.org/packages/im.vector.app/
[debian]:                   https://www.debian.org/
[ubuntu]:                   https://ubuntu.com/
[flathub]:                  https://flathub.org/apps/details/im.riot.Riot
[faq]:                      https://matrix.org/faq/
[ajuda]:                    https://element.io/help
