---
date: 2021-10-03 16:30:00 GMT-3
image: '/files/2021/10/free-speech.png'
layout: post
published: true
nickname: 'free-sw-free-speech'
title: 'Software livre e liberdade de expressão'
excerpt: 'Comecemos lembrando do conceito de software livre como apresentado pela Free Software Foundation (FSF, &quot;Fundação do Software Livre&quot;): Por &quot;software livre&quot; devemos entender aquele software que respeita a liberdade e senso de comunidade dos usuários. Grosso modo, isso significa que os usuários possuem a liberdade de executar, copiar, distribuir, estudar, mudar e melhorar o software. Assim sendo, &quot;software livre&quot; é uma questão de liberdade, não de preço. Para entender o conceito, pense em &quot;liberdade de expressão&quot;, não em &quot;cerveja grátis&quot;.'
---

{% include update.html date="13/06/2023" message="Texto revisado e imagens atualizadas." %}

{% include image.html src='/files/2021/10/free-speech.png' caption='Crédito da imagem: Madelgarius via [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Freedom_of_speech_(3).svg), licença de uso: [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR)' %}

Eu não costumo escrever textos filosóficos aqui, mas alguns problemas que percebo no que estamos vivendo têm me incomodado profundamente. Como um desses problemas pode ser resolvido com _software_ livre, me senti particularmente inspirado a escrever sobre isso.

Comecemos lembrando do conceito de _[software livre]_ como apresentado pela _Free Software Foundation_ (FSF, "Fundação do _Software_ Livre"):

[software livre]: https://www.gnu.org/philosophy/free-sw.pt-br.html

> Por "software livre" devemos entender aquele software que respeita a liberdade e senso de comunidade dos usuários. Grosso modo, isso significa que os usuários possuem a liberdade de executar, copiar, distribuir, estudar, mudar e melhorar o software. Assim sendo, "software livre" é uma questão de liberdade, não de preço. **Para entender o conceito, pense em "liberdade de expressão", não em "cerveja grátis".**

(ênfase minha)

{% capture nota_free %}

No inglês, a palavra _free_ pode significar "livre" -- como em _freedom_, "liberdade" -- ou "grátis" -- como em _freeware_, "_software_ gratuito" -- e por isso a FSF faz essa distinção, por vezes preferindo o espanhol: _software libre_.

{% endcapture %}

{% include note.html text=nota_free %}

Vejamos alguns exemplos de _softwares_ livres e _softwares_ não livres para ilustrar melhor essa definição, que pode ser estranha para quem não está acostumado com ela.

Por exemplo, o sistema operacional [Windows], desenvolvido pela [Microsoft] e presente na maioria dos computadores vendidos no mundo todo, não é um _software_ livre, já que seus usuários não estão autorizados a copiá-lo e distribui-lo, o que seria considerado crime de pirataria. Também não é possível estudar o funcionamento interno do Windows nem mudá-lo, porque seu código-fonte não é disponibilizado pela Microsoft. Ninguém pode criar sua própria versão do Windows. Eventuais melhorias no Windows só podem ser feitas pela Microsoft. O máximo que usuários podem fazer para mudar o Windows são reclamações ou sugestões para a Microsoft, que pode ou não resolvê-las ou atendê-las.

[Windows]: https://www.microsoft.com/pt-br/windows
[Microsoft]: https://www.microsoft.com/pt-br/

Diferente é o sistema operacional [Linux], inicialmente desenvolvido pelo engenheiro de _software_ [Linus Torvalds], mas que hoje recebe contribuições de várias pessoas e empresas do mundo todo. Hoje há milhares de distribuições Linux (como é chamado o conjunto de sistema operacional Linux mais aplicativos para ele), algumas maiores e mais conhecidas -- como [Ubuntu], [Debian], [openSUSE Leap], [openSUSE Tumbleweed], [Fedora] e [Manjaro] -- e outras menores, em sua maioria originadas a partir dessas maiores. Qualquer pessoa pode baixar uma distribuição Linux e instalá-la no seu computador, fazer uma cópia da mídia de instalação para um amigo sem incorrer em crime de pirataria, estudar o funcionamento interno do sistema a partir do seu código-fonte, assim como criar sua própria distribuição Linux a partir de outra maior.

[Linux]: https://www.vivaolinux.com.br/linux/
[Linus Torvalds]: https://pt.wikipedia.org/wiki/Linus_Torvalds
[Ubuntu]: https://ubuntu.com/
[Debian]: https://www.debian.org/
[openSUSE Leap]: {% post_url pt/2020-12-06-opensuse-leap-e-opensuse-tumbleweed-qual-e-a-diferenca %}
[openSUSE Tumbleweed]: {% post_url pt/2020-12-06-opensuse-leap-e-opensuse-tumbleweed-qual-e-a-diferenca %}
[Fedora]: https://fedoraproject.org/pt-br/
[Manjaro]: https://manjaro.org/

O autor deste artigo é mantenedor de uma distribuição Linux brasileira, o [Linux Kamarada], baseada na distribuição Linux maior openSUSE Leap. Enquanto o openSUSE Leap tem um propósito mais geral, fornecendo um sistema operacional estável para computadores pessoais e servidores, assim como ferramentas para desenvolvedores e administradores de sistemas, o Linux Kamarada é focado em computadores pessoais e em usuários iniciantes.

[Linux Kamarada]: {% post_url pt/2023-02-27-linux-kamarada-15-4-mais-funcional-bonito-polido-e-elegante-do-que-nunca %}

Mais alguns exemplos de _softwares_ livres famosos incluem o sistema operacional [Android], presente na maioria dos _smartphones_, e o navegador [Firefox], um dos navegadores mais utilizados pelos internautas.

[Android]: https://www.android.com/intl/pt-BR_br/
[Firefox]: https://www.mozilla.org/pt-BR/firefox/

Voltando à definição de _software_ livre, como se vê pela própria definição, a liberdade de _software_ está intimamente associada à liberdade de expressão. Isso porque quando você é livre para usar o _software_ da forma que bem entende, ou modificá-lo da forma que bem entende, por vezes até mesmo clonando o _software_ original e iniciando um _software_ novo, em última análise você é livre para expressar **concordância** ou **discordância** em relação a quem desenvolve aquele _software_.

Um exemplo bastante ilustrativo disso é a suíte de escritório [LibreOffice]. Antes dela, havia a suíte [OpenOffice], _software_ livre, então desenvolvido pela [Sun]. Aqui no Brasil, ele era chamado de [BrOffice], devido a problemas de propriedade intelectual. A Sun foi adquirida pela [Oracle] em 2009. Mas a comunidade do OpenOffice não ficou satisfeita com a forma como a Oracle deu continuidade ao desenvolvimento dos _softwares_ livres que herdou da Sun. Em 2010, [os desenvolvedores do OpenOffice decidiram se desvincular da Oracle][libreoffice-2010] e criaram uma fundação, a [The Document Foundation][tdf], para dar continuidade ao desenvolvimento da suíte de escritório de forma independente. Eis que criaram a suíte de escritório LibreOffice como uma ramificação (_fork_) do projeto original OpenOffice. Em [2011][openoffice-2011], a Oracle passou o OpenOffice para a [Fundação Apache], que o mantém desde então. Mas [o OpenOffice está praticamente parado no tempo][openoffice-2020], com o LibreOffice tendo recebido nesses anos mais atualizações, correções de _bugs_ e novas funcionalidades e se tornando, na prática, a sua continuação.

[LibreOffice]: https://pt-br.libreoffice.org/
[OpenOffice]: https://www.openoffice.org/pt-br/
[Sun]: https://pt.wikipedia.org/wiki/Sun_Microsystems
[BrOffice]: https://www.vivaolinux.com.br/artigo/Conheca-o-OpenOffice.org-e-o-BrOffice.org?pagina=3
[Oracle]: https://www.oracle.com/br/
[libreoffice-2010]: https://www.hardware.com.br/noticias/2010-09/openoffice-libreoffice.html
[tdf]: https://www.documentfoundation.org/
[openoffice-2011]: https://www.techtudo.com.br/artigos/noticia/2012/03/o-que-esta-acontecendo-com-o-openoffice.html
[Fundação Apache]: https://www.apache.org/
[openoffice-2020]: https://blog.documentfoundation.org/blog/2020/10/12/open-letter-to-apache-openoffice/

Devido a [insatisfação semelhante][mariadb-2013] com a Oracle, hoje temos o [MariaDB], _fork_ do [MySQL].

[mariadb-2013]: https://www2.computerworld.com.au/article/457551/dead_database_walking_mysql_creator_why_future_belongs_mariadb/
[MariaDB]: https://mariadb.org/
[MySQL]: https://www.mysql.com/

Há outros exemplos. O [KeePassXC] surgiu devido a uma insatisfação com o desenvolvimento (considerado [lento]) do [KeePassX]. Já o KeePassX surgiu como um _port_ para Linux do [KeePass], que até então era desenvolvido apenas para Windows.

[KeePassXC]: https://keepassxc.org/
[lento]: https://keepassxc.org/docs/#faq-keepassx
[KeePassX]: https://www.keepassx.org/
[KeePass]: https://keepass.info/

Tanto o LibreOffice quanto o KeePassXC já vêm instalados por padrão no Linux Kamarada, cujo desenvolvimento, aliás, eu mesmo comecei por [não concordar][opensuse-list-1] com o [Projeto openSUSE][opensuse] não ter lançado [imagens Live para o Linux openSUSE Leap 42.1][opensuse-list-2]. Depois, com o lançamento do [openSUSE Leap 15.0], o openSUSE voltou a fazê-las, e hoje temos imagens Live das duas distribuições, openSUSE Leap e Linux Kamarada.

[opensuse-list-1]: https://lists.opensuse.org/archives/list/users@lists.opensuse.org/message/S6JUZYUVDJJQE3N2BUXKH2WEFV6W423B/
[opensuse]: https://www.opensuse.org/
[opensuse-list-2]: https://lists.opensuse.org/archives/list/factory@lists.opensuse.org/message/EQUOAEOSWMUBWGDAYIB35HM54Q7LYJ5B/
[openSUSE Leap 15.0]: {% post_url pt/2018-05-25-baseado-no-codigo-do-enterprise-testado-milhoes-de-vezes-opensuse-leap-15-lancado %}

Como se vê, **progresso se faz no debate saudável de ideias**.

E isso acontece não só na área de _software_, mas na ciência como um todo.

{% include image.html src='/files/2021/10/voltaire-liberdade-de-expressao.jpg' caption='Crédito da imagem: [Pensador](https://www.pensador.com/frase/MjE2OTIwNw/)' %}

Hoje sabemos que a Terra gira em torno do Sol, mas sabia que nem sempre essa foi a "versão oficial" dos fatos? Há muito tempo atrás se acreditava que o Sol girava em torno da Terra, teoria proposta pelo astrônomo grego [Ptolomeu] (90-168 d.C.) e aceita como verdade por mais de mil anos. O teólogo [Giordano Bruno] (1548-1600) **morreu queimado na fogueira** por defender o contrário, ou seja, que a Terra girava em torno do Sol. O astrônomo [Nicolau Copérnico] (1473-1543), o primeiro a estabelecer de forma científica o [heliocentrismo], teve seu livro **censurado**, listado no Índex -- a lista dos livros "hereges" proibidos pela Igreja Católica. Já o astrônomo [Galileu Galilei] (1564-1642) foi o primeiro a comprovar, por meio de observações, a teoria heliocêntrica de Copérnico, mas foi preso e ameaçado de morte pela Santa Inquisição, até que negou suas descobertas. Somente [em 1992][Galileu Galilei], passados mais de 300 anos, a Igreja Católica reconheceu o engano que cometeu ao condenar Galileu.

[Ptolomeu]: https://pt.wikipedia.org/wiki/Geocentrismo
[Giordano Bruno]: https://www.terra.com.br/diversao/arte-e-cultura/quem-foi-giordano-bruno-o-mistico-visionario-queimado-na-fogueira-ha-418-anos,23c9a38115984e869b1f2e75e884bddevle0kw25.html
[Nicolau Copérnico]: https://www.terra.com.br/noticias/ciencia/nicolau-copernico-revolucionario-da-astronomia-morreu-ha-470-anos,167f3922a26de310VgnVCM4000009bcceb0aRCRD.html
[heliocentrismo]: https://pt.wikipedia.org/wiki/Heliocentrismo
[Galileu Galilei]: https://www.terra.com.br/noticias/educacao/voce-sabia/voce-sabia-a-igreja-reconheceu-que-errou-ao-condenar-galileu,400811f48735b310VgnCLD200000bbcceb0aRCRD.html

Hoje também sabemos que não é possível transformar qualquer coisa em ouro. Mas ficamos sabendo disso depois de muitas tentativas dos [alquimistas] na Idade Média. Inspirados pelo que hoje poderíamos chamar de "crenças malucas", os alquimistas realizaram vários estudos e experimentos que acabaram dando origem à Química Moderna.

[alquimistas]: https://pt.wikipedia.org/wiki/Alquimia

Hoje, há quem acredite -- e até mesmo quem [tente provar][terraplanista] -- que a Terra é plana. Não foi o que aprendi nas aulas de Geografia e, sinceramente, acho que essas pessoas estão perdendo seu tempo. Mesmo assim, não as desencorajo a fazerem seus experimentos. Da minha parte, não concordo com elas e não as financio, mas penso que elas são livres pra fazer o que bem entendem com seu tempo e dinheiro. Acredito que vão acabar descobrindo por conta própria que a Terra é redonda (ou, para ser mais preciso, oval), e não plana. Quem sabe nesses experimentos não descobrem mais alguma coisa, como fizeram os alquimistas?

[terraplanista]: https://www.tecmundo.com.br/ciencia/150580-terraplanista-morre-lancar-foguete-caseiro-eua.htm

Mas os maiores impasses científicos dos últimos anos não são nem esse. Se você acompanha as notícias, deve estar cansado de saber, dispensa eu explicar aqui. Esses impasses não me preocupam, porque ciência se faz assim: eventualmente, esses impasses se resolvem, e um dos lados acaba virando o "consenso". O que me preocupa é a percepção de que nas maiores redes sociais -- como [Facebook], [Instagram], [Twitter] ou [YouTube] -- dependendo do que você fale sobre esses assuntos, você pode ter seu alcance limitado, sua conta suspensa ou até mesmo excluída. Embora as redes apresentem essas punições como "combate à desinformação", e isso pareça ser uma coisa boa, devo alertar a quem me lê que o nome correto disso é **censura**, não é nada bom, e é ineficaz em "combater desinformação".

[Facebook]: https://www.facebook.com/
[Instagram]: https://www.instagram.com/
[Twitter]: https://twitter.com/
[YouTube]: https://www.youtube.com/

Não vou citar exemplos aqui, até porque não quero defender "um lado" ou "o outro lado". Se quiser, você pode pesquisar e encontrar exemplos. Vou focar no problema da censura.

Antes, uma ressalva: entendo que as redes sociais são propriedades privadas dos seus donos e que "minha casa, minhas regras". O YouTube, por exemplo, tem o direito de remover vídeos que lhe desagradem -- principalmente se tal remoção está prevista nos seus termos de uso, com os quais seus usuários implicitamente concordam ao usar a plataforma.

Dito isso, entendo que o YouTube _pode_ fazer isso, mas penso que _não deveria_ fazer isso.

Se o objetivo dessas redes é realmente "combater desinformação", estão fazendo isso do jeito errado. A melhor forma de combater "desinformação" é com **informação**. Se vejo alguém falando algo de que discordo, deixo um comentário esclarecedor embaixo, ou faço uma publicação no meu próprio perfil falando o que penso, com o que concordo. A melhor forma de combater uma "má ideia" é deixar que ela apareça, que ela seja conhecida, para que possa, então, ser refutada e nunca mais defendida ou praticada. Ninguém é dono da verdade. Por isso, não há ninguém melhor do que o próprio usuário das redes sociais para julgar a veracidade de cada conteúdo. E se as redes sociais permitem que uma infinidade de opiniões diferentes sobre determinado assunto circulem livremente, elas oferecem mais material para que cada usuário possa julgar o mérito de cada opinião e, assim, formar sua própria opinião.

Se, em vez disso, uma rede social bane um usuário, ela não está verdadeiramente silenciando essa pessoa ou suas ideias. Especialmente na era da Internet e da informação descentralizada e distribuída, essa pessoa vai encontrar outros canais para divulgar suas ideias. Dessa forma, outras redes estão crescendo, como [WhatsApp] e [Telegram], onde as comunicações são mais privadas, e até mesmo redes sociais alternativas estão surgindo e crescendo também, como [Gab], [Parler] e [GETTR]. Há quem migre pra essas redes porque foi banido das outras, e há quem migre pra continuar seguindo quem já seguia antes.

Com relação ao YouTube, tudo o que conseguiu com sua "caça às bruxas" foi ganhar concorrentes: [BitChute], [Rumble] e [LBRY]/[Odysee] -- já já volto a esta última.

[WhatsApp]: https://www.whatsapp.com/
[Telegram]: {% post_url pt/2021-05-09-o-mensageiro-telegram %}
[Gab]: https://gab.com/
[Parler]: https://olhardigital.com.br/2023/04/15/internet-e-redes-sociais/rede-social-parler-e-tirada-do-ar-apos-ser-comprada/
[GETTR]: https://gettr.com/
[BitChute]: https://www.bitchute.com/
[Rumble]: https://rumble.com/
[LBRY]: https://lbry.com/
[Odysee]: https://odysee.com/$/invite/@LinuxKamarada:f

Os algoritmos das redes sociais já percebiam os gostos das pessoas e lhes recomendavam conteúdos do seu interesse, criando uma espécie de ciclo vicioso no qual seus usuários cada vez mais recebiam conteúdos com os quais já concordavam previamente. Sem contato com ideias diferentes, as pessoas acabaram se isolando em bolhas, e isso já prejudicava o debate. Se agora nem diferentes bolhas podem conviver na mesma rede social, e cada bolha vai precisar ter uma rede social inteira só sua, não vai haver mais debate. E isso é perigoso.

Vou citar apenas um exemplo não relacionado ao assunto mais polêmico de 2020 e 2021.

Veja, por exemplo, o caso do jornalista e advogado [Glenn Greenwald][gg], que também é ativista na luta por liberdades civis, liberdade de expressão e [privacidade]. Ele se tornou mundialmente conhecido após denunciar em 2013 no jornal [The Guardian][gg-2013], com documentos confidenciais vazados da NSA por [Edward Snowden][es], que o governo norte-americano vigiava as ligações dos seus cidadãos. Aqui no Brasil, ele se tornou mais conhecido por sua cobertura da Lava Jato em 2019 no jornal [The Intercept][gg-2019], que ele cofundou em 2014. Em outubro de 2020, ele [pediu demissão][gg-substack] desse mesmo jornal, depois de ter sofrido censura interna dos próprios colegas para que não publicasse uma matéria criticando Joe Biden, então candidato à presidência dos EUA. Hoje, em 2021, Glenn Greenwald conduz seu trabalho de forma independente escrevendo no [Substack][gg-substack] e lançando vídeos no [Rumble][gg-rumble].

[gg]: https://pt.wikipedia.org/wiki/Glenn_Greenwald
[privacidade]: https://www.privacidade.digital/
[gg-2013]: https://www.theguardian.com/world/2013/jun/06/nsa-phone-records-verizon-court-order
[es]: https://www.theguardian.com/world/2013/jun/09/edward-snowden-nsa-whistleblower-surveillance
[gg-2019]: https://theintercept.com/2019/06/09/editorial-chats-telegram-lava-jato-moro/
[gg-2020]: https://greenwald.substack.com/p/my-resignation-from-the-intercept
[gg-substack]: https://greenwald.substack.com/p/my-resignation-from-the-intercept
[gg-rumble]: https://rumble.com/vl268j-welcome-to-glenn-greenwalds-system-update.html

Novamente, quero deixar claro que aqui não estou defendendo o Glenn Greenwald, até porque não concordo com todas as ideias dele, embora concordo, sim, com muitas, e até o respeito e admiro pelas ideias em que concordamos. Eu diria até que é difícil -- talvez impossível -- alguém concordar com outrem em tudo. Apenas o trouxe como exemplo de alguém que não se deixou intimidar pela censura e encontrou meios alternativos para falar.

Das novas redes sociais que estão surgindo, uma em particular me chamou a atenção por ser uma solução interessante envolvendo _software_ livre.

[LBRY][what-is-lbry] (do inglês _library_, que quer dizer "biblioteca") é, ao mesmo tempo, o **protocolo** de uma rede para compartilhamento de conteúdos (vídeos, áudios, documentos ou qualquer outro tipo de arquivo) que combina ideias do [BitTorrent] e do [Bitcoin], e o **aplicativo** cliente que permite o uso dessa rede. O LBRY é _software_ livre e seu código-fonte pode ser conferido no [GitHub].

[what-is-lbry]: https://lbry.com/faq/what-is-lbry
[BitTorrent]: https://pt.wikipedia.org/wiki/BitTorrent
[Bitcoin]: {% post_url pt/2018-12-12-bitcoin-para-iniciantes-com-a-carteira-electrum %}
[GitHub]: https://github.com/lbryio

{% include image.html src='/files/2023/06/lbry.jpg' caption='O aplicativo LBRY' %}

[Odysee] é o nome da interface _web_ do LBRY, feita e mantida pelos mesmos criadores do LBRY, que permite que vídeos publicados nessa rede possam ser vistos em qualquer navegador, sem necessidade de instalar o aplicativo. Experimente: acesse [odysee.com][Odysee].

Como a rede LBRY é baseada em _blockchain_ (semelhante à do Bitcoin) e seus conteúdos são servidos diretamente a partir dos computadores dos usuários (conceito semelhante ao de semeador, _seeder_, do BitTorrent), é impossível censurar um vídeo compartilhado usando o protocolo LBRY. Enquanto houver pelo menos uma cópia desse vídeo em pelo menos um computador da rede, será possível ver esse vídeo. Desse modo, canais no LBRY são bastante resilientes a censura. A rede LBRY possibilita verdadeira liberdade de expressão.

Porém, isso não quer dizer que qualquer tipo de conteúdo possa ser postado sem qualquer tipo de consequência. O _site_ Odysee possui suas [regras da comunidade][Odysee-Community-Guidelines] e vídeos com conteúdos que a infringem, como pornografia ou terrorismo, por exemplo, podem ser deslistados do _site_, de modo que não possam ser encontrados pela busca do _site_, nem visualizados no _site_. Ainda assim, poderiam ser encontrados e visualizados pelo aplicativo. Como a [monetização] dos vídeos também é gerenciada pelos criadores do LBRY, é possível que vídeos que não sigam as regras da comunidade sejam desmonetizados.

[Odysee-Community-Guidelines]: https://help.odysee.tv/communityguidelines/
[monetização]: https://lbry.com/faq/rewards

Note que este _software_ livre, o LBRY, também surgiu de uma discordância, como as que mencionei antes: discordância da forma como as grandes redes sociais têm tratado seus usuários e conteúdos. E ele veio para permitir que outras pessoas sigam discordando.

Todos podem se expressar na rede LBRY, independentemente de orientação política, crença religiosa, etc.

Os [Bitcoinheiros], cujo [canal no YouTube][Bitcoinheiros-YouTube] eu já havia indicado em [outro texto][electrum], também têm um [canal no LBRY][Bitcoinheiros-LBRY].

[Bitcoinheiros]: https://bitcoinheiros.com/
[Bitcoinheiros-YouTube]: https://www.youtube.com/c/bitcoinheiros
[electrum]: {% post_url pt/2020-11-30-bitcoin-instalando-a-carteira-electrum %}
[Bitcoinheiros-LBRY]: https://odysee.com/@bitcoinheiros:f?r=5vLjYu4iNzsaYfZT3r5B7Dz7oKgVqk57

Eu também criei um [canal do Linux Kamarada no LBRY][kamarada-lbry] e disponibilizei lá os vídeos que estavam no [canal no YouTube][kamarada-youtube] (os próximos vídeos serão enviados para ambas as redes):

[kamarada-lbry]: https://odysee.com/@LinuxKamarada:f?r=5vLjYu4iNzsaYfZT3r5B7Dz7oKgVqk57
[kamarada-youtube]: https://www.youtube.com/channel/UCSv0jYFQP8vzOJwKN_8LrNg

{% include image.html src='/files/2023/06/odysee.jpg' caption='Canal do Linux Kamarada no LBRY/Odysee' %}

Oportunamente, em um próximo texto, falarei um pouco mais sobre o aplicativo LBRY para Linux. Enquanto isso, você pode explorar a interface _web_ do [Odysee]. Até a próxima!
