---
date: '2024-06-12 15:00:00 UTC'
image: '/files/2024/06/leap-15.6.png'
layout: post
nickname: 'leap-15.6'
title: 'Leap 15.6 revela opções para usuários'
excerpt: 'O lançamento do Leap 15.6 é oficial e abre caminho para que profissionais e organizações façam a transição para a distribuição empresarial do SUSE com suporte estendido ou se preparem para a próxima grande atualização, que será o Leap 16. As demandas por sistemas operacionais robustos, seguros e estáveis ​​no setor da infraestrutura digital estão mais críticas do que nunca. A combinação do Leap 15.6, desenvolvido pela comunidade, e do SUSE Linux Enterprise 15 Service Pack 6, que integra novos recursos e melhorias, oferece uma solução ótima para o gerenciamento de infraestrutura crítica. Notavelmente, as versões de suporte geral e de suporte estendido do SUSE; esses ciclos de vida de suporte ao produto duram muito além da vida útil do Leap 15, garantindo um serviço mais longo e confiável para os usuários.'
---

{% capture nota1 %}
Esta é a tradução da notícia originalmente publicada por Douglas DeMaio no _site_ [news.opensuse.org](https://news.opensuse.org/2024/06/12/leap-unveils-choices-for-users/). Esta tradução foi contribuída pelo Projeto Linux Kamarada para o Projeto openSUSE e se tornou oficial, podendo ser conferida também na [_wiki_ do openSUSE](https://pt.opensuse.org/openSUSE:Lancamento_versao_15.6).
{% endcapture %}

{% include note.html text=nota1 %}

{% include image.html src='/files/2024/06/leap-15.6.png' %}

NUREMBERGUE, Alemanha -- O lançamento do [Leap 15.6](https://get.opensuse.org/leap/15.6/) é oficial e abre caminho para que profissionais e organizações façam a transição para a distribuição empresarial do SUSE com suporte estendido ou se preparem para a próxima grande atualização, que será o Leap 16.

As demandas por sistemas operacionais robustos, seguros e estáveis ​​no setor da infraestrutura digital estão mais críticas do que nunca. A combinação do [Leap 15.6](https://get.opensuse.org/leap/15.6/), desenvolvido pela comunidade, e do [SUSE Linux Enterprise 15](https://www.suse.com/products/server/) Service Pack 6, que integra novos recursos e melhorias, oferece uma solução ótima para o gerenciamento de infraestrutura crítica. Notavelmente, as versões de suporte geral e de [suporte estendido](https://www.suse.com/products/long-term-service-pack-support/) do SUSE; esses [ciclos de vida de suporte ao produto](https://www.suse.com/lifecycle/) duram muito além da vida útil do Leap 15, garantindo um serviço mais longo e confiável para os usuários.

O [SLE 15](https://www.suse.com/products/server/) SP 6 é um lançamento de recurso, então os usuários podem esperar várias novas funcionalidades no lançamento do [Leap 15.6](https://get.opensuse.org/leap/15.6/).

Esse alinhamento garante que as empresas e os profissionais que utilizam o Leap para necessidades operacionais possam desfrutar de uma transição clara e apoiada para um ambiente empresarial, o que é crucial numa mudança para sistemas que requerem estabilidade a longo prazo e maior segurança. À medida que organizações traçam estratégias para suas melhorias, adotar uma solução de nível empresarial como o SUSE torna-se uma decisão estratégica, especialmente para aquelas que gerenciam redes extensas e dados críticos em vários setores.

Desde que foi lançado em [25 de maio de 2018][lk-1], a série Leap 15.x recebeu diversas adições, como tecnologias de contêineres, sistemas imutáveis, virtualização, desenvolvimento embarcado, juntamente com outros avanços da alta tecnologia. Um [aumento no uso](https://metrics.opensuse.org/) versão após versão mostra que empreendedores, amadores, profissionais e desenvolvedores estão consistentemente escolhendo o Leap como distribuição Linux preferida.

[lk-1]: https://linuxkamarada.com/pt/2018/05/25/baseado-no-codigo-do-enterprise-testado-milhoes-de-vezes-opensuse-leap-15-lancado/

O [Leap 15.6](https://get.opensuse.org/leap/15.6/) deve receber atualizações de manutenção e segurança até o final de 2025 para garantir suficiente sobreposição com a próxima versão. Isso dará aos usuários bastante tempo para atualizar para o sucessor dessa versão, que é o Leap 16, ou mudar para a versão de serviço de suporte estendido do SUSE. Usuários interessados ​​em suporte comercial podem usar uma [ferramenta de migração](https://en.opensuse.org/SDB:How_to_migrate_to_SLE) para migrar para a versão de suporte comercial do SUSE.

A inclusão do pacote [Cockpit](https://cockpit-project.org/)[^1] no openSUSE Leap 15.6 representa uma melhoria significativa na capacidade de gerenciamento de sistemas e contêineres para os usuários. Essa integração no Leap 15.6 melhora a usabilidade e o acesso, além de ligar a administração avançada do sistema à operação fácil por meio do navegador. A adição ressalta o compromisso do openSUSE em fornecer ferramentas poderosas que atendem tanto profissionais quanto amadores. O Leap não vem com uma política SELinux, portanto os recursos do SELinux para o Cockpit não estão funcionando.

As tecnologias de contêineres recebem um reforço com o Podman 4.8, que inclui suporte sob medida para o Nextcloud por meio de _quadlets_, juntamente com as versões mais recentes do Distrobox, Docker, python-podman, Skopeo, containerd, libcontainers-common, garantindo um sistema robusto de gerenciamento de contêineres. As tecnologias de virtualização também foram melhoradas, apresentando atualizações para Xen 4.18, KVM 8.2.2, libvirt 10.0 e virt-manager 4.1.

A versão 15.6 do Leap incorpora várias atualizações importantes de _software_ que melhoram o desempenho e a segurança. Ele integra Linux Kernel 6.4, que fornece _backports_ para alguns dos _drivers_ de _hardware_ mais recentes, que oferecem melhorias de desempenho. O OpenSSL 3.1 se torna o novo padrão e fornece recursos de segurança robustos e algoritmos de criptografia atualizados. Os sistemas de gerenciamento de banco de dados recebem atualizações significativas com MariaDB 10.11.6 e PostgreSQL 16. Redis 7.2 oferece recursos avançados de manipulação de dados e a pilha de _software_ é complementada com PHP 8.2 e Node.js 20; ambos receberam atualizações para melhor desempenho e segurança no desenvolvimento para a _web_. O Leap também terá o OpenJDK 21, fornecendo melhorias para melhor desempenho e segurança em aplicativos baseados em Java.

Atualizações no _software_ de telecomunicações são vistas com DPDK 22.11, Open vSwitch 3.1 e OVN 23.03.

O ambiente KDE avança com a introdução do KDE Plasma 5.27.11, que é a versão mais recente com suporte estendido (_Long Term Support_, LTS), Qt 5.15.12+kde151 e KDE Frameworks 5.115.0, assim como Qt6 versão 6.6.3, facilitando a operação suave dos aplicativos com ligações Python atualizadas para PyQt5 e PyQt6 alinhadas com o Python 3.11.

Muitos pacotes Python que não recebiam manutenção foram removidos como parte da transição para o Python 3.11; mais detalhes podem ser encontrados nas [notas de lançamento](https://doc.opensuse.org/release-notes/x86_64/openSUSE/Leap/15.6/index.html).

O GNOME 45 traz melhorias para a área de trabalho, adicionando recursos que elevam a experiência do usuário. As tecnologias de áudio passam por grandes atualizações com o lançamento do PulseAudio 17.0 e PipeWire 1.0.4, que melhoram a compatibilidade de _hardware_ e a funcionalidade Bluetooth, incluindo indicadores de nível de bateria dos dispositivos.

Coletivamente, essas atualizações melhoram a estabilidade do sistema e a experiência do usuário e tornam o Leap 15.6 uma escolha atraente para profissionais, empresas e organizações.

O Leap pode ser baixado em [get.opensuse.org](https://get.opensuse.org/leap/15.6/).

## Fim de vida

O openSUSE Leap 15.5 terá seu fim de vida (_End of Life_, EOL) seis meses a partir do lançamento de hoje. Seus usuários devem atualizar para o openSUSE Leap 15.6 dentro de seis meses a partir de hoje para continuar recebendo atualizações de segurança e manutenção.

## Baixe o Leap 15.6

Para baixar a imagem ISO, visite <https://get.opensuse.org/leap/>.

Se você tiver alguma dúvida sobre o lançamento ou encontrar um _bug_, adoraríamos escutar você em:

<https://t.me/openSUSE>

<https://chat.opensuse.org>

<https://lists.opensuse.org/opensuse-support/>

<https://discordapp.com/invite/openSUSE>

<https://www.facebook.com/groups/opensuseproject>

## Participe

O Projeto openSUSE é uma comunidade ao redor do mundo que promove o uso do Linux em todos os lugares. Ele cria duas das melhores distribuições Linux do mundo: a Tumbleweed, atualizada continuamente (_rolling-release_), e a Leap, distribuição híbrida de empresa e comunidade. O openSUSE trabalha continuamente de maneira conjunta, aberta, transparente e amigável como parte da comunidade internacional de _software_ livre e de código aberto (_Free and Open Source Software_, FOSS). O projeto é comandado por sua comunidade e depende das contribuições de indivíduos que trabalham como testadores, escritores, tradutores, especialistas em usabilidade, artistas, embaixadores ou desenvolvedores. O projeto abrange uma ampla variedade de tecnologias, pessoas com diferentes níveis de conhecimentos, que falam diferentes línguas e possuem diversas raízes culturais. Saiba mais sobre o Projeto openSUSE em [opensuse.org](https://opensuse.org).

[^1]: O _login_ com o usuário root vem desativado por padrão. Mais informações no artigo [_Try Cockpit in Leap Release Candidate_](https://news.opensuse.org/2024/04/29/try-cockpit-in-leap-rc/) (Experimente o Cockpit na versão candidata a lançamento do Leap).

**\*\*\* Dois _bugs_ relacionados ao Chrome com Wayland no [GNOME 45](https://en.opensuse.org/openSUSE:Known_bugs_15.6#GNOME) podem ser corrigidos em uma atualização. \*\*\***

## Retrospectiva

Forneça seus comentários à nossa equipe de lançamento visitando [survey.opensuse.org](https://survey.opensuse.org/) e respondendo à nossa [pesquisa retrospectiva](https://survey.opensuse.org/).

<sub><sup>(Imagem feita com a inteligência artificial DALL-E)</sup></sub>
