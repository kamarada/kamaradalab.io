---
date: '2023-11-01 10:00:00 GMT-3'
image: '/files/2023/11/15.5-beta.png'
layout: post
nickname: 'kamarada-15.5-beta'
title: 'Linux Kamarada 15.5 Beta: ajude a testar a melhor versão da distribuição'
---

{% include image.html src='/files/2023/11/15.5-beta.png' %}

O Projeto Linux Kamarada anuncia o lançamento da versão 15.5 Beta da [distribuição Linux][linux] de mesmo nome, baseada no [openSUSE Leap 15.5][leap-15.5]. Ela já está disponível para _[download]_.

<!--more-->

A página [Download] foi atualizada e agora oferece principalmente duas versões para _download_:

- a versão 15.4 Final, que você pode instalar no computador de casa ou do trabalho; e
- a versão 15.5 Beta, que você pode testar, se quiser ajudar no desenvolvimento.

O openSUSE Leap 15.5 foi lançado em [junho][leap-15.5] e desde então já recebeu diversas atualizações e correções. Portanto, o Linux Kamarada 15.5, embora ainda esteja em fase [beta], deve apresentar desde já uma estabilidade considerável. Ainda assim, pode ser que essa versão tenha _bugs_ e não esteja pronta para o uso diário. Se você encontrar um _bug_, por favor, reporte. Veja formas de entrar em contato na página [Ajuda]. Claro, _bugs_ podem ser encontrados e corrigidos a qualquer momento, mas quanto antes, melhor!

A forma mais fácil de testar o Linux é usando o [VirtualBox]. Para mais informações, leia:

- [VirtualBox: a forma mais fácil de conhecer o Linux sem precisar instalá-lo][VirtualBox]

Mas, se você quiser testar o Linux Kamarada no seu próprio computador, a forma mais fácil de fazer isso é usando um _pendrive_ inicializável que você pode criar com o [Ventoy]:

- [Ventoy: crie um pendrive multiboot simplesmente copiando imagens ISO para ele][Ventoy]

Se você deseja instalar o Linux Kamarada no computador para usar no dia-a-dia, o recomendado é que você instale a versão 15.4 Final, que já está pronta, e depois, quando a versão 15.5 estiver pronta, atualize. Para mais informações sobre a versão 15.4 Final, leia:

- [Linux Kamarada 15.4: mais funcional, bonito, polido e elegante do que nunca][kamarada-15.4]

## Novidades

As novidades da versão 15.5 se resumem a:

- os aplicativos da versão 15.4 foram mantidos em sua maioria, porém atualizados, em novas versões, com novas funcionalidades e _bugs_ corrigidos;

- o [Wayland] foi adotado como servidor gráfico padrão (ainda é possível usar o [X.Org], se necessário);

- a tela de boas vindas da imagem _live_ está mais polida e elegante, após ter passado por mudanças técnicas: a biblioteca de interface gráfica que ela usava foi migrada do [GTK] 3 para o GTK 4, e a área de trabalho que ela usava foi mudada do [Openbox] para o [GNOME Kiosk] (não houve mudanças nas funcionalidades ou na forma de usar essa tela, as mudanças foram apenas internas, o usuário não deve perceber diferença exceto pelo novo visual);

- foi adicionado o aplicativo **Configuração avançada de rede**, presente em várias outras distribuições Linux que também usam a área de trabalho [GNOME];

- o [padrão] do Linux Kamarada ([`patterns-kamarada-gnome.spec`][patterns-spec]) foi revisado de acordo com os [padrões do openSUSE], assim como cada pacote mencionado na definição da imagem _live_ do Linux Kamarada ([`appliance.kiwi`][appliance.kiwi]) foi revisto, essa mudança também é apenas técnica e interna, mas busca uma melhor qualidade de _software_;

- pacotes de tradução foram adicionados, alcançando um nível de tradução ainda mais completo da distribuição como um todo;

- o pacote **[openSUSE-repos-Leap]** passou a ser usado para adicionar os repositórios oficiais do openSUSE, tornando assim mais fácil a configuração desses repositórios (você pode conferir mais informações [aqui][openSUSE-repos-Leap]);

{% capture atualizacao-repos %}

Fiz uma publicação explicando em mais detalhes essa novidade:

- [Instale o novo pacote openSUSE-repos-Leap]({% post_url pt/2023-11-12-instale-o-novo-pacote-opensuse-repos-leap %})

{% endcapture %}

{% include update.html date="12/11/2023" message=atualizacao-repos %}

- o [Python] 2 foi removido do openSUSE Leap 15.5 e, por tabela, do Linux Kamarada 15.5 também, com isso também foi removido o jogo **[Copas]**, que [não recebia atualizações desde 2013][Copas] e dependia do Python 2;

- o [tema de ícones Papirus para LibreOffice][papirus-libreoffice], que [não recebia atualizações desde 2020][papirus-libreoffice], foi removido e substituído pelo tema de ícones padrão do [LibreOffice] (Elementary); e

- novos belos papéis de parede (ainda é possível usar os antigos ou o padrão do openSUSE, se você preferir).

## Atualizando para o 15.5 Beta

Se você já usa o Linux Kamarada 15.4 e confia na sua experiência como usuário, pode querer atualizar para o Linux Kamarada 15.5 Beta para ajudar a testar o processo de atualização. Em linhas gerais, deve ser suficiente seguir esse tutorial de atualização, substituindo `15.4` por `15.5`, onde houver:

- [Linux Kamarada e openSUSE Leap: como atualizar da versão 15.3 para a 15.4][upgrade-howto]

Como o Linux Kamarada 15.4 já configurava os repositórios com a variável `$releasever`, deve ser bem fácil atualizar para a versão 15.5. Exemplos de comandos do tutorial acima, já adaptados para a nova versão:

```
# zypper --releasever=15.5 ref
# zypper --releasever=15.5 dup --download-only --allow-vendor-change
```

Ao final da atualização, como uma novidade da versão 15.5, é recomendado (porém não necessário) que você instale o pacote **[openSUSE-repos-Leap]**:

```
# zypper in openSUSE-repos-Leap
```

Se você não sentir segurança de atualizar para a versão 15.5 Beta com essas dicas, não tem problema. Nos próximos dias, devo estar publicando uma versão atualizada do tutorial de atualização.

{% capture atualizacao-upgrade %}

Aqui está:

- [Linux Kamarada 15.4 e openSUSE Leap 15.4: como atualizar para a versão 15.5]({% post_url pt/2023-11-26-linux-kamarada-15-4-e-opensuse-leap-15-4-como-atualizar-para-a-versao-15-5 %})

{% endcapture %}

{% include update.html date="26/11/2023" message=atualizacao-upgrade %}

## Ficha técnica

Para que seja possível comparar esta versão com as anteriores e as futuras, segue um resumo do _software_ contido no Linux Kamarada 15.5 Beta Build 5.4:

- _kernel_ Linux 5.14.21
- servidor gráfico X.Org 21.1.4 (com Wayland 1.21.0 habilitado por padrão)
- área de trabalho GNOME 41.8 acompanhada de seus principais aplicativos (_core apps_), como Arquivos (antigo Nautilus), Calculadora, Terminal, Editor de texto (gedit), Captura de tela e outros
- suíte de aplicativos de escritório LibreOffice 7.5.4.1
- navegador Mozilla Firefox 115.4.0 ESR (navegador padrão)
- navegador Chromium 118 (navegador alternativo disponível)
- reprodutor multimídia VLC 3.0.18
- cliente de _e-mail_ Evolution 3.42.4
- centro de controle do YaST
- Brasero 3.12.3
- CUPS 2.2.7
- Drawing 1.0.1
- Firewalld 0.9.3
- GParted 1.4.0
- HPLIP 3.21.10
- Java (OpenJDK) 11.0.21
- KeePassXC 2.7.6
- Linphone 5.0.5
- PDFsam Basic 5.1.3
- Pidgin 2.14.8
- Python 3.6.15
- Samba 4.17.9
- Tor 0.4.7.13
- Transmission 3.00
- Vim 9.0.1632
- Wine 8.0
- instalador Calamares 3.2.62
- Flatpak 1.14.4
- jogos: AisleRiot (Paciência), Mahjongg, Minas (Campo minado), Nibbles (Cobras), Quadrapassel (Tetris), Reversi, Sudoku, Xadrez

Essa lista não é exaustiva, mas já dá uma noção do que se encontra na distribuição.

[linux]:                https://www.vivaolinux.com.br/linux/
[leap-15.5]:            {% post_url pt/2023-06-07-lancamento-do-opensuse-leap-15-5-amadurece-a-distribuicao-e-estabelece-transicao-tecnologica %}
[download]:             /pt/download
[beta]:                 https://pt.wikipedia.org/wiki/Ciclo_de_vida_de_liberação_de_software#Beta
[Ajuda]:                /pt/ajuda
[VirtualBox]:           {% post_url pt/2019-10-08-virtualbox-a-forma-mais-facil-de-conhecer-o-linux-sem-precisar-instala-lo %}
[Ventoy]:               {% post_url pt/2020-07-29-ventoy-crie-pendrives-multiboot-simplesmente-copiando-imagens-iso-para-ele %}
[kamarada-15.4]:        {% post_url pt/2023-02-27-linux-kamarada-15-4-mais-funcional-bonito-polido-e-elegante-do-que-nunca %}
[Wayland]:              https://wayland.freedesktop.org/
[X.Org]:                https://www.x.org/
[GTK]:                  https://www.gtk.org/
[Openbox]:              http://openbox.org/
[GNOME Kiosk]:          https://gitlab.gnome.org/GNOME/gnome-kiosk
[GNOME]:                https://www.gnome.org/
[padrão]:               {% post_url pt/2021-04-01-o-que-sao-padroes-e-como-instala-los-no-opensuse %}
[patterns-spec]:        https://gitlab.com/kamarada/patterns/-/blob/15.5/patterns-kamarada-gnome.spec
[padrões do openSUSE]:  https://software.opensuse.org/search?baseproject=openSUSE%3ALeap%3A15.5&q=patterns
[appliance.kiwi]:       https://gitlab.com/kamarada/Linux-Kamarada-GNOME/-/blob/15.5/appliance.kiwi
[openSUSE-repos-Leap]:  {% post_url pt/2023-11-12-instale-o-novo-pacote-opensuse-repos-leap %}
[Python]:               https://www.python.org/
[Copas]:                https://www.jejik.com/gnome-hearts
[papirus-libreoffice]:  https://github.com/PapirusDevelopmentTeam/papirus-libreoffice-theme
[LibreOffice]:          https://pt-br.libreoffice.org/
[upgrade-howto]:        {% post_url pt/2022-12-28-linux-kamarada-e-opensuse-leap-como-atualizar-da-versao-153-para-a-154 %}
