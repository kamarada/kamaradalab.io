---
date: '2024-08-04 20:00:00 GMT-3'
image: '/files/2018/10/how-to-veracrypt.jpg'
layout: post
nickname: 'veracrypt-src'
title: 'VeraCrypt: compilando e instalando a partir do código-fonte'
---

{% include image.html src='/files/2018/10/how-to-veracrypt.jpg' %}

Já falei aqui sobre o [VeraCrypt], que é um [_software_ livre][free-sw] e gratuito que nos permite criar volumes criptografados para guardar arquivos. Em um [primeiro _post_][VeraCrypt], vimos como instalá-lo e usá-lo no [Linux]. No [segundo][veracrypt-dropbox], vimos uma ideia de como usá-lo para guardar os arquivos do [Dropbox] em um volume criptografado.

Ocorre que o instalador disponível no [_site_ do VeraCrypt][veracrypt-downloads] funciona apenas com [PCs] de 32 ou 64 _bits_ (arquiteturas `x86` e `x64`). Uma novidade desde que escrevi aquele tutorial é que agora o _site_ oficial oferece pacotes [DEB] e [RPM] para as maiores distribuições Linux, mas novamente apenas para PCs. Também são oferecidos pacotes para [Raspberry Pi] (arquitetura `arm64`), mas apenas no formato DEB.

Se estamos usando o [openSUSE no Raspberry Pi][opensuse-raspberrypi], precisaríamos de pacotes RPM. Sendo assim, nenhuma das opções já compiladas oferecidas no _site_ do VeraCrypt nos atende. Se quisermos usar o VeraCrypt no openSUSE no Raspberry Pi, precisaremos compilá-lo a partir do seu código-fonte.

Como fazer isso é o que veremos a seguir.

Para referência futura, aqui uso a [versão 1.26.7][releases] do VeraCrypt (última versão estável), lançada em 1 de outubro de 2023, e a distribuição Linux [openSUSE Leap 15.6], instalada no Raspberry Pi seguindo [esse tutorial][opensuse-raspberrypi].

## Instalando as dependências

Antes de baixar o código-fonte, vamos instalar o que vamos precisar para compilá-lo (as dependências estão listadas no [GitHub do VeraCrypt][github]):

```
# zypper install git make gcc-c++ yasm pkg-config wxWidgets-3_0-devel fuse-devel pcsc-lite-devel
```

## Baixando e compilando o código-fonte

Agora sim baixe o código-fonte usando o [Git]:

```
$ git clone https://github.com/veracrypt/VeraCrypt.git
$ cd VeraCrypt
```

Mude para a _tag_ da versão mais recente (você pode conferir qual é no [GitHub][releases]):

```
$ git checkout VeraCrypt_1.26.7
```

Por fim, vamos compilar o código-fonte:

```
$ cd src
$ make
```

E instalar o VeraCrypt:

```
# make install
```

## Conclusão

Agora você pode [iniciar o VeraCrypt] e usá-lo normalmente.

{% include image.html src='/files/2024/08/veracrypt-raspberry-pi-pt.png' %}

Não deixe de conferir os outros tutoriais de VeraCrypt que temos no _site_:

- [Criptografia de arquivos com o VeraCrypt][VeraCrypt]
- [Criptografando os arquivos do Dropbox no disco com VeraCrypt][veracrypt-dropbox]

[VeraCrypt]:            {% post_url pt/2018-10-15-criptografia-de-arquivos-com-o-veracrypt %}
[free-sw]:              https://www.gnu.org/philosophy/free-sw.pt-br.html
[Linux]:                http://www.vivaolinux.com.br/linux/
[veracrypt-dropbox]:    {% post_url pt/2018-11-06-criptografando-os-arquivos-do-dropbox-no-disco-com-veracrypt %}
[Dropbox]:              https://db.tt/4VzN0K26
[veracrypt-downloads]:  https://www.veracrypt.fr/en/Downloads.html
[PCs]:                  https://pt.wikipedia.org/wiki/IBM_PC
[DEB]:                  https://pt.wikipedia.org/wiki/.deb
[RPM]:                  https://pt.wikipedia.org/wiki/RPM_(software)
[Raspberry Pi]:         {% post_url pt/2019-09-20-primeiros-passos-no-raspberry-pi-com-noobs-e-raspbian %}
[opensuse-raspberrypi]: {% post_url pt/2020-10-07-opensuse-no-raspberry-pi-4-parte-1-download-e-instalacao %}
[releases]:             https://github.com/veracrypt/VeraCrypt/releases
[openSUSE Leap 15.6]:   {% post_url pt/2024-06-12-leap-15-6-revela-opcoes-para-usuarios %}
[github]:               https://github.com/veracrypt/VeraCrypt
[Git]:                  https://git-scm.com/
[iniciar o VeraCrypt]:  {% post_url pt/2018-10-15-criptografia-de-arquivos-com-o-veracrypt %}#iniciando-o-veracrypt