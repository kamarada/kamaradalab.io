---
date: 2021-12-28 23:00:00 GMT-3
image: '/files/2021/12/15.3-beta.png'
layout: post
published: true
nickname: 'kamarada-15.3-rc'
title: 'Linux Kamarada libera versão candidata a lançamento (15.3 RC)'
---

{% include image.html src='/files/2021/12/15.3-beta.png' %}

O desenvolvimento está quase concluído: já está disponível para _[download]_ a versão candidata a lançamento do Linux Kamarada 15.3, baseado no [openSUSE Leap 15.3][leap-15.3].

Nesse momento, a página [Download] oferece duas versões para _download_:

- a versão [15.2 Final][kamarada-15.2], que você pode instalar no computador de casa ou do trabalho; e
- a versão 15.3 RC, que você pode testar, se quiser ajudar no desenvolvimento.

A forma mais fácil de testar o [Linux] é usando uma máquina virtual do [VirtualBox]:

- [VirtualBox: a forma mais fácil de conhecer o Linux sem precisar instalá-lo][virtualbox]

Mas, se você quiser testar o Linux Kamarada no seu próprio computador, a forma mais fácil de fazer isso é usando um _pendrive_ inicializável que você pode criar com o [Ventoy]:

- [Ventoy: crie um pendrive multiboot simplesmente copiando imagens ISO para ele][ventoy]

Se você deseja instalar o Linux Kamarada no computador para usar no dia-a-dia, o recomendado é que você instale a versão 15.2 Final, que já está pronta, e depois, quando a versão 15.3 estiver pronta, atualize. Para mais informações sobre a versão 15.2 Final, leia:

- [Linux Kamarada 15.2: venha para o lado verde, elegante e moderno da força!][kamarada-15.2]

## O que é versão candidata a lançamento?

Uma versão **candidata a lançamento** (do inglês [_release candidate_][rc], por isso abreviada **RC**) é uma versão de um _software_ que está praticamente pronto para ser lançado no mercado. Essa versão pode se tornar a versão final (estável) do _software_, a não ser que algum defeito (_bug_) sério seja percebido a tempo de ser corrigido antes do lançamento final. Nesse estágio do desenvolvimento, todas as funcionalidades inicialmente planejadas já estão presentes e nenhum recurso novo é adicionado.

Uma versão RC pode ser considerada um tipo de versão [beta]. Portanto, _bugs_ são esperados. Se você encontrar um _bug_, por favor, me avise por um dos canais listados na página [Ajuda].

Claro, _bugs_ podem ser encontrados e corrigidos a qualquer momento, mas quanto antes, melhor!

## Novidades

Além das novidades listadas quando a versão [15.3 Beta][kamarada-15.3-beta] foi lançada, nessa nova versão 15.3 RC você vai encontrar as seguintes:

- o [Flathub], principal repositório do [Flatpak], já vem pré-configurado

- alguns utilitários que podem ser úteis para diagnosticar problemas de _hardware_ e de sistemas de arquivos foram adicionados: **[inxi]**, **[lshw]**, **[lsof]**, **[lspci]** e **[smartctl]**

- diferente do [openSUSE Leap 15.2], que só tinha um repositório de atualização principal (`repo-update`), o openSUSE Leap 15.3 agora tem [três repositórios de atualização][release-notes], dos quais dois (os novos: `repo-backports-update` e `repo-sle-update`) não estavam presentes no Linux Kamarada 15.3 Beta e foram adicionados no 15.3 RC

## Atualizando para o 15.3 RC

Se você já usa o Linux Kamarada 15.2 e confia na sua experiência como usuário, pode querer atualizar para o Linux Kamarada 15.3 RC para ajudar a testar o processo de atualização. Confira como fazer isso no tutorial:

- [Linux Kamarada e openSUSE Leap: como atualizar da versão 15.2 para a 15.3][upgrade-to-15.3]

Se você já usa o Linux Kamarada 15.3 Beta e quer atualizar para o 15.3 RC, basta obter as atualizações para os pacotes, como de costume:

- [Como obter atualizações para o Linux openSUSE][howto-update]

Em ambos os casos, após a atualização, para manter todos na mesma página, adicione o repositório do Flathub, caso pretenda usar o Flatpak para instalar programas:

```
# flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
```

## Ficha técnica

Para que seja possível comparar esta versão com a anterior e as futuras, segue um resumo do _software_ contido no Linux Kamarada 15.3 RC Build 4.3:

- _kernel_ Linux 5.3.18
- servidor gráfico X.Org 1.20.3 (sem Wayland)
- área de trabalho GNOME 3.34.7 acompanhada de seus principais aplicativos (_core apps_), como Arquivos (antigo Nautilus), Calculadora, Terminal, Editor de texto (gedit), Captura de tela e outros
- suíte de aplicativos de escritório LibreOffice 7.1.4.2
- navegador Mozilla Firefox 91.4.0 ESR (navegador padrão)
- navegador Chromium 96 (navegador alternativo disponível)
- reprodutor multimídia VLC 3.0.16
- cliente de _e-mail_ Evolution
- centro de controle do YaST
- Brasero
- CUPS 2.2.7
- Firewalld 0.9.3
- GParted 0.31.0
- HPLIP 3.20.11
- Java (OpenJDK) 11.0.13
- KeePassXC 2.6.6
- KolourPaint 20.04.2
- Linphone 4.1.1
- PDFsam Basic 4.2.10
- Pidgin 2.13.0
- Python 2.7.18 e 3.6.15
- Samba 4.13.13
- Tor 0.4.6
- Transmission 2.94
- Vim 8.0
- Wine 6.0
- instalador Calamares 3.2.36
- Flatpak 1.10.5
- jogos: Aisleriot (Paciência), Copas, Iagno (Reversi), Mahjongg, Minas (Campo minado), Nibbles (Cobras), Quadrapassel (Tetris), Sudoku, Xadrez

Essa lista não é exaustiva, mas já dá uma noção do que se encontra na distribuição.

[download]:             /pt/download
[leap-15.3]:            {% post_url pt/2021-06-02-opensuse-leap-153-constroi-um-caminho-para-a-versao-empresarial %}
[kamarada-15.2]:        {% post_url pt/2020-09-11-linux-kamarada-15.2-venha-para-o-lado-verde-elegante-e-moderno-da-forca %}
[linux]:                https://www.vivaolinux.com.br/linux/
[virtualbox]:           {% post_url pt/2019-10-08-virtualbox-a-forma-mais-facil-de-conhecer-o-linux-sem-precisar-instala-lo %}
[ventoy]:               {% post_url pt/2020-07-29-ventoy-crie-pendrives-multiboot-simplesmente-copiando-imagens-iso-para-ele %}
[rc]:                   https://pt.wikipedia.org/wiki/Ciclo_de_vida_de_liberação_de_software#Release_candidate
[beta]:                 https://pt.wikipedia.org/wiki/Ciclo_de_vida_de_liberação_de_software#Beta
[ajuda]:                /pt/ajuda
[kamarada-15.3-beta]:   {% post_url pt/2021-12-11-linux-kamarada-15-3-beta-ajude-a-testar-a-melhor-versao-da-distribuicao %}
[Flathub]:              https://flathub.org/
[Flatpak]:              https://flatpak.org/
[inxi]:                 https://smxi.org/docs/inxi.htm
[lshw]:                 https://www.ezix.org/project/wiki/HardwareLiSter
[lsof]:                 https://man7.org/linux/man-pages/man8/lsof.8.html
[lspci]:                https://man7.org/linux/man-pages/man8/lspci.8.html
[smartctl]:             https://www.smartmontools.org/
[openSUSE Leap 15.2]:   {% post_url pt/2020-07-02-versao-15.2-do-opensuse-leap-traz-novos-e-empolgantes-pacotes-de-inteligencia-artificial-aprendizagem-de-maquina-e-containers %}
[release-notes]:        https://doc.opensuse.org/release-notes/x86_64/openSUSE/Leap/15.3/RELEASE-NOTES.pt_BR.html#installation-new-update-repos
[upgrade-to-15.3]:      {% post_url pt/2021-12-23-linux-kamarada-e-opensuse-leap-como-atualizar-da-versao-152-para-a-153 %}
[howto-update]:         {% post_url pt/2019-05-14-como-obter-atualizacoes-para-o-linux-opensuse %}
