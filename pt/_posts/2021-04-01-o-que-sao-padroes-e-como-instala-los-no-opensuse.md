---
date: '2021-04-01 23:20:00 GMT-3'
image: '/files/2021/04/patterns.jpg'
layout: post
nickname: 'patterns'
title: 'O que são padrões e como instalá-los no openSUSE'
---

{% include image.html src='/files/2021/04/patterns.jpg' %}

No [openSUSE], um **padrão** (do inglês _pattern_) é uma lista de pacotes que podem ser instalados para atender a alguma finalidade. Cada padrão tem um [pacote RPM][rpm] correspondente, com uma descrição da sua utilidade e uma lista de dependências (que podem ser [pacotes requeridos, recomendados][recomendados] ou sugeridos). Os padrões foram pensados para agrupar pacotes relacionados, mas sem inserir essa informação de agrupamento em cada pacote, de modo a manter enxuta a lista de dependências de cada pacote.

Esse conceito também existe em distribuições baseadas no openSUSE, como o [Linux Kamarada][kamarada-15.2].

Talvez fique melhor de entender com exemplos. Digamos que você queira instalar a área de trabalho [XFCE] no seu sistema. Uma opção seria pesquisar todos os pacotes necessários e marcá-los todos para instalação. Outra opção, mais prática, é instalar o padrão do XFCE.

No openSUSE e no Linux Kamarada, padrões podem ser instalados usando o gerenciador de pacotes **[zypper]**. Por exemplo, você poderia instalar o padrão do XFCE com o comando:

```
# zypper in -t pattern xfce
```

Outro exemplo seria a instalação de um [servidor LAMP][lamp] (jargão popular entre administradores de sistemas e desenvolvedores _web_, se refere à combinação de Linux, [Apache], [MariaDB] ou [MySQL] e [PHP] ou [Python]). Uma opção seria instalar o Apache, depois instalar o MariaDB, depois o PHP... outra opção é instalar o padrão do servidor LAMP:

```
# zypper in -t pattern lamp_server
```

Esse exemplo nos permite entender como padrões agrupam pacotes, mas sem inserir essa informação de agrupamento em cada pacote, o que aumentaria a lista de dependências de cada pacote: o Apache e o PHP estão relacionados, mas não faria sentido listar o PHP como pacote requerido, recomendado ou mesmo sugerido do Apache (nem todo mundo que usa Apache usa PHP, pode ser que o _site_ a ser servido seja feito em Python, em outra linguagem de programação ou até usando só [HTML]). Da mesma forma, não faria sentido listar o Apache como pacote requerido, recomendado ou sugerido do PHP (nem todo mundo que usa PHP usa Apache, com PHP também é possível fazer _scripts_ para usar no terminal). Mas faz sentido criar um padrão — o servidor LAMP — que depende de ambos Apache e PHP.

Cada padrão corresponde a um pacote RPM. Os comandos acima instalariam os pacotes **patterns-xfce-xfce** e **patterns-server-lamp_server** no sistema, respectivamente.

Para ver a lista dos padrões que você pode instalar em seu sistema, execute o comando:

```
# zypper search -t pattern
```

A lista pode ser bem extensa:

```
Carregando dados do repositório...
Lendo os pacotes instalados...

S | Nome                         | Resumo                               | Tipo
--+------------------------------+--------------------------------------+-------
  | 32bit                        | 32-Bit Runtime Environment           | padrão
i | apparmor                     | AppArmor                             | padrão
i | base                         | Minimal Base System                  | padrão
  | books                        | Documentation                        | padrão
  | ceph_base                    | Ceph base                            | padrão
  | console                      | Console Tools                        | padrão
  | container_runtime            | Container Runtime for non-clustere-> | padrão
  | container_runtime_kubernetes | Container Runtime for kubernetes c-> | padrão
  | devel_C_C++                  | C/C++ Development                    | padrão
  | devel_basis                  | Base Development                     | padrão
  | devel_gnome                  | GNOME Development                    | padrão
  | devel_java                   | Java Development                     | padrão
  | devel_kde_frameworks         | KDE Frameworks and Plasma Developm-> | padrão
  | devel_kernel                 | Linux Kernel Development             | padrão
  | devel_mono                   | .NET Development                     | padrão
  | devel_osc_build              | Tools for Packaging with Open Buil-> | padrão
  | devel_perl                   | Perl Development                     | padrão
  | devel_python3                | Python 3 Developement                | padrão
  | devel_qt5                    | Qt 5 Development                     | padrão
  | devel_rpm_build              | RPM Build Environment                | padrão
  | devel_ruby                   | Ruby Development                     | padrão
  | devel_tcl                    | Tcl/Tk Development                   | padrão
  | devel_web                    | Web Development                      | padrão
  | devel_yast                   | YaST Development                     | padrão
  | dhcp_dns_server              | DHCP and DNS Server                  | padrão
  | directory_server             | Directory Server (LDAP)              | padrão
  | documentation                | Help and Support Documentation       | padrão
i | enhanced_base                | Enhanced Base System                 | padrão
  | enlightenment                | Enlightenment                        | padrão
  | file_server                  | File Server                          | padrão
i | fonts                        | Fonts                                | padrão
  | games                        | Games                                | padrão
  | gateway_server               | Internet Gateway                     | padrão
  | gnome                        | GNOME Desktop Environment (Wayland)  | padrão
  | gnome_basic                  | GNOME Desktop Environment (Basic)    | padrão
  | gnome_x11                    | GNOME Desktop Environment (X11)      | padrão
  | hpc_compute_node             | HPC Basic Compute Node               | padrão
  | hpc_development_node         | HPC Development Packages             | padrão
  | hpc_libraries                | HPC Modularized Libraries            | padrão
  | hpc_workload_server          | HPC Workload Manager                 | padrão
  | imaging                      | Graphics                             | padrão
i | kamarada-gnome               | Linux Kamarada with GNOME desktop    | padrão
  | kde                          | KDE Applications and Plasma 5 Desk-> | padrão
  | kde_pim                      | KDE PIM Suite                        | padrão
  | kde_plasma                   | KDE Plasma 5 Desktop Base            | padrão
  | kubeadm                      | kubeadm Stack                        | padrão
  | kubernetes_utilities         | Utilities to manage kubernetes       | padrão
  | kvm_server                   | KVM Host Server                      | padrão
  | kvm_tools                    | KVM Virtualization Host and tools    | padrão
  | lamp_server                  | Web and LAMP Server                  | padrão
i | laptop                       | Laptop                               | padrão
  | lxde                         | LXDE Desktop Environment             | padrão
  | lxqt                         | LXQt Desktop Environment             | padrão
  | mail_server                  | Mail and News Server                 | padrão
  | mate                         | MATE Desktop Environment             | padrão
  | multimedia                   | Multimedia                           | padrão
  | network_admin                | Network Administration               | padrão
  | non_oss                      | Misc. Proprietary Packages           | padrão
  | office                       | Office Software                      | padrão
  | print_server                 | Print Server                         | padrão
  | sw_management                | Software Management                  | padrão
  | technical_writing            | Technical Writing                    | padrão
  | update_test                  | Tests for the Update Stack           | padrão
i | x11                          | X Window System                      | padrão
  | xen_server                   | Xen Virtual Machine Host Server      | padrão
  | xen_tools                    | XEN Virtualization Host and tools    | padrão
  | xfce                         | XFCE Desktop Environment             | padrão
  | yast2_basis                  | YaST System Administration           | padrão
```

Usuários do openSUSE e do Linux Kamarada também podem listar e instalar padrões pela interface gráfica, usando o **Gerenciamento de software** do [Centro de controle do YaST][yast].

Para ver a lista dos padrões que você pode instalar em seu sistema nessa interface gráfica, abra o menu **Ver**, no canto superior esquerdo da tela, e clique em **Padrões**:

{% include image.html src='/files/2021/04/patterns-01-pt.jpg' %}

Na lista à esquerda, você pode selecionar um padrão e na lista à direita, ver quais pacotes fazem parte dele:

{% include image.html src='/files/2021/04/patterns-02-pt.jpg' %}

Para instalar um padrão usando o YaST, marque-o na lista à esquerda. Os pacotes requeridos por esse padrão são marcados para instalação:

{% include image.html src='/files/2021/04/patterns-03-pt.jpg' %}

Você pode selecionar mais pacotes e/ou padrões para instalação, se quiser. Quando terminar, clique no botão **Aceitar**, no canto inferior direito, para realizar a instalação de fato.

[opensuse]:         https://www.opensuse.org/
[rpm]:              https://pt.wikipedia.org/wiki/RPM_(software)
[recomendados]:     {% post_url pt/2021-01-12-o-que-sao-pacotes-recomendados-e-como-instala-los-no-opensuse %}
[kamarada-15.2]:    {% post_url pt/2020-09-11-linux-kamarada-15.2-venha-para-o-lado-verde-elegante-e-moderno-da-forca %}
[xfce]:             https://xfce.org/
[zypper]:           https://pt.opensuse.org/Portal:Zypper
[lamp]:             https://pt.wikipedia.org/wiki/LAMP
[apache]:           https://httpd.apache.org/
[mariadb]:          https://mariadb.com/
[mysql]:            https://www.mysql.com/
[php]:              https://www.php.net/
[python]:           https://www.python.org/
[html]:             https://pt.wikipedia.org/wiki/HTML
[yast]:             http://yast.opensuse.org/
