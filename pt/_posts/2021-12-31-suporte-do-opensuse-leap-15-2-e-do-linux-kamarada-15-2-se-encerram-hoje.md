---
date: '2021-12-31 03:00:00 GMT-3'
image: '/files/2021/12/kamarada-15.2-eol.jpg'
layout: post
nickname: 'kamarada-15.2-eol'
title: 'Suporte do openSUSE Leap 15.2 e do Linux Kamarada 15.2 se encerram hoje'
---

{% include image.html src='/files/2021/12/kamarada-15.2-eol.jpg' %}

O suporte do [openSUSE Leap 15.2][leap-15.2], lançado em 02 de julho de 2020, se encerra hoje, 31 de dezembro de 2021, de acordo com a [_wiki_ do openSUSE][lifetime]. Isso quer dizer que o openSUSE Leap 15.2 não receberá mais atualizações, seja para corrigir _bugs_ de funcionalidades ou mesmo falhas de segurança.&nbsp;

O [Projeto openSUSE][opensuse] recomenda que usuários do openSUSE Leap 15.2 atualizem para o [openSUSE Leap 15.3][leap-15.3], lançado em 02 de junho de 2021 e que deve receber atualizações até o fim de novembro de 2022, também de acordo com a [_wiki_ do openSUSE][lifetime].

Até lá, muito provavelmente já teremos o [openSUSE Leap 15.4][leap-15.4], cujo lançamento é [previsto][leap-15.4-roadmap] para 08 de junho de 2022. Portanto, quando o Leap 15.4 for lançado, os usuários do Leap 15.3 terão aproximadamente 6 meses para fazer a atualização da distribuição.

Como o Linux Kamarada é baseado no openSUSE Leap, usuários do Linux Kamarada são, por tabela, usuários do openSUSE Leap também e recebem essas mesmas atualizações.

E também, como eu já havia avisado na [notícia de descontinuação do Linux Kamarada 15.1][kamarada-15.1-eol], "o fim do suporte de todas as próximas versões do Linux Kamarada se dará junto do fim do suporte da versão do openSUSE Leap equivalente".

Portanto, o suporte do [Linux Kamarada 15.2][kamarada-15.2], lançado em 11 de setembro de 2020, também se encerra hoje, 31 de dezembro de 2021.

Se você usa o Linux Kamarada 15.2, eu recomendo que atualize para o [Linux Kamarada 15.3][kamarada-15.3], lançado ontem e que deve receber atualizações pelo mesmo período que o openSUSE Leap 15.3, ou seja, até novembro de 2022.

Se você usa o openSUSE Leap (ou o Linux Kamarada) 15.2, veja como atualizar para o 15.3 em:

- [Linux Kamarada e openSUSE Leap: como atualizar da versão 15.2 para a 15.3][upgrade]

As instruções no tutorial acima se aplicam a ambas as distribuições.

Se tiver alguma dúvida, comente neste texto ou no tutorial acima. Você também pode conferir outras opções na página [Ajuda].

[leap-15.2]:                    {% post_url pt/2020-07-02-versao-15.2-do-opensuse-leap-traz-novos-e-empolgantes-pacotes-de-inteligencia-artificial-aprendizagem-de-maquina-e-containers %}
[lifetime]:                     https://en.opensuse.org/Lifetime
[opensuse]:                     https://www.opensuse.org/
[leap-15.3]:                    {% post_url pt/2021-06-02-opensuse-leap-153-constroi-um-caminho-para-a-versao-empresarial %}
[leap-15.4]:                    https://en.opensuse.org/Portal:15.4
[leap-15.4-roadmap]:            https://en.opensuse.org/openSUSE:Roadmap#Schedule_for_openSUSE_Leap_15.4
[kamarada-15.1-eol]:            {% post_url pt/2021-01-31-suporte-do-opensuse-leap-151-e-do-linux-kamarada-151-se-encerram-hoje %}
[kamarada-15.2]:                {% post_url pt/2020-09-11-linux-kamarada-15.2-venha-para-o-lado-verde-elegante-e-moderno-da-forca %}
[kamarada-15.3]:                {% post_url pt/2021-12-30-linux-kamarada-15-3-ano-novo-distribuicao-nova %}
[upgrade]:                      {% post_url pt/2021-12-23-linux-kamarada-e-opensuse-leap-como-atualizar-da-versao-152-para-a-153 %}
[ajuda]:                        /pt/ajuda
