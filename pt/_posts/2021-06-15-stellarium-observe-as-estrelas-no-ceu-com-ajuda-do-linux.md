---
date: 2021-06-15 11:50:00 GMT-3
image: '/files/2021/06/stellarium-08-pt.jpg'
layout: post
nickname: 'stellarium'
title: 'Stellarium: observe as estrelas no céu com ajuda do Linux'
---

Você gosta de [olhar as estrelas] no céu à noite? É uma visão bonita e agradável, especialmente se você está na zona rural, onde os prédios não limitam a visão do céu e não há luzes urbanas ofuscando a luz das estrelas. Também pode ser divertido tentar ligar as estrelas buscando [constelações]. E se eu te disser que tem um aplicativo gratuito para [Linux], [Android] e outros sistemas que pode te ajudar exatamente nisso? Hoje veremos como olhar para o céu pode se tornar ainda mais interessante com o Stellarium.

{% include image.html src="/files/2021/06/stellarium.jpg" %}

O [Stellarium] é um planetário [livre, gratuito e aberto][foss] para o seu computador ou dispositivo móvel. Ele mostra uma imagem realista do céu em três dimensões, semelhante ao que você pode ver com seus próprios olhos, ou com a ajuda de um binóculo ou [telescópio].

Veja a tradução de um trecho da introdução do [manual do Stellarium]:

> O _Stellarium_ é um projeto de _software_ que permite às pessoas usarem seus computadores de casa como um planetário virtual. Ele calcula as posições do Sol e da Lua, planetas e estrelas, e desenha como o céu se parece para um observador dependendo da sua localização e da hora. Ele também pode desenhar as constelações e simular fenômenos astronômicos, como chuvas de meteoros ou cometas, e eclipses solares ou lunares.
>
> O Stellarium pode ser usado como uma ferramenta educacional para ensinar sobre o céu noturno, como um auxílio para observação de astrônomos amadores que desejam planejar uma noite de observação ou mesmo posicionar seus telescópios para observar alvos, ou simplesmente como uma curiosidade (é divertido!). Por causa da alta qualidade dos gráficos produzidos pelo Stellarium, ele é usado em alguns produtos reais de projetores de planetário e instalações de projeção em museus. Alguns grupos amadores de astronomia usam-no para criar mapas do céu para descrever regiões do céu em artigos para jornais e revistas, e o recurso de recortes do céu que podem ser compartilhados (_exchangeable sky cultures_) ​​convida seu uso no campo da pesquisa e divulgação da Astronomia Cultural.

## Instalando o Stellarium no Linux

No [Linux Kamarada] e nas distribuições do [Projeto openSUSE] ([Leap e Tumbleweed]), você pode instalar o Stellarium a partir dos [repositórios oficiais do openSUSE][repos] de duas formas: pela interface gráfica, usando a instalação com 1 clique (*1-Click Install*), ou pelo terminal, usando o gerenciador de pacotes **zypper**. Escolha a que prefere.

Para instalar o Stellarium usando a instalação com 1 clique, clique no botão abaixo:

<p class='text-center'><a class='btn btn-sm btn-outline-primary' href='/downloads/stellarium.ymp'><i class='fas fa-bolt'></i> Instalação com 1 clique</a></p>

Para instalar o Stellarium usando o terminal, execute o comando a seguir:

```
# zypper in stellarium
```

Se você usa o [Flatpak], outra opção é instalar o Stellarium a partir do [Flathub]:

```
# flatpak install org.stellarium.Stellarium
```

O _download_ é demorado porque o aplicativo é baixado junto do seu banco de dados, permitindo seu uso mesmo sem conexão com a Internet. Se você vai viajar para uma região rural sem acesso à Internet, pode baixar o Stellarium na cidade, antes de pegar a estrada.

Se você não puder ou não quiser instalar o Stellarium, pode acessar sua versão _web_ usando o navegador, como veremos a seguir (nesse caso, você precisará de acesso à Internet).

## Iniciando o Stellarium no Linux

Logo após a instalação, você já deve ser capaz de iniciar o Stellarium.

Para iniciar o Stellarium, se você usa a área de trabalho [GNOME] (padrão do Linux Kamarada), abra o menu **Atividades**, no canto superior esquerdo da tela, digite `stellarium` e clique no ícone do aplicativo:

{% include image.html src="/files/2021/06/stellarium-01-pt.jpg" %}

Se você estiver conectado à Internet, o Stellarium vai tentar descobrir sua localização aproximada e mostrar como o céu é visto de onde você está:

{% include image.html src="/files/2021/06/stellarium-02-pt.jpg" %}

A interface do Stellarium é bem limpa, apresentando inicialmente o céu, as estrelas (algumas indicadas, como a [Acrux], da constelação do [Cruzeiro do Sul]), os [pontos cardeais] — na imagem acima, sudeste (SE), sul (S) e sudoeste (SO) — e mais algumas informações na barra de _status_, como localização, data e hora.

Muito do Stellarium pode ser controlado intuitivamente com o _mouse_. Alternativamente, muitas configurações podem ser ativadas ou desativadas com teclas de atalho (_hotkeys_).

## Melhorando a localização e a visão

Se você não estiver conectado à Internet, ou se estiver mas quiser melhorar a precisão do aplicativo, para informar manualmente sua localização ao Stellarium, deslize o cursor do _mouse_ pela borda esquerda da tela para mostrar alguns controles e clique no primeiro deles, **Janela de localização** (veja que um atalho para esse controle é a tecla **F6**):

{% include image.html src="/files/2021/06/stellarium-03-pt.jpg" %}

Na janela que aparece, você pode pesquisar pelo nome da cidade onde está:

{% include image.html src="/files/2021/06/stellarium-04-pt.jpg" %}

Você também pode informar manualmente suas [coordenadas geográficas] em termos de [latitude e longitude] e [elevação] (altitude acima do nível do mar).

Você pode obter as coordenadas de onde está do [Google Maps], pelo computador ou celular:

{% include image.html src="/files/2021/06/stellarium-05-pt.jpg" %}

Aproveite para verificar também para qual direção está olhando. No _app_ do Google Maps para Android, por exemplo, a metade vermelha da [bússola] aponta para o norte, o ponto azul mostra onde você está e o feixe azul, para onde o celular está apontado:

{% include image.html src="/files/2021/06/stellarium-06-pt.png" caption="Nesse exemplo, eu estou olhando para o leste." %}

Quando terminar, feche a janela de localização.

A visão do céu mostrada pelo Stellarium é tridimensional. Para mudar a direção da visão, arraste-a usando o _mouse_ ou use as **teclas de setas**. No meu caso, arrastei da esquerda para a direita para mudar a visão do sul (S), como estava inicialmente, para o leste (L):

{% include image.html src="/files/2021/06/stellarium-07-pt.jpg" %}

Você pode dar _zoom_ usando a roda do _mouse_ ou as teclas **Page Up** e **Page Down**.

Agora vamos mexer com mais alguns controles. Deslize o cursor do _mouse_ pela borda inferior da tela. Clique nos ícones para ativar ou desativar efeitos visuais.

Eu deixei ativados os seguintes:

- **Linhas das Constelações** (tecla de atalho **C**)
- **Rótulos das constelações** (**V**)
- **Figuras das Constelações** (**R**)
- **Superfície** (**G**)
- **Pontos cardeais** (**Q**)
- **Atmosfera** (**A**) e
- **Rótulos dos planetas** (**Alt + P**)

Veja como ficou:

{% include image.html src="/files/2021/06/stellarium-08-pt.jpg" %}

Agora é só comparar as estrelas que você vê no céu com as mostradas pelo aplicativo.

Como diz o _slogan_ do Projeto openSUSE, divirta-se bastante! (_Have a lot of fun!_)

## Saindo do Stellarium

Quando terminar de usar o Stellarium, deslize o cursor do _mouse_ pela borda inferior da tela e clique no último ícone, **Sair** (você também pode usar a combinação de teclas **Ctrl + Q**):

{% include image.html src="/files/2021/06/stellarium-09-pt.jpg" %}

## Stellarium Web

Existe uma versão _online_ do Stellarium, bem parecida com a versão _desktop_ e que também é controlada com o _mouse_. Para usá-la, abra o navegador e acesse [stellarium-web.org](https://stellarium-web.org/):

{% include image.html src="/files/2021/06/stellarium-10-pt.jpg" %}

A vantagem da versão _online_ é que nada precisa ser instalado ou baixado. Por outro lado, precisa de acesso à Internet, está disponível apenas em inglês e oferece menos recursos que a versão _desktop_, embora todos os recursos mais básicos já estejam presentes.

Quando terminar de usar o Stellarium Web, é só fechar a janela do navegador.

## Stellarium no Android

Também existe uma versão do Stellarium para Android, que pode ser obtida do [Google Play]:

{% include image.html src="/files/2021/06/stellarium-11-pt.jpg" caption="Essa imagem foi feita durante o dia. Imagino que a única estrela que dá pra observar nesse horário é o sol mesmo, às vezes a lua também." %}

Observe que o tamanho do _app_ para Android também é grande, porque ele também inclui o banco de dados que permite o uso _offline_. A vantagem desse _app_ é que é mais fácil controlar a visão: o _app_ já obtém a localização e a direção do GPS e da bússola do celular, você só precisa apontar o celular para onde quer que o aplicativo mostre as estrelas. O _app_ também permite informar a localização manualmente, caso você esteja sem sinal.

Outra vantagem do Stellarium para Android é que se você tocar em uma constelação, ele mostra informações da Wikipedia (se houver conexão com a Internet):

{% include image.html src="/files/2021/06/stellarium-12-pt.jpg" %}

## Onde obter mais informações

Se quiser mais informações sobre o Stellarium, acesse seu _site_ em [stellarium.org][Stellarium] e clique no _link_ **Guia do usuário** (o guia está em inglês):

{% include image.html src="/files/2021/06/stellarium-13-pt.jpg" %}

O código-fonte do Stellarium para _desktop_ e _web_ está disponível no [GitHub].

[olhar as estrelas]:        https://super.abril.com.br/ciencia/manual-como-observar-as-estrelas/
[constelações]:             https://brasilescola.uol.com.br/fisica/constelacoes.htm
[Linux]:                    http://www.vivaolinux.com.br/linux/
[Android]:                  https://www.android.com/
[Stellarium]:               https://stellarium.org/pt_BR/
[foss]:                     https://pt.wikipedia.org/wiki/Software_livre_e_de_código_aberto
[telescópio]:               https://pt.wikipedia.org/wiki/Telescópio
[manual do Stellarium]:     https://github.com/Stellarium/stellarium/releases/download/v0.21.0/stellarium_user_guide-0.21.0-1.pdf
[Linux Kamarada]:           {% post_url pt/2020-09-11-linux-kamarada-15.2-venha-para-o-lado-verde-elegante-e-moderno-da-forca %}
[Projeto openSUSE]:         https://www.opensuse.org/
[Leap e Tumbleweed]:        {% post_url pt/2020-12-06-opensuse-leap-e-opensuse-tumbleweed-qual-e-a-diferenca %}
[repos]:                    https://en.opensuse.org/Package_repositories#Official_Repositories
[Flatpak]:                  https://flatpak.org/
[Flathub]:                  https://flathub.org/apps/details/org.stellarium.Stellarium
[GNOME]:                    https://br.gnome.org/
[Acrux]:                    https://pt.wikipedia.org/wiki/Acrux
[Cruzeiro do Sul]:          https://pt.wikipedia.org/wiki/Crux
[pontos cardeais]:          https://brasilescola.uol.com.br/geografia/os-pontos-cardeais-suas-subdivisoes.htm
[coordenadas geográficas]:  https://brasilescola.uol.com.br/geografia/coordenadas-geograficas.htm
[latitude e longitude]:     https://brasilescola.uol.com.br/geografia/latitudes-longitudes.htm
[elevação]:                 https://pt.wikipedia.org/wiki/Elevação
[Google Maps]:              https://www.google.com.br/maps
[bússola]:                  https://brasilescola.uol.com.br/geografia/bussola.htm
[Google Play]:              https://play.google.com/store/apps/details?id=com.noctuasoftware.stellarium_free&hl=pt_BR
[github]:                   https://github.com/Stellarium
