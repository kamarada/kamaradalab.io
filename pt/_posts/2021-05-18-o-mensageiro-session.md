---
date: 2021-05-18 10:50:00 GMT-3
image: '/files/2021/05/im-session-pt.jpg'
layout: post
nickname: 'session'
title: 'O mensageiro Session'
---

{% include image.html src='/files/2021/05/im-session-pt.jpg' %}

O [Session] é um [aplicativo de mensagens], um _fork_ do [Signal] que leva a privacidade um passo adiante. Diferente de outros mensageiros instantâneos mais comuns, o Session não pede nenhuma informação pessoal, como número de telefone ou _e-mail_, para que você se cadastre e comece a usá-lo. Enquanto o Signal não coleta quase nenhum metadado do usuário, o Session não coleta nenhum metadado mesmo. Além disso, todas as mensagens entre usuários do Session são criptografadas de ponta a ponta — um recurso herdado do Signal. Isso significa que não há vazamento de dados, porque não há nada para vazar.

Pra você ter uma ideia de como isso é sério, a [política de privacidade] do Session começa assim:

> O Session nunca sabe quem é você, com quem está falando ou o conteúdo das suas mensagens.
>
> O aplicativo Session não coleta ou compartilha suas informações. Essa é a nossa política, em resumo.

Ao usar o Session pela primeira vez, escolha criar uma conta e ele gerará uma **Session ID** aleatória (única) para você. É um número hexadecimal comprido que funciona como um endereço que as pessoas podem usar para te encontrar no Session. Você não precisa passar seu número de telefone para outras pessoas. No [WhatsApp], no Signal ou no [Telegram], você adiciona contatos por meio dos números de telefone deles. No Session, você adiciona contatos por meio das Session IDs deles.

Um desafio em sistemas de comunicação verdadeiramente anônimos como o Session é que você **realmente** precisa verificar a identidade da pessoa que está adicionando. Certifique-se de que você e seu contato troquem suas Session IDs pessoalmente ou usando um canal de comunicação alternativo em que ambos confiem. Para facilitar a troca de Session IDs, o Session oferece um QR Code, de modo que vocês podem escanear o QR Code um do outro. Vocês também podem copiar e colar a Session ID um do outro.

O Session é um projeto da [Oxen Privacy Tech Foundation][optf] (OPTF, antes chamada de Loki Foundation), uma ONG com sede na Austrália que apoia o desenvolvimento de [_softwares_ livres][free-sw] que protegem a privacidade e a segurança no mundo digital. Além do Session para conversação privada, essa fundação também é responsável pela [Lokinet], uma rede descentralizada de "roteamento cebola" (_onion routing_) para navegação privada (semelhante ao [Tor]), e pela [Oxen], a criptomoeda que fornece incentivo econômico aos **nós de serviço** (_[service nodes]_) que formam a Lokinet. Ao menos na teoria, qualquer um pode [rodar um nó de serviço][run a service node] e receber Oxen por isso.

Diferente de outros mensageiros mais comuns, como WhatsApp, Signal ou Telegram, que possuem servidores centralizados, os servidores do Session são descentralizados: são os nós de serviço da Lokinet. O Session usa o "roteamento cebola" da Lokinet para enviar mensagens de forma segura e anônima. Uma **rede de "roteamento cebola"** (_onion routing network_) é uma rede de nós que permite a seus usuários enviar mensagens criptografadas anônimas. As "redes cebola" criptografam mensagens com várias camadas de criptografia e, em seguida, as enviam por meio de vários nós. Cada nó "desembrulha" (descriptografa) uma camada de criptografia. Assim, nenhum nó conhece, ao mesmo tempo, o destino e a origem da mensagem. Isso também garante que um servidor que recebe uma mensagem nunca sabe o endereço IP do remetente. Observe que uma desvantagem do "roteamento cebola" é que ele pode causar um pequeno atraso no envio e recebimento de mensagens.

{% include image.html src='/files/2021/05/onion-routing.jpg' %}

Caso você queira parar de usar sua Session ID (por exemplo, no caso de pessoas estarem enviando mensagens indesejadas), basta apagar os dados do aplicativo e gerar uma nova Session ID. Se lembre de informar aos seus contatos que sua Session ID mudou. Você pode gerar quantas Session IDs quiser dessa forma.

Mas se você quiser manter sua Session ID, como o Session não tem um servidor central que armazena informações sobre sua identidade, você é responsável por fazer o _backup_ da sua **frase de recuperação** (_recovery phrase_) você mesmo. É uma semente mnemônica que pode ser usada para restaurar sua Session ID em um novo dispositivo. Você pode anotar sua frase de recuperação em um pedaço de papel e, em seguida, guardá-lo em um local seguro ao qual só você tenha acesso.

Infelizmente, a frase de recuperação só serve para restaurar a Session ID. Contatos e mensagens são armazenados apenas localmente. Portanto, não podem ser recuperados se forem perdidos. E o Session ainda não fornece uma forma de fazer _backup_ de contatos e mensagens (nem mesmo localmente, como o Signal faz).

Além disso, o Session não oferece chamadas de voz nem de vídeo no momento.

Mas tudo isso (_backup_, chamadas de voz e de vídeo) são recursos planejados para o futuro.

Durante os meus testes, descobri que a mesma conta do Session não pode ser usada em vários dispositivos, como é possível com o Telegram e o Signal. Você pode restaurar uma Session ID no computador, mas aí o aplicativo móvel para de funcionar (e vice-versa).

Assim como o Signal, o Session permite enviar mensagens de texto e de voz, fotos, vídeos e arquivos, em conversas com contatos ou em grupo. No Session, os grupos são sempre fechados (privados), totalmente criptografados de ponta a ponta e comportam até 100 membros. Grupos abertos (públicos) são possíveis no Session, mas são tratados de forma muito diferente (você pode encontrar mais informações sobre eles no [FAQ] do Session). O Session também herda as mesmas [mensagens efêmeras] e [bloqueio de tela] do Signal.

Por último, mas não menos importante, o Session conta com um modo escuro muito bonito.

O Session é totalmente código-aberto: os códigos-fonte de todos os clientes e do servidor estão disponíveis no [GitHub], permitindo uma auditoria independente.

Os clientes para _desktop_, [Android] e [iOS] do Session foram submetidos a uma [auditoria de segurança pela Quarkslab][quarkslab].

Eu não recomendaria o Session para o público em geral (especialmente idosos) pela falta de alguns dos recursos presentes em outros mensageiros mais comuns. Creio que o Telegram ou o Signal já são bons o suficiente para quem procura uma simples alternativa ao WhatsApp. Mas se você é um usuário _expert_ em tecnologia que busca privacidade absoluta e se libertar de qualquer forma de vigilância, você deveria pelo menos testar o Session.

O _app_ do Session para Android pode ser obtido do [Google Play] ou do [GitHub][session-android] (como APK).

O _app_ do Session para Linux em formato [AppImage] pode ser obtido do [_site_ do Session][session-appimage]. Mas eu, pessoalmente, achei mais fácil instalá-lo a partir do [Flathub]:

```
# flatpak install network.loki.Session
```

Também existem versões do Session para outros sistemas. Veja mais em: [getsession.org](https://getsession.org/download/).

O Session possui uma página de perguntas frequentes ([FAQ]) muito explicativa no seu _site_.

## Semelhanças com o Bitcoin

Por curiosidade, vejo algumas semelhanças entre o Session e o [Bitcoin]. Ambos são sistemas **descentralizados** baseados em **criptografia**: o Bitcoin é uma moeda descentralizada, enquanto o Session é um serviço de mensagens descentralizado. Se você já conhece o Bitcoin, creio que entenderá alguns conceitos do Session com mais facilidade.

Lembre-se do meu [tutorial sobre o Bitcoin][bitcoin] que uma carteira tem um **endereço** que é basicamente um número hexadecimal comprido. Apenas examinando uma carteira, é impossível dizer a quem ela pertence (a menos que o dono tenha deixado descrições detalhadas para cada transação e/ou uma investigação seja realizada). O mesmo acontece com uma Session ID, que também é um número hexadecimal comprido: apenas olhando para ela, é impossível dizer a quem pertence. Ao contrário dos números de contas bancárias e de telefones celulares, não existe uma autoridade central onde se possa perguntar quem está por trás de um endereço Bitcoin ou de uma Session ID. Observe também que, assim como você anota uma **semente** para poder restaurar uma carteira Bitcoin, você anota uma frase de recuperação para poder restaurar uma Session ID.

O Bitcoin é **_peer-to-peer_** (ponto-a-ponto) e não depende de um servidor central. Embora o Session não seja _peer-to-peer_, ele também não depende de um servidor central, adotando [uma abordagem descentralizada para rotear mensagens][session-blog-1]. Isso torna o Bitcoin e o Session resistentes à censura: sem um ponto central de falha, é mais difícil derrubá-los.

Também interessante é o uso de uma **criptomoeda** — nomeadamente a Oxen — para recompensar financeiramente os servidores que transportam as mensagens do Session. O próprio Session não usa **_blockchain_** para armazenar mensagens. Mas, assim como a rede Bitcoin tem seu próprio _blockchain_ para registrar transações de Bitcoin, a rede Oxen tem seu próprio _blockchain_ para registrar transações de Oxen.

Enquanto o Bitcoin é baseado em [**_proof of work_**][session-blog-2] (prova de trabalho), a Oxen é baseada em [**_proof of stake_**][session-blog-2] (prova de participação). Não vou explicar essa diferença aqui (para isso, recomendo que você leia [esse texto][session-blog-2]), mas isso nos leva ao próximo ponto.

A Oxen tem outra função importante no Session, que é [proteger a rede de um ataque Sybil][session-blog-3]. Atacar a Lokinet não é fácil. Para rodar um nó de serviço, alguém precisa possuir (segurar, _hold_, travar temporariamente, _stake_) uma certa quantidade de Oxen (no momento da escrita, [15.000 Oxen][oxen dashboard], o equivalente a 98.000 reais ou 0,41 BTC). Somente os nós que atenderam ao requisito de posse têm permissão para participar da rede. Se um nó não atendeu a esse requisito, ele não pode participar da construção da _blockchain_, do roteamento de mensagens do Session ou do fornecimento de qualquer outro serviço da Lokinet. Isso desencoraja eventual mau comportamento dos nós e torna extremamente caro (pra não dizer impossível) um invasor assumir o controle da rede pela posse de vários nós.

Muitas vezes, obter Oxen suficiente para rodar um nó significa comprá-la no [mercado].

O Session é bastante interessante não só porque fornece privacidade às conversas, mas também porque propõe uma forma economicamente viável de suportar e proteger uma rede descentralizada de servidores que transportam as mensagens.

Esse texto faz parte da série:

- [Aplicativos de mensagens livres e focados em privacidade alternativos ao WhatsApp][aplicativo de mensagens]

[session]:                  https://getsession.org/
[aplicativo de mensagens]:  {% post_url pt/2021-05-08-aplicativos-de-mensagens-livres-e-focados-em-privacidade-alternativos-ao-whatsapp %}
[signal]:                   {% post_url pt/2021-05-11-o-mensageiro-signal %}
[política de privacidade]:  https://getsession.org/privacy-policy/
[whatsapp]:                 https://www.whatsapp.com/
[telegram]:                 {% post_url pt/2021-05-09-o-mensageiro-telegram %}
[optf]:                     https://optf.ngo/
[free-sw]:                  https://www.gnu.org/philosophy/free-sw.pt-br.html
[lokinet]:                  https://lokinet.org/
[tor]:                      https://www.torproject.org/
[oxen]:                     https://oxen.io/
[service nodes]:            https://docs.oxen.io/about-the-oxen-blockchain/oxen-service-nodes
[run a service node]:       https://docs.oxen.io/using-the-oxen-blockchain/oxen-service-node-guides/setting-up-an-oxen-service-node
[faq]:                      https://getsession.org/faq/
[mensagens efêmeras]:       https://support.signal.org/hc/pt-br/articles/360007320771-Definir-e-gerenciar-as-mensagens-efêmeras
[bloqueio de tela]:         https://support.signal.org/hc/pt-br/articles/360007059572-Bloqueio-de-tela
[github]:                   https://github.com/oxen-io
[android]:                  https://www.android.com/
[ios]:                      https://www.apple.com/br/ios/
[quarkslab]:                https://blog.quarkslab.com/resources/2021-05-04_audit-of-session-secure-messaging-application/20-08-Oxen-REP-v1.4.pdf
[google play]:              https://play.google.com/store/apps/details?id=network.loki.messenger
[session-android]:          https://github.com/loki-project/session-android/releases
[appimage]:                 https://appimage.org/
[session-appimage]:         https://getsession.org/download/
[flathub]:                  https://flathub.org/apps/details/network.loki.Session

[bitcoin]:                  {% post_url pt/2018-12-12-bitcoin-para-iniciantes-com-a-carteira-electrum %}
[session-blog-1]:           https://getsession.org/centralisation-vs-decentralisation-in-private-messaging/
[session-blog-2]:           https://loki.network/2020/01/24/blockchain-consensus-mechanisms/
[session-blog-3]:           https://getsession.org/how-session-protects-your-anonymity-with-blockchain-and-crypto/
[oxen dashboard]:           https://lokidashboard.com/
[mercado]:                  https://coinmarketcap.com/pt-br/currencies/oxen/markets/
