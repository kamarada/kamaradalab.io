---
date: 2024-05-27 23:59:00 GMT-3
image: '/files/2024/05/kamarada-15.5.jpg'
layout: post
published: true
nickname: 'kamarada-15.5-final'
title: 'Linux Kamarada 15.5: mais alinhado com o openSUSE Leap e com outras distribuições'
---

{% include image.html src='/files/2024/05/kamarada-15.5.jpg' %}

É com enorme satisfação que venho a público anunciar que o **Linux Kamarada 15.5** está pronto para ser usado por todos!&nbsp;

A distribuição Linux Kamarada é baseada no [openSUSE Leap] e se destina ao uso em _desktops_ em casa e no trabalho, seja em empresas privadas ou em órgãos públicos. Contém os programas essenciais a qualquer instalação do [Linux] e uma área de trabalho com visual moderno e agradável.

O Linux Kamarada 15.5 não é uma distribuição Linux criada "do nada", mas sim baseada em outra distribuição maior, o [openSUSE Leap 15.5]. Os números de versões iguais (15.5) refletem o alinhamento entre as distribuições. Enquanto o openSUSE Leap tem um propósito mais geral, fornecendo um sistema operacional estável para computadores pessoais e servidores, assim como ferramentas para desenvolvedores e administradores de sistemas, o Linux Kamarada é focado em computadores pessoais e em usuários iniciantes.

Novos usuários podem encontrar a nova versão do Linux Kamarada na página [Download]. Se você já usa o Linux Kamarada, confira orientações sobre como atualizar para a versão nova mais adiante.

Confira a seguir o que mudou no Linux Kamarada da versão anterior ([15.4]) para a versão atual (15.5).

[openSUSE Leap]:        {% post_url pt/2020-12-06-opensuse-leap-e-opensuse-tumbleweed-qual-e-a-diferenca %}
[Linux]:                https://www.vivaolinux.com.br/linux/
[openSUSE Leap 15.5]:   {% post_url pt/2023-06-07-lancamento-do-opensuse-leap-15-5-amadurece-a-distribuicao-e-estabelece-transicao-tecnologica %}
[Download]:             /pt/download
[15.4]:                 {% post_url pt/2023-02-27-linux-kamarada-15-4-mais-funcional-bonito-polido-e-elegante-do-que-nunca %}

<div class="d-flex align-items-center my-3">
    <div class="flex-shrink-0">
        <img src="/files/2024/05/wayland.svg" style="max-width: 64px;">
    </div>
    <div class="flex-grow-1 ms-3">
        <h2 class="mb-0">Wayland como servidor gráfico padrão</h2>
    </div>
</div>

O [Wayland] é um protocolo de servidor de exibição projetado para ser o sucessor do [sistema de janelas X], atualmente implementado pelo [X.Org]. Grosso modo, o Linux usa o Wayland ou o X.Org para desenhar na tela as interfaces gráficas dos aplicativos. Sem Wayland nem X.Org, você conseguiria usar apenas o terminal e aplicativos de modo texto.

O Projeto Wayland foi iniciado em 2008 por um desenvolvedor da [Red Hat] que já havia feito várias contribuições para o X.Org. Atualmente, [a maioria das distribuições Linux][wayland-adoption] já suporta o Wayland de fábrica, inclusive trazendo-o como padrão no lugar do X.Org, a exemplo do [Fedora] desde a versão 25 (lançada em novembro de 2016), do [Red Hat Enterprise Linux][rhel] 8 (maio de 2019), do [Debian] 10 (julho de 2019) e do [Ubuntu] 21.04. No openSUSE Leap, o Wayland é usado por padrão com a área de trabalho [GNOME] desde a [versão 15][leap-15] (maio de 2018).

No caso do Linux Kamarada, eu optei por adiar a adoção do Wayland por padrão porque percebia vários problemas de compatibilidade aqui e ali com aplicativos (a exemplo do [TeamViewer]) e placas de vídeo (a exemplo da [NVIDIA]). Mas agora me parece que o suporte ao Wayland está bem maduro e é seguro adotá-lo por padrão, alinhando o Linux Kamarada 15.5 com as demais distribuições Linux.

Essa mudança é interna e, para a maioria dos usuários, deve passar despercebida, exceto talvez pela melhor qualidade gráfica e desempenho.

Para saber se você está usando o Wayland ou o X.Org, abra o aplicativo **Configurações** do GNOME, vá na seção **Sobre** e verifique o **Sistema de janelas**:

{% include image.html src='/files/2024/05/kamarada-15.5-wayland-1-pt.jpg' %}

Note que você pode reverter para o X.Org, se precisar. Para isso, na tela de _login_, clique no ícone da engrenagem e selecione a sessão do tipo **GNOME sobre Xorg**:

{% include image.html src='/files/2024/05/kamarada-15.5-wayland-2-pt.jpg' %}

[Wayland]:              https://wayland.freedesktop.org/
[sistema de janelas X]: https://pt.wikipedia.org/wiki/X_Window_System
[X.Org]:                https://www.x.org/

[Red Hat]:              https://www.redhat.com/pt-br
[wayland-adoption]:     https://en.wikipedia.org/wiki/Wayland_(protocol)#Desktop_Linux_distributions
[Fedora]:               https://fedoraproject.org/pt-br/
[rhel]:                 https://www.redhat.com/pt-br/technologies/linux-platforms/enterprise-linux
[Debian]:               https://www.debian.org/
[Ubuntu]:               https://ubuntu.com/
[GNOME]:                https://br.gnome.org/
[leap-15]:              https://linuxkamarada.com/pt/2018/05/25/baseado-no-codigo-do-enterprise-testado-milhoes-de-vezes-opensuse-leap-15-lancado/

[TeamViewer]:           https://linuxkamarada.com/pt/2019/07/05/18-aplicativos-que-voce-pode-usar-do-mesmo-jeito-no-linux-e-no-windows-parte-1/#6-teamviewer
[NVIDIA]:               https://www.nvidia.com/pt-br/

<div class="d-flex align-items-center my-3">
    <div class="flex-shrink-0">
        <img src="/files/2021/12/media-optical.svg" style="max-width: 64px;">
    </div>
    <div class="flex-grow-1 ms-3">
        <h2 class="mb-0">Tela de boas vindas atualizada</h2>
    </div>
</div>

A tela de boas vindas da imagem _live_ está mais polida e elegante, após ter passado por mudanças técnicas: a biblioteca de interface gráfica que ela usava foi migrada do [GTK] 3 para o GTK 4, e a área de trabalho que ela usava foi mudada do [Openbox] para o [GNOME Kiosk] (não houve mudanças nas funcionalidades ou na forma de usar essa tela, as mudanças foram apenas internas, o usuário não deve perceber diferença exceto pelo novo visual).

<div class="row">
    <div class="col-md">
        {% include image.html src='/files/2024/05/kamarada-15.5-firstboot-1-pt.jpg' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2024/05/kamarada-15.5-firstboot-2-pt.jpg' %}
    </div>
</div>

[GTK]:                  https://www.gtk.org/
[Openbox]:              http://openbox.org/
[GNOME Kiosk]:          https://gitlab.gnome.org/GNOME/gnome-kiosk

<div class="d-flex align-items-center my-3">
    <div class="flex-shrink-0">
        <img src="/files/2023/02/system-software-install.svg" style="max-width: 64px;">
    </div>
    <div class="flex-grow-1 ms-3">
        <h2 class="mb-0">Mudanças nos aplicativos</h2>
    </div>
</div>

Os aplicativos da versão 15.4 foram mantidos em sua maioria, porém atualizados, em novas versões, com novas funcionalidades e _bugs_ corrigidos.

Foi adicionado o aplicativo **Configuração avançada de rede**, presente em várias outras distribuições Linux que também usam a área de trabalho GNOME.

O [Python] 2 foi removido do openSUSE Leap 15.5 e, por tabela, do Linux Kamarada 15.5 também, com isso também foi removido o jogo **[Copas]**, que [não recebia atualizações desde 2013][Copas] e dependia do Python 2.

O [tema de ícones Papirus para LibreOffice][papirus-libreoffice], que [não recebia atualizações desde 2020][papirus-libreoffice], foi removido e substituído pelo tema de ícones padrão do [LibreOffice] (Elementary).

Pacotes de tradução foram adicionados, alcançando um nível de tradução ainda mais completo da distribuição como um todo.

<div class="row">
    <div class="col-md">
        {% include image.html src='/files/2024/05/kamarada-15.5-nm-connection-editor-pt.jpg' caption='Configuração avançada de rede' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2024/05/kamarada-15.5-libreoffice-elementary-pt.jpg' caption='LibreOffice com seu tema de ícones padrão (Elementary)' %}
    </div>
</div>

Outras mudanças técnicas e internas, visando uma melhor qualidade de _software_, incluem:

 - o pacote **[openSUSE-repos-Leap]** passou a ser usado para adicionar os repositórios padrão do openSUSE, tornando assim mais fácil a configuração desses repositórios (você pode conferir mais informações [aqui][openSUSE-repos-Leap]); e

- o [padrão] do Linux Kamarada ([`patterns-kamarada-gnome.spec`][patterns-spec]) foi revisado de acordo com os [padrões do openSUSE], assim como cada pacote mencionado na definição da imagem _live_ do Linux Kamarada ([`appliance.kiwi`][appliance.kiwi]) foi revisto.

[Python]:               https://www.python.org/
[Copas]:                https://www.jejik.com/gnome-hearts
[papirus-libreoffice]:  https://github.com/PapirusDevelopmentTeam/papirus-libreoffice-theme
[LibreOffice]:          https://pt-br.libreoffice.org/
[openSUSE-repos-Leap]:  {% post_url pt/2023-11-12-instale-o-novo-pacote-opensuse-repos-leap %}
[padrão]:               https://linuxkamarada.com/pt/2021/04/01/o-que-sao-padroes-e-como-instala-los-no-opensuse/
[patterns-spec]:        https://gitlab.com/kamarada/patterns/-/blob/15.5/patterns-kamarada-gnome.spec
[padrões do openSUSE]:  https://software.opensuse.org/search?baseproject=openSUSE%3ALeap%3A15.5&q=patterns
[appliance.kiwi]:       https://gitlab.com/kamarada/Linux-Kamarada-GNOME/-/blob/15.5/appliance.kiwi

<div class="d-flex align-items-center my-3">
    <div class="flex-shrink-0">
        <img src="/files/2021/12/multimedia-photo-viewer.svg" style="max-width: 64px;">
    </div>
    <div class="flex-grow-1 ms-3">
        <h2 class="mb-0">Novos planos de fundo</h2>
    </div>
</div>

Como de costume, essa nova versão vem com novos planos de fundo, com fotos de belas praias da cidade de [Florianópolis]:

<div class="row">
    <div class="col-md">
        {% include image.html src='/files/2024/05/kamarada-15.5-background-1-pt.jpg' caption='Ribeirão Capivari (padrão)' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2024/05/kamarada-15.5-background-2-pt.jpg' caption='Mirante Para Baía Sul' %}
    </div>
</div>

Nas versões mais recentes do GNOME, a tela de bloqueio usa o mesmo plano de fundo da área de trabalho. Mesmo assim, mantendo a tradição das versões anteriores do Linux Kamarada, na versão 15.5 eu incluí dois novos planos de fundo.

Se preferir, você também pode usar os planos de fundo de versões anteriores do Linux Kamarada ou o padrão do openSUSE Leap (ou definir suas próprias imagens como planos de fundo, claro).

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        {% include image.html src='/files/2024/05/kamarada-15.5-backgrounds-pt.jpg' caption='Planos de fundo inclusos no Linux Kamarada 15.5' %}
    </div>
    <div class="col-md-2"></div>
</div>

[Florianópolis]:    https://pt.wikipedia.org/wiki/Florian%C3%B3polis

## Onde baixo o Linux Kamarada?

A página [Download] foi atualizada com o _link_ para _download_ da versão 15.5 Final.

**Observação:** não é recomendado usar o Linux Kamarada em servidores, embora seja possível. Nesse caso, a distribuição openSUSE Leap é mais adequada, por fornecer uma opção de instalação própria para servidores, sem área de trabalho.

## O que faço em seguida?

Quando o _download_ estiver concluído, verifique a integridade da imagem ISO calculando sua soma de verificação (_checksum_), que deve coincidir com a soma que aparece na página [Download]. Se a soma não corresponder, faça o _download_ novamente.

Se não estiver com pressa, é recomendado verificar também a autenticidade da imagem ISO.

A impressão digital (_fingerprint_) da chave pública do Projeto Linux Kamarada é:

```
6b18 52e7 764f b302 b805 a4a0 a575 bcce 1737 8ecc
```

Para importá-la, execute os comandos a seguir:

```
$ wget -O kamarada.gpg https://build.opensuse.org/projects/home:kamarada:15.5/signing_keys/download?kind=gpg
$ gpg --import kamarada.gpg
```

Para mais informações sobre essas verificações, leia:

- [Verificação de integridade e autenticidade com SHA-256 e GPG][how-to-verify-iso]

Com a imagem ISO baixada e as verificações feitas e bem-sucedidas, você tem 3 opções:

<div class="d-flex">
    <div class="flex-shrink-0">
        <img src="/files/2020/02/disk-burner.svg" style="max-width: 48px;">
    </div>
    <div class="flex-grow-1 ms-3">
        <h6>1) Gravar a imagem ISO em um DVD (gerando, assim, um LiveDVD)</h6>

        <p>Use um aplicativo como o <a href="https://cdburnerxp.se/">CDBurnerXP</a> (no Windows), o <a href="https://apps.kde.org/pt-br/k3b/">K3b</a> (em um Linux com KDE) ou o <a href="https://wiki.gnome.org/Apps/Brasero">Brasero</a> (em um Linux com GNOME) para gravar a imagem ISO em um DVD. Ligue o computador com o LiveDVD na unidade de DVD para iniciar o Linux Kamarada.</p>

        <p>Esse <em>post</em> pode te ajudar com a gravação do DVD:</p>

        <ul><li><a href="{% post_url pt/2016-04-22-como-gravar-um-livedvd %}">Como gravar um LiveDVD</a></li></ul>
    </div>
</div>

<div class="d-flex">
    <div class="flex-shrink-0">
        <img src="/files/2020/02/usb-creator.svg" style="max-width: 48px;">
    </div>
    <div class="flex-grow-1 ms-3">
        <h6>2) Gravar a imagem ISO em um <em>pendrive</em> (gerando, assim, um LiveUSB)</h6>

        <p>Use o <a href="https://www.ventoy.net/">Ventoy</a> (disponível para Windows e Linux) para preparar o <em>pendrive</em> e copie a imagem ISO para ele. Ligue o computador com o LiveUSB para iniciar o Linux Kamarada.</p>

        <p>Esse <em>post</em> pode te ajudar com a preparação do LiveUSB:</p>

        <ul><li><a href="{% post_url pt/2020-07-29-ventoy-crie-pendrives-multiboot-simplesmente-copiando-imagens-iso-para-ele %}">Ventoy: crie um pendrive multiboot simplesmente copiando imagens ISO para ele</a></li></ul>
    </div>
</div>

<div class="d-flex">
    <div class="flex-shrink-0">
        <img src="/files/2020/02/virtualbox.svg" style="max-width: 48px;">
    </div>
    <div class="flex-grow-1 ms-3">
        <h6>3) Criar uma máquina virtual e usar a imagem ISO para iniciá-la</h6>

        <p>Essa opção te possibilita testar o Linux Kamarada sem precisar reiniciar o computador e sair do sistema operacional que você já usa.</p>

        <p>Para mais informações, leia:</p>

        <ul><li><a href="{% post_url pt/2019-10-08-virtualbox-a-forma-mais-facil-de-conhecer-o-linux-sem-precisar-instala-lo %}">VirtualBox: a forma mais fácil de conhecer o Linux sem precisar instalá-lo</a></li></ul>
    </div>
</div>

<div class="d-flex mb-3">
    <div class="flex-grow-1">
        Depois de testar o Linux Kamarada, caso queira instalá-lo no seu computador ou na máquina virtual, inicie o instalador fazendo um duplo-clique nesse ícone, situado na Área de Trabalho.
    </div>
    <div class="flex-shrink-0 ms-3">
        <img src="/files/2021/12/calamares.svg" style="max-width: 48px;">
    </div>
</div>

O instalador fará o particionamento, a cópia do sistema para o computador e todas as configurações iniciais (idioma, leiaute de teclado, fuso horário, usuário e senha, etc.). Ao final, reinicie o computador para começar a usar o sistema instalado.

[how-to-verify-iso]:    {% post_url pt/2018-10-06-verificacao-de-integridade-e-autenticidade-com-sha-256-e-gpg %}

## E se eu já uso o Linux Kamarada?

A imagem ISO não se destina a atualizações, apenas para testes e instalações novas.

Se você já usa o Linux Kamarada 15.4, pode atualizar para o 15.5 seguindo o tutorial:

- [Linux Kamarada e openSUSE Leap: como atualizar da versão 15.4 para a 15.5][upgrade-to-15.5]

Se você usa uma versão em desenvolvimento do Linux Kamarada 15.5 ([Beta] ou [RC]), basta obter as atualizações para os pacotes, como de costume:

- [Como obter atualizações para o Linux openSUSE][howto-update]

[upgrade-to-15.5]:  {% post_url pt/2023-11-26-linux-kamarada-15-4-e-opensuse-leap-15-4-como-atualizar-para-a-versao-15-5 %}
[Beta]:             {% post_url pt/2023-11-01-linux-kamarada-15-5-beta-ajude-a-testar-a-melhor-versao-da-distribuicao %}
[RC]:               {% post_url pt/2024-05-08-linux-kamarada-libera-versao-candidata-a-lancamento-15-5-rc %}
[howto-update]:     {% post_url pt/2019-05-14-como-obter-atualizacoes-para-o-linux-opensuse %}

## E se eu já uso o openSUSE Leap?

Você já usa o openSUSE Leap e quer transformá-lo no Linux Kamarada? Simples!

Basta adicionar o repositório do Linux Kamarada e instalar o pacote **[patterns-kamarada-gnome]**.

Você pode fazer isso de duas formas: pela interface gráfica, usando a instalação com 1 clique (*1-Click Install*), ou pelo terminal, usando o gerenciador de pacotes **zypper**. Escolha a que prefere.

Para instalar usando a instalação com 1 clique, clique no botão abaixo:

<p class='text-center'>
    <a class='btn btn-primary' href='/downloads/patterns-kamarada-gnome-pt.ymp'>
        <i class='fas fa-bolt'></i> Instalação com 1 clique
    </a>
</p>

Para instalar usando o terminal, primeiro adicione o repositório do Linux Kamarada:

```
# zypper addrepo -f -K -n "Linux Kamarada" -p 95 "https://packages.linuxkamarada.com/\$releasever/openSUSE_Leap_\$releasever/" kamarada
```

Depois, instale o pacote **patterns-kamarada-gnome**:

```
# zypper in patterns-kamarada-gnome
```

Se você já usa a área de trabalho GNOME, provavelmente será necessário baixar poucos pacotes. Se você usa outra área de trabalho, o tamanho do _download_ será maior.

Quando a instalação terminar, se você criar um novo usuário, perceberá que ele receberá as configurações padrão do Linux Kamarada (como tema, papel de parede, etc.). Usuários já existentes podem seguir usando suas personalizações ou ajustar a aparência do sistema (por exemplo, usando os aplicativos **Ajustes** e **Configurações**).

[patterns-kamarada-gnome]:  https://software.opensuse.org/package/patterns-kamarada-gnome

## Onde obtenho ajuda?

A página [Ajuda] indica alguns lugares onde é possível obter auxílio com o Linux Kamarada, o [openSUSE] ou distribuições Linux em geral (incluindo essas duas).

O canal de suporte preferido pelos usuários tem sido o grupo [@LinuxKamarada](https://t.me/LinuxKamarada) no [Telegram], que é um aplicativo de mensagens que pode ser instalado ou usado a partir do navegador.

[Ajuda]:    /pt/ajuda
[openSUSE]: https://www.opensuse.org/
[Telegram]: {% post_url pt/2021-05-09-o-mensageiro-telegram %}

## Até quando receberá suporte?

De acordo com a [_wiki_ do openSUSE][lifetime], o openSUSE Leap 15.5 deve receber atualizações até o fim de dezembro de 2024.

Como o Linux Kamarada é baseado no openSUSE Leap, usuários do Linux Kamarada são, por tabela, usuários do openSUSE Leap também, e recebem essas mesmas atualizações.

Portanto, o Linux Kamarada 15.5 também receberá suporte até o fim de dezembro de 2024.

[lifetime]: https://en.opensuse.org/Lifetime

## Onde obtenho os códigos-fonte?

Como todo projeto de _software_ livre, o Linux Kamarada disponibiliza seus códigos-fonte para quem quiser estudá-los, adaptá-los ou contribuir com o projeto.

O desenvolvimento do Linux Kamarada ocorre no [GitLab] e no [Open Build Service][obs]. Lá podem ser obtidos os códigos-fonte dos pacotes desenvolvidos especificamente para esse projeto (até mesmo [o código-fonte deste _site_][kamarada-website] que você lê está disponível).

Os códigos-fonte de pacotes herdados do openSUSE Leap podem ser obtidos diretamente dessa distribuição. Se precisar de ajuda para fazer isso, entre em contato, posso ajudar.

[GitLab]:           https://gitlab.com/kamarada
[obs]:              https://build.opensuse.org/project/subprojects/home:kamarada
[kamarada-website]: https://gitlab.com/kamarada/kamarada.gitlab.io

## Sobre o Linux Kamarada

O Projeto Linux Kamarada visa divulgar e promover o Linux como um sistema operacional robusto, seguro, versátil e fácil de usar, adequado para o uso diário seja em casa, no trabalho ou no servidor. O projeto começou como um _blog_ sobre o openSUSE, que é a distribuição Linux que eu uso há 12 anos (desde [abril de 2012][antoniomedeiros], na época eu instalei o openSUSE 11.4 e depois atualizei para o 12.1). Agora o projeto disponibiliza sua própria distribuição Linux, que traz vários programas apresentados no _blog_ já instalados e prontos para uso.

[antoniomedeiros]:  https://antoniomedeiros.dev/blog/2012/04/21/problemas-envolvendo-bootloaders-mbr-e-tabela-de-particoes/

## Ficha técnica

Para que seja possível comparar esta versão com a anterior e as futuras, segue um resumo do _software_ contido no Linux Kamarada 15.5 Final Build 5.78:

- _kernel_ Linux 5.14.21
- servidor gráfico X.Org 21.1.4 (com Wayland 1.21.0 habilitado por padrão)
- área de trabalho GNOME 41.9 acompanhada de seus principais aplicativos (_core apps_), como Arquivos (antigo Nautilus), Calculadora, Terminal, Editor de texto (gedit), Captura de tela e outros
- suíte de aplicativos de escritório LibreOffice 24.2.1.2
- navegador Mozilla Firefox 115.11.0 ESR (navegador padrão)
- navegador Chromium 125 (navegador alternativo disponível)
- reprodutor multimídia VLC 3.0.20
- cliente de _e-mail_ Evolution 3.42.4
- centro de controle do YaST
- Brasero 3.12.3
- CUPS 2.2.7
- Drawing 1.0.1
- Firewalld 0.9.3
- GParted 1.4.0
- HPLIP 3.21.10
- Java (OpenJDK) 11.0.23
- KeePassXC 2.7.8
- Linphone 5.0.5
- PDFsam Basic 5.2.3
- Pidgin 2.14.8
- Python 3.6.15
- Samba 4.17.12
- Tor 0.4.8.10
- Transmission 3.00
- Vim 9.1.0330
- Wine 8.0
- instalador Calamares 3.2.62
- Flatpak 1.14.5
- jogos: AisleRiot (Paciência), Mahjongg, Minas (Campo minado), Nibbles (Cobras), Quadrapassel (Tetris), Reversi, Sudoku, Xadrez

Essa lista não é exaustiva, mas já dá uma noção do que se encontra na distribuição.
