---
date: '2021-12-11 17:50:00 GMT-3'
image: '/files/2021/12/15.3-beta.png'
layout: post
nickname: 'kamarada-15.3-beta'
title: 'Linux Kamarada 15.3 Beta: ajude a testar a melhor versão da distribuição'
---

{% include image.html src='/files/2021/12/15.3-beta.png' %}

O Projeto Linux Kamarada anuncia o lançamento da versão 15.3 Beta da [distribuição Linux][linux] de mesmo nome, baseada no [openSUSE Leap 15.3][leap-15.3]. Ela já está disponível para [download].

<!--more-->

A página [Download] foi atualizada e agora oferece principalmente duas versões para _download_:

- a versão 15.2 Final, que você pode instalar no computador de casa ou do trabalho; e
- a versão 15.3 Beta, que você pode testar, se quiser ajudar no desenvolvimento.

O openSUSE Leap 15.3 foi lançado em [junho][leap-15.3] e desde então já recebeu diversas atualizações e correções. Portanto, o Linux Kamarada 15.3, embora ainda esteja em fase [beta], deve apresentar desde já uma estabilidade considerável. Ainda assim, pode ser que essa versão tenha _bugs_ e não esteja pronta para o uso diário. Se você encontrar um _bug_, por favor, reporte. Veja formas de entrar em contato na página [Ajuda]. Claro, _bugs_ podem ser encontrados e corrigidos a qualquer momento, mas quanto antes, melhor!

A forma mais fácil de testar o Linux é usando o [VirtualBox]. Para mais informações, leia:

- [VirtualBox: a forma mais fácil de conhecer o Linux sem precisar instalá-lo][virtualbox]

Mas, se você quiser testar o Linux Kamarada no seu próprio computador, a forma mais fácil de fazer isso é usando um _pendrive_ inicializável que você pode criar com o [Ventoy]:

- [Ventoy: crie um pendrive multiboot simplesmente copiando imagens ISO para ele][ventoy]

Se você deseja instalar o Linux Kamarada no computador para usar no dia-a-dia, o recomendado é que você instale a versão 15.2 Final, que já está pronta, e depois, quando a versão 15.3 estiver pronta, atualize. Para mais informações sobre a versão 15.2 Final, leia:

- [Linux Kamarada 15.2: venha para o lado verde, elegante e moderno da força!][kamarada-15.2]

## Novidades

As novidades da versão 15.3 se resumem a:

- mesmos aplicativos da versão 15.2, porém atualizados, em novas versões, com novas funcionalidades e _bugs_ corrigidos;

- novos belos papéis de parede (ainda é possível usar os antigos ou o padrão do openSUSE, se você preferir);

- suporte nativo ao [Flatpak], gerenciador de pacotes independente de distribuição (mais sobre isso adiante);

- mais _drivers_ do [X.Org] foram incluídos na imagem Live, que antes possuía apenas os _drivers_ mais básicos, o que gerava problemas principalmente para usuários de placas de vídeo [NVIDIA], que agora contam com os _drivers_ de código aberto **[nouveau]** e **nv** instalados por padrão;

- agora o Linux Kamarada tem apenas uma imagem Live, que pode ser usada tanto em Português Brasileiro quanto em Inglês Internacional (nas versões 15.1 e 15.2, eram disponibilizadas duas imagens Live, uma para cada idioma);

- ao iniciar a imagem Live, uma tela permite a seleção de idioma -- Português Brasileiro ou Inglês Internacional -- assim como testar ou instalar o Linux Kamarada, essa tela foi inspirada na tela de boas vindas da imagem Live do [Ubuntu];

- o [YaST Firstboot], que antes era usado na imagem Live internacional e no sistema recém instalado (a partir de ambas as imagens) para definir as configurações iniciais (idioma, leiaute de teclado, fuso horário, etc.), não é mais usado, porque em muitos computadores ele não aparecia, e ficava uma tela preta no lugar;

- o instalador [Calamares] agora faz todas as configurações iniciais no sistema instalado, de modo que, no final da instalação, ao reiniciar o computador, o sistema já está completamente pronto para uso;

- o ícone do instalador foi movido da _dock_ do [GNOME] para a Área de Trabalho (obrigado ao [SlackJeff], que fez uma [_review_ do Linux Kamarada 15.2][SlackJeff], pela sugestão);

- a extensão [TopIcons Plus] do GNOME, que estava sem receber manutenção, foi substituída pela extensão [AppIndicator Support], do Ubuntu; e

- foi adicionado um cliente para VPNs do tipo L2TP.

Com relação ao [suporte ao Flatpak][flatpak-opensuse], o pacote [**flatpak**][flatpak-softwareoo] já vem instalado por padrão no Linux Kamarada 15.3. Portanto, usuários do Linux Kamarada 15.3 não precisam executar o primeiro passo das [instruções de instalação do Flatpak no openSUSE][flatpak-setup]. Porém, o [Flathub], repositório padrão, não vem pré-configurado, de modo que é necessário adicioná-lo antes de instalar programas:

```
$ flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
```

Eu gostaria que o Linux Kamarada fosse como o [Linux Mint], que já traz inclusive o repositório Flathub configurado, de modo que o usuário, sem precisar fazer nenhuma configuração adicional, já consegue instalar programas a partir do Flathub usando a própria loja de _software_. Vou estudar o que preciso fazer para conseguir isso ainda nessa versão 15.3 ou na próxima, 15.4. Comentários e ideias são bem vindas.

## Atualizando para o 15.3 Beta

Se você já usa o Linux Kamarada 15.2 e confia na sua experiência como usuário, pode querer atualizar para o Linux Kamarada 15.3 Beta para ajudar a testar o processo de atualização. Em linhas gerais, deve ser suficiente seguir esse tutorial de atualização, substituindo `15.2` por `15.3`, onde houver:

- [Linux Kamarada e openSUSE Leap: como atualizar da versão 15.1 para a 15.2][upgrade-howto]

Como o Linux Kamarada 15.2 já configurava os repositórios com a variável `$releasever`, deve ser bem fácil atualizar para a versão 15.3. Exemplos de comandos do tutorial acima, já adaptados para a nova versão:

```
# zypper --releasever=15.3 ref
# zypper --releasever=15.3 dup --download-only
```

Há um detalhe com relação ao openSUSE Leap 15.3, que compartilha pacotes RPM com o [SUSE Linux Enterprise 15 SP3][sle-15-sp3] (não apenas o código-fonte, como nas versões anteriores, mas até mesmo os pacotes binários, já compilados): durante a atualização, o gerenciador de pacotes **zypper** vai acusar muitas mudanças de fornecedor (_[vendor changes]_). Para aceitá-las, use a opção `--allow-vendor-change`. O último comando acima ficaria, portanto, assim:

```
# zypper --releasever=15.3 dup --download-only --allow-vendor-change
```

Para mais informações, consulte as [notas de lançamento do openSUSE Leap 15.3][leap-15.3-release-notes].

Se não sentir segurança de atualizar para a versão 15.3 Beta com essas dicas, não tem problema. Nos próximos dias, devo estar publicando uma versão atualizada desse tutorial.

{% capture atualizacao %}

Aqui está:

- [Linux Kamarada e openSUSE Leap: como atualizar da versão 15.2 para a 15.3]({% post_url pt/2021-12-23-linux-kamarada-e-opensuse-leap-como-atualizar-da-versao-152-para-a-153 %})

{% endcapture %}

{% include update.html date="23/12/2021" message=atualizacao %}

## Ficha técnica

Para que seja possível comparar esta versão com a anterior e as futuras, segue um resumo do _software_ contido no Linux Kamarada 15.3 Beta Build 3.1:

- _kernel_ Linux 5.3.18
- servidor gráfico X.Org 1.20.3 (sem Wayland)
- área de trabalho GNOME 3.34.7 acompanhada de seus principais aplicativos (_core apps_), como Arquivos (antigo Nautilus), Calculadora, Terminal, Editor de texto (gedit), Captura de tela e outros
- suíte de aplicativos de escritório LibreOffice 7.1.4.2
- navegador Mozilla Firefox 91.4.0 ESR (navegador padrão)
- navegador Chromium 95 (navegador alternativo disponível)
- reprodutor multimídia VLC 3.0.16
- cliente de _e-mail_ Evolution
- centro de controle do YaST
- Brasero
- CUPS 2.2.7
- Firewalld 0.9.3
- GParted 0.31.0
- HPLIP 3.20.11
- Java (OpenJDK) 11.0.13
- KeePassXC 2.6.6
- KolourPaint 20.04.2
- Linphone 4.1.1
- PDFsam Basic 4.2.7
- Pidgin 2.13.0
- Python 2.7.18 e 3.6.13
- Samba 4.13.13
- Tor 0.4.6
- Transmission 2.94
- Vim 8.0
- Wine 6.0
- instalador Calamares 3.2.36
- Flatpak 1.10.5
- jogos: Aisleriot (Paciência), Copas, Iagno (Reversi), Mahjongg, Minas (Campo minado), Nibbles (Cobras), Quadrapassel (Tetris), Sudoku, Xadrez

Essa lista não é exaustiva, mas já dá uma noção do que se encontra na distribuição.

[linux]:                    https://www.vivaolinux.com.br/linux/
[leap-15.3]:                {% post_url pt/2021-06-02-opensuse-leap-153-constroi-um-caminho-para-a-versao-empresarial %}
[download]:                 /pt/download
[beta]:                     https://pt.wikipedia.org/wiki/Ciclo_de_vida_de_liberação_de_software#Beta
[Ajuda]:                    /pt/ajuda
[virtualbox]:               {% post_url pt/2019-10-08-virtualbox-a-forma-mais-facil-de-conhecer-o-linux-sem-precisar-instala-lo %}
[ventoy]:                   {% post_url pt/2020-07-29-ventoy-crie-pendrives-multiboot-simplesmente-copiando-imagens-iso-para-ele %}
[kamarada-15.2]:            {% post_url pt/2020-09-11-linux-kamarada-15.2-venha-para-o-lado-verde-elegante-e-moderno-da-forca %}
[Flatpak]:                  https://flatpak.org/
[X.Org]:                    https://www.x.org/wiki/
[NVIDIA]:                   https://www.nvidia.com/pt-br/
[nouveau]:                  https://nouveau.freedesktop.org/
[Ubuntu]:                   https://ubuntu.com/
[YaST Firstboot]:           https://en.opensuse.org/YaST_Firstboot
[Calamares]:                https://calamares.io/
[SlackJeff]:                https://www.youtube.com/watch?v=VlSCytf7Tns
[TopIcons Plus]:            https://github.com/phocean/TopIcons-plus
[GNOME]:                    https://br.gnome.org/
[AppIndicator Support]:     https://github.com/ubuntu/gnome-shell-extension-appindicator
[flatpak-opensuse]:         https://en.opensuse.org/Flatpak
[flatpak-softwareoo]:       https://software.opensuse.org/package/flatpak
[flatpak-setup]:            https://flatpak.org/setup/openSUSE/
[Flathub]:                  https://flathub.org/
[Linux Mint]:               https://linuxmint.com/
[upgrade-howto]:            {% post_url pt/2020-08-31-linux-kamarada-e-opensuse-leap-como-atualizar-da-versao-151-para-a-152 %}
[sle-15-sp3]:               https://www.suse.com/c/introducing-suse-linux-enterprise-15-sp3/
[vendor changes]:           https://en.opensuse.org/SDB:Vendor_change_update
[leap-15.3-release-notes]:  https://doc.opensuse.org/release-notes/x86_64/openSUSE/Leap/15.3/RELEASE-NOTES.pt_BR.html#sec.upgrade.152
