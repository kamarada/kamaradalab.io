---
date: '2023-08-15 20:00:00 GMT-3'
image: '/files/2023/08/rip-cd-pt.jpg'
layout: post
nickname: 'sound-juicer'
title: 'Como ripar CDs no Linux com o Sound Juicer'
excerpt: 'O que fazer se você tem um CD na sua mão, esse CD não está disponível em nenhum serviço de streaming, e você quer ouvir as músicas desse CD no seu carro, cujo aparelho de som não tem leitor de CD? Nesse caso, extrair as músicas do CD para arquivos MP3 – processo mais conhecido como &quot;ripar&quot; o CD – pode ser uma opção. No Linux, é possível fazer isso de algumas formas, e uma delas é usando o aplicativo Sound Juicer. Nesse tutorial, você verá como instalá-lo e usá-lo no Linux Kamarada 15.4.'
---

{% include image.html src='/files/2023/08/rip-cd-pt.jpg' %}

Hoje em dia é raro um aparelho de som que toque CDs. É interessante pensar na evolução das mídias: na minha infância, eu alcancei a [fita cassete], mas na época já havia o [CD]. Depois, tornaram-se comuns os arquivos de músicas [MP3], primeiro armazenados em CDs, depois em _[pendrives]_. Até que, por fim, vieram os _[smartphones]_ e o [Bluetooth]. Hoje, o que me parece ser mais comum é: o aparelho de som serve só mesmo para tocar as músicas, que estão no celular, como arquivos MP3 ou baixadas de algum serviço de _streaming_, como o [Spotify].

Os serviços de _streaming_ tornaram muito fácil procurar por e ouvir músicas. Dificilmente não encontro alguma música que procuro no Spotify ou no [YouTube].

Mas o que fazer se você tem um CD na sua mão, esse CD não está disponível em nenhum serviço de _streaming_, e você quer ouvir as músicas desse CD no seu carro, cujo aparelho de som não tem leitor de CD?

Nesse caso, extrair as músicas do CD para arquivos MP3 -- processo mais conhecido como "ripar" o CD -- pode ser uma opção. Você pode, então, passar esses MP3 para um _pendrive_ ou celular, e ouvi-los no seu aparelho de som.

No [Linux], é possível fazer isso de algumas formas, e uma delas é usando o aplicativo **[Sound Juicer]**. Nesse tutorial, você verá como instalá-lo e usá-lo no [Linux Kamarada 15.4], que será a distribuição que usaremos como referência.

<div class='alert alert-danger' role='alert'>
    <i class='fa-solid fa-triangle-exclamation' aria-hidden='true'></i> <strong>Cuidado:</strong> dependendo do uso que você faça desses arquivos MP3, pode incorrer em crime de pirataria. O objetivo deste artigo é apresentar essa tecnologia e suas possibilidades, mas o uso que você faz delas é de sua responsabilidade. Este artigo é técnico e não contém aconselhamento jurídico. Em caso de dúvidas com relação a esse aspecto, contate um advogado.
</div>

## Instalando o Sound Juicer

Você pode instalar o Sound Juicer a partir dos [repositórios oficiais do openSUSE][repos] de duas formas: pela interface gráfica, usando a instalação com 1 clique (*1-Click Install*), ou pelo terminal, usando o gerenciador de pacotes **zypper**. Escolha a que prefere.

Para instalar o Sound Juicer usando a instalação com 1 clique, clique no botão abaixo:

<p class='text-center'>
    <a class='btn btn-primary' href='/downloads/sound-juicer.ymp'>
        <i class='fas fa-bolt'></i> Instalação com 1 clique
    </a>
</p>

Para instalar o Sound Juicer usando o terminal, execute o comando a seguir:

```
# zypper in sound-juicer
```

Se você usa o [FlatPak], outra opção é instalar o Sound Juicer a partir do [Flathub]:

```
# flatpak install org.gnome.SoundJuicer
```

Logo após a instalação, você já deve ser capaz de iniciar o Sound Juicer.

## Iniciando o Sound Juicer

Para iniciar o Sound Juicer, se você usa a área de trabalho [GNOME] (padrão do Linux Kamarada), clique em **Atividades**, no canto superior esquerdo da tela, comece a digitar `sound juicer` e clique no ícone correspondente:

{% include image.html src='/files/2023/08/sound-juicer-01-pt.jpg' %}

## Configurando o Sound Juicer no primeiro uso

Por padrão, o Sound Juicer extrai as músicas do CD para arquivos [Ogg Vorbis], que é um formato aberto de arquivo de música. Mas o formato MP3 é mais comum. Vamos configurar o Sound Juicer para guardar as músicas como MP3.

Felizmente, só precisamos fazer essa configuração ao usar o programa pela primeira vez. Ela é lembrada nos usos seguintes.

Abra o **menu** do Sound Juicer (clicando no ícone que tem 3 pontos na vertical, no canto superior direito da janela) e clique em **Preferências**:

{% include image.html src='/files/2023/08/sound-juicer-02-pt.jpg' %}

Em **Formato da saída**, selecione **MPEG Layer 3 Audio**:

{% include image.html src='/files/2023/08/sound-juicer-03-pt.jpg' %}

Feche a caixa de diálogo **Preferências**.

## Ripando um CD com o Sound Juicer

Insira o CD de música que deseja ripar na unidade de CD do seu computador, caso ainda não tenha feito.

O Sound Juicer lê o CD e tenta obter informações sobre ele, seja do próprio CD, seja do banco de dados _online_ [MusicBrainz]:

{% include image.html src='/files/2023/08/sound-juicer-04-pt.jpg' %}

Note que **Título**, **Artista**, **Gênero**, **Ano** e **Disco** são campos de texto e você pode inserir ou alterar essas informações:

{% include image.html src='/files/2023/08/sound-juicer-05-pt.jpg' %}

Nesse exemplo, eu defini **Gênero** como `Jazz` e **Ano** como `2020`.

Fazendo duplo-clique nos nomes das faixas, você também pode alterá-los:

{% include image.html src='/files/2023/08/sound-juicer-06-pt.jpg' %}

Quando terminar de revisar as informações sobre o CD e suas faixas, certifique-se de que estão todas selecionadas e clique em **Extrair**:

{% include image.html src='/files/2023/08/sound-juicer-07-pt.jpg' %}

Uma barra de progresso ao final mostra o andamento da extração:

{% include image.html src='/files/2023/08/sound-juicer-08-pt.jpg' %}

Quando tudo terminar, os arquivos MP3 estarão na pasta **Músicas**:

{% include image.html src='/files/2023/08/sound-juicer-09-pt.jpg' %}

Não compartilharei os arquivos MP3 que gerei durante a produção deste tutorial em lugar algum. Até porque não há necessidade: se você tiver curiosidade de ouvir esse CD, saiba que ele é da banda [GrOOfbOOgalOO] e está disponível no [Spotify][cd-spotify], no [YouTube][cd-youtube] e em outros serviços de _streaming_.

[fita cassete]:         https://pt.wikipedia.org/wiki/Fita_cassete
[CD]:                   https://pt.wikipedia.org/wiki/Compact_disc
[MP3]:                  https://pt.wikipedia.org/wiki/MP3
[pendrives]:            https://pt.wikipedia.org/wiki/USB_flash_drive
[smartphones]:          https://pt.wikipedia.org/wiki/Smartphone
[Bluetooth]:            https://pt.wikipedia.org/wiki/Bluetooth
[Spotify]:              https://spotify.com/
[YouTube]:              https://music.youtube.com
[Linux]:                http://www.vivaolinux.com.br/linux/
[Sound Juicer]:         https://wiki.gnome.org/Apps/SoundJuicer
[Linux Kamarada 15.4]:  {% post_url pt/2023-02-27-linux-kamarada-15-4-mais-funcional-bonito-polido-e-elegante-do-que-nunca %}
[repos]:                https://en.opensuse.org/Package_repositories#Official_Repositories
[FlatPak]:              {% post_url pt/2022-01-17-flatpak-tudo-o-que-voce-precisa-saber-sobre-o-gerenciador-de-pacotes-independente-de-distribuicao %}
[Flathub]:              https://flathub.org/pt-BR/apps/org.gnome.SoundJuicer
[GNOME]:                https://br.gnome.org/
[Ogg Vorbis]:           https://xiph.org/vorbis/
[MusicBrainz]:          https://musicbrainz.org/
[GrOOfbOOgalOO]:        https://www.groofboogaloo.com.br/
[cd-spotify]:           http://open.spotify.com/album/1hI3pGwo0z4jtQOEsquwvo
[cd-youtube]:           https://music.youtube.com/playlist?list=OLAK5uy_mX_xzseU96D7lBG-nZ-DrW_JsyGXTJ9Lg
