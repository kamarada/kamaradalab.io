---
date: '2022-06-08 23:50:00 GMT-3'
image: '/files/2022/06/leap-15.4.jpg'
layout: post
nickname: 'leap-15.4'
title: 'Leap 15.4 oferece novos recursos, com a estabilidade de sempre'
excerpt: 'A próxima versão menor do openSUSE Leap 15 já está disponível em get.opensuse.org para usuários, profissionais, entusiastas e desenvolvedores que desejem atualizar para a versão mais recente. O Leap 15.4 é uma versão que fornece uma quantidade significativa de atualizações em comparação às versões anteriores do Leap 15.x, juntamente com novos recursos.'
---

{% capture nota1 %}
Esta é uma tradução não oficial da notícia originalmente publicada por Douglas DeMaio no site [news.opensuse.org](https://news.opensuse.org/2022/06/08/leap-offers-new-features-familiar-stability/).
{% endcapture %}

{% include note.html text=nota1 %}

{% include image.html src='/files/2022/06/leap-15.4.jpg' %}

NUREMBERGUE, Alemanha -- A próxima versão menor do openSUSE Leap 15 já está disponível em [get.opensuse.org](https://get.opensuse.org/leap) para usuários, profissionais, entusiastas e desenvolvedores que desejem atualizar para a versão mais recente.

O Leap 15.4 é uma versão de lançamento de recursos e fornece uma quantidade significativa de atualizações em comparação às versões anteriores do Leap 15.x, juntamente com novos recursos.

"O Leap 15.4 continua a fornecer uma versão robusta e familiar e entrega _software_ de código aberto estável para _desktops_, servidores, _containers_ e cargas de trabalho virtualizadas", disse Max Lin, membro da equipe de lançamento. "O Leap é uma distribuição difícil de ignorar para especialistas em tecnologia; correções de segurança, novas tecnologias e pacotes atualizados oferecem aos profissionais uma versão comunitária bem projetada que é idêntica à sua gêmea corporativa. E oferece uma enorme quantidade de _software_ comunitário."

Assim como na versão anterior do Leap, os usuários podem migrar para o SUSE Linux Enterprise (SLE) e deixar as cargas de trabalho funcionando normalmente. Esta versão aprimora ainda mais a proficiência da migração porque a equipe do [YaST](https://yast.opensuse.org/) desenvolveu uma [ferramenta de migração](https://build.opensuse.org/package/show/YaST:Head/yast2-migration-sle) simplificada para migrações para o SLE.

Os _containers_ e as cargas de trabalho fazem a transição perfeita, e a história do _container_ para o Leap se expandiu com uma nova oferta do [Leap Micro](https://download.opensuse.org/distribution/leap-micro/).

Uma novidade no [Leap 15.4](https://get.opensuse.org/leap) é o [Leap Micro 5.2](https://en.opensuse.org/openSUSE:Roadmap#Schedule_for_Leap_Micro_5.2). O Leap Micro é um sistema operacional leve moderno, que é imutável e ideal para cargas de trabalho virtualizadas e de _container/host_. O Leap Micro é adequado para ambientes de computação descentralizados, usos de ponta e implantações incorporadas/IoT. Desenvolvedores e profissionais podem construir e escalonar sistemas para uso no aeroespaço, telecomunicações, automotivo, defesa, saúde, hospitalidade, manufatura e robótica. O Leap Micro fornece administração e _patches_ automatizados. Um dos pacotes relacionados ao Leap Micro para desenvolvedores é o [Podman](https://github.com/containers/podman/blob/main/RELEASE_NOTES.md#342). O Podman oferece aos desenvolvedores opções para executar seus aplicativos com o Podman em produção e a versão [3.4.2](https://github.com/containers/podman/blob/main/RELEASE_NOTES.md#342) atualizada traz novos _pods_ com suporte para _containers init_, que são _containers_ que são executados antes que o restante do _pod_ inicie.

Grandes equipes de desenvolvimento ganham valor agregado com o openSUSE Leap 15.4 e o Leap Micro 5.2, pois as cargas de trabalho podem ser levantadas e transferidas para o SUSE Linux Enterprise Linux 15 SP4 ou o [SLE Micro](https://www.suse.com/download/sle-micro/) para manutenção estendida e vantagens favoráveis de migração de versão.

Esta versão do Leap simplifica a instalação de _codecs_ multimídia. Houve progresso para levar os _codecs_ de vídeo OpenH264 da [Cisco](https://www.cisco.com/) aos usuários por meio de um repositório presente por padrão no sistema, que virá em uma atualização de manutenção. A nova versão não só ganha melhorias na multimídia; ela ganha suporte a _drivers_ de código aberto. Além do compromisso contínuo de _drivers_ gráficos de código aberto para Linux da [AMD](https://www.amd.com/) e da [Intel](https://www.intel.com/), os usuários das modernas GPUs [NVIDIA](https://www.nvidia.com) se beneficiarão com a assinatura de imagens de _firmware_ para as GPUs de última geração da série GeForce 30.

Outro novo pacote para o [Leap 15.4](https://get.opensuse.org/leap) é o [sassist](https://github.com/dell/sassist) da [Dell](https://github.com/dell). O pacote ajuda na solução/depuração de problemas com o Dell PowerEdge Server e é executado no sistema operacional Linux para funcionar com o Dell Remote Access Controller (iDRAC) integrado da Dell, permitindo a coleta de _logs_ e configuração.

Aqueles que usam o Leap em servidores encontrarão algumas mudanças notáveis. Essas mudanças incluem a descontinuação do Python 2, _containers_ libvirt LXC e servidor OpenLDAP. O [389 Directory Server](https://directory.fedoraproject.org) é o servidor LDAP primário, substituindo o servidor OpenLDAP.

O [PHP 8.1.0](https://www.php.net/releases/8.1/) foi adicionado e traz muitas melhorias. Estas incluem enumerações, propriedades somente leitura, fsync e muitas outras. Há também uma aceleração de 3,5% com o [PHP 8.1.0](https://www.php.net/releases/8.1/) para WordPress e a nova versão do PHP fornece um _backend_ Just-In-Time para ARM64, juntamente com outras melhorias e correções no JIT. Algumas outras mudanças notáveis ​​para o Leap é que o Wayland agora funciona com o _driver_ proprietário mais recente da NVIDIA e o LUKS2 é suportado no Particionador do YaST, mas precisa ser habilitado explicitamente.

O Leap tem uma vasta seleção para usuários de _desktop_ e tem a tradição de oferecer versões de Suporte de Longo Prazo de vários pacotes; esta versão comunitária também não decepciona. A nova versão menor do Leap oferecerá o [KDE Plasma 5.24 LTS](https://community.kde.org/Schedules/Plasma_5), sobre o [Qt 5.15 LTS](https://www.qt.io/blog/qt-5.15-released) com a ["coleção de _patches_ do KDE para o Qt 5"](https://dot.kde.org/2021/04/06/announcing-kdes-qt-5-patch-collection).

"Para fazer a transição para grandes tecnologias futuras como o Qt 6, precisamos ter a tranquilidade de que nossos usuários atuais são atendidos", disse Aleix Pol, presidente da KDE e.V., em um [anúncio sobre a coleção de _patches_ para o Qt 5](https://dot.kde.org/2021/04/06/announcing-kdes-qt-5-patch-collection). "Com esta coleção de _patches_, ganhamos a flexibilidade necessária para estabilizar o _status quo_. Dessa forma, podemos continuar colaborando com a Qt e entregar ótimas soluções para nossos usuários."

Vários outros pacotes deliberadamente selecionados são voltados para os propósitos de estabilidade e desenvolvimento da versão, incluindo o [Qt 6](https://www.qt.io/product/qt6).

Existem alguns ambientes de _desktop_ mais recentes, como o [Plasma 5.24](https://kde.org/announcements/plasma/5/5.24.0/), [GNOME 41](https://help.gnome.org/misc/release-notes/41.0/), [Enlightenment 0.25](https://www.enlightenment.org/news/2021-12-26-enlightenment-0.25.0) e [MATE 1.26](https://mate-desktop.org/blog/2021-08-08-mate-1-26-released/). Esses _desktops_ oferecerão as funcionalidades mais recentes, embora nem todos os _desktops_ dessa versão ganhem novos recursos. O Leap 15.4 manterá o [Xfce 4.16](https://www.xfce.org/about/news/?post=1608595200), que foi atualizado na versão 15.3 do Leap. O [Deepin 20.3](https://www.deepin.org/en/2021/11/23/deepin-20-3/) estreia no Leap 15.4.

O Leap 15.4 vem com o [KDE Frameworks 5.90.0](https://kde.org/announcements/frameworks/5/5.90.0/), que fez alterações em vários pacotes, incluindo Baloo, Breeze Icons, KConfig, KIO, Kirigami, KWayland, Oxygen Icons e mais. Esta versão do Leap também inclui o [KDE Gear 21.12.2](https://kde.org/announcements/gear/21.12.2/); os aplicativos Gear incluem melhorias no reprodutor de música Elisa, _tags_ de pesquisa para o gerenciador de arquivos Dolphin e fornece edição de vídeos mais rápida com o editor de vídeo avançado do KDE, o Kdenlive.

O versátil _framework_ de aplicativos [Qt 5.15.2](https://www.qt.io/blog/qt-5.15.2-released) será atualizado; sua versão 5.12.7 esteve inalterada na distribuição desde o Leap 15.2. Esta versão traz recursos de três versões menores e vem com um [Qt Quick 3D](https://doc-snapshots.qt.io/qt5-5.15/qtquick3d-index.html) totalmente suportado.

O núcleo do sistema recebeu inúmeras atualizações. Esta versão do Leap atualiza o [systemd](https://github.com/systemd/systemd) para a versão 249, que possui muitas alterações para aprimorar a experiência do usuário. Os novos componentes do sistema agora podem identificar corretamente ambientes Amazon EC2, e várias melhorias foram feitas para o protocolo de gerenciamento de rede do servidor DHCP. Um novo banco de dados de hardware do udev foi adicionado para dispositivos FireWire e outra mudança notável na versão são as atualizações A/B de todo o sistema de arquivos, onde novas versões do sistema operacional são colocadas em partições cujo rótulo é então atualizado com um identificador de versão correspondente. O Leap fornece o conjunto de compiladores mais atualizado. A [versão 13.0 do LLVM Compiler 13.0](https://releases.llvm.org/13.0.0/tools/clang/docs/ReleaseNotes.html) possui alguns novos recursos importantes e melhorias nos diagnósticos do Clang. Há um punhado de novas _flags_ de compilador.

A pilha DNF foi atualizada para a versão 4.10.0 e adiciona novos recursos. Foi adicionado suporte para detecção automática e exclusão de pacotes da instalação devido a dependências fracas, o que dá ao gerenciador de pacotes uma nova qualidade.

O Leap não é apenas para os experientes administradores de sistemas ou profissionais de TI. O Leap oferece _software_ para que os músicos possam aprimorar a qualidade de som, gravação e _streaming_ da sua apresentação. A tecnologia Virtual Studio com pacotes como [PipeWire](https://software.opensuse.org/package/pipewire), [Wireplumber](https://software.opensuse.org/package/wireplumber) e sintetizador [LV2](https://software.opensuse.org/package/lv2) levam instrumentos e letras a um novo nível. Criadores de conteúdo profissionais e _designers_ de _sites_ podem aproveitar ferramentas de modelagem 3D como o [Blender](https://software.opensuse.org/package/blender), o editor de vídeo [Kdenlive](https://kdenlive.org) e _softwares_ de edição de imagens como o [Krita](https://software.opensuse.org/package/krita) para transformar sua visão em realidade.

Usuários que desejem pacotes específicos na próxima versão do Leap 15.5 são incentivados a entrar em contato com a equipe de lançamento. Se houver esforços da comunidade que possam ser alocados para manter certos pacotes, alguns pacotes poderão ser atualizados na próxima versão. Não se espera que o Leap 15.5 seja uma versão de lançamento de recurso e deve ter muitos dos mesmos pacotes que estão no Leap 15.4. O sucessor do Leap 15 provavelmente virá logo após o lançamento do Leap 15.5.

Você pode encontrar mais informações sobre o openSUSE Leap 15.4 Windows Subsystem for Linux [aqui](https://en.opensuse.org/openSUSE:WSL).

## Fim da vida

O openSUSE Leap 15.3 terá seu fim de vida (_end of life_, EOL) seis meses a partir do lançamento de hoje. Os usuários devem atualizar para o openSUSE Leap 15.4 para continuar recebendo atualizações de segurança e manutenção dentro de seis meses a partir de 8 de junho de 2022.

## Baixe o Leap 15.4

Para baixar a imagem ISO, visite <https://get.opensuse.org/leap/>.

## Perguntas

Se você tiver alguma dúvida sobre o lançamento ou achar que encontrou um _bug_, adoraríamos ouvi-lo em:

<https://t.me/openSUSE>

<https://chat.opensuse.org>

<https://lists.opensuse.org/opensuse-support/>

<https://discordapp.com/invite/openSUSE>

<https://www.facebook.com/groups/opensuseproject>

## Participe

O Projeto openSUSE é uma comunidade ao redor do mundo que promove o uso do Linux em todos os lugares. Ele cria duas das melhores distribuições Linux do mundo: a Tumbleweed, atualizada continuamente (_rolling-release_), e a Leap, distribuição híbrida de empresa e comunidade. O openSUSE trabalha continuamente de maneira conjunta, aberta, transparente e amigável como parte da comunidade internacional de _software_ livre e de código aberto (_Free and Open Source Software_, FOSS). O projeto é comandado por sua comunidade e depende das contribuições de indivíduos que trabalham como testadores, escritores, tradutores, especialistas em usabilidade, artistas, embaixadores ou desenvolvedores. O projeto abrange uma ampla variedade de tecnologias, pessoas com diferentes níveis de conhecimentos, que falam diferentes línguas e possuem diversas raízes culturais. Saiba mais sobre o Projeto openSUSE em [opensuse.org](https://opensuse.org).
