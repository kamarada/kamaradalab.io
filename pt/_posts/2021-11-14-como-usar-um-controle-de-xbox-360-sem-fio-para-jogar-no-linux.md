---
date: '2021-11-14 19:30:00 GMT-3'
image: '/files/2021/11/xbox360-wireless.jpg'
layout: post
published: true
nickname: 'xbox360-wireless'
title: 'Como usar um controle de Xbox 360 sem fio para jogar no Linux'
excerpt: 'Se você tem um controle sem fio para Xbox 360, pode querer usá-lo com o PC também. Como ele possui muitos botões, pode ser compatível com vários jogos. Inclusive, pode ser usado para emular os controles de vários videogames, com emuladores como o Snes 9x, RetroArch e PCSX2, dos quais já falamos aqui.'
---

{% include image.html src='/files/2021/11/xbox360-wireless.jpg' %}

Se você tem um controle sem fio para [Xbox 360], pode querer usá-lo com o PC também. Como ele possui muitos botões, pode ser compatível com vários jogos. Inclusive, pode ser usado para emular os controles de vários _videogames_, com emuladores como o [Snes 9x], [RetroArch] e [PCSX2], dos quais já falamos aqui.

[Xbox 360]: https://pt.wikipedia.org/wiki/Xbox_360
[Snes 9x]: {% post_url pt/2019-08-11-como-jogar-super-nintendo-no-opensuse-com-o-emulador-snes9x %}
[RetroArch]: {% post_url pt/2020-05-22-retroarch-um-emulador-so-para-varios-videogames %}
[PCSX2]: {% post_url pt/2021-07-26-como-jogar-playstation-2-no-linux-com-o-emulador-pcsx2 %}

{% include image.html src='/files/2021/11/360_controller.svg' caption='Controle para Xbox 360. Fonte: [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:360_controller.svg).' %}

Porém, diferente dos controles com fio, que usam o padrão USB, e dos [controles sem fio para modelos mais novos de Xbox][controle-xbox-one], que usam o padrão [Bluetooth][makeuseof], esse modelo de [controle sem fio para o Xbox 360][controle-xbox-360] usa um protocolo sem fio proprietário de 2,4 GHz, precisando de um receptor sem fio para ser usado no computador.

[controle-xbox-one]: https://pt.wikipedia.org/wiki/Controle_do_Xbox_One
[makeuseof]: https://www.makeuseof.com/tag/get-game-controllers-running-linux/
[controle-xbox-360]: https://pt.wikipedia.org/wiki/Controle_do_Xbox_360

Embora a [Microsoft] fabrique esse receptor, ela não o vende separadamente. Então, você tem 3 opções: comprar o controle sem fio original da Microsoft que vem com o receptor; comprar apenas o receptor usado, separado do controle sem fio (que você já tem); ou comprar um receptor chinês que é um clone perfeito, indistinguível do receptor oficial.

[Microsoft]: https://www.microsoft.com/pt-br

Eu optei pela terceira opção, que foi a mais barata e funcionou. Você pode encontrar várias opções pesquisando no [Google] ou [Mercado Livre], por exemplo, mas eu achei mais barato comprar pelo [AliExpress]. Demorou mais porque veio da China, mas chegou em 1 mês.

[Google]: https://www.google.com/search?q=receptor+usb+para+controle+de+xbox+360+sem+fio&&tbm=shop
[Mercado Livre]: https://lista.mercadolivre.com.br/receptor-usb-para-controle-de-xbox-360-sem-fio
[AliExpress]: https://s.click.aliexpress.com/e/_AnCjDn

O receptor veio com um CD com um _driver_ para [Windows]. Pro [Linux], claro, não serve. Para fazer esse receptor funcionar no Linux, eu precisei instalar o _driver_ [Xpad].

[Windows]: https://www.microsoft.com/pt-br/windows
[Linux]: https://www.vivaolinux.com.br/linux/
[Xpad]: https://github.com/paroj/xpad

Nesse tutorial, você verá como baixar, compilar e instalar o _driver_ Xpad e testar o controle sem fio para Xbox 360 no Linux. Como referência, estou usando o [Linux Kamarada 15.2][kamarada-15.2], mas testei o passo a passo também no [openSUSE Leap 15.3][leap-15.3]. Pode ser que as instruções a seguir, com modificações, funcionem em outras distribuições.

[kamarada-15.2]: {% post_url pt/2020-09-11-linux-kamarada-15.2-venha-para-o-lado-verde-elegante-e-moderno-da-forca %}
[leap-15.3]: {% post_url pt/2021-06-02-opensuse-leap-153-constroi-um-caminho-para-a-versao-empresarial %}

Pesquisando, eu encontrei tutoriais mencionando outro _driver_, o [xboxdrv], mas eu não consegui fazê-lo funcionar no openSUSE Leap 15.3.

[xboxdrv]: https://xboxdrv.gitlab.io/

{% capture atualizacao %}

Caso você tenha se esbarrado nesse tutorial, mas esteja na verdade procurando como usar esse controle no Windows 10, confira uma versão desse mesmo tutorial para Windows no [meu _blog_ pessoal][antoniomedeiros]:

- [Como usar um controle de Xbox 360 sem fio para jogar no Windows 10 - Antônio Medeiros][antoniomedeiros]

[antoniomedeiros]: https://antoniomedeiros.dev/blog/2021/11/15/como-usar-um-controle-de-xbox-360-sem-fio-para-jogar-no-windows-10/

{% endcapture %}

{% include update.html date="15/11/2021" message=atualizacao %}

## Instalando o driver Xpad

Como vamos compilar um _driver_ para o _[kernel]_, primeiro, certifique-se de ter instalada a versão mais recente do _kernel_ disponível para a sua distribuição, assim como as versões correspondentes dos pacotes que contêm o seu código-fonte:

[kernel]: https://kernel.org/

```
# zypper in kernel-default kernel-default-devel kernel-macros kernel-syms
```

Se o **zypper** te orientar a reiniciar o computador, faça isso antes de continuar.

Depois, instale os programas que vamos precisar, o [DKMS] e o [Git]:

[DKMS]: https://pt.wikipedia.org/wiki/Dynamic_Kernel_Module_Support
[Git]: https://git-scm.com/

```
# zypper in dkms git
```

Por fim, baixe, compile e instale o _driver_ Xpad (faça isso como _root_):

```
# git clone https://github.com/paroj/xpad.git /usr/src/xpad-0.4
# dkms install -m xpad -v 0.4
```

Não é necessário reiniciar o computador.

## Conectando o controle

Plugue o receptor em uma porta USB livre do computador.

Se você executar o comando **lsusb**, perceberá um dispositivo novo de ID `045e:02a9`:

```
$ lsusb
[...]
Bus 002 Device 008: ID 045e:02a9 Microsoft Corp.
[...]
```

Se você tiver um Xbox 360 por perto, recomendo desligá-lo e desconectá-lo da energia. O passo a seguir pode fazer com que ele ligue sem necessidade.

1. No controle sem fio, pressione e segure o botão **Guia** (_guide_) até que ele ligue
2. No receptor, pressione e solte o botão de conexão (o único botão que ele tem), a luz dele começa a piscar
3. No controle sem fio, pressione e solte o botão de conexão, na parte de cima, a luz dele começa a piscar
4. Ambas as luzes deixam de piscar quando o controle se conecta ao receptor. A luz fixa acesa no controle passa a indicar sua posição (uma de 4 possíveis).

{% include image.html src='/files/2021/11/xbox360-wireless-01.png' %}

Para conectar mais controles, siga esses 4 passos novamente. Em tese, você pode conectar até 4 controles (eu, particularmente, só testei até 2).

## Testando o controle

Provavelmente a forma mais fácil de testar o controle é abrir o navegador e acessar o _site_ [gamepad-tester.com](https://gamepad-tester.com/). Pressione um botão no controle para começar o teste:

{% include image.html src='/files/2021/11/xbox360-wireless-02.jpg' %}

Mexa os eixos e aperte os botões e confira a animação na tela.

Você também pode instalar um programa no seu computador para fazer o teste, o [jstest-gtk].

[jstest-gtk]: https://jstest-gtk.gitlab.io/

Baixe o código-fonte do programa usando o Git:

```
$ git clone https://gitlab.com/jstest-gtk/jstest-gtk.git
$ cd jstest-gtk
```

Instale os programas necessários para compilá-lo:

```
# zypper in cmake gcc-c++ gtkmm3-devel libsigc++2-devel
```

Então, compile-o:

```
$ mkdir build
$ cd build
$ cmake ..
$ make
```

Ao final, você pode iniciá-lo executando:

```
$ ./jstest-gtk
```

Eis o que aparece com apenas um controle conectado:

{% include image.html src='/files/2021/11/xbox360-wireless-03-pt.jpg' %}

Selecione o controle na lista e clique em **Propriedades**.

Mexa os eixos e aperte os botões e confira a animação na tela:

{% include image.html src='/files/2021/11/xbox360-wireless-04-pt.jpg' %}

Se você conectar o segundo controle e clicar em **Atualizar**, ele aparece na lista:

{% include image.html src='/files/2021/11/xbox360-wireless-05-pt.jpg' %}

## Usando o controle

Pronto, agora é só usar o controle!

O [RetroArch] e o [PCSX2] devem detectar e configurar o controle automaticamente.

No Snes 9x, é preciso configurar o controle conforme as instruções do nosso [tutorial][Snes 9x].

Para usar o controle em outros jogos, confira as instruções de cada jogo.

_Have a lot of fun..._ (Divirta-se bastante...)

## Referências

Além das páginas cujos _links_ aparecem ao longo do texto, eu consultei também estas:

- [How to Hook Up a Wireless Xbox 360 Controller to Your Computer - How-To Geek][howtogeek]
- [Conectar um controle ao seu Xbox 360 - Suporte do Xbox 360][xbox-360-help]
- [Gamepad - ArchWiki][archlinux]

[howtogeek]: https://www.howtogeek.com/178881/ask-htg-how-can-i-hook-up-my-xbox-360-controllers-to-my-computer/
[xbox-360-help]: https://support.xbox.com/pt-BR/help/xbox-360/accessories/connecting-game-controllers
[archlinux]: https://wiki.archlinux.org/title/Gamepad
