---
date: '2024-10-01 01:20:00 GMT-3'
image: '/files/2024/10/tor.jpg'
layout: post
nickname: 'tor'
title: 'Tor: configurando proxy para tunelar aplicativos no Linux'
excerpt: 'Vimos o que é VPN, Rede Tor e como usá-la por meio do Navegador Tor. Como nos computadores é mais comum acessarmos sites, o Navegador Tor é a melhor forma de começar a usar a Rede Tor no Linux. Mas você também pode tunelar aplicativos usando o proxy do cliente Tor. Veja como.'
---

{% include image.html src='/files/2024/10/tor.jpg' %}

Vimos o que é **[VPN]**, **[Rede Tor][VPN]** e a forma mais fácil de usá-la no [Linux], que é por meio do **[Navegador Tor][VPN]** (_Tor Browser_), que é uma versão do [Mozilla Firefox] modificada para trafegar dados somente dentro da Rede Tor. Como nos computadores (_desktops_ e _notebooks_) é mais comum acessarmos serviços _online_ por meio do navegador, em vez de aplicativos (como é o caso com dispositivos móveis -- _smartphones_ e _tablets_), o Navegador Tor é a melhor forma de começar a usar a Rede Tor no Linux.

No entanto, se você precisa conectar um ou mais aplicativos no Linux à Internet por meio da Rede Tor, saiba que é possível fazer isso usando o cliente **[Tor]**. Normalmente, ele é instalado como um **serviço**, ou seja, um programa que roda em segundo plano (ao fundo, em _background_) enquanto você usa outros programas. Quando iniciado, esse serviço se conecta à Rede Tor e disponibiliza essa conexão para outros aplicativos por meio de um _proxy_. Então, para conectar um aplicativo à Rede Tor, basta configurá-lo para usar esse _proxy_.

A seguir, você verá como instalar e usar o Tor no Linux. Como referência, vou usar a distribuição [Linux Kamarada 15.5], baseada no [openSUSE Leap]. Se você usa outra distribuição, provavelmente apenas a forma de instalar é diferente. Para mais informações, consulte a documentação da sua distribuição.

## Instalando o Tor

Na verdade, o Linux Kamarada já vem com o Tor instalado "de fábrica" [desde a versão 15.2][15.2], lançada em setembro de 2020, só não vem habilitado por padrão. Portanto, se você usa o Linux Kamarada, pode pular para a seção seguinte deste artigo.

Você pode instalar o Tor a partir dos [repositórios oficiais do openSUSE][repos] de duas formas: pela interface gráfica, usando a instalação com 1 clique (*1-Click Install*), ou pelo terminal, usando o gerenciador de pacotes **[zypper]**. Escolha a que prefere.

Para instalar o Tor usando a instalação com 1 clique, clique no botão abaixo:

<p class='text-center'>
    <a class='btn btn-primary' href='/downloads/tor.ymp'>
        <i class='fas fa-bolt'></i> Instalação com 1 clique
    </a>
</p>

Para instalar o Tor usando o terminal, execute o comando a seguir:

```
# zypper in tor
```

## Iniciando o Tor

Você pode iniciar o serviço do Tor pelo terminal executando este comando:

```
# systemctl start tor
```

Esse comando não produz saída, mas o Tor pode levar alguns segundos ou minutos para iniciar.

## Verificando o estado do Tor

Você pode verificar se o serviço do Tor está rodando pelo terminal usando:

```
# systemctl status tor
```

Esse comando deve apresentar algo como:

{% include image.html src='/files/2024/10/tor-01-pt.jpg' %}

Perceba ao final: `Bootstrapped 100% (done): Done`. Isso indica que a conexão com a Rede Tor foi estabelecida e a VPN está pronta para ser usada.

## Testando o Tor

Como [vimos][VPN], sempre antes de começar a usar a Rede Tor, convém testar se você está de fato conectado à Rede Tor acessando [check.torproject.org](https://check.torproject.org/). No [tutorial anterior][VPN], fizemos isso no Navegador Tor.

Como agora estamos usando o cliente Tor, podemos fazer isso usando qualquer navegador instalado no sistema, contanto que o configuremos para usar o _proxy_ do Tor, como veremos a seguir.

## Configurando aplicativos para usar o Tor

O serviço do Tor disponibiliza um _proxy_ do tipo [SOCKS] no próprio computador (referenciado como `localhost` ou `127.0.0.1`) na porta `9050`. Se você configurar um aplicativo para usar esse _proxy_, todo o tráfego de rede desse aplicativo passará a ser tunelado pela Rede Tor.

Já mostramos em outro artigo, por exemplo, [como usar o Telegram via Tor][telegram-tor].

## Usando outros navegadores com o Tor

Observe que a melhor forma de acessar _sites_ usando a Rede Tor é por meio do [Navegador Tor][VPN], que não apenas só trafega dados dentro da Rede Tor, como também já vem com vários ajustes relacionados a privacidade.

[**Não é recomendado**][tor-with-a-browser] usar um navegador comum, como o Mozilla Firefox ou o [Google Chrome], para navegar pela Rede Tor, mesmo que seu tráfego esteja sendo redirecionado pelo _proxy_ do Tor.

Mas, se por qualquer motivo você precisar fazer isso (como, por exemplo, para acessar rapidamente [check.torproject.org](https://check.torproject.org/) para conferir se o Tor está mesmo pronto para uso), veja a seguir como fazê-lo nos principais navegadores.

### Firefox

Abra o **menu do Firefox**, no canto superior direito da janela, e clique em **Configurações**:

{% include image.html src='/files/2024/09/firefox-import-other-browser-01-pt.jpg' %}

Na caixa de busca, digite `proxy`, e depois clique no botão **Configurar conexão**:

{% include image.html src='/files/2024/10/tor-02-pt.jpg' %}

Selecione **Configuração manual de proxy**:

{% include image.html src='/files/2024/10/tor-03-pt.jpg' %}

Em **Domínio SOCKS**, informe `127.0.0.1`.

Em **Porta**, informe `9050`.

Ceritifique-se de que está selecionado **SOCKS v5** (já vem, por padrão).

Mais embaixo, marque a opção **Proxy DNS ao usar SOCKS v5**:

{% include image.html src='/files/2024/10/tor-04-pt.jpg' %}

Por fim, clique em **OK**.

Agora acesse [check.torproject.org](https://check.torproject.org/) para testar a conexão com a Rede Tor:

{% include image.html src='/files/2024/10/tor-05.jpg' %}

### Chromium/Chrome

Abra o **menu do Chromium**, no canto superior direito da janela, e clique em **Configurações**:

{% include image.html src='/files/2024/10/tor-06-pt.jpg' %}

Na caixa de busca, digite `proxy`, e depois clique em **Abre as configurações de proxy do computador**:

{% include image.html src='/files/2024/10/tor-07-pt.jpg' %}

É aberto o aplicativo **Configurações** do [GNOME], na seção **Rede**:

{% include image.html src='/files/2024/10/tor-08-pt.jpg' %}

Em **Proxy de rede**, clique no ícone da engrenagem.

Selecione **Manual**:

{% include image.html src='/files/2024/10/tor-09-pt.jpg' %}

Em **Servidor socks**, informe `127.0.0.1`. Ao lado, informe a porta `9050`.

Pode fechar a caixa de diálogo **Proxy de rede** e a janela **Configurações**.

De volta ao Chromium, acesse [check.torproject.org](https://check.torproject.org/) para testar a conexão com a Rede Tor:

{% include image.html src='/files/2024/10/tor-10.jpg' %}

Observe que o Chromium na verdade não tem uma tela de configuração própria de _proxy_ como o Firefox e o [Telegram][telegram-tor], ele usa as configurações de _proxy_ do sistema. E do jeito que acabamos de fazer essa configuração, ela afetará também outros aplicativos, que também usarão esse _proxy_.

Alternativamente, você pode abrir o terminal e iniciar o Chromium dessa forma:

```
$ chromium --proxy-server="socks5://127.0.0.1:9050"
```

{% include image.html src='/files/2024/10/tor-11-pt.jpg' %}

## Encerrando o Tor

Se por qualquer motivo você quiser ou precisar encerrar o Tor (se, por exemplo, você terminou de usar a VPN e não vai mais usá-la hoje), você pode fazer isso executando este comando no terminal:

```
# systemctl stop tor
```

## Iniciando o Tor junto com o sistema

Se você usa o Tor todo dia, pode ser conveniente configurar seu sistema para iniciá-lo automaticamente durante a inicialização do sistema (_boot_). Para isso, execute:

```
# systemctl enable tor
```

Fazendo isso, seu sistema estará sempre pronto para usar a Rede Tor.

## Conclusão

Para uma experiência mais completa da Rede Tor no Linux, tenha os dois instalados no seu computador: o [Navegador Tor][VPN], para acessar _sites_, e o cliente Tor, para conectar aplicativos como um _proxy_.

Não encerre o assunto por aqui. Pesquise sobre VPN, inclusive sobre VPNs alternativas ao Tor, porque pode ser que você venha a precisar delas.

Se você está recorrendo ao Tor para vencer censura, observe que pode acontecer de seu país passar a censurar a própria Rede Tor. Talvez você queira consultar antecipadamente a seção sobre [Censura] da documentação do Tor.

## Referências

- [Tor - ArchWiki](https://wiki.archlinux.org/title/Tor)
- [How to Install and Use Tor Network in Your Web Browser - Tecmint](https://www.tecmint.com/use-tor-network-in-web-browser/)
- [Configurações de conexão no Firefox \| Ajuda do Firefox](https://support.mozilla.org/pt-BR/kb/configuracoes-de-conexao-no-firefox)

[VPN]:                  {% post_url pt/2024-09-02-o-que-e-vpn-e-a-forma-mais-facil-de-usa-la-no-linux-por-meio-do-navegador-tor %}
[Linux]:                https://www.vivaolinux.com.br/linux/
[Mozilla Firefox]:      {% post_url pt/2019-07-05-18-aplicativos-que-voce-pode-usar-do-mesmo-jeito-no-linux-e-no-windows-parte-1 %}#1-mozilla-firefox
[Tor]:                  https://www.torproject.org/pt-BR/download/tor/
[Linux Kamarada 15.5]:  {% post_url pt/2024-05-27-linux-kamarada-15-5-mais-alinhado-com-o-opensuse-leap-e-com-outras-distribuicoes %}
[openSUSE Leap]:        {% post_url pt/2020-12-06-opensuse-leap-e-opensuse-tumbleweed-qual-e-a-diferenca %}
[15.2]:                 {% post_url pt/2020-09-11-linux-kamarada-15.2-venha-para-o-lado-verde-elegante-e-moderno-da-forca %}
[repos]:                https://en.opensuse.org/Package_repositories#Official_Repositories
[zypper]:               https://en.opensuse.org/Portal:Zypper
[SOCKS]:                https://pt.wikipedia.org/wiki/SOCKS
[telegram-tor]:         {% post_url pt/2022-02-26-dica-como-usar-o-telegram-via-tor %}
[tor-with-a-browser]:   https://support.torproject.org/pt-BR/tbb/#tbb_tbb-9
[Google Chrome]:        {% post_url pt/2019-07-05-18-aplicativos-que-voce-pode-usar-do-mesmo-jeito-no-linux-e-no-windows-parte-1 %}#2-google-chrome
[GNOME]:                https://br.gnome.org/
[Censura]:              https://support.torproject.org/pt-BR/censorship/
