---
date: 2021-03-10 10:00:00 GMT-3
image: '/files/2021/03/moving-to-gitlab.jpg'
layout: post
nickname: 'moving-to-gitlab'
title: 'Linux Kamarada está migrando para o GitLab'
---

{% include image.html src='/files/2021/03/moving-to-gitlab.jpg' %}

O Projeto Linux Kamarada tem o orgulho de anunciar que está se [mudando para o **GitLab**][moving-to-gitlab].

Durante os primeiros anos do projeto, o código-fonte e o _site_ foram hospedados no [GitHub], e eu sou grato por isso. No entanto, depois da [compra do GitHub pela Microsoft][microsoft-github] — que não tem bom histórico de relacionamento com _softwares_ livres — e, mais recentemente, do [episódio envolvendo o projeto youtube-dl][youtube-dl], receio que o GitHub não seja mais o melhor lugar para hospedar meu projeto. E, no momento, a melhor alternativa ao GitHub é o [GitLab].

Eu já usava o GitLab no trabalho e decidi usá-lo para o Linux Kamarada também, seguindo outros projetos de _software_ livre maiores que decidiram pelo mesmo movimento, como [GNOME], [KDE], [XFCE], [VLC], [Inkscape], [Manjaro], [Kali Linux][kali-linux] e [muitos outros][gitlab-open-source].

Embora o GitHub hospede hoje milhares de projetos de código aberto, o GitHub em si não é código aberto. Por sua vez, [o GitLab disponibiliza seu código-fonte][gitlab-source], permitindo a qualquer um [contribuir] para a plataforma ou [levantar seu próprio servidor do GitLab][gitlab-install]. Eu não sou um purista do _software_ livre, não me importo de usar _software_ proprietário quando preciso, mas prefiro usar (e desenvolver) _softwares_ livres sempre que posso. Me anima imaginar que um dia posso reportar um _bug_ para o GitLab (talvez até mesmo corrigi-lo) ou ajudar na tradução.

Estou migrando os repositórios do GitHub para o GitLab. Em breve, o código-fonte do Linux Kamarada estará disponível em [gitlab.com/kamarada](https://gitlab.com/kamarada). Os repositórios antigos em [github.com/kamarada](https://github.com/kamarada) serão arquivados.

No momento, este _site_ está sendo servido pelo [GitLab Pages][gitlab-pages], análogo ao [GitHub Pages][github-pages]. Vale observar que o GitHub Pages suporta apenas o gerador de _site_ estático [Jekyll] (ou você pode usar qualquer outro, desde que compile o _site_ em seu computador), enquanto o GitLab Pages suporta o Jekyll e [outros geradores de _sites_ estáticos][gitlab-ssg], a exemplo do [Hugo].

Ainda sobre o GitLab, por último, mas não menos importante, eu gosto da [_issue board_][gitlab-issues] ("quadro de _issues_"), que é a interface que o GitLab oferece para gerenciar as _issues_. O conceito de _issue_ é um tanto amplo e abstrato, mas você pode entender como "coisas a fazer" em um projeto: funcionalidades, _bugs_ ou, em se tratando de um _blog_, textos para escrever. Se você já viu algum quadro [Kanban], vai achar o visual (e o uso) bastante parecido:

{% include image.html src='/files/2021/03/gitlab-kanban.jpg' caption='Borrei os nomes dos textos, afinal de contas, vocês não querem spoiler, não é mesmo?' %}

{% capture atualizacao %}Se você também pensa em migrar seus repositórios de código-fonte do GitHub para o GitLab, veja o [passo a passo]({% post_url pt/2021-06-25-como-migrar-projetos-do-github-para-o-gitlab %}) que preparei mostrando como você pode fazer isso.{% endcapture %}
{% include update.html date="25/06/2021" message=atualizacao %}

Aproveitando a oportunidade, quero falar brevemente sobre outra novidade, que é a **mudança de endereço**: de [kamarada.github.io](https://kamarada.github.io/) para [linuxkamarada.com](https://linuxkamarada.com/). Deixarei o _site_ antigo no ar como uma espécie de arquivo/cópia dos textos antigos e para avisar as pessoas do endereço novo. Se você tem o Linux Kamarada nos favoritos, atualize o _link_!

Esse domínio foi registrado na [Namecheap]. É uma registrar (empresa que registra domínios) americana que existe desde o ano 2000. Ela me chamou a atenção por ser a registrar usada por algumas das distribuições [Linux] mais conhecidas, como [Arch Linux][archlinux], [Zorin OS][zorinos], [KDE neon][kde-neon] e [MX Linux][mxlinux] (você pode verificar isso com a ajuda do comando **[whois]**, por exemplo: `whois archlinux.org`). Além disso, a Namecheap tem um recurso de privacidade gratuito e que vem ativado por padrão, que é o [WhoisGuard], que protege os dados sensíveis dos usuários (para muitos domínios `.com.br`, o **whois** informa o nome e o CPF de alguém, o que é um vazamento de dados grave). Também achei interessante que a Namecheap aceita [Bitcoin].

[moving-to-gitlab]:     https://about.gitlab.com/community/moving-to-gitlab/
[github]:               https://github.com/
[microsoft-github]:     https://diolinux.com.br/noticias/sobre-compra-do-github-pela-microsoft.html
[youtube-dl]:           {% post_url pt/2020-10-25-microsoft-tira-do-ar-repositorio-github-do-projeto-de-codigo-aberto-youtube-dl %}
[gitlab]:               https://gitlab.com/
[gnome]:                https://about.gitlab.com/blog/2018/05/31/welcome-gnome-to-gitlab/
[kde]:                  https://about.gitlab.com/blog/2020/06/29/welcome-kde/
[xfce]:                 https://simon.shimmerproject.org/2020/04/30/xfce-switches-to-gitlab/
[vlc]:                  https://gitlab.com/gitlab-org/gitlab/-/issues/26534
[inkscape]:             https://inkscape.org/news/2017/06/10/inkscape-moves-gitlab/
[manjaro]:              https://twitter.com/ManjaroLinux/status/1004822535136075776
[kali-linux]:           https://about.gitlab.com/blog/2021/02/18/kali-linux-movingtogitlab/
[gitlab-open-source]:   https://about.gitlab.com/solutions/open-source/projects/
[gitlab-source]:        https://gitlab.com/gitlab-org/gitlab
[gitlab-install]:       https://about.gitlab.com/install/
[contribuir]:           https://about.gitlab.com/community/contribute/
[gitlab-pages]:         https://docs.gitlab.com/ee/user/project/pages/
[github-pages]:         https://pages.github.com/
[jekyll]:               https://jekyllrb.com/
[gitlab-ssg]:           https://about.gitlab.com/blog/2016/06/17/ssg-overview-gitlab-pages-part-3-examples-ci/
[hugo]:                 https://gohugo.io/
[gitlab-issues]:        https://docs.gitlab.com/ee/user/project/issue_board.html
[kanban]:               https://en.wikipedia.org/wiki/Kanban_(development)
[namecheap]:            https://bit.ly/kamarada-namecheap
[linux]:                https://www.vivaolinux.com.br/linux/
[archlinux]:            https://archlinux.org/
[zorinos]:              https://zorinos.com/
[kde-neon]:             https://neon.kde.org/
[mxlinux]:              https://mxlinux.org/
[whois]:                https://linux.die.net/man/1/whois
[whoisguard]:           https://www.namecheap.com/security/whoisguard/
[bitcoin]:              {% post_url pt/2018-12-12-bitcoin-para-iniciantes-com-a-carteira-electrum %}
