---
date: 2021-12-30 18:00:00 GMT-3
image: '/files/2021/12/kamarada-15.3.jpg'
layout: post
published: true
nickname: 'kamarada-15.3-final'
title: 'Linux Kamarada 15.3: ano novo, distribuição nova'
---

{% include image.html src='/files/2021/12/kamarada-15.3.jpg' %}

É com enorme satisfação que venho a público anunciar que o **Linux Kamarada 15.3** está pronto para ser usado por todos! O Linux Kamarada 15.3 é uma distribuição [Linux] baseada em outra distribuição maior, o [openSUSE Leap 15.3]. Enquanto o [openSUSE Leap] tem um propósito mais geral, fornecendo um sistema operacional estável para computadores pessoais e servidores, assim como ferramentas para desenvolvedores e administradores de sistemas, o Linux Kamarada é focado em computadores pessoais e em usuários iniciantes.

Novos usuários podem encontrar a nova versão na página [Download]. Se você já usa o Linux Kamarada, confira orientações sobre como atualizar para a nova versão mais adiante.

Confira a seguir o que mudou no Linux Kamarada da versão anterior ([15.2]) para a nova versão (15.3).

<div class="media mb-3">
    <img src="/files/2021/12/media-optical.svg" class="mr-3" style="max-width: 60px;">
    <div class="media-body align-self-center">
        <h2 class="mt-0 mb-0">Mudanças na mídia Live</h2>
    </div>
</div>

Na página de [Download] você já pode perceber uma novidade do Linux Kamarada 15.3: agora o Linux Kamarada oferece apenas uma [imagem Live][live], que pode ser usada tanto em Português Brasileiro quanto em Inglês Internacional. Nas versões [15.1] e 15.2, eram disponibilizadas duas imagens Live, uma para cada idioma.

Ao iniciar a imagem Live, uma nova tela de boas vindas permite selecionar o idioma desejado, assim como experimentar ou instalar o Linux Kamarada:

<div class="row">
    <div class="col-md">
        {% include image.html src='/files/2021/12/kamarada-15.3-welcome-01-pt.jpg' caption='Português ou Inglês?' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2021/12/kamarada-15.3-welcome-02-pt.jpg' caption='Testar ou instalar?' %}
    </div>
</div>

Mesmo que escolha testar o Linux Kamarada, você pode instalá-lo a qualquer momento, iniciando o instalador a partir do ícone que está na Área de Trabalho. Aliás, essa é outra novidade, já que em versões anteriores o ícone do instalador estava na _dock_ do [GNOME], e na versão 15.3 foi movido para a Área de Trabalho, onde fica mais visível.

{% include image.html src='/files/2021/12/kamarada-15.3-installer-icon-pt.jpg' caption='Ícone do instalador na Área de Trabalho' %}

E agora o instalador [Calamares] é o único responsável por fazer todas as configurações durante a instalação:

<div class="row">
    <div class="col-md">
        {% include image.html src='/files/2021/12/kamarada-15.3-calamares-01-pt.jpg' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2021/12/kamarada-15.3-calamares-02-pt.jpg' %}
    </div>
</div>

Assim, no final da instalação, ao reiniciar o computador, o sistema já estará pronto para uso.

<div class="media mb-3">
    <img src="/files/2021/12/multimedia-photo-viewer.svg" class="mr-3" style="max-width: 60px;">
    <div class="media-body align-self-center">
        <h2 class="mt-0 mb-0">Novos planos de fundo</h2>
    </div>
</div>

Como de costume, essa nova versão vem com novos papéis de parede, com fotos de belas praias da cidade de [Florianópolis]:

<div class="row">
    <div class="col-md">
        {% include image.html src='/files/2021/12/kamarada-15.3-lock-pt.jpg' caption='Tela de bloqueio' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2021/12/kamarada-15.3-desktop-pt.jpg' caption='Área de trabalho' %}
    </div>
</div>

Se preferir, você também pode usar os papéis de parede de versões anteriores do Linux Kamarada ou o padrão do openSUSE Leap (ou definir suas próprias imagens como papéis de parede, claro).

<div class="media mb-3">
    <img src="/files/2021/12/application-vnd.flatpak.svg" class="mr-3" style="max-width: 60px;">
    <div class="media-body align-self-center">
        <h2 class="mt-0 mb-0">Suporte nativo ao Flatpak</h2>
    </div>
</div>

O **[Flatpak]** é um gerenciador de pacotes independente de distribuição. Até pouco tempo atrás, diferentes distribuições Linux usavam diferentes formatos de pacotes, geralmente incompatíveis entre si. Por exemplo, [Debian] e [Ubuntu] usam pacotes [DEB], enquanto [RedHat], [Fedora] e [openSUSE] (e, portanto, o Linux Kamarada) usam pacotes [RPM]. O Flatpak trouxe uma alternativa mais simples para instalar programas em diferentes distribuições: contanto que o Flatpak esteja instalado no sistema, o mesmo pacote Flatpak pode ser instalado em qualquer distribuição.

O Linux Kamarada 15.3 já traz o Flatpak instalado por padrão. O [Flathub], o principal repositório do Flatpak, também já vem pré-configurado. Você pode simplesmente instalar aplicativos usando o Flatpak, sem precisar fazer sua [instalação e configuração inicial][flatpak-setup]. Inclusive a "loja" de aplicativos do GNOME (o _app_ **Programas**) suporta o Flatpak:

<div class="row">
    <div class="col-md">
        {% include image.html src='/files/2021/12/kamarada-15.3-flatpak-01-pt.jpg' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2021/12/kamarada-15.3-flatpak-02-pt.jpg' %}
    </div>
</div>

Se você já usava o Linux Kamarada e atualizou a partir de uma versão anterior, pode ser necessário configurar o Flathub. Para isso, antes do primeiro uso, execute o comando:

```
# flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
```

<div class="media mb-3">
    <img src="/files/2021/12/audio-card.svg" class="mr-3" style="max-width: 60px;">
    <div class="media-body align-self-center">
        <h2 class="mt-0 mb-0">Melhor suporte a hardware</h2>
    </div>
</div>

Versões anteriores do Linux Kamarada traziam apenas os _drivers_ mais básicos do [X.Org] instalados por padrão, o que gerava problemas principalmente para usuários de placas de vídeo [NVIDIA]. A imagem Live do Linux Kamarada 15.3 traz mais _drivers_, inclusive os de código aberto **[nouveau]** e **nv**, instalados por padrão.

Também foram incluídos alguns utilitários que podem ser úteis para diagnosticar problemas de _hardware_ e de sistemas de arquivos: **[inxi]**, **[lshw]**, **[lsof]**, **[lspci]** e **[smartctl]**.

## Onde baixo o Linux Kamarada?

A página [Download] foi atualizada com o _link_ para _download_ da versão 15.3 Final.

**Observação:** não é recomendado usar o Linux Kamarada em servidores, embora seja possível. Nesse caso, a distribuição openSUSE Leap é mais adequada, por fornecer uma opção de instalação própria para servidores, sem área de trabalho.

## O que faço em seguida?

Quando o _download_ estiver concluído, verifique a integridade da imagem ISO calculando sua soma de verificação (_checksum_), que deve coincidir com a soma que aparece na página [Download]. Se a soma não corresponder, faça o _download_ novamente.

Se não estiver com pressa, é recomendado verificar também a autenticidade da imagem ISO.

A impressão digital (_fingerprint_) da chave pública do Projeto Linux Kamarada é:

```
6b18 52e7 764f b302 b805 a4a0 a575 bcce 1737 8ecc
```

Para importá-la, execute os comandos a seguir:

```
$ wget https://build.opensuse.org/projects/home:kamarada:15.3/public_key
$ gpg --import public_key
```

Para mais informações sobre essas verificações, leia:

- [Verificação de integridade e autenticidade com SHA-256 e GPG][how-to-verify-iso]

Com a imagem ISO baixada e as verificações feitas e bem-sucedidas, você tem 3 opções:

<ul class="list-unstyled">
    <li class="media mb-3">
        <img src="/files/2020/02/disk-burner.svg" class="mr-3" style="max-width: 48px;">
        <div class="media-body">
            <h6 class="mt-0">1) Gravar a imagem ISO em um DVD (gerando, assim, um LiveDVD)</h6>

            <p>Use um aplicativo como o <a href="https://cdburnerxp.se/">CDBurnerXP</a> (no Windows), o <a href="http://www.k3b.org/">K3b</a> (em um Linux com KDE) ou o <a href="https://wiki.gnome.org/Apps/Brasero">Brasero</a> (em um Linux com GNOME) para gravar a imagem ISO em um DVD. Ligue o computador com o LiveDVD na unidade de DVD para iniciar o Linux Kamarada.</p>

            <p>Esse <em>post</em> pode te ajudar com a gravação do DVD:</p>

            <ul><li><a href="{% post_url pt/2016-04-22-como-gravar-um-livedvd %}">Como gravar um LiveDVD</a></li></ul>
        </div>
    </li>

    <li class="media mb-3">
        <img src="/files/2020/02/usb-creator.svg" class="mr-3" style="max-width: 48px;">
        <div class="media-body">
            <h6 class="mt-0">2) Gravar a imagem ISO em um <em>pendrive</em> (gerando, assim, um LiveUSB)</h6>

            <p>Use o <a href="https://www.ventoy.net/">Ventoy</a> (disponível para Windows e Linux) para preparar o <em>pendrive</em> e copie a imagem ISO para ele. Ligue o computador com o LiveUSB para iniciar o Linux Kamarada.</p>

            <p>Esse <em>post</em> pode te ajudar com a preparação do LiveUSB:</p>

            <ul><li><a href="{% post_url pt/2020-07-29-ventoy-crie-pendrives-multiboot-simplesmente-copiando-imagens-iso-para-ele %}">Ventoy: crie um pendrive multiboot simplesmente copiando imagens ISO para ele</a></li></ul>
        </div>
    </li>

    <li class="media mb-3">
        <img src="/files/2020/02/virtualbox.svg" class="mr-3" style="max-width: 48px;">
        <div class="media-body">
            <h6 class="mt-0">3) Criar uma máquina virtual e usar a imagem ISO para iniciá-la</h6>

            <p>Essa opção te possibilita testar o Linux Kamarada sem precisar reiniciar o computador e sair do sistema operacional que você já usa.</p>

            <p>Para mais informações, leia:</p>

            <ul><li><a href="{% post_url pt/2019-10-08-virtualbox-a-forma-mais-facil-de-conhecer-o-linux-sem-precisar-instala-lo %}">VirtualBox: a forma mais fácil de conhecer o Linux sem precisar instalá-lo</a></li></ul>
        </div>
    </li>

    <li class="media mb-3">
        <div class="media-body">
            Depois de testar o Linux Kamarada, caso queira instalá-lo no seu computador ou na máquina virtual, inicie o instalador fazendo um duplo-clique nesse ícone, situado na Área de Trabalho.
        </div>
        <img src="/files/2021/12/calamares.svg" class="ml-3" style="max-width: 48px;">
    </li>
</ul>

O instalador fará o particionamento, a cópia do sistema para o computador e todas as configurações iniciais (idioma, leiaute de teclado, fuso horário, usuário e senha, etc.). Ao final, reinicie o computador para começar a usar o sistema instalado.

## E se eu já uso o Linux Kamarada?

A imagem ISO não se destina a atualizações, apenas para testes e instalações novas.

Se você já usa o Linux Kamarada 15.2, pode atualizar para o 15.3 seguindo o tutorial:

- [Linux Kamarada e openSUSE Leap: como atualizar da versão 15.2 para a 15.3][upgrade-to-15.3]

Se você usa uma versão em desenvolvimento do Linux Kamarada 15.3 ([Beta] ou [RC]), basta obter as atualizações para os pacotes, como de costume:

- [Como obter atualizações para o Linux openSUSE][howto-update]

## E se eu já uso o openSUSE Leap?

Você já usa o openSUSE Leap e quer transformá-lo no Linux Kamarada? Simples!

Basta adicionar o repositório do Linux Kamarada e instalar o pacote **[patterns-kamarada-gnome]**.

Você pode fazer isso de duas formas: pela interface gráfica, usando a instalação com 1 clique (*1-Click Install*), ou pelo terminal, usando o gerenciador de pacotes **zypper**. Escolha a que prefere.

Para instalar usando a instalação com 1 clique, clique no botão abaixo:

<p class='text-center'><a class='btn btn-sm btn-outline-primary' href='/downloads/patterns-kamarada-gnome-pt.ymp'><i class='fas fa-bolt'></i> Instalação com 1 clique</a></p>

Para instalar usando o terminal, primeiro adicione o repositório do Linux Kamarada:

```
# zypper addrepo -f -K -n "Linux Kamarada" -p 95 "http://c3sl.dl.osdn.jp/storage/g/k/ka/kamarada/\$releasever/openSUSE_Leap_\$releasever/" kamarada
```

O repositório do Linux Kamarada é gentilmente hospedado pela [OSDN], que replica seu conteúdo em alguns servidores de espelho (_mirrors_) espalhados pelo mundo. A instalação com 1 clique e o comando acima instruem seu sistema a usar o espelho brasileiro do repositório, gentilmente hospedado pela [C3SL (UFPR)][c3sl].

Se você está em outro país, consulte a [lista de espelhos na _wiki_ do Linux Kamarada no GitLab][mirrors].

Depois, instale o pacote **patterns-kamarada-gnome**:

```
# zypper in patterns-kamarada-gnome
```

Se você já usa a área de trabalho GNOME, provavelmente será necessário baixar poucos pacotes. Se você usa outra área de trabalho, o tamanho do _download_ será maior.

Quando a instalação terminar, se você criar um novo usuário, perceberá que ele receberá as configurações padrão do Linux Kamarada (como tema, papel de parede, etc.). Usuários já existentes podem seguir usando suas personalizações ou ajustar a aparência do sistema (por exemplo, usando os aplicativos **Ajustes** e **Configurações**).

## Onde obtenho ajuda?

A página [Ajuda] indica alguns lugares onde é possível obter auxílio com o Linux Kamarada, o openSUSE ou distribuições Linux em geral (incluindo essas duas).

O canal de suporte preferido pelos usuários tem sido o grupo [@LinuxKamarada](https://t.me/LinuxKamarada) no [Telegram], que é um aplicativo de mensagens que pode ser instalado ou usado a partir do navegador.

## Até quando receberá suporte?

De acordo com a [_wiki_ do openSUSE][lifetime], o openSUSE Leap 15.3 deve receber atualizações até o fim de novembro de 2022.

Como o Linux Kamarada é baseado no openSUSE Leap, usuários do Linux Kamarada são, por tabela, usuários do openSUSE Leap também, e recebem essas mesmas atualizações.

Portanto, o Linux Kamarada 15.3 também receberá suporte até o fim de novembro de 2022.

## Onde obtenho os códigos-fonte?

Como todo projeto de _software_ livre, o Linux Kamarada disponibiliza seus códigos-fonte para quem quiser estudá-los, adaptá-los ou contribuir com o projeto.

O desenvolvimento do Linux Kamarada ocorre no [GitLab] e no [Open Build Service][obs]. Lá podem ser obtidos os códigos-fonte dos pacotes desenvolvidos especificamente para esse projeto (até mesmo [o código-fonte deste _site_][kamarada-website] que você lê está disponível).

Os códigos-fonte de pacotes herdados do openSUSE Leap podem ser obtidos diretamente dessa distribuição. Se precisar de ajuda para fazer isso, entre em contato, posso ajudar.

## Sobre o Linux Kamarada

O Projeto Linux Kamarada visa divulgar e promover o Linux como um sistema operacional robusto, seguro, versátil e fácil de usar, adequado para o uso diário seja em casa, no trabalho ou no servidor. O projeto começou como um _blog_ sobre o openSUSE, que é a distribuição Linux que eu uso há 9 anos (desde [abril de 2012][vinyanalista], na época eu instalei o openSUSE 11.4 e depois atualizei para o 12.1). Agora o projeto disponibiliza sua própria distribuição Linux, que traz vários programas apresentados no _blog_ já instalados e prontos para uso.

A distribuição Linux Kamarada é baseada no openSUSE Leap e se destina ao uso em _desktops_ em casa e no trabalho, seja em empresas privadas ou em órgãos públicos. Contém os programas essenciais a qualquer instalação do Linux e uma área de trabalho com visual moderno e agradável.

## Ficha técnica

Para que seja possível comparar esta versão com a anterior e as futuras, segue um resumo do _software_ contido no Linux Kamarada 15.3 Final Build 4.3:

- _kernel_ Linux 5.3.18
- servidor gráfico X.Org 1.20.3 (sem Wayland)
- área de trabalho GNOME 3.34.7 acompanhada de seus principais aplicativos (_core apps_), como Arquivos (antigo Nautilus), Calculadora, Terminal, Editor de texto (gedit), Captura de tela e outros
- suíte de aplicativos de escritório LibreOffice 7.1.4.2
- navegador Mozilla Firefox 91.4.0 ESR (navegador padrão)
- navegador Chromium 96 (navegador alternativo disponível)
- reprodutor multimídia VLC 3.0.16
- cliente de _e-mail_ Evolution
- centro de controle do YaST
- Brasero
- CUPS 2.2.7
- Firewalld 0.9.3
- GParted 0.31.0
- HPLIP 3.20.11
- Java (OpenJDK) 11.0.13
- KeePassXC 2.6.6
- KolourPaint 20.04.2
- Linphone 4.1.1
- PDFsam Basic 4.2.10
- Pidgin 2.13.0
- Python 2.7.18 e 3.6.15
- Samba 4.13.13
- Tor 0.4.6
- Transmission 2.94
- Vim 8.0
- Wine 6.0
- instalador Calamares 3.2.36
- Flatpak 1.10.5
- jogos: Aisleriot (Paciência), Copas, Iagno (Reversi), Mahjongg, Minas (Campo minado), Nibbles (Cobras), Quadrapassel (Tetris), Sudoku, Xadrez

Essa lista não é exaustiva, mas já dá uma noção do que se encontra na distribuição.

[Linux]:                    https://www.vivaolinux.com.br/linux/
[openSUSE Leap 15.3]:       {% post_url pt/2021-06-02-opensuse-leap-153-constroi-um-caminho-para-a-versao-empresarial %}
[openSUSE Leap]:            {% post_url pt/2020-12-06-opensuse-leap-e-opensuse-tumbleweed-qual-e-a-diferenca %}
[Download]:                 /pt/download
[15.2]:                     {% post_url pt/2020-09-11-linux-kamarada-15.2-venha-para-o-lado-verde-elegante-e-moderno-da-forca %}
[live]:                     {% post_url pt/2015-11-25-o-que-e-um-livecd-um-livedvd-um-liveusb %}
[15.1]:                     {% post_url pt/2020-02-24-kamarada-15.1-vem-com-tudo-que-voce-precisa-para-usar-o-linux-no-dia-a-dia %}
[GNOME]:                    https://br.gnome.org/
[Calamares]:                https://calamares.io/
[Florianópolis]:            https://pt.wikipedia.org/wiki/Florianópolis
[flatpak]:                  https://flatpak.org/
[debian]:                   https://www.debian.org/index.pt.html
[ubuntu]:                   https://ubuntu.com/
[deb]:                      https://pt.wikipedia.org/wiki/.deb
[redhat]:                   https://www.redhat.com/pt-br/technologies/linux-platforms/enterprise-linux
[fedora]:                   https://getfedora.org/pt_BR/
[opensuse]:                 https://www.opensuse.org/
[rpm]:                      https://pt.wikipedia.org/wiki/RPM_(software)
[Flathub]:                  https://flathub.org/
[flatpak-setup]:            https://flatpak.org/setup/openSUSE/
[X.Org]:                    https://www.x.org/wiki/
[NVIDIA]:                   https://www.nvidia.com/pt-br/
[nouveau]:                  https://nouveau.freedesktop.org/
[inxi]:                     https://smxi.org/docs/inxi.htm
[lshw]:                     https://www.ezix.org/project/wiki/HardwareLiSter
[lsof]:                     https://man7.org/linux/man-pages/man8/lsof.8.html
[lspci]:                    https://man7.org/linux/man-pages/man8/lspci.8.html
[smartctl]:                 https://www.smartmontools.org/
[how-to-verify-iso]:        {% post_url pt/2018-10-06-verificacao-de-integridade-e-autenticidade-com-sha-256-e-gpg %}
[upgrade-to-15.3]:          {% post_url pt/2021-12-23-linux-kamarada-e-opensuse-leap-como-atualizar-da-versao-152-para-a-153 %}
[Beta]:                     {% post_url pt/2021-12-11-linux-kamarada-15-3-beta-ajude-a-testar-a-melhor-versao-da-distribuicao %}
[RC]:                       {% post_url pt/2021-12-28-linux-kamarada-libera-versao-candidata-a-lancamento-153-rc %}
[howto-update]:             {% post_url pt/2019-05-14-como-obter-atualizacoes-para-o-linux-opensuse %}
[patterns-kamarada-gnome]:  https://software.opensuse.org/package/patterns-kamarada-gnome
[osdn]:                     https://osdn.net/projects/kamarada/
[c3sl]:                     https://www.c3sl.ufpr.br/
[mirrors]:                  https://gitlab.com/kamarada/Linux-Kamarada-GNOME/-/wikis/Mirrors
[ajuda]:                    /pt/ajuda
[telegram]:                 {% post_url pt/2021-05-09-o-mensageiro-telegram %}
[lifetime]:                 https://en.opensuse.org/Lifetime
[GitLab]:                   https://gitlab.com/kamarada
[obs]:                      https://build.opensuse.org/project/subprojects/home:kamarada
[kamarada-website]:         https://gitlab.com/kamarada/kamarada.gitlab.io
[vinyanalista]:             https://vinyanalista.github.io/blog/2012/04/21/problemas-envolvendo-bootloaders-mbr-e-tabela-de-particoes/
