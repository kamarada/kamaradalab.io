---
date: '2021-04-12 23:30:00 GMT-3'
image: '/files/2021/04/kick-off.jpg'
layout: post
nickname: 'kick-off'
title: 'Como fazer uma distro baseada no openSUSE (bastidores do Linux Kamarada): começando'
---

{% include image.html src='/files/2021/04/kick-off.jpg' %}

Como fazer uma distribuição [Linux] baseada no [openSUSE] é um tutorial que eu imagino fazer há algum tempo já. Agora que o Linux Kamarada já tem duas versões lançadas ([15.1], [descontinuada], e [15.2], atual) e uma terceira a caminho (a 15.3) creio que consigo explicar como fazer. Decidi aproveitar a oportunidade, já que comecei a trabalhar na próxima versão, para registrar o processo e compartilhar como faço, pro caso de ser útil para mais alguém.

Não necessariamente seu objetivo precisa ser criar uma distribuição para disponibilizar na Internet. Pode ser que você só queira personalizar o openSUSE (ou — por que não? — o Linux Kamarada) para instalar nos computadores da empresa ou órgão público em que trabalha. Ou talvez você esteja desenvolvendo um _software_ e queira criar uma [imagem Live][live] com seu _software_ já instalado, de modo a facilitar o teste por potenciais usuários ou a demonstração em eventos. Um sistema Live é um ambiente controlado, ideal para testes.

Seja qual for seu objetivo, esse _post_ e os seguintes podem te ajudar a alcançá-lo.

O tutorial será formado na verdade por alguns _posts_. À medida em que eu for avançando no desenvolvimento do Linux Kamarada 15.3, vou compartilhando o que estou fazendo e como.

Pretendo fazer algo prático, então não vou me aprofundar em detalhes sobre as ferramentas — principalmente [Git], [Kiwi] e [Open Build Service (OBS)][obs] — de modo que esse tutorial não dispensa a consulta a outras fontes. Aliás, muito do que aprendi foi "dando a cara a tapa": pesquisando, tentando, errando e pedindo ajuda em fóruns, grupos e listas de discussão. Então esse tutorial será mais para mostrar o caminho e atalhar a pesquisa, em vez de ser auto contido. Depois, com o tempo, posso fazer outros tutoriais para preencher as lacunas.

Hoje, veremos como iniciar o desenvolvimento da distribuição: como criar o projeto no Open Build Service e como criar o primeiro pacote. Vou mostrar como dei o pontapé inicial no Linux Kamarada 15.3. Vamos lá!

## Ingredientes: do que vamos precisar

Como eu disse na [notícia de lançamento do Linux Kamarada 15.2][15.2], o desenvolvimento do Linux Kamarada ocorre no [GitHub] e no OBS. Na verdade, para a versão 15.3, estou [migrando do GitHub para o GitLab][moving-to-gitlab]. Eu prefiro usar o Git para armazenar e gerenciar o código-fonte e o OBS para compilar e empacotar, embora também seja possível fazer tudo isso no OBS. Fique à vontade para usar o GitHub, o [GitLab], outro serviço de Git (como o [Bitbucket]) ou só o OBS.

Se você ainda não conhece o OBS, leia a primeira seção — Conheça o Open Build Service (OBS)
 — do texto:

- [Integrando Open Build Service com GitLab][obs-gitlab]

Guarde esse texto porque vamos voltar a ele mais tarde.

Se você prefere usar o GitHub, leia esse texto, em vez do anterior:

- [Integrando Open Build Service com GitHub][obs-github]

Vou assumir que você já tem uma conta em um serviço como o GitHub ou o GitLab, que já consegue entrar ("fazer _login_") nela e que já tem familiaridade com o Git.

Para hospedar os repositórios do Git do projeto, eu recomendo criar uma organização (no caso do GitHub) ou um grupo (no caso do GitLab), exemplos: [github.com/kamarada][github] ou [gitlab.com/kamarada][gitlab]. Mas você também pode usar seu perfil pessoal, exemplo: [gitlab.com/antoniomedeiros](https://gitlab.com/antoniomedeiros). Pro OBS, isso não faz diferença.

Também vou assumir que você já consegue entrar com sua conta do openSUSE em [build.opensuse.org][buildoo]. Se você ainda não tem uma conta, crie uma clicando no _link_ **Sign Up** (inscrever-se) no topo da página.

Por fim, também vou assumir que você está usando uma das [distribuições do openSUSE][leap-tumbleweed] (Leap ou Tumbleweed) ou uma distribuição baseada nelas (como o Linux Kamarada). Se estamos falando em fazer uma distro baseada no openSUSE, nada me parece mais natural.

Instale o Git e o **[osc]** (o cliente de linha de comando do OBS) no seu computador:

```
# zypper in git osc
```

## O padrão do Linux Kamarada

Caso você não saiba o que são padrões no openSUSE, eu expliquei esse conceito no texto:

- [O que são padrões e como instalá-los no openSUSE][patterns]

Aquele texto apresenta o ponto de vista do usuário. Agora veremos o ponto de vista de quem faz a distribuição.

Para definir quais pacotes compõem o Linux Kamarada, eu criei um padrão para ele, tomando como exemplo outros padrões do openSUSE:

{% include image.html src='/files/2021/04/kick-off-01-pt.jpg' %}

Por isso é fácil, como eu disse na [notícia de lançamento do Linux Kamarada 15.2][15.2], transformar qualquer instalação do openSUSE Leap em uma instalação do Linux Kamarada: basta adicionar o repositório da distribuição e instalar o pacote **[patterns-kamarada-gnome]**.

## Como criar padrões

Se você quiser ver como padrões são definidos no openSUSE, acesse [software.opensuse.org][softwareoo] e pesquise por `patterns`. Vários pacotes cujo nome contém `patterns` são listados. Todos eles correspondem a padrões. Clique em um pacote.

A página seguinte lista a disponibilidade desse pacote para várias distribuições. Abaixo da distribuição desejada, clique em **official release** (versão oficial) para ver o código-fonte desse pacote no OBS:

{% include image.html src='/files/2021/04/kick-off-02.jpg' %}

{% include image.html src='/files/2021/04/kick-off-03.jpg' %}

Nessa tela, estamos vendo os arquivos que compõem o código-fonte do pacote **patterns-xfce** no projeto **openSUSE:Leap:15.2** do OBS. Desses arquivos, o principal é o arquivo _spec_.

Caso você não saiba o que é um arquivo _spec_ e/ou é novo no empacotamento [RPM], leia (em inglês):

- [Fedora RPM Guide][fedora-rpm-guide]
- [RPM Packaging Guide][rpm-packaging-guide]
- [openSUSE:Packaging guidelines - openSUSE Wiki][opensuse-packaging]

{% capture fedora_rpm_guide %}

Atualizei o _link_ do Fedora RPM Guide para uma versão arquivada na Wayback Machine, porque o Projeto Fedora removeu o livro do seu _site_ por algum motivo. Diante disso, decidi disponibilizar também [minha própria cópia em PDF][fedora-rpm-guide-pdf] que baixei lá atrás, em 2014.

[fedora-rpm-guide-pdf]:     /files/2024/10/fedora-rpm-guide.pdf

{% endcapture %}

{% include update.html date="28/10/2024" message=fedora_rpm_guide %}

De forma resumida, é esse arquivo _spec_ que define o pacote RPM. No caso do padrão, esse arquivo diz o nome e a descrição do padrão e suas dependências ([pacotes requeridos, recomendados][recommended] ou sugeridos). Por exemplo, o arquivo _spec_ do padrão do XFCE:

{% include image.html src='/files/2021/04/kick-off-04.jpg' %}

O código-fonte do padrão do Linux Kamarada pode ser visto em:

- [gitlab.com/kamarada/patterns](https://gitlab.com/kamarada/patterns/-/tree/15.2) (o _link_ aponta para o _branch_ 15.2)

Os arquivos mais importantes são:

- o [`patterns-kamarada-gnome.spec`](https://gitlab.com/kamarada/patterns/-/blob/15.2/patterns-kamarada-gnome.spec), que define o padrão, e
- o [`pattern-kamarada-gnome.png`](https://gitlab.com/kamarada/patterns/-/blob/15.2/pattern-kamarada-gnome.png), que é o ícone do padrão.

Abrindo o arquivo _spec_, logo no início temos o nome (`Name`), a descrição (`Summary`) e a versão (`Version`) do padrão (tenho que atualizar esse endereço — `Url`):

```
Name:           patterns-kamarada-gnome
Summary:        Linux Kamarada with GNOME desktop
Url:            https://github.com/openSUSE/patterns
Version:        15.2
Release:        0
Group:          Metapackages
License:        MIT
BuildRoot:      %{_tmppath}/%{name}-%{version}-build
```

Mais adiante, temos a linha que define o ícone do padrão:

```
Source0:        pattern-kamarada-gnome.png
```

E ao longo do arquivo temos as dependências do padrão.

O padrão do Linux Kamarada depende de outros padrões:

```
Requires:       pattern() = apparmor
Requires:       pattern() = enhanced_base
Requires:       pattern() = x11_enhanced
Requires:       pattern() = gnome_basis
```

(essas linhas não estão juntas no arquivo, estão espalhadas, não vou reproduzir o arquivo fielmente aqui pro texto ficar mais fluido)

Achei melhor não criar muitas dependências de outros padrões, pra ter um controle maior sobre os pacotes que são instalados. Em vez disso, eu copiei as dependências que eu queria desses padrões e os mantive comentados, pra ter uma ideia de onde eu as obtive:

```
#Requires:       pattern() = enhanced_base_opt

Requires:       man-pages
Requires:       man-pages-posix

Requires:       unzip

Requires:       tcpdump
Requires:       telnet
```

Minha estratégia para os pacotes, de modo geral, é:

- pacotes que eu considero essenciais são requeridos (`Requires`) — se você não tem esses pacotes, eu não considero que você tem uma instalação do Linux Kamarada (note que a remoção desses pacotes ocasiona a remoção do padrão); e
- traduções ou outros pacotes que são úteis, mas não para todos os computadores, são recomendados (`Recommends`) ou sugeridos (`Suggests`) — por exemplo, os pacotes **[ucode-amd]** e **[ucode-intel]** são sugeridos, os dois vem instalados por padrão, mas dependendo do seu processador, você pode perfeitamente remover um ou outro (a remoção desses pacotes não ocasiona a remoção do padrão, eu considero que você continua tendo uma instalação do Linux Kamarada ainda que remova esses pacotes).

## Outros exemplos de padrões

Aqui estão exemplos de padrões que consulto para definir o meu e que talvez possam servir de referência para você também:

- **[patterns-base]:** código-fonte para os padrões apparmor, base, console, enhanced_base, minimal_base, x11 e x11_enhanced
- **[patterns-gnome]:** código-fonte para os padrões gnome_x11, gnome_basis, gnome_basic, gnome_games, gnome_imaging, gnome_internet, gnome_utilities e gnome_yast
- **[patterns-yast]:** código-fonte para os padrões yast2_basis e yast2_install_wf

Agora que você já entendeu a ideia básica por trás do padrão, vamos pôr a mão na massa.

## Criando o padrão no Git

Note que eu já tenho um repositório no Git para o meu padrão ([gitlab.com/kamarada/patterns](https://gitlab.com/kamarada/patterns)). Sendo assim, vou usá-lo como ponto de partida. Talvez você precise criar um repositório.

Eu costumo usar um _branch_ para cada versão (`15.1`, `15.2`) e um _branch_ para o desenvolvimento de cada versão  (`15.1-dev`, `15.2-dev`). Sendo assim, posso experimentar no _branch_ de desenvolvimento e mesclar com o principal quando estiver pronto.

Vou criar o _branch_ `15.3-dev` usando como ponto de partida o `15.2-dev`.

Para isso, no repositório local do Git:

```
$ git checkout 15.2-dev
$ git checkout -b 15.3-dev
```

Mudei a versão no arquivo _spec_

```
Version:        15.3
```

Fiz o _commit_:

```
$ git commit -a
```

E enviei a alteração para o repositório remoto (como estamos criando esse _branch_, no primeiro _push_ precisamos definir o _remote_):

```
$ git push --set-upstream origin 15.3-dev
```

## Criando os projetos no OBS

No [openSUSE Build Service][buildoo], acesse o seu projeto pessoal por meio do _link_ **Your Home Project** na barra lateral do OBS. Mude para a aba **Subprojects** (subprojetos). Em seguida, clique em **Create Subproject** (criar subprojeto):

{% include image.html src='/files/2021/04/kick-off-05.jpg' %}

Como nome do subprojeto (**Subproject Name**) eu defini `15.3` e como título (**Title**) eu defini `Linux Kamarada 15.3`:

{% include image.html src='/files/2021/04/kick-off-06.jpg' %}

Depois clique em **Accept** (aceitar).

Feito isso, agora temos um projeto no OBS para o Linux Kamarada 15.3 em:

- [https://build.opensuse.org/project/show/home:kamarada:15.3](https://build.opensuse.org/project/show/home:kamarada:15.3)

Entrei no projeto **home:kamarada:15.3** e criei mais um subprojeto, esse chamado `dev`, resultando no projeto **home:kamarada:15.3:dev**:

- [https://build.opensuse.org/project/show/home:kamarada:15.3:dev](https://build.opensuse.org/project/show/home:kamarada:15.3:dev)

Se você voltar à lista de subprojetos do projeto principal do usuário, verá os dois projetos recém criados:

{% include image.html src='/files/2021/04/kick-off-07.jpg' %}

## Configurando os repositórios

Entre no projeto de desenvolvimento (no meu caso, **home:kamarada:15.3:dev**), mude para a aba **Repositories** (repositórios) e clique em **Add from a Distribution** (adicionar de uma distribuição):

{% include image.html src='/files/2021/04/kick-off-08.jpg' %}

Marque o repositório do **openSUSE Leap 15.3**:

{% include image.html src='/files/2021/04/kick-off-09.jpg' %}

E no final da página também o repositório do Kiwi:

{% include image.html src='/files/2021/04/kick-off-10.jpg' %}

Os repositórios já são adicionados ao projeto assim que você os marca. Não precisa clicar em nenhum botão. Volte para a aba **Repositories**.

Temos então dois repositórios configurados para esse projeto:

- o **images**, que será usado para produzir a imagem ISO; e
- o **openSUSE_Leap_15.3**, que será usado para produzir os pacotes RPM.

A menos que você queira compilar sua distribuição para todas as arquiteturas suportadas pelo Kiwi, precisamos fazer uma "poda" nas arquiteturas do repositório **images** (imagens).

Para isso, no repositório **images**, clique no ícone de editar:

{% include image.html src='/files/2021/04/kick-off-11.jpg' %}

Deixe marcadas apenas as arquiteturas para as quais você vai compilar sua distribuição (no meu caso, apenas **x86_64**). Quando terminar, clique em **Update** (atualizar):

{% include image.html src='/files/2021/04/kick-off-12.jpg' %}

Para cada repositório do nosso projeto no OBS, podemos configurar quais outros repositórios de outros projetos ele considera na hora de obter pacotes.

No caso do openSUSE Leap 15.3, vamos adicionar o repositório de atualização.

Para isso, clique no ícone de adicionar:

{% include image.html src='/files/2021/04/kick-off-13.jpg' %}

Em **Project** (projeto), informe **openSUSE:Leap:15.3:Update**:

{% include image.html src='/files/2021/04/kick-off-14.jpg' %}

Em **Repositories** (repositórios), mantenha **standard** (padrão) e clique em **Accept**.

Como o repositório **openSUSE:Leap:15.3:Update** já considera o repositório **openSUSE:Leap:15.3**, e o OBS tem a "inteligência" de perceber isso, podemos remover o repositório **openSUSE:Leap:15.3**, pra deixar nossa configuração mais enxuta:

{% include image.html src='/files/2021/04/kick-off-15.jpg' %}

No caso das imagens do Kiwi, adicione os seguintes repositórios:

- seu próprio repositório de pacotes (no meu caso, **home:kamarada:15.3:dev/openSUSE_Leap_15.3**)
- o repositório com a versão mais recente do Kiwi (**Virtualization:Appliances:Builder/openSUSE_Leap_15.3**, recomendação da [documentação do Kiwi][kiwi-doc])
- repositório de atualização do openSUSE Leap 15.3 (**openSUSE:Leap:15.3:Update/standard**)

No final, deve ficar assim:

{% include image.html src='/files/2021/04/kick-off-16.jpg' %}

Feita essa configuração de repositórios no projeto de desenvolvimento (no meu caso, **home:kamarada:15.3:dev**), você pode fazer configuração semelhante no projeto principal (**home:kamarada:15.3**).

## Criando o padrão no OBS

No projeto de desenvolvimento (no meu caso, **home:kamarada:15.3:dev**), mude para a aba **Overview** (visão geral) e clique em **Create Package** (criar pacote):

{% include image.html src='/files/2021/04/kick-off-17.jpg' %}

Dê pelo menos um nome (**Name**) para o pacote (no meu caso, `patterns-kamarada-gnome`) e clique em **Create** (criar):

{% include image.html src='/files/2021/04/kick-off-18.jpg' %}

Criado o pacote do padrão no OBS, vamos trazê-lo para o computador para trabalhar nele localmente, de modo análogo ao que fazemos clonando um repositório do Git.

Crie uma cópia local ("faça _checkout_") do projeto do OBS:

```
$ osc checkout home:kamarada:15.3:dev
```

Entre na pasta do pacote:

```
$ cd home:kamarada:15.3:dev/patterns-kamarada-gnome
```

## Integrando o OBS com o Git

Agora vamos integrar o OBS com o Git, para que o código-fonte do pacote seja obtido do Git e compilado no OBS. Para instruções de como fazer isso, consulte um dos tutoriais a seguir, conforme o provedor de Git que você usa (GitLab ou GitHub):

- [Integrando Open Build Service com GitLab][obs-gitlab]
- [Integrando Open Build Service com GitHub][obs-github]

## Primeiro pacote pronto

Ao final, voltando para a visão geral do repositório de desenvolvimento, temos nosso primeiro pacote compilado com sucesso no OBS:

{% include image.html src='/files/2021/04/kick-off-19.jpg' %}

Tem mais alguns pacotes no Linux Kamarada que são compilados por essa dupla OBS + Git. Com o roteiro apresentado aqui, já vou dar o pontapé inicial nesses pacotes também.

Siga o Linux Kamarada nas redes sociais se quiser ser avisado de novos episódios dessa jornada.

[linux]:                    http://www.vivaolinux.com.br/linux/
[opensuse]:                 https://www.opensuse.org/
[15.1]:                     {% post_url pt/2020-02-24-kamarada-15.1-vem-com-tudo-que-voce-precisa-para-usar-o-linux-no-dia-a-dia %}
[descontinuada]:            {% post_url pt/2021-01-31-suporte-do-opensuse-leap-151-e-do-linux-kamarada-151-se-encerram-hoje %}
[15.2]:                     {% post_url pt/2020-09-11-linux-kamarada-15.2-venha-para-o-lado-verde-elegante-e-moderno-da-forca %}
[live]:                     {% post_url pt/2015-11-25-o-que-e-um-livecd-um-livedvd-um-liveusb %}
[git]:                      https://git-scm.com/
[kiwi]:                     https://osinside.github.io/kiwi/
[obs]:                      https://openbuildservice.org/

[github]:                   https://github.com/kamarada
[moving-to-gitlab]:         {% post_url pt/2021-03-10-linux-kamarada-esta-migrando-para-o-gitlab %}
[gitlab]:                   https://gitlab.com/kamarada
[bitbucket]:                https://bitbucket.org/
[obs-gitlab]:               {% post_url pt/2021-03-15-integrando-open-build-service-com-gitlab %}
[obs-github]:               {% post_url pt/2019-07-23-integrando-open-build-service-com-github %}
[buildoo]:                  https://build.opensuse.org
[leap-tumbleweed]:          {% post_url pt/2020-12-06-opensuse-leap-e-opensuse-tumbleweed-qual-e-a-diferenca %}
[osc]:                      https://linux.die.net/man/1/osc

[patterns]:                 {% post_url pt/2021-04-01-o-que-sao-padroes-e-como-instala-los-no-opensuse %}
[patterns-kamarada-gnome]:  https://software.opensuse.org/package/patterns-kamarada-gnome

[softwareoo]:               https://software.opensuse.org/
[rpm]:                      https://pt.wikipedia.org/wiki/RPM_(software)
[fedora-rpm-guide]:         https://web.archive.org/web/20220628164331/docs.fedoraproject.org/en-US/Fedora_Draft_Documentation/0.1/html//RPM_Guide/index.html
[rpm-packaging-guide]:      https://rpm-packaging-guide.github.io/
[opensuse-packaging]:       https://en.opensuse.org/openSUSE:Packaging_guidelines
[recommended]:              {% post_url pt/2021-01-12-o-que-sao-pacotes-recomendados-e-como-instala-los-no-opensuse %}
[ucode-amd]:                https://software.opensuse.org/package/ucode-amd
[ucode-intel]:              https://software.opensuse.org/package/ucode-intel

[patterns-base]:            https://build.opensuse.org/package/view_file/openSUSE:Leap:15.3/patterns-base/patterns-base.spec
[patterns-gnome]:           https://build.opensuse.org/package/view_file/openSUSE:Leap:15.3/patterns-gnome/patterns-gnome.spec
[patterns-yast]:            https://build.opensuse.org/package/view_file/openSUSE:Leap:15.2/patterns-yast/patterns-yast.spec

[kiwi-doc]:                 https://osinside.github.io/kiwi/working_with_images/build_in_buildservice.html
