---
date: '2022-12-02 07:50:00 GMT-3'
layout: post
published: true
title: 'Copa do Mundo 2022: sincronize a tabela dos jogos com o Calendário do GNOME'
image: /files/2022/12/qatar2022-gnome-calendar-pt.jpg
nickname: 'qatar2022-gnome-calendar'
---

Já imaginou conferir os jogos da [Copa do Mundo FIFA 2022][fifa] e seus resultados diretamente do seu _desktop_ [Linux]? Pois saiba que você não precisa ficar só na imaginação, porque isso é possível: como fazê-lo é o que você verá aqui hoje.

<!--more-->

{% include image.html src='/files/2022/12/qatar2022-gnome-calendar-pt.jpg' %}

O jornalista esportivo [Victor Gama] criou um [calendário do Google Agenda][calendar-google-1] com os horários de todos os jogos e tem atualizado esse calendário com os placares conforme os jogos acontecem. Ele tornou esse calendário público, de modo que outras pessoas, mesmo as que não usam os serviços do [Google], conseguem acompanhá-lo e o disponibilizou em seu [perfil pessoal no Twitter][twitter]. Uma bela iniciativa!

O [Google Agenda][calendar-google-2] exporta esse calendário no [formato ICS][ics], que pode ser sincronizado por aplicativos como o **[Calendário]** do [GNOME], presente no [Linux Kamarada].

Para abrir o Calendário do GNOME, clique em **Atividades**, no canto superior esquerdo da tela, comece a digitar `calendario` e clique no ícone do aplicativo:

{% include image.html src='/files/2022/12/qatar2022-gnome-calendar-pt-01.jpg' %}

Clique no ícone **Gerenciar seus calendários** e, em seguida, na opção **Gerenciar calendários**:

{% include image.html src='/files/2022/12/qatar2022-gnome-calendar-pt-02.jpg' %}

Clique em **Adicionar calendário**:

{% include image.html src='/files/2022/12/qatar2022-gnome-calendar-pt-03.jpg' %}

Copie o _link_ a seguir. Para isso, clique com o botão direito nele e, no menu de contexto que aparecer, clique em **Copiar link** (ou a opção com nome parecido disponibilizada pelo seu navegador):

- [_Link_ para o arquivo ICS][ics-file]

Cole-o na caixa de diálogo **Novo calendário**, no campo de texto abaixo de **Importar um calendário**, e clique no botão **Adicionar**:

{% include image.html src='/files/2022/12/qatar2022-gnome-calendar-pt-04.jpg' %}

De volta à tela anterior, note que o calendário passa a aparecer na lista de calendários, e os jogos já são exibidos ao fundo:

{% include image.html src='/files/2022/12/qatar2022-gnome-calendar-pt-05.jpg' %}

Clicando no nome do calendário, você pode dar um novo nome e cor a ele:

{% include image.html src='/files/2022/12/qatar2022-gnome-calendar-pt-06.jpg' %}

Quando terminar, feche essa caixa de diálogo para voltar para a tela principal do **Calendário**.

Agora você pode facilmente exibir ou ocultar a tabela de jogos clicando no ícone **Gerenciar seus calendários**:

{% include image.html src='/files/2022/12/qatar2022-gnome-calendar-pt-07.jpg' %}

Passe o cursor sobre um jogo, ou clique nele, para ver mais informações:

{% include image.html src='/files/2022/12/qatar2022-gnome-calendar-pt-08.jpg' %}

Da forma como fizemos (copiando e colando o _link_ no aplicativo **Calendário**), o aplicativo verifica de tempos em tempos se a tabela de jogos foi atualizada e sincroniza as atualizações.

Uma alternativa seria baixar o arquivo ICS pro computador e importá-lo para o aplicativo, mas aí as atualizações feitas pelo jornalista não seriam sincronizadas.

Pronto! Com isso, você poderá conferir os horários e placares de todos os 65 jogos do maior torneio de seleções de futebol do planeta fácil e diretamente pelo aplicativo **Calendário** no seu computador com Linux.

Espero que tenha gostado da dica e aproveite. Se tiver qualquer dúvida ou sugestão, não deixe de comentar!

[fifa]:                 https://www.fifa.com/fifaplus/pt/tournaments/mens/worldcup/qatar2022
[Linux]:                http://www.vivaolinux.com.br/linux
[Victor Gama]:          https://twitter.com/victorgaama
[calendar-google-1]:    https://calendar.google.com/calendar/u/0/r?cid=MG5nM2lyMDJpZ3N0MmhkazBkMzNmcTFrdTRAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ
[Google]:               https://www.google.com
[twitter]:              https://twitter.com/victorgaama/status/1536802417538482177
[calendar-google-2]:    https://calendar.google.com
[ics]:                  https://en.wikipedia.org/wiki/ICalendar
[Calendário]:           https://apps.gnome.org/pt-BR/app/org.gnome.Calendar
[GNOME]:                https://br.gnome.org
[Linux Kamarada]:       {% post_url pt/2021-12-30-linux-kamarada-15-3-ano-novo-distribuicao-nova %}
[ics-file]:             https://calendar.google.com/calendar/ical/0ng3ir02igst2hdk0d33fq1ku4%40group.calendar.google.com/public/basic.ics
