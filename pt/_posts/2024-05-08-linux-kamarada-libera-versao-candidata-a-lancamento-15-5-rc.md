---
date: '2024-05-08 23:50:00 GMT-3'
image: '/files/2024/05/15.5-rc.jpg'
layout: post
published: true
nickname: 'kamarada-15.5-rc'
title: 'Linux Kamarada libera versão candidata a lançamento (15.5 RC)'
---

{% include image.html src='/files/2024/05/15.5-rc.jpg' %}

O Projeto Linux Kamarada anuncia o lançamento da versão 15.5 RC da [distribuição Linux][linux] de mesmo nome, baseada no [openSUSE Leap 15.5][leap-15.5]. Ela já está disponível para [download].

<!--more-->

A página [Download] foi atualizada e agora oferece principalmente duas versões para _download_:

- a versão [15.4 Final][kamarada-15.4], que você pode instalar no computador de casa ou do trabalho; e
- a versão 15.5 RC, que você pode testar, se quiser ajudar no desenvolvimento.

Lembre-se que a forma mais fácil de testar o [Linux] é usando uma máquina virtual do [VirtualBox]. Para mais informações sobre como fazer isso, leia:

- [VirtualBox: a forma mais fácil de conhecer o Linux sem precisar instalá-lo][VirtualBox]

Mas, se você quiser testar o Linux Kamarada no seu próprio computador, a forma mais fácil de fazer isso é usando um _pendrive_ inicializável que você pode criar com o [Ventoy]:

- [Ventoy: crie um pendrive multiboot simplesmente copiando imagens ISO para ele][Ventoy]

Se você deseja instalar o Linux Kamarada no computador para usar no dia-a-dia, o recomendado é que você instale a versão 15.4 Final, que já está pronta, e depois, quando a versão 15.5 estiver pronta, atualize. Para mais informações sobre a versão 15.4 Final, leia:

- [Linux Kamarada 15.4: mais funcional, bonito, polido e elegante do que nunca][kamarada-15.4]

## O que é versão candidata a lançamento?

Uma versão **candidata a lançamento** (do inglês [_release candidate_][rc], por isso abreviada **RC**) é uma versão de um _software_ que está praticamente pronto para ser lançado no mercado. Essa versão pode se tornar a versão final (estável) do _software_, a não ser que algum defeito (_bug_) sério seja percebido a tempo de ser corrigido antes do lançamento final. Nesse estágio do desenvolvimento, todas as funcionalidades inicialmente planejadas já estão presentes e nenhum recurso novo é adicionado.

Uma versão RC pode ser considerada um tipo de versão [beta]. Portanto, _bugs_ são esperados. Se você encontrar um _bug_, por favor, me avise por um dos canais listados na página [Ajuda].

Claro, _bugs_ podem ser encontrados e corrigidos a qualquer momento, mas quanto antes, melhor!

## Novidades

Em comparação com a versão [15.5 Beta], a versão 15.5 RC apresenta pacotes atualizados. Para além disso, não houve grandes mudanças.

## Atualizando para o 15.5 RC

Se você já usa o Linux Kamarada 15.4 e confia na sua experiência como usuário, pode querer atualizar para o Linux Kamarada 15.5 RC para ajudar a testar o processo de atualização. Confira como fazer isso no tutorial:

- [Linux Kamarada 15.4 e openSUSE Leap 15.4: como atualizar para a versão 15.5][upgrade-howto]

Se você já usa o Linux Kamarada 15.5 Beta e quer atualizar para o 15.5 RC, basta obter as atualizações para os pacotes, como de costume:

- [Como obter atualizações para o Linux openSUSE][update-howto]

## Ficha técnica

Para que seja possível comparar esta versão com a anterior e as futuras, segue um resumo do _software_ contido no Linux Kamarada 15.5 RC Build 5.69:

- _kernel_ Linux 5.14.21
- servidor gráfico X.Org 21.1.4 (com Wayland 1.21.0 habilitado por padrão)
- área de trabalho GNOME 41.9 acompanhada de seus principais aplicativos (_core apps_), como Arquivos (antigo Nautilus), Calculadora, Terminal, Editor de texto (gedit), Captura de tela e outros
- suíte de aplicativos de escritório LibreOffice 24.2.1.2
- navegador Mozilla Firefox 115.10.0 ESR (navegador padrão)
- navegador Chromium 122 (navegador alternativo disponível)
- reprodutor multimídia VLC 3.0.20
- cliente de _e-mail_ Evolution 3.42.4
- centro de controle do YaST
- Brasero 3.12.3
- CUPS 2.2.7
- Drawing 1.0.1
- Firewalld 0.9.3
- GParted 1.4.0
- HPLIP 3.21.10
- Java (OpenJDK) 11.0.23
- KeePassXC 2.7.8
- Linphone 5.0.5
- PDFsam Basic 5.2.3
- Pidgin 2.14.8
- Python 3.6.15
- Samba 4.17.12
- Tor 0.4.8.10
- Transmission 3.00
- Vim 9.1.0330
- Wine 8.0
- instalador Calamares 3.2.62
- Flatpak 1.14.5
- jogos: AisleRiot (Paciência), Mahjongg, Minas (Campo minado), Nibbles (Cobras), Quadrapassel (Tetris), Reversi, Sudoku, Xadrez

Essa lista não é exaustiva, mas já dá uma noção do que se encontra na distribuição.

[linux]:            https://www.vivaolinux.com.br/linux/
[leap-15.5]:        {% post_url pt/2023-06-07-lancamento-do-opensuse-leap-15-5-amadurece-a-distribuicao-e-estabelece-transicao-tecnologica %}
[download]:         /pt/download
[kamarada-15.4]:    {% post_url pt/2023-02-27-linux-kamarada-15-4-mais-funcional-bonito-polido-e-elegante-do-que-nunca %}
[VirtualBox]:       {% post_url pt/2019-10-08-virtualbox-a-forma-mais-facil-de-conhecer-o-linux-sem-precisar-instala-lo %}
[Ventoy]:           {% post_url pt/2020-07-29-ventoy-crie-pendrives-multiboot-simplesmente-copiando-imagens-iso-para-ele %}
[rc]:               https://pt.wikipedia.org/wiki/Ciclo_de_vida_de_libera%C3%A7%C3%A3o_de_software#Release_candidate
[beta]:             https://pt.wikipedia.org/wiki/Ciclo_de_vida_de_libera%C3%A7%C3%A3o_de_software#Beta
[Ajuda]:            /pt/ajuda
[15.5 Beta]:        {% post_url pt/2023-11-01-linux-kamarada-15-5-beta-ajude-a-testar-a-melhor-versao-da-distribuicao %}
[upgrade-howto]:    {% post_url pt/2023-11-26-linux-kamarada-15-4-e-opensuse-leap-15-4-como-atualizar-para-a-versao-15-5 %}
[update-howto]:     {% post_url pt/2019-05-14-como-obter-atualizacoes-para-o-linux-opensuse %}
