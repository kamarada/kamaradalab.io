---
date: '2023-11-26 11:30:00 GMT-3'
layout: post
published: true
title: 'Linux Kamarada 15.4 e openSUSE Leap 15.4: como atualizar para a versão 15.5'
image: /files/2023/11/upgrade-to-15.5.jpg
nickname: 'upgrade-to-15.5'
---

{% include image.html src="/files/2023/11/upgrade-to-15.5.jpg" %}

Veja nesse tutorial como atualizar do [openSUSE Leap 15.4][leap-15.4] para o [openSUSE Leap 15.5][leap-15.5]. Como o Linux Kamarada é uma distribuição [Linux] baseada no [openSUSE Leap], esse tutorial também mostrará como os usuários do [Linux Kamarada 15.4][kamarada-15.4] podem atualizar suas instalações para o [Linux Kamarada 15.5][kamarada-15.5].&nbsp;

Os mesmos números de versão indicam o alinhamento entre as distribuições e o procedimento para atualizá-las é bem parecido, tanto que ambas cabem no mesmo tutorial.

Hoje, o openSUSE Leap está na versão [15.5][leap-15.5], lançada em [junho][leap-15.5]. A versão anterior, a [15.4][leap-15.4], foi lançada em [junho do ano passado][leap-15.4] e deve ter seu suporte encerrado com a virada do ano. Portanto, é recomendado a usuários do openSUSE Leap 15.4 atualizarem para o openSUSE Leap 15.5.

O Linux Kamarada está na versão [15.4][kamarada-15.4], lançada em [fevereiro][kamarada-15.4]. O Linux Kamarada 15.5 será lançado em breve. Por enquanto, temos a versão [15.5 Beta][kamarada-15.5-beta], que já apresenta uma boa estabilidade. Nos meus testes, a atualização do Linux Kamarada 15.4 Final para o 15.5 Beta ocorreu sem grandes problemas.

Como o Linux Kamarada 15.5 ainda não foi finalizado, não recomendo a atualização para todos os usuários. Mas, se você se sente "corajoso" e quer ajudar no desenvolvimento, testando a atualização e reportando quaisquer problemas, por favor, sinta-se à vontade.

{% capture final %}

O Linux Kamarada 15.5 Final foi lançado. A atualização é recomendada para todos os usuários do Linux Kamarada 15.4.

Para mais informações sobre a nova versão, leia:

- [Linux Kamarada 15.5: mais alinhado com o openSUSE Leap e com outras distribuições]({% post_url pt/2024-05-27-linux-kamarada-15-5-mais-alinhado-com-o-opensuse-leap-e-com-outras-distribuicoes %})

{% endcapture %}

{% include update.html date="27/05/2024" message=final %}

Atualizar (fazer o _upgrade_) de uma versão para outra no openSUSE Leap é um processo fácil e tranquilo. Eu sigo a "receita de bolo" que aprendi na [_wiki_ do openSUSE][system-upgrade]. É praticamente a mesma "receita" desde 2012, quando comecei a usar o [openSUSE]. Como o Linux Kamarada é baseado no openSUSE Leap, sua atualização segue a mesma "receita", a única diferença é a configuração de um repositório a mais, que é o do Linux Kamarada.

A seguir, você verá como atualizar o openSUSE Leap e o Linux Kamarada da versão 15.4 para a 15.5.

**Observação:** para não ficar repetitivo, o que eu falo sobre o openSUSE Leap se aplica às duas distribuições, o que eu falo sobre o Linux Kamarada é específico deste.

## Algumas recomendações

A atualização da distribuição (_upgrade_) no openSUSE Leap é um procedimento seguro, que deve funcionar para a maioria dos casos, mas que requer alguns cuidados. É sempre bom relembrar as recomendações a seguir.

**Você deve estar usando a versão imediatamente anterior do openSUSE Leap.** Isso quer dizer que se você pretende atualizar para a versão 15.5, deve estar usando agora a versão 15.4. Saltos de versões não são suportados. Podem funcionar, mas não há garantia. Portanto, se agora você está usando a versão 15.3, por exemplo, é melhor que primeiro [atualize para a versão 15.4][upgrade-to-15.4], para então atualizar para a versão 15.5.

**Sua instalação do openSUSE Leap deve estar atualizada** com as versões mais recentes dos pacotes lançadas até o momento. Observe que há dois tipos de atualização: neste *post*, você vai ver como atualizar a distribuição (*upgrade*), mas antes você deve atualizar os pacotes (*update*). Para mais informações sobre os dois tipos de atualizações e como atualizar os pacotes, consulte o *post*:

- [Como obter atualizações para o Linux openSUSE][howto-update]

**Você deve se informar sobre a versão do openSUSE Leap que vai instalar.** Consulte as [notas de lançamento][release-notes], que descrevem o que muda na nova versão e listam alguns *bugs* conhecidos, como preveni-los e/ou solucioná-los.

**Você deve fazer uma cópia de segurança (*backup*) de arquivos importantes.** apesar de a atualização do openSUSE ser segura (fazendo lembrar, para quem conhece, a atualização do [Debian]), não quer dizer que seja perfeita. Ganhar na loteria é difícil, mas uma hora alguém ganha. Algo dar errado na atualização é difícil, especialmente se você toma todos os cuidados, mas pode acontecer. Eu, particularmente, já atualizei algumas vezes sem problema. Nunca é demais fazer uma cópia dos seus arquivos pessoais por precaução, especialmente se a pasta `/home` não está em uma partição reservada. Se o computador não pode ficar parado por muito tempo (um servidor, por exemplo), considere fazer uma cópia de todo o sistema, a fim de restaurá-la imediatamente caso algo não funcione como esperado.

**Você não deve usar repositórios não oficiais (de terceiros) durante a atualização.** Antes de começar a atualização (*upgrade*), removeremos os [repositórios de terceiros][third-party-repos]. É possível que isso ocasione a remoção também de programas de terceiros. Você pode devolvê-los depois. Isso não quer dizer que não seja possível atualizar com esses repositórios presentes, mas usar apenas os [repositórios oficiais][official-repos] é mais garantido. A base do sistema será atualizada para uma nova base estável, sólida, confiável. Depois, sobre essa base, você poderá instalar o que quiser. Faça diferente dessa recomendação apenas se souber o que está fazendo.

Recomendo também que você leia toda essa página antes de começar de fato a agir. Para organizá-la melhor, vou dividi-la em 6 etapas. Conhecendo todo o processo, você terá condição de agendá-lo melhor. Você pode até fazer outras coisas usando o computador enquanto segue o passo a passo, mas depois que começar a atualização (*upgrade*), só poderá usar o computador quando ela for concluída.

## 1) Atualização dos pacotes (*update*)

Certifique-se de que sua instalação do openSUSE Leap está atualizada. Para isso, execute o comando `zypper up` como usuário _root_ (administrador). Ele deve informar que não há o que fazer:

{% include image.html src="/files/2022/12/upgrade-to-15.4-02-pt.jpg" %}

Para mais informações sobre atualização de pacotes (_update_), consulte o _post_:

- [Como obter atualizações para o Linux openSUSE][howto-update]

## 2) *Backup* dos repositórios atuais

Essa etapa, na verdade, é opcional.

Talvez você queira fazer *backup* da sua lista atual de repositórios para devolvê-la depois da atualização. O openSUSE Leap guarda as configurações de repositórios na pasta `/etc/zypp/repos.d`, sendo um arquivo de texto para cada repositório.

Vamos copiar a pasta `repos.d`, em `/etc/zypp`, para outra no mesmo lugar, chamada `repos.d.antigos`. Para isso, usaremos a interface de linha de comando.

Se não já o fez, alterne do seu usuário para o usuário *root* executando o comando:

```
$ su
```

Forneça a senha do usuário *root* para continuar.

Se você já fez alguma atualização de versão seguindo alguma [edição anterior desse mesmo tutorial][upgrade-to-15.4], talvez ainda tenha o *backup* antigo guardado. Se esse é o seu caso, execute o comando a seguir para excluir a pasta `repos.d.antigos`:

```
# rm -rf /etc/zypp/repos.d.antigos
```

Então, ordene a cópia:

```
# cp -Rv /etc/zypp/repos.d /etc/zypp/repos.d.antigos
```

{% include image.html src="/files/2022/12/upgrade-to-15.4-03-pt.jpg" %}

Se você ainda não fez *backup* dos seus arquivos pessoais e/ou do seu sistema, considere fazê-lo agora.

## 3) Faxina dos repositórios

Como expliquei antes, durante a atualização usaremos apenas os repositórios oficiais. Mais especificamente, apenas dois deles, no caso do openSUSE Leap:

- **Main Repository** (repositório principal): contém apenas [*softwares* de código aberto][oss] (do inglês *open source sofware*, OSS).

URL: `http://download.opensuse.org/distribution/leap/$releasever/repo/oss/`

URL calculada para o 15.4: [http://download.opensuse.org/distribution/leap/15.4/repo/oss/](http://download.opensuse.org/distribution/leap/15.4/repo/oss/)

- **Main Update Repository** (repositório de atualização principal): contém atualizações (*updates*) oficiais para os pacotes do repositório principal.

URL: `http://download.opensuse.org/update/leap/$releasever/oss/`

URL calculada para o 15.4: [http://download.opensuse.org/update/leap/15.4/oss/](http://download.opensuse.org/update/leap/15.4/oss/)

Note que o sistema automaticamente substitui a variável `$releasever` pela versão da distribuição sendo usada no momento.

No caso do Linux Kamarada, além dos repositórios acima, usaremos também o repositório oficial do **Linux Kamarada**.

URL: `https://packages.linuxkamarada.com/$releasever/openSUSE_Leap_$releasever/`

URL calculada para o 15.4: [https://packages.linuxkamarada.com/15.4/openSUSE_Leap_15.4/](https://packages.linuxkamarada.com/15.4/openSUSE_Leap_15.4/)

Vamos fazer uma faxina nos repositórios, excluindo quaisquer outros repositórios configurados (vamos excluir inclusive quaisquer repositórios de terceiros).

Abra o [Centro de Controle do YaST][yast]. Para isso, clique em **Mostrar aplicativos**, no canto inferior esquerdo da tela, e clique no ícone do **YaST**, no final da lista de aplicativos:

{% include image.html src="/files/2023/11/upgrade-to-15.5-01-pt.jpg" %}

Será solicitada a você a senha do usuário *root*. Digite-a e clique em **Continuar**:

{% include image.html src='/files/2023/11/openSUSE-repos-Leap-02-pt.jpg' %}

Dentro da categoria **Software**, a primeira, clique no item **Repositórios de software**:

{% include image.html src="/files/2022/12/upgrade-to-15.4-06-pt.jpg" %}

Você será apresentado à lista de repositórios do seu sistema:

{% include image.html src="/files/2023/11/upgrade-to-15.5-02-pt.jpg" %}

Para cada repositório que não seja um dos listados acima, selecione o repositório na lista e clique em **Remover**.

Aparecerá uma mensagem de confirmação. Clique em **Sim**:

{% include image.html src="/files/2023/11/upgrade-to-15.5-03-pt.jpg" %}

Faça isso com todos os repositórios até que só sobrem os repositórios listados acima:

{% include image.html src="/files/2023/11/upgrade-to-15.5-04-pt.jpg" %}

Esses repositórios podem estar com nomes diferentes no seu computador. Se esse for o caso, você pode se orientar pela [URL] em vez do nome. Se não encontrá-los, não tem problema: adicione-os, informando as URLs acima.

## 4) Repositórios da nova versão

Nessa etapa, em edições anteriores desse tutorial, editávamos os repositórios um a um, substituindo a versão anterior da distribuição (15.4) pela versão nova (15.5). Agora que temos a variável `$releasever`, isso não é mais necessário.

Apenas selecione cada um dos seus repositórios e confira se seu **URL Bruto** contém a variável `$releasever`:

{% include image.html src="/files/2023/11/upgrade-to-15.5-05-pt.jpg" %}

Caso a versão da distribuição (no momento, `15.4`) apareça em algum trecho do **URL Bruto**, clique em **Editar**. Na tela que aparece, substitua essa versão por `$releasever` e clique em **OK**:

{% include image.html src="/files/2023/11/upgrade-to-15.5-06-pt.jpg" %}

No caso do **Linux Kamarada**, observe que a variável `$releasever` aparece duas vezes:

{% include image.html src="/files/2023/11/upgrade-to-15.5-07-pt.jpg" %}

Quando terminar, clique em **OK** para gravar as alterações e sair do módulo **Repositórios de software**. Você também pode fechar o YaST.

## 5) *Download* dos pacotes

Já podemos começar a baixar os pacotes da nova versão do openSUSE Leap.

De volta à interface de linha de comando, baixe a lista de pacotes dos novos repositórios (note que vamos usar a opção `--releasever=15.5` para indicar o uso da versão nova):

```
# zypper --releasever=15.5 ref
```

Depois, execute o comando:

```
# zypper --releasever=15.5 dup --download-only --allow-vendor-change
```

(*distribution upgrade*, atualização da distribuição, *download only*, apenas baixar, *allow vendor change*, permitir [mudanças de fornecedor][vendor-change])

{% include image.html src="/files/2023/11/upgrade-to-15.5-08-pt.jpg" %}

Usamos a opção `--allow-vendor-change` porque o openSUSE Leap compartilha pacotes RPM com o [SUSE Linux Enterprise][sle] (não apenas o código-fonte, como nas versões anteriores, mas até mesmo os pacotes binários, já compilados). Por isso, o gerenciador de pacotes **[zypper]** poderia acusar muitas mudanças de fornecedor durante a atualização. A opção `--allow-vendor-change` faz com que o **zypper** aceite essas mudanças de antemão.

O gerenciador de pacotes **zypper** passa um tempo "pensando". Depois, mostra as alterações que precisa fazer no sistema para atualizar o openSUSE Leap para a próxima versão (quais pacotes serão atualizados, quais serão removidos, quais mudarão de fornecedor, etc) e pergunta se você deseja continuar:

{% include image.html src="/files/2023/11/upgrade-to-15.5-09-pt.jpg" %}

Note que essa lista de ações é semelhante à produzida pelo comando `zypper up`, apresentado no [*post* sobre atualização de pacotes][howto-update]. No entanto, como agora estamos fazendo uma atualização de distribuição, a lista do que precisa ser feito é bem maior.

Confira essa lista com cuidado. Você pode ir para cima ou para baixo usando a barra de rolagem à direita da tela ou a roda (*scroll*) do *mouse*, se utiliza o **[Terminal]**, ou as combinações de teclas **Shift + Page Up** ou **Shift + Page Down**, se está em uma interface puramente textual (elas também funcionam no **Terminal**).

A opção padrão é continuar (**s**). Se você concorda, pode apenas teclar **Enter** e o _download_ dos novos pacotes começará. Enquanto aguarda, você pode usar seu computador normalmente. A atualização propriamente dita ainda não começou.

Observe que o **zypper** apenas baixa os pacotes, ele não inicia a atualização:

{% include image.html src="/files/2023/11/upgrade-to-15.5-10-pt.jpg" %}

Isso porque ordenamos a ele por ora apenas baixar os pacotes (opção `--download-only`).

Termine o que está fazendo e salve os arquivos abertos para iniciarmos a atualização da distribuição. Note que ela pode levar alguns (vários) minutos e você só poderá voltar a usar o computador quando ela for concluída.

## 6) Atualização da distribuição (*upgrade*)

Agora que já baixamos os pacotes necessários para atualizar o openSUSE Leap da versão 15.4 para a 15.5, vamos sair da interface gráfica e usar a linha de comando para realizar a instalação. Aqui, você não poderá usar o **Terminal**.

Isso é necessário porque inclusive a própria interface gráfica será atualizada. Se estivermos usando a interface gráfica durante a atualização, pode ser que o sistema trave no meio da processo e as consequências disso são imprevisíveis.

Considere abrir essa página em outro computador, em um *smartphone* ou *tablet*, imprimi-la ou anotar o que vai fazer.

Se o sistema que você vai atualizar está em um *notebook*, certifique-se de que ele esteja com a bateria completamente carregada e conectado à fonte de alimentação. Não desconecte a bateria ou a fonte durante a atualização. Vamos prevenir qualquer possibilidade de problema.

Encerre sua sessão (*logout*). Para isso, clique no **menu do sistema**, no canto superior direito da tela, clique no seu nome de usuário e escolha a opção **Encerrar sessão**:

{% include image.html src="/files/2023/11/upgrade-to-15.5-11-pt.jpg" %}

Você será apresentado à tela de *login*, onde você poderia informar seu nome de usuário e senha caso quisesse iniciar uma nova sessão na interface gráfica:

{% include image.html src="/files/2023/11/upgrade-to-15.5-12-pt.jpg" %}

Mas não é isso que queremos. Aperte a combinação de teclas **Ctrl + Alt + F1** para alternar para uma interface puramente textual:

{% include image.html src="/files/2023/11/upgrade-to-15.5-13.png" %}

Caso isso seja novidade para você, saiba que o Linux disponibiliza seis [consoles] (terminais) além da interface gráfica. Você pode usar as teclas de **F1** a **F6** na mesma combinação (**Ctrl + Alt + F1**, **Ctrl + Alt + F2** e assim por diante) para alternar entre os consoles, assim como pressionar **Ctrl + Alt + F7** para retornar à interface gráfica.

Vamos permanecer no primeiro console.

Entre com o usuário *root*. Para isso, digite `root` e tecle **Enter**. Depois, digite a senha do usuário *root* e tecle **Enter**.

Vamos alternar do nível de execução (_[runlevel]_) 5, nível padrão, no qual o sistema nos provê interface gráfica, para o nível de execução 3, no qual temos apenas a interface de linha de comando e conexão de rede.

Para [alterar o nível de execução][switch-runlevel] para 3, execute o comando:

```
# init 3
```

Finalmente, vamos realizar a atualização de distribuição propriamente dita. Para isso, execute o comando (se atente, também aqui, às mesmas opções que usamos antes):

```
# zypper --no-refresh --releasever=15.5 dup --allow-vendor-change
```

{% include image.html src="/files/2023/11/upgrade-to-15.5-14.png" %}

A opção `--no-refresh` faz com que o **zypper** não atualize a lista de pacotes dos repositórios. Com isso, garantimos que ele não tente baixar mais algum pacote além dos que nós já baixamos. Isso pode ser útil especialmente em *notebooks*, que podem perder a conexão com a rede Wi-Fi ao sair da interface gráfica.

Como antes, o **zypper** vai processar a atualização e mostrar o que vai fazer (se antes ele fez perguntas sobre as mudanças, e agora ele repetir as mesmas perguntas, responda da mesma forma):

{% include image.html src="/files/2023/11/upgrade-to-15.5-15-pt.png" %}

Note que todos os pacotes que ele precisa já foram baixados: ao final ele mostra "Tamanho total do download: 0 B. Já em cache: 2,14 GiB."

Apenas tecle **Enter** para que a atualização comece.

O **zypper** pula o _download_ dos pacotes e já vai direto para a instalação:

{% include image.html src="/files/2023/11/upgrade-to-15.5-16-pt.png" %}

A atualização da distribuição pode demorar alguns (vários) minutos.

Quando ela terminar, o **zypper** vai sugerir reiniciar. É o que vamos fazer executando:

```
# reboot
```

{% include image.html src="/files/2023/11/upgrade-to-15.5-17-pt.png" %}

## Quase pronto

Se você conseguiu fazer tudo até aqui, seu computador já está com o Linux Kamarada 15.5 (ou com o openSUSE Leap 15.5) e ele já está quase pronto para uso:

{% include image.html src="/files/2023/11/upgrade-to-15.5-18-pt.jpg" %}

Verifique se está tudo no lugar.

Por que eu disse que o sistema está quase pronto? Porque tem uma novidade da versão 15.5 que é o pacote **[openSUSE-repos-Leap]**, que facilita a configuração dos repositórios padrão do openSUSE. É recomendado (porém não necessário) que você instale esse pacote. Para mais informações, leia:

- [Instale o novo pacote openSUSE-repos-Leap][openSUSE-repos-Leap]

Agora você pode devolver os repositórios que excluiu, lembrando de substituir a versão do openSUSE Leap, onde aparecer, pela variável `$releasever`. Pela interface de linha de comando, fazer isso é fácil:

```
# sed -i 's/15.4/$releasever/g' /etc/zypp/repos.d.antigos/*
# mv /etc/zypp/repos.d.antigos/* /etc/zypp/repos.d/
```

Feito isso, você pode excluir o *backup* da sua antiga lista de repositórios:

```
# rm -rf /etc/zypp/repos.d.antigos/
```

Você também pode tentar instalar qualquer programa que por ventura tenha sido removido durante a atualização.

## Conclusão

Se lembre de regularmente [verificar se há atualizações (*updates*)][howto-update] para sua nova distribuição. Note que no uso cotidiano do **zypper** você não precisa usar a opção `--releasever`.

Se você encontrou alguma dificuldade durante a atualização ou possui alguma dúvida, não deixe de comentar!

Até a próxima!

[leap-15.4]:            {% post_url pt/2022-06-08-leap-15-4-oferece-novos-recursos-com-a-estabilidade-de-sempre %}
[leap-15.5]:            {% post_url pt/2023-06-07-lancamento-do-opensuse-leap-15-5-amadurece-a-distribuicao-e-estabelece-transicao-tecnologica %}
[Linux]:                https://www.vivaolinux.com.br/linux/
[openSUSE Leap]:        {% post_url pt/2020-12-06-opensuse-leap-e-opensuse-tumbleweed-qual-e-a-diferenca %}
[kamarada-15.4]:        {% post_url pt/2023-02-27-linux-kamarada-15-4-mais-funcional-bonito-polido-e-elegante-do-que-nunca %}
[kamarada-15.5]:        {% post_url pt/2023-11-01-linux-kamarada-15-5-beta-ajude-a-testar-a-melhor-versao-da-distribuicao %}
[kamarada-15.5-beta]:   {% post_url pt/2023-11-01-linux-kamarada-15-5-beta-ajude-a-testar-a-melhor-versao-da-distribuicao %}
[system-upgrade]:       https://en.opensuse.org/SDB:System_upgrade
[openSUSE]:             https://www.opensuse.org/
[upgrade-to-15.4]:      {% post_url pt/2022-12-28-linux-kamarada-e-opensuse-leap-como-atualizar-da-versao-153-para-a-154 %}
[howto-update]:         {% post_url pt/2019-05-14-como-obter-atualizacoes-para-o-linux-opensuse %}
[release-notes]:        https://doc.opensuse.org/release-notes/x86_64/openSUSE/Leap/15.5/RELEASE-NOTES.pt_BR.html
[Debian]:               https://www.debian.org/
[third-party-repos]:    https://en.opensuse.org/Additional_package_repositories
[official-repos]:       https://en.opensuse.org/Package_repositories#Official_Repositories
[oss]:                  https://pt.wikipedia.org/wiki/Software_de_c%C3%B3digo_aberto
[yast]:                 http://yast.opensuse.org/
[url]:                  https://pt.wikipedia.org/wiki/URL
[vendor-change]:        https://en.opensuse.org/SDB:Vendor_change_update
[sle]:                  https://www.suse.com/
[zypper]:               https://pt.opensuse.org/Portal:Zypper
[Terminal]:             https://wiki.gnome.org/Apps/Terminal
[consoles]:             http://www.hardware.com.br/livros/linux/usando-terminal.html
[runlevel]:             https://mateusmuller.me/2017/09/10/dica-de-lpic-1-o-que-e-e-como-funciona-o-runlevel-no-linux/
[switch-runlevel]:      https://en.opensuse.org/SDB:Switch_runlevel
[openSUSE-repos-Leap]:  {% post_url pt/2023-11-12-instale-o-novo-pacote-opensuse-repos-leap %}
