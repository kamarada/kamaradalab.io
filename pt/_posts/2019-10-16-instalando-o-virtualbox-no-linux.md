---
date: '2019-10-16 03:00:00 GMT-3'
layout: post
published: true
title: 'Instalando o VirtualBox no Linux'
image: '/files/2023/03/virtualbox-39-pt.jpg'
nickname: 'virtualbox-linux'
excerpt: 'Nessa parte 2 de uma trilogia de posts sobre o VirtualBox, você verá como instalá-lo no Linux. As distribuições mais populares receberão uma atenção especial, e ao final serão mostradas instruções genéricas, que devem funcionar para instalações Linux em geral.'
---

{% capture atualizacao_2023 %}
O texto foi atualizado depois do lançamento do [Linux Kamarada 15.4][kamarada-15.4]. A parte 2 teve muitas alterações. Inclusive as imagens foram atualizadas.

[kamarada-15.4]: {% post_url pt/2023-02-27-linux-kamarada-15-4-mais-funcional-bonito-polido-e-elegante-do-que-nunca %}
{% endcapture %}
{% include update.html date="09/05/2023" message=atualizacao_2023 %}

Nessa parte 2 de uma trilogia de _posts_ sobre o [VirtualBox], você verá como instalá-lo no [Linux]. As distribuições mais populares receberão uma atenção especial, e ao final serão mostradas instruções genéricas, que devem funcionar para instalações Linux em geral.

Na [parte 1][VirtualBox], vimos o que é virtualização, o que é o VirtualBox, como instalar o VirtualBox no [Windows], como criar uma máquina virtual e como usá-la para experimentar o Linux.

Caso você tenha caído aqui de paraquedas, comece sua leitura pela parte 1:

- [VirtualBox: a forma mais fácil de conhecer o Linux sem precisar instalá-lo][VirtualBox]

Agora, veremos como instalar o VirtualBox no Linux.

Normalmente, eu falaria aqui da distribuição [Linux Kamarada][kamarada] (que equivale a falar da distribuição [openSUSE Leap][leap-vs-tumbleweed], que é sua base). Mas, como o objetivo é mostrar como você pode experimentar o Linux sem sair do sistema que você já usa, falaremos hoje de outras distribuições Linux também. Faz sentido, se você parar pra pensar: pode ser que você já use Linux, mas queira experimentar outra distribuição, como o Linux Kamarada, por exemplo.

## Instalando o VirtualBox no Ubuntu

Vejamos como instalar o VirtualBox no [Ubuntu]. Provavelmente as instruções a seguir também funcionam em distribuições derivadas do Ubuntu, a exemplo de: [Linux Mint], [Pop!_OS], [elementary OS] e [Zorin OS].

A distribuição Ubuntu disponibiliza o VirtualBox no repositório oficial **multiverse**. Obtê-lo desse repositório é a forma mais fácil de instalar o VirtualBox no Ubuntu e derivados. A versão disponibilizada nesse repositório é sempre a mais recente ou próxima dela.

Comece habilitando o repositório multiverse. O modo mais fácil de fazer isso é abrindo o aplicativo **Programas e atualizações** e marcando a opção **Aplicativos restritos por copyright ou questões legais (multiverse)**, na aba **Aplicativos Ubuntu**:

{% include image.html src="/files/2023/05/virtualbox-22-pt.jpg" %}

Note que a opção **Aplicativos livres de código aberto mantidos pela comunidade (universe)** é marcada automaticamente.

Clique em **Fechar**.

Como você mudou a configuração de repositórios, o sistema sugere atualizar a lista de pacotes disponíveis. No aviso que aparece, clique em **Recarregar**:

{% include image.html src="/files/2023/05/virtualbox-23-pt.jpg" %}

Se você prefere usar o terminal, essas mesmas duas ações podem ser feitas com os comandos:

```
$ sudo add-apt-repository universe multiverse
$ sudo apt update
```

Com o repositório multiverse habilitado e a lista de pacotes atualizada, para instalar o VirtualBox usando a interface gráfica, abra o aplicativo **Ubuntu Software**, pesquise por `virtualbox`, clique na única opção que aparece e depois clique em **Instalar**:

{% include image.html src="/files/2023/05/virtualbox-24-pt.jpg" %}

Se você prefere instalar o VirtualBox pelo terminal, execute:

```
$ sudo apt install virtualbox virtualbox-qt
```

Com o VirtualBox instalado, para iniciá-lo, abra o menu **Atividades**, no canto superior esquerdo da tela, digite `virtualbox` e clique no ícone correspondente:

{% include image.html src="/files/2023/05/virtualbox-26-pt.png" %}

Aparece a tela inicial do VirtualBox, por enquanto sem nenhuma máquina virtual:

{% include image.html src="/files/2023/05/virtualbox-27-pt.jpg" %}

Agora você pode seguir o mesmo passo-a-passo que vimos na [parte 1][VirtualBox] para criar uma máquina virtual e rodar o Linux nela.

## Instalando o VirtualBox no Debian

Vejamos como instalar o VirtualBox no [Debian]. Provavelmente as instruções a seguir também funcionam em distribuições derivadas do Debian, a exemplo de: [MX Linux], [deepin] e [KNOPPIX].

A distribuição Debian não disponibiliza o VirtualBox em seus repositórios oficiais. É necessário adicionar o repositório do VirtualBox. Esse repositório sempre disponibiliza a versão mais recente do VirtualBox.

Para isso, abra o arquivo `/etc/apt/sources.list` usando um editor de texto (por exemplo, o **[nano]**):

```
$ sudo nano /etc/apt/sources.list
```

Adicione a seguinte linha ao final e salve:

```
deb [arch=amd64 signed-by=/usr/share/keyrings/oracle-virtualbox-2016.gpg] https://download.virtualbox.org/virtualbox/debian bullseye contrib
```

(caso use outra versão do Debian, mude `bullseye` por seu codinome, por exemplo: `buster`)

Além disso, importe a chave do repositório do VirtualBox para confiar nele:

```
$ wget -O- https://www.virtualbox.org/download/oracle_vbox_2016.asc | sudo gpg --dearmor --yes --output /usr/share/keyrings/oracle-virtualbox-2016.gpg
```

Atualize a lista de pacotes disponíveis:

```
$ sudo apt update
```

E instale o VirtualBox:

```
$ sudo apt install virtualbox-7.0
```

Feito isso, você já pode iniciar o VirtualBox e começar a usá-lo (veja instruções na [parte 1][VirtualBox]).

## Instalando o VirtualBox no openSUSE

Vejamos como instalar o VirtualBox no [Linux Kamarada][kamarada], que é baseado na distribuição [openSUSE Leap][leap-vs-tumbleweed]. Outro derivado brasileiro do openSUSE Leap que merece menção é o [Regata OS]. As instruções a seguir também devem funcionar no [openSUSE Tumbleweed][leap-vs-tumbleweed] e no [SUSE Linux Enterprise][sle].

O [Projeto openSUSE][opensuse] disponibiliza a versão mais recente do VirtualBox em seus repositórios oficiais (ou uma versão próxima da mais recente).

Para instalar o VirtualBox usando a interface gráfica, abra o menu **Atividades**, no canto superior esquerdo da tela, digite `software` e clique em **YaST Gerenciamento de software**:

{% include image.html src="/files/2023/03/virtualbox-28-pt.jpg" %}

Aguarde a lista de pacotes disponíveis ser atualizada:

{% include image.html src="/files/2023/03/virtualbox-29-pt.jpg" %}

No campo de texto, digite `virtualbox` e clique no botão **Pesquisar** (ou tecle **Enter**), marque o pacote **virtualbox** para instalação e clique em **Aceitar**:

{% include image.html src="/files/2023/03/virtualbox-30-pt.jpg" %}

O sistema informa que pacotes necessários serão instalados, clique em **Continuar**:

{% include image.html src="/files/2023/03/virtualbox-31-pt.jpg" %}

Aguarde o _download_ e instalação dos pacotes:

{% include image.html src="/files/2023/03/virtualbox-32-pt.jpg" %}

Quando a instalação terminar, clique em **Concluir**:

{% include image.html src="/files/2023/03/virtualbox-33-pt.jpg" %}

Se você prefere instalar o VirtualBox pelo terminal, execute:

```
$ sudo zypper ref
$ sudo zypper in virtualbox
```

Com o VirtualBox instalado, para iniciá-lo, abra o menu **Atividades**, digite `virtualbox` e clique no ícone do aplicativo:

{% include image.html src="/files/2023/03/virtualbox-34-pt.jpg" %}

Caso sua conta de usuário não pertença ao grupo de usuários `vboxusers`, aparece essa mensagem de erro, clique em **OK** para fechá-la:

{% include image.html src="/files/2023/03/virtualbox-35.jpg" %}

Usando o menu **Atividades**, abra o **YaST Gerenciamento de usuários e grupos**.

Selecione sua conta de usuário na lista e clique em **Editar**:

{% include image.html src="/files/2023/03/virtualbox-36-pt.jpg" %}

Mude para a aba **Detalhes**, em **Grupos Adicionais** ative o grupo **vboxusers** e clique em **OK**:

{% include image.html src="/files/2023/03/virtualbox-37-pt.jpg" %}

Clique em **OK** novamente para sair do **YaST Gerenciamento de usuários e grupos**.

Encerre sua sessão e faça _log in_ novamente para o sistema perceber seu novo grupo.

Inicie o VirtualBox novamente. O sistema pergunta se deseja habilitar o acesso a dispositivos USB (_USB passthrough_), essa mensagem aparece apenas no primeiro uso do VirtualBox:

{% include image.html src="/files/2023/03/virtualbox-38.jpg" style="max-height: 430px;" %}

A menos que você seja aficionado por segurança, creio que não há porque se preocupar. Clique em **Enable** para seguir.

Finalmente, chegamos à tela inicial do VirtualBox, por enquanto sem nenhuma máquina virtual:

{% include image.html src="/files/2023/03/virtualbox-39-pt.jpg" %}

Agora você pode seguir o mesmo passo-a-passo que vimos na [parte 1][VirtualBox] para criar uma máquina virtual e rodar o Linux nela. Divirta-se bastante! (_have a lot of fun!_)

## Instalando o VirtualBox no Fedora

Vejamos como instalar o VirtualBox no [Fedora]. Provavelmente as instruções a seguir também funcionam em distribuições relacionadas, como o [Red Hat Enterprise Linux][redhat] e o [CentOS].

A distribuição Fedora não disponibiliza o VirtualBox em seus repositórios oficiais. É necessário adicionar o repositório do VirtualBox. Esse repositório sempre disponibiliza a versão mais recente do VirtualBox.

Para isso, execute o comando:

```
$ sudo dnf config-manager --add-repo https://download.virtualbox.org/virtualbox/rpm/fedora/virtualbox.repo
```

Atualize a lista de pacotes disponíveis:

```
$ sudo dnf makecache
```

Após baixar a lista de pacotes do repositório do VirtualBox, o sistema pergunta se deseja importar a chave do repositório:

```
Fedora 38 - x86_64                              8.5 MB/s |  83 MB     00:09    
Fedora 38 openh264 (From Cisco) - x86_64        625  B/s | 2.5 kB     00:04    
Fedora Modular 38 - x86_64                      1.3 MB/s | 2.8 MB     00:02    
Fedora 38 - x86_64 - Updates                    3.3 MB/s |  15 MB     00:04    
Fedora Modular 38 - x86_64 - Updates            127  B/s | 257  B     00:02    
Fedora 38 - x86_64 - VirtualBox                 330  B/s | 819  B     00:02    
Fedora 38 - x86_64 - VirtualBox                 2.7 kB/s | 3.1 kB     00:01    
Importing GPG key 0x2980AECF:
 Userid     : "Oracle Corporation (VirtualBox archive signing key) <info@virtualbox.org>"
 Fingerprint: B9F8 D658 297A F3EF C18D 5CDF A2F6 83C5 2980 AECF
 From       : https://www.virtualbox.org/download/oracle_vbox_2016.asc
Is this ok [y/N]:
```

Responda que "sim" digitando `y` (de _yes_, sim em inglês) e teclando **Enter**:

```
Is this ok [y/N]: y
Fedora 38 - x86_64 - VirtualBox                  13 kB/s |  69 kB     00:05    
Metadata cache created.
```

Instale alguns pacotes que o VirtualBox usa para compilar módulos para o _kernel_ no Fedora:

```
$ sudo dnf install @development-tools
$ sudo dnf install kernel-headers kernel-devel dkms elfutils-libelf-devel qt5-qtx11extras
```

E, por fim, instale o VirtualBox propriamente dito:

```
$ sudo dnf install VirtualBox-7.0
```

O grupo `vboxusers` é criado durante a instalação. Adicione sua conta de usuário a esse grupo para poder usar o VirtualBox:

```
$ sudo usermod -a -G vboxusers $USER
```

Feito isso, você já pode iniciar o VirtualBox e começar a usá-lo (veja instruções na [parte 1][VirtualBox]).

## Instalando o VirtualBox no Manjaro

Vejamos como instalar o VirtualBox no [Manjaro]. Provavelmente as instruções a seguir também funcionam em distribuições derivadas do Manjaro, a exemplo da brasileira [BigLinux].

O Manjaro é baseado no [Arch Linux], que disponibiliza o VirtualBox em seu repositório oficial. Devido ao Arch Linux ser uma distribuição do tipo _[rolling release]_ (lançamento contínuo, em inglês, significa que novas versões de programas são disponibilizadas assim que lançadas), esse repositório sempre contém a versão mais recente do VirtualBox.

Para instalar o VirtualBox no Manjaro, você precisa instalar os pacotes **virtualbox** e **linux*-virtualbox-host-modules**, sendo que este último deve corresponder à versão do _kernel_ que você está usando.

Para listar quais _kernels_ estão instalados, use o comando:

```
$ mhwd-kernel -li
Currently running: 6.1.25-1-MANJARO (linux61)
The following kernels are installed in your system:
   * linux61
```

Então, para instalar o VirtualBox e os módulos do _kernel_ para o _kernel_ instalado, execute:

```
$ sudo pacman -Syu virtualbox linux61-virtualbox-host-modules
```

Concluída a instalação, será necessário carregar o módulo do VirtualBox para o _kernel_. A maneira mais fácil de fazer isso é simplesmente reiniciar o sistema. Se você prefere fazer isso manualmente, caso queira começar a usar o VirtualBox imediatamente, execute o seguinte comando:

```
$ sudo vboxreload
```

Feito isso, você já pode iniciar o VirtualBox e começar a usá-lo (veja instruções na [parte 1][VirtualBox]).

## Instalando o VirtualBox em outras distribuições Linux

Assim como o _site_ do VirtualBox disponibiliza um instalador para Windows, ele também disponibiliza pacotes para as distribuições Linux mais populares e um instalador genérico, que possibilita para instalar o VirtualBox em qualquer sistema Linux.

Acesse o _site_ oficial do VirtualBox em:

- [https://www.virtualbox.org/](https://www.virtualbox.org/)

E clique no _banner_ **Download VirtualBox 7.0**.

Na página seguinte, abaixo de _VirtualBox binaries_ (binários do VirtualBox), clique em _Linux distributions_ (distribuições Linux).

Nessa página, clique no _link_ correspondente à distribuição que você usa (note que ela pode não estar na lista, mas ser baseada em uma das que estão):

{% include image.html src="/files/2023/05/virtualbox-40.jpg" %}

Uma vez baixado o pacote, use a ferramenta apropriada da sua distribuição para instalá-lo.

Alternativamente, você pode baixar o instalador genérico clicando com o botão direito em _All distributions_ (todas as distribuições, o último _link_) e usando **Salvar link como**.

Uma vez baixado o instalador genérico, execute-o a partir do terminal:

```
$ chmod +x Downloads/VirtualBox-7.0.8-156879-Linux_amd64.run
$ sudo Downloads/VirtualBox-7.0.8-156879-Linux_amd64.run
```

Na dúvida, verifique se a distribuição que você usa fornece instruções sobre como instalar o VirtualBox.

## Ainda tem mais...

Agora, usuários de Windows e de Linux todos estamos na mesma página, ou seja, já sabemos instalar o VirtualBox nos sistemas que usamos e conseguimos criar máquinas virtuais.

Na [terceira parte][virtualbox-tips], veremos como criar um disco rígido virtual e instalar o Linux na máquina virtual, assim como dicas para usar o VirtualBox no dia a dia.

{% capture atualizacao %}
A [terceira parte]({% post_url pt/2019-10-30-dicas-para-usar-o-virtualbox-no-dia-a-dia %}) (também atualizada) já está disponível!
{% endcapture %}
{% include update.html date="06/04/2023" message=atualizacao %}

## Referências

Para referência futura, aqui utilizei as seguintes versões de _softwares_:

- Ubuntu 23.04 (Lunar Lobster) com VirtualBox 7.0.6
- Debian 11.7.0 (Bullseye) com VirtualBox 7.0.8
- Linux Kamarada 15.4 Build 5.6 com VirtualBox 7.0.6
- Fedora 38 Workstation com VirtualBox 7.0.8
- Manjaro GNOME 22.1.0 com VirtualBox 7.0.8

Todas as distribuições são as versões mais recentes no momento da escrita.

Para escrever esse _post_, consultei os seguintes textos:

- [Linux Downloads - Oracle VM VirtualBox][virtualbox-linux-downloads]
- [Oracle VM VirtualBox - User Manual - Chapter 2 - Installation Details][virtualbox-manual]
- [package management - How do I enable the "multiverse" repository? - Ask Ubuntu][askubuntu]
- [How to Enable Universe and Multiverse Repositories in Ubuntu - It's FOSS][itsfoss]
- [How To Install VirtualBox 7.0 on Fedora 38/37/36/35/34/33 \| ComputingForGeeks][computingforgeeks]
- [Adding or removing software repositories in Fedora - Fedora Docs Site][fedora-docs]
- [VirtualBox - Manjaro][manjaro-wiki]

[VirtualBox]:                   {% post_url pt/2019-10-08-virtualbox-a-forma-mais-facil-de-conhecer-o-linux-sem-precisar-instala-lo %}
[Linux]:                        https://www.vivaolinux.com.br/linux/
[Windows]:                      https://www.microsoft.com/pt-br/windows/
[kamarada]:                     {% post_url pt/2023-02-27-linux-kamarada-15-4-mais-funcional-bonito-polido-e-elegante-do-que-nunca %}
[leap-vs-tumbleweed]:           {% post_url pt/2020-12-06-opensuse-leap-e-opensuse-tumbleweed-qual-e-a-diferenca %}

[Ubuntu]:                       https://ubuntu.com/
[Linux Mint]:                   https://linuxmint.com/
[Pop!_OS]:                      https://pop.system76.com/
[elementary OS]:                https://elementary.io/
[Zorin OS]:                     https://zorinos.com/

[Debian]:                       https://www.debian.org/
[MX Linux]:                     https://mxlinux.org/
[deepin]:                       https://www.deepin.org/index/en
[KNOPPIX]:                      https://www.knopper.net/knoppix/index-en.html
[nano]:                         https://www.nano-editor.org/

[Regata OS]:                    https://www.regataos.com.br/
[sle]:                          https://www.suse.com/pt-br/
[opensuse]:                     https://www.opensuse.org/

[Fedora]:                       https://getfedora.org/pt_BR/
[redhat]:                       https://www.redhat.com/pt-br
[CentOS]:                       https://centos.org/

[Manjaro]:                      https://manjaro.org/
[BigLinux]:                     https://www.biglinux.com.br/
[Arch Linux]:                   https://www.archlinux.org/
[rolling release]:              https://pt.wikipedia.org/wiki/Rolling_release

[virtualbox-tips]:              {% post_url pt/2019-10-30-dicas-para-usar-o-virtualbox-no-dia-a-dia %}

[virtualbox-linux-downloads]:   https://www.virtualbox.org/wiki/Linux_Downloads
[virtualbox-manual]:            https://www.virtualbox.org/manual/ch02.html#install-linux-host
[askubuntu]:                    https://askubuntu.com/a/89100/560233
[itsfoss]:                      https://itsfoss.com/ubuntu-repositories/
[computingforgeeks]:            https://computingforgeeks.com/how-to-install-virtualbox-on-fedora-linux/
[fedora-docs]:                  https://docs.fedoraproject.org/en-US/quick-docs/adding-or-removing-software-repositories-in-fedora/
[manjaro-wiki]:                 https://wiki.manjaro.org/index.php?title=VirtualBox
