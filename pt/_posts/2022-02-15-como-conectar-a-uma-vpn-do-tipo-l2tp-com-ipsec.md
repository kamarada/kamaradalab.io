---
date: '2022-02-15 11:59:00 GMT-3'
image: '/files/2022/02/l2tp-client.jpg'
layout: post
nickname: 'l2tp-client'
title: 'Como conectar a uma VPN do tipo L2TP com IPsec'
---

{% include image.html src='/files/2022/02/l2tp-client.jpg' %}

No meu [_blog_ pessoal][antoniomedeiros-1], fiz um [tutorial][antoniomedeiros-1] explicando como configurar em um [roteador MikroTik doméstico][antoniomedeiros-2] uma [VPN] usando o protocolo [L2TP] (do inglês _Layer 2 Tunnelling Protocol_, Protocolo de Tunelamento de Camada 2) em conjunto com o protocolo [IPsec] (do inglês _IP Security Protocol_, Protocolo de Segurança IP). O objetivo daquela VPN era permitir que computadores de fora (por exemplo, eu com meu _notebook_ na rua, conectado à rede Wi-Fi de algum café) se conectassem à rede de casa. Mas note que organizações (como empresas ou universidades) também podem fornecer VPNs desse tipo para permitir que pessoas (funcionários ou alunos) se conectem remotamente às suas redes.

Se você usa um computador com [Linux] e precisa conectá-lo a uma VPN desse tipo (baseada nos protocolos L2TP e IPsec), veja a seguir como fazer isso, instalando e configurando o _software_ necessário.

Como referência, vou usar a distribuição [Linux Kamarada 15.3]. As instruções também se aplicam às distribuições do [Projeto openSUSE] ([Leap e Tumbleweed]). Eu uso a área de trabalho [GNOME], mas as configurações devem ser parecidas em outras áreas de trabalho.

Se por um acaso você procurava instruções para outro sistema operacional (como [Windows], [Android] ou [iOS]), consulte os _links_ no final do [tutorial no meu _blog_ pessoal][antoniomedeiros-1].

## Instalando os pacotes necessários

Instale os pacotes necessários usando o gerenciador de pacotes **[zypper]**:

```
# zypper in NetworkManager-l2tp-gnome NetworkManager-l2tp-lang NetworkManager-strongswan-gnome NetworkManager-strongswan-lang strongswan-ipsec
```

## Configurando a VPN

Abra o aplicativo **Configurações**. Você pode fazer isso abrindo o **menu do sistema**, no canto superior direito da tela, e clicando no ícone da **engrenagem**:

{% include image.html src='/files/2022/02/l2tp-client-01-pt.jpg' %}

No aplicativo **Configurações**, à esquerda, selecione a categoria **Rede**. À direita, na seção **VPN**, clique no botão de **adicionar**:

{% include image.html src='/files/2022/02/l2tp-client-02-pt.jpg' %}

Escolha a VPN do tipo **Layer 2 Tunneling Protocol (L2TP)**:

{% include image.html src='/files/2022/02/l2tp-client-03-pt.jpg' %}

Preencha as informações sobre a VPN de acordo com as orientações a seguir, mas também de acordo com as orientações passadas pelo administrador da rede que configurou a VPN (os exemplos se referem ao [tutorial de como configurar VPN no roteador MikroTik][vpn]):

{% include image.html src='/files/2022/02/l2tp-client-04-pt.jpg' %}

- No campo **Nome**, forneça um nome que te permita identificar a VPN, pode ser o nome que você quiser (exemplo: `VPN de Teste`)
- Em **Gateway**, indique o endereço IP (exemplo: `179.216.177.166`) ou nome e domínio DNS (exemplo: `6bxxxxxxxxc2.sn.mynetname.net`) do servidor da VPN (nesse caso, do roteador MikroTik)
- Informe seu **Nome de usuário** para conectar à VPN (exemplo: `teste`)
- Informe sua **Senha** para conectar à VPN (exemplo: `testando`)

Clique no botão **IPsec Settings**.

Na caixa de diálogo que aparece, habilite a opção **Enable IPsec tunnel to L2TP host**:

{% include image.html src='/files/2022/02/l2tp-client-05-pt.jpg' %}

No campo **Pre-shared key**, informe a chave pré-compartilhada (exemplo: `12345678`).

Clique em **OK** para voltar às configurações da VPN e, depois, clique em **PPP Settings**.

Desabilite o método de autenticação **PAP** e clique em **OK**:

{% include image.html src='/files/2022/02/l2tp-client-06-pt.jpg' %}

Para terminar de configurar a VPN, clique em **Adicionar**.

De volta às configurações de **Rede**, note que a VPN passa a aparecer na lista de VPNs:

{% include image.html src='/files/2022/02/l2tp-client-07-pt.jpg' %}

Por meio do aplicativo **Configurações**, você pode ativar ou desativar a conexão com a VPN, assim como mudar suas configurações. Mas vejamos um jeito mais prático de fazer isso no dia a dia, sem precisar passar por esse aplicativo. Por ora, você pode fechá-lo.

## Usando a VPN

Para conectar à VPN, abra o **menu do sistema**, expanda o submenu da VPN e clique em **Conectar**:

{% include image.html src='/files/2022/02/l2tp-client-08-pt.jpg' %}

Se a conexão for feita com sucesso, você verá um ícone indicando essa conexão:

{% include image.html src='/files/2022/02/l2tp-client-09.jpg' %}

Você pode testar a conexão com o comando **[ping]**, "fazendo um pingue" para algum endereço na rede local da VPN:

```
$ ping 10.0.0.2
```

(use a combinação de teclas **Ctrl + C** para interromper o comando **ping**)

Se souber de algum servidor _web_ na rede local da VPN, você também pode testar a conexão abrindo o navegador e acessando esse servidor. Nesse exemplo, eu testo a conexão com a VPN acessando a interface _web_ da minha impressora:

{% include image.html src='/files/2022/02/l2tp-client-10-pt.jpg' %}

Quando não precisar mais usar a VPN, desconecte da VPN abrindo o **menu do sistema**, depois expandindo a VPN e por último clicando em **Desligar**:

{% include image.html src='/files/2022/02/l2tp-client-11-pt.jpg' %}

[antoniomedeiros-1]:    https://antoniomedeiros.dev/blog/2021/04/09/mikrotik-como-criar-uma-vpn-com-l2tp-e-ipsec/
[antoniomedeiros-2]:    https://antoniomedeiros.dev/blog/2020/07/20/primeiros-passos-com-o-mikrotik-hap-ac-roteador-profissional-para-a-rede-de-casa/
[VPN]:                  {% post_url pt/2017-07-07-como-conectar-a-uma-vpn-do-openvpn %}
[L2TP]:                 https://pt.wikipedia.org/wiki/Layer_2_Tunneling_Protocol
[IPsec]:                https://pt.wikipedia.org/wiki/IPsec
[Linux]:                https://www.vivaolinux.com.br/linux/
[Linux Kamarada 15.3]:  {% post_url pt/2021-12-30-linux-kamarada-15-3-ano-novo-distribuicao-nova %}
[Projeto openSUSE]:     https://www.opensuse.org/
[Leap e Tumbleweed]:    {% post_url pt/2020-12-06-opensuse-leap-e-opensuse-tumbleweed-qual-e-a-diferenca %}
[GNOME]:                https://br.gnome.org/
[Windows]:              https://www.microsoft.com/pt-br/windows/
[Android]:              https://www.android.com/
[iOS]:                  https://www.apple.com/br/ios/
[zypper]:               https://en.opensuse.org/Portal:Zypper
[ping]:                 https://man7.org/linux/man-pages/man8/ping.8.html
