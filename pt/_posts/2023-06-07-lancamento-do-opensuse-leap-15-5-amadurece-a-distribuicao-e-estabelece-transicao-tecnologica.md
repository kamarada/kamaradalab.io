---
date: '2023-06-07 20:00:00 GMT-3'
image: '/files/2023/06/leap-15.5.png'
layout: post
nickname: 'leap-15.5'
title: 'Lançamento do openSUSE Leap 15.5 amadurece a distribuição e estabelece transição tecnológica'
excerpt: 'O lançamento da última versão da série 15.x do openSUSE Leap marca anos de manutenção e segurança que começaram há mais de cinco anos. A maturidade do openSUSE Leap 15.5 entra em jogo à medida que novas mudanças tecnológicas dos últimos cinco anos foram introduzidas, como tecnologias de contêineres, sistemas imutáveis, virtualização, desenvolvimento embarcado e outros avanços da alta tecnologia.'
---

{% capture nota1 %}
Esta é uma tradução não oficial da notícia originalmente publicada por Douglas DeMaio no _site_ [news.opensuse.org](https://news.opensuse.org/2023/06/07/leap-release-matures-sets-up-tech-transition/).
{% endcapture %}

{% include note.html text=nota1 %}

{% include image.html src='/files/2023/06/leap-15.5.png' %}

NUREMBERGUE, Alemanha -- O lançamento da última versão da série 15.x do [openSUSE](https://get.opensuse.org) Leap marca anos de manutenção e segurança que começaram há mais de cinco anos.

A maturidade do [openSUSE Leap 15.5](https://get.opensuse.org/leap/15.5/) entra em jogo à medida que novas mudanças tecnológicas dos últimos cinco anos foram introduzidas, como tecnologias de contêineres, sistemas imutáveis, virtualização, desenvolvimento embarcado e outros avanços da alta tecnologia.

Empreendedores, entusiastas, profissionais e desenvolvedores adotaram o openSUSE Leap como sua distribuição Linux preferida, conforme visto pelo [aumento no uso](https://metrics.opensuse.org/) de cada versão menor. A passagem do _software_ legado para sistemas mais modernos é iminente.

O [openSUSE Leap 15.5](https://get.opensuse.org/leap/15.5/), que é baseado no [SUSE Linux Enterprise 15](https://www.suse.com/products/server/) Service Pack 5, receberá atualizações de manutenção e segurança até o final de 2024. Isso fornecerá aos usuários bastante tempo para fazer a transição para a versão seguinte (ainda a ser confirmada). Usuários interessados em suporte comercial podem usar uma [nova ferramenta](https://en.opensuse.org/SDB:How_to_migrate_to_SLE) para fazer a transição para o suporte comercial. O openSUSE Leap pode ser baixado em [get.opensuse.org](https://get.opensuse.org).

O [Leap 15.5](https://get.opensuse.org/leap/15.5/) traz pacotes mais novos, como [Mesa](https://www.mesa3d.org/) e outros, mas esta versão é uma atualização sem novos recursos (_non-feature release_). Alguns desses pacotes mais novos a serem destacados incluem o KDE [Plasma 5.27](https://kde.org/announcements/plasma/5/5.27.0/), que é uma versão com suporte estendido (_Long Term Support_, LTS) do Plasma até que a próxima seja lançada em 2024. Os amantes do [Konqi](https://community.kde.org/Konqi) irão desfrutar de um novo assistente de boas-vindas, personalização dinâmica de ambientes da área de trabalho e mais funcionalidades com o [KRunner](https://userbase.kde.org/Plasma/Krunner) que inclui uma pesquisa completa na área de trabalho, conversões de unidades de medidas e taxas de câmbio de moedas, definições de dicionários, recursos de calculadora e mostra representações gráficas de funções matemáticas. O Color Picker teve algumas melhorias e adicionou a possibilidade de exibir outro círculo de cores para pré-visualização. O [KDE Gear 22.12.3](https://kde.org/announcements/gear/22.12.3/) é um novo pacote no lançamento e complementa o uso do [Plasma 5.27](https://kde.org/announcements/plasma/5/5.27.0/). A atualização corrige _bugs_ com os aplicativos do KDE e destaca o aprimoramento do utilitário de compactação/descompactação [ark](https://apps.kde.org/ark/), melhorias no editor de texto [Kate](https://apps.kde.org/kate/) e corrige alguns travamentos do editor de vídeo [Kdenlive](https://apps.kde.org/kdenlive/). O Qt 5.15 LTS está disponível com a [coleção de _patches_ do KDE para o Qt 5](https://dot.kde.org/2021/04/06/announcing-kdes-qt-5-patch-collection).

O editor de texto [Vim](https://www.vim.org/) foi atualizado para uma nova versão principal. O [Vim](https://www.vim.org/) 9 tem uma nova linguagem de _script_ que melhora drasticamente o desempenho. Aumentos na velocidade de execução de 10 a 100 vezes são esperados para o editor de texto. Recomenda-se que os usuários leiam as [informações de lançamento do Vim 9.0](https://www.vim.org/vim90.php) divulgadas pelo projeto para saber como a atualização afeta _scripts_ legados, a compatibilidade com versões anteriores e outros novos recursos adicionados à versão.

Para profissionais que configuram redes de contêineres Linux, o pacote [netavark](https://github.com/containers/netavark) 1.5.0 está presente nessa versão. Usuários que já fizeram a transição para pacotes do [Flatpak](https://flatpak.org/) terão uma versão [1.14.4](https://github.com/flatpak/flatpak/releases/tag/1.14.4) atualizada que aborda uma vulnerabilidade conhecida ([_Common Vulnerability and Exposure_](https://pt.wikipedia.org/wiki/Common_Vulnerabilities_and_Exposures)) detalhada no [CVE-2023-28101](https://github.com/flatpak/flatpak/security/advisories/GHSA-h43h-fwqx-mpp8). A versão mais recente corrige a ocultação de permissões para um invasor que publica um aplicativo Flatpak com intenção maliciosa de elevar as permissões.

O Leap 15.5 está configurado para ter o repositório do [OpenH264](https://en.opensuse.org/OpenH264) habilitado por padrão para todas as novas instalações, graças às contribuições da [equipe de código aberto da Cisco](https://eti.cisco.com/open-source).

O Leap 15.5 vem com o [Linux Kernel](https://www.kernel.org/) 5.14.21 que inclui _backports_ para esse _kernel_ LTS específico do SUSE. O Leap 15.5 compartilha o _kernel_ com o SLE 15 SP5 e recebe as mesmas correções e _backports_. Isso também se aplica a alguns milhares de outros pacotes compartilhados. O pacote do atualizador de firmware do dispositivo [fwupd](https://github.com/fwupd/fwupd) mudará para a versão 1.8.6. A versão mais recente corrige erros de compilação ao compilar para [s390x](https://en.wikipedia.org/wiki/IBM_System/390) e [ppc64le](https://en.wikipedia.org/wiki/Ppc64).

Boas notícias para desenvolvedores Python. Os usuários do Leap 15.5 terão uma pilha Python 3.11 completamente utilizável em paralelo ao Python do sistema (Python 3.6). Com isso, uma versão mais moderna é fornecida para usuários e desenvolvedores. Os empacotadores são aconselhados a mudar para o Python 3.11.

Outros pacotes cujas atualizações os usuários podem notar são o [Ugrep](https://github.com/Genivia/ugrep) 3.11.0 e o [NetworkManager](https://networkmanager.dev/) 1.38.6. Tanto o [webkit2gtk3](https://webkitgtk.org/) quanto o [webkit2gtk4](https://webkitgtk.org/) foram atualizados para a versão 2.38.5. O [Firefox](https://www.mozilla.org/en-US/firefox/new/), o navegador da [Mozilla](https://www.mozilla.org/), recebe uma nova versão de suporte estendido (_Extended Support Release_) -- 102.9.0. O [Thunderbird](https://www.thunderbird.net/en-US/thunderbird/102.8.0/releasenotes/), o cliente de _e-mail_ da [Mozilla](https://www.mozilla.org/), ganha várias melhorias visuais e de experiência do usuário com a versão 102.8.0.

## Fim de vida

O openSUSE Leap 15.4 terá seu fim de vida (_End of Life_, EOL) seis meses a partir do lançamento de hoje. Seus usuários devem atualizar para o openSUSE Leap 15.5 dentro de seis meses a partir de hoje para continuar recebendo atualizações de segurança e manutenção.

## Importante para usuários atualizando de versões anteriores

Uma nova chave de assinatura RSA de 4096 _bits_ foi introduzida como parte do openSUSE Leap 15.5, assim como do 15.4 por meio de uma atualização de manutenção. Espera-se que os usuários do Leap 15.4 atualizem (_update_) seus sistemas antes de atualizarem (_upgrade_) para o 15.5. Para mais informações, consulte a [_wiki_ do openSUSE](https://en.opensuse.org/SDB:System_upgrade#0._New_4096_bit_RSA_signing_key).

## Baixe o Leap 15.5

Para baixar a imagem ISO, visite <https://get.opensuse.org/leap/>.

## Dúvidas

Se você tiver alguma dúvida sobre o lançamento ou encontrar um _bug_, adoraríamos escutar você em:

<https://t.me/openSUSE>

<https://chat.opensuse.org>

<https://lists.opensuse.org/opensuse-support/>

<https://discordapp.com/invite/openSUSE>

<https://www.facebook.com/groups/opensuseproject>

## Participe

O Projeto openSUSE é uma comunidade ao redor do mundo que promove o uso do Linux em todos os lugares. Ele cria duas das melhores distribuições Linux do mundo: a Tumbleweed, atualizada continuamente (_rolling-release_), e a Leap, distribuição híbrida de empresa e comunidade. O openSUSE trabalha continuamente de maneira conjunta, aberta, transparente e amigável como parte da comunidade internacional de _software_ livre e de código aberto (_Free and Open Source Software_, FOSS). O projeto é comandado por sua comunidade e depende das contribuições de indivíduos que trabalham como testadores, escritores, tradutores, especialistas em usabilidade, artistas, embaixadores ou desenvolvedores. O projeto abrange uma ampla variedade de tecnologias, pessoas com diferentes níveis de conhecimentos, que falam diferentes línguas e possuem diversas raízes culturais. Saiba mais sobre o Projeto openSUSE em [opensuse.org](https://opensuse.org).
