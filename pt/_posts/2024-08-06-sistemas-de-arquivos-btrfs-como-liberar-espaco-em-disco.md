---
date: '2024-08-06 23:00:00 GMT-3'
image: '/files/2024/08/btrfs-free-space.jpg'
layout: post
nickname: 'btrfs-free-space'
title: 'Sistemas de arquivos BTRFS: como liberar espaço em disco'
excerpt: 'Normalmente, quando queremos liberar espaço em disco, desinstalamos programas que não usamos mais e excluímos arquivos temporários, assim como arquivos pessoais que não precisamos mais. Mas liberar espaço em um sistema de arquivos BTRFS pode não ser tão simples assim. Confira algumas medidas para liberar espaço específicas desse sistema de arquivos.'
---

{% include image.html src='/files/2024/08/btrfs-free-space.jpg' %}

Normalmente, quando queremos liberar espaço em disco, desinstalamos programas que não usamos mais e excluímos arquivos temporários, assim como arquivos pessoais que não precisamos mais. O aplicativo [**Analisador de utilização do disco**][baobab] pode ajudar nessa faxina. Ele vem instalado por padrão no [Linux Kamarada] e em outras distribuições que usam a área de trabalho [GNOME].

No meu caso, eu queria testar uma distribuição [Linux], instalando-a ao lado do Linux Kamarada. Para isso, eu queria reduzir a partição raiz (`/`) usando o **[Gparted]**. Vendo o espaço **Usado** na partição `/dev/sda5`, pensei que seria interessante fazer uma faxina para liberar mais espaço para a nova distribuição:

{% include image.html src='/files/2024/08/btrfs-gparted.jpg' %}

Acontece que, ao usar o Analisador de utilização do disco para ver o que estava ocupando tanto espaço, ele me mostrou uma informação diferente do Gparted:

{% include image.html src='/files/2024/08/btrfs-baobab.jpg' %}

Segundo o Gparted, minha partição raiz usava 66 GB, mas segundo o Analisador de utilização do disco, ela usava 17 GB.

Isso pode acontecer em sistemas de arquivos [BTRFS][diolinux], o sistema de arquivos usado por padrão no [openSUSE Leap], devido a algumas características próprias desse sistema de arquivos -- como, por exemplo, a criação de instantâneos (_snapshots_) dos arquivos, compressão, armazenamento dos arquivos em blocos (_chunks_). Tudo isso faz com que o tamanho dos arquivos armazenados na partição não corresponda exatamente ao tamanho do espaço usado no disco. Dependendo do aplicativo e de como ele trate o sistema de arquivos BTRFS, ele vai mostrar como "espaço usado" um número ou outro.

Por isso, liberar espaço em sistemas de arquivos BTRFS pode não ser tão óbvio quanto apenas desinstalar programas e excluir arquivos.

Pesquisei e encontrei algumas medidas para liberar espaço específicas do BTRFS, e as compartilho a seguir, mostrando como fiz para resolver meu problema. 

## Verifique o espaço usado

Normalmente, quando queremos verificar o espaço livre no disco, usamos o comando **[df]**. No caso de um sistema de arquivos BTRFS, diante do que expus antes, para obtermos informações mais precisas e detalhadas, é melhor usarmos o comando **[btrfs]**, específico desse sistema de arquivos.

Use o comando **btrfs** para listar todas as partições BTRFS no seu computador:

```
# btrfs filesystem show | grep -C2 '/dev/.*'
```

Ele deve retornar algo parecido com:

```
Label: none  uuid: 10d08ffc-e2d5-4501-9089-d4f29e7dc3d7
	Total devices 1 FS bytes used 66.63GiB
	devid    1 size 100.00GiB used 77.02GiB path /dev/sda5
```

O comando **[grep]**, da forma que usamos, destaca a partição em vermelho:

{% include image.html src='/files/2024/08/btrfs-filesystem-show.jpg' %}

No meu caso, só há uma partição BTRFS, que é a `/dev/sda5`.

Monte a partição BTRFS:

```
# mount -t btrfs -o subvol=/@ /dev/sda5 /mnt
```

Por fim, obtenha informações detalhadas sobre o espaço usado em disco:

```
# btrfs filesystem usage /mnt

Overall:
    Device size:		 100.00GiB
    Device allocated:		  77.02GiB
    Device unallocated:		  22.98GiB
    Device missing:		     0.00B
    Used:			  66.63GiB
    Free (estimated):		  32.55GiB	(min: 32.55GiB)
    Free (statfs, df):		  32.55GiB
    Data ratio:			      1.00
    Metadata ratio:		      1.00
    Global reserve:		 112.59MiB	(used: 0.00B)
    Multiple profiles:		        no

Data,single: Size:73.98GiB, Used:64.41GiB (87.06%)
   /dev/sda5	  73.98GiB

Metadata,single: Size:3.01GiB, Used:2.23GiB (74.02%)
   /dev/sda5	   3.01GiB

System,single: Size:32.00MiB, Used:16.00KiB (0.05%)
   /dev/sda5	  32.00MiB

Unallocated:
   /dev/sda5	  22.98GiB
```

De todas essas informações, nos interessam três:

- o tamanho da partição (`Device size`): 100 GB;
- quanto foi alocado para armazenar os arquivos (`Device allocated`): 77 GB; e
- quanto esses arquivos ocupam de fato (`Used`): 66 GB.

## Exclua instantâneos

Cada vez que você faz uma alteração importante no seu sistema -- por exemplo, instala um programa -- ele cria um instantâneo (_snapshot_) dos arquivos, de modo que, se algo der errado, você pode retornar o sistema para um estado anterior. Se você usa [Windows], é a mesma ideia do [ponto de restauração].

Não preciso dizer que esses instantâneos ocupam espaço no disco. Se seu sistema está funcionando bem, você pode exclui-los, o que liberará espaço.

No [openSUSE], podemos gerenciar os instantâneos do BTRFS com a ferramenta [Snapper]. Para listá-los, execute:

```
# snapper --no-dbus --root /mnt list

   # | Tipo   | Pre # | Data                     | Usuário | Espaço usado | Limpeza | Descrição             | Dados de usuário
-----+--------+-------+--------------------------+---------+--------------+---------+-----------------------+-----------------
  0  | single |       |                          | root    |              |         | current               |                 
  1+ | single |       | dom 18 set 2022 14:45:19 | root    |   133,69 MiB |         | first root filesystem |                 
924  | pre    |       | qua 26 jun 2024 17:56:16 | root    |   222,42 MiB | number  | zypp(zypper)          | important=yes   
925  | post   |   924 | qua 26 jun 2024 17:57:45 | root    |    78,13 MiB | number  |                       | important=yes   
954  | pre    |       | qua 03 jul 2024 15:46:27 | root    |   156,52 MiB | number  | zypp(zypper)          | important=yes   
955  | post   |   954 | qua 03 jul 2024 15:54:23 | root    |   348,55 MiB | number  |                       | important=yes   
986  | pre    |       | seg 22 jul 2024 15:55:50 | root    |     3,32 MiB | number  | yast sw_single        |                 
987  | pre    |       | seg 22 jul 2024 15:56:46 | root    |   160,00 KiB | number  | zypp(ruby.ruby2.5)    | important=no    
988  | post   |   987 | seg 22 jul 2024 15:57:06 | root    |   400,00 KiB | number  |                       | important=no    
989  | post   |   986 | ter 23 jul 2024 19:13:56 | root    |     3,50 MiB | number  |                       |                 
990  | pre    |       | ter 23 jul 2024 20:53:03 | root    |   144,00 KiB | number  | zypp(zypper)          | important=yes   
991  | pre    |       | ter 23 jul 2024 20:53:30 | root    |    96,00 KiB | number  | zypp(zypper)          | important=yes   
992  | pre    |       | ter 23 jul 2024 21:02:32 | root    |     1,47 MiB | number  | zypp(zypper)          | important=yes   
993  | post   |   992 | ter 23 jul 2024 21:17:51 | root    |     6,42 MiB | number  |                       | important=yes   
994  | pre    |       | ter 23 jul 2024 23:47:18 | root    |     1,91 MiB | number  | zypp(zypper)          | important=yes   
995  | post   |   994 | ter 23 jul 2024 23:47:58 | root    |     1,66 MiB | number  |                       | important=yes   
996  | pre    |       | qua 24 jul 2024 00:50:40 | root    |   612,00 KiB | number  | yast sw_single        |                 
997  | post   |   996 | qua 24 jul 2024 00:51:36 | root    |     1,71 MiB | number  |                       |                 
998  | pre    |       | ter 30 jul 2024 12:16:36 | root    |   193,77 MiB | number  | zypp(zypper)          | important=yes   
999  | post   |   998 | ter 30 jul 2024 12:20:10 | root    |    17,38 MiB | number  |                       | important=yes
```

Em seguida, exclua-os (esse comando pode demorar um pouco):

```
# snapper --no-dbus --root /mnt delete --sync 924-999
```

Repita o comando anterior de listagem para se certificar de que foram excluídos:

```
# snapper --no-dbus --root /mnt list

 # | Tipo   | Pre # | Data                     | Usuário | Espaço usado | Limpeza | Descrição             | Dados de usuário
---+--------+-------+--------------------------+---------+--------------+---------+-----------------------+-----------------
0  | single |       |                          | root    |              |         | current               |                 
1+ | single |       | dom 18 set 2022 14:45:19 | root    |    16,22 GiB |         | first root filesystem |
```

Feito isso, vamos verificar novamente o espaço usado em disco:

```
# btrfs filesystem usage /mnt

Overall:
    Device size:		 100.00GiB
    Device allocated:		  77.02GiB
    Device unallocated:		  22.98GiB
    Device missing:		     0.00B
    Used:			  59.09GiB
    Free (estimated):		  39.14GiB	(min: 39.14GiB)
    Free (statfs, df):		  39.14GiB
    Data ratio:			      1.00
    Metadata ratio:		      1.00
    Global reserve:		  99.58MiB	(used: 0.00B)
    Multiple profiles:		        no

Data,single: Size:73.98GiB, Used:57.82GiB (78.16%)
   /dev/sda5	  73.98GiB

Metadata,single: Size:3.01GiB, Used:1.27GiB (42.14%)
   /dev/sda5	   3.01GiB

System,single: Size:32.00MiB, Used:16.00KiB (0.05%)
   /dev/sda5	  32.00MiB

Unallocated:
   /dev/sda5	  22.98GiB
```

O tamanho dos arquivos (`Used`) reduziu bastante: de 66 para 59 GB, ou seja, liberamos 7 GB excluindo instantâneos. Nada mal.

Mas o espaço em disco alocado para armazenar os arquivos (`Device allocated`) não mudou: continua os mesmos 77 GB. Agora vamos melhorar essa alocação.

## Rebalanceie o sistema de arquivos

O sistema de arquivos BTRFS armazena os arquivos em blocos (_chunks_). Um arquivo grande pode ocupar vários blocos, enquanto um arquivo pequeno pode não preencher um bloco inteiro. Por isso essa diferença: 77 GB escritos no disco representam, na verdade, 59 GB de dados úteis. Fazendo as contas, temos uma proporção de 59 / 77 = 0,77 = 77% (guarde esse número).

Felizmente, há uma forma de resolver esse problema: o BTRFS tem uma ferramenta para "rebalancear" o sistema de arquivos.

Por padrão, esse rebalanceamento reescreverá todos os dados no disco, o que provavelmente é desnecessário. Os blocos não são igualmente preenchidos, mas vimos que, na média, eles estão cerca de 77% preenchidos. Podemos rebalancear apenas os blocos que estiverem menos de 77% preenchidos, não mexendo em quaisquer blocos que estejam mais preenchidos do que a média.

Para rebalancear o sistema de arquivos BTRFS dessa forma, execute:

```
# btrfs balance start -dusage=77 /mnt
```

Note que esse comando demora bastante.

Ao final, ele deve reportar algo como:

```
Done, had to relocate 15 out of 82 chunks
```

Verifique mais uma vez o espaço usado em disco:

```
# btrfs filesystem usage /mnt

Overall:
    Device size:		 100.00GiB
    Device allocated:		  67.02GiB
    Device unallocated:		  32.98GiB
    Device missing:		     0.00B
    Used:			  59.08GiB
    Free (estimated):		  39.14GiB	(min: 39.14GiB)
    Free (statfs, df):		  39.14GiB
    Data ratio:			      1.00
    Metadata ratio:		      1.00
    Global reserve:		  95.70MiB	(used: 0.00B)
    Multiple profiles:		        no

Data,single: Size:63.98GiB, Used:57.82GiB (90.37%)
   /dev/sda5	  63.98GiB

Metadata,single: Size:3.01GiB, Used:1.26GiB (42.03%)
   /dev/sda5	   3.01GiB

System,single: Size:32.00MiB, Used:16.00KiB (0.05%)
   /dev/sda5	  32.00MiB

Unallocated:
   /dev/sda5	  32.98GiB
```

Dessa vez, foi o tamanho dos arquivos (`Used`) que não mudou: continuou os mesmos 59 GB.

Mas o espaço em disco alocado para armazenar os arquivos (`Device allocated`) reduziu bastante: de 77 GB para 67 GB. Agora, a proporção é de 88%, indicando que os blocos estão, na média, mais preenchidos. Ou seja, a alocação de espaço em disco melhorou.

## Conclusão

Considerando que agora eu tinha 67 GB alocados em uma partição de 100 GB, eu consegui reduzir essa partição para 70 GB (deixando ainda algum espaço livre) e criar uma partição nova de 30 GB para hospedar a nova distribuição.

Espero que essas dicas possam ser úteis para você caso também precise liberar espaço no seu sistema de arquivos BTRFS.

Se precisar, confira mais informações nos textos que consultei, listo-os a seguir.

Ou escreva sua dúvida em um comentário. Até a próxima!

## Referências

- [BTRFS and free space - emergency response • Oh The Huge Manatee!][ohthehugemanatee]
- [\[HowTo\] get Space on BTRFS - Tutorials - Manjaro Linux Forum][manjaro-forum]
- [btrfs - No space left on device \| Support \| SUSE][suse]
- [System recovery and snapshot management with Snapper][Snapper]
- [opensuse - Clear all Snapper snapshots - Unix &amp; Linux Stack Exchange][stackexchange]

[baobab]:               https://apps.gnome.org/pt-BR/Baobab/
[Linux Kamarada]:       {% post_url pt/2024-05-27-linux-kamarada-15-5-mais-alinhado-com-o-opensuse-leap-e-com-outras-distribuicoes %}
[GNOME]:                https://br.gnome.org/
[Linux]:                http://www.vivaolinux.com.br/linux/
[Gparted]:              https://gparted.org/
[diolinux]:             https://diolinux.com.br/editorial/btrfs-o-sistema-de-arquivos-do-futuro.html
[openSUSE Leap]:        {% post_url pt/2024-06-12-leap-15-6-revela-opcoes-para-usuarios %}
[df]:                   https://man7.org/linux/man-pages/man1/df.1.html
[btrfs]:                https://man7.org/linux/man-pages/man8/btrfs.8.html
[grep]:                 https://man7.org/linux/man-pages/man1/grep.1.html
[Windows]:              https://www.microsoft.com/pt-br/windows/
[ponto de restauração]: https://tecnoblog.net/responde/como-restaurar-windows-10-ou-windows-7/
[openSUSE]:             https://www.opensuse.org/
[Snapper]:              https://doc.opensuse.org/documentation/leap/reference/html/book-reference/cha-snapper.html#sec-snapper-manage-delete
[ohthehugemanatee]:     https://ohthehugemanatee.org/blog/2019/02/11/btrfs-out-of-space-emergency-response/
[manjaro-forum]:        https://forum.manjaro.org/t/howto-get-space-on-btrfs/145130
[suse]:                 https://www.suse.com/pt-br/support/kb/doc/?id=000018807
[stackexchange]:        https://unix.stackexchange.com/a/196196