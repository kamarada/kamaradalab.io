---
date: '2022-12-19 23:00:00 GMT-3'
image: '/files/2022/12/15.4-rc.jpg'
layout: post
published: true
nickname: 'kamarada-15.4-rc'
title: 'Linux Kamarada libera versão candidata a lançamento (15.4 RC)'
---

{% include image.html src='/files/2022/12/15.4-rc.jpg' %}

O Projeto Linux Kamarada anuncia o lançamento da versão 15.4 RC da [distribuição Linux][linux] de mesmo nome, baseada no [openSUSE Leap 15.4][leap-15.4]. Ela já está disponível para [download].

<!--more-->

A página [Download] foi atualizada e agora oferece principalmente duas versões para _download_:

- a versão [15.3 Final][kamarada-15.3], que você pode instalar no computador de casa ou do trabalho; e
- a versão 15.4 RC, que você pode testar, se quiser ajudar no desenvolvimento.

Lembre-se que a forma mais fácil de testar o Linux é usando uma máquina virtual do [VirtualBox]. Para mais informações sobre como fazer isso, leia:

- [VirtualBox: a forma mais fácil de conhecer o Linux sem precisar instalá-lo][virtualbox]

Mas, se você quiser testar o Linux Kamarada no seu próprio computador, a forma mais fácil de fazer isso é usando um _pendrive_ inicializável que você pode criar com o [Ventoy]:

- [Ventoy: crie um pendrive multiboot simplesmente copiando imagens ISO para ele][ventoy]

Se você deseja instalar o Linux Kamarada no computador para usar no dia-a-dia, o recomendado é que você instale a versão 15.3 Final, que já está pronta, e depois, quando a versão 15.4 estiver pronta, atualize. Para mais informações sobre a versão 15.3 Final, leia:

- [Linux Kamarada 15.3: ano novo, distribuição nova][kamarada-15.3]

## O que é versão candidata a lançamento?

Uma versão **candidata a lançamento** (do inglês [_release candidate_][rc], por isso abreviada **RC**) é uma versão de um _software_ que está praticamente pronto para ser lançado no mercado. Essa versão pode se tornar a versão final (estável) do _software_, a não ser que algum defeito (_bug_) sério seja percebido a tempo de ser corrigido antes do lançamento final. Nesse estágio do desenvolvimento, todas as funcionalidades inicialmente planejadas já estão presentes e nenhum recurso novo é adicionado.

Uma versão RC pode ser considerada um tipo de versão [beta]. Portanto, _bugs_ são esperados. Se você encontrar um _bug_, por favor, me avise por um dos canais listados na página [Ajuda].

Claro, _bugs_ podem ser encontrados e corrigidos a qualquer momento, mas quanto antes, melhor!

## Para onde foi a versão 15.4 Beta?

O openSUSE Leap 15.4 foi lançado em [junho][leap-15.4] e desde então já recebeu diversas atualizações e correções. A versão anterior, [15.3][leap-15.3], lançada em [junho do ano passado][leap-15.3], terá seu suporte encerrado com a virada do ano, de acordo com o [_site_ de notícias do openSUSE][opensuse-news]. Com isso, o Linux Kamarada 15.3, baseado no openSUSE Leap 15.3, também deixará de receber suporte.

Infelizmente, não consegui lançar o Linux Kamarada 15.4 com mais antecedência. Como o prazo está apertado, e também porque nessa versão as novidades serão mais por conta das atualizações trazidas pelos próprios aplicativos (do que por mudanças na seleção do _software_), decidi pular a versão 15.4 Beta e lançar logo a versão 15.4 RC. Até porque o openSUSE Leap 15.4 já apresenta uma estabilidade considerável e o mesmo é esperado do Linux Kamarada 15.4, mesmo na fase RC.

Pretendo lançar a versão 15.4 Final nessa semana ou na próxima, de modo que os usuários ainda tenham algum prazo para atualizar da versão 15.3, com esta ainda recebendo suporte oficial do [Projeto openSUSE].

## Novidades

As novidades da versão 15.4 se resumem a:

- os aplicativos da versão 15.3 foram mantidos em sua maioria, porém atualizados, em novas versões, com novas funcionalidades e _bugs_ corrigidos;

- a atualização mais notável foi da área de trabalho [GNOME], que no [Linux Kamarada 15.2][kamarada-15.2] e 15.3 estava na versão 3.34, e agora está na versão 41.2;

- as extensões do GNOME, que antes eram configuradas por meio do aplicativo **[Ajustes]**, agora são configuradas por meio do novo aplicativo **[Extensões]**;

- o aplicativo **[Jogos]** deixou de ser um dos principais aplicativos do GNOME (_[GNOME Core Apps]_) e está sendo [renomeado] para **[Highscore]**, ele não foi empacotado para o openSUSE Leap 15.4 e, portanto, também foi removido do Linux Kamarada 15.4 (ainda é possível instalar o aplicativo **Jogos** por meio do [Flathub], se desejado);

- o aplicativo **[Mapas]** foi incluído no Linux Kamarada 15.4;

- o aplicativo de desenho **[KolourPaint]** (que, embora fosse mais apropriado para a área de trabalho [KDE], também funcionava no GNOME) foi substituído pelo **[Drawing]**, que é um aplicativo do círculo do GNOME (_[GNOME Circle App]_);

- o Linux Kamarada 15.4 traz novos belos papéis de parede (ainda é possível usar os antigos ou o padrão do openSUSE, se você preferir); e

- o tema [GTK] usado na versão 15.3, o tema [Materia], não tem recebido mais atualizações, por isso foi substituído pelo tema [Orchis], que inclusive é baseado no Materia e mantém o alinhamento com o [Material Design] do [Google] (o mesmo _design_ do [Android]).

## Atualizando para o 15.4 RC

Se você já usa o Linux Kamarada 15.3 e confia na sua experiência como usuário, pode querer atualizar para o Linux Kamarada 15.4 RC para ajudar a testar o processo de atualização. Em linhas gerais, deve ser suficiente seguir esse tutorial de atualização, substituindo `15.3` por `15.4`, onde houver:

- [Linux Kamarada e openSUSE Leap: como atualizar da versão 15.2 para a 15.3][upgrade-howto]

Como o Linux Kamarada 15.3 já configurava os repositórios com a variável `$releasever`, deve ser bem fácil atualizar para a versão 15.4. Exemplos de comandos do tutorial acima, já adaptados para a nova versão:

```
# zypper --releasever=15.4 ref
# zypper --releasever=15.4 dup --download-only --allow-vendor-change
```

## Ficha técnica

Para que seja possível comparar esta versão com a anterior e as futuras, segue um resumo do _software_ contido no Linux Kamarada 15.4 RC Build 4.2:

- _kernel_ Linux 5.14.21
- servidor gráfico X.Org 1.20.3 (sem Wayland)
- área de trabalho GNOME 41.2 acompanhada de seus principais aplicativos (_core apps_), como Arquivos (antigo Nautilus), Calculadora, Terminal, Editor de texto (gedit), Captura de tela e outros
- suíte de aplicativos de escritório LibreOffice 7.3.6.2
- navegador Mozilla Firefox 102.6.0 ESR (navegador padrão)
- navegador Chromium 108 (navegador alternativo disponível)
- reprodutor multimídia VLC 3.0.17
- cliente de _e-mail_ Evolution 3.42.4
- centro de controle do YaST
- Brasero 3.12.3
- CUPS 2.2.7
- Drawing 1.0.1
- Firewalld 0.9.3
- GParted 1.3.1
- HPLIP 3.21.10
- Java (OpenJDK) 11.0.17
- KeePassXC 2.7.4
- Linphone 4.3.2
- PDFsam Basic 4.3.4
- Pidgin 2.14.8
- Python 2.7.18 e 3.6.15
- Samba 4.15.8
- Tor 0.4.7
- Transmission 3.00
- Vim 9.0
- Wine 7.0
- instalador Calamares 3.2.36
- Flatpak 1.12.5
- jogos: Aisleriot (Paciência), Copas, Iagno (Reversi), Mahjongg, Minas (Campo minado), Nibbles (Cobras), Quadrapassel (Tetris), Sudoku, Xadrez

Essa lista não é exaustiva, mas já dá uma noção do que se encontra na distribuição.

[linux]:            https://www.vivaolinux.com.br/linux/
[leap-15.4]:        {% post_url pt/2022-06-08-leap-15-4-oferece-novos-recursos-com-a-estabilidade-de-sempre %}
[download]:         /pt/download
[kamarada-15.3]:    {% post_url pt/2021-12-30-linux-kamarada-15-3-ano-novo-distribuicao-nova %}
[virtualbox]:       {% post_url pt/2019-10-08-virtualbox-a-forma-mais-facil-de-conhecer-o-linux-sem-precisar-instala-lo %}
[ventoy]:           {% post_url pt/2020-07-29-ventoy-crie-pendrives-multiboot-simplesmente-copiando-imagens-iso-para-ele %}
[rc]:               https://pt.wikipedia.org/wiki/Ciclo_de_vida_de_libera%C3%A7%C3%A3o_de_software#Release_candidate
[beta]:             https://pt.wikipedia.org/wiki/Ciclo_de_vida_de_libera%C3%A7%C3%A3o_de_software#Beta
[Ajuda]:            /pt/ajuda
[leap-15.3]:        {% post_url pt/2021-06-02-opensuse-leap-153-constroi-um-caminho-para-a-versao-empresarial %}
[opensuse-news]:    https://news.opensuse.org/2022/12/12/leap-153-to-reach-eol/
[Projeto openSUSE]: https://www.opensuse.org/
[GNOME]:            https://br.gnome.org/
[kamarada-15.2]:    {% post_url pt/2020-09-11-linux-kamarada-15.2-venha-para-o-lado-verde-elegante-e-moderno-da-forca %}
[Ajustes]:          https://wiki.gnome.org/Apps/Tweaks
[Extensões]:        https://apps.gnome.org/pt-BR/app/org.gnome.Extensions/
[Jogos]:            https://wiki.gnome.org/Apps/Games
[GNOME Core Apps]:  https://apps.gnome.org/pt-BR/#core
[renomeado]:        https://gitlab.gnome.org/Archive/gnome-games/-/issues/243
[Highscore]:        https://gitlab.gnome.org/World/highscore
[Flathub]:          https://flathub.org/apps/details/org.gnome.Games
[Mapas]:            https://apps.gnome.org/pt-BR/app/org.gnome.Maps/
[KolourPaint]:      https://apps.kde.org/pt-br/kolourpaint/
[KDE]:              https://kde.org/pt-br/
[Drawing]:          https://apps.gnome.org/pt-BR/app/com.github.maoschanz.drawing/
[GNOME Circle App]: https://apps.gnome.org/pt-BR/#circle
[GTK]:              https://www.gtk.org/
[Materia]:          https://github.com/nana-4/materia-theme
[Orchis]:           https://github.com/vinceliuice/Orchis-theme
[Material Design]:  https://material.io/
[Google]:           https://www.google.com.br/
[Android]:          https://www.android.com/
[upgrade-howto]:    {% post_url pt/2021-12-23-linux-kamarada-e-opensuse-leap-como-atualizar-da-versao-152-para-a-153 %}
