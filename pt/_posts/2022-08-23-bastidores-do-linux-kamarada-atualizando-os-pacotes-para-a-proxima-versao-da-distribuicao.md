---
date: '2022-08-23 21:45:00 GMT-3'
layout: post
published: true
title: 'Bastidores do Linux Kamarada: atualizando os pacotes para a próxima versão da distribuição'
image: /files/2021/04/kick-off.jpg
nickname: 'making-of-updating'
excerpt: 'O openSUSE Leap 15.4 foi lançado em junho, enquanto o Linux Kamarada está na versão 15.3, lançada em dezembro do ano passado. Ao mesmo tempo em que inicio o desenvolvimento da nova versão, venho retomar a série &quot;bastidores do Linux Kamarada&quot;, em que tento compartilhar como é feito o desenvolvimento da distribuição.'
---

{% include image.html src="/files/2021/04/kick-off.jpg" %}

O [openSUSE Leap 15.4][ol-154] foi lançado em [junho][ol-154], enquanto o [Linux Kamarada][lk-153] está na versão [15.3][lk-153], lançada em [dezembro][lk-153] do ano passado. Ao mesmo tempo em que inicio o desenvolvimento da nova versão, venho retomar a série "bastidores do Linux Kamarada", em que tento compartilhar como é feito o desenvolvimento da distribuição. Por enquanto, essa série já conta com dois artigos:

- [Como fazer uma distro baseada no openSUSE (bastidores do Linux Kamarada): começando][bastidores-1]
- [Criando pacotes RPM no Open Build Service (OBS)][bastidores-2]

Se você ainda não leu os artigos anteriores, recomendo que comece por eles.

No [primeiro artigo][bastidores-1], vimos como iniciar o desenvolvimento da distribuição criando e configurando o repositório no [Open Build Service (OBS)][obs] e criando o primeiro pacote já [integrando o OBS com o GitLab][obs-gitlab]. No [segundo artigo][bastidores-2], vimos como criar um pacote diretamente no OBS, sem usar o [GitLab]. Na época em que escrevi esses dois artigos, estava desenvolvendo o Linux Kamarada 15.3.

Note que em ambos os artigos eu iniciei o desenvolvimento dos pacotes do zero, para mostrar como é feito. Mas uma vez que eu já fiz o Linux Kamarada 15.3, posso reaproveitar muito do meu trabalho anterior para o Linux Kamarada 15.4, não preciso começar tudo do zero de novo.

No OBS, podemos fazer isso criando _branches_ (ramificações) dos pacotes.

Se você já usa [Git], essa palavra certamente lhe soará familiar. Mas o conceito de _branch_ do OBS na verdade é análogo ao conceito de _[fork]_ do Git.

Normalmente, no OBS (assim como no Git) usamos _branches_ (ou _forks_) para contribuir com outros projetos: acessamos um pacote (repositório), criamos um _branch_ (_fork_) dele em um espaço nosso, fazemos alterações e enviamos essas alterações de volta para o pacote (repositório) original.

Para iniciar uma nova versão de um pacote em um novo repositório, faremos um uso criativo dos _branches_ no OBS. Sem mais conversa, vamos colocar a mão na massa.

Na verdade, antes, só mais uma observação: para o Linux Kamarada 15.4, hoje o nosso ponto de partida é o projeto de desenvolvimento (**[home:kamarada:15.4:dev]**), criado seguindo as instruções do [primeiro artigo][bastidores-1].

## Criando branches no OBS

No [primeiro artigo][bastidores-1], vimos como criar o pacote **[patterns-kamarada-gnome]** do Linux Kamarada 15.3 do zero. Hoje, vamos fazer diferente: vamos fazer um _branch_ daquele pacote para o projeto do Linux Kamarada 15.4, de modo a reaproveitar o trabalho anterior.

Comece acessando o pacote no projeto da versão anterior (no meu caso, o pacote [**patterns-kamarada-gnome**][patterns-kamarada-gnome-lk153] no projeto **[home:kamarada:15.3:dev]**) e, na barra de ações à esquerda, clique em **Branch Package** (criar um _branch_ do pacote):

{% include image.html src="/files/2022/08/bastidores-01.jpg" %}

Expanda **More options** (mais opções) e informe o nome do projeto (`home:kamarada:15.4:dev`) e o nome do pacote (`patterns-kamarada-gnome`) para o _branch_:

{% include image.html src="/files/2022/08/bastidores-02.jpg" %}

Marque a opção **Stay on this revision** (permaneça nessa revisão) para que o OBS não atualize o _branch_ caso haja atualizações no pacote original.

Marque também a opção **Disable Autocleanup** (desabilitar limpeza automática) para que o OBS não exclua o _branch_ caso fique um tempo sem receber alterações.

Ao final, clique no botão **Branch**.

O pacote [**patterns-kamarada-gnome**][patterns-kamarada-gnome-lk154] é criado no projeto **home:kamarada:15.4:dev**.

Edite o arquivo `_service` e atualize o parâmetro `revision`:

{% include image.html src="/files/2022/08/bastidores-03.jpg" %}

Note como você pode fazer isso pela própria interface _web_ do OBS. Se você não lembra do arquivo `_service`, consulte um dos tutoriais a seguir:

- [Integrando Open Build Service com GitLab][obs-gitlab]
- [Integrando Open Build Service com GitHub][obs-github]

## Desfazendo a ligação com o pacote original

Como eu disse, estamos fazendo um uso criativo dos _branches_ do OBS para reaproveitar o trabalho que tivemos com a versão anterior do pacote. Não queremos contribuir alterações de volta pro pacote original, tampouco receber atualizações dele. Sendo assim, vamos desfazer a ligação do _branch_ com o pacote original, tornando o novo pacote independente.

Volte para a página do projeto, que lista os pacotes, e note que os pacotes que são _branches_ são sinalizados com **Link**:

{% include image.html src="/files/2022/08/bastidores-04.jpg" %}

Abra o pacote em questão e note que também a página dele mostra que ele é um _branch_ (**Links to**, tem ligação com):

{% include image.html src="/files/2022/08/bastidores-05.jpg" %}

Clique em **show unmerged sources** (mostrar código não mesclado) para mostrar apenas o código do _branch_, sem misturar com o código do pacote original.

Com isso, aparece o arquivo `_link`, que guarda informações sobre a ligação:

{% include image.html src="/files/2022/08/bastidores-06.jpg" %}

Exclua esse arquivo e note que a ligação com o pacote original desaparece tanto na página do pacote:

{% include image.html src="/files/2022/08/bastidores-07.jpg" %}

Como na página do projeto, que lista os pacotes:

{% include image.html src="/files/2022/08/bastidores-08.jpg" %}

## Finalizando

Termine de configurar a integração do OBS com o GitLab ou o [GitHub].

E lembre-se também de atualizar os arquivos _spec_ e _changes_.

Feito isso, está tudo pronto para que você possa trabalhar na próxima versão deste pacote.

Agora é repetir esse procedimento para todos os demais pacotes que você pretende atualizar de uma versão para a outra da distribuição.

[ol-154]:                           {% post_url pt/2022-06-08-leap-15-4-oferece-novos-recursos-com-a-estabilidade-de-sempre %}
[lk-153]:                           {% post_url pt/2021-12-30-linux-kamarada-15-3-ano-novo-distribuicao-nova %}
[bastidores-1]:                     {% post_url pt/2021-04-12-como-fazer-uma-distro-baseada-no-opensuse-bastidores-do-linux-kamarada-comecando %}
[bastidores-2]:                     {% post_url pt/2021-08-15-criando-pacotes-rpm-no-open-build-service-obs %}
[obs]:                              https://openbuildservice.org/
[obs-gitlab]:                       {% post_url pt/2021-03-15-integrando-open-build-service-com-gitlab %}
[GitLab]:                           https://gitlab.com/
[Git]:                              https://git-scm.com/
[fork]:                             https://git-scm.com/book/pt-br/v2/GitHub-Contribuindo-em-um-projeto
[home:kamarada:15.4:dev]:           https://build.opensuse.org/project/show/home:kamarada:15.4:dev
[patterns-kamarada-gnome]:          https://software.opensuse.org/package/patterns-kamarada-gnome
[patterns-kamarada-gnome-lk153]:    https://build.opensuse.org/package/show/home:kamarada:15.3:dev/patterns-kamarada-gnome
[home:kamarada:15.3:dev]:           https://build.opensuse.org/project/show/home:kamarada:15.3:dev
[patterns-kamarada-gnome-lk154]:    https://build.opensuse.org/package/show/home:kamarada:15.4:dev/patterns-kamarada-gnome
[obs-github]:                       {% post_url pt/2019-07-23-integrando-open-build-service-com-github %}
[GitHub]:                           https://github.com/
