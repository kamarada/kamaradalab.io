---
date: 2021-05-11 23:59:00 GMT-3
image: '/files/2021/05/im-signal-pt.jpg'
layout: post
nickname: 'signal'
title: 'O mensageiro Signal'
---

{% include image.html src='/files/2021/05/im-signal-pt.jpg' %}

O [Signal] é um [mensageiro instantâneo] simples, mas poderoso e seguro. Oferece um conjunto de funcionalidades semelhante aos dos seus concorrentes [WhatsApp] e [Telegram]: o Signal permite o envio de mensagens de texto e de voz, assim como fotos, vídeos e arquivos, em conversas com contatos ou em grupo, além de oferecer chamadas de voz e de vídeo. Mas uma diferença importante ao usar o Signal é que privacidade não é opcional — cada mensagem, cada chamada entre usuários do Signal é criptografada de ponta a ponta.

Além disso, a versão para Android do Signal também pode enviar e receber [mensagens SMS/MMS][sms] e substituir o aplicativo de SMS que vem de fábrica no sistema. Usado dessa forma, o Signal permite enviar mensagens não criptografadas para quem não usa Signal, assim como receber mensagens não criptografadas de quem não usa Signal. Observe que as mensagens SMS trafegam pela rede da sua operadora e não pela Internet, e não são criptografadas. O Signal vai te avisar quando mensagens forem enviadas sem criptografia.

O [_site_ do Signal][signal] ostenta indicações de especialistas em privacidade como [Edward Snowden]:

<blockquote class="twitter-tweet" data-lang="pt"><p lang="en" dir="ltr">I use Signal every day. <a href="https://twitter.com/hashtag/notesforFBI?src=hash&amp;ref_src=twsrc%5Etfw">#notesforFBI</a> (Spoiler: they already know) <a href="https://t.co/KNy0xppsN0">https://t.co/KNy0xppsN0</a></p>&mdash; Edward Snowden (@Snowden) <a href="https://twitter.com/Snowden/status/661313394906161152?ref_src=twsrc%5Etfw">2 de novembro de 2015</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Quando a polêmica do WhatsApp estourou em janeiro, ninguém menos que o bilionário da tecnologia [Elon Musk] tuitou em apoio ao Signal:

<blockquote class="twitter-tweet" data-lang="pt"><p lang="en" dir="ltr">Use Signal</p>&mdash; Elon Musk (@elonmusk) <a href="https://twitter.com/elonmusk/status/1347165127036977153?ref_src=twsrc%5Etfw">7 de janeiro de 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Entre 12 e 14 de janeiro de 2021, o contador de instalações do Signal no [Google Play] aumentou de mais de 10 milhões para mais de 50 milhões.

Os servidores do Signal armazenam quase nenhum metadado do usuário. Em 2016, o serviço recebeu uma [intimação] exigindo informações sobre dois usuários para uma investigação federal, à qual o serviço respondeu: "a única informação que podemos produzir em resposta a uma requisição como essa é a data e a hora em que um usuário se cadastrou no Signal e a última data em que um usuário se conectou ao serviço do Signal".

Mensagens, imagens, arquivos e outros conteúdos não são armazenados nos servidores do Signal por muito tempo. Eles são irrevogavelmente excluídos do servidor assim que são entregues ao destinatário. Com isso, suas mensagens e contatos permanecem armazenados apenas localmente no seu dispositivo. Diferente do WhatsApp e do Telegram, o Signal não oferece _backup_ na nuvem. O que você pode fazer é um [_backup_ local][backup], que é protegido por uma senha longa. Verifique onde o _backup_ é armazenado e certifique-se de não excluí-lo. Copiá-lo para outro lugar (como um computador ou um HD externo) é uma boa ideia.

O Signal tem um recurso semelhante à autodestruição de mensagens do Telegram, que são as [mensagens efêmeras], que desaparecem depois de um certo tempo. Isso garante a privacidade da conversa, mesmo que outra pessoa consiga acesso ao seu telefone.

{% include update.html date="18/05/2021" message="Assim como o Telegram, o Signal também permite [bloquear a tela](https://support.signal.org/hc/pt-br/articles/360007059572-Bloqueio-de-tela), mas com base nos recursos de segurança configurados no próprio sistema do _smartphone_, seja uma senha ou impressão digital. Esse recurso está disponível apenas nos aplicativos móveis. No momento, o Signal para _desktop_ não suporta o bloqueio de tela." %}

Também como o WhatsApp e o Telegram, o Signal requer um número de telefone válido para que você se cadastre e encontre contatos, o que é a principal crítica ao aplicativo. Deve-se observar, no entanto, que os contatos do Signal são armazenados apenas localmente.

O Signal é totalmente código-aberto: os códigos-fonte de todos os clientes do Signal e do servidor do Signal estão disponíveis no [GitHub]. O cliente para Android é feito com [compilação determinística]. Ao menos na teoria, você pode [ter seu próprio servidor do Signal (_self-hosted_)][quora], mas isso parece muito trabalhoso, já que demandaria fazer vários ajustes no código e compilar o servidor e os clientes. Isso seria, na verdade, um clone (_fork_) do Signal.

O protocolo do Signal foi [auditado profissionalmente] contra vulnerabilidades de segurança. É considerado o estado da arte em criptografia de ponta-a-ponta e é usado até mesmo por outros mensageiros nos bastidores, como [WhatsApp][signal-whatsapp], [Facebook Messenger] e [Skype].

O Signal nasceu em 2013 como resultado da fusão de dois aplicativos para comunicação criptografada, o RedPhone e o TextSecure, ambos desenvolvidos pelo especialista em criptografia e ativista de privacidade [Moxie Marlinspike], da Open Whisper Systems. Em 2017, Brian Acton, cofundador do WhatsApp, deixou o Facebook devido a divergências inconciliáveis sobre as práticas de privacidade. No ano seguinte, Acton e Marlinspike formaram uma parceria para criar a [Signal Foundation], uma ONG com sede nos Estados Unidos. A fundação recebeu um financiamento inicial de 50 milhões de dólares de Acton.

Agora, a Signal Foundation funciona inteiramente por doações, que ajudam a pagar a infraestrutura e a equipe de funcionários. O Signal é gratuito e não exibe anúncios.

Você pode usar o Signal no computador, mas deve instalá-lo primeiro no celular.

O aplicativo do Signal para Android pode ser obtido do [Google Play].

No openSUSE, o aplicativo do Signal pode ser instalado pelo pacote semi-oficial **[signal-desktop]**. Mas eu, pessoalmente, achei mais fácil instalá-lo a partir do [Flathub]:

```
# flatpak install org.signal.Signal
```

Também existem versões do Signal para outros sistemas. Veja mais opções em: [signal.org](https://www.signal.org/pt_BR/download/).

Há uma seção de [Suporte] muito completa no _site_ do Signal.

Esse texto faz parte da série:

- [Aplicativos de mensagens livres e focados em privacidade alternativos ao WhatsApp][mensageiro instantâneo]

[signal]:                       https://www.signal.org/pt_BR/
[mensageiro instantâneo]:       {% post_url pt/2021-05-08-aplicativos-de-mensagens-livres-e-focados-em-privacidade-alternativos-ao-whatsapp %}
[whatsapp]:                     https://www.whatsapp.com/
[telegram]:                     {% post_url pt/2021-05-09-o-mensageiro-telegram %}
[sms]:                          https://support.signal.org/hc/pt-br/articles/360007321171-Posso-usar-Signal-para-enviar-SMS-MMS-
[edward snowden]:               https://pt.wikipedia.org/wiki/Edward_Snowden
[elon musk]:                    https://pt.wikipedia.org/wiki/Elon_Musk
[google play]:                  https://play.google.com/store/apps/details?id=org.thoughtcrime.securesms
[intimação]:                    https://signal.org/bigbrother/eastern-virginia-grand-jury/
[backup]:                       https://support.signal.org/hc/pt-br/articles/360007059752-Backup-e-recuperação-de-mensagens
[mensagens efêmeras]:           https://support.signal.org/hc/pt-br/articles/360007320771-Definir-e-gerenciar-as-mensagens-efêmeras
[github]:                       https://github.com/signalapp
[compilação determinística]:    https://github.com/signalapp/Signal-Android/tree/master/reproducible-builds
[quora]:                        https://www.quora.com/Can-you-run-your-own-signal-messaging-server/answer/Aleksey-Yavorskiy
[auditado profissionalmente]:   https://eprint.iacr.org/2016/1013.pdf
[signal-whatsapp]:              https://signal.org/blog/whatsapp-complete/
[facebook messenger]:           https://signal.org/blog/facebook-messenger/
[skype]:                        https://signal.org/blog/skype-partnership/
[moxie marlinspike]:            https://www.newyorker.com/magazine/2020/10/26/taking-back-our-privacy
[signal foundation]:            https://signal.org/blog/signal-foundation/
[signal-desktop]:               https://software.opensuse.org/package/signal-desktop
[flathub]:                      https://flathub.org/apps/details/org.signal.Signal
[suporte]:                      https://support.signal.org/hc/pt-br
