---
date: 2021-05-09 01:00:00 GMT-3
image: '/files/2021/05/im-telegram-pt.jpg'
layout: post
nickname: 'telegram'
title: 'O mensageiro Telegram'
---

{% include image.html src='/files/2021/05/im-telegram-pt.jpg' %}

O [Telegram] é um [aplicativo de mensagens][messaging-apps] com foco em velocidade e segurança. É o concorrente mais próximo do WhatsApp em termos de funcionalidades e popularidade. Permite enviar mensagens de texto, de voz e de vídeo, assim como fotos, vídeos e arquivos de qualquer tipo, além de oferecer chamadas de voz e de vídeo. Em janeiro, ultrapassou [500 milhões de usuários ativos por mês][durov-1] e é um dos [10 aplicativos mais baixados do mundo][10-most-downloaded]. O Telegram é conhecido por seus grupos capazes de comportar até 200.000 membros. Também tem os [canais][telegram-channels], que permitem o envio de mensagens para pessoas que se inscrevam para recebê-las, semelhante a uma _newsletter_ por _e-mail_ ou um _feed_ RSS.

Você pode usar o Telegram em vários dispositivos — computadores, _smartphones_ e _tablets_ — ao mesmo tempo, suas mensagens serão sincronizadas entre eles. Você pode até usar o Telegram pelo navegador sem que nenhum dispositivo esteja conectado à Internet. Mas isso só é possível porque as mensagens são armazenadas em servidores do Telegram, que só usa criptografia de ponta-a-ponta por padrão para chamadas de voz e de vídeo. Para mensagens de texto, a criptografia de ponta-a-ponta precisa ser ativada manualmente.

Para enviar mensagens criptografadas para um contato, você precisa iniciar um [chat secreto][secret-chats], que só fica armazenado e disponível nos dispositivos móveis de origem e destino. Também é possível definir a [autodestruição de mensagens][self-destructing-messages] em uma conversa.

{% include update.html date="18/05/2021" message="O Telegram oferece a possibilidade de [bloquear o aplicativo com senha](https://canaltech.com.br/apps/como-bloquear-o-telegram-com-senha/) (ou também impressão digital, nos celulares com leitor biométrico), adicionando uma camada extra de segurança. Observe que essa configuração não é sincronizada entre os dispositivos e precisa ser feita em cada dispositivo separadamente." %}

Você precisa de um número de telefone para começar a usar o Telegram. Mas o serviço oferece uma possibilidade interessante se você não quiser passar seu número para outras pessoas que é a criação de um [nome de usuário][telegram-username]. Com isso, outros usuários podem contatar você por esse nome, mesmo que não saibam seu número de telefone. O aplicativo não revela seu número para quem entrar em contato com você pelo seu nome de usuário (a menos que você permita isso em suas configurações de privacidade).

Os aplicativos clientes do Telegram possuem todos [código aberto][telegram-clients-source] e são feitos com [compilação determinística][telegram-reproducible-builds]. O código-fonte do servidor, no entanto, [não é aberto][telegram-faq-1]. Também não é possível [ter um servidor próprio (_self-hosted_)][telegram-faq-2] do Telegram. Um dos criadores do Telegram, Pavel Durov, [explica][durov-2] que não abre o código-fonte do servidor porque isso não traria benefícios adicionais em termos de segurança e porque teme que o código poderia ser usado por um regime autoritário para criar seu próprio _fork_ do Telegram.

O protocolo do Telegram foi [formalmente verificado][telegram-verified] como criptograficamente correto.

O Telegram foi criado em 2013 pelos irmãos russos Nikolai e Pavel Durov. A contribuição de Nikolai é tecnológica, enquanto Pavel apoia o Telegram financeira e ideologicamente. Em 2018, eles deixaram a Rússia devido a mudanças nas regulamentações locais de TI e desde então já passaram por alguns lugares incluindo Berlim, Londres e Singapura. Hoje, a equipe de desenvolvimento do Telegram está sediada em Dubai, mas [afirma][telegram-faq-3] que pode se mudar novamente se as regulamentações locais mudarem.

[Afirma][telegram-faq-4] também que até hoje não foram compartilhados quaisquer dados de usuários com terceiros, incluindo governos. O que provavelmente é verdade, já que o Telegram é polêmico por ser [o mensageiro usado pelo Estado Islâmico][isis]. Isso impulsionou ainda mais o debate sobre até que ponto os _apps_ de mensagens deveriam cooperar com a polícia ou manter os dados dos usuários em total sigilo. A equipe do Telegram tem proibido conteúdo (robôs e canais públicos) desses grupos terroristas sempre que os descobre ou recebe [denúncias][telegram-isis-watch].

O Telegram é completamente gratuito e tem sido [financiado][telegram-faq-5] pelo fundador e CEO Pavel Durov com recursos próprios. Ele [explicou][durov-3] que tem planos para começar a monetizar o Telegram em 2021 para pagar a infraestrutura e os salários dos desenvolvedores. As funcionalidades que já são gratuitas devem permanecer gratuitas, não serão exibidos anúncios, nem dados dos usuários serão vendidos. Em vez disso, funcionalidades opcionais serão adicionadas e pagará por elas quem as quiser utilizar.

No Brasil, o Telegram se tornou conhecido devido às [suspensões judiciais do WhatsApp][whatsapp-justica], durante as quais as pessoas recorreram ao Telegram como alternativa.

Dentre as opções da [lista de aplicativos de mensagens livres][messaging-apps], o Telegram pode não ser o melhor aplicativo no quesito privacidade. Porém, é um mensageiro que simplesmente funciona, é prático e já é melhor que o WhatsApp em vários aspectos. Para muitos usuários, a conveniência de não precisar fazer _backup_, mais a possibilidade dos _chats_ secretos para conversas que precisam de maior proteção, pode ser uma barganha que vale a pena.

No [openSUSE], o aplicativo do Telegram pode ser obtido do repositório da distribuição:

```
# zypper in telegram-desktop
```

No caso do [openSUSE Leap][leap-tumbleweed], a versão nesse repositório pode estar desatualizada. Se você usa o [FlatPak], outra opção é instalar o Telegram a partir do [Flathub][telegram-flathub]:

```
# flatpak install org.telegram.desktop
```

No Android, o Telegram pode ser obtido do [próprio _site_][telegram-apk] (como APK) ou do [Google Play][telegram-google-play].

Para usar o Telegram pelo navegador, sem instalar aplicativo, acesse: [web.telegram.org](https://web.telegram.org/).

Também existem versões do Telegram para outros sistemas, disponíveis em: [telegram.org](https://telegram.org/apps).

O Telegram possui uma página de [perguntas frequentes][telegram-faq] muito explicativa no seu _site_.

Se você usa o Telegram, se inscreva no canal [@LinuxKamaradaNoticias](https://t.me/LinuxKamaradaNoticias) para receber avisos de novos textos, ou participe do grupo de usuários [@LinuxKamarada](https://t.me/LinuxKamarada), onde, além de receber os mesmos avisos, você pode conversar com outras pessoas que usam o [Linux Kamarada][kamarada-15.2].

Esse texto faz parte da série:

- [Aplicativos de mensagens livres e focados em privacidade alternativos ao WhatsApp][messaging-apps]

[telegram]:                     https://telegram.org/
[messaging-apps]:               {% post_url pt/2021-05-08-aplicativos-de-mensagens-livres-e-focados-em-privacidade-alternativos-ao-whatsapp %}
[durov-1]:                      https://t.me/durov/147
[10-most-downloaded]:           https://canaltech.com.br/apps/telegram-ultrapassa-tiktok-e-whatsapp-como-app-mais-baixado-em-janeiro-de-2021-178599/
[telegram-channels]:            https://telegram.org/blog/channels
[secret-chats]:                 https://tecnoblog.net/294208/como-funcionam-os-chats-secretos-do-telegram/
[self-destructing-messages]:    https://www.techtudo.com.br/dicas-e-tutoriais/2021/02/mensagem-autodestrutiva-no-telegram-como-usar-funcao-que-apaga-conversas.ghtml
[telegram-username]:            https://telegram.org/faq#nomes-de-usuario-e-t-me
[telegram-clients-source]:      https://telegram.org/apps#source-code
[telegram-reproducible-builds]: https://core.telegram.org/reproducible-builds
[telegram-faq-1]:               https://telegram.org/faq?setln=pt-br#p-posso-ter-o-codigo-fonte-do-servidor-do-telegram
[telegram-faq-2]:               https://telegram.org/faq?setln=pt-br#p-eu-posso-ter-um-servidor-proprio-do-telegram
[durov-2]:                      https://t.me/durovschat/515221
[telegram-verified]:            https://arxiv.org/abs/2012.03141
[telegram-faq-3]:               https://telegram.org/faq?setln=pt-br#p-onde-fica-a-sede-do-telegram
[telegram-faq-4]:               https://telegram.org/faq?setln=pt-br#p-voces-processam-solicitacoes-de-dados
[isis]:                         https://www.bbc.com/portuguese/noticias/2015/10/151026_telegram_aplicativo_estado_islamico_rb
[telegram-isis-watch]:          https://t.me/isiswatch/2
[telegram-faq-5]:               https://telegram.org/faq?setln=pt-br#p-como-voces-ganharao-dinheiro
[durov-3]:                      https://t.me/durov/142
[whatsapp-justica]:             https://pt.wikipedia.org/wiki/Bloqueio_do_WhatsApp_no_Brasil
[opensuse]:                     https://www.opensuse.org/
[leap-tumbleweed]:              {% post_url pt/2020-12-06-opensuse-leap-e-opensuse-tumbleweed-qual-e-a-diferenca %}
[flatpak]:                      https://flatpak.org/
[telegram-flathub]:             https://www.flathub.org/apps/details/org.telegram.desktop
[telegram-apk]:                 https://telegram.org/android
[telegram-google-play]:         https://play.google.com/store/apps/details?id=org.telegram.messenger
[telegram-faq]:                 https://telegram.org/faq
[kamarada-15.2]:                {% post_url pt/2020-09-11-linux-kamarada-15.2-venha-para-o-lado-verde-elegante-e-moderno-da-forca %}
