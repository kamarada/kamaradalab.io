---
date: '2024-09-02 19:00:00 GMT-3'
image: '/files/2024/09/tor-browser-pt.jpg'
layout: post
nickname: 'tor-browser'
title: 'O que é VPN e a forma mais fácil de usá-la no Linux: por meio do Navegador Tor'
excerpt: 'Entenda o que é VPN, porque usá-la, conheça a Rede Tor e veja como instalar e usar o Navegador Tor no Linux.'
---

{% include image.html src='/files/2024/09/tor-browser-pt.jpg' %}

A busca por VPNs [cresceu recentemente][poder360] no Brasil, segundo dados do [Google Trends]. Mas afinal, o que é VPN, e por que alguém iria quer usar isso?

A sigla **[VPN]** vem do inglês _Virtual Private Network_, que quer dizer **Rede Privada Virtual**. É uma tecnologia que estabelece uma conexão segura e criptografada sobre uma rede pública, menos segura, como a Internet. Ela serve para proteger seus dados e sua privacidade enquanto você navega, garantindo que suas atividades _online_ permaneçam seguras e confidenciais.

A VPN funciona assim: dois computadores pertencem, a princípio, a redes diferentes (eles podem estar em prédios, cidades ou até mesmo países diferentes, por exemplo), mas ambos estão conectados à Internet. Então, eles estabelecem na rede mundial um circuito criptografado por onde trocam informações de forma privada. Seria como uma ligação telefônica que não pudesse ser grampeada.

{% include image.html src='/files/2024/09/vpn-01.jpg' caption='Apesar de estarem conectados à Internet, dois computadores em uma VPN se comunicam de forma privada' %}

Para quem usa esses computadores, é imperceptível que eles estão distantes. É como se eles estivessem conectados diretamente por um cabo, apesar de esse cabo não existir de verdade (por isso, essa rede é dita **virtual**). E a comunicação, apesar de ocorrer pela Internet, não pode ser lida pelos demais computadores da rede, porque esses dois computadores usam criptografia para se comunicar (por isso, rede **privada**). Porque a informação trafega por esse circuito criptografado que não pode ser interceptado, a VPN também é conhecida como **túnel**.

{% include image.html src='/files/2024/09/vpn-02.png' caption='Um túnel é bem assim: só quem está dentro dele vê o que por ele passa' %}

Um uso prático de VPNs é burlar censuras a _sites_. Normalmente, quando acessamos um _site_, nossa comunicação com o servidor desse _site_ é roteada pela infraestrutura de rede da operadora, que pode estar bloqueando o acesso ao _site_ (por ordem do governo, por exemplo). Tem VPNs que permitem que o computador na outra ponta, que pode estar em outro país, faça o trabalho do roteador da operadora, então conseguimos, assim, acessar o _site_ como se estivéssemos em outro país.

{% include image.html src='/files/2024/09/vpn-03.png' %}

Essas VPNs são usadas sobretudo em [países com governos autoritários][china] em que os cidadãos são proibidos de ter acesso a _sites_ ou aplicativos considerados impróprios pelo ditador no poder. É preocupante que a busca por VPNs esteja crescendo no Brasil, assim como a menção ao nosso país na [Declaração de Westminster], assinada por vários ativistas defensores da [liberdade de expressão], incluindo [Julian Assange], [Edward Snowden], [Glenn Greenwald] e [Jordan Peterson].

Se você sente necessidade de usar uma VPN, quero te apresentar hoje uma que é baseada em [_software_ livre][free-sw] e que pode ser usada gratuitamente por qualquer pessoa em qualquer lugar do mundo.

{% include image.html src='/files/2024/09/tor.svg' %}

O **[Projeto Tor]** (sigla de _The Onion Router_, "o roteador cebola", em uma tradução livre) mantém uma rede de túneis ao redor do mundo -- a **Rede Tor** -- pela qual trafegam dados de seus usuários de forma anônima, criptografada, privada, segura e livre de censura. A menção à cebola vem do fato de que a informação dentro dessa rede circula de forma compartimentada. A Rede Tor emprega várias camadas de criptografia para garantir que cada computador no meio do caminho só saiba o suficiente para levar a informação adiante até o próximo:

{% include image.html src='/files/2024/09/how-tor-works.png' caption='Fonte da imagem: [manual do Navegador Tor](https://tb-manual.torproject.org/pt-BR/about/)' %}

A forma mais fácil de se conectar à Rede Tor e usá-la no [Linux] é por meio do **[Navegador Tor]** (_Tor Browser_), que é uma versão do [Mozilla Firefox] modificada para trafegar dados somente dentro da Rede Tor. Ao usar o Navegador Tor, você navega sem ser identificado ou rastreado, para os _sites_ seu endereço IP aparece diferente, como se estivesse em outro país.

A seguir, você verá como instalar e usar o Navegador Tor no Linux. Como referência, vou usar a distribuição [Linux Kamarada 15.5], baseada no [openSUSE Leap]. Se você usa outra distribuição, apenas a forma de instalar deve ser diferente. Para mais informações, consulte a documentação da sua distribuição.

{% capture outros_sos %}

Se, por um acaso, você procurava instruções para outro sistema, mas caiu nesta página, consulte versões deste mesmo tutorial para outros sistemas:

- Android: [O que é VPN e a forma mais fácil de usá-la no Android: por meio do app Orbot - Antônio Medeiros](https://antoniomedeiros.dev/blog/2024/09/03/o-que-e-vpn-e-a-forma-mais-facil-de-usa-la-no-android-por-meio-do-app-orbot/)
- Windows: [O que é VPN e a forma mais fácil de usá-la no Windows: por meio do Navegador Tor - Antônio Medeiros](https://antoniomedeiros.dev/blog/2024/09/03/o-que-e-vpn-e-a-forma-mais-facil-de-usa-la-no-windows-por-meio-do-navegador-tor/)
- iOS (iPhone e iPad): [O que é VPN e a forma mais fácil de usá-la no iOS: por meio do app Orbot - Antônio Medeiros](https://antoniomedeiros.dev/blog/2024/09/03/o-que-e-vpn-e-a-forma-mais-facil-de-usa-la-no-ios-por-meio-do-app-orbot/)

{% endcapture %}

{% include update.html date="03/09/2024" message=outros_sos %}

## Instalando o Navegador Tor

Você pode instalar o Navegador Tor a partir dos [repositórios oficiais do openSUSE][repos] de duas formas: pela interface gráfica, usando a instalação com 1 clique (*1-Click Install*), ou pelo terminal, usando o gerenciador de pacotes **[zypper]**. Escolha a que prefere.

Para instalar o Navegador Tor usando a instalação com 1 clique, clique no botão abaixo:

<p class='text-center'>
    <a class='btn btn-primary' href='/downloads/tor-browser.ymp'>
        <i class='fas fa-bolt'></i> Instalação com 1 clique
    </a>
</p>

Para instalar o Navegador Tor usando o terminal, execute o comando a seguir:

```
# zypper in torbrowser-launcher torbrowser-launcher-lang
```

## Iniciando o Navegador Tor

Para iniciar o Navegador Tor, se você usa a área de trabalho [GNOME] (padrão do Linux Kamarada), clique em **Atividades**, no canto superior esquerdo da tela, digite `tor` e clique no ícone do navegador:

{% include image.html src='/files/2024/09/tor-browser-01-pt.jpg' %}

O pacote que baixamos e instalamos contém, na verdade, um _script_ que baixa e instala o Navegador Tor, assim como baixa e instala eventuais atualizações, para que você esteja usando sempre a versão mais recente do Navegador Tor. No primeiro uso, ele pode demorar um pouco baixando o navegador:

{% include image.html src='/files/2024/09/tor-browser-02-pt.jpg' %}

Essa é a tela inicial do Navegador Tor, ainda não conectado à Rede Tor e, portanto, ainda não pronto para uso:

{% include image.html src='/files/2024/09/tor-browser-03-pt.jpg' %}

Clique em **Conectar**. Aguarde a conexão com a Rede Tor ser estabelecida:

{% include image.html src='/files/2024/09/tor-browser-04-pt.jpg' %}

A tela seguinte indica que o Navegador Tor está conectado à Rede Tor e, portanto, pronto para ser usado:

{% include image.html src='/files/2024/09/tor-browser-05-pt.jpg' %}

## Testando a conexão com a Rede Tor

Sempre antes de começar a usar o Navegador Tor, convém testar se ele está de fato conectado à Rede Tor. Para isso, acesse:

- <https://check.torproject.org/>

{% include image.html src='/files/2024/09/tor-browser-06-pt.jpg' %}

A página deve informar: **"Parabéns. Este navegador está configurado para usar Tor."** (ou o equivalente a isso em inglês, como na imagem) Se essa mensagem aparece para você, já é seguro usar o Navegador Tor para acessar os _sites_ que você precisa acessar.

Se aparecer uma mensagem diferente dessa, o Navegador Tor não está conectado adequadamente à Rede Tor e, portanto, não está pronto para uso. Navegar assim é inseguro. Para mais informações sobre o que pode ser feito para solucionar isso, consulte o [manual] ou o [suporte] do Navegador Tor.

## Leitura adicional recomendada

Conhecimento é poder. E o que você acabou de ler neste artigo é o tipo de conhecimento que você precisa ter antes que precise usar de fato. Recomendo que você estude o material dos _sites_ a seguir, que podem te ajudar a navegar com mais segurança e privacidade e menos vigilância e censura:

- [Security in-a-box - ferramentas e táticas de segurança digital](https://securityinabox.org/pt/)
- [Privacidade.Digital – Criptografia Contra a Vigilância Global em Massa](https://www.privacidade.digital/)
- [Guia de Autodefesa contra Vigilância da Electronic Frontier Foundation (EFF)](https://ssd.eff.org/pt-br/)

[poder360]:                     https://www.poder360.com.br/poder-justica/buscas-por-vpn-no-google-disparam-com-decisao-de-moraes/
[Google Trends]:                https://trends.google.com.br/trends/explore?date=2024-08-25%202024-09-01&geo=BR&q=vpn&hl=pt
[VPN]:                          https://pt.wikipedia.org/wiki/Rede_privada_virtual
[china]:                        http://tecnologia.terra.com.br/internet/pai-da-censura-na-china-e-surpreendido-burlando-a-mesma-em-publico,1d6ba1d9b58468d4df30fb84daf29ee6e3c193t0.html
[Declaração de Westminster]:    https://westminsterdeclaration.org/portugues
[liberdade de expressão]:       {% post_url pt/2021-10-03-software-livre-e-liberdade-de-expressao %}
[Julian Assange]:               https://pt.wikipedia.org/wiki/Julian_Assange
[Edward Snowden]:               https://pt.wikipedia.org/wiki/Edward_Snowden
[Glenn Greenwald]:              https://pt.wikipedia.org/wiki/Glenn_Greenwald
[Jordan Peterson]:              https://www.jordanbpeterson.com/about/
[free-sw]:                      https://www.gnu.org/philosophy/free-sw.pt-br.html
[Projeto Tor]:                  https://www.torproject.org/
[Linux]:                        https://www.vivaolinux.com.br/linux/
[Navegador Tor]:                https://www.torproject.org/download/
[Mozilla Firefox]:              {% post_url pt/2019-07-05-18-aplicativos-que-voce-pode-usar-do-mesmo-jeito-no-linux-e-no-windows-parte-1 %}#1-mozilla-firefox
[Linux Kamarada 15.5]:          {% post_url pt/2024-05-27-linux-kamarada-15-5-mais-alinhado-com-o-opensuse-leap-e-com-outras-distribuicoes %}
[openSUSE Leap]:                {% post_url pt/2020-12-06-opensuse-leap-e-opensuse-tumbleweed-qual-e-a-diferenca %}
[repos]:                        https://en.opensuse.org/Package_repositories#Official_Repositories
[zypper]:                       https://en.opensuse.org/Portal:Zypper
[GNOME]:                        https://br.gnome.org/
[manual]:                       https://tb-manual.torproject.org/pt-BR/running-tor-browser/
[suporte]:                      https://support.torproject.org/pt-BR/
