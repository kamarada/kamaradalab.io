---
date: '2023-11-12 11:40:00 GMT-3'
image: '/files/2023/11/openSUSE-repos-Leap-05-pt.jpg'
layout: post
nickname: 'openSUSE-repos-Leap'
title: 'Instale o novo pacote openSUSE-repos-Leap'
excerpt: 'Nunca mais se preocupe com a configuração dos repositórios oficiais com essa novidade do openSUSE Leap 15.5 (e do Linux Kamarada 15.5).'
---

Se você já usa o [openSUSE Leap] há alguns anos e sempre [atualiza de uma versão para outra][upgrade], como eu, deve ter percebido que a configuração dos [repositórios oficiais] já mudou algumas vezes. A mudança mais recente foi [a migração para a nova CDN][cdn], em julho. Tudo bem que não é algo que acontece com frequência: normalmente, [uma nova versão do openSUSE Leap é lançada uma vez por ano][lifetime], e é durante o procedimento de atualização que aproveitamos para fazer ajustes nessa configuração. Ainda assim, não deixa de ser uma preocupação para os usuários, que devem se atentar às mudanças.

Pensando em simplificar a configuração desses repositórios, especialmente quando há mudanças nessa configuração, o [Projeto openSUSE] desenvolveu o pacote **[openSUSE-repos-Leap]**, que traz em si a configuração dos repositórios oficiais da distribuição. O usuário não precisa mais se preocupar em verificar quais são os endereços dos repositórios oficiais e adicioná-los um a um: basta instalar esse pacote, que todos os repositórios são adicionados por ele. Se é necessário mexer nessa configuração, o Projeto openSUSE lança uma atualização para esse pacote e o usuário só precisa [atualizar os pacotes do sistema][update] (como de costume) para receber e passar a usar a nova configuração.

Essa é uma novidade do [openSUSE Leap 15.5] -- e, por tabela, do [Linux Kamarada 15.5], que é baseado nele. Se você utiliza uma versão anterior da distribuição, deve [atualizar até a versão 15.5][upgrade] para poder se beneficiar dessa novidade.

A instalação do pacote **openSUSE-repos-Leap** não é obrigatória para poder usar o openSUSE Leap 15.5 ou o Linux Kamarada 15.5, mas é recomendada.

Se você começou a usar o Linux Kamarada na versão 15.5, saiba que o pacote **openSUSE-repos-Leap** já vem instalado por padrão.

Veja a seguir como conferir se o pacote **openSUSE-repos-Leap** está instalado no seu sistema. As capturas de tela a seguir mostram o Linux Kamarada 15.5, mas as telas do openSUSE Leap 15.5 são semelhantes.

Abra o [Centro de Controle do YaST][yast]. Para isso, clique em **Mostrar aplicativos**, no canto inferior esquerdo da tela, e clique no ícone do **YaST**, no final da lista de aplicativos:

{% include image.html src='/files/2023/11/openSUSE-repos-Leap-01-pt.jpg' %}

Será solicitada a você a senha do usuário *root*. Digite-a e clique em **Continuar**:

{% include image.html src='/files/2023/11/openSUSE-repos-Leap-02-pt.jpg' %}

Dentro da categoria **Software**, a primeira, clique no item **Repositórios de software**:

{% include image.html src="/files/2022/12/upgrade-to-15.4-06-pt.jpg" %}

Você será apresentado à lista de repositórios do seu sistema:

{% include image.html src='/files/2023/11/openSUSE-repos-Leap-03-pt.jpg' %}

Perceba que os repositórios oficiais do openSUSE aparecem listados, porém a coluna **Serviço** está vazia para todos eles. Se no seu computador aparece assim também, provavelmente o pacote **openSUSE-repos-Leap** não está instalado.

Para instalá-lo, feche a janela **Repositórios de software** e, de volta à tela principal do YaST, clique em **Gerenciamento de software**. Pesquise pelo pacote **openSUSE-repos-Leap**, marque-o para instalação e clique em **Aceitar**:

{% include image.html src='/files/2023/11/openSUSE-repos-Leap-04-pt.jpg' %}

Quando a instalação terminar, clique em **Concluir**, fechando, assim, a janela **Gerenciamento de software**. De volta à tela principal do YaST, abra novamente o módulo **Repositórios de software**.

{% include image.html src='/files/2023/11/openSUSE-repos-Leap-05-pt.jpg' %}

Perceba que agora os repositórios oficiais do openSUSE são gerenciados pelo **Serviço** chamado **openSUSE**. Com isso, você não precisa mais se preocupar com a configuração deles.

Só não se esqueça de regularmente [atualizar o seu sistema][update] e se divertir bastante (o _slogan_ do openSUSE: _have a lot of fun_), mas isso de certo você já faz.

Se quiser ler mais sobre o pacote **openSUSE-repos-Leap** em fontes oficiais:

- [Leap 15.5 Reaches Beta Phase - openSUSE News][leap-beta]
- [Try out the new CDN with openSUSE-repos - openSUSE News][cdn]
- [openSUSE/openSUSE-repos - GitHub][openSUSE-repos]

[openSUSE Leap]:            {% post_url pt/2020-12-06-opensuse-leap-e-opensuse-tumbleweed-qual-e-a-diferenca %}
[upgrade]:                  {% post_url pt/2023-11-26-linux-kamarada-15-4-e-opensuse-leap-15-4-como-atualizar-para-a-versao-15-5 %}
[repositórios oficiais]:    https://en.opensuse.org/Package_repositories#Official_Repositories
[cdn]:                      https://news.opensuse.org/2023/07/31/try-out-cdn-with-opensuse-repos/
[lifetime]:                 https://en.opensuse.org/Lifetime
[Projeto openSUSE]:         https://www.opensuse.org/
[openSUSE-repos-Leap]:      https://software.opensuse.org/package/openSUSE-repos-Leap
[update]:                   {% post_url pt/2019-05-14-como-obter-atualizacoes-para-o-linux-opensuse %}
[openSUSE Leap 15.5]:       {% post_url pt/2023-06-07-lancamento-do-opensuse-leap-15-5-amadurece-a-distribuicao-e-estabelece-transicao-tecnologica %}
[Linux Kamarada 15.5]:      {% post_url pt/2023-11-01-linux-kamarada-15-5-beta-ajude-a-testar-a-melhor-versao-da-distribuicao %}
[yast]:                     http://yast.opensuse.org/
[leap-beta]:                https://news.opensuse.org/2023/02/21/leap-reaches-beta-phase/
[openSUSE-repos]:           https://github.com/openSUSE/openSUSE-repos
