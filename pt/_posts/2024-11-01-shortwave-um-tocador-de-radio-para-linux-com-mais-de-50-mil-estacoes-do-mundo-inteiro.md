---
date: '2024-11-01 20:30:00 GMT-3'
image: '/files/2024/11/shortwave-pt.jpg'
layout: post
nickname: 'shortwave'
title: 'Shortwave: um tocador de rádio para Linux com mais de 50 mil estações do mundo inteiro'
---

Você gosta de ouvir rádio? É um meio de comunicação que aos poucos perde espaço para as plataformas de _streaming_, como [Spotify], [Amazon Music], [Apple Music], [YouTube Music] e [tantas outras]. No entanto, [a rádio ainda é um meio de comunicação importante][valor], especialmente para o entretenimento e a informação. Praticamente todo carro tem um aparelho de som que sintoniza rádio, e cada vez mais pessoas escutam rádio pela Internet.

Existe um aplicativo para [Linux] que te permite escutar rádio e hoje você vai conhecê-lo.

O **[Shortwave]** é um tocador de rádio pela Internet que te permite escutar mais de 50 mil rádios dos quatro cantos do mundo. Ele possui uma interface bastante simples, leve, intuitiva e também responsiva, ou seja, se adapta a diferentes tamanhos de tela (por exemplo, ele deve funcionar bem no [PinePhone]). É um [_software_ livre][free-sw] e seu código-fonte está disponível no [GitLab do Projeto GNOME][gitlab].

{% include image.html src='/files/2024/11/shortwave-pt.jpg' %}

Mostrarei a seguir como instalar e usar o Shortwave no Linux. Usarei a distribuição [Linux Kamarada 15.5] como referência.

## Instalando o Shortwave

A forma [recomendada][gitlab] de instalar o Shortwave é por meio do [Flatpak]. Clique no botão abaixo para abrir o aplicativo **Programas** e instalar o Shortwave:

<p class='text-center'>
    <a class='btn btn-primary' href='https://dl.flathub.org/repo/appstream/de.haeckerfelix.Shortwave.flatpakref'>
        <i class="fa-solid fa-bag-shopping"></i> Instalar com Flatpak
    </a>
</p>

Você também pode abrir o aplicativo **Programas**, pesquisar pelo Shortwave e instalá-lo por lá:

{% include image.html src='/files/2024/11/shortwave-01-pt.jpg' %}

Ou ainda, se prefere o terminal, pode instalar o Shortwave executando:

```
$ flatpak install de.haeckerfelix.Shortwave
```

Se você não sabe o que é Flatpak ou precisa de ajuda com isso, consulte:

- [Flatpak: tudo o que você precisa saber sobre o gerenciador de pacotes independente de distribuição][Flatpak]

Logo após a instalação, você já deve ser capaz de iniciar o Shortwave.

## Iniciando o Shortwave

Para iniciar o Shortwave, se você usa a área de trabalho [GNOME] (padrão do Linux Kamarada), clique em **Atividades**, no canto superior esquerdo da tela, digite `shortwave` e clique no ícone correspondente:

{% include image.html src='/files/2024/11/shortwave-02-pt.jpg' %}

Essa é a tela inicial do Shortwave, por enquanto sem nenhuma estação de rádio:

{% include image.html src='/files/2024/11/shortwave-03-pt.jpg' %}

## Escutando estações de rádio

Clique no botão **Adicionar novas estações** (você também pode usar o botão no canto superior esquerdo da tela).

Você pode escutar uma das **Estações populares** sugeridas ou **Pesquisar estações**:

{% include image.html src='/files/2024/11/shortwave-04-pt.jpg' %}

Digite o nome de uma rádio, cidade, estado, país ou qualquer palavra para procurar estações:

{% include image.html src='/files/2024/11/shortwave-05-pt.jpg' %}

Clique em uma estação e você verá mais informações sobre ela:

{% include image.html src='/files/2024/11/shortwave-06-pt.jpg' %}

Clique em **Reproduzir** para ouvir essa estação.

Você pode voltar à tela anterior clicando no botão **Mostrar detalhes da estação**:

{% include image.html src='/files/2024/11/shortwave-07-pt.jpg' %}

Clique em **Adicionar à biblioteca** se quiser que essa estação apareça na tela inicial do Shortwave.

Essa é a tela inicial do Shortwave após a adição de algumas estações de rádio:

{% include image.html src='/files/2024/11/shortwave-08-pt.jpg' %}

## Adicionando uma estação não listada

As estações de rádio listadas pelo Shortwave são obtidas de um banco de dados aberto, o [RadioBrowser], que pode ser alimentado por qualquer pessoa (à semelhança da [Wikipedia]). Qualquer um pode adicionar uma rádio a esse banco de dados, possibilitando que outras pessoas conheçam essa rádio.

Mas você não precisa se limitar a esse banco de dados, caso conheça uma rádio que não está catalogada nele.

Para adicioná-la à biblioteca do Shortwave e, assim, poder ouvi-la, abra o menu no canto superior esquerdo da tela e clique em **Adicionar estação local**:

{% include image.html src='/files/2024/11/shortwave-09-pt.jpg' %}

Informe o **Nome** e a **URL de reprodução** da estação de rádio:

{% include image.html src='/files/2024/11/shortwave-10-pt.jpg' %}

Como exemplo, vou adicionar a [Rádio Novo Tempo de Florianópolis][novotempo], que, no momento em que escrevo, não está catalogada no RadioBrowser:

{% include image.html src='/files/2024/11/shortwave-11-pt.jpg' %}

Como eu descobri a URL de reprodução? "Hackeando" (inspecionando) o [_site_ da rádio][novotempo]. Conhecimentos de programação foram necessários. Mas eu poderia também ter entrado em contato com a rádio e perguntado essa informação.

Se tudo der certo e você conseguir ouvir a rádio, considere cadastrá-la no [RadioBrowser], para que outras pessoas possam encontrá-la facilmente pelo Shortwave ou [outros aplicativos que usam esse mesmo banco de dados][radio-browser-apps].

## Integração com o GNOME

Por fazer parte do [círculo de aplicativos do GNOME][gnome-circle], o Shortwave se integra perfeitamente a essa área de trabalho. Por exemplo, você pode controlá-lo a partir da lista de notificações:

{% include image.html src='/files/2024/11/shortwave-12-pt.jpg' %}

## Integração com o Chromecast

Da mesma forma que o [Google Chrome][Chromecast] e o [VLC], o Shortwave também transmite para o [Chromecast], permitindo que você escute estações de rádio na TV.

Para trasmitir para o Chromecast, clique no botão **Conectar dispositivo**:

{% include image.html src='/files/2024/11/shortwave-13-pt.jpg' %}

Seu dispositivo Chromecast deve aparecer listado, clique nele:

{% include image.html src='/files/2024/11/shortwave-14-pt.jpg' %}

Se a conexão foi bem sucedida, o nome do dispositivo Chromecast é mostrado na tela inicial do Shortwave e você pode controlá-lo por ela:

{% include image.html src='/files/2024/11/shortwave-15-pt.jpg' %}

{% include image.html src='/files/2024/11/shortwave-16-pt.jpg' %}

Se algo não funcionou como o esperado, é possível que você tenha que liberar portas no _firewall_ do computador para permitir a comunicação com o Chromecast. Para mais informações, consulte:

- [Transmitindo do Linux para a TV com Chromecast][Chromecast]

[Spotify]:              {% post_url pt/2019-07-05-18-aplicativos-que-voce-pode-usar-do-mesmo-jeito-no-linux-e-no-windows-parte-1 %}#8-spotify
[Amazon Music]:         https://music.amazon.com/
[Apple Music]:          https://music.apple.com/br/
[YouTube Music]:        https://music.youtube.com/
[tantas outras]:        https://en.wikipedia.org/wiki/Comparison_of_music_streaming_services
[valor]:                https://valor.globo.com/patrocinado/dino/noticia/2023/05/08/radio-se-mantem-forte-como-meio-de-midia-aponta-pesquisa.ghtml
[Linux]:                https://www.vivaolinux.com.br/linux/
[Shortwave]:            https://apps.gnome.org/pt-BR/Shortwave/
[PinePhone]:            {% post_url pt/2020-10-14-pinephone-o-smartphone-que-roda-linux-saiba-tudo-sobre-ele %}
[free-sw]:              https://www.gnu.org/philosophy/free-sw.pt-br.html
[gitlab]:               https://gitlab.gnome.org/World/Shortwave
[Linux Kamarada 15.5]:  {% post_url pt/2024-05-27-linux-kamarada-15-5-mais-alinhado-com-o-opensuse-leap-e-com-outras-distribuicoes %}
[Flatpak]:              {% post_url pt/2022-01-17-flatpak-tudo-o-que-voce-precisa-saber-sobre-o-gerenciador-de-pacotes-independente-de-distribuicao %}
[GNOME]:                https://br.gnome.org/
[RadioBrowser]:         https://www.radio-browser.info/
[Wikipedia]:            https://pt.wikipedia.org/
[novotempo]:            https://www.novotempo.com/radioflorianopolis/
[radio-browser-apps]:   https://www.radio-browser.info/users
[gnome-circle]:         https://apps.gnome.org/pt-BR/#circle
[Chromecast]:           {% post_url pt/2019-03-26-transmitindo-do-linux-para-a-tv-com-chromecast %}
[VLC]:                  {% post_url pt/2020-04-03-transmitindo-do-vlc-para-a-tv-com-o-chromecast %}
