---
date: '2024-08-22 04:50:00 GMT-3'
image: '/files/2024/08/arduino-uno.jpg'
layout: post
nickname: 'arduino'
title: 'Arduino IDE: como instalar e dar os primeiros passos no Arduino'
excerpt: 'Veja como instalar o Arduino IDE no Linux e usá-lo para dar seus primeiros passos com o Arduino. Como referência, usarei a placa Arduino UNO R3 SMD, que é a mais usada e documentada de toda a família Arduino, indicada para quem está iniciando na eletrônica e mexendo com essa plataforma pela primeira vez.'
---

A cultura do "faça você mesmo" (_do it yourself_, DIY) tem crescido com a facilidade de encontrar tutoriais na Internet sobre como fazer praticamente qualquer coisa. Cada vez mais pessoas tem se aventurado a fabricar suas próprias roupas, móveis, programas de computador e até mesmo aparelhos.

Nas áreas de computação e eletrônica, o "faça você mesmo" é possibilitado por tecnologias como o [Raspberry Pi] e o [Arduino].

Já falamos [aqui][Raspberry Pi] do Raspberry Pi, mas ele é um computador inteiro. Pode até ser usado como _desktop_ para tarefas cotidianas, como navegar na Internet ou redigir um trabalho escolar. Dependendo do que se queira fazer, ele pode ser demais. Seria como matar uma barata com um tiro de canhão.

Hoje, vamos falar do Arduino!

[**Arduino**][wikipedia] é uma plataforma programável de prototipagem eletrônica [baseada][guide] em _hardware_ e _software_ fáceis de usar e [livres][free-sw] (de [código aberto][oss]). Há [diversos modelos de placas][placas] com microcontroladores e pinos que podem ser usados para ler entradas -- um dedo que aperta um botão, algo que se aproxima ou uma mensagem do [X/Twitter] -- e transformá-las em saídas -- acender um LED, ligar um motor, postar algo em uma rede social.

{% include image.html src='/files/2024/08/arduino-uno.jpg' caption='A placa Arduino UNO R3 SMD' %}

O Arduino foi [criado na Itália][ieee] em 2005, no Interaction Design Institute Ivrea, por um grupo de pesquisadores que queriam oferecer a estudantes um dispositivo que fosse ao mesmo tempo fácil de programar e barato.

Hoje, no Brasil, você pode comprar uma placa Arduino a [20 ou 30 reais][placas]. Existe [_kit_ de aprendizado][kit], que vem com a placa e diversos componentes para serem usados em conjunto, e [curso _online_ gratuito][curso].

A programação do Arduino é feita usando a [linguagem de programação do Arduino][programming] (que faz lembrar as linguagens [C/C++]) e um ambiente de desenvolvimento integrado (do inglês _Integrated Development Environment_, [IDE]), mais especificamente o oficial do Arduino, chamado de **[Arduino IDE]**.

Você verá a seguir como instalar o Arduino IDE no [Linux] e usá-lo para dar seus primeiros passos com o Arduino. Como referência, vou usar a distribuição [Linux Kamarada 15.5] e a placa [Arduino UNO R3 SMD][uno], que é a placa mais usada e documentada de toda a família Arduino, indicada para quem está iniciando na eletrônica e mexendo com essa plataforma pela primeira vez.

Não é o objetivo deste tutorial te ensinar a programar o Arduino. Aqui, vamos apenas garantir que as ferramentas necessárias para isso estejam funcionando no seu computador. Você pode encontrar explicações mais detalhadas sobre a programação do Arduino em cursos como [esse][curso].

**Curiosidade:** o nome Arduino vem de um bar onde alguns dos fundadores do projeto costumavam se reunir. O nome do bar, por sua vez, faz menção a [Arduíno de Ivrea], que foi rei da Itália de 1002 a 1014.

## Baixando e instalando o Arduino IDE

Para baixar o Arduino IDE, acesse o _site_ oficial do Arduino e clique em **Software**:

- <https://www.arduino.cc/>

{% include image.html src='/files/2024/08/arduino-ide-01.jpg' %}

Na página seguinte, clique no _link_ do arquivo ZIP (_ZIP file_) para Linux:

{% include image.html src='/files/2024/08/arduino-ide-02.jpg' %}

Opcionalmente, você pode fazer uma doação em dinheiro para o Projeto Arduino. Ou apenas baixar o Arduino IDE clicando em **Just Download**:

{% include image.html src='/files/2024/08/arduino-ide-03.jpg' %}

Também opcionalmente você pode se inscrever para receber _e-mails_ com novidades do Arduino. Se não quiser, basta clicar em **Just Download** (apenas baixar):

{% include image.html src='/files/2024/08/arduino-ide-04.jpg' %}

O _download_ do arquivo ZIP será iniciado automaticamente.

No momento em que escrevo, o Arduino IDE está na [versão 2.3.2][arduino-ide-releases] e o arquivo ZIP é chamado `arduino-ide_2.3.2_Linux_64bit.zip`.

O navegador deve baixá-lo para a pasta **Downloads**. Quando o _download_ terminar, abra essa pasta, clique com o botão direito do _mouse_ no arquivo ZIP e clique na opção **Extrair aqui**:

{% include image.html src='/files/2024/08/arduino-ide-05-pt.jpg' %}

O conteúdo do arquivo ZIP é extraído para uma pasta com o mesmo nome dele. Mova essa pasta para algum local que seja fácil de você lembrar.

Por exemplo, eu criei uma pasta `programas` dentro da minha pasta pessoal (`/home/vinicius/`) e movi a pasta do Arduino IDE (`arduino-ide_2.3.2_Linux_64bit`) para lá. Portanto, o caminho para o Arduino IDE no meu computador ficou sendo:

`/home/vinicius/programas/arduino-ide_2.3.2_Linux_64bit`

{% include image.html src='/files/2024/08/arduino-ide-06-pt.jpg' %}

Vamos facilitar iniciar o Arduino IDE pelo menu **Atividades**. Para isso, vamos criar um arquivo `.desktop` para ele.

Abra o aplicativo **Editor de texto** ([gedit]), copie e cole o seguinte conteúdo:

```
[Desktop Entry]
Name=Arduino IDE
Exec=/home/vinicius/programas/arduino-ide_2.3.2_Linux_64bit/arduino-ide
Icon=/home/vinicius/programas/arduino-ide_2.3.2_Linux_64bit/resources/app/resources/icons/512x512.png
Type=Application
```

(ajuste os caminhos para os arquivos conforme o seu caso)

Salve esse arquivo em `~/.local/share/applications` com o nome de `arduino-ide.desktop`.

Ainda precisamos fazer mais uma [configuração]. Para isso, abra uma janela de terminal e mude para usuário administrador (_root_):

```
$ su
```

Para permitir que o Arduino IDE acesse a porta serial e carregue o código para sua placa, execute:

```
# echo 'SUBSYSTEMS=="usb", ATTRS{idVendor}=="2341", GROUP="plugdev", MODE="0666"' > /etc/udev/rules.d/99-arduino.rules
```

Por fim, reinicie seu computador para que as alterações tenham efeito.

## Iniciando o Arduino IDE

Para iniciar o Arduino IDE, abra o menu **Atividades**, no canto superior esquerdo da tela, comece a digitar `arduino` e clique no ícone do IDE:

{% include image.html src='/files/2024/08/arduino-ide-07-pt.jpg' %}

Eis a tela do Arduino IDE:

{% include image.html src='/files/2024/08/arduino-ide-08.jpg' %}

## Traduzindo a interface

O Arduino IDE vem em inglês por padrão, mas ele possui uma tradução para o português brasileiro.

Para aplicá-la, abra o menu **File** (Arquivo) e clique em **Preferences** (Preferências):

{% include image.html src='/files/2024/08/arduino-ide-09.jpg' %}

Em **Language** (Linguagem do Editor), selecione **português (Brasil)**:

{% include image.html src='/files/2024/08/arduino-ide-10.jpg' %}

Clique em **OK**.

A interface é recarregada e traduzida.

Note que o código com comentários em inglês não é traduzido.

## Programando o Arduino

Para ter uma ideia de como fazer para programar o Arduino, vamos usar um exemplo de programa que já vem com o Arduino IDE, que é o _[Blink]_ (piscar).

Se você já tem alguma experiência com programação, sabe que toda linguagem de programação tem um exemplo didático chamado de "[olá, mundo]" (_hello, world_), que é o programa mais simples que se pode escrever com aquela linguagem de programação.

O programa _Blink_ mostra a coisa mais simples que você pode fazer com um Arduino, que é fazer seu LED embutido piscar.

Após iniciar o Arduino IDE, conecte a placa Arduino ao computador usando o cabo USB que a acompanha.

Informe para o Arduino IDE qual é o modelo da sua placa indo no menu **Ferramentas > Placa > Arduino AVR Boards > Arduino Uno**:

{% include image.html src='/files/2024/08/arduino-ide-11-pt.jpg' %}

Em seguida, vá no menu **Ferramentas > Porta** e informe para o Arduino IDE em qual porta sua placa está conectada (no meu caso, `/dev/ttyUSB0`):

{% include image.html src='/files/2024/08/arduino-ide-12-pt.jpg' %}

Para abrir o programa _Blink_, vá em **Arquivo > Exemplos > 01. Basics > Blink**:

{% include image.html src='/files/2024/08/arduino-ide-13-pt.jpg' %}

O IDE abre uma nova janela com o código do programa de exemplo:

{% include image.html src='/files/2024/08/arduino-ide-14-pt.jpg' %}

Normalmente, começaríamos a programação do Arduino escrevendo um programa. Pulamos essa etapa carregando esse exemplo.

Após digitar (ou, nesse caso, abrir) o programa, e antes de carregá-lo para a placa, é uma boa prática verificá-lo. Para isso, clique no botão **Verificar**:

{% include image.html src='/files/2024/08/arduino-ide-15-pt.jpg' %}

O Arduino IDE percorre todo o código e verifica se há erros. Se ele encontra algum erro, o aponta. Senão, compila o programa. Você pode usar isso para detectar eventuais erros no seu código e corrigi-los antes de carregá-lo de fato para a placa.

Como é um programa de exemplo do próprio Arduino IDE, não deve ter erros.

{% include image.html src='/files/2024/08/arduino-ide-16-pt.jpg' %}

O próximo passo é clicar no botão **Carregar**:

{% include image.html src='/files/2024/08/arduino-ide-17-pt.jpg' %}

Com isso, o programa é carregado para a placa:

{% include image.html src='/files/2024/08/arduino-ide-18-pt.jpg' %}

Observe que agora a placa age de acordo com o programa:

{% include image.html src='/files/2024/08/arduino-blink.gif' %}

E esse programa fica armazenado de forma permanente na memória interna do Arduino: se você desconectar a placa do computador e liga-la à energia (usando o mesmo cabo USB e um carregador de celular, por exemplo), verá que ela fará o que foi programada para fazer. A placa funciona independente do computador.

## Solução de problemas

[Pode acontecer][uploading-a-sketch] que ao tentar carregar o programa para a placa você receba a seguinte mensagem de erro:

```
avrdude: ser_open(): can't open device "/dev/ttyUSB0": Permission denied
```

Se isso acontecer, você precisa verificar a permissão da porta serial e adicionar seu usuário ao grupo dela. Para fazer isso, abra o terminal e execute:

```
$ ls -l /dev/ttyUSB0
```

Esse comando deve produzir uma saída como:

```
crw-rw---- 1 root dialout 188, 0 ago 22  2024 /dev/ttyUSB0
```

Note que a porta `/dev/ttyUSB0` pertence ao grupo `dialout`.

Adicione seu usuário a esse grupo:

```
# usermod -a -G dialout seu_nome_de_usuario
```

Reinicie seu computador e tente novamente.

## Mais um exemplo

Agora que você já fez o LED da própria placa piscar, pode incrementar o experimento e fazer o Arduino controlar um LED externo.

Para isso, você vai precisar, além da placa e do cabo USB, de:

- 1 LED 5mm;
- 1 resistor de 220 ohm;
- 1 [protoboard]; e
- 2 fios de ligação (_jumpers_) macho-macho.

Monte o circuito da seguinte forma:

- conecte a porta digital 10 do Arduino a uma extremidade do resistor;
- conecte a perna longa (positiva) do LED à outra extremidade do resistor; e
- conecte a perna curta (negativa) do LED ao pino GND do Arduino.

{% include image.html src='/files/2024/08/arduino-led.png' %}

{% include image.html src='/files/2024/08/arduino-led-schematic.png' %}

(diagramas feitos usando o programa [Fritzing], e baseados em um diagrama disponibilizado pela [comunidade Johnny-Five][johnny-five])

Você pode reaproveitar o código do programa de exemplo _Blink_, apenas substitua `LED_BUILTIN` por `10`. Vai ficar assim:

```c
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(10, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(10, HIGH);  // turn the LED on (HIGH is the voltage level)
  delay(1000);                      // wait for a second
  digitalWrite(10, LOW);   // turn the LED off by making the voltage LOW
  delay(1000);                      // wait for a second
}
```

Verifique e carregue o programa para a placa e este será o resultado:

{% include image.html src='/files/2024/08/arduino-led.gif' %}

## Leitura recomendada

Agora que o Arduino IDE já está instalado e funcionando no seu computador, e você já sabe como usá-lo para programar sua placa Arduino, pode continuar suas aventuras em _links_ como os seguintes:

- [O que é Arduino, para que serve e primeiros passos][makerhero-blog-1]
- [Primeiros passos com Arduino][makerhero-blog-2]
- [Curso Kit Maker Arduino Iniciante][curso]
- [Projetos com Arduino][makerhero-blog-3]

[Raspberry Pi]:         {% post_url pt/2019-09-20-primeiros-passos-no-raspberry-pi-com-noobs-e-raspbian %}
[Arduino]:              https://www.arduino.cc/
[wikipedia]:            https://pt.wikipedia.org/wiki/Arduino
[guide]:                https://www.arduino.cc/en/Guide/Introduction
[free-sw]:              https://www.gnu.org/philosophy/free-sw.pt-br.html
[oss]:                  https://pt.wikipedia.org/wiki/C%C3%B3digo_aberto
[placas]:               https://www.makerhero.com/categoria/arduino/placas-arduino/
[X/Twitter]:            https://x.com/
[ieee]:                 https://spectrum.ieee.org/the-making-of-arduino
[kit]:                  https://www.makerhero.com/produto/kit-maker-arduino-iniciante/
[curso]:                https://www.makerhero.com/curso/kit-maker-arduino-iniciante/
[programming]:          https://docs.arduino.cc/programming/
[C/C++]:                https://www.alura.com.br/artigos/linguagens-c-c-qual-diferenca-entre-elas
[IDE]:                  https://www.redhat.com/pt-br/topics/middleware/what-is-ide
[Arduino IDE]:          https://www.arduino.cc/en/software
[Linux]:                https://www.vivaolinux.com.br/linux/
[Linux Kamarada 15.5]:  {% post_url pt/2024-05-27-linux-kamarada-15-5-mais-alinhado-com-o-opensuse-leap-e-com-outras-distribuicoes %}
[uno]:                  https://docs.arduino.cc/hardware/uno-rev3-smd/
[Arduíno de Ivrea]:     https://pt.wikipedia.org/wiki/Ardu%C3%ADno_de_Ivrea
[arduino-ide-releases]: https://github.com/arduino/arduino-ide/releases/tag/2.3.2
[gedit]:                https://gedit-technology.github.io/apps/gedit/
[configuração]:         https://docs.arduino.cc/software/ide-v2/tutorials/getting-started/ide-v2-downloading-and-installing/#linux
[Blink]:                https://docs.arduino.cc/tutorials/uno-rev3-smd/Blink/
[olá, mundo]:           https://pt.wikipedia.org/wiki/Programa_Ol%C3%A1_Mundo
[uploading-a-sketch]:   https://docs.arduino.cc/software/ide-v2/tutorials/getting-started/ide-v2-uploading-a-sketch/#please-read-only-linux-users
[protoboard]:           https://www.makerhero.com/blog/como-funciona-uma-protoboard/
[Fritzing]:             https://fritzing.org/
[johnny-five]:          https://johnny-five.io/examples/led-blink/
[makerhero-blog-1]:     https://www.makerhero.com/blog/o-que-e-arduino/
[makerhero-blog-2]:     https://www.makerhero.com/blog/primeiros-passos-com-arduino/
[makerhero-blog-3]:     https://www.makerhero.com/blog/category/arduino/
