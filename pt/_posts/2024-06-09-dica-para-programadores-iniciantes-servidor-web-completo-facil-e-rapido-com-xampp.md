---
date: '2024-06-09 16:50:00 GMT-3'
image: '/files/2024/06/xampp.jpg'
layout: post
nickname: 'xampp'
title: 'Dica para programadores iniciantes: servidor web completo, fácil e rápido com XAMPP'
---

Aqui vai uma dica preciosa para quem está começando a programar para a _web_. Como se não bastasse a dificuldade que é aprender a programação em si, você ainda precisa instalar vários programas no seu computador, a exemplo de [Apache], [PHP], [MySQL]/[MariaDB]... seria mais fácil se existisse um pacote com todos esses programas já instalados e pré-configurados, prontos para uso, não?

Pois saiba que existe, sim. É exatamente isso que é o **[XAMPP]**: uma distribuição Apache contendo MariaDB, PHP e [Perl] completamente gratuita, de [código aberto][GitHub], criada para ser extremamente fácil de instalar e de usar.

{% include image.html src='/files/2024/06/xampp.jpg' %}

Você pode imaginar que o nome XAMPP vem das iniciais de  Apache, MariaDB, PHP e Perl. O X provavelmente vem do fato de ele estar disponível para os sistemas operacionais mais comuns: [Windows], [Linux] e [macOS].

A seguir, você verá como instalar o XAMPP no Linux, assim como dicas que podem te auxiliar a usá-lo no dia a dia. Como referência, usaremos a distribuição [Linux Kamarada 15.5], baseada no [openSUSE Leap 15.5], mas as instruções podem ser parecidas para outras distribuições.

## Não use o XAMPP em produção

Antes de começar, um aviso importante: o XAMPP não foi feito para ser instalado em produção, ou seja, no servidor que ficará exposto à Internet e será acessado pelos usuários (e, possivelmente, por pessoas mal intencionadas).

O XAMPP é um ambiente de desenvolvimento e deve ser instalado apenas no seu computador, de desenvolvedor. Os componentes do XAMPP foram configurados de forma que o desenvolvedor consegue facilmente fazer o que quiser com eles, mas algumas dessas configurações não são recomendadas quando segurança é uma preocupação. Por exemplo, o usuário administrador (_root_) do banco de dados não tem senha.

Se quiser mais informações, consulte a [documentação do XAMPP para Linux][xampp-faq].

## Baixando o XAMPP

Para baixar o instalador do XAMPP para Linux, acesse o _site_ oficial do XAMPP em:

- <https://www.apachefriends.org/pt_br/>

E clique no _link_ do **XAMPP para Linux**:

{% include image.html src='/files/2024/06/xampp-01-pt.jpg' %}

O _download_ do instalador do XAMPP será iniciado automaticamente.

No momento em que escrevo, o XAMPP para Linux está na [versão 8.2.12][xampp-blog] e o instalador é um arquivo chamado `xampp-linux-x64-8.2.12-0-installer.run`.

## Instalando o XAMPP

Antes de instalar propriamente o XAMPP, instale algumas dependências necessárias para usá-lo no [openSUSE Leap] ou no Linux Kamarada:

```
# zypper install insserv-compat net-tools-deprecated
```

Também crie o grupo e o usuário `daemon`, ausentes em uma instalação nova do Linux Kamarada 15.5:

```
# groupadd daemon
# useradd -d /sbin -g daemon --no-create-home --system -s /usr/sbin/nologin -u 2 daemon
```

Altere as permissões do instalador baixado:

```
$ chmod 755 xampp-linux-*-installer.run
```

E, finalmente, execute o instalador:

```
$ sudo ./xampp-linux-*-installer.run
```

Tecle **Enter** a cada pergunta para aceitar as respostas padrão.

Isso é tudo, pessoal! O XAMPP agora está instalado no diretório `/opt/lampp/`.

## Facilitando o acesso à pasta htdocs

Os arquivos e pastas servidos pelo Apache do XAMPP ficam na pasta `htdocs` dentro de `/opt/lampp/`.

Como o XAMPP é apenas um ambiente de desenvolvimento e só será usado por você no seu computador, sugiro que você mude as permissões da pasta `htdocs` para que você possa trabalhar mais tranquilamente, sem se preocupar com permissões em um primeiro momento:

```
# chmod 777 /opt/lampp/htdocs
```

Note que essa não é uma boa prática de segurança. Quando for implantar seus _sites_ e sistemas em produção, lembre-se de definir as permissões adequadas para pastas e arquivos para prevenir acessos indevidos.

Também sugiro que você adicione um marcador para a pasta `htdocs` no **Arquivos**. Para isso, acesse a pasta `htdocs`, clique no seu nome na barra de endereços e clique em **Adicionar marcador**. Note que agora ela pode ser facilmente acessada pela barra lateral:

<div class="row">
    <div class="col-md">
        {% include image.html src='/files/2024/06/xampp-02-pt.jpg' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2024/06/xampp-03-pt.jpg' %}
    </div>
</div>

## Iniciando o XAMPP

Você pode iniciar o XAMPP pelo terminal executando este comando:

```
$ sudo /opt/lampp/lampp start
```

Agora você deve ver algo como isto em sua tela:

```
Starting XAMPP for Linux 8.2.12-0...
XAMPP: Starting Apache...ok.
XAMPP: Starting MySQL...ok.
XAMPP: Starting ProFTPD...redirecting to systemctl start .service
ok.
```

Você também pode iniciar o XAMPP por uma interface gráfica, assim como configurar seu sistema para iniciar o XAMPP automaticamente na inicialização, como veremos a seguir.

## Verificando o estado do XAMPP

Você pode verificar se o XAMPP está em execução pelo terminal usando:

```
$ sudo /opt/lampp/lampp status
```

Se o XAMPP tiver iniciado, ele deve retornar algo como:

```
Version: XAMPP for Linux 8.2.12-0
Apache is running.
MySQL is running.
ProFTPD is running.
```

Você também pode verificar o estado do XAMPP por uma interface gráfica, como veremos a seguir.

## Testando o XAMPP

Uma vez iniciado o XAMPP, abra seu navegador preferido e acesse `http://localhost`. Deve aparecer a página de boas vindas do XAMPP:

{% include image.html src='/files/2024/06/xampp-04.jpg' %}

Isso já mostra que o XAMPP foi instalado e está funcionando corretamente.

Mas proponho mais um teste: crie uma pasta `teste` dentro da pasta `htdocs`.

Dentro dela, usando seu editor de texto preferido (ou um [ambiente de desenvolvimento integrado][ide]), crie um arquivo `index.php` com o seguinte conteúdo (inspirado nesse [GitHub Gist]):

```php
<?php
date_default_timezone_set('America/Sao_Paulo');
$hora = date('H');
if ($hora >= 6 && $hora <= 12) echo 'Bom dia!';
else if ($hora > 12 && $hora <= 18) echo 'Boa tarde!';
else echo 'Boa noite!';
?>
```

Usando o navegador, acesse `http://localhost/teste`. Deve aparecer uma saudação diferente, conforme a hora atual:

{% include image.html src='/files/2024/06/xampp-05-pt.jpg' %}

## Obtendo informações sobre o servidor com phpinfo

Proponho ainda mais um teste, mais útil. Substitua o conteúdo do arquivo `index.php` pelo seguinte, com apenas uma chamada à função [`phpinfo()`][phpinfo]:

```php
<?php
phpinfo();
?>
```

E atualize a página no navegador (**F5**):

{% include image.html src='/files/2024/06/xampp-06-pt.jpg' %}

A função `phpinfo()` é útil para obter informações sobre a instalação do PHP.

## Encerrando o XAMPP

Se por qualquer motivo você quiser ou precisar encerrar o XAMPP (se, por exemplo, você terminou de programar e não vai mais trabalhar hoje), você pode fazer isso executando este comando no terminal:

```
$ sudo /opt/lampp/lampp stop
```

Deve aparecer algo como isto em sua tela:

```
Stopping XAMPP for Linux 8.2.12-0...
XAMPP: Stopping Apache...ok.
XAMPP: Stopping MySQL...ok.
XAMPP: Stopping ProFTPD...ok.
```

Você também pode encerrar o XAMPP por uma interface gráfica, como veremos a seguir.

## Gerenciando o XAMPP pela interface gráfica

O XAMPP oferece uma interface gráfica por meio da qual é possível iniciá-lo ou encerrá-lo. Ele só não adiciona um ícone para ela no menu de aplicativos. Façamos isso.

Usando seu editor de texto preferido, comece um novo arquivo com o seguinte conteúdo:

```
[Desktop Entry]
Name=XAMPP
Exec=gnomesu -c /opt/lampp/manager-linux-x64.run
Icon=/opt/lampp/htdocs/dashboard/images/xampp-logo.svg
Type=Application
```

Salve esse arquivo em `~/.local/share/applications` com o nome de `xampp.desktop`.

Com isso, você já deve ser capaz de abrir o menu **Atividades** e iniciar a interface gráfica de gerenciamento do XAMPP por ele:

{% include image.html src='/files/2024/06/xampp-07-pt.jpg' %}

{% include image.html src='/files/2024/06/xampp-08.jpg' %}

Mude para a aba **Manage Servers** (gerenciar servidores) para conferir se os serviços estão em execução, assim como iniciá-los ou encerrá-los:

{% include image.html src='/files/2024/06/xampp-09.jpg' %}

## Iniciando o XAMPP junto com o sistema

Se você usa o XAMPP todo dia, pode ser conveniente configurar seu sistema para iniciá-lo automaticamente durante a inicialização do sistema (_boot_).

Para fazer isso no openSUSE Leap e no Linux Kamarada, execute estes comandos:

```
$ sudo ln -s /opt/lampp/lampp /etc/init.d/lampp
$ sudo chkconfig lampp 2345
```

Feito isso, se você reiniciar seu sistema e verificar o estado do XAMPP, verá que ele está em execução.

## Arquivos de configuração do XAMPP

Pro caso de você precisar, os principais arquivos de configuração do XAMPP são os seguintes:

- Arquivos de configuração do Apache: `/opt/lampp/etc/httpd.conf` e `/opt/lampp/etc/extra/httpd-xampp.conf`;
- Arquivo de configuração do PHP: `/opt/lampp/etc/php.ini`;
- Arquivo de configuração do MariaDB: `/opt/lampp/etc/my.cnf`; e
- Arquivo de configuração do [ProFTPD]: `/opt/lampp/etc/proftpd.conf`.

## Onde obter mais informações

O XAMPP para Linux tem uma excelente [documentação no formato de perguntas e respostas][xampp-faq] no seu _site_ oficial. Muitas das informações presentes lá eu já trouxe para este artigo, mas você pode encontrar ainda mais informações lá.

Caso precise de mais ajuda, consulte a página [Comunidade] do _site_ do XAMPP.

Ou comente abaixo!

Espero ter ajudado. Até a próxima!

[Apache]:               https://httpd.apache.org/
[PHP]:                  https://www.php.net/
[MySQL]:                https://www.mysql.com/
[MariaDB]:              https://mariadb.org/
[XAMPP]:                https://www.apachefriends.org/pt_br/index.html
[Perl]:                 https://www.perl.org/
[GitHub]:               https://github.com/ApacheFriends
[Windows]:              https://www.microsoft.com/pt-br/windows/
[Linux]:                https://www.vivaolinux.com.br/linux/
[macOS]:                https://www.apple.com/br/macos/
[Linux Kamarada 15.5]:  {% post_url pt/2024-05-27-linux-kamarada-15-5-mais-alinhado-com-o-opensuse-leap-e-com-outras-distribuicoes %}
[openSUSE Leap 15.5]:   {% post_url pt/2023-06-07-lancamento-do-opensuse-leap-15-5-amadurece-a-distribuicao-e-estabelece-transicao-tecnologica %}
[xampp-faq]:            https://www.apachefriends.org/pt_br/faq_linux.html
[xampp-blog]:           https://www.apachefriends.org/blog/new_xampp_20231119.html
[openSUSE Leap]:        {% post_url pt/2020-12-06-opensuse-leap-e-opensuse-tumbleweed-qual-e-a-diferenca %}
[ide]:                  https://www.redhat.com/pt-br/topics/middleware/what-is-ide
[GitHub Gist]:          https://gist.github.com/danilowm/1997944
[phpinfo]:              https://www.php.net/manual/pt_BR/function.phpinfo.php
[ProFTPD]:              http://proftpd.org/
[Comunidade]:           https://www.apachefriends.org/pt_br/community.html
