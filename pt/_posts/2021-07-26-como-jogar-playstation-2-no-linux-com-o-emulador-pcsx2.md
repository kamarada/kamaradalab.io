---
date: 2021-07-27 23:30:00 GMT-3
image: '/files/2021/07/pcsx2.jpg'
layout: post
published: true
nickname: 'pcsx2'
title: 'Como jogar PlayStation 2 no Linux com o emulador PCSX2'
excerpt: 'Os jogos do videogame mais vendido de todos os tempos no seu computador!'
---

{% include image.html src="/files/2021/07/pcsx2.jpg" %}

O [**PlayStation 2**][ps2], mais conhecido simplesmente por **PS2**, foi um console de *videogame* de 128 *bits* lançado pela [Sony] no ano 2000. Foi o console mais vendido de todos os tempos, com mais de 155 milhões de unidades vendidas. Foram lançados mais de 10 mil jogos para o PS2, alguns dos mais conhecidos incluem: [Pro Evolution Soccer][pes] (que aqui no Brasil era chamado pelo nome japonês, Winning Eleven), [GTA (Grand Theft Auto)][gta], [Guitar Hero], [Resident Evil], [God of War], [Devil May Cry], [Final Fantasy X][ffx], [Mortal Kombat Shaolin Monks][mksm] e outros. Gostaria de relembrar algum desses clássicos? Veja como fazer isso com o PCSX2.

[ps2]: https://pt.wikipedia.org/wiki/PlayStation_2
[sony]: https://pt.wikipedia.org/wiki/Sony_Interactive_Entertainment
[pes]: https://pt.wikipedia.org/wiki/Pro_Evolution_Soccer
[gta]: https://pt.wikipedia.org/wiki/Grand_Theft_Auto
[Guitar Hero]: https://pt.wikipedia.org/wiki/Guitar_Hero
[Resident Evil]: https://pt.wikipedia.org/wiki/Resident_Evil
[God of War]: https://pt.wikipedia.org/wiki/God_of_War
[Devil May Cry]: https://pt.wikipedia.org/wiki/Devil_May_Cry
[ffx]: https://pt.wikipedia.org/wiki/Final_Fantasy_X
[mksm]: https://pt.wikipedia.org/wiki/Mortal_Kombat_Shaolin_Monks

O **[PCSX2]** é um emulador de PS2 para PC. Um **emulador** é um programa que permite que seu computador execute programas feitos para outros dispositivos. Um emulador de um *videogame* faz com que seu computador seja capaz de rodar os jogos feitos para aquele *videogame*. Com o PCSX2, você consegue jogar a maioria dos jogos de PS2 no computador.

[PCSX2]: https://pcsx2.net/

O PCSX2 é um [*software* livre][free-software] e seu código-fonte está disponível no [GitHub].

[free-software]:    https://www.gnu.org/philosophy/free-sw.pt-br.html
[github]:           https://github.com/PCSX2/pcsx2

Nesse *post*, você verá como instalar e usar o PCSX2 no [Linux]. Como de costume, aqui eu uso a distribuição [Linux Kamarada], baseada no [openSUSE Leap].

[Linux]: http://www.vivaolinux.com.br/linux/
[Linux Kamarada]: {% post_url pt/2020-09-11-linux-kamarada-15.2-venha-para-o-lado-verde-elegante-e-moderno-da-forca %}
[openSUSE Leap]: {% post_url pt/2020-12-06-opensuse-leap-e-opensuse-tumbleweed-qual-e-a-diferenca %}

## Requisitos de sistema do PCSX2

Quando foi lançado, o PS2 era um _videogame_ com especificações bem superiores às dos concorrentes na época. Mesmo hoje, muitos anos depois e com o avanço da tecnologia, não são todos os computadores capazes de emulá-lo. O PCSX2 é um programa que demanda muito do _hardware_ do computador. Para jogar jogos de PS2 — e, principalmente, jogar bem, porque de nada adianta conseguir iniciar o jogo se ele fica lento — você vai precisar de um computador considerado "intermediário" a "avançado" pros padrões atuais.

O [_site_ do PCSX2][getting-started] apresenta como **configuração mínima** exigida a seguinte:

[getting-started]: https://pcsx2.net/getting-started.html

- **Sistema operacional:**
  - [Windows] 7 ou mais novo (32 ou 64 _bits_), ou
  - [Ubuntu] 18.04 ou mais novo, também pode ser [Debian], [Arch Linux] ou outra distribuição (32 ou 64 _bits_)
- **Processador (CPU):**
  - que suporte o conjunto de instruções SSE2
  - que tenha 2 núcleos de processamento (_dual core_) e _hyperthreading_
  - que no _benchmark_ [PassMark Single Thread Performance][cpubenchmark] tenha nota 1600 ou maior
- **_Chip_ gráfico (GPU):**
  - que suporte Direct3D10 (no caso do Windows) ou OpenGL 3.x (no caso do Linux)
  - que tenha 2 GB de memória dedicada de vídeo
  - que no _benchmark_ [PassMark G3D Mark][videocardbenchmark] tenha nota 3000 ou maior (por exemplo, a placa de vídeo GeForce GTX 750)
- **Memória RAM:** 4 GB

[Windows]: https://www.microsoft.com/pt-br/windows
[ubuntu]:                   https://ubuntu.com/
[debian]:                   https://www.debian.org/index.pt.html
[Arch Linux]: https://archlinux.org/
[cpubenchmark]: https://www.cpubenchmark.net/singleThread.html
[videocardbenchmark]: https://www.videocardbenchmark.net/high_end_gpus.html

Os desenvolvedores do PCSX2 listaram seus requisitos de forma um tanto diferente da usual. Mas vou te mostrar na prática como você pode ver se funcionará no seu computador.

Para descobrir as especificações do seu computador, se você usa a área de trabalho [GNOME] (padrão do Linux Kamarada), inicie o aplicativo **Configurações** e vá na seção **Detalhes**:

[GNOME]: https://br.gnome.org/

{% include image.html src='/files/2021/07/pcsx2-01-pt.jpg' %}

No meu caso, eu tenho:

- **Sistema operacional:** openSUSE Leap (Linux Kamarada) 15.2 de 64 _bits_
- **Processador (CPU):** Intel Core i5-5200U com 4 núcleos a 2,2 GHz
- **_Chip_ gráfico (GPU):** Intel HD Graphics 5500
- **Memória RAM:** 16 GB

De posse dessas informações, vamos verificar nossas notas nos _benchmarks_.

Abra o [_site_ do _benchmark_ do processador][cpubenchmark] e clique no _link_ **Search Model** (procurar modelo) na barra de navegação à esquerda:

{% include image.html src='/files/2021/07/pcsx2-02.jpg' %}

Pesquise pelo modelo do seu processador e clique nele na lista:

{% include image.html src='/files/2021/07/pcsx2-03.jpg' %}

Na página do modelo, verifique o número que aparece em **Single Thread Rating**:

{% include image.html src='/files/2021/07/pcsx2-04.jpg' %}

Depois, abra o [_site_ do _benchmark_ do _chip_ gráfico][videocardbenchmark] e faça o mesmo para o seu _chip_ gráfico.

{% include image.html src='/files/2021/07/pcsx2-05.jpg' %}

No meu caso, a nota do meu processador é 1519 e a nota do meu _chip_ gráfico é 580.

Como você pode ver, meu computador não atende aos requisitos mínimos do PCSX2. Ao menos na teoria, eu não conseguiria jogar jogos de PS2 no meu computador.

Mas o [_site_ do PCSX2][getting-started] também explica que esses requisitos são recomendados para jogos razoavelmente complexos, que exigiam do PS2. Alguns jogos em 2D e outros que subutilizavam o _hardware_ do PS2 podem rodar com processadores mais modestos, com nota a partir de 1200 (felizmente, esse é o meu caso).

Você pode conferir listas de **jogos leves** (no [fórum][pcsx2-forum-1]) e **pesados** (no [fórum][pcsx2-forum-2] e na [_wiki_][pcsx2-wiki]).

[pcsx2-forum-1]: https://forums.pcsx2.net/Thread-LIST-Games-that-don-t-need-a-strong-CPU-to-emulate
[pcsx2-forum-2]: https://forums.pcsx2.net/Thread-LIST-The-Most-CPU-Intensive-Games
[pcsx2-wiki]: https://wiki.pcsx2.net/Category:CPU_intensive_games

Também felizmente, na lista de jogos leves aparece o [Disgaea: Hour of Darkness][disgaea], cujo DVD eu já tinha. Esse jogo é muito bom, muito fácil e divertido de jogar, eu recomendo!

[disgaea]: https://pt.wikipedia.org/wiki/Disgaea:_Hour_of_Darkness

Já a **configuração recomendada** para o PCSX2 é a seguinte:

- **Sistema operacional:**
  - Windows 10 (64 _bits_), ou
  - Ubuntu 19.04 ou mais novo, também pode ser Debian, Arch Linux ou outra distribuição (32 ou 64 _bits_)
- **Processador (CPU):**
  - que suporte o conjunto de instruções AVX2
  - que tenha 4 núcleos (_quad core_), com ou sem _hyperthreading_
  - que no _benchmark_ [PassMark Single Thread Performance][cpubenchmark] tenha nota 2100 ou maior
- **_Chip_ gráfico (GPU):**
  - que suporte Direct3D11 (no caso do Windows) ou OpenGL 4.5 (no caso do Linux)
  - que tenha 4 GB de memória dedicada de vídeo
  - que no _benchmark_ [PassMark G3D Mark][videocardbenchmark] tenha nota 6000 ou maior (por exemplo, a placa de vídeo GeForce GTX 1050 Ti)
- **Memória RAM:** 8 GB

Se seu computador tiver essas especificações ou maiores, é provável que consiga jogar bem qualquer jogo de PS2 com bom desempenho.

Mas, como eu disse, na verdade todas essas recomendações dependem muito dos jogos. Mesmo que seu computador possua especificações abaixo da recomendada (ou mesmo abaixo da mínima, como o meu), não deixe de conferir as listas de jogos pra ver quais jogos você conseguirá emular com seu computador.

## Recomendação: controle de PS2

Os consoles de PlayStation também são diferenciados dos demais com relação aos controles, que tem muitos botões. Para jogar jogos de PS2 no PC, eu recomendo que você use um controle semelhante ao controle do PS2. Há modelos com conexão USB, outros com as duas conexões, USB e PS2, que podem ser usados tanto no PC quanto no PS2.

{% include image.html src='/files/2021/07/ps2-controle.jpg' caption='Exemplo de controle que pode ser usado tanto no PS2 quanto no computador (via USB)' %}

Se você já tem um controle para PS2, um alternativa é usar um adaptador para USB.

{% include image.html src='/files/2021/07/ps2-adaptador.jpg' caption='Exemplo de adaptador de PS2 para USB' %}

Controles de PS2 são _plug and play_ no PCSX2, ou seja, dispensam configuração, é só plugar e usar. Além de serem mais fáceis de configurar, facilitam jogar, melhorando a experiência, que fica parecida com a do console. Recomendo fortemente usar um controle de PS2.

{% capture atualizacao %}

Um [controle sem fio de Xbox 360][xbox360-wireless], que tem a mesma quantidade de botões (na verdade, tem até um a mais: o botão **Guia**), também funciona com o PCSX2, que também faz a configuração automática desse controle.

[xbox360-wireless]: {% post_url pt/2021-11-14-como-usar-um-controle-de-xbox-360-sem-fio-para-jogar-no-linux %}

{% endcapture %}

{% include update.html date="15/11/2021" message=atualizacao %}

## Onde conseguir o BIOS do PS2?

O PCSX2 não é o emulador mais simples que existe, apesar do esforço dos desenvolvedores em fazê-lo o mais amigável possível. Isso se deve ao fato de o PS2 também não ser o _videogame_ mais simples que existe. E há uma diferença na emulação de PS2 comparada à emulação de outros _videogames_, como [Super Nintendo][snes9x], por exemplo, que [já mostrei aqui][snes9x]: para emular PS2, você precisa de mais um programa além do emulador, que é o BIOS.

[snes9x]: {% post_url pt/2019-08-11-como-jogar-super-nintendo-no-opensuse-com-o-emulador-snes9x %}

O **[BIOS]** (do inglês _Basic Input/Output System_, traduzindo: Sistema Básico de Entrada e Saída) é um programa que fica gravado em um _chip_ no computador e é responsável por reconhecer os componentes de _hardware_ quando o computador é ligado. Assim como seu PC tem um BIOS, o PS2 também tem um. E o PCSX2 precisa desse BIOS para emular PS2.

[bios]: https://www.tecmundo.com.br/o-que-e/244-o-que-e-bios-.htm

O BIOS do PS2 não vem incluído no PCSX2, porque é um programa pelo qual a Sony exige direitos autorais. Idealmente, você deve extrair o BIOS de um console PS2. O _site_ do PCSX2 tem um programa que permite fazer isso na seção de [ferramentas].

[ferramentas]: https://pcsx2.net/download/releases/tools.html

Uma alternativa é baixar o BIOS já extraído de algum _site_. Você pode encontrar um _site_ que disponibilize o BIOS do PS2 usando mecanismos de busca como [DuckDuckGo][duckduckgo-bios] ou [Google][google-bios].

[duckduckgo-bios]: https://duckduckgo.com/?q=ps2+bios
[google-bios]: https://www.google.com/search?q=ps2+bios

## Onde conseguir jogos para PS2?

Diferente dos jogos para Super Nintendo que vinham em cartuchos, os jogos para PS2 vinham em DVDs, que são mídias para as quais muitos computadores têm leitores. Se seu computador tem leitor de DVD, basta inserir o DVD do jogo no leitor. Você também pode gerar uma [imagem ISO][iso] desse DVD e guardá-la no computador para acesso mais rápido.

[iso]: https://canaltech.com.br/windows/O-que-e-uma-imagem-ISO/

Outra alternativa, principalmente se seu computador não tem leitor de DVD, é baixar imagens ISO de jogos de algum _site_. Você pode encontrar _sites_ que disponibilizem jogos de PS2 usando mecanismos de busca como [DuckDuckGo][duckduckgo-isos] ou [Google][google-isos].

[duckduckgo-isos]: https://duckduckgo.com/?q=ps2+isos
[google-isos]: https://www.google.com/search?q=ps2+isos

## Instalando o PCSX2

Você pode encontrar pacotes do PCSX2 pesquisando em [software.opensuse.org][softwareoo] ou no [PackMan]. Mas pra mim o que funcionou melhor foi instalar o PCSX2 via Flatpak.

[softwareoo]: https://software.opensuse.org/package/pcsx2
[PackMan]: http://packman.links2linux.org/package/pcsx2

O **[Flatpak]** é um gerenciador de pacotes independente de distribuição. Até pouco tempo atrás, diferentes distribuições Linux usavam diferentes formatos de pacotes, geralmente incompatíveis entre si. Por exemplo, Debian e Ubuntu usam pacotes [DEB], enquanto [RedHat], [Fedora] e [openSUSE] usam pacotes [RPM]. O Flatpak trouxe uma alternativa mais simples para instalar programas em diferentes distribuições: contanto que o Flatpak esteja instalado no sistema, o mesmo pacote Flatpak pode ser instalado em qualquer distribuição.

[flatpak]:                  https://flatpak.org/
[deb]:                      https://pt.wikipedia.org/wiki/.deb
[redhat]:                   https://www.redhat.com/pt-br/technologies/linux-platforms/enterprise-linux
[fedora]:                   https://getfedora.org/pt_BR/
[opensuse]:                 https://www.opensuse.org/
[rpm]:                      https://pt.wikipedia.org/wiki/RPM_(software)

Para instalar o Flatpak no Linux Kamarada ou no openSUSE, caso você ainda não o tenha instalado, execute:

```
# zypper in flatpak
```

Com o Flatpak instalado, adicione o principal repositório do Flatpak, o [Flathub]:

[flathub]:                  https://flathub.org/

```
# flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
```

Por fim, para instalar o [PCSX2 a partir do Flathub][pcsx2-flathub], execute:

[pcsx2-flathub]: https://flathub.org/apps/details/net.pcsx2.PCSX2

```
# flatpak install net.pcsx2.PCSX2
```

Após a instalação, você já deve ser capaz de iniciar o emulador.

## Iniciando o PCSX2

Para iniciar o PCSX2, se você usa a área de trabalho GNOME, clique em **Atividades**, no canto superior esquerdo da tela, comece a digitar `pcsx2` e clique no ícone do emulador:

{% include image.html src='/files/2021/07/pcsx2-06-pt.jpg' %}

## Configurando o PCSX2

Na primeira vez que o PCSX2 é iniciado, ele apresenta um assistente de configuração inicial. Existem algumas configurações simples que você precisa fazer antes de começar a jogar. Normalmente, você só precisa fazê-las uma vez, no primeiro uso do emulador.

{% include image.html src='/files/2021/07/pcsx2-07-pt.jpg' %}

Nessa primeira tela, o assistente permite a você mudar o idioma do emulador, que, por padrão, é o mesmo do sistema. Pode deixar como está e clicar em **Próximo**.

Na segunda tela, podemos selecionar e configurar os _plug-ins_ do PCSX2:

{% include image.html src='/files/2021/07/pcsx2-08-pt.jpg' %}

Se você vai usar o teclado como controle, em vez do controle de PS2, clique no botão **Configurar** na linha do _plug-in_ **PAD** (controle). Na janela que abre, configure os mapeamentos como desejar, e quando terminar clique em **OK**:

{% include image.html src='/files/2021/07/pcsx2-09.jpg' %}

A maioria das pessoas usando um computador moderno será capaz de continuar normalmente, sem alterar outros _plug-ins_ ou configurações. Clique em **Próximo**.

Se você tentar continuar com a configuração e receber um aviso de que o _plug-in_ **GS** (_Graphics Synthesizer_, o _chip_ gráfico do PS2) falhou ao carregar ou é incompatível:

{% include image.html src='/files/2021/07/pcsx2-10-pt.jpg' %}

Clique em **OK** e, de volta à seleção dos _plug-ins_, mude o _plug-in_ **GS**:

{% include image.html src='/files/2021/07/pcsx2-11-pt.jpg' %}

Selecione o _plug-in_ com **SSE4** e tente de novo. Se esse também não funcionar, selecione o _plug-in_ com **SSE2** e tente de novo. Se mesmo esse não funcionar, é possível que seu computador seja muito antigo e não seja capaz de emular jogos de PS2 com o PCSX2. Se esse é o seu caso, talvez você queira pedir ajuda no [fórum do PCSX2][forum].

[forum]: https://forums.pcsx2.net/

Na terceira e última tela do assistente, é necessário selecionar o BIOS:

{% include image.html src='/files/2021/07/pcsx2-12-pt.jpg' %}

Veja a pasta onde o PCSX2 espera que esteja o BIOS (no meu exemplo, `/home/nome_de_usuario/.var/app/net.pcsx2.PCSX2/config/PCSX2/bios`) e copie os arquivos do BIOS para essa pasta:

{% include image.html src='/files/2021/07/pcsx2-13-pt.jpg' %}

Ou, se preferir, desmarque a opção **Utilizar configuração padrão** e clique no botão **Procurar** para informar a pasta onde está o BIOS.

Em ambos os casos, clique em **Atualizar lista**. Então, selecione o BIOS na lista (não sei qual é a diferença entre as versões, na dúvida, eu selecionei a mais recente) e clique em **Concluir**:

{% include image.html src='/files/2021/07/pcsx2-14-pt.jpg' %}

## Tela inicial do PCSX2

Concluído o assistente de configuração, você verá a tela inicial do PCSX2:

{% include image.html src='/files/2021/07/pcsx2-15-pt.jpg' %}

Das próximas vezes que você iniciar o emulador, ele abrirá direto nessa tela.

## Iniciando um jogo

Se você vai jogar com um controle de PS2, agora é uma boa hora para conectá-lo, caso ainda não tenha feito.

Abra o menu **CDVD** e selecione uma das seguintes opções:

{% include image.html src='/files/2021/07/pcsx2-16-pt.jpg' %}

- **ISO:** se pretende carregar o jogo de uma imagem ISO;
- **Plug-in:** se pretende carregar o jogo de um DVD inserido no leitor de DVD; ou
- **Nenhum disco:** vai para o BIOS (semelhante a ligar um PS2 sem um DVD inserido).

Se você marcou a opção **ISO**, vá em **CDVD** > **Seleção de ISO** > **Procurar** e informe onde está a imagem ISO do jogo.

Feito isso, abra o menu **Sistema** e clique em uma das seguintes opções:

{% include image.html src='/files/2021/07/pcsx2-17-pt.jpg' %}

- **Carregar ISO (completo):** inicia o BIOS e, depois, o jogo (igual ao PS2); ou
- **Carregar ISO (rápido):** pula o BIOS e inicia o jogo diretamente.

Em seguida, o jogo deve começar:

{% include image.html src="/files/2021/07/pcsx2-18-pt.jpg" %}

## Salvando e retomando

Uma vantagem do emulador em relação ao *videogame* é que você pode [salvar o estado][savestate] do jogo a qualquer momento, assim como retomar a qualquer momento do ponto em que salvou. Mesmo em momentos onde o jogo normalmente não permitiria salvar ou retomar.

[savestate]: https://wiki.pcsx2.net/Savestate

Jogando no PCSX2, você pode salvar da mesma forma que no PS2 (usando o menu do próprio jogo, o PCSX2 emula cartões de memória, como veremos a seguir), ou pode usar uma das 10 memórias que o emulador oferece.

Para salvar o estado do jogo, abra o menu **Sistema**, aponte para **Salvar estado** e escolha uma das memórias (compartimentos) disponíveis:

{% include image.html src="/files/2021/07/pcsx2-19-pt.jpg" %}

Para retomar um estado previamente salvo, abra o menu **Sistema**, aponte para **Carregar estado** e escolha a memória que deseja retomar:

{% include image.html src="/files/2021/07/pcsx2-20-pt.jpg" %}

Para facilitar salvar e retomar estados, o PCSX2 oferece as seguintes teclas de atalho:

- **F1:** salva o estado no compartimento atual. O padrão é o compartimento 0.
- **F2:** aponta para o próximo compartimento (se o máximo de 9 for alcançado, o próximo será o compartimento 0).
- **Shift + F2:** aponta para o compartimento anterior (se o mínimo de 0 for atingido, o anterior será o compartimento 9).
- **F3:** carrega o estado do compartimento atual. O padrão é o compartimento 0.

## Emulação de cartões de memória

O [**cartão de memória**][memcard] (_memory card_) é um dispositivo de armazenamento com memória _flash_ (mesma tecnologia dos _pendrives_) usado pelo PS2 para salvar o progresso no jogo. Comumente sua capacidade era de 8 MB e era possível inserir no PS2 até 2 cartões de memória.

[memcard]: https://wiki.pcsx2.net/Memcard

O PCSX2 cria 2 arquivos com extensão **.ps2** que simulam 2 cartões de memória na subpasta `memcards` dentro da pasta de configuração do PCSX2 (no nosso caso, `/home/nome_de_usuario/.var/app/net.pcsx2.PCSX2/config/PCSX2/memcards`). É uma boa ideia de tempos em tempos fazer _backup_ desses arquivos. Para imitar os cartões de memória do PS2, cada um desses arquivos tem 8 MB.

Sendo assim, você pode usar cartões de memória dentro do jogo normalmente:

{% include image.html src="/files/2021/07/pcsx2-21.jpg" %}

Caso precise alterar alguma configuração referente a cartões de memória, vá em **Configuração** > **Cartões de memória**.

Os cartões de memória são separados e diferentes dos estados do emulador, que vimos antes. Os cartões de memória são um recurso do PS2 emulado, não um recurso nativo do emulador. Para salvar seu progresso nos jogos, você pode usar um recurso ou outro, ou ambos ao mesmo tempo.

## Tela cheia

Se quiser jogar em tela cheia, simplesmente dê um duplo-clique dentro da janela do jogo. Repita esse duplo-clique para sair do modo tela cheia.

Como alternativa, você pode usar a combinação de teclas **Alt + Enter** tanto para entrar em tela cheia quanto para sair do modo tela cheia.

## Conclusão

Encerro minha parte por aqui. Agora é com você: boa diversão!
