---
layout: download-release
nickname: download-15.6
permalink: /pt/download/15.6/
title: Linux Kamarada 15.6 Beta

intro: 'Baixe e ajude a testar a próxima versão do Linux Kamarada, para PCs e <i>notebooks</i>.'

news_link: 'Para saber mais sobre esta versão, leia a <a href="/pt/2024/12/18/linux-kamarada-15-6-beta-ajude-a-testar-a-melhor-versao-da-distribuicao">notícia de lançamento</a>.'

free_sw: 'O Linux Kamarada é totalmente <a href="https://www.gnu.org/philosophy/free-sw.pt-br.html"><strong>livre</strong></a> para baixar, usar e compartilhar.'

based_on_opensuse: 'Baseada no openSUSE'
based_on_opensuse_desc: 'É uma distribuição Linux baseada no excelente <a href="/pt/2024/06/12/leap-15-6-revela-opcoes-para-usuarios/">openSUSE Leap</a> — distribuição híbrida de empresa e comunidade do <a href="http://opensuse.org">openSUSE</a> — e contém personalizações.'

language_label: 'Idiomas'
language_value: 'Português Brasileiro e Inglês Internacional'

filename_label: 'Nome do arquivo'
filename_value: '{{ site.data.download156[page.lang].filename_value }}'
download_link: '{{ site.data.download156[page.lang].download_link }}'
download: 'Baixar'

date_label: 'Data'
date_value: '{{ site.data.download156[page.lang].date_value }}'

size_label: 'Tamanho'
size_value: '{{ site.data.download156[page.lang].size_value }}'

sha256sum_label: 'Soma SHA256'
sha256sum_value: '{{ site.data.download156[page.lang].sha256sum_value }}'
sha256sum_link: '{{ site.data.download156[page.lang].sha256sum_link }}'

system_requirements_title: 'Requisitos de sistema recomendados'
system_requirements_list: '<li>Processador 2 GHz dual core ou melhor</li>
<li>Memória RAM de 4 GB</li>
<li>Mais de 40 GB de espaço livre em disco</li>
<li>Unidade de DVD ou porta USB para uso da mídia Live</li>'

hosted-by: 'As imagens ISO do Linux Kamarada são gentilmente hospedadas pelo'
---
