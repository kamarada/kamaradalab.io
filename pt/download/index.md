---
layout: download
nickname: download
title: Download

stable_title: 'Linux Kamarada 15.5'
stable_release: '15.5'
stable_desc: 'Versão estável, pronta para usar em computadores pessoais, seja em casa ou no trabalho, em empresas privadas ou órgãos públicos.'

dev_title: 'Linux Kamarada 15.6 Beta'
dev_release: '15.6'
dev_desc: 'Próxima versão do Linux Kamarada, em desenvolvimento, não recomendada para uso diário. Como essa versão está em fase beta, é esperado que tenha _bugs_. Baixe-a se deseja testá-la e contribuir com o desenvolvimento do Linux Kamarada. Se você encontrar um _bug_, por favor, reporte. Consulte a página [Ajuda](/pt/ajuda) para saber como entrar em contato.'

archive_title: 'Arquivo (versões antigas)'
archive_desc: 'A seguir, são listadas versões mais antigas do Linux Kamarada, que estão disponíveis para que você possa baixá-las, caso por algum motivo precise. Observe que estas versões não recebem mais suporte nem atualizações.'
---
