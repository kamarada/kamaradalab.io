---
layout: contribute
nickname: contribute
title: Contribute
permalink: /en/contribute

intro: 'It is very easy to contribute to Linux Kamarada. Depending on your skill set, on your availability, and on your wealth, you can help the project in one or more of the following ways:'

help_other_users_title: 'Help other users'
help_other_users_desc: "If you have some spare time and you're willing to help other users with technical problems, you can enter the <a href='/en/help'>Help</a> channels and assist other Linux Kamarada users to solve the problems you know how to fix."

test_bugs_title: 'Test and report bugs'
test_bugs_desc: "If you notice something that doesn't work properly, troubleshoot it and if you find a bug, please report it. Take a look at the <a href='/en/help'>Help</a> page to see how to get in touch."

financial_title: 'Financial help'
financial_desc: 'Linux Kamarada is free of cost and does not generate any direct sort of income. It is funded by advertising<!--, sponsoring--> and donations. You can help by making a financial contribution to the project.'

donations_title: 'One-time donations'
donations_desc: '<p>Any amount will help and be very much appreciated!</p>
<p>You can make a one-time donation quickly with PayPal:</p>'
---
