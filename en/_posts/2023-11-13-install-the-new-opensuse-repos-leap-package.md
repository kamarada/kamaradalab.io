---
date: '2023-11-13 22:30:00 GMT-3'
image: '/files/2023/11/openSUSE-repos-Leap-05-en.jpg'
layout: post
nickname: 'openSUSE-repos-Leap'
title: 'Install the new openSUSE-repos-Leap package'
excerpt: 'Never worry about configuring official repositories again with this new feature of openSUSE Leap 15.5 (and Linux Kamarada 15.5).'
---

If you've been using [openSUSE Leap] for a while now and always [upgrade from one release to the next][upgrade], as I do, you may have noticed that the [official repositories] settings have changed a few times already. The most recent change was the [migration to the new CDN][cdn] in July. Okay, this is not something that happens often: openSUSE Leap new releases are [normally released once a year][lifetime], and it's when we upgrade that we take the opportunity to change these settings. Still, it remains a concern for users, who must pay attention to the changes.

In an effort to simplify openSUSE repository management, especially when changes need to be made to this configuration, the [openSUSE Project] developed the **[openSUSE-repos-Leap]** package, which contains the distribution's official repositories settings. The user no longer needs to worry about checking the addresses of the official repositories and adding them one by one: they just need to install this package, and it adds all the official repositories to the system. If it is necessary to change the repository configuration, the openSUSE Project releases an update for this package and the user just needs to [update the system packages][update] (as usual) to receive and start using the new configuration.

This is a new feature of [openSUSE Leap 15.5] -- and, by extension, of [Linux Kamarada 15.5], which is based on it. If you use a previous release of one of these distros, you need to [upgrade to 15.5][upgrade] to benefit from this new feature.

Installing the **openSUSE-repos-Leap** package is not mandatory to use either openSUSE Leap 15.5 or Linux Kamarada 15.5, but it is recommended.

If you started using Linux Kamarada with the 15.5 release, please note that the **openSUSE-repos-Leap** package comes already installed out-of-the-box.

Now let's see how to check if the **openSUSE-repos-Leap** package is installed on your system. The screenshots below show Linux Kamarada 15.5, but the openSUSE Leap 15.5 screens are very similar.

Start the [YaST Control Center][yast]. To do this, click **Show Applications**, in the lower left corner of the screen, and click the **YaST** icon, at the end of the list:

{% include image.html src='/files/2023/11/openSUSE-repos-Leap-01-en.jpg' %}

You will be asked the root password. Type it and click **Continue**:

{% include image.html src='/files/2023/11/openSUSE-repos-Leap-02-en.jpg' %}

Within the **Software** category (the first one), click **Software Repositories**:

{% include image.html src="/files/2023/01/upgrade-to-15.4-06-en.jpg" %}

You are presented to your system's repository list:

{% include image.html src='/files/2023/11/openSUSE-repos-Leap-03-en.jpg' %}

Notice that the official openSUSE repos are listed, but the **Service** column is empty for all of them. If it looks like this on your computer, probably the **openSUSE-repos-Leap** package is not installed.

To install it, close the **Software Repositories** window and, back to the YaST main screen, click **Software Management**. Search for the **openSUSE-repos-Leap** package, mark it for installation, and click **Accept**:

{% include image.html src='/files/2023/11/openSUSE-repos-Leap-04-en.jpg' %}

When the installation is complete, click **Finish** to close the **Software Management** window. Back to the YaST main screen, open the **Software Repositories** module again.

{% include image.html src='/files/2023/11/openSUSE-repos-Leap-05-en.jpg' %}

Notice that now the official openSUSE repos are managed by the **Service** called **openSUSE**: you no longer need to worry about their configuration.

Just remember to regularly [update your system][update] and _have a lot of fun_, but you surely do that already.

In case you want to read more about **openSUSE-repos-Leap** from official sources:

- [Leap 15.5 Reaches Beta Phase - openSUSE News][leap-beta]
- [Try out the new CDN with openSUSE-repos - openSUSE News][cdn]
- [openSUSE/openSUSE-repos - GitHub][openSUSE-repos]


[openSUSE Leap]:            {% post_url en/2020-12-07-opensuse-leap-vs-opensuse-tumbleweed-what-is-the-difference %}
[upgrade]:                  {% post_url en/2023-11-27-linux-kamarada-15-4-and-opensuse-leap-15-4-how-to-upgrade-to-15-5 %}
[official repositories]:    https://en.opensuse.org/Package_repositories#Official_Repositories
[cdn]:                      https://news.opensuse.org/2023/07/31/try-out-cdn-with-opensuse-repos/
[lifetime]:                 https://en.opensuse.org/Lifetime
[openSUSE Project]:         https://www.opensuse.org/
[openSUSE-repos-Leap]:      https://software.opensuse.org/package/openSUSE-repos-Leap
[update]:                   {% post_url en/2019-05-20-how-to-get-updates-for-opensuse-linux %}
[openSUSE Leap 15.5]:       {% post_url en/2023-06-07-leap-15-5-release-matures-sets-up-technological-transition %}
[Linux Kamarada 15.5]:      {% post_url en/2023-11-01-linux-kamarada-15-5-beta-help-test-the-best-release-of-the-distribution %}
[yast]:                     http://yast.opensuse.org/
[leap-beta]:                https://news.opensuse.org/2023/02/21/leap-reaches-beta-phase/
[openSUSE-repos]:           https://github.com/openSUSE/openSUSE-repos
