---
date: '2023-02-27 11:10:00 GMT-3'
image: '/files/2023/02/kamarada-15.3-eol.jpg'
layout: post
nickname: 'kamarada-15.3-eol'
title: 'Linux Kamarada 15.3 and openSUSE Leap 15.3 reached end of life'
excerpt: 'Support for openSUSE Leap 15.3, released on Jun 2, 2021, ended on Dec 31, 2022, totaling 18 months of security and bugfix support. There was a first announcement in advance on the mailing list on November 30th, and then a second announcement in advance on the openSUSE news website on December 12th. Actually, still on January 16th there was an update for Leap 15.3 released by SUSE, after which the support for openSUSE Leap 15.3 was definitively ended. Since then, openSUSE Leap 15.3 has received no further updates of any kind.'
---

{% include image.html src='/files/2023/02/kamarada-15.3-eol.jpg' %}

## openSUSE Leap 15.3

Support for [openSUSE Leap 15.3][leap-15.3], released on Jun 2, 2021, ended on Dec 31, 2022, totaling 18 months of security and bugfix support.

There was a first announcement in advance on the [mailing list] on [November 30th][list-1], and then a second announcement in advance on the openSUSE news website on [December 12th][news]. Actually, still on January 16th there was an update for Leap 15.3 released by [SUSE], after which the [support for openSUSE Leap 15.3 was definitively ended][list-2]. Since then, openSUSE Leap 15.3 has received no further updates of any kind.

The [openSUSE Project][opensuse] recommends that openSUSE Leap 15.3 users upgrade to [openSUSE Leap 15.4][leap-15.4], released on Jun 8, 2022 and expected to be maintained until the end of November 2023, according to the [openSUSE wiki][lifetime].

By then, we will probably have openSUSE Leap 15.5 already, which is [scheduled][leap-15.5-roadmap] to be released on Jun 7, 2023. Therefore, when Leap 15.5 is released, Leap 15.4 users will have approximately 6 months to upgrade their installations.

## Linux Kamarada 15.3

Since Linux Kamarada is based on [openSUSE Leap][leap], Linux Kamarada users are by transitivity openSUSE Leap users and receive the same updates.

The end of support of all Linux Kamarada releases should normally occur alongside the end of support of the equivalent openSUSE Leap release.

As [Linux Kamarada 15.4][kamarada-15.4] was released today, support for [Linux Kamarada 15.3][kamarada-15.3], released on Dec 30, 2021, has been extended to today, but it ends today, Feb 27, 2023.

If you use Linux Kamarada 15.3, I recommend you to upgrade to [Linux Kamarada 15.4][kamarada-15.4], expected to be maintained for the same period of time as openSUSE Leap 15.4, that is, until the end of November 2023.

## How to upgrade

If you use openSUSE Leap (or Linux Kamarada) 15.3, you can find instructions on how to upgrade to 15.4 here:

- [Linux Kamarada and openSUSE Leap: how to upgrade from 15.3 to 15.4][upgrade]

Instructions on that how-to apply to both distributions.

If you have any questions, write in the comments for this text or the tutorial above. You can also check other support options on the [Help] page.

[leap-15.3]:            {% post_url en/2021-06-02-opensuse-leap-153-bridges-path-to-enterprise %}
[mailing list]:         https://en.opensuse.org/openSUSE:Mailing_lists_subscription
[list-1]:               https://lists.opensuse.org/archives/list/announce@lists.opensuse.org/thread/OCJDZIP63AUG4TW4W5JKR6TVWZ6N2TMT/
[news]:                 https://news.opensuse.org/2022/12/12/leap-153-to-reach-eol/
[SUSE]:                 https://www.suse.com/
[list-2]:               https://lists.opensuse.org/archives/list/announce@lists.opensuse.org/thread/57Z3SCL5R7ZW7UUFZDR6I27WD545YD2V/
[opensuse]:             https://www.opensuse.org/
[leap-15.4]:            {% post_url en/2022-06-08-leap-15-4-offers-new-features-familiar-stability %}
[lifetime]:             https://en.opensuse.org/Lifetime
[leap-15.5-roadmap]:    https://en.opensuse.org/openSUSE:Roadmap#Schedule_for_openSUSE_Leap_15.5
[leap]:                 {% post_url en/2020-12-07-opensuse-leap-vs-opensuse-tumbleweed-what-is-the-difference %}
[kamarada-15.4]:        {% post_url en/2023-02-27-linux-kamarada-15-4-more-functional-beautiful-polished-and-elegant-than-ever %}
[kamarada-15.3]:        {% post_url en/2021-12-30-linux-kamarada-15-3-new-year-new-release %}
[upgrade]:              {% post_url en/2023-01-16-linux-kamarada-and-opensuse-leap-how-to-upgrade-from-153-to-154 %}
[Help]:                 /en/help
