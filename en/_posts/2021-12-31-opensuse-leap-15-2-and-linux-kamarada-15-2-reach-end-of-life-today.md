---
date: '2021-12-31 03:00:00 GMT-3'
image: '/files/2021/12/kamarada-15.2-eol.jpg'
layout: post
nickname: 'kamarada-15.2-eol'
title: 'openSUSE Leap 15.2 and Linux Kamarada 15.2 reach end of life today'
---

{% include image.html src='/files/2021/12/kamarada-15.2-eol.jpg' %}

Support for [openSUSE Leap 15.2][leap-15.2], released on Jul 2, 2020, ends today, December 31, 2021, according to the [openSUSE wiki][lifetime]. This means that openSUSE Leap 15.2 will no longer receive security updates nor bugs fixes.&nbsp;

The [openSUSE Project][opensuse] recommends that Leap 15.2 users upgrade to [Leap 15.3][leap-15.3], released on July 2, 2021 and expected to be maintained until the end of November 2022, also according to the [openSUSE wiki][lifetime].

By then, we will probably have [openSUSE Leap 15.4][leap-15.4] already, which is [scheduled][leap-15.4-roadmap] to be released on June 8, 2022. Therefore, when Leap 15.4 is released, Leap 15.3 users will have approximately 6 months to upgrade their installations.

Since Linux Kamarada is based on openSUSE Leap, Linux Kamarada users are by transitivity openSUSE Leap users and receive the same updates.

Also, as I previously stated in the [Linux Kamarada 15.1 discontinuation notice][kamarada-15.1-eol], "each Linux Kamarada release will reach its end of life together with its openSUSE Leap counterpart".

Therefore, support for [Linux Kamarada 15.2][kamarada-15.2], released on September 11, 2020, also ends today, December 31, 2021.

If you still use Linux Kamarada 15.2, I recommend you to upgrade to [Linux Kamarada 15.3][kamarada-15.3], released yesterday and expected to be maintained for the same period of time as openSUSE Leap 15.3, that is, until the end of November 2022.

If you use openSUSE Leap (or Linux Kamarada) 15.2, you can find instructions on how to upgrade to 15.3 here:

- [Linux Kamarada and openSUSE Leap: how to upgrade from 15.2 to 15.3][upgrade]

Instructions on that how-to apply to both distributions.

If you have any questions, write in the comments for this text or the tutorial above. You can also check other support options on the [Help] page.

[leap-15.2]:                    {% post_url en/2020-07-02-opensuse-leap-15-2-release-brings-exciting-new-artificial-intelligence-ai-machine-learning-and-container-packages %}
[lifetime]:                     https://en.opensuse.org/Lifetime
[opensuse]:                     https://www.opensuse.org/
[leap-15.3]:                    {% post_url en/2021-06-02-opensuse-leap-153-bridges-path-to-enterprise %}
[leap-15.4]:                    https://en.opensuse.org/Portal:15.4
[leap-15.4-roadmap]:            https://en.opensuse.org/openSUSE:Roadmap#Schedule_for_openSUSE_Leap_15.4
[kamarada-15.1-eol]:            {% post_url en/2021-01-31-opensuse-leap-151-and-linux-kamarada-151-reach-end-of-life-today %}
[kamarada-15.2]:                {% post_url en/2020-09-11-linux-kamarada-15.2-come-to-the-elegant-modern-green-side-of-the-force %}
[kamarada-15.3]:                {% post_url en/2021-12-30-linux-kamarada-15-3-new-year-new-release %}
[upgrade]:                      {% post_url en/2021-12-23-linux-kamarada-and-opensuse-leap-how-to-upgrade-from-152-to-153 %}
[help]:                         /en/help
