---
date: '2021-05-20 10:30:00 GMT-3'
image: '/files/2021/05/im-element-pt.jpg'
layout: post
nickname: 'element-matrix'
title: 'The Element messenger (and the Matrix standard)'
---

{% include image.html src='/files/2021/05/im-element-pt.jpg' %}

[Element] is a [messaging app] that is the reference client for the [Matrix] network. Different from other more common messengers, such as [WhatsApp], [Telegram] or [Signal], which are centralized, Element and Matrix are decentralized, like [Session]. But also they are decentralized in a different way from Session: they are built on the idea of **[federation]**.

As much about Element is actually about Matrix, we are going to analyze both here.

**Federated systems** use multiple, independent servers that are able to talk to each other.

Email can be seen as a federated service. For example, let's suppose you have a [Gmail] account. You can send an email to another Gmail user. When you do that, your message does not leave the Gmail server, which simply copies your message to the receiver's inbox. But you can also send an email to a [Hotmail] user. In this case, the Gmail server sends your message to the Hotmail server, which in its turn delivers your message to the receiver. Besides that, when sending an email, you don't have to care what app the receiver is using: both of you may be using a webmail or an email client app such as [Thunderbird] or [Evolution].

Like email, Matrix is a protocol: an [open standard] for interoperable, secure, decentralized, real-time communication over IP. It can be used for instant messaging, VoIP/WebRTC calling, Internet of Things communication — or anything that needs a standard way for publishing and subscribing to data whilst tracking the conversation history. The Matrix protocol is maintained by the UK-based non-profit [Matrix.org Foundation][foundation], which also provides [open source reference implementations][matrix-source] of Matrix-compatible servers and client SDKs.

The Matrix open stardard allows you to host your own Matrix server or use a public one, either free or paid, hosted by someone else. You can send messages among users of the same Matrix server, as well as users of other Matrix servers. For instance, [matrix.org] is a public Matrix server provided by the Matrix.org Foundation for free for anyone to use.

{% include image.html src='/files/2021/05/matrix.png' %}

There are many [Matrix clients] available, Element being one of them.

Element is a free app developed by the for-profit company of the same name that makes flagship products and services on top of the Matrix standard. In the [past], the app was called Riot and the company, New Vector. Some of the founders of this company are also the founders of the Matrix.org Foundation. They created Element to bring Matrix to the mainstream.

Talking about the Element company, its revenue comes from two sources: Element Matrix Services — a hosting platform for Element (and other Matrix-based) messaging and collaboration deployments — and consulting services — large organisations often need specialist knowledge and support for such huge deployments.

Talking about the Element app, beyond its federated nature, it has most of the features you find in common messengers: it allows you to send text messages, photos, videos and files, in one-to-one or group chats, in addition to make voice and video calls.

Element features end-to-end encryption, but it's not enabled by default: you need to enable it manually for each chat by going to the chat settings.

You need an email address to register at [matrix.org], but you can disassociate it from your account later, if you want (what is not recommended by Element). Optionally, you can later associate your account with a phone number. You can have as many email addresses and phone numbers associated with your account as you like — including none. You create a username during registration, so you can keep your personal information confidential, if you prefer. People can find each other on Element by their username. You can also choose, for each email and phone number you registered, if you want to be discoverable by them.

Besides that, having an email associated with your account allows you to reset your password if you forget it. In case you have forgotten your password and you have not associated an email with your account, you won't be able to reclaim your account.

Like Telegram, you can use Element on multiple devices — computers, smartphones and tablets — at the same time, your messages sync seamlessly among them. You can even use Element from a web browser. That's possible because messages are stored on Matrix servers.

Also because messages are stored on servers, the app doesn't offer a message backup feature. What it does offer is to backup your encryption keys. If you enable key backup, your device will maintain a secure copy of its keys on the Matrix server. To ensure those keys can only ever be accessed by you, they are encrypted on your device before they are uploaded, so the server never sees them unencrypted. They can be encrypted with either a key that you store yourself or a security phrase.

Like other messengers, Element allows you to block the app based on a PIN or biometrics.

[Numbers] from Element and Matrix are impressive: more than 28 million users spread across more than 60,000 deployments worldwide. The largest public deployment has more than 7 million users, while the largest commercial deployment serves more than 500,000 users. Element and Matrix users include individuals, companies and governments.

Element is used by some well-known open source projects such as [Mozilla], [KDE], [GNOME] and [Gitter] because it has features for [developers], including syntax-highlighted code snippets, markdown support and programmable bots.

Interoperability is a key idea so present in Matrix that it can integrate to other services with **[bridges]**. There are Matrix bridges for WhatsApp, Facebook Messenger, Skype, Telegram, Signal, Slack, Rocket.Chat, Discord, IRC, just to mention a few. Bridges allow you to make your Matrix client an "all in one" messenger. But remember that these services may still track you somehow. So, if you worry about anonymity, maybe integrating them is not a good idea.

All Element apps are open source, you can find their source code on [GitHub].

[End-to-end encryption in Matrix][e2ee] (and thus Element) is based on the Olm implementation of the [Double Ratchet algorithm],  popularised by Signal. The Megolm implementation is used for group communications. Although the Element apps have not been formally audited, Olm and Megolm have been independently audited by [NCC Group].

You can get the Element app for Android from [Google Play] or from [F-Droid].

Regarding Linux, Element official packages are available for [Debian] and [Ubuntu] only. But it is possible to install it on openSUSE using Flatpak from [Flathub]:

```
# flatpak install im.riot.Riot
```

To use Element from a web browser, without installing any app, go to: [app.element.io](https://app.element.io/).

Element is also available for other systems. For more options, take a look at: [element.io](https://element.io/get-started).

Matrix has a very explanatory [FAQ] (frequently asked questions) page in its website. Element also has a very explanatory [Help] page.

This text is part of the series:

- [Open-source privacy-focused messaging apps alternatives to WhatsApp][messaging app]

[element]:                  https://element.io/
[messaging app]:            {% post_url en/2021-05-10-open-source-privacy-focused-messaging-apps-alternatives-to-whatsapp %}
[matrix]:                   https://matrix.org/
[whatsapp]:                 https://www.whatsapp.com/
[telegram]:                 {% post_url en/2021-05-10-the-telegram-messenger %}
[signal]:                   {% post_url en/2021-05-11-the-signal-messenger %}
[session]:                  {% post_url en/2021-05-17-the-session-messenger %}
[federation]:               https://en.wikipedia.org/wiki/Federation_(information_technology)
[gmail]:                    https://gmail.com/
[hotmail]:                  https://www.hotmail.com/
[thunderbird]:              https://www.thunderbird.net/
[evolution]:                https://wiki.gnome.org/Apps/Evolution
[open standard]:            https://matrix.org/docs/spec/
[foundation]:               https://matrix.org/foundation/
[matrix-source]:            https://github.com/matrix-org
[matrix.org]:               https://matrix.org/
[Matrix clients]:           https://matrix.org/clients/
[past]:                     https://element.io/blog/the-world-is-changing/
[numbers]:                  https://element.io/about
[Mozilla]:                  https://chat.mozilla.org/
[KDE]:                      http://webchat.kde.org/
[GNOME]:                    https://gnome.riot.im/
[Gitter]:                   https://gitter.im/
[developers]:               https://element.io/developers
[bridges]:                  https://matrix.org/bridges/
[github]:                   https://github.com/vector-im/
[e2ee]:                     https://matrix.org/docs/guides/end-to-end-encryption-implementation-guide
[Double Ratchet algorithm]: https://en.wikipedia.org/wiki/Double_Ratchet_Algorithm
[NCC Group]:                https://www.nccgroup.trust/globalassets/our-research/us/public-reports/2016/november/ncc_group_olm_cryptogrpahic_review_2016_11_01.pdf
[google play]:              https://play.google.com/store/apps/details?id=im.vector.app
[f-droid]:                  https://f-droid.org/packages/im.vector.app/
[debian]:                   https://www.debian.org/
[ubuntu]:                   https://ubuntu.com/
[flathub]:                  https://flathub.org/apps/details/im.riot.Riot
[faq]:                      https://matrix.org/faq/
[help]:                     https://element.io/help
