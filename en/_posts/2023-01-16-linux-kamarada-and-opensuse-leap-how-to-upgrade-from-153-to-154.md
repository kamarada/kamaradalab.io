---
date: '2023-01-16 11:00:00 GMT-3'
layout: post
published: true
title: 'Linux Kamarada and openSUSE Leap: how to upgrade from 15.3 to 15.4'
image: /files/2022/12/upgrade-to-15.4.jpg
nickname: 'upgrade-to-15.4'
---

{% include image.html src="/files/2022/12/upgrade-to-15.4.jpg" %}

Today you are going to see how to upgrade from [openSUSE Leap 15.3][leap-15.3] to [openSUSE Leap 15.4][leap-15.4]. Since Linux Kamarada is an [openSUSE Leap] based [Linux] distribution, this tutorial is also going to show how [Linux Kamarada 15.3][kamarada-15.3] users can upgrade their installations to [Linux Kamarada 15.4][kamarada-15.4-rc].&nbsp;

The same version numbers indicate the alignment between the distros and the procedure for upgrading them is very similar, so much so that they both fit in the same tutorial.

Today, the latest openSUSE Leap is [15.4][leap-15.4], released in [June][leap-15.4]. Previously, openSUSE Leap [15.3][leap-15.3] was released in [June last year][leap-15.3] and, according to the [openSUSE news website][opensuse-news], reached EOL (end of life) on the last day of 2022. Therefore, Leap 15.3 users are recommended to upgrade to Leap 15.4.

Linux Kamarada is currently at [15.3][kamarada-15.3], released in [December 2021][kamarada-15.3]. Linux Kamarada 15.4 is going to be released in the next few days. For now, we have [15.4 RC][kamarada-15.4-rc], which presents considerable stability already. In my tests, upgrading Linux Kamarada from 15.3 Final to 15.4 RC went flawlessly.

Since development is not finished yet, I don't recommend upgrading to Linux Kamarada 15.4 to all users yet. But, if you feel "adventurous" enough and want to help in development, please feel free to test the upgrade process and report the problems you face, if any.

{% capture final %}

Linux Kamarada 15.4 Final was released. Upgrading is recommended for all Linux Kamarada 15.3 users.

For more information about the new release, read:

- [Linux Kamarada 15.4: more functional, beautiful, polished and elegant than ever]({% post_url en/2023-02-27-linux-kamarada-15-4-more-functional-beautiful-polished-and-elegant-than-ever %})

{% endcapture %}

{% include update.html date="Feb 27, 2023" message=final %}

Upgrading from one openSUSE Leap release to the next is an easy and safe procedure. I always repeat the steps I learned reading the [openSUSE wiki][system-upgrade]. It's quite the same recipe since 2012, when I started using [openSUSE]. As Linux Kamarada is based on openSUSE Leap, upgrading it is done the same way, there is just one additional repository to setup.

In this post, you are going to see how to upgrade openSUSE Leap and Linux Kamarada from 15.3 to 15.4.

**Note:** not to be repetitive, what I say about openSUSE Leap applies to both distributions, when I mention Linux Kamarada I'm talking specifically about it.

{% capture packages %}

The official Linux Kamarada repository is no longer hosted on [OSDN](https://osdn.net/projects/kamarada/). For more information, read:

- [Changes to the Linux Kamarada infrastructure]({% post_url en/2023-02-15-changes-to-the-linux-kamarada-infrastructure %})

This tutorial has been updated in line with those changes.

{% endcapture %}

{% include update.html date="Feb 15, 2023" message=packages %}

## A few recommendations

Upgrading openSUSE is a safe procedure, which should work for most cases, but requires some cautions. It is always good to remember the following recommendations.

**You must be using the immediately previous openSUSE Leap release.** It means that if you want to upgrade to 15.4, now you must be using 15.3. Hopping over one or more releases is not supported. It may work, but that is not guaranteeded. So if you use openSUSE Leap 15.2 now, for example, you should first [upgrade to 15.3][upgrade-to-15.3], and then to 15.4.

**Your openSUSE Leap installation must be up to date** with the latest updates for the release you are currently running. There are differences between upgrading and updating. Here, you are going to see how to upgrade, but you may need to update first. For more information about those differences and how to update openSUSE Leap, please read:

- [How to get updates for openSUSE Linux][howto-update]

**You should read about the openSUSE Leap release you are going to install.** Take a look at the [release notes][release-notes], which list changes and glitches in the new release.

**You must backup all important data.** Even though upgrading openSUSE is a safe procedure (that resembles upgrading [Debian], for those who know it), it is not perfect. Winning the lottery is difficult, but eventually someone wins. Something may go wrong during upgrade, although it is difficult, especially if you act cautiosly. I myself have been using openSUSE for some years now, and always have upgraded from one release to the next without a problem. Anyway, it never hurts to make a backup of your personal files as a precaution, especially if the `/home` directory is not on a dedicated partition. If the computer is one that should not be down for long (a server, for example), consider backing up the entire system, so you will be able to restore it immediately if something does not work as expected.

**You should not use third party repositories during upgrade.** Before upgrading, we are going to remove all [third party repositories][third-party-repos]. Doing that may cause third party software to be removed during upgrade. If that happens, you may try to reinstall it later. It is not that you cannot upgrade with third party repositories enabled, but using only the [official repositories][official-repos] is more guaranteed. That way, the base system will be upgraded to a new stable, solid and reliable base. Then, on top of that base, you can install whatever you want. Do differently only if you know what you are doing.

I also recommend that you read this entire how-to before you start. I'm going to break the upgrade process down into 6 steps for better understading. Once you know the whole process, you will be able to better schedule it. You can even use your computer as normal while you follow the step by step, but after you start actually upgrading, your computer will become usable again only after upgrading is completed.

## 1) Update your current system

Make sure your current openSUSE Leap installation is up to date. To do this, run `zypper up` as the root (administrator) user. It should inform you there is "nothing to do":

{% include image.html src="/files/2023/01/upgrade-to-15.4-02-en.jpg" %}

If you need more information on how to update your system, please read:

- [How to get updates for openSUSE Linux][howto-update]

## 2) Backup your current repositories

Actually, this step is optional.

You may want to back up your current list of repositories to return them after upgrading. openSUSE Leap saves repository settings in the `/etc/zypp/repos.d` folder. It has some text files, one for each repository.

Let's copy the `repos.d` folder, in `/etc/zypp/`, to another one in the same place, called `repos.d.old`. It is easier to use the CLI for that.

Enter the CLI and switch to the root user, if you haven't done that yet:

```
$ su
```

Enter the root password to continue.

If you have already upgraded following any [previous edition of this same tutorial][upgrade-to-15.3], you may still have the old backup. If that is the case, remove the existing `repos.d.old` folder:

```
# rm -rf /etc/zypp/repos.d.old
```

Then, order the copy:

```
# cp -Rv /etc/zypp/repos.d /etc/zypp/repos.d.old
```

{% include image.html src="/files/2023/01/upgrade-to-15.4-03-en.jpg" %}

If you have not made a backup of your data yet, consider doing that now.

## 3) Clean up repositories

As already mentioned, for upgrade we use only the official repositories. To be more precise, just two of them, in the case of openSUSE Leap:

- **Main Repository**: the main repository, contains [open source software][oss] only.

URL: `http://download.opensuse.org/distribution/leap/$releasever/repo/oss/`

Calculated URL for 15.3: [http://download.opensuse.org/distribution/leap/15.3/repo/oss/](http://download.opensuse.org/distribution/leap/15.3/repo/oss/)

- **Main Update Repository**: contains official updates for OSS packages.

URL: `http://download.opensuse.org/update/leap/$releasever/oss/`

Calculated URL for 15.3: [http://download.opensuse.org/update/leap/15.3/oss/](http://download.opensuse.org/update/leap/15.3/oss/)

Note that the system automatically replaces the `$releasever` variable with the distribution release currently being used.

For Linux Kamarada, besides the above repos, we'll also use the official **Linux Kamarada** repo, previously hosted by [OSDN], and [now hosted at packages.linuxkamarada.com][packages]. So, note that in this case a [URL] change will be needed.

Old URL: `http://c3sl.dl.osdn.jp/storage/g/k/ka/kamarada/$releasever/openSUSE_Leap_$releasever/`

New URL: `https://packages.linuxkamarada.com/$releasever/openSUSE_Leap_$releasever/`

New URL calculated URL for 15.3: [https://packages.linuxkamarada.com/15.3/openSUSE_Leap_15.3/](https://packages.linuxkamarada.com/15.3/openSUSE_Leap_15.3/)

Let's clean up our repositories removing any repository we won't need (including any third party repositories).

Start the [YaST Control Center][yast]. To do this, click **Show Applications**, in the lower left corner of the screen, and click the **YaST** icon, at the end of the list:

{% include image.html src="/files/2023/01/upgrade-to-15.4-04-en.jpg" %}

You will be asked the root password. Type it and click **Continue**:

{% include image.html src="/files/2023/01/upgrade-to-15.4-05-en.jpg" %}

Within the **Software** category (the first one), click **Software Repositories**:

{% include image.html src="/files/2023/01/upgrade-to-15.4-06-en.jpg" %}

You are presented to your system's repository list:

{% include image.html src="/files/2023/01/upgrade-to-15.4-07-en.jpg" %}

For each repository other than the ones listed above, select the repository from the list and click **Delete**. YaST asks you for confirmation. Click **Yes**:

{% include image.html src="/files/2023/01/upgrade-to-15.4-08-en.jpg" %}

Do that with all repositories, until the list has only the repositories listed above:

{% include image.html src="/files/2023/01/upgrade-to-15.4-09-en.jpg" %}

Maybe those repositories are named differently on your system. If that is the case, try to recognize them by their URL instead of their name. If you don't find them, there is no problem: add them, providing the URLs above.

## 4) Switch to the new repositories

In previous editions of this how-to, in this step we used to edit the repositories one by one, replacing the previous release (15.3) with the new release (15.4). Now that we have the `$releasever` variable, this is no longer needed.

Just select each of your repos and make sure its **Raw URL** contains the `$releasever` variable:

{% include image.html src="/files/2023/01/upgrade-to-15.4-10-en.jpg" %}

If the release (currently `15.3`) appears somewhere in the **Raw URL**, click **Edit**. On the screen that appears, replace the release with `$releasever` and click **OK**:

{% include image.html src="/files/2023/01/upgrade-to-15.4-11-en.jpg" %}

In the case of **Linux Kamarada**, select the repo from the list and click **Edit**. On the screen that appears, replace the **URL of the Repository** with:

```
https://packages.linuxkamarada.com/$releasever/openSUSE_Leap_$releasever/
```

Note that the `$releasever` variable appears twice. Click **OK**.

{% include image.html src="/files/2023/01/upgrade-to-15.4-12-en.jpg" %}

At the end, your system's repository list should look like this:

{% include image.html src="/files/2023/01/upgrade-to-15.4-13-en.jpg" %}

Click **OK** to save the changes. You can close YaST.

## 5) Download packages

We can start downloading the packages of the new openSUSE Leap release.

Back to the CLI, retrieve the list of available packages from the new repositories (observe that we use the `--releasever=15.4` option to indicate the use of the new version):

```
# zypper --releasever=15.4 ref
```

Then, run:

```
# zypper --releasever=15.4 dup --download-only --allow-vendor-change
```

(`dup` is an acronym for *distribution upgrade*):

{% include image.html src="/files/2023/01/upgrade-to-15.4-14-en.jpg" %}

We use the `--allow-vendor-change` option because openSUSE Leap shares RPM packages with the [SUSE Linux Enterprise][sle] (not just the source code, as in previous releases, but even the already compiled, binary packages). Therefore, the **[zypper]** package manager would warn about many [vendor changes][vendor-change] during the upgrade. To accept them in advance, we add the `--allow-vendor-change` option.

The **zypper** package manager spends some time "thinking". Soon after, it shows what it needs to do in order to upgrade openSUSE Leap to the next release (which packages are going to be upgraded, which ones are going to be removed, which ones are going to change vendor, etc.) and asks whether you want to continue:

{% include image.html src="/files/2023/01/upgrade-to-15.4-15-en.jpg" %}

That list of actions is similar to that produced by `zypper up`, presented in the [updating how-to][howto-update]. However, as we are now doing a distribution upgrade, and not an ordinary update, the list of what is needed to be done is much bigger.

Review that list carefully. You can go up or down using the scrollbar at the right of the window or the mouse wheel (scroll wheel) if you are using **[Terminal]**, or the **Shift + Page Up** / **Shift + Page Down** key combinations if you are using a purely textual interface (they also work in **Terminal**).

The default option is to continue (**y**). If you agree, you can just hit **Enter** and downloading of the new packages will start. Meanwhile, you can use your computer normally. The upgrade itself has not started yet.

Note that **zypper** only downloads packages, it does not start upgrading:

{% include image.html src="/files/2023/01/upgrade-to-15.4-16-en.jpg" %}

That's because we invoked it with the `--download-only` option.

Finish what you are doing and save any open files so we can start upgrading. Note that upgrading may take several minutes and you will be able to continue using your computer only after it is complete.

## 6) Upgrade your system

Now that we have already downloaded all the packages needed to upgrade our openSUSE Leap from 15.3 to 15.4, we need to leave the desktop environment and use the pure textual interface to perform the upgrade. Here on, you cannot use the **Terminal**.

That is necessary because the desktop itself will be upgraded. If we run the upgrade while still using the desktop, it may stop/crash, causing the upgrade to abort, which in turn lefts the system in an inconsistent, unpredictable state.

Consider opening this page on another computer (or on a mobile device), print it or take note of the next actions.

If you are upgrading a laptop system, make sure its battery is fully charged and its AC adapter is plugged in. Do not remove the battery or unplug the AC adapter during the upgrade. We want to prevent any possibility of problem.

Logout by opening the **system menu**, on the right side of the top bar, clicking on your username and then choosing **Log Out**:

{% include image.html src="/files/2023/01/upgrade-to-15.4-17-en.jpg" %}

You are presented to the login screen where you could enter your username and password to start a new desktop session:

{% include image.html src="/files/2023/01/upgrade-to-15.4-18-en.jpg" %}

But that's not what we want. Press the **Ctrl + Alt + F1** key combination to switch to a purely textual interface:

{% include image.html src="/files/2022/12/upgrade-to-15.4-19.jpg" %}

If that was new to you, know that Linux provides six [consoles] (terminals) besides the graphical interface. You can use the keys **F1** to **F6** in that same combination (**Ctrl + Alt + F1**, **Ctrl + Alt + F2** and so on) to switch between consoles, pressing **Ctrl + Alt + F7** you can return to the graphical interface.

But let's stand on the first console.

Login as root. To do that, type `root` and hit **Enter**, then enter the root password and hit **Enter** again.

Let's switch from [runlevel] 5, the default runlevel, in which the system provides the GUI, to the runlevel 3, in which we have only the CLI and network connection.

To [change to runlevel][switch-runlevel] 3, run:

```
# init 3
```

Finally, we are going to start the distribution upgrade itself. Run: (mind the options we used before)

```
# zypper --no-refresh --releasever=15.4 dup --allow-vendor-change
```

{% include image.html src="/files/2022/12/upgrade-to-15.4-20.jpg" %}

The `--no-refresh` option makes **zypper** not retrieve the list of packages from repositories. Using that option, we ensure that **zypper** won't try to download any package in addition to those we have already downloaded. That can be useful especially for laptops, which may lose the Wi-Fi connection when leaving the GUI.

As before, **zypper** is going to compute the upgrade and show what it is going to do (if before it asked about changes and licenses, and now it repeats the same questions, answer them the same way as before):

{% include image.html src="/files/2023/01/upgrade-to-15.4-21-en.jpg" %}

Note that all the needed packages have already been downloaded: at the end, **zypper**'s report shows "Overall download size: 0 B. Already cached: 2.18 GiB."

Just hit **Enter** to start upgrading.

**zypper** skips downloading packages and goes straight to upgrading:

{% include image.html src="/files/2023/01/upgrade-to-15.4-22-en.jpg" %}

Distribution upgrade may take several minutes.

When it finishes, **zypper** suggests to restart. Let's do that by running:

```
# reboot
```

{% include image.html src="/files/2023/01/upgrade-to-15.4-23-en.jpg" %}

## That's it! Have a lot of fun!

If you did everything up to here, now your computer is running Linux Kamarada 15.4 (or openSUSE Leap 15.4) and ready for use:

{% include image.html src="/files/2023/01/upgrade-to-15.4-24-en.jpg" %}

Make sure everything is in place.

Now you may return any repositories you removed. Remember to make the appropriate adjustments with respect to the openSUSE Leap version, replacing it by `$releasever` where it appears. It's easy to do that using the CLI:

```
# sed -i 's/15.3/$releasever/g' /etc/zypp/repos.d.old/*
# mv /etc/zypp/repos.d.old/* /etc/zypp/repos.d/
```

That done, you can delete your repositories backup:

```
# rm -rf /etc/zypp/repos.d.old/
```

You may also try to install any program that perhaps has been removed during the upgrade.

## Troubleshooting

Linux Kamarada users have one small problem to fix when upgrading from 15.3 to 15.4: in my tests, after the upgrade, the **Terminal** shows a "command not found" error when it is started.

{% include image.html src="/files/2023/01/upgrade-to-15.4-25-en.jpg" %}

This error message is due to a change in the [Base16 Shell] scripts, which customize terminal colors in Linux Kamarada. Fresh Linux Kamarada 15.4 installations (done using the ISO image downloaded from the [Download] page) won't present this issue, but anyone upgrading from 15.3 to 15.4 will face it.

To fix this problem, open the `/etc/bash.bashrc.local` file as root. You can do that running the following command:

```
$ gnomesu gedit /etc/bash.bashrc.local
```

The opened file will have the following content:

```
# Base16 Shell
# https://github.com/chriskempson/base16-shell

BASE16_SHELL="/usr/share/base16-shell/"
[ -n "$PS1" ] && \
    [ -s "$BASE16_SHELL/profile_helper.sh" ] && \
        eval "$("$BASE16_SHELL/profile_helper.sh")"
```

Replace it with:

```
# Base16 Shell
# https://github.com/chriskempson/base16-shell

BASE16_SHELL="/usr/share/base16-shell"
[ -n "$PS1" ] && \
    [ -s "$BASE16_SHELL/profile_helper.sh" ] && \
        source "$BASE16_SHELL/profile_helper.sh"
```

(just out of curiosity, you can see the differences in [this commit][commit])

And save the file.

Close the **Terminal** and start it again. The problem should have been fixed.

## Conclusion

Remember to regularly [check for updates][howto-update] for your new distribution. Note that, after upgrading, the daily use of **zypper** does not require you to use the `--releasever` option .

If you find any difficulty during the upgrade or have any questions, don't hesitate to comment!

[leap-15.3]:            {% post_url en/2021-06-02-opensuse-leap-153-bridges-path-to-enterprise %}
[leap-15.4]:            {% post_url en/2022-06-08-leap-15-4-offers-new-features-familiar-stability %}
[Linux]:                https://www.kernel.org/linux.html
[openSUSE Leap]:        {% post_url en/2020-12-07-opensuse-leap-vs-opensuse-tumbleweed-what-is-the-difference %}
[kamarada-15.3]:        {% post_url en/2021-12-30-linux-kamarada-15-3-new-year-new-release %}
[kamarada-15.4-rc]:     {% post_url en/2022-12-19-linux-kamarada-15-4-enters-release-candidate-rc-phase %}
[opensuse-news]:        https://news.opensuse.org/2022/12/12/leap-153-to-reach-eol/
[system-upgrade]:       https://en.opensuse.org/SDB:System_upgrade
[openSUSE]:             https://www.opensuse.org/
[upgrade-to-15.3]:      {% post_url en/2021-12-23-linux-kamarada-and-opensuse-leap-how-to-upgrade-from-152-to-153 %}
[howto-update]:         {% post_url en/2019-05-20-how-to-get-updates-for-opensuse-linux %}
[release-notes]:        https://doc.opensuse.org/release-notes/x86_64/openSUSE/Leap/15.4/
[Debian]:               https://www.debian.org/
[third-party-repos]:    https://en.opensuse.org/Additional_package_repositories
[official-repos]:       https://en.opensuse.org/Package_repositories#Official_Repositories
[oss]:                  https://opensource.org/osd-annotated
[osdn]:                 https://osdn.net/
[packages]:             {% post_url en/2023-02-15-changes-to-the-linux-kamarada-infrastructure %}
[url]:                  https://en.wikipedia.org/wiki/Uniform_Resource_Locator
[yast]:                 http://yast.opensuse.org/
[sle]:                  https://www.suse.com/
[zypper]:               https://en.opensuse.org/Portal:Zypper
[vendor-change]:        https://en.opensuse.org/SDB:Vendor_change_update
[Terminal]:             https://wiki.gnome.org/Apps/Terminal
[consoles]:             http://opensuse-guide.org/command.php
[runlevel]:             https://en.wikipedia.org/wiki/Runlevel
[switch-runlevel]:      https://en.opensuse.org/SDB:Switch_runlevel
[Base16 Shell]:         https://github.com/chriskempson/base16-shell
[Download]:             /en/download
[commit]:               https://gitlab.com/kamarada/Linux-Kamarada-GNOME/-/commit/9dfc630d5e12fc6357aaf7aa4aaf2d4adfeff852
