---
date: '2024-08-14 01:00:00 GMT-3'
image: '/files/2021/04/patterns.jpg'
layout: post
nickname: 'patterns'
title: 'openSUSE: what are patterns and how to install them'
---

{% include image.html src='/files/2021/04/patterns.jpg' %}

In the [openSUSE] world, a **pattern** is a list of packages that can be installed to some purpose. Each pattern has a corresponding [RPM package][rpm], with a description of its purpose and a list of dependencies (which can be required, [recommended] or suggested packages). Patterns are designed to group related packages, but without inserting this grouping information into each package, in order to keep the each package's list of dependencies lean.

This concept also exists in openSUSE-based distributions such as [Linux Kamarada][kamarada-15.5].

Maybe it's easier to understand with examples. Let's say you want to install the [XFCE] desktop on your system. One option would be to search for all required packages and mark them all for installation. Another, more practical option is to install the XFCE pattern.

On openSUSE and Linux Kamarada, patterns can be installed using the **[zypper]** package manager. For example, you could install the XFCE pattern with the following command:

```
# zypper in -t pattern xfce
```

Another example would be the installation of a [LAMP server][lamp] -- popular jargon among system administrators and web developers, it refers to the combination of [Linux], [Apache], [MariaDB] (or [MySQL]) and [PHP] (or [Python]). One option would be to install Apache, then install MariaDB, then PHP... another option is to install the LAMP server pattern:

```
# zypper in -t pattern lamp_server
```

This example allows us to understand how patterns group packages, but without inserting this grouping information into each package, which would increase the list of dependencies for each package: Apache and PHP are related, but it would not make sense to list PHP as a required, recommended or even suggested package by Apache (not everyone who uses Apache uses PHP, it may be that the website to be served is made in Python, in another programming language or just in HTML). Likewise, it would not make sense to list Apache as a required, recommended or suggested package by PHP (not everyone who uses PHP uses Apache, PHP can also be used for command line scripts). But it makes sense to create a pattern -- the LAMP server -- that depends on both Apache and PHP.

Each pattern corresponds to an RPM package. The above commands would install the **patterns-xfce-xfce** and **patterns-server-lamp_server** packages on the system respectively.

To see the list of patterns you can install on your system, run:

```
# zypper search -t pattern
```

The list can be quite extensive:

```
Refreshing service 'openSUSE'.
Loading repository data...
Reading installed packages...

S | Name                 | Summary                                     | Type
--+----------------------+---------------------------------------------+--------
  | 32bit                | 32-Bit Runtime Environment                  | pattern
i | apparmor             | AppArmor                                    | pattern
i | base                 | Minimal Base System                         | pattern
  | basic_desktop        | A very basic desktop (previously part of -> | pattern
  | books                | Documentation                               | pattern
  | budgie               | Budgie Desktop Environment                  | pattern
  | budgie_applets       | Applets for Budgie Desktop Environment      | pattern
  | ceph_base            | Ceph base                                   | pattern
  | cinnamon             | Cinnamon Desktop Environment                | pattern
  | console              | Console Tools                               | pattern
  | deepin               | Deepin Desktop Environment                  | pattern
  | devel_basis          | Base Development                            | pattern
  | devel_C_C++          | C/C++ Development                           | pattern
  | devel_gnome          | GNOME Development                           | pattern
  | devel_java           | Java Development                            | pattern
  | devel_kde_frameworks | KDE Frameworks and Plasma Development       | pattern
  | devel_kernel         | Linux Kernel Development                    | pattern
  | devel_mono           | .NET Development                            | pattern
  | devel_osc_build      | Tools for Packaging with Open Build Service | pattern
  | devel_perl           | Perl Development                            | pattern
  | devel_python3        | Python 3 Developement                       | pattern
  | devel_qt5            | Qt 5 Development                            | pattern
  | devel_qt6            | Qt 6 Development                            | pattern
  | devel_rpm_build      | RPM Build Environment                       | pattern
  | devel_ruby           | Ruby Development                            | pattern
  | devel_tcl            | Tcl/Tk Development                          | pattern
  | devel_vulkan         | Vulkan Development                          | pattern
  | devel_web            | Web Development                             | pattern
  | devel_yast           | YaST Development                            | pattern
  | dhcp_dns_server      | DHCP and DNS Server                         | pattern
  | directory_server     | Directory Server (LDAP)                     | pattern
i | documentation        | Help and Support Documentation              | pattern
i | enhanced_base        | Enhanced Base System                        | pattern
  | enlightenment        | Enlightenment                               | pattern
  | file_server          | File Server                                 | pattern
  | fips                 | FIPS 140-2 specific packages                | pattern
i | fonts                | Fonts                                       | pattern
  | games                | Games                                       | pattern
  | gateway_server       | Internet Gateway                            | pattern
  | gnome                | GNOME Desktop Environment (Wayland)         | pattern
  | gnome_basic          | GNOME Desktop Environment (Basic)           | pattern
  | gnome_x11            | GNOME Desktop Environment (X11)             | pattern
  | imaging              | Graphics                                    | pattern
i | kamarada-gnome       | Linux Kamarada with GNOME desktop           | pattern
  | kde                  | KDE Applications and Plasma 5 Desktop       | pattern
  | kde_pim              | KDE PIM Suite                               | pattern
  | kde_plasma           | KDE Plasma 5 Desktop Base                   | pattern
  | kvm_server           | KVM Host Server                             | pattern
  | kvm_tools            | KVM Virtualization Host and tools           | pattern
  | lamp_server          | Web and LAMP Server                         | pattern
i | laptop               | Mobile                                      | pattern
  | lxde                 | LXDE Desktop Environment                    | pattern
  | lxqt                 | LXQt Desktop Environment                    | pattern
  | mail_server          | Mail and News Server                        | pattern
  | mate                 | MATE Desktop Environment                    | pattern
i | minimal_base         | Minimal Appliance Base                      | pattern
  | multimedia           | Multimedia                                  | pattern
  | network_admin        | Network Administration                      | pattern
  | non_oss              | Misc. Proprietary Packages                  | pattern
i | office               | Office Software                             | pattern
  | oracle_server        | Oracle Server Base                          | pattern
  | print_server         | Print Server                                | pattern
  | sap-bone             | SAP BusinessOne Server Base                 | pattern
  | sap-hana             | SAP HANA Server Base                        | pattern
  | sap-nw               | SAP NetWeaver Server Base                   | pattern
  | sap_server           | SAP Application Server Base                 | pattern
i | sw_management        | Software Management                         | pattern
  | sway                 | Sway Tiling Wayland Compositor and relate-> | pattern
  | technical_writing    | Technical Writing                           | pattern
  | update_test          | Tests for the Update Stack                  | pattern
  | wsl_base             | Base WSL packages                           | pattern
  | wsl_gui              | WSL GUI packages                            | pattern
  | wsl_systemd          | WSL systemd setup                           | pattern
i | x11                  | X Window System                             | pattern
i | x11_yast             | YaST User Interfaces                        | pattern
  | xen_server           | Xen Virtual Machine Host Server             | pattern
  | xen_tools            | XEN Virtualization Host and tools           | pattern
  | xfce                 | XFCE Desktop Environment                    | pattern
  | yast2_basis          | YaST Base Utilities                         | pattern
  | yast2_desktop        | YaST Desktop Utilities                      | pattern
  | yast2_server         | YaST Server Utilities                       | pattern
```

openSUSE and Linux Kamarada users can also list and install patterns via the graphical interface using the [YaST Control Center][yast] **Software Management** module.

To see the list of patterns that you can install on your system using this graphical tool, open the **View** menu in the upper-left corner of the screen and click **Patterns**:

{% include image.html src='/files/2024/08/patterns-01-en.jpg' %}

In the list on the left, you can select a pattern and, in the list on the right, see which packages are part of it:

{% include image.html src='/files/2024/08/patterns-02-en.jpg' %}

To install a pattern using YaST, check it in the list on the left. The packages required by this pattern are marked for installation:

{% include image.html src='/files/2024/08/patterns-03-en.jpg' %}

You can select more packages and/or patterns to install if you want. When finished, click the **Accept** button in the bottom-right corner to actually install them.

[opensuse]:         https://www.opensuse.org/
[rpm]:              https://en.wikipedia.org/wiki/RPM_Package_Manager
[recommended]:      {% post_url en/2021-04-07-what-are-recommended-packages-and-how-to-install-them-on-opensuse %}
[kamarada-15.5]:    {% post_url en/2024-05-27-linux-kamarada-15-5-more-aligned-with-opensuse-leap-and-other-distributions %}
[xfce]:             https://xfce.org/
[zypper]:           https://en.opensuse.org/Portal:Zypper
[lamp]:             https://en.wikipedia.org/wiki/LAMP_(software_bundle)
[Linux]:            https://www.kernel.org/linux.html
[apache]:           https://httpd.apache.org/
[mariadb]:          https://mariadb.com/
[mysql]:            https://www.mysql.com/
[php]:              https://www.php.net/
[python]:           https://www.python.org/
[html]:             https://en.wikipedia.org/wiki/HTML
[yast]:             http://yast.opensuse.org/
