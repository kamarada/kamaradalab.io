---
date: '2024-11-12 00:40:00 GMT-3'
layout: post
published: true
title: 'Linux Kamarada making of: updating packages for the next distribution release'
image: /files/2021/04/kick-off.jpg
nickname: 'making-of-updating'
excerpt: 'openSUSE Leap 15.6 was released in June, while Linux Kamarada is at 15.5, released in May. As I&apos;m developing the distribution&apos;s next release, I&apos;m writing this &quot;Linux Kamarada making of&quot; series, in which I try to share how this development is done.'
---

{% include image.html src="/files/2021/04/kick-off.jpg" %}

[openSUSE Leap 15.6][ol-156] was released in [June][ol-156], while Linux Kamarada is at [15.5][lk-155], released in [May][lk-155]. As I'm developing the distribution's next release, I'm writing this "Linux Kamarada making of" series, in which I try to share how this development is done. So far, this series has two articles already:

- [How to make an openSUSE-based distro (Linux Kamarada making of): getting started][making-of-1]
- [Building RPM packages with the Open Build Service (OBS)][making-of-2]

If you haven't read the previous articles yet, I recommend you start with them.

In the [first article][making-of-1], we saw how to start developing an [openSUSE]-based distribution by creating the repository on the [Open Build Service (OBS)][obs] and creating the first package by [integrating OBS with GitLab][obs-gitlab]. In the [second article][making-of-2], we saw how to create a package using just OBS, without using [GitLab].

Note that in both articles I started the development of the packages from scratch, to show how it is done. But since I have a previous release (Linux Kamarada 15.5) already built, I can reuse much of my previous work for the next release (Linux Kamarada 15.6), I don't need to start all over again from scratch.

On OBS, we can do this by creating **branches** of the packages.

If you're already used to [Git], this word will probably sound familiar to you. But the OBS concept of branch is actually analogous to the Git concept of [fork].

Typically, in OBS (as in Git) we use branches (or forks) to contribute to other projects: we access a package (repository), create a branch ( fork) of it in our own space, make changes, and push these changes back to the original package (repository).

To start a new version of a package in a new repository, we'll make a creative use of OBS branches. Without further ado, let's get started.

## Creating branches on OBS

In the [first article][making-of-1], we saw how to create the **[patterns-kamarada-gnome]** package from scratch. Today, we're going to do something different: we're going to branch the **[branding-kamarada]** package into the Linux Kamarada 15.6 project, so that we can reuse the previous work.

Begin by accessing the package in the previous release's project (in my case, the [**branding-kamarada**][branding-kamarada-15.5] package in the **[home:kamarada:15.5:dev]** project) and, in the action bar by the left, click **Branch Package**:

{% include image.html src="/files/2024/11/making-of-updating-01.jpg" %}

Expand **More options** and enter the project name (`home:kamarada:15.6:dev`) and the package name (`branding-kamarada`) for the branch:

{% include image.html src="/files/2024/11/making-of-updating-02.jpg" %}

Check the **Stay on this revision** option so that OBS won't update the branch if there are updates to the original package.

Also check the **Disable Autocleanup** option so that OBS won't delete the branch if it goes a while without receiving changes.

Finally, click the **Branch** button.

The [**branding-kamarada**][branding-kamarada-15.6] package is created in the **[home:kamarada:15.6:dev]** project.

Edit the `_service` file and update the `revision` parameter:

{% include image.html src="/files/2024/11/making-of-updating-03.jpg" %}

Note how you can edit this file from the OBS web interface. If you don't remember what this file is for, take a look at one of the following tutorials:

- [Integrating the Open Build Service with GitLab][obs-gitlab]
- [Integrating the Open Build Service with GitHub][obs-github]

## Unlinking the original package

As I said, we're making a creative use of OBS branches to take advantage of our previous work on previous versions of the packages. We don't want to contribute changes back to the original package, nor receive updates from it. So we're going to unlink the branch from the original package, making the new package independent.

Go back to the project page (the one that lists the packages) and notice that branched packages are tagged with **Link**:

{% include image.html src="/files/2024/11/making-of-updating-04.jpg" %}

Click the package in question and note that its page also shows that it is a branch (**Links to**):

{% include image.html src="/files/2024/11/making-of-updating-05.jpg" %}

Click **show unmerged sources** to show only the branch code, without mixing it with the original package code.

This brings up the `_link` file, which stores the link to the original package:

{% include image.html src="/files/2024/11/making-of-updating-06.jpg" %}

Delete this file and notice that the link indication disappears both on the package page:

{% include image.html src="/files/2024/11/making-of-updating-07.jpg" %}

And on the project page:

{% include image.html src="/files/2024/11/making-of-updating-08.jpg" %}

## Finishing

Finish configuring OBS integration with GitLab or [GitHub].

And remember to also update the spec and changes files.

Once this is done, you are ready to work on the next version of this package.

Now repeat this procedure for all other packages that you intend to update from one distribution release to the next.

[ol-156]:                   https://news.opensuse.org/2024/06/12/leap-unveils-choices-for-users/
[lk-155]:                   {% post_url en/2024-05-27-linux-kamarada-15-5-more-aligned-with-opensuse-leap-and-other-distributions %}
[making-of-1]:              {% post_url en/2024-10-28-how-to-make-an-opensuse-based-distro-linux-kamarada-making-of-getting-started %}
[making-of-2]:              {% post_url en/2024-11-06-building-rpm-packages-with-the-open-build-service-obs %}
[openSUSE]:                 https://www.opensuse.org/
[obs]:                      https://openbuildservice.org/
[obs-gitlab]:               {% post_url en/2021-03-15-integrating-the-open-build-service-with-gitlab %}
[GitLab]:                   https://gitlab.com/
[Git]:                      https://git-scm.com/
[fork]:                     https://git-scm.com/book/en/v2/GitHub-Contributing-to-a-Project
[patterns-kamarada-gnome]:  https://software.opensuse.org/package/patterns-kamarada-gnome
[branding-kamarada]:        https://software.opensuse.org/package/branding-kamarada
[branding-kamarada-15.5]:   https://build.opensuse.org/package/show/home:kamarada:15.5:dev/branding-kamarada
[home:kamarada:15.5:dev]:   https://build.opensuse.org/project/show/home:kamarada:15.5:dev
[branding-kamarada-15.6]:   https://build.opensuse.org/package/show/home:kamarada:15.6:dev/branding-kamarada
[home:kamarada:15.6:dev]:   https://build.opensuse.org/project/show/home:kamarada:15.6:dev
[obs-github]:               {% post_url en/2019-03-19-integrating-the-open-build-service-with-github %}
[GitHub]:                   https://github.com/
