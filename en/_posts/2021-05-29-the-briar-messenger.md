---
date: '2021-05-29 16:30:00 GMT-3'
image: '/files/2021/05/im-briar-pt.jpg'
layout: post
nickname: 'briar'
title: 'The Briar messenger'
---

{% include image.html src='/files/2021/05/im-briar-pt.jpg' %}

[Briar] is a [peer-to-peer] (P2P) fully end-to-end encrypted [messaging app] like [Jami]. It stores messages securely on the user's devices, not on the cloud. Briar was designed for activists, journalists, and anyone else who needs a safe, easy and robust way to communicate. It features one-to-one chats, private groups (in which only the creator of the group can invite new members), forums (public groups, in which any member can invite others) and blogs (where you can post news about your life, like traditional blogs or social networks profiles).

Briar doesn't rely on a central server — messages are synchronized directly between the users' devices. If the Internet is down, Briar can sync via proximity-based networks, where a connection is established over Wi-Fi or Bluetooth. Technology such as this has proven to be useful when Internet availability is an issue, as in times of crisis: in situations like that, Briar allows its users to keep information flowing. On the other hand, if the Internet is up, Briar can sync via the [Tor] network, protecting users and their relationships from surveillance.

{% include image.html src='/files/2021/05/briar-sharing.png' %}

Remember the test I did with Jami? I repeated the same test with Briar: without Internet access, I created two Briar accounts on two devices and sent messages from one to the other. With Briar, this test succeeded.

You don't provide any personal information (e.g. email address or phone number) when creating a Briar account, just a nickname (I'm not sure what it is used for) and a password (for encrypting your account's archive). The app creates a Briar link for you, which is basically your public key preceeded by `briar://`. It's via this link that other people can add you as their contact. Actually, you must add each other as contacts, so you need their Briar link as well.

To make things easier, Briar provides a QR Code you can show each other to scan, if you are nearby. To add a contact at distance, you can also copy and paste the Briar link of each other.

Similarly to Jami, your Briar account is stored only on your device: if you uninstall the app or forget your password, there's no way to recover your account. So, take care!

[Backing up a Briar account][backup] is not possible at the moment, but it is a planned feature.

Similarly to [Signal] and [Session], Briar offers a [screen lock] system. It uses the same password, pattern or fingerprint that you normally use to unlock your device.

Besides texting, Briar does not offer any other communication modals: audio or video messages, files and voice or video calls. But the developers long-term plans go far beyond messaging: they want to use Briar's data synchronization capabilities to support secure, distributed applications such as crisis mapping and collaborative document editing.

The Briar Project is formed by a [team] of programmers, computer scientists, hackers and freedom activists. Their goal is to enable people in any country to create safe spaces where they can debate any topic, plan events, and organise social movements.

The project is governed by a voluntary [board] and has received [funding] from foundations that fight against surveillance and censorship and in favor of human rights. There are also some ways to donate to the project listed on their [home page][briar].

At the moment, Briar is officially available for Android only. You can install the app from [Google Play], from [F-Droid] or [download the APK file directly from the Briar website][briar-apk].

Briar is a [free and open source software][foss]. You can find its source code on the [project's self-hosted GitLab][source]. Briar has [a reproducible build process][reproducer] that can prove that the application you downloaded from any of the above corresponds exactly to the published source code.

Briar was independently audited by [Cure53] in 2017. All the issues identified by the audit were fixed before the first public release of the app. Another audit is planned for 2023.

There is a Briar version for Linux being developed on the [project's GitLab][briar-gtk]. For now, it is possible to install it using Flatpak, but from its own repository, not from Flathub:

```
$ flatpak install --user https://flatpak.dorfbrunnen.eu/repo/appstream/app.briar.gtk.flatpakref
```

Briar has a comprehensive online [manual] describing all the available features and explaining how to use the app, as well as a [wiki] for technical details. Besides that, the wiki has a [FAQ].

This text is part of the series:

- [Open-source privacy-focused messaging apps alternatives to WhatsApp][messaging app]

[briar]:            https://briarproject.org/
[peer-to-peer]:     https://en.wikipedia.org/wiki/Peer-to-peer
[messaging app]:    {% post_url en/2021-05-10-open-source-privacy-focused-messaging-apps-alternatives-to-whatsapp %}
[jami]:             {% post_url en/2021-05-25-the-jami-messenger %}
[tor]:              https://www.torproject.org/
[backup]:           https://code.briarproject.org/briar/briar/-/wikis/FAQ#how-do-i-backup-my-account
[signal]:           {% post_url en/2021-05-11-the-signal-messenger %}
[session]:          {% post_url en/2021-05-17-the-session-messenger %}
[screen lock]:      https://briarproject.org/manual/#screen-lock
[team]:             https://briarproject.org/about-us/
[board]:            https://briarproject.org/governance
[funding]:          https://briarproject.org/about-us/#funding
[google play]:      https://play.google.com/store/apps/details?id=org.briarproject.briar.android
[f-droid]:          https://briarproject.org/installing-briar-via-f-droid/
[briar-apk]:        https://briarproject.org/installing-briar-via-direct-download/
[foss]:             https://en.wikipedia.org/wiki/Free_and_open-source_software
[source]:           https://code.briarproject.org/briar/briar/
[reproducer]:       https://code.briarproject.org/briar/briar-reproducer/-/blob/master/README.md
[Cure53]:           https://briarproject.org/raw/BRP-01-report.pdf
[briar-gtk]:        https://code.briarproject.org/briar/briar-gtk
[manual]:           https://briarproject.org/manual/
[wiki]:             https://code.briarproject.org/briar/briar/wikis
[faq]:              https://code.briarproject.org/briar/briar/-/wikis/FAQ
