---
date: '2024-12-05 22:00:00 GMT-3'
image: '/files/2024/11/shortwave-en.jpg'
layout: post
nickname: 'shortwave'
title: 'Shortwave: an Internet radio player for Linux with over 50,000 stations from around the world'
---

Do you like listening to radio? You may think that it's a media that is gradually losing ground to streaming services such as [Spotify], [Amazon Music], [Apple Music], [YouTube Music] and [many others]. However, [radio is still an important media][cnbc], especially for entertainment and news. Practically every car has a stereo able to tune in to radio stations, and more and more people are listening to radio over the Internet.

There is a [Linux] app that allows you to listen to radio. Today, we are going to talk about it.

**[Shortwave]** is an Internet radio player that lets you listen to over 50,000 radio stations from all over the world. It has a very simple, lightweight, intuitive and responsive interface, meaning it adapts to different screen sizes (for example, it's expected to work well on a [PinePhone]). Shortwave is a [free (_libre_) software][free-sw] and its source code is available on the [GNOME Project's GitLab][gitlab].

{% include image.html src='/files/2024/11/shortwave-en.jpg' %}

Below I will show you how to install and use Shortwave on Linux. I'm going to use [Linux Kamarada 15.5] as the reference Linux distribution.

## Installing Shortwave

The [recommended][gitlab] way to install Shortwave is via [Flatpak]. Click the button below to open the **Software** app and install Shortwave:

<p class='text-center'>
    <a class='btn btn-primary' href='https://dl.flathub.org/repo/appstream/de.haeckerfelix.Shortwave.flatpakref'>
        <i class="fa-solid fa-bag-shopping"></i> Install with Flatpak
    </a>
</p>

You can also open the **Software** app yourself, search for Shortwave and install it from there:

{% include image.html src='/files/2024/11/shortwave-01-en.jpg' %}

Or, if you prefer the terminal, you can install Shortwave by running:

```
$ flatpak install de.haeckerfelix.Shortwave
```

If you don't know what Flatpak is or need help with it, see:

- [Flatpak: what you need to know about the distro-agnostic package manager][Flatpak]

Right after installing, you should be able to launch Shortwave.

## Starting Shortwave

To start Shortwave, if you use the [GNOME] desktop, click **Activities**, by the upper-left corner of the screen, start typing `shortwave` and then click its icon:

{% include image.html src='/files/2024/11/shortwave-02-en.png' %}

This is the Shortwave home screen, without any radio stations for now:

{% include image.html src='/files/2024/11/shortwave-03-en.jpg' %}

## Listening to radio stations

Click the **Add New Stations** button (you can also use the button in the upper-left corner of the screen).

You can listen to one of the suggested **Popular Stations** or **Search stations**:

{% include image.html src='/files/2024/11/shortwave-04-en.jpg' %}

Enter the name of a radio station, city, state, country or any word to search for stations:

{% include image.html src='/files/2024/11/shortwave-05-en.jpg' %}

Click a radio station and you will see more information about it:

{% include image.html src='/files/2024/11/shortwave-06-en.jpg' %}

Click **Play** to listen to that station.

You can return to the previous screen by clicking the **Show Station Details** button:

{% include image.html src='/files/2024/11/shortwave-07-en.jpg' %}

Click **Add to Library** if you want this station to appear on the Shortwave home screen.

This is the Shortwave home screen after adding some radio stations:

{% include image.html src='/files/2024/11/shortwave-08-en.jpg' %}

## Adding an unlisted station

The radio stations listed by Shortwave are retrieved from the open online database [RadioBrowser], which can be populated by anyone (like [Wikipedia]). Anyone can add a radio station to this database, making it possible for others to discover that station.

But you don't need to limit yourself to this database, if you know a radio station that isn't listed in it.

To add that station to your Shortwave library so you can listen to it, open the menu in the upper-left corner of the screen and click **Add Local Station**:

{% include image.html src='/files/2024/11/shortwave-09-en.jpg' %}

Enter the **Name** and **Stream URL** of the radio station:

{% include image.html src='/files/2024/11/shortwave-10-en.jpg' %}

For example, I'm going to add the [Fan FM Aracaju][redefanfm] radio station, which, at the time of writing, is not listed in RadioBrowser:

{% include image.html src='/files/2024/11/shortwave-11-en.jpg' %}

How did I find out the stream URL? By "hacking" (inspecting) [the radio station's website][redefanfm]. To do that, some programming knowledge is needed. But I could also have contacted the radio station and asked for this information.

If everything goes well and you can listen to the radio station, consider adding it to [RadioBrowser], so that other people can easily find it through Shortwave or [other apps that use that same database][radio-browser-apps].

## GNOME integration

Being a [GNOME Circle app], Shortwave integrates seamlessly into that desktop. For example, you can control it from the notification list:

{% include image.html src='/files/2024/11/shortwave-12-en.jpg' %}

## Chromecast integration

Similar to [Google Chrome][Chromecast] and [VLC], Shortwave can also cast to the [Chromecast], allowing you to listen to radio stations on your TV.

To cast to a Chromecast, click the **Connect Device** button:

{% include image.html src='/files/2024/11/shortwave-13-en.jpg' %}

Your Chromecast device should appear listed, click it:

{% include image.html src='/files/2024/11/shortwave-14-en.jpg' %}

If the connection was successful, the name of the Chromecast device is shown on the Shortwave home screen and you can control it from there:

{% include image.html src='/files/2024/11/shortwave-15-en.jpg' %}

{% include image.html src='/files/2024/11/shortwave-16-en.jpg' %}

If something didn't work as expected, you may need to open ports in your computer's firewall to allow communication with the Chromecast. For more information, see:

- [Streaming from Linux to TV using Chromecast][Chromecast]

[Spotify]:              {% post_url en/2019-07-20-20-apps-you-can-use-the-same-way-on-both-linux-and-windows-part-1 %}#8-spotify
[Amazon Music]:         https://music.amazon.com/
[Apple Music]:          https://music.apple.com/
[YouTube Music]:        https://music.youtube.com/
[many others]:          https://en.wikipedia.org/wiki/Comparison_of_music_streaming_services
[cnbc]:                 https://www.cnbc.com/2024/01/05/radio-resilience-media.html
[Linux]:                https://www.kernel.org/linux.html
[Shortwave]:            https://apps.gnome.org/Shortwave/
[PinePhone]:            {% post_url en/2020-11-02-pinephone-the-linux-based-smartphone-everything-you-need-to-know-about-it %}
[free-sw]:              https://www.gnu.org/philosophy/free-sw.html
[gitlab]:               https://gitlab.gnome.org/World/Shortwave
[Linux Kamarada 15.5]:  {% post_url en/2024-05-27-linux-kamarada-15-5-more-aligned-with-opensuse-leap-and-other-distributions %}
[Flatpak]:              {% post_url en/2022-01-31-flatpak-what-you-need-to-know-about-the-distro-agnostic-package-manager %}
[GNOME]:                https://www.gnome.org/
[RadioBrowser]:         https://www.radio-browser.info/
[Wikipedia]:            https://www.wikipedia.org/
[redefanfm]:            https://redefanfm.com.br/ouca-ao-vivo/
[radio-browser-apps]:   https://www.radio-browser.info/users
[GNOME circle app]:     https://apps.gnome.org/en/#circle
[Chromecast]:           {% post_url en/2019-04-01-streaming-from-linux-to-tv-using-chromecast %}
[VLC]:                  {% post_url en/2020-04-04-streaming-from-vlc-to-tv-using-chromecast %}
