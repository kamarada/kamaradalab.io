---
date: '2024-07-24 21:00:00 GMT-3'
image: '/files/2024/07/vlc-remote-en.jpg'
layout: post
nickname: 'vlc-remote'
title: 'Use your smartphone as a VLC remote control'
---

Have you plugged your laptop to your TV, started a movie, sat on the couch and don't want to have to get up to pause the movie, turn the volume up/down, etc? How cool would it be to use your smartphone as a remote control? Well, if you use the **[VLC media player]**, that is possible! Today you are going to see how.

{% include image.html src='/files/2024/07/vlc-remote-en.jpg' %}

I use and recommend VLC because I consider it a multimedia "Swiss Army knife". Not only does it support all major media formats -- which would be good enough already -- but it also has many features, some of which its users may not even imagine, such as this: it is possible to [remotely control VLC][vlc-control] using [Android][vlc-android] or [iPhone][vlc-iphone] apps. I tested some of them and recommend **[VLC Mobile Remote]**.

As a reference for this how-to, I'm going to use a [Linux] laptop running [Linux Kamarada 15.5] (which comes with VLC installed out of the box), and an [Android 14] phone. But please note that VLC is also available for other systems, such as [Windows][VLC media player] and [macOS], and there is also VLC Mobile Remote for [iPhone][vlcmobileremote-iphone].

## Install the app on your smartphone

Start by installing the VLC Mobile Remote app on your phone:

<div class="row">
    <div class="col-md">
        <div class="image no-ads-here text-center mb-3">
            <a href="https://play.google.com/store/apps/details?id=adarshurs.android.vlcmobileremote" title="">
                <img src="/assets/img/google-play-pt.png" alt="" class="img-fluid" style="width: 200px;">
            </a>
        </div>
    </div>
    <div class="col-md">
        <div class="image no-ads-here text-center mb-3">
            <a href="https://apps.apple.com/us/app/remote-for-vlc-pc-mac/id1140931401?ls=1" title="">
                <img src="/assets/img/app-store-pt.svg" alt="" class="img-fluid" style="width: 200px;">
            </a>
        </div>
    </div>
</div>

When you've finished downloading and installing the app, open it.

Upon first use, it asks what is your computer's operating system:

{% include image.html src='/files/2024/07/vlcmobileremote-01-en.png' %}

Choose your computer's operating system (in my case, **Linux**).

On the following screens, the app presents a tutorial on how to set up VLC on your computer, but you are already following this tutorial. Go through the screens, swiping from right to left, until you reach the one with the **Auto connect** button:

<div class="row">
    <div class="col-md">
        {% include image.html src='/files/2024/07/vlcmobileremote-02-en.jpg' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2024/07/vlcmobileremote-03-en.jpg' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2024/07/vlcmobileremote-04-en.jpg' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2024/07/vlcmobileremote-05-en.jpg' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2024/07/vlcmobileremote-06-en.jpg' %}
    </div>
</div>

For now, let's leave the phone like that and go to the computer.

## Enable the VLC web interface

On your computer, open VLC. Go to **Tools > Preferences**:

{% include image.html src='/files/2024/07/vlcmobileremote-07-en.jpg' %}

The VLC preferences dialog open in **Simple** mode. In the bottom left corner of the screen, switch to **All**:

{% include image.html src='/files/2024/07/vlcmobileremote-08-en.jpg' %}

By the left, go to **Interface > Main interfaces**. By the right, under **Extra interface modules**, enable the **Web** module:

{% include image.html src='/files/2024/07/vlcmobileremote-09-en.jpg' %}

By the left, expand **Main interfaces** and select **Lua**. By the right, under **Lua HTTP**, set a **Password** (I recommend `1234`, so that the VLC Mobile Remote auto connection feature works) and, finally, click **Save**:

{% include image.html src='/files/2024/07/vlcmobileremote-10-en.jpg' %}

Now close VLC and reopen it. You can even open the movie you want to watch.

## Finish configuring your smartphone

Make sure both your computer and phone are connected to the same network.

On your phone, touch the **Auto connect** button.

VLC Mobile Remote now displays your computer's VLC controls. Optionally, you can **Allow** it to show notifications:

{% include image.html src='/files/2024/07/vlcmobileremote-11-en.jpg' %}

## Have a lot of fun!

Now you can control your computer's VLC from your smartphone, as in the image that opens this tutorial. You don't need to go through these settings again. Whenever you want to use your phone as a VLC remote control: connect your computer and phone to the same network, open VLC on your computer and VLC Mobile Remote on your phone (in that order).

Out of curiosity, the film I'm using as example is called [Wing It!] and was produced in 2023 using [Blender], a [free and open-source][foss] 3D computer graphics software tool. It's an [open source] film (you can download the [files that were used to produce it][wing-it-production files]). You can also [download the film][wing-it-download] to watch offline ([CC BY 4.0] license) or simply watch it on [YouTube]:

{% include youtube.html id="u9lj-c29dxI" %}

You can also take a look at other [Blender Studio productions][blender-studio].

[VLC media player]:         {% post_url en/2019-07-20-20-apps-you-can-use-the-same-way-on-both-linux-and-windows-part-1 %}#7-vlc
[vlc-control]:              https://wiki.videolan.org/Category:Control_VLC/
[vlc-android]:              https://wiki.videolan.org/Control_VLC_from_an_Android_Phone/
[vlc-iphone]:               https://wiki.videolan.org/Control_VLC_from_an_iPhone/
[VLC Mobile Remote]:        https://vlcmobileremote.com/
[Linux]:                    https://www.kernel.org/linux.html
[Linux Kamarada 15.5]:      {% post_url en/2024-05-27-linux-kamarada-15-5-more-aligned-with-opensuse-leap-and-other-distributions %}
[Android 14]:               https://www.android.com/android-14/
[macOS]:                    https://www.videolan.org/vlc/download-macosx.html
[vlcmobileremote-iphone]:   https://apps.apple.com/us/app/remote-for-vlc-pc-mac/id1140931401?ls=1
[Wing It!]:                 https://studio.blender.org/films/wing-it/
[Blender]:                  https://www.blender.org/
[free-sw]:                  https://www.gnu.org/philosophy/free-sw.html
[foss]:                     https://en.wikipedia.org/wiki/Free_and_open-source_software
[open source]:              https://en.wikipedia.org/wiki/Open_source
[wing-it-production files]: https://studio.blender.org/films/wing-it/3c308f54ee719e/?asset=6966
[wing-it-download]:         https://studio.blender.org/films/wing-it/3c402f7c9ab362/?asset=7023
[CC BY 4.0]:                https://creativecommons.org/licenses/by/4.0/
[YouTube]:                  https://www.youtube.com/watch?v=u9lj-c29dxI
[blender-studio]:           https://studio.blender.org/films/