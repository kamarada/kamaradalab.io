---
date: '2022-12-10 11:40:00 GMT-3'
layout: post
published: true
title: '2022 FIFA World Cup: how to sync the match schedule and scores to GNOME Calendar'
image: '/files/2022/12/qatar2022-gnome-calendar-en-08.jpg'
nickname: 'qatar2022-gnome-calendar'
---

What about having the [2022 FIFA World Cup][fifa] matches and their scores right on your [Linux] desktop? You can add all the games to the calendar so that you can organize your routine not to miss a fixture you really want to watch. Today you are going to see how to do that.

<!--more-->

Googling for public World Cup calendars available, I found [this one][jayasurian] made by front end engineer Jayasurian Makkoth ([Twitter], [GitHub]) for [Android] and [Mac] / [iPhone] devices. It is actually a [Google Calendar calendar][calendar-google-1] made public by him so that other people, even those who don't use [Google] services, can follow it.

[Google Calendar][calendar-google-2] exports this calendar as an [ICS file][ics], which can be synced by apps such as GNOME **[Calendar]**, present on [Linux Kamarada].

To open the GNOME Calendar, click **Activities**, by the upper-left corner of the screen, start typing `calendar`and click the application icon:

{% include image.html src='/files/2022/12/qatar2022-gnome-calendar-en-01.jpg' %}

Click the **Manage your calendars** icon and then the **Manage Calendars** option:

{% include image.html src='/files/2022/12/qatar2022-gnome-calendar-en-02.jpg' %}

Click **Add Calendar**:

{% include image.html src='/files/2022/12/qatar2022-gnome-calendar-en-03.jpg' %}

Copy the following link. To do so, right-click it and then click **Copy link** on the context menu (or the similarly named option provided by your browser):

- [Link to the ICS file][ics-file]

Paste it into the **New Calendar** dialog box, on the text field below **Import a Calendar**, and click the **Add** button:

{% include image.html src='/files/2022/12/qatar2022-gnome-calendar-en-04.jpg' %}

Back to the previous screen, note that the calendar is now listed, and the matches are now appearing:

{% include image.html src='/files/2022/12/qatar2022-gnome-calendar-en-05.jpg' %}

By clicking the name of the calendar, you can give it a new name and color:

{% include image.html src='/files/2022/12/qatar2022-gnome-calendar-en-06.jpg' %}

When finished, close this dialog to return to the **Calendar** main screen.

You can now easily show or hide the match schedule by clicking the **Manage your calendars** icon:

{% include image.html src='/files/2022/12/qatar2022-gnome-calendar-en-07.jpg' %}

Hover over a game, or click it, to see more info about it:

{% include image.html src='/files/2022/12/qatar2022-gnome-calendar-en-08.jpg' %}

The way we did it (by copying and pasting that link into the **Calendar** app), the app regularly looks for updates to the match schedule and syncs them.

An alternative would be to download the ICS file to the computer and import it into the app, but then updates made by its author would not be synced.

That's it! With this, you will be able to check the schedules and scores of all of the 65 matches of the biggest soccer tournament in the world easily right from the **Calendar** app on your Linux computer.

I hope you liked and enjoyed this tip. If you have any questions or suggestions, please, don't hesitate to comment!

[fifa]:                 https://www.fifa.com/fifaplus/en/tournaments/mens/worldcup/qatar2022
[Linux]:                https://www.kernel.org/linux.html
[jayasurian]:           https://jayasurian123.github.io/calendars/fifa-worldcup-2022/
[Twitter]:              https://twitter.com/jayasurian123
[GitHub]:               https://github.com/jayasurian123
[Android]:              https://www.android.com/
[Mac]:                  https://www.apple.com/mac/
[iPhone]:               https://www.apple.com/iphone/
[calendar-google-1]:    https://calendar.google.com/calendar/u/0?cid=bTI1dmdzYXJlOGlkZ2M0cDY0NDQ1cnEyNWNAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ
[Google]:               https://www.google.com
[calendar-google-2]:    https://calendar.google.com
[ics]:                  https://en.wikipedia.org/wiki/ICalendar
[Calendar]:             https://apps.gnome.org/en/app/org.gnome.Calendar/
[GNOME]:                https://www.gnome.org/
[Linux Kamarada]:       {% post_url en/2021-12-30-linux-kamarada-15-3-new-year-new-release %}
[ics-file]:             https://calendar.google.com/calendar/ical/m25vgsare8idgc4p64445rq25c%40group.calendar.google.com/public/basic.ics
