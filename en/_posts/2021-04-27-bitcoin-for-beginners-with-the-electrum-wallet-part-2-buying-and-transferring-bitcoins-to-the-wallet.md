---
date: 2021-04-27 23:30:00 GMT-3
image: '/files/2021/04/bitcoin-electrum-binance.jpg'
layout: post
published: true
nickname: 'bitcoin-electrum'
title: 'Bitcoin for beginners with the Electrum wallet — part 2: buying and transferring bitcoins to the wallet'
---

{% include image.html src="/files/2021/04/bitcoin-electrum-binance.jpg" %}

This is the part 2 of our trilogy of posts about how to transact bitcoins using the [Electrum] Bitcoin wallet. In part 1, you've seen what Bitcoin is and some basic concepts, how to install Electrum and set it up. If you fell into this page, I recommend you start reading at the first part:

- [Bitcoin for beginners with the Electrum wallet — part 1: basic concepts and setup][part-1]

Now that our Electrum wallet is ready to use, today let's see how to transfer bitcoins to it.

As I said, someone who already has bitcoins can transfer you some (in this case, this someone can follow today's post). Another option is: you can buy bitcoins on an exchange and transfer them to your wallet (whole bitcoins are very expensive today, I recommend you to buy satoshis to get started). This is the quickest way of getting bitcoins and usually the way people get started with it.

Also as I said, I'm going to use the [Binance] exchange as example, but you can buy bitcoins on any exchange you trust, the process of transferring them to Electrum should be similar.

I'll try not to dive into Binance details here, because I want to focus on Electrum, and also because Binance is an international exchange and how to use it may differ in your country. In case of doubt, you can refer to the [Binance Support][binance-support], which has lots of guides for everything.

I'm going to assume you have already [registered][binance-support-1] on Binance, enabled [two-factor authentication (2FA)][binance-support-2] and completed [identity verification][binance-support-3] — you are going to supply the exchange with some identity information, a process required by most exchanges today and known in general as [Know Your Customer (KYC)][kyc]. Some people see it as good  — a safety measure — others see it as bad — privacy threat. I'm not going to discuss that here.

Now, with no further ado, let's move on!

## Deposit: transferring fiat from bank to exchange

[**Fiat currency**][fiat] (also known as **fiduciary currency**) is any government-backed currency, such as the United States dollar (symbol: $; code: USD) or the Brazilian real (symbol: R$; code: BRL).

Binance allows credit card and wire transfer purchases of cryptocurrencies, depending on your country. Buying with a credit card is usually faster, but a little more expensive (up to 3.5% for USD purchases, according to [99Bitcoins]). So, I prefer to deposit fiat to Binance by bank transfer and then buy crypto with the cash balance. Fiat deposits are free of [fees] on Binance. You can find more information on the following texts on Binance Support:

- [How to get started with Fiat Funding][binance-support-4]
- [How to Buy Cryptos with Credit/Debit Card][binance-support-5]
- [Bank Transfer][binance-support-6]
- [How to deposit BRL on Binance][binance-support-7] (this probably interests Brazilian users only)

For the purpose of demonstration, I'm going to transfer R$ 1,000.00 (a thousand reais, or nearly $ 180, in today's exchange rate) to Binance. At first sight, it may seem a high amount, but a smaller amount may not be interesting, because of Bitcoin transaction fees.

I would like to remember a note from the beginning: the Bitcoin price is volatile, I recommend that you do not follow this tutorial to the letter if there is no need, as you may **lose money**.

To deposit fiat on Binance, once you are logged in, hover over **Buy Crypto** on the top navigation bar and then click **Bank Deposit**:

{% include image.html src="/files/2021/04/binance-fund-01.jpg" %}

Select your currency and payment method, enter the amount you are going to transfer as well as any other requested information:

{% include image.html src="/files/2021/04/binance-fund-02.jpg" %}

Make the transfer and wait for the Binance email informing your deposit has been received:

{% include image.html src="/files/2021/04/binance-fund-03.jpg" %}

Now your Binance account is funded with fiat and you can proceed to buy bitcoins.

## Buy: converting fiat into crypto

On Binance, you can buy bitcoins anytime, even on non-business hours and weekends.

To buy bitcoins with your fiat balance, hover over **Buy Crypto** on the top navigation bar and then click **Cash Balance**.

Select the fiat currency to spend (e.g. USD, EUR, BRL) and enter the amount (e.g. 1,000 BRL). Choose the crypto that you want to buy (in our case, **BTC**) and click **Buy**:

{% include image.html src="/files/2021/04/binance-buy-01.jpg" %}

Check transaction details and click **Confirm**:

{% include image.html src="/files/2021/04/binance-buy-02.jpg" %}

The purchase is executed immediately. Click **Go To Wallet** to see your BTC balance:

{% include image.html src="/files/2021/04/binance-buy-03.jpg" %}

{% include image.html src="/files/2021/04/binance-buy-04.jpg" %}

Another way to reach that same screen is **Wallet** > **Overview**:

{% include image.html src="/files/2021/04/binance-buy-05.jpg" %}

The following pages of the Binance Support can give you more information about the buying options offered by Binance:

- [Buy Crypto (Fiat/P2P)][binance-support-8]
- [How to Buy Crypto with BRL][binance-support-9]

## Custodial versus non-custodial wallets

As we have seen [previously][part-1-wallets], there are many [types of wallets][types-of-wallets]. And now it's a perfect time to explain another possible categorization for wallets: custodial versus non-custodial.

A **non-custodial wallet** is a wallet that allows you as its user to own the private key. No one other than yourself has access to the private key. Remember that bitcoin transactions need to be signed using the private key. This means that a non-custodial wallet gives you full control of your funds, as only you (with your private key) are able to transfer your bitcoins. Electrum is an example of a desktop non-custodial wallet.

A **custodial wallet** is similar to today's bank accounts where you keep your money. If you don't want to keep your bitcoins with you, you can keep them on a custodial wallet whose private keys are managed by a third party you trust. Any Bitcoin exchange like Binance is considered to be a web custodial wallet.

Note that a custodial wallet does not give you the private key, so you don't have full control of your funds. For instance, if the exchange goes offline for a while, you won't be able to use your bitcoins during the outage. Worse, exchanges are subject to hacker attacks. Other risks include the company might freeze your funds, go bankrupt or commit fraud and steal your coins. There is a popular saying in the crypto community: "not your keys, not your coins". You are advised not to keep your bitcoins on the exchange and transfer them to your own wallet.

Of course, non-custodial wallets aren't a perfect 100% safe solution. For instance, if your computer is taken away, you may lose your coins, if you don't have written down your seed.

It's up to you to decide where you are going to keep your bitcoins.

But following our how-to, let's transfer our newly purchased bitcoins from Binance to Electrum.

## Withdraw: transferring crypto from exchange to wallet

Bitcoin transactions can be made at any time, including non-business hours and weekends. Remember that on every Bitcoin transaction there is a fee to be paid to miners, so you can't transfer your total Bitcoin balance.

Binance does not charge additional [fees][binance-support-10] on top of that, just the transaction fee, which is estimated based on the current network transaction fees and cannot be changed by the user. Also, for each withdrawal request, Binance does not allow you to withdraw less than a minimum amount. Both the withdrawal fee and the minimum withdrawal are informed on the withdrawal page, but you can also check them visiting the [Fee Schedule page][fee].

To receive bitcoins on your Bitcoin wallet, you need an address. Open Electrum and select the **Receive** tab:

{% include image.html src="/files/2021/04/electrum-receive-01.jpg" %}

Optionally, give a **Description** to the transaction. Then, click **New Address** to receive one.

Copy the receiving address to the clipboard:

{% include image.html src="/files/2021/04/electrum-receive-02.jpg" %}

Open your Binance wallet (**Wallet** > **Overview**) and click **Withdraw** on the BTC line.

{% include image.html src="/files/2021/04/electrum-receive-03.jpg" %}

By the left, select the **Coin** you want to withdraw. Here, we are going to withdraw **BTC**.

By the right, on **Recipient's BTC Address**, paste the address copied from Electrum.

On **Transfer network**, select **Bitcoin (BTC)**.

Enter the withdrawal **Amount**. Here, I clicked **Max** (maximum).

Check the **Transaction Fee** and the amount **The receiver will get** (which is equal to the **Amount** you informed less the **Transaction Fee**). Click **Submit** when you are ready.

Click **Send Code** and Binance will send you an email with a verification code. Enter that code here to continue. Also, enter the 2FA code generated by your 2FA app. Pay attention to the warning on the screen: **withdrawals cannot be canceled once submited**. So, take a last look at the transaction information to ensure it is correct and click **Submit**:

{% include image.html src="/files/2021/04/electrum-receive-04.jpg" %}

That's it, we are done with Binance:

{% include image.html src="/files/2021/04/electrum-receive-05.jpg" %}

Usually exchanges don't send funds immediately, they often batch users transfers to save on fees, so you can expect a few minutes delay.

Wait for an email from Binance entitled **Withdrawal Successful**.

When you receive that email, you can go back to Electrum and switch to the **History** tab:

{% include image.html src="/files/2021/04/electrum-receive-06.jpg" %}

The transaction will initially be marked as **Unconfirmed**, indicating that it has been broadcast, but it has not yet been added to the blockchain. Next, you need to wait for miners to confirm the transaction so you can use the bitcoins. Note that the status bar displays **Balance: 0 mBTC**, as if there were no bitcoins on the wallet yet.

You can see transaction details by right clicking it and then clicking **View Transaction**:

{% include image.html src="/files/2021/04/electrum-receive-07.jpg" %}

The confirmation process takes a few minutes, but it's automatic: it does not require human intervention, only the work of the computers connected to the Bitcoin network (the miners).

After some confirmations (usually at least six), the transaction appears as confirmed, with a checkmark next to its timestamp:

{% include image.html src="/files/2021/04/electrum-receive-08.png" %}

That indicates that the transaction has been recorded into the blockchain and now you can use the bitcoins. Note the updated balance on the status bar.

In case you need more information about the Binance withdrawal process, please refer to the following pages of the Binance Support:

- [Crypto Withdrawal Fees on Binance][binance-support-10]
- [How do I Deposit/Withdraw Cryptocurrency][binance-support-11]

## To be continued...

In [part 3][part-3], you are going to see how to transfer bitcoins from Electrum to an exchange, in case you need to sell them (and how to do so).

{% capture part3 %}[Part 3 is here! (click)]({% post_url en/2021-05-03-bitcoin-for-beginners-with-the-electrum-wallet-part-3-transferring-from-the-wallet-and-selling-bitcoins %}){% endcapture %}
{% include update.html date="May 03, 2021" message=part3 %}

[electrum]:             https://electrum.org/
[part-1]:               {% post_url en/2021-04-23-bitcoin-for-beginners-with-the-electrum-wallet-part-1-basic-concepts-and-setup %}
[binance]:              https://bit.ly/kamarada-binance
[binance-support]:      https://www.binance.com/en/support
[binance-support-1]:    https://www.binance.com/en/support/faq/115003764911
[binance-support-2]:    https://www.binance.com/en/support/faq/c-1?navId=1#/1/11
[binance-support-3]:    https://www.binance.com/en/support/faq/360027287111
[kyc]:                  https://99bitcoins.com/bitcoin-exchanges/#kyc

[fiat]:                 https://en.wikipedia.org/wiki/Fiat_money
[99bitcoins]:           https://99bitcoins.com/bitcoin-exchanges/binance-review/#payment-methods
[fees]:                 https://www.binance.com/en/fee/schedule
[binance-support-4]:    https://www.binance.com/en/support/faq/9480499f26494ec7999b006dfa271170
[binance-support-5]:    https://www.binance.com/en/support/faq/8df758a570ba4d18941f38423f63aae5
[binance-support-6]:    https://www.binance.com/en/support/faq/c-66?navId=66#/66/75
[binance-support-7]:    https://www.binance.com/en/support/faq/53636607eebc42dbb513863b49bba98f

[binance-support-8]:    https://www.binance.com/en/support/faq/c-66?navId=66
[binance-support-9]:    https://www.binance.com/en/support/faq/b90065c7a7d44d0bb8b2ac7dc2cb0391

[part-1-wallets]:       {% post_url en/2021-04-23-bitcoin-for-beginners-with-the-electrum-wallet-part-1-basic-concepts-and-setup %}#bitcoin-wallets
[types-of-wallets]:     https://99bitcoins.com/bitcoin-wallet/#wallet-type

[fee]:                  https://www.binance.com/en/fee/depositFee
[binance-support-10]:   https://www.binance.com/en/support/faq/d9ff84dccc24423cbe6d59859d86afe3
[binance-support-11]:   https://www.binance.com/en/support/faq/85a1c394ac1d489fb0bfac0ef2fceafd
[part-3]:               {% post_url en/2021-05-03-bitcoin-for-beginners-with-the-electrum-wallet-part-3-transferring-from-the-wallet-and-selling-bitcoins %}
