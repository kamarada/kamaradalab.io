---
date: '2022-01-24 20:00:00 GMT-3'
image: '/files/2022/01/nomachine-rpi.jpg'
layout: post
nickname: 'nomachine-rpi'
title: 'Tip: installing NoMachine on the Raspberry Pi'
---

{% include image.html src='/files/2022/01/nomachine-rpi.jpg' %}

If you have installed [openSUSE on the Raspberry Pi][opensuse-rpi4] and intend to use it as a server, installing [NoMachine] on it can be useful for you to have easy remote access to it. If you intend to use your Raspberry Pi as a desktop, NoMachine can be useful as well to remotely access a server that has NoMachine installed.&nbsp;

[opensuse-rpi4]: {% post_url en/2020-12-26-opensuse-on-the-raspberry-pi-4-part-1-downloading-and-installing %}
[NoMachine]: {% post_url en/2020-03-28-remote-access-with-nomachine %}

Installing NoMachine on a Raspberry Pi is as easy as installing NoMachine on a desktop or server compatible with the IBM PC architecture. You just need to be aware that, when installing NoMachine on the Raspberry Pi 4, its architecture is different (ARMv8). Therefore, the RPM package that you are going to download is in a slightly different location.&nbsp;

And that's why I made this tip.

<!--more-->

Start by following the NoMachine how-to as usual until you reach the **Download** section:

- [Remote access with NoMachine][NoMachine]

Continue with the instructions below.

## Download

To download NoMachine for the Raspberry Pi, visit its website at [nomachine.com](https://www.nomachine.com/) and click **Other operating systems**:

{% include image.html src='/files/2022/01/nomachine-rpi-01.jpg' %}

Among the download options, in the **NoMachine for Raspberry Pi** section, click the **Download** link:

{% include image.html src='/files/2022/01/nomachine-rpi-02.jpg' %}

On the next page, choose the RPM package for the ARMv8 architecture in the Raspberry Pi 4 section:

{% include image.html src='/files/2022/01/nomachine-rpi-03.jpg' %}

On the next page, click **Download**:

{% include image.html src='/files/2022/01/nomachine-rpi-04.jpg' %}

If your browser asks what to do with the file (open or save), choose to **Open with** [YaST]:

[YaST]: http://yast.opensuse.org/

{% include image.html src='/files/2022/01/nomachine-rpi-05.jpg' %}

## Installation

When the download is finished, provide the root password to continue to the installation:

{% include image.html src='/files/2022/01/nomachine-rpi-06.jpg' %}

The installation procedure is similar, click **Accept** to get it started:

{% include image.html src='/files/2022/01/nomachine-rpi-07.jpg' %}

You can now continue with the NoMachine how-to by the **[Installation]** section.

[Installation]: {% post_url en/2020-03-28-remote-access-with-nomachine %}#installation

## Finding the server's IP address

Notice that NoMachine has added an icon to the [XFCE] status tray, at the bottom right of the screen. It is already running and ready to be used:

[XFCE]: https://www.xfce.org/

{% include image.html src='/files/2022/01/nomachine-rpi-08.jpg' %}
