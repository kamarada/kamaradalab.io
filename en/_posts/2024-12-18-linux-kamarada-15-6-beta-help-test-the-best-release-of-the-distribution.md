---
date: '2024-12-18 15:00:00 GMT-3'
image: '/files/2024/12/15.6-beta.jpg'
layout: post
nickname: 'kamarada-15.6-beta'
title: 'Linux Kamarada 15.6 Beta: help test the best release of the distribution'
---

{% include image.html src='/files/2024/12/15.6-beta.jpg' %}

The Linux Kamarada Project announces the 15.6 Beta release of the homonym [Linux distribution][linux], based on [openSUSE Leap 15.6][leap-15.6]. It is now available for [download].

<!--more-->

The [Download] page has been updated and now offer mainly two releases for download:

- 15.5 Final, which you can install on your home or work desktop; and
- 15.6 Beta, which you can test if you want to help in development.

openSUSE Leap 15.6 was released in [June][leap-15.6] and has since received various updates and fixes. Therefore, Linux Kamarada 15.6, although still in [beta] phase, should present considerable stability already. Still, bugs are expected and this version may not be ready for daily use. If you find a bug, please report it. Take a look at the [Help] page to see how to get in touch. Of course, bugs can be found and fixed anytime, but the sooner the better!

The easiest way to test Linux is using [VirtualBox]. For more information, please read:

- [VirtualBox: the easiest way to try Linux without installing it][VirtualBox]

Or, if you want to try Linux Kamarada on your own machine, the easiest way to do this is using a USB stick that you can make bootable with [Ventoy]:

- [Ventoy: create a multiboot USB drive by simply copying ISO images to it][Ventoy]

If you want to install Linux Kamarada on your computer for daily use, it is recommended that you install the 15.5 Final release, which is already ready, and then, when the 15.6 release is ready, upgrade. For more information on Linux Kamarada 15.5 Final, see:

- [Linux Kamarada 15.5: more aligned with openSUSE Leap and other distributions][kamarada-15.5]

## What's new?

What's new in the 15.6 release is:

- most of the same apps as in 15.5, but updated with new features and bug fixes;

- the most notable update was to the [GNOME] desktop, which on [Linux Kamarada 15.4][kamarada-15.4] and [15.5][kamarada-15.5] was at version 41, and is now at 45;

- the [GSConnect] GNOME extension is now installed and enabled by default, it is an implementation of the [KDE Connect] protocol for the GNOME desktop, which allows you to integrate your phone with your computer to display notifications from your phone on your computer and vice versa, share files and links, copy and paste between devices, among many other possibilities;

- you can now switch to a dark theme, instead of the default light theme;

- and, as usual, new beautiful wallpapers (but you can keep using the previous ones or the openSUSE defaults, if you prefer).

## Upgrading to 15.6 Beta

If you are already using Linux Kamarada 15.5 and trust your personal experience as an user, you may want to upgrade to Linux Kamarada 15.6 Beta to help test the upgrade process. Generally speaking, you should just need to follow this upgrade how-to, replacing `15.5` with `15.6`, where applicable:

- [Linux Kamarada and openSUSE Leap: how to upgrade from 15.4 to 15.5][upgrade-howto]

Since Linux Kamarada 15.5 repos are already setup with the `$releasever` variable, it should be pretty easy to upgrade to 15.6. Examples of commands from the tutorial above, already adapted for the new release:

```
# zypper --releasever=15.6 ref
# zypper --releasever=15.6 dup --download-only --allow-vendor-change
```

If you don't feel safe upgrading to 15.6 Beta with these tips, don't worry. Soon I will publish an updated version of the upgrade how-to.

## Specs

To allow comparison among Linux Kamarada releases, here is a summary of the software contained in Linux Kamarada 15.6 Beta Build 4.2:

- Linux kernel 6.4.0
- X.Org display server 21.1.11 (with Wayland 1.22.0 enabled by default)
- GNOME desktop 45.3 including its core apps, such as Files (previously Nautilus), Calculator, Terminal, Text editor (gedit), Screenshot and others
- LibreOffice office suite 24.8.1.2
- Mozilla Firefox 128.5.1 ESR (default web browser)
- Chromium 131 web browser (alternative web browser)
- VLC media player 3.0.21
- Evolution email client 3.50.3
- YaST control center
- Brasero 3.12.3
- CUPS 2.2.7
- Drawing 1.0.2
- Firewalld 2.0.1
- GParted 1.5.0
- HPLIP 3.23.8
- Java (OpenJDK) 11.0.25
- KeePassXC 2.7.9
- Linphone 5.0.16
- PDFsam Basic 5.2.9
- Pidgin 2.14.8
- Python 3.6.15 and 3.11.10
- Samba 4.19.8
- Tor 0.4.8.11
- Transmission 4.0.5
- Vim 9.1.0836
- Wine 9.0
- Calamares installer 3.2.62
- Flatpak 1.14.6
- games: AisleRiot (Solitaire), Chess, Mahjongg, Mines (Minesweeper), Nibbles (Snake), Quadrapassel (Tetris), Reversi, Sudoku

That list is not exhaustive, but it gives a notion of what can be found in the distribution.

[linux]:            https://www.kernel.org/linux.html
[leap-15.6]:        {% post_url en/2024-06-12-leap-15-6-unveils-choices-for-users %}
[download]:         /en/download
[beta]:             https://en.wikipedia.org/wiki/Software_release_life_cycle#Beta
[Help]:             /en/help
[virtualbox]:       {% post_url en/2019-10-10-virtualbox-the-easiest-way-to-try-linux-without-installing-it %}
[ventoy]:           {% post_url en/2020-07-29-ventoy-create-a-multiboot-usb-drive-by-simply-copying-iso-images-to-it %}
[kamarada-15.5]:    {% post_url en/2024-05-27-linux-kamarada-15-5-more-aligned-with-opensuse-leap-and-other-distributions %}
[GNOME]:            https://www.gnome.org/
[kamarada-15.4]:    {% post_url en/2023-02-27-linux-kamarada-15-4-more-functional-beautiful-polished-and-elegant-than-ever %}
[GSConnect]:        https://extensions.gnome.org/extension/1319/gsconnect/
[KDE Connect]:      https://userbase.kde.org/KDEConnect
[upgrade-howto]:    {% post_url en/2023-11-27-linux-kamarada-15-4-and-opensuse-leap-15-4-how-to-upgrade-to-15-5 %}
