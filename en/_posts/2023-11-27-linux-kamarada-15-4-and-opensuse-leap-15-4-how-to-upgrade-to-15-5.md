---
date: '2023-11-27 23:59:00 GMT-3'
layout: post
published: true
title: 'Linux Kamarada 15.4 and openSUSE Leap 15.4: how to upgrade to 15.5'
image: /files/2023/11/upgrade-to-15.5.jpg
nickname: 'upgrade-to-15.5'
---

{% include image.html src="/files/2023/11/upgrade-to-15.5.jpg" %}

Today you are going to see how to upgrade from [openSUSE Leap 15.4][leap-15.4] to [openSUSE Leap 15.5][leap-15.5]. Since Linux Kamarada is an [openSUSE Leap] based [Linux] distribution, this tutorial is also going to show how [Linux Kamarada 15.4][kamarada-15.4] users can upgrade their installations to [Linux Kamarada 15.5][kamarada-15.5].&nbsp;

The same version numbers indicate the alignment between the distros and the procedure for upgrading them is very similar, so much so that they both fit in the same tutorial.

Today, the latest openSUSE Leap is [15.5][leap-15.5], released in [June][leap-15.5]. Previously, openSUSE Leap [15.4][leap-15.4] was released in [June last year][leap-15.4] and is expected to be supported until the turn of the year. Therefore, Leap 15.4 users are recommended to upgrade to Leap 15.5.

Linux Kamarada is currently at [15.4][kamarada-15.4], released in [February][kamarada-15.4]. Linux Kamarada 15.5 is going to be released soon. For now, we have [15.5 Beta][kamarada-15.5-beta], which presents considerable stability already. In my tests, upgrading Linux Kamarada from 15.4 Final to 15.5 Beta went flawlessly.

Since development is not finished yet, I don't recommend upgrading to Linux Kamarada 15.5 to all users yet. But, if you feel "adventurous" enough and want to help in development, please feel free to test the upgrade process and report the problems you face, if any.

{% capture final %}

Linux Kamarada 15.5 Final was released. Upgrading is recommended for all Linux Kamarada 15.4 users.

For more information about the new release, read:

- [Linux Kamarada 15.5: more aligned with openSUSE Leap and other distributions]({% post_url en/2024-05-27-linux-kamarada-15-5-more-aligned-with-opensuse-leap-and-other-distributions %})

{% endcapture %}

{% include update.html date="May 27, 2024" message=final %}

Upgrading from one openSUSE Leap release to the next is an easy and safe procedure. I always repeat the steps I learned reading the [openSUSE wiki][system-upgrade]. It's quite the same recipe since 2012, when I started using [openSUSE]. As Linux Kamarada is based on openSUSE Leap, upgrading it is done the same way, there is just one additional repository to setup.

In this post, you are going to see how to upgrade openSUSE Leap and Linux Kamarada from 15.4 to 15.5.

**Note:** not to be repetitive, what I say about openSUSE Leap applies to both distributions, when I mention Linux Kamarada I'm talking specifically about it.

## A few recommendations

Upgrading openSUSE is a safe procedure, which should work for most cases, but requires some cautions. It is always good to remember the following recommendations.

**You must be using the immediately previous openSUSE Leap release.** It means that if you want to upgrade to 15.5, now you must be using 15.4. Hopping over one or more releases is not supported. It may work, but that is not guaranteeded. So if you use openSUSE Leap 15.3 now, for example, you should first [upgrade to 15.4][upgrade-to-15.4], and then to 15.5.

**Your openSUSE Leap installation must be up to date** with the latest updates for the release you are currently running. There are differences between upgrading and updating. Here, you are going to see how to upgrade, but you may need to update first. For more information about those differences and how to update openSUSE Leap, please read:

- [How to get updates for openSUSE Linux][howto-update]

**You should read about the openSUSE Leap release you are going to install.** Take a look at the [release notes][release-notes], which list changes and glitches in the new release.

**You must backup all important data.** Even though upgrading openSUSE is a safe procedure (that resembles upgrading [Debian], for those who know it), it is not perfect. Winning the lottery is difficult, but eventually someone wins. Something may go wrong during upgrade, although it is difficult, especially if you act cautiosly. I myself have been using openSUSE for some years now, and always have upgraded from one release to the next without a problem. Anyway, it never hurts to make a backup of your personal files as a precaution, especially if the `/home` directory is not on a dedicated partition. If the computer is one that should not be down for long (a server, for example), consider backing up the entire system, so you will be able to restore it immediately if something does not work as expected.

**You should not use third party repositories during upgrade.** Before upgrading, we are going to remove all [third party repositories][third-party-repos]. Doing that may cause third party software to be removed during upgrade. If that happens, you may try to reinstall it later. It is not that you cannot upgrade with third party repositories enabled, but using only the [official repositories][official-repos] is more guaranteed. That way, the base system will be upgraded to a new stable, solid and reliable base. Then, on top of that base, you can install whatever you want. Do differently only if you know what you are doing.

I also recommend that you read this entire how-to before you start. I'm going to break the upgrade process down into 6 steps for better understading. Once you know the whole process, you will be able to better schedule it. You can even use your computer as normal while you follow the step by step, but after you start actually upgrading, your computer will become usable again only after upgrading is completed.

## 1) Update your current system

Make sure your current openSUSE Leap installation is up to date. To do this, run `zypper up` as the root (administrator) user. It should inform you there is "nothing to do":

{% include image.html src="/files/2023/01/upgrade-to-15.4-02-en.jpg" %}

If you need more information on how to update your system, please read:

- [How to get updates for openSUSE Linux][howto-update]

## 2) Backup your current repositories

Actually, this step is optional.

You may want to back up your current list of repositories to return them after upgrading. openSUSE Leap saves repository settings in the `/etc/zypp/repos.d` folder. It has some text files, one for each repository.

Let's copy the `repos.d` folder, in `/etc/zypp/`, to another one in the same place, called `repos.d.old`. It is easier to use the command line interface (CLI) for that.

Enter the CLI and switch to the root user, if you haven't done that yet:

```
$ su
```

Enter the root password to continue.

If you have already upgraded following any [previous edition of this same tutorial][upgrade-to-15.4], you may still have the old backup. If that is the case, remove the existing `repos.d.old` folder:

```
# rm -rf /etc/zypp/repos.d.old
```

Then, order the copy:

```
# cp -Rv /etc/zypp/repos.d /etc/zypp/repos.d.old
```

{% include image.html src="/files/2023/01/upgrade-to-15.4-03-en.jpg" %}

If you have not made a backup of your data yet, consider doing that now.

## 3) Clean up repositories

As already mentioned, for upgrade we use only the official repositories. To be more precise, just two of them, in the case of openSUSE Leap:

- **Main Repository**: the main repository, contains [open source software][oss] only.

URL: `http://download.opensuse.org/distribution/leap/$releasever/repo/oss/`

Calculated URL for 15.4: [http://download.opensuse.org/distribution/leap/15.4/repo/oss/](http://download.opensuse.org/distribution/leap/15.4/repo/oss/)

- **Main Update Repository**: contains official updates for OSS packages.

URL: `http://download.opensuse.org/update/leap/$releasever/oss/`

Calculated URL for 15.4: [http://download.opensuse.org/update/leap/15.4/oss/](http://download.opensuse.org/update/leap/15.4/oss/)

Note that the system automatically replaces the `$releasever` variable with the distribution release currently being used.

For Linux Kamarada, besides the above repos, we'll also use the official **Linux Kamarada** repo.

URL: `https://packages.linuxkamarada.com/$releasever/openSUSE_Leap_$releasever/`

URL calculated URL for 15.4: [https://packages.linuxkamarada.com/15.4/openSUSE_Leap_15.4/](https://packages.linuxkamarada.com/15.4/openSUSE_Leap_15.4/)

Let's clean up our repositories removing any repository we won't need (including any third party repositories).

Start the [YaST Control Center][yast]. To do this, click **Show Applications**, in the lower left corner of the screen, and click the **YaST** icon, at the end of the list:

{% include image.html src="/files/2023/11/upgrade-to-15.5-01-en.jpg" %}

You will be asked the root password. Type it and click **Continue**:

{% include image.html src='/files/2023/11/openSUSE-repos-Leap-02-en.jpg' %}

Within the **Software** category (the first one), click **Software Repositories**:

{% include image.html src="/files/2023/01/upgrade-to-15.4-06-en.jpg" %}

You are presented to your system's repository list:

{% include image.html src="/files/2023/11/upgrade-to-15.5-02-en.jpg" %}

For each repository other than the ones listed above, select the repository from the list and click **Delete**. YaST asks you for confirmation. Click **Yes**:

{% include image.html src="/files/2023/11/upgrade-to-15.5-03-en.jpg" %}

Do that with all repositories, until the list has only the repositories listed above:

{% include image.html src="/files/2023/11/upgrade-to-15.5-04-en.jpg" %}

Maybe those repositories are named differently on your system. If that is the case, try to recognize them by their [URL] instead of their name. If you don't find them, there is no problem: add them, providing the URLs above.

## 4) Switch to the new repositories

In previous editions of this how-to, in this step we used to edit the repositories one by one, replacing the previous release (15.4) with the new release (15.5). Now that we have the `$releasever` variable, this is no longer needed.

Just select each of your repos and make sure its **Raw URL** contains the `$releasever` variable:

{% include image.html src="/files/2023/11/upgrade-to-15.5-05-en.jpg" %}

If the release (currently `15.4`) appears somewhere in the **Raw URL**, click **Edit**. On the screen that appears, replace the release with `$releasever` and click **OK**:

{% include image.html src="/files/2023/11/upgrade-to-15.5-06-en.jpg" %}

In the case of **Linux Kamarada**, note that the `$releasever` variable appears twice:

{% include image.html src="/files/2023/11/upgrade-to-15.5-07-en.jpg" %}

When finished, click **OK** to save the changes and exit the **Software Repositories** module. You can also close YaST.

## 5) Download packages

We can start downloading the packages of the new openSUSE Leap release.

Back to the CLI, retrieve the list of available packages from the new repositories (observe that we use the `--releasever=15.5` option to indicate the use of the new version):

```
# zypper --releasever=15.5 ref
```

Then, run:

```
# zypper --releasever=15.5 dup --download-only --allow-vendor-change
```

(`dup` is an acronym for *distribution upgrade*):

{% include image.html src="/files/2023/11/upgrade-to-15.5-08-en.jpg" %}

We use the `--allow-vendor-change` option because openSUSE Leap shares RPM packages with the [SUSE Linux Enterprise][sle] (not just the source code, as in previous releases, but even the already compiled, binary packages). Therefore, the **[zypper]** package manager would warn about many [vendor changes][vendor-change] during the upgrade. To accept them in advance, we add the `--allow-vendor-change` option.

The **zypper** package manager spends some time "thinking". Soon after, it shows what it needs to do in order to upgrade openSUSE Leap to the next release (which packages are going to be upgraded, which ones are going to be removed, which ones are going to change vendor, etc.) and asks whether you want to continue:

{% include image.html src="/files/2023/11/upgrade-to-15.5-09-en.jpg" %}

That list of actions is similar to that produced by `zypper up`, presented in the [updating how-to][howto-update]. However, as we are now doing a distribution upgrade, and not an ordinary update, the list of what is needed to be done is much bigger.

Review that list carefully. You can go up or down using the scrollbar at the right of the window or the mouse wheel (scroll wheel) if you are using **[Terminal]**, or the **Shift + Page Up** / **Shift + Page Down** key combinations if you are using a purely textual interface (they also work in **Terminal**).

The default option is to continue (**y**). If you agree, you can just hit **Enter** and downloading of the new packages will start. Meanwhile, you can use your computer normally. The upgrade itself has not started yet.

Note that **zypper** only downloads packages, it does not start upgrading:

{% include image.html src="/files/2023/11/upgrade-to-15.5-10-en.jpg" %}

That's because we invoked it with the `--download-only` option.

Finish what you are doing and save any open files so we can start upgrading. Note that upgrading may take several minutes and you will be able to continue using your computer only after it is complete.

## 6) Upgrade your system

Now that we have already downloaded all the packages needed to upgrade our openSUSE Leap from 15.4 to 15.5, we need to leave the desktop environment and use the pure textual interface to perform the upgrade. Here on, you cannot use the **Terminal**.

That is necessary because the desktop itself will be upgraded. If we run the upgrade while still using the desktop, it may stop/crash, causing the upgrade to abort, which in turn lefts the system in an inconsistent, unpredictable state.

Consider opening this page on another computer (or on a mobile device), print it or take note of the next actions.

If you are upgrading a laptop system, make sure its battery is fully charged and its AC adapter is plugged in. Do not remove the battery or unplug the AC adapter during the upgrade. We want to prevent any possibility of problem.

Logout by opening the **system menu**, on the right side of the top bar, clicking on your username and then choosing **Log Out**:

{% include image.html src="/files/2023/11/upgrade-to-15.5-11-en.jpg" %}

You are presented to the login screen where you could enter your username and password to start a new desktop session:

{% include image.html src="/files/2023/11/upgrade-to-15.5-12-en.jpg" %}

But that's not what we want. Press the **Ctrl + Alt + F1** key combination to switch to a purely textual interface:

{% include image.html src="/files/2023/11/upgrade-to-15.5-13.png" %}

If that was new to you, know that Linux provides six [consoles] (terminals) besides the graphical interface. You can use the keys **F1** to **F6** in that same combination (**Ctrl + Alt + F1**, **Ctrl + Alt + F2** and so on) to switch between consoles, pressing **Ctrl + Alt + F7** you can return to the graphical interface.

But let's stand on the first console.

Log in as root. To do that, type `root` and hit **Enter**, then enter the root password and hit **Enter** again.

Let's switch from [runlevel] 5, the default runlevel, in which the system provides the GUI, to the runlevel 3, in which we have only the CLI and network connection.

To [change to runlevel][switch-runlevel] 3, run:

```
# init 3
```

Finally, we are going to start the distribution upgrade itself. Run: (mind the options we used before)

```
# zypper --no-refresh --releasever=15.5 dup --allow-vendor-change
```

{% include image.html src="/files/2023/11/upgrade-to-15.5-14.png" %}

The `--no-refresh` option makes **zypper** not retrieve the list of packages from repositories. Using that option, we ensure that **zypper** won't try to download any package in addition to those we have already downloaded. That can be useful especially for laptops, which may lose the Wi-Fi connection when leaving the GUI.

As before, **zypper** is going to compute the upgrade and show what it is going to do (if before it asked about changes and licenses, and now it repeats the same questions, answer them the same way as before):

{% include image.html src="/files/2023/11/upgrade-to-15.5-15-en.png" %}

Note that all the needed packages have already been downloaded: at the end, **zypper**'s report shows "Overall download size: 0 B. Already cached: 2.15 GiB."

Just hit **Enter** to start upgrading.

**zypper** skips downloading packages and goes straight to upgrading:

{% include image.html src="/files/2023/11/upgrade-to-15.5-16-en.png" %}

Distribution upgrade may take several minutes.

When it finishes, **zypper** suggests to restart. Let's do that by running:

```
# reboot
```

{% include image.html src="/files/2023/11/upgrade-to-15.5-17-en.png" %}

## Almost there

If you did everything up to here, now your computer is running Linux Kamarada 15.5 (or openSUSE Leap 15.5) and it is almost ready for use:

{% include image.html src="/files/2023/11/upgrade-to-15.5-18-en.jpg" %}

Make sure everything is in place.

Why did I say the system is almost ready? Because there is a new feature of the 15.5 release, which is the **[openSUSE-repos-Leap]** package, which makes it easier to set up the official openSUSE repositories. It is recommended (although not strictly necessary) that you install this package. For more information, read:

- [Install the new openSUSE-repos-Leap package][openSUSE-repos-Leap]

Now you may return any repositories you removed. Remember to make the appropriate adjustments with respect to the openSUSE Leap version, replacing it by `$releasever` where it appears. It's easy to do that using the CLI:

```
# sed -i 's/15.4/$releasever/g' /etc/zypp/repos.d.old/*
# mv /etc/zypp/repos.d.old/* /etc/zypp/repos.d/
```

That done, you can delete your repositories backup:

```
# rm -rf /etc/zypp/repos.d.old/
```

You may also try to install any program that perhaps has been removed during the upgrade.

## That's it! Have a lot of fun!

Remember to regularly [check for updates][howto-update] for your new distribution. Note that, after upgrading, the daily use of **zypper** does not require you to use the `--releasever` option .

If you find any difficulty during the upgrade or have any questions, don't hesitate to comment!

[leap-15.4]:            {% post_url en/2022-06-08-leap-15-4-offers-new-features-familiar-stability %}
[leap-15.5]:            {% post_url en/2023-06-07-leap-15-5-release-matures-sets-up-technological-transition %}
[openSUSE Leap]:        {% post_url en/2020-12-07-opensuse-leap-vs-opensuse-tumbleweed-what-is-the-difference %}
[Linux]:                https://www.kernel.org/linux.html
[kamarada-15.4]:        {% post_url en/2023-02-27-linux-kamarada-15-4-more-functional-beautiful-polished-and-elegant-than-ever %}
[kamarada-15.5]:        {% post_url en/2023-11-01-linux-kamarada-15-5-beta-help-test-the-best-release-of-the-distribution %}
[kamarada-15.5-beta]:   {% post_url en/2023-11-01-linux-kamarada-15-5-beta-help-test-the-best-release-of-the-distribution %}
[system-upgrade]:       https://en.opensuse.org/SDB:System_upgrade
[openSUSE]:             https://www.opensuse.org/
[upgrade-to-15.4]:      {% post_url en/2023-01-16-linux-kamarada-and-opensuse-leap-how-to-upgrade-from-153-to-154 %}
[howto-update]:         {% post_url en/2019-05-20-how-to-get-updates-for-opensuse-linux %}
[release-notes]:        https://doc.opensuse.org/release-notes/x86_64/openSUSE/Leap/15.5/
[Debian]:               https://www.debian.org/
[third-party-repos]:    https://en.opensuse.org/Additional_package_repositories
[official-repos]:       https://en.opensuse.org/Package_repositories#Official_Repositories
[oss]:                  https://opensource.org/osd-annotated
[yast]:                 http://yast.opensuse.org/
[url]:                  https://en.wikipedia.org/wiki/Uniform_Resource_Locator
[sle]:                  https://www.suse.com/
[zypper]:               https://en.opensuse.org/Portal:Zypper
[vendor-change]:        https://en.opensuse.org/SDB:Vendor_change_update
[Terminal]:             https://wiki.gnome.org/Apps/Terminal
[consoles]:             http://opensuse-guide.org/command.php
[runlevel]:             https://en.wikipedia.org/wiki/Runlevel
[switch-runlevel]:      https://en.opensuse.org/SDB:Switch_runlevel
[openSUSE-repos-Leap]:  {% post_url en/2023-11-13-install-the-new-opensuse-repos-leap-package %}
