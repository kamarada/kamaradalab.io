---
date: 2021-04-23 01:30:00 GMT-3
image: '/files/2021/04/bitcoin-electrum-binance.jpg'
layout: post
published: true
nickname: 'bitcoin-electrum'
title: 'Bitcoin for beginners with the Electrum wallet — part 1: basic concepts and setup'
---

{% include image.html src="/files/2021/04/bitcoin-electrum-binance.jpg" %}

[Bitcoin] is a digital currency that only exists electronically. There are no physical, touchable bitcoins. Released as a [free and open-source software][foss] in 2009, Bitcoin has mainly been used for [speculation][reuters], but it also has been adopted as a [protection against government-backed currencies devaluation][reuters]. There are a growing number of [stores and services accepting Bitcoin][spend] all over the world (e.g. [Namecheap]). There are also projects that accept donations in bitcoins (e.g [Wikipedia]). It seems Bitcoin is here to stay.

In this tutorial, you are going to learn how to transact bitcoins using the [Electrum] Bitcoin wallet. I decided to split it into three parts:

1. in this first part, I'll try to briefly explain a little bit about Bitcoin, then you are going to see how to install a Bitcoin wallet on your computer and set it up;
2. in the [second part][part-2], you will see how to buy bitcoins on an exchange and transfer them to your wallet;
3. in the [third part][part-3], you will see how to transfer bitcoins from your wallet to an exchange and sell them.

For future reference, here I use the Linux Kamarada distribution version 15.2, released on [Sep 11, 2020][kamarada-15.2], and the Electrum Bitcoin wallet version 4.1.2, released on [Apr 8, 2021][electrum-release-notes].

**Notes:**

1. There are people who make money **speculating** in Bitcoin, buying or selling it as it appreciates or depreciates. I advance that it's **not** the objective of this tutorial to teach you how to do that. This is not an investment website. If that's what you're looking for, I recommend looking for other contents.

2. Due to the high volatility of the Bitcoin price, if you follow the steps in this tutorial all at once, you may gain or **lose money**, be aware of that. I recommend that you read the entire how-to just to learn how Bitcoin and Electrum work and then use them according to your real needs.

## Understanding how Bitcoin works

Bitcoin was created in 2008 by Satoshi Nakamoto, who wrote [a paper explaining how the currency works][bitcoin-paper]. Out of curiosity, Satoshi Nakamoto is a pseudonym, nobody knowns if he is actually a person or a group of people.

{% include image.html src="/files/2018/12/bitcoin-paper.jpg" caption="The paper that introduced the concept of Bitcoin, available for reading [here](https://bitcoin.org/bitcoin.pdf)." %}

The Bitcoin Project aims to develop a **decentralized** currency, which is not controlled by governments or banks, is not dependent on a kind of central authority to be reliable, nor is it affected by political events. The Bitcoin currency depends only on the user network itself to exist. For this reason, it is called a **[peer-to-peer]** (P2P) currency.

If you already know the [BitTorrent] network, which is also a peer-to-peer network, but used to share files, probably you are going to understand the Bitcoin network more easily.

Unlike traditional paper currencies, which are printed by central banks, bitcoins cannot be printed: they exist only electronically. They also cannot be downloaded, because they are not files. In fact, what travels on the network are bitcoin **transactions**. All transactions are announced to all of the network users, a process called [broadcasting].

A transaction is a transfer of value between **Bitcoin wallets**. Conceptually, these wallets are similar to the traditional ones in which we carry notes, coins and credit cards. But remember that bitcoins cannot be stored, just transferred. Technically, Bitcoin wallets have an **address**, from or to which are transferred bitcoins, and a **private key** (also called a **seed**), which is a secret information used to sign transactions. The **signature** provides a mathematical proof that the transaction was actually made by the owner of the wallet and also prevents the transaction from being altered by anybody once it has been issued.

Private key and signature are [cryptography] concepts, which is one of the Bitcoin pillars. It's thanks to cryptography that Bitcoin transactions can be secured by the parties involved, unlike traditional currency transactions, which are secured by third parties (usually banks).

Just as in the BitTorrent network a file exists while users store and share that file, in the Bitcoin network a transaction exists from the moment that users **confirm** that transaction. After confirmation by several users (usually at least six), the transaction is recorded in a gigantic ledger that records all Bitcoin transactions that have ever occurred in history, called the **blockchain**. This ledger is built synchronously, distributed by several network users and also uses encryption, so it's difficult to tamper with it.

The process of confirming transactions is called **mining**. It takes work, because it involves heavy encryption operations. It consumes time, processing, disk space, electricity and, therefore, has a cost. Those who transfer bitcoins pay a small **fee** to those who confirm the transaction. Whoever needs their transaction to be confirmed faster can offer a higher fee. The higher the fee, the higher the transaction is prioritized and sooner it is written onto the blockchain.

[Mining may be a profitable endeavor][mining] if you live in a region with cheap electricity and have the needed equipment. It was once possible to do at home with powerful CPUs or GPUs. But as the network evolved, successive cryptographic operations have made confirmations more and more difficult, so that now a specialized hardware is needed.

If you don't have any bitcoins yet and want to get started with Bitcoin, the first step is to choose a wallet, which will provide you an address that you can use to receive bitcoins. Someone who already has bitcoins can transfer you some (as a payment for goods and services, or even as a donation) or you can **buy** bitcoins. To do this, you need to register on an **exchange** and transfer some money to it (e.g. dollars or euros). Bitcoin exchanges are analogous to currency exchanges where people can exchange traditional paper currencies (e.g. convert Brazilian reais to dollars and vice versa).

If all that explanation seemed confusing, don't worry: we'll see in practice next.

Here we have not exhausted the subject of Bitcoin, which is quite interesting. To learn more, you can visit the [Bitcoin Project website][bitcoin], which has a lot of information, [read the Bitcoin paper][bitcoin-paper] and search [DuckDuckGo] or [Google]. At a more general level, you can search for **[cryptocurrencies]** (also called "cryptos"), which are cryptography-based currencies, just like Bitcoin, which was the first cryptocurrency. Bitcoin inspired several other cryptocurrencies that emerged after it proposing some improvement.

## Bitcoin price

Just as the dollar's value (in terms of its exchange rate versus other currencies, such as the euro) fluctuates, the bitcoin's value also fluctuates according to the market ([supply and demand law][supply-and-demand]).

The best way to find out how much 1 bitcoin (also abbreviated BTC or <i class="fab fa-btc"></i>) is worth compared to the [fiat currency][fiat] you use (e.g. USD or $) is to take a look at exchange websites.

But if you don't need the quote in real time and just want to have an idea, it may be more practical to search [DuckDuckGo][duckduckgo-2] or [Google][google-2]. Try searching e.g. "btc usd" (without quotes):

{% include image.html src="/files/2021/04/btc-usd-google.jpg" %}

At the time of writing, 1 bitcoin is oscillating between 50,000 and 60,000 dollars.

As I live in Brazil, I note that 1 bitcoin is oscillating between 300,000 and 360,000 Brazilian reais (BRL or R$).

## Bitcoin divisions

The Bitcoin currency can be divided into into smaller units, with up to 8 decimal places.

The smallest amount within Bitcoin is called **satoshi** (sat), named in homage to its creator.

<pre>1 BTC = 100,000,000 satoshis (sats)
1 satoshi (sat) = 1/100,000,000 BTC = 0.00000001 BTC</pre>

Thus, even if the value of 1 BTC was high, it would be possible to represent any value. If 1 BTC was worth 1 million dollars, 1 satoshi would be worth 1 cent.

If 1 BTC is worth 50,000 dollars, 1 satoshi is worth 0.0005 dollars (less than 1 cent of dollar).

Another common unit is the **millibitcoin** (mBTC), which is a thousandth of Bitcoin.

<pre>1 BTC = 1,000 mBTCs
1 mBTC = 1/1,000 BTC = 0.001 BTC = 100,000 sats</pre>

If 1 BTC is worth 50,000 dollars, 1 mBTC is worth 50 dollars.

## Bitcoin exchanges

There are many exchanges where you can buy and sell bitcoins in exchange for other currencies. Some of them are listed on [bitcoin.org][exchanges]. You can find others searching.

When choosing an exchange, it's important that you take into account some factors such as: countries supported, safety, privacy and security features, accepted payment methods, exchange rate, transaction fees, trading volume, buying limits, reputation, customer support, among others. For more information, you can read these texts: [Cointelegraph] and [99Bitcoins]. You can search for more information on other websites too. Perform your own due diligence.

To demonstrate how you can convert fiat currency into Bitcoin and transfer it to a Bitcoin wallet (and the other way around too), I'm going to use the Binance exchange as example.

[Binance] is a Hong Kong-based cryptocurrency exchange that accepts users from around the world. It supports Bitcoin as well as other cryptos, allows buying cryptos via bank transfer or credit/debit card and is the leading exchange in terms of volume according to [CoinMarketCap]. Binance is up since 2017 and has received [favorable reviews][binance-review].

Of course, to buy bitcoins and transfer them to a Bitcoin wallet any exchange will do, but in case you decide to go on with Binance, please use [my referral link][binance] to create your account, as that rewards me a commission and helps me keeping this project and website.

{% include image.html src="/files/2021/04/binance.jpg" caption="Binance: the world&apos;s largest crypto exchange (in terms of volume according to CoinMarketCap)" %}

{% include update.html date='Apr 27, 2021' message='If you live in the United States, access Binance via [Binance.US](https://www.binance.us/).' %}

## Bitcoin wallets

There are many [types of wallets][types-of-wallets], each with its advantages and disadvantages: desktop applications, mobile applications, hardware wallets, web wallets and paper wallets. The Bitcoin Project website ([bitcoin.org][bitcoin-wallets]) lists some available wallets. Among them, wallets that are desktop applications, work on [Linux] and have open source include:

- [Armory]
- [Bitcoin Core][bitcoin-core]
- [Bitcoin Knots][bitcoinknots]
- [Bither]
- [BitPay]
- [Electrum]
- [Specter]
- [Wasabi]

For new Bitcoin users, the Bitcoin Project website recommends BitPay or Electrum.

So I chose to use Electrum because it is [cross-platform] (available for Linux, [Android], [Windows] and [macOS]) and has good documentation both on its [website][electrum] and [elsewhere][bitzuma-electrum]. In addition, Electrum provides the developer's public key, making it possible to [verify the authenticity of the downloaded software][how-to-verify-iso]. It's always important to verify this, especially in the case of security-critical programs, such as a Bitcoin wallet. Electrum's source code is available on [GitHub] and receives frequent updates, another important point to consider.

{% include image.html src="/files/2018/12/electrum.jpg" %}

## Installing the Electrum Bitcoin wallet

I've recently made a tutorial showing how to install Electrum:

- [Installing the Electrum Bitcoin wallet on Linux][electrum-2020]

I've actually shown 4 possible ways of installing Electrum. Choose one, install Electrum and come back here, so we can start it and set it up.

## Starting Electrum for the first time

Regardless of how you have installed Electrum, to start it, click **Activities**, by the upper-left corner of the screen, type `electrum` and click its icon:

{% include image.html src="/files/2020/11/electrum-activities-en.jpg" %}

When run for the first time, Electrum presents a configuration wizard.

For a beginner, it's safe to accept all the default settings by clicking **Next**...

{% include image.html src="/files/2021/04/electrum-config-01.jpg" %}
{% include image.html src="/files/2021/04/electrum-config-02.jpg" %}
{% include image.html src="/files/2021/04/electrum-config-03.jpg" %}
{% include image.html src="/files/2021/04/electrum-config-04.jpg" %}

...until you reach this screen (**don't** click **Next** yet):

{% include image.html src="/files/2021/04/electrum-config-05.jpg" %}

This screen displays your Bitcoin wallet's **private key** (or **seed**). Write it down on a safe place (if you are concerned about security, do not store it electronically, write it down on a piece of paper). It will allow you to recover your wallet in case something goes wrong.

**Only after you write down** your seed, click **Next**.

The next screen asks you to type your seed:

{% include image.html src="/files/2021/04/electrum-config-06.jpg" %}

Click inside the text area and type your seed. When you finish, if it's correct, the **Next** button will become available. Click it.

{% include image.html src="/files/2021/04/electrum-config-07.jpg" %}

On the last wizard screen, you have the opportunity to create a password to protect your wallet, which is **strongly recommended**. In case your computer is compromised (e.g. hacked or stolen), your wallet will be password protected and it will not be possible to transfer your Bitcoins or view your transaction history.

Think of a secure password and type it on the first text field. To confirm it, type it again on the second text field. Make sure the **Encrypt wallet file** option is checked and click **Next**.

{% include image.html src="/files/2021/04/electrum-config-08.jpg" %}

Electrum warns you about the importance of keeping it always up-to-date. I believe it's a good idea to be notified about new versions. I recommend you to answer this question with **Yes**.

<div class="alert alert-warning" role="alert">
    Always update Electrum from a trusted source, the same you used to install it.
</div>

After the configuration wizard is finished, Electrum will present its main screen:

{% include image.html src="/files/2021/04/electrum-main-screen.png" %}

Your wallet has been created in `/home/yourusername/.electrum/wallets/default_wallet`. This file is encrypted and stores your wallet's Bitcoin address and private key.

## To be continued...

In [part 2][part-2], you are going to see how to buy bitcoins on an exchange and transfer them to Electrum.

{% capture part2 %}[Part 2 is here! (click)]({% post_url en/2021-04-27-bitcoin-for-beginners-with-the-electrum-wallet-part-2-buying-and-transferring-bitcoins-to-the-wallet %}){% endcapture %}
{% include update.html date="Apr 27, 2021" message=part2 %}

[bitcoin]:                  https://bitcoin.org/
[foss]:                     https://en.wikipedia.org/wiki/Free_and_open-source_software
[reuters]:                  https://www.reuters.com/article/us-crypto-currencies-africa-insight-idUSKBN25Z0Q8
[spend]:                    https://bitcoin.org/en/spend-bitcoin
[namecheap]:                https://bit.ly/kamarada-namecheap
[wikipedia]:                https://donate.wikimedia.org/wiki/Ways_to_Give#Bitcoin
[electrum]:                 https://electrum.org/
[part-2]:                   {% post_url en/2021-04-27-bitcoin-for-beginners-with-the-electrum-wallet-part-2-buying-and-transferring-bitcoins-to-the-wallet %}
[part-3]:                   {% post_url en/2021-05-03-bitcoin-for-beginners-with-the-electrum-wallet-part-3-transferring-from-the-wallet-and-selling-bitcoins %}
[kamarada-15.2]:            {% post_url en/2020-09-11-linux-kamarada-15.2-come-to-the-elegant-modern-green-side-of-the-force %}
[electrum-release-notes]:   https://github.com/spesmilo/electrum/blob/4.1.2/RELEASE-NOTES

[bitcoin-paper]:            https://bitcoin.org/bitcoin.pdf
[peer-to-peer]:             https://en.wikipedia.org/wiki/Peer-to-peer
[bittorrent]:               https://en.wikipedia.org/wiki/BitTorrent
[broadcasting]:             https://en.wikipedia.org/wiki/Broadcasting_(networking)
[cryptography]:             https://en.wikipedia.org/wiki/Public-key_cryptography
[mining]:                   https://decrypt.co/39426/is-bitcoin-mining-still-profitable-in-2021
[duckduckgo]:               https://duckduckgo.com/?q=Bitcoin
[google]:                   https://www.google.com.br/search?q=Bitcoin
[cryptocurrencies]:         https://en.wikipedia.org/wiki/Cryptocurrency

[supply-and-demand]:        https://en.wikipedia.org/wiki/Supply_and_demand
[fiat]:                     https://en.wikipedia.org/wiki/Fiat_money
[duckduckgo-2]:             https://duckduckgo.com/?q=btc+usd
[google-2]:                 https://www.google.com/search?q=btc+usd

[exchanges]:                https://bitcoin.org/en/exchanges
[cointelegraph]:            https://cointelegraph.com/news/10-key-things-to-consider-when-choosing-a-crypto-exchange
[99bitcoins]:               https://99bitcoins.com/bitcoin-exchanges/
[binance]:                  https://bit.ly/kamarada-binance
[coinmarketcap]:            https://coinmarketcap.com/rankings/exchanges/
[binance-review]:           https://99bitcoins.com/bitcoin-exchanges/binance-review/

[types-of-wallets]:         https://99bitcoins.com/bitcoin-wallet/#wallet-type
[bitcoin-wallets]:          https://bitcoin.org/en/choose-your-wallet
[linux]:                    https://www.vivaolinux.com.br/linux/
[armory]:                   https://btcarmory.com/
[bitcoin-core]:             https://bitcoin.org/en/download
[bitcoinknots]:             https://bitcoinknots.org
[bither]:                   https://bither.net
[bitpay]:                   https://bitpay.com/wallet/
[electrum]:                 https://electrum.org
[specter]:                  https://specter.solutions/
[wasabi]:                   https://wasabiwallet.io/
[cross-platform]:           https://en.wikipedia.org/wiki/Cross-platform_software
[android]:                  https://www.android.com/
[windows]:                  https://www.microsoft.com/pt-br/windows/
[macos]:                    https://www.apple.com/br/macos/
[bitzuma-electrum]:         https://bitzuma.com/posts/a-beginners-guide-to-the-electrum-bitcoin-wallet/
[how-to-verify-iso]:        {% post_url en/2018-11-08-verifying-data-integrity-and-authenticity-using-sha-256-and-gpg %}
[github]:                   https://github.com/spesmilo/electrum

[electrum-2020]:            {% post_url en/2020-12-04-installing-the-electrum-bitcoin-wallet-on-linux %}
