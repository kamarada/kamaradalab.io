---
date: '2024-10-31 23:59:59 GMT-3'
image: '/files/2024/10/chrome-sync-bookmarks.png'
layout: post
nickname: 'chrome-sync-bookmarks'
title: 'Google Chrome / Chromium: how to sync / import / restore your bookmarks'
excerpt: 'Have you just formatted your computer? Changed computers? Migrating from one browser to another? Or from Windows to Linux? Whatever your situation is, you want to take your bookmarks with you. Here are some tips that can help if you use Chrome, the most used browser in the world, or Chromium, its open base.'
---

{% include image.html src='/files/2024/10/chrome-sync-bookmarks.png' %}

Have you just formatted your computer? Changed computers? Migrating from one browser to another? Or from [Windows] to [Linux]? Whatever your situation is, you want to take your bookmarks with you. Here are some tips that can help you if you use [Google Chrome], the [most used browser in the world][statcounter].

Almost all of the following tips also apply to [Chromium], which is the [free software][free-sw] base for the [proprietary] Google Chrome. Along with [Mozilla Firefox], Chromium is one of the browsers that come out of the box with [Linux Kamarada 15.5].

## The easiest way: sync

If you're moving to a new system or computer, or if you use Google Chrome on multiple devices (desktop, smartphone, tablet, etc.), the easiest way to take your bookmarks with you wherever you go is to keep them in sync across all your devices [turning on Google Chrome sync][sync]. Set it up on all your devices, and every time you create, edit or delete a bookmark on one device, that change will be reflected on the other devices when you open Google Chrome on them.

Google Chrome syncs not only your bookmarks, but also several other browser data and preferences, including passwords, history, extensions, among others. To use it, you need a [Google Account].

Please note that sync is a Chrome-only feature, [not present on Chromium][omgubuntu].

To turn on Google Chrome sync on your computer, in the top-right corner of the window, click **Profile**, and then click **Turn on sync**:

{% include image.html src='/files/2024/10/chrome-sync-01-en.jpg' %}

Enter your email address and password to sign in to your Google Account, or create one if you don't have one already:

{% include image.html src='/files/2024/10/chrome-sync-02-en.jpg' %}

When asked to **Turn on sync**, click **Yes, I'm in**:

{% include image.html src='/files/2024/10/chrome-sync-03-en.jpg' %}

That's it! Google Chrome syncing is now enabled.

If you previously connected another browser to your Google Account, Google Chrome will download your bookmarks and display them in the bookmarks bar. Note that if you already had bookmarks in this browser before syncing, they will be merged with the ones you already had in your Google Account.

## Changing browsers?

If you're just moving from another browser on the same system -- say you were used to another browser, such as [Mozilla Firefox], and want to migrate to Google Chrome -- you can import bookmarks from other browsers into Google Chrome (and other data such as history and passwords).

First, close the other browser.

Then, open the **Chrome menu** in the top-right corner of the window, point to **Bookmarks and lists**, and then click **Import bookmarks and settings**:

{% include image.html src='/files/2024/10/chrome-import-other-browser-01-en.jpg' %}

Choose from the list the browser whose data you want to import to Google Chrome and click **Import**:

{% include image.html src='/files/2024/10/chrome-import-other-browser-02-en.jpg' %}

Finally, click **Done**:

{% include image.html src='/files/2024/10/chrome-import-other-browser-03-en.jpg' %}

Now you should notice the other browser's bookmarks imported into your Chrome.

Note that Chrome puts imported bookmarks on a separate folder on the bookmarks bar. You can organize them however you like.

## Exporting Chrome bookmarks

You can export your bookmarks to an HTML file, which can be saved as a backup of your bookmarks, or imported into another web browser such as Mozilla Firefox or even Google Chrome itself.

To do this, open the **Chrome menu**, point to **Bookmarks and lists** and click **Bookmark manager**:

{% include image.html src='/files/2024/10/chrome-export-01-en.jpg' %}

In the top right corner, open the **Organize** menu and click **Export bookmarks**:

{% include image.html src='/files/2024/10/chrome-export-02-en.jpg' %}

Enter a location and name for the file (Chrome suggests a name that contains the current date, such as `bookmarks_10_31_24.html`).

Your bookmarks are now successfully exported from Chrome. The HTML file you saved is now ready to be imported into another browser.

If you want instructions on how to import it into Firefox, [check out this how-to][Mozilla Firefox].

## Importing bookmarks into Chrome

You can import into Chrome bookmarks exported from another browser such as Firefox or even Chrome itself. These bookmarks need to have been exported as an HTML file.

To do this, open the **Chrome menu**, point to **Bookmarks and lists** and click **Import bookmarks and settings**.

Select **Bookmarks HTML File** from the list and click **Choose File**:

{% include image.html src='/files/2024/10/chrome-import-01-en.jpg' %}

Navigate to the HTML file containing the bookmarks to be imported and open it.

The bookmarks in the selected HTML file will be added to your Chrome bookmarks bar on a separate folder:

{% include image.html src='/files/2024/10/chrome-import-02-en.jpg' %}

You can organize them however you like.

## Manually restoring bookmarks

If you formatted your computer, or changed computers, and you can no longer open Chrome on the previous system, but you can access its files, your only option may be to manually restore your bookmarks, without the aid of a Chrome graphical interface.

All of the changes you make in Chrome, such as your home page, extensions, saved passwords and bookmarks, are stored in a special folder called a **[profile]**. The profile folder is stored in your home folder, separate from the Chrome app, so that if something goes wrong with Chrome, your information will still be there.

Before you begin, close all open Chrome and Chromium windows.

Using the **Files** app, browse to the folder of the old profile, whose bookmarks you want to restore, as well as to the folder of the current profile, which you are currently using and to which you want to import the bookmarks.

By default, Google Chrome stores profiles:

- On Windows, at: `C:\Users\YourUsername\AppData\Local\Google\Chrome\User Data\`; and
- On Linux, at: `/home/yourusername/.config/google-chrome/` (ou `chromium`).

In these folders, there may be folders called `Default`, `Profile 2`, `Profile 3`, etc. corresponding to the profiles.

To find these folders, you may need to enable showing hidden files. To do this, open the **Files menu** in the top-right corner of the window and activate the **Show Hidden Files** option:

{% include image.html src='/files/2024/09/firefox-profile-03-en.jpg' %}

Open the profile folders and locate in both of them the `Bookmarks` file:

{% include image.html src='/files/2024/10/chrome-profile-en.jpg' %}

This file, as the name suggests, contains your bookmarks.

In your current profile folder, rename this file to something like `Bookmarks-original`. You can also make a copy of this file somewhere else, just in case.

Copy the `Bookmarks` file from the old profile folder to the current profile folder.

Open Chrome and notice that your old bookmarks now appear on the bookmarks bar.

Export these bookmarks following the instructions above.

Close Chrome once more. Back to **Files**, in the current profile folder, delete the `Bookmarks` file and rename `Bookmarks-original` back to `Bookmarks`.

Open Chrome once more. You will notice that the bookmarks that appear now are the ones you had before.

Finally, you can import the bookmarks from the old profile that you just exported.

Alternatively, note that the `Bookmarks` files are [JSON] text files. If you understand this format, you can open the old and the new `Bookmarks` files side by side and manually copy and paste bookmarks from one to the other:

{% include image.html src='/files/2024/10/chrome-bookmarks-json-en.jpg' %}

## References

I hope those tips have helped you recover your Chrome bookmarks after formatting or changing computers or systems. This article was a compilation of information found on several pages of the official Chrome documentation and some other websites. If you need more information, you can take a look at them:

- [Get your bookmarks, passwords and more on all your devices - Computer - Google Chrome Help](https://support.google.com/chrome/answer/165139)
- [Sign in and sync in Chrome - Computer - Google Chrome Help][sync]
- [Import Chrome bookmarks & settings - Google Chrome Help](https://support.google.com/chrome/answer/96816)
- [Use Chrome with multiple profiles - Computer - Google Chrome Help][profile]
- [Use Chromium? Sync Features Will Stop Working on March 15 - OMG! Ubuntu][omgubuntu]
- [Where Are Google Chrome Bookmarks Stored? - Alphr](https://www.alphr.com/where-are-google-chrome-bookmarks-stored/)

[Windows]:              https://www.microsoft.com/windows/
[Linux]:                https://www.kernel.org/linux.html
[Google Chrome]:        {% post_url en/2019-07-20-20-apps-you-can-use-the-same-way-on-both-linux-and-windows-part-1 %}#2-google-chrome
[statcounter]:          https://gs.statcounter.com/
[Chromium]:             https://www.chromium.org/
[free-sw]:              https://www.gnu.org/philosophy/free-sw.html
[proprietary]:          https://en.wikipedia.org/wiki/Proprietary_software
[Mozilla Firefox]:      {% post_url en/2024-09-26-firefox-how-to-sync-import-restore-your-bookmarks %}
[Linux Kamarada 15.5]:  {% post_url en/2024-05-27-linux-kamarada-15-5-more-aligned-with-opensuse-leap-and-other-distributions %}
[sync]:                 https://support.google.com/chrome/answer/185277
[Google Account]:       https://support.google.com/accounts/answer/27441
[omgubuntu]:            https://www.omgubuntu.co.uk/2021/01/chromium-sync-google-api-removed
[profile]:              https://support.google.com/chrome/answer/2364824
[JSON]:                 https://en.wikipedia.org/wiki/JSON
