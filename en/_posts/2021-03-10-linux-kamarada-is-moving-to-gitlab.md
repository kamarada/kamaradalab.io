---
date: '2021-03-10 10:00:00 GMT-3'
image: '/files/2021/03/moving-to-gitlab.jpg'
layout: post
nickname: 'moving-to-gitlab'
title: 'Linux Kamarada is moving to GitLab'
---

{% include image.html src='/files/2021/03/moving-to-gitlab.jpg' %}

The Linux Kamarada Project is proud to announce that it is [moving to **GitLab**][moving-to-gitlab].

During the early years of the project, the source code and the website were hosted on [GitHub], for which I am grateful. However, after the [acquisition of GitHub by Microsoft][microsoft-github] — which [does not have a good past relationship with free software][microsoft-free-sw] — and, more recently, [the episode involving the youtube-dl project][youtube-dl], I am afraid that GitHub is no longer the best place to host my project. And, at the moment, the best alternative to GitHub is [GitLab].

I already used GitLab at work and decided to use it for Linux Kamarada as well, following other bigger open source projects that decided to take the same move, such as [GNOME], [KDE], [XFCE], [VLC], [Inkscape], [Manjaro], [Kali Linux][kali-linux] and [many others][gitlab-open-source].

Although GitHub hosts thousands of open source projects, GitHub itself is closed-source. In contrast, [GitLab makes its source code available][gitlab-source], allowing anyone to [contribute] to the platform or [install their own self-managed GitLab server][gitlab-install]. I'm not a free software purist, I don't mind using proprietary software when I need to, but I do prefer to use (and develop) free software whenever I can. It's exciting to imagine that, in case of need, I can report a bug to GitLab (maybe even fix it) or help with translation.

I'm migrating the GitHub repos to GitLab. Soon, the Linux Kamarada source code will be available at [gitlab.com/kamarada](https://gitlab.com/kamarada). The old repos at [github.com/kamarada](https://github.com/kamarada) will be archived.

This website is currently being served by [GitLab Pages][gitlab-pages], similar to [GitHub Pages][github-pages]. It is worth noting that GitHub Pages only supports the [Jekyll] static site generator (you can use any other, as long as you compile the website locally on your computer), while GitLab Pages supports Jekyll and [other static site generators][gitlab-ssg], for instance, [Hugo].

Still on GitLab, last but not least, I like the [issue board][gitlab-issues], which is the interface GitLab offers to manage issues. While the concept of issue is somewhat broad and abstract, you can understand issues as "things to do" in a project: features, bugs or, in the case of a blog, posts to write. If you have seen any [Kanban] board, you will find the look (and usage) very similar:

{% include image.html src='/files/2021/03/gitlab-kanban.jpg' caption='I blurred the post titles, after all, you don&apos;t want spoilers, do you?' %}

{% capture update %}If you too are thinking about moving your source code repositories from GitHub to GitLab, see [my how-to]({% post_url en/2021-06-25-how-to-migrate-projects-from-github-to-gitlab %}).{% endcapture %}
{% include update.html date="Jun 25, 2021" message=update %}

Taking this opportunity, I want to briefly talk about another good news, which is the **change of address**: from [kamarada.github.io](https://kamarada.github.io/) to [linuxkamarada.com](https://linuxkamarada.com/). I will keep the website on the previous address as is, serving as kind of a mirror for the old posts and notifying people of the new address. If you have Linux Kamarada in your bookmarks, please update your links!

This domain has been registered with [Namecheap]. It is an American registrar (a company that registers domains) that has existed since 2000. It caught my attention because it is the registrar used by some of the most popular [Linux] distros, such as [Arch Linux][archlinux], [Zorin OS][zorinos], [KDE neon][kde-neon] and [MX Linux][mxlinux] (you can check this yourself using the **[whois]** command, for example: `whois archlinux.org`). In addition, Namecheap has a free privacy feature that is enabled by default, which is [WhoisGuard], it protects users' sensitive data (for many Brazilian `.com.br` domains, **whois** informs someone's individual taxpayer registry identification, which is a serious data leak). I also found it interesting that Namecheap accepts [Bitcoin].

[moving-to-gitlab]:     https://about.gitlab.com/community/moving-to-gitlab/
[github]:               https://github.com/
[microsoft-github]:     https://about.gitlab.com/blog/2018/06/03/microsoft-acquires-github/
[microsoft-free-sw]:    https://en.wikipedia.org/wiki/Microsoft_and_open_source#Initial_stance_on_open_source
[youtube-dl]:           https://itsfoss.com/youtube-dl-github-takedown/
[gitlab]:               https://gitlab.com/
[gnome]:                https://about.gitlab.com/blog/2018/05/31/welcome-gnome-to-gitlab/
[kde]:                  https://about.gitlab.com/blog/2020/06/29/welcome-kde/
[xfce]:                 https://simon.shimmerproject.org/2020/04/30/xfce-switches-to-gitlab/
[vlc]:                  https://gitlab.com/gitlab-org/gitlab/-/issues/26534
[inkscape]:             https://inkscape.org/news/2017/06/10/inkscape-moves-gitlab/
[manjaro]:              https://twitter.com/ManjaroLinux/status/1004822535136075776
[kali-linux]:           https://about.gitlab.com/blog/2021/02/18/kali-linux-movingtogitlab/
[gitlab-open-source]:   https://about.gitlab.com/solutions/open-source/projects/
[gitlab-source]:        https://gitlab.com/gitlab-org/gitlab
[gitlab-install]:       https://about.gitlab.com/install/
[contribute]:           https://about.gitlab.com/community/contribute/
[gitlab-pages]:         https://docs.gitlab.com/ee/user/project/pages/
[github-pages]:         https://pages.github.com/
[jekyll]:               https://jekyllrb.com/
[gitlab-ssg]:           https://about.gitlab.com/blog/2016/06/17/ssg-overview-gitlab-pages-part-3-examples-ci/
[hugo]:                 https://gohugo.io/
[gitlab-issues]:        https://docs.gitlab.com/ee/user/project/issue_board.html
[kanban]:               https://en.wikipedia.org/wiki/Kanban_(development)
[namecheap]:            https://bit.ly/kamarada-namecheap
[linux]:                https://www.kernel.org/linux.html
[archlinux]:            https://archlinux.org/
[zorinos]:              https://zorinos.com/
[kde-neon]:             https://neon.kde.org/
[mxlinux]:              https://mxlinux.org/
[whois]:                https://linux.die.net/man/1/whois
[whoisguard]:           https://www.namecheap.com/security/whoisguard/
[bitcoin]:              https://bitcoin.org/en/
