---
date: '2021-05-25 11:15:00 GMT-3'
image: '/files/2021/05/im-jami-pt.jpg'
layout: post
nickname: 'jami'
title: 'The Jami messenger'
---

{% include image.html src='/files/2021/05/im-jami-pt.jpg' %}

[Jami] is a [peer-to-peer] (P2P) [instant messenger]. Different from other more common messengers, such as [WhatsApp], [Telegram] or [Signal], which are centralized, or other messengers we've seen here, such as [Session] and [Element], which are decentralized, Jami takes a **[distributed]** approach and does not require a server for relaying data between users. Two people who are connected by the same network (it can be even a local network disconnected from the Internet) can communicate with Jami, at least in theory (I'll explain).

{% include image.html src='/files/2021/05/network-topologies.png' %}

All communications with Jami are peer-to-peer and end-to-end encrypted. Your account is not stored on any server, but only on your device. You don't need to provide any personal information (e.g. email address or phone number) to create an account. All the information you can provide is optional. You can optionally define a username to make it easier for others to find you. And it's also optional to define a password, which is used only to encrypt your account's archive, protecting you from someone who gets physical access to your device.

When you use Jami for the first time, it presents a configuration wizard. The app asks you to create an username, a password, a display name and an avatar. You can create an account without any of those, if you want, and set them up later. After this initial configuration, which can be done even if you're not connected to the Internet, Jami generates a random (unique) **JamiID** for you. It's a large hexadecimal number, written as a 40-chars length string, that technically is the fingerprint of your public key. And that's it, now you have a Jami account.

I said "at least in theory" earlier because I couldn't reproduce Jami's capability to deliver messages when offline in my experiments. I connected a laptop and an Android smartphone to the same Wi-Fi network. I downloaded and installed Jami on both devices and then I disconnected the ISP cable from my Wi-Fi router. So, both devices were on the same local network, but disconnected from the Internet. Next, I set up Jami on both devices. In fact, the first-time configuration worked offline. But then, when I tried to send messages from one device to the other, they were not delivered, until I plugged the ISP cable again.

That shows the trade-off of peer-to-peer messengers like Jami: although P2P is good for privacy, as no central server means no central failure or interception point, messages can only be sent when both peers are online. However, the app can store messages locally to wait for the contact to return online, what Jami does.

Probably that happened when I tried Jami because [it actually depends on some servers][servers]. But you have the option to self-host all of them and point your client apps to use them. The main servers Jami depends on are the one that hosts the [DHT] (distributed hash table, the same technique used by [BitTorrent] and [IPFS] to help nodes on the network find and connect to each other) and the name server, used for resolving Jami usernames. By default, Jami is setup to use `dhtproxy.jami.net` and `ns.jami.net`, respectively.

You can have the same Jami account linked to more than one device. To do this, you need to open Jami on a device which already has the account and choose to link another device. Jami generates a random PIN code and makes your account available on the DHT for 10 minutes. Then, on the other device, open Jami and choose to link this device to an existing account. I tested this feature as well and only contacts were copied, not messages. Also, after your account is linked to both devices and both are online, received messages are shown on both devices, but sent messages appear only on the actual sender device. So, although that feature works, it's not as smooth as in other messengers, such as Telegram.

Linking to another device is also one of the ways of backing up a Jami account. As it exists only on the linked devices, the other way to back it up is copying the account archives on the device to another location. On both the Linux and Android clients there is an option in the account settings that facilitate this task.

Also because your account exists only on your devices, there is no way to reset your password if you set up one and forget it. If you are afraid to forget your password, you can use a password manager to backup it up safely, such as [KeePassXC], which comes with [Linux Kamarada][kamarada-15.2].

Using Jami, you are the only one in charge of your data, so take care. If you don't want to lose your account, back up its files and your password. On the other hand, if you want to delete your account, just uninstall Jami and delete its files on all linked devices. Doing this, your account will not exist anymore and nobody will be able to restore it. Never, ever.

Jami is developed by [Savoir-faire Linux][sfl] — a Canadian GNU/Linux consulting company — but it is also considered a [GNU] project backed by the [Free Software Foundation][fsf]. Jami's [source code] is available and the app is distributed under a [GPLv3] or later license.

The ancestor of Jami is [SFLphone], a portable SIP softphone, also developed by Savoir-faire Linux and launched in [2004]. Later, in [2015], chat and encryption features were added and it was renamed to Ring. In [2016], it became part of the GNU Project. Finally, in [2019], Ring was renamed to Jami to avoid confusion with commercial products (some with similar features). Jami is inspired by a [Swahili] word that means "community". Because of this history mostly associated to calls, there are [websites][techradar] listing Jami as an open-source alternative to [Skype].

Jami has all the common features to messengers: it allows you to send text, audio and video messages, photos, videos and files, make voice and video calls, conferences and share the screen. Jami also inherits SIP support from SFLphone and allows you to setup SIP accounts in addition to Jami accounts, which allows you to call both Jami users and SIP users.

{% include update.html date="May 26, 2021" message="Groups are not supported by Jami yet, but this is a feature [in development](https://git.jami.net/savoirfairelinux/ring-project/-/wikis/technical/2.3.-Swarm)." %}

I wouldn't recommend Jami for users in general, because I believe most people don't want to worry about backing up messages themselves and will need to resort to some kind of account recovery someday. But for privacy-minded users, it's certainly a great option.

The Jami website has instructions on [how to install it on some Linux distros][jami-linux], openSUSE included. For instance, on [openSUSE Leap 15.2][leap-15.2] (and Linux Kamarada 15.2 as well):

```
# zypper addrepo https://dl.jami.net/nightly/opensuse-leap_15.2/jami-nightly.repo
# zypper install jami
```

I personally found it easier to install Jami using Flatpak from [Flathub]:

```
# flatpak install net.jami.Jami
```

You can get the Jami app for Android from [Google Play] or from [F-Droid].

Jami is also available for other systems. For more options, take a look at: [jami.net](https://jami.net/download/).

Jami has two very explanatory FAQ pages in its [website][faq-1] and [self-hosted GitLab][faq-2].

This text is part of the series:

- [Open-source privacy-focused messaging apps alternatives to WhatsApp][instant messenger]

[jami]:                 https://jami.net/
[peer-to-peer]:         https://en.wikipedia.org/wiki/Peer-to-peer
[instant messenger]:    {% post_url en/2021-05-10-open-source-privacy-focused-messaging-apps-alternatives-to-whatsapp %}
[whatsapp]:             https://www.whatsapp.com/
[telegram]:             {% post_url en/2021-05-10-the-telegram-messenger %}
[signal]:               {% post_url en/2021-05-11-the-signal-messenger %}
[session]:              {% post_url en/2021-05-17-the-session-messenger %}
[element]:              {% post_url en/2021-05-20-the-element-messenger-and-the-matrix-standard %}
[distributed]:          https://en.wikipedia.org/wiki/Distributed_networking
[servers]:              https://jami.net/why-is-jami-truly-distributed/
[DHT]:                  https://en.wikipedia.org/wiki/Distributed_hash_table
[BitTorrent]:           https://en.wikipedia.org/wiki/BitTorrent
[IPFS]:                 https://en.wikipedia.org/wiki/InterPlanetary_File_System
[KeePassXC]:            https://keepassxc.org/
[kamarada-15.2]:        {% post_url en/2020-09-11-linux-kamarada-15.2-come-to-the-elegant-modern-green-side-of-the-force %}
[sfl]:                  https://savoirfairelinux.com/en/
[GNU]:                  https://www.gnu.org/
[fsf]:                  https://www.fsf.org/
[source code]:          https://git.jami.net/savoirfairelinux/ring-project
[GPLv3]:                https://www.gnu.org/licenses/gpl-3.0.html
[SFLphone]:             https://en.wikipedia.org/wiki/Jami_(software)#History
[2004]:                 https://en.wikipedia.org/wiki/Savoir-faire_Linux#History
[2015]:                 https://blog.savoirfairelinux.com/en-ca/2015/ring-ultimate-privacy-and-control-for-your-voice-video-and-chat-communications/
[2016]:                 https://lists.gnu.org/archive/html/info-gnu/2016-11/msg00001.html
[2019]:                 https://jami.net/ring-becomes-jami/
[Swahili]:              https://en.wikipedia.org/wiki/Swahili_language
[techradar]:            https://www.techradar.com/best/best-alternatives-to-skype
[Skype]:                https://www.skype.com/
[jami-linux]:           https://jami.net/download-jami-linux/
[leap-15.2]:            {% post_url en/2020-07-02-opensuse-leap-15-2-release-brings-exciting-new-artificial-intelligence-ai-machine-learning-and-container-packages %}
[flathub]:              https://flathub.org/apps/details/net.jami.Jami
[google play]:          https://play.google.com/store/apps/details?id=cx.ring
[f-droid]:              https://f-droid.org/repository/browse/?fdid=cx.ring
[faq-1]:                https://jami.net/help/
[faq-2]:                https://git.jami.net/savoirfairelinux/ring-project/-/wikis/tutorials/Frequently-Asked-Questions
