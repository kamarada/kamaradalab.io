---
date: '2021-05-10 10:00:00 GMT-3'
image: '/files/2021/05/messaging-apps.jpg'
layout: post
nickname: 'messaging-apps'
title: 'Open-source privacy-focused messaging apps alternatives to WhatsApp'
---

{% include image.html src='/files/2021/05/messaging-apps.jpg' %}

Over the last 20 years, there has been a significant increase in the usage of [instant messengers][im], with the most popular of them having amassed over 1 billion active users each.

{% include youtube.html id="pdZ179PmCPk" %}

Today, the most popular messenger app in the world is [WhatsApp], which has more than 2 billion users in over 180 countries. Many people using the same messenger app may be convenient. But, on the other hand, that has raised concerns about the privacy and security of conversations.

Especially after [Facebook acquired WhatsApp for $19 billion in 2014][fb-whatsapp], a figure that drew attention, because WhatsApp is a free messaging app with no apparent monetization, such as displaying ads, subscriptions or paid extra features. It was speculated that [Facebook] was interested in WhatsApp for [data mining][data-mining].

At the time, Facebook promised to keep WhatsApp data private and not share it with other services. However, since then WhatsApp has already announced two changes to its terms of service and privacy policy, in order to share more and more data with Facebook: one in [2016] and another in this year of [2021].

It seems the first change has gone unnoticed, but the recent one [caused users to shift to other messaging services][shift] such as [Telegram] and [Signal]. The exodus was so large that [WhatsApp postponed the change][postponed], previously scheduled for February 8, to May 15, 2021, and has resorted to [in-app messages][status] to better explain it.

{% include image.html src='/files/2021/05/whatsapp-en.png' %}

You may want to take this time to look for another messenger. If that is your case, then you are going to find here a list of possible alternatives to WhatsApp. I searched and tested some privacy-focused instant messengers and compiled this list. All of them are [cross-platform] and have versions for [Linux] and [Android] — the ones I tested — as well as other systems.

## What makes a messaging app secure?

The main method used by messaging apps to ensure users' privacy is encrypting the contents of messages.

**[Encryption]** is a technique that uses mathematics to encode (scramble) the content of a message so that it can only be decoded and read by someone who has certain information, such as a password or encryption key (a kind of a "secret"). If you look at the [history of cryptography][history], you will see that it has been used for a long time: [the Roman emperor Julius Caesar already used cryptography][cesar] to communicate with his generals.

Encryption can be used in many different ways. Particularly, secure messengers do [**end-to-end encryption**][e2ee]: when you send a message or file, the content is encrypted by the messaging app **before** leaving your device (e.g. computer or smartphone), and remains encrypted until it arrives at the device(s) of the other person(s) you are talking to. Only the sender and the receiver(s) have the needed keys to decrypt — and, therefore, read — the messages. This ensures that no one besides the participants in the conversation can know what is being talked about, even when the conversation crosses an insecure way like the Internet.

Also pay attention to the **default settings** of the messaging apps: some messengers offer end-to-end encryption but it's not enabled by default, so you need to go into the app settings and actually enable encryption. Other messengers encrypt only certain types of messages. Don't assume the app is safe just because its description contains "end-to-end encryption": find out when the app actually encrypts your messages.

WhatsApp says it uses end-to-end encryption. In 2016 there were some episodes in which [WhatsApp was banned in Brazil][banned] because it failed to hand over conversations requested by criminal court orders. This can be seen as strong evidence that WhatsApp does use end-to-end encryption: the service would not be able to provide users' conversations to the courts even if it wanted to. If it stores encrypted messages, it is technically impossible indeed. On the other hand, it is not possible to state this with complete certainty, because the WhatsApp [source code][source] is not open, which leads us to the next point.

[**Open-source software**][oss] is a software in which developers make the source code available so that anyone who is interested (and has the necessary technical knowledge) be able to analyze how the software works internally. The most secure messengers are open source, because they can be audited more easily and independently, as well as errors (bugs) that are eventually found can be fixed more easily and quickly. In general, when the software is related to privacy, security and encryption, revealing its source code is widely regarded as an indicator of the software's integrity: you can see that it actually does what it says it does.

Still safer is the app built with [**reproducible builds**][reproducible-builds] (a.k.a. **deterministic compilation**). In its simplest form, it means that anyone can build from source a binary that is bit-identical to the one officially available for download. That is, the app downloaded from the app store was actually built with the same source code publicly available.

While many messaging apps today use end-to-end encryption, thus protecting the content of messages, another point to analyze in a messaging app is whether it still collects other information about you, such as IP address, mobile phone number and model, contact list, dates and times when messages are sent and received, how much information is sent, etc. This kind of "information about information" is called **[metadata]**. The existence and analysis of this metadata poses a risk to journalists, protesters and human rights activists.

I also think it is important to analyze who are the people behind the app, both **who develop** it and **who financially support** it. Depending on the messenger type, it may need an entire infrastructure to store and forward messages. If there is a cost involved, it's important to know how it is financed. After all, there is a saying that "if you're not paying, you're the product".

All of the apps listed below use end-to-end encryption (almost all, by default). All are open source (at least the clients, not all the servers are) and collect little or no metadata.

## Free and open source instant messengers

We are going to review the following messaging apps:

<div class="media mb-3">
    <img src="/files/2021/05/telegram.svg" class="mr-3" style="max-width: 48px;">
    <div class="media-body align-self-center">
        <h3 class="mt-0 mb-0"><a href="{% post_url en/2021-05-10-the-telegram-messenger %}">Telegram</a></h3>
    </div>
</div>

<div class="media mb-3">
    <img src="/files/2021/05/signal.svg" class="mr-3" style="max-width: 48px;">
    <div class="media-body align-self-center">
        <h3 class="mt-0 mb-0"><a href="{% post_url en/2021-05-11-the-signal-messenger %}">Signal</a></h3>
    </div>
</div>

<div class="media mb-3">
    <img src="/files/2021/05/session.png" class="mr-3" style="max-width: 48px;">
    <div class="media-body align-self-center">
        <h3 class="mt-0 mb-0"><a href="{% post_url en/2021-05-17-the-session-messenger %}">Session</a></h3>
    </div>
</div>

<div class="media mb-3">
    <img src="/files/2021/05/element.svg" class="mr-3" style="max-width: 48px;">
    <div class="media-body align-self-center">
        <h3 class="mt-0 mb-0"><a href="{% post_url en/2021-05-20-the-element-messenger-and-the-matrix-standard %}">Element (and Matrix)</a></h3>
    </div>
</div>

<div class="media mb-3">
    <img src="/files/2021/05/jami.svg" class="mr-3" style="max-width: 48px;">
    <div class="media-body align-self-center">
        <h3 class="mt-0 mb-0"><a href="{% post_url en/2021-05-25-the-jami-messenger %}">Jami</a></h3>
    </div>
</div>

<div class="media mb-3">
    <img src="/files/2021/05/briar.svg" class="mr-3" style="max-width: 48px;">
    <div class="media-body align-self-center">
        <h3 class="mt-0 mb-0"><a href="{% post_url en/2021-05-29-the-briar-messenger %}">Briar</a></h3>
    </div>
</div>

<div class="media mb-3">
    <img src="/files/2021/05/wire.svg" class="mr-3" style="max-width: 48px;">
    <div class="media-body align-self-center">
        <h3 class="mt-0 mb-0"><a href="{% post_url en/2021-06-02-the-wire-messenger %}">Wire</a></h3>
    </div>
</div>

## References

I anticipate sharing the list of texts I've read to write this series:

- [Keep your digital communication private - Security-in-a-Box][securityinabox]
- [Real-Time Communication \| PrivacyTools][privacytools]
- [The Most Secure Messaging Apps for Android & iOS 2021 \| AVG][avg]
- [5 Better Privacy Alternatives to Dump WhatsApp - It's FOSS][itsfoss]
- [7 Best WhatsApp Alternatives In 2021: Privacy-Focused Messaging Apps - Fossbytes][fossbytes]
- [WhatsApp alternatives: the best messaging apps for Android and iPhone \| TechRadar][techradar]
- [Top 5 WhatsApp Alternatives in 2021 \[Best For Privacy & Safety\] - vpnMentor][vpnmentor]
- [5 alternatives to using WhatsApp in 2021 \| TechSpot][techspot]
- [5 of the Best Alternatives to WhatsApp that Actually Respect Your Privacy - Make Tech Easier][maketecheasier]
- [Best WhatsApp alternatives that respect your privacy - ProtonMail Blog][protonmail]

[im]:                   https://en.wikipedia.org/wiki/Instant_messaging
[whatsapp]:             https://www.whatsapp.com/
[fb-whatsapp]:          https://techcrunch.com/2014/02/19/facebook-buying-whatsapp-for-16b-in-cash-and-stock-plus-3b-in-rsus/
[facebook]:             https://facebook.com/
[data-mining]:          https://www.linkedin.com/pulse/20140227065630-64875646-facebook-whatsapp-the-ultimate-spying-machine
[2016]:                 https://www.eff.org/deeplinks/2016/08/what-facebook-and-whatsapps-data-sharing-plans-really-mean-user-privacy-0
[2021]:                 https://www.eff.org/pt-br/deeplinks/2021/01/its-business-usual-whatsapp
[shift]:                https://www.theguardian.com/technology/2021/jan/24/whatsapp-loses-millions-of-users-after-terms-update
[telegram]:             {% post_url en/2021-05-10-the-telegram-messenger %}
[signal]:               {% post_url en/2021-05-11-the-signal-messenger %}
[postponed]:            https://www.reuters.com/article/facebook-whatsapp-privacy-idUSKBN2AJ0BC
[status]:               https://www.theverge.com/2021/1/30/22257721/whatsapp-status-privacy-facebook-signal-telegram
[cross-platform]:       https://en.wikipedia.org/wiki/Cross-platform_software
[linux]:                https://www.kernel.org/linux.html
[android]:              https://www.android.com/

[encryption]:           https://securityinabox.org/en/guide/secure-communication/#encryption
[history]:              https://en.wikipedia.org/wiki/Cryptography#History_of_cryptography_and_cryptanalysis
[cesar]:                https://en.wikipedia.org/wiki/Caesar_cipher
[e2ee]:                 https://en.wikipedia.org/wiki/End-to-end_encryption
[banned]:               https://en.wikipedia.org/wiki/WhatsApp#Brazil
[source]:               https://en.wikipedia.org/wiki/Source_code
[oss]:                  https://en.wikipedia.org/wiki/Open-source_software
[reproducible-builds]:  https://www.suse.com/c/reproducible-builds-in-opensuse-and-sle/
[metadata]:             https://securityinabox.org/en/guide/secure-communication/#metadata

[securityinabox]:       https://securityinabox.org/en/guide/secure-communication/
[privacytools]:         https://www.privacytools.io/software/real-time-communication/
[avg]:                  https://www.avg.com/en/signal/secure-message-apps
[itsfoss]:              https://itsfoss.com/private-whatsapp-alternatives/
[fossbytes]:            https://fossbytes.com/best-whatsapp-alternatives-privacy-focused-messaging-apps/
[techradar]:            https://www.techradar.com/best/whatsapp-alternatives
[vpnmentor]:            https://www.vpnmentor.com/blog/best-secure-alternatives-whatsapp/
[techspot]:             https://www.techspot.com/news/88291-5-alternatives-using-whatsapp-2021.html
[maketecheasier]:       https://www.maketecheasier.com/alternatives-to-whatsapp/
[protonmail]:           https://protonmail.com/blog/whatsapp-alternatives/
