---
date: '2023-02-15 23:30:00 GMT-3'
image: '/files/2023/02/kamarada-infra.jpg'
layout: post
nickname: 'packages.linuxkamarada.com'
title: 'Changes to the Linux Kamarada infrastructure'
---

{% include image.html src='/files/2023/02/kamarada-infra.jpg' %}

Linux Kamarada 15.4 Final is ready to be released: there was only a small fix after [15.4 RC], the ISO image has already been built and the release announcement has already been written. However, last week I was unable to upload the updated 15.4 repository to [OSDN]. The repo is still available, the files that were already there are still accessible, but I can't replace them with new files.

I contacted OSDN support but still no response ([OSDN User Support Ticket #47329][ticket-47329]). Other open source projects hosted by OSDN are experiencing the same issue ([Ticket #47324][ticket-47324]).

To overcome this problem, I made some changes to the Linux Kamarada infrastructure.

Previously, both [RPM] packages and Linux Kamarada ISO images were hosted on OSDN. This repository will no longer be updated.

From now on, Linux Kamarada RPM packages are available at [packages.linuxkamarada.com](https://packages.linuxkamarada.com/), an instance of [GitLab Pages][gitlab], the same service that hosts the [linuxkamarada.com](https://linuxkamarada.com/) website.

Linux Kamarada ISO images are now hosted on [SourceForge].

I didn't use the same service to host packages and ISO images, as it was before with OSDN, because GitLab Pages limits storage space and bandwidth (which makes it unfeasible to host ISO images) and SourceForge doesn't allow direct downloading of files (which makes it unfeasible to host package repos).

Out of curiosity, this is not the first time Linux Kamarada uses SourceForge hosting. The first version of Linux Kamarada officially released (with a release announcement) was [15.1]. But there were Linux Kamarada releases based on openSUSE Leap 42.2 that were never officially released. Their ISO images can be found at [SourceForge][sf-42.2]. To my surprise, they had 850 downloads since 2017.

## Where to download ISO images

The [Download] page has been updated with links to the ISO images on SourceForge.

## How to use the new repository

If you use Linux Kamarada already, you need to adjust the official Linux Kamarada repository [URL] on your system.

There are two ways to do this: through the graphical user interface or through the command line interface.

### Via the command line interface

The CLI method involves the **[zypper]** package manager. As explaining it is shorter, I'm going to start by it. In this case, it's easier to remove the Linux Kamarada repo and then re-add it with the new URL. To do so, run the following commands as administrator (root user):

```
# zypper rr kamarada
# zypper addrepo -f -K -n "Linux Kamarada" -p 95 "https://packages.linuxkamarada.com/\$releasever/openSUSE_Leap_\$releasever/" kamarada
```

### Via the graphical user interface

Although it is more user friendly, the GUI method requires a few clicks and goes through more screens. But the easiness more than makes up for the longer path.

Start the [YaST Control Center][yast]. To do this, click **Show Applications**, in the lower left corner of the screen, and click the **YaST** icon, at the end of the list:

{% include image.html src="/files/2023/01/upgrade-to-15.4-04-en.jpg" %}

You will be asked the root password. Type it and click **Continue**:

{% include image.html src="/files/2023/01/upgrade-to-15.4-05-en.jpg" %}

Within the **Software** category (the first one), click **Software Repositories**:

{% include image.html src="/files/2023/01/upgrade-to-15.4-06-en.jpg" %}

You are presented to your system's repository list:

{% include image.html src="/files/2023/02/packages-01-en.jpg" %}

Select the official **Linux Kamarada** repo from the list and click **Edit**.

On the screen that appears, replace the **URL of the Repository** with:

```
https://packages.linuxkamarada.com/$releasever/openSUSE_Leap_$releasever/
```

{% include image.html src="/files/2023/02/packages-02-en.jpg" %}

And click **OK**.

Back to the repository list, click **OK** to save the changes. You can close YaST.

## When is 15.4 Final going to be released?

I am going to postpone the 15.4 Final release by a few days in order to test the new infrastructure. Fresh Linux Kamarada installs made with the 15.4 Final image will come with the new repo location setup out-of-the-box.

[15.4 RC]:      {% post_url en/2022-12-19-linux-kamarada-15-4-enters-release-candidate-rc-phase %}
[OSDN]:         https://osdn.net/projects/kamarada/
[ticket-47329]: https://osdn.net/projects/support/ticket/47329
[ticket-47324]: https://osdn.net/projects/support/ticket/47324
[RPM]:          https://en.wikipedia.org/wiki/RPM_Package_Manager
[gitlab]:       {% post_url en/2021-03-10-linux-kamarada-is-moving-to-gitlab %}
[SourceForge]:  https://sourceforge.net/projects/kamarada/
[15.1]:         {% post_url en/2020-02-27-kamarada-15.1-comes-with-everything-you-need-to-use-Linux-everyday %}
[sf-42.2]:      https://sourceforge.net/projects/kamarada/files/distribution/leap/42.2/
[Download]:     /en/download
[URL]:          https://en.wikipedia.org/wiki/URL
[zypper]:       https://en.opensuse.org/Portal:Zypper
[yast]:         http://yast.opensuse.org/
