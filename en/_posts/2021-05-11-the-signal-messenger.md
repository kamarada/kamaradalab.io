---
date: '2021-05-11 22:00:00 GMT-3'
image: '/files/2021/05/im-signal-pt.jpg'
layout: post
nickname: 'signal'
title: 'The Signal messenger'
---

{% include image.html src='/files/2021/05/im-signal-pt.jpg' %}

[Signal] is a simple yet powerful and secure [instant messenger]. It offers a similar set of features to its competitors [WhatsApp] and [Telegram]: Signal allows you to send text and voice messages, as well as photos, videos and files, in one-to-one or group chats, in addition to make voice and video calls. A key difference is that privacy isn't optional when using Signal — every message, every call between Signal users is end-to-end encrypted.

In addition to that, the Android version of Signal can also send and receive regular [SMS/MMS messages][sms] and replace your system built-in SMS app. Used this way, Signal has the ability to send unencrypted text messages to — and display unencrypted text messages from — non-Signal users. Note that SMS messages are relayed by your mobile carrier rather than the Internet and are not encrypted. Signal will warn you when messages are sent unencrypted.

[Signal's website][signal] features endorsements from privacy experts like [Edward Snowden]:

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">I use Signal every day. <a href="https://twitter.com/hashtag/notesforFBI?src=hash&amp;ref_src=twsrc%5Etfw">#notesforFBI</a> (Spoiler: they already know) <a href="https://t.co/KNy0xppsN0">https://t.co/KNy0xppsN0</a></p>&mdash; Edward Snowden (@Snowden) <a href="https://twitter.com/Snowden/status/661313394906161152?ref_src=twsrc%5Etfw">November 2, 2015</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

When the WhatsApp controversy blew up in January, none other than the tech billionaire [Elon Musk] tweeted his support for Signal:

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Use Signal</p>&mdash; Elon Musk (@elonmusk) <a href="https://twitter.com/elonmusk/status/1347165127036977153?ref_src=twsrc%5Etfw">January 7, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Between 12 and 14 January 2021, the number of Signal installations listed on [Google Play] increased from over 10 million to over 50 million.

Signal servers keep almost no user metadata. In 2016, the service received a [subpoena] requiring information about two users for a federal grand jury investigation, to which they answered: "the only information we can produce in response to a request like this is the date and time a user registered with Signal and the last date of a user's connectivity to the Signal service".

Signal messages, pictures, files and other contents are not stored on the cloud for long. They are immediately and irrevocably deleted from the server once they are delivered to the recipient. So your Signal messages and contacts remain stored only locally on your device. Different from WhatsApp and Telegram, Signal does not offer cloud backup. What you can do is a [local backup], which is protected by a passphrase. Check where the local backup gets stored and make sure you don't delete it. You may also want to copy it to somewhere else.

Like Telegram, Signal allows you to [make messages disappear] after a certain time. This guarantees the privacy of the conversation, even if someone else gets access to your phone.

{% include update.html date="May 17, 2021" message="As well as Telegram, Signal also offers a [lock system](https://support.signal.org/hc/en-us/articles/360007059572-Screen-Lock), but based on your smartphone's lock screen security features, be it a password or fingerprint. This feature is only available on mobile apps. Signal for desktop does not currently support screen lock." %}

Also like WhatsApp and Telegram, Signal requires a valid phone number to register and find contacts, which is the main source of criticism the app receives. It should be noted, though, that contacts are stored locally only.

Signal is completely open source: the source code for all of the Signal clients and the Signal server is available on [GitHub]. The Android client allows for [reproducible builds]. At least in theory, you can [run your own (self-hosted) Signal server][quora], but that seems a lot of work, as you would need to tweak the code and build server and clients. That would actually be a fork.

The Signal protocol has been [professionally audited] for security vulnerabilities. It is considered to be a state-of-the-art end-to-end encryption protocol, and is used even by messengers other than Signal itself, such as [WhatsApp][signal-whatsapp], [Facebook Messenger] and [Skype].

Signal was born in 2013 as the merge of two mobile apps for encrypted communication, RedPhone and TextSecure, both developed by cryptographer and privacy activist [Moxie Marlinspike] at Open Whisper Systems. In 2017, Brian Acton, a co-founder of WhatsApp, left Facebook due to intractable differences about privacy practices. In the following year, Acton and Marlinspike partnered to form the [Signal Foundation], a US-based non-profit organization. The foundation was started with an initial $50 million in funding from Acton.

Now, the Signal Foundation runs entirely on donations, which help pay for the servers, bandwidth, and staff for developing the app. Signal is free for use and doesn't show ads.

You can use Signal on your desktop, but you must install it first on your phone.

The Signal app for Android can be downloaded from the [Google Play].

On openSUSE, you can retrieve Signal from the semi official **[signal-desktop]** package. But I personally found it easier to install Signal from [Flathub]:

```
# flatpak install org.signal.Signal
```

Signal is also available for other systems. For more options, take a look at: [signal.org](https://www.signal.org/download/).

There is a very complete [Support] section in the Signal website.

This text is part of the series:

- [Open-source privacy-focused messaging apps alternatives to WhatsApp][instant messenger]

[signal]:                   https://www.signal.org/
[instant messenger]:        {% post_url en/2021-05-10-open-source-privacy-focused-messaging-apps-alternatives-to-whatsapp %}
[whatsapp]:                 https://www.whatsapp.com/
[telegram]:                 {% post_url en/2021-05-10-the-telegram-messenger %}
[sms]:                      https://support.signal.org/hc/en-us/articles/360007321171-Can-I-send-SMS-MMS-with-Signal-
[edward snowden]:           https://en.wikipedia.org/wiki/Edward_Snowden
[elon musk]:                https://en.wikipedia.org/wiki/Elon_Musk
[google play]:              https://play.google.com/store/apps/details?id=org.thoughtcrime.securesms
[subpoena]:                 https://signal.org/bigbrother/eastern-virginia-grand-jury/
[local backup]:             https://support.signal.org/hc/en-us/articles/360007059752-Backup-and-Restore-Messages
[make messages disappear]:  https://support.signal.org/hc/en-us/articles/360007320771-Set-and-manage-disappearing-messages
[github]:                   https://github.com/signalapp
[reproducible builds]:      https://github.com/signalapp/Signal-Android/tree/master/reproducible-builds
[quora]:                    https://www.quora.com/Can-you-run-your-own-signal-messaging-server/answer/Aleksey-Yavorskiy
[professionally audited]:   https://eprint.iacr.org/2016/1013.pdf
[signal-whatsapp]:          https://signal.org/blog/whatsapp-complete/
[facebook messenger]:       https://signal.org/blog/facebook-messenger/
[skype]:                    https://signal.org/blog/skype-partnership/
[moxie marlinspike]:        https://www.newyorker.com/magazine/2020/10/26/taking-back-our-privacy
[signal foundation]:        https://signal.org/blog/signal-foundation/
[signal-desktop]:           https://software.opensuse.org/package/signal-desktop
[flathub]:                  https://flathub.org/apps/details/org.signal.Signal
[support]:                  https://support.signal.org/
