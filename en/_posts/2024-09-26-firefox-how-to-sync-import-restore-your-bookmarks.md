---
date: '2024-09-26 00:30:00 GMT-3'
image: '/files/2024/09/firefox-sync-bookmarks.png'
layout: post
nickname: 'firefox-sync-bookmarks'
title: 'Firefox: how to sync / import / restore your bookmarks'
excerpt: 'Have you just formatted your computer? Changed computers? Migrating from one browser to another? Or from Windows to Linux? Whatever your situation is, you want to take your bookmarks with you. Here are some tips that can help you if you use Mozilla Firefox, the default browser for Linux Kamarada 15.5.'
---

{% include image.html src='/files/2024/09/firefox-sync-bookmarks.png' %}

Have you just formatted your computer? Changed computers? Migrating from one browser to another? Or from [Windows] to [Linux]? Whatever your situation is, you want to take your bookmarks with you. Here are some tips that can help you if you use [Mozilla Firefox], the default browser for [Linux Kamarada 15.5].

## The easiest way: sync

If you're moving to a new system or computer, or if you use Firefox on multiple devices (desktop, smartphone, tablet, etc.), the easiest way to take your bookmarks with you wherever you go is to keep them in sync across all your devices with [Firefox Sync][sync]. Set it up on all your devices, and every time you create, edit or delete a bookmark on one device, that change will be reflected on the other devices when you open Firefox on them.

Firefox Sync synchronizes not only your bookmarks, but also several other browser data and preferences, including passwords, history, extensions, among others. To use it, you need a [Mozilla Account].

To enable Firefox Sync on your computer, open the **Firefox menu** in the top-right corner of the window and, next to **Sync and save data**, click the **Sign In** button:

{% include image.html src='/files/2024/09/firefox-sync-01-en.jpg' %}

Enter your email address and password to sign in to your Mozilla Account, or create one if you don't have one already:

{% include image.html src='/files/2024/09/firefox-sync-02-en.jpg' %}

In the end, if everything goes well, you should see this message: **You're signed in to Firefox**.

{% include image.html src='/files/2024/09/firefox-sync-03-en.jpg' %}

If you previously connected another Firefox instance to your Mozilla Account, Firefox Sync will now download your bookmarks and display them in the bookmarks bar. Note that if you already had bookmarks in this instance before syncing, they will be merged with the ones you already had in your Mozilla Account.

## Changing browsers?

If you're just moving from another browser on the same system -- say you were used to another browser, such as [Google Chrome], and want to migrate to Firefox -- you can import bookmarks from other browsers into Firefox (and other data such as history and passwords).

To do this, open the **Firefox menu** and click **Settings**:

{% include image.html src='/files/2024/09/firefox-import-other-browser-01-en.jpg' %}

Under the **Import Browser Data** section, click the **Import Data** button:

{% include image.html src='/files/2024/09/firefox-import-other-browser-02-en.jpg' %}

Choose from the list the browser whose data you want to import to Firefox and click **Import**:

{% include image.html src='/files/2024/09/firefox-import-other-browser-03-en.jpg' %}

Finally, click **Done**:

{% include image.html src='/files/2024/09/firefox-import-other-browser-04-en.jpg' %}

Now you should notice the other browser's bookmarks imported into your Firefox.

## Exporting Firefox bookmarks

You can export your bookmarks to an HTML file, which can be saved as a backup of your bookmarks, or imported into another web browser such as Google Chrome or even Firefox itself.

To do this, open the **Firefox menu**, click **Bookmarks** and then click **Manage bookmarks** at the bottom:

{% include image.html src='/files/2024/09/firefox-export-01-en.jpg' %}

On the **Library** window, open the **Import and Backup** menu and click **Export Bookmarks to HTML**:

{% include image.html src='/files/2024/09/firefox-export-02-en.jpg' %}

Choose a location to save the file, which is named `bookmarks.html` by default.

Back to the **Library** window, you can close it.

Your bookmarks are now successfully exported from Firefox. The HTML file you saved is now ready to be imported into another web browser.

## Importing bookmarks into Firefox

You can import into Firefox bookmarks exported from another browser such as Google Chrome or even Firefox itself. These bookmarks need to have been exported as an HTML file.

To do this, open the **Firefox menu**, click **Bookmarks** and then click **Manage bookmarks**.

On the **Library** window, open the **Import and Backup** menu and click **Import Bookmarks from HTML**.

Navigate to the HTML file containing the bookmarks to be imported and open it.

The bookmarks in the selected HTML file will be added to your Firefox bookmarks within the **Bookmarks Menu** folder.

If you expected to see the imported bookmarks on the bookmarks bar and they didn't appear there, notice on the **Library** window that there is a **Bookmarks bar** (imported) folder inside the **Bookmarks Menu** folder and a **Bookmarks Toolbar** (native) folder inside **All Bookmarks**:

{% include image.html src='/files/2024/09/firefox-import-en.jpg' %}

The bookmarks that appear on the Firefox bookmarks bar are in the **Bookmarks Toolbar** folder inside **All Bookmarks**. You may need to manually organize your bookmarks to get them to appear where you want them. Use the **Library** window to do this. When you're done, you can close it.

## Manually restoring bookmarks

If you formatted your computer, or changed computers, and you can no longer open Firefox on the previous system, but you can access its files, your only option may be to manually restore your bookmarks, without the aid of a graphical interface.

All of the changes you make in Firefox, such as your home page, extensions, saved passwords and bookmarks, are stored in a special folder called a **[profile]**. The profile folder is stored in your home folder, separate from the Firefox program, so that if something goes wrong with Firefox, your information will still be there.

To open the folder for the profile you are currently using in your current Firefox instance, open the **Firefox menu**, click **Help** and then click **More troubleshooting information**.

On the page that opens, next to **Profile Directory**, click **Open Directory**:

{% include image.html src='/files/2024/09/firefox-profile-01-en.jpg' %}

The profile folder is opened on the **Files** app.

Locate in this folder the file called `places.sqlite`:

{% include image.html src='/files/2024/09/firefox-profile-02-en.jpg' %}

This file is a database that contains your bookmarks and lists of all the files you've downloaded and websites you've visited.

Close Firefox and rename this file to something like `places.sqlite-original`. You can also make a copy of this file somewhere else, just in case.

Open a new **Files** tab or window and locate the `places.sqlite` file for the old profile.

By default, Firefox stores profiles:

- On Windows, at: `C:\Users\YourUsername\AppData\Roaming\Mozilla\Firefox\Profiles\`; and
- On Linux, at: `/home/yourusername/.mozilla/firefox/`.

To find the old profile, you may need to enable showing hidden files. To do this, open the **Files menu** in the top-right corner of the window and activate the **Show Hidden Files** option:

{% include image.html src='/files/2024/09/firefox-profile-03-en.jpg' %}

Copy the `places.sqlite` file from the old profile folder to the current profile folder.

Open Firefox and notice that your old bookmarks now appear on the bookmarks bar.

Export these bookmarks following the instructions above.

Close Firefox once more. Back to **Files**, in the current profile folder, delete the `places.sqlite` file and rename `places.sqlite-original` back to `places.sqlite`.

Open Firefox once more. You will notice that the bookmarks that appear now are the ones you had before.

Finally, you can import the bookmarks from the old profile that you just exported.

## References

I hope those tips have helped you recover your Firefox bookmarks after formatting or changing computers or systems. This article was a compilation of information found on several pages of the official Firefox documentation. If you need more information, you can take a look at them:

- [Bookmarks in Firefox \| Firefox Help](https://support.mozilla.org/en-US/kb/bookmarks-firefox)
- [How do I set up Sync on my computer? \| Mozilla Support](https://support.mozilla.org/en-US/kb/how-do-i-set-sync-my-computer)
- [Sync Firefox data \| Mozilla Support][sync]
- [Import data from another browser \| Firefox Help](https://support.mozilla.org/en-US/kb/import-data-another-browser)
- [Export Firefox bookmarks to an HTML file to back up or transfer bookmarks \| Firefox Help](https://support.mozilla.org/en-US/kb/export-firefox-bookmarks-to-backup-or-transfer)
- [Import Bookmarks from an HTML file \| Firefox Help](https://support.mozilla.org/en-US/kb/import-bookmarks-html-file)
- [Restore bookmarks from backup or move them to another computer \| Firefox Help](https://support.mozilla.org/en-US/kb/restore-bookmarks-from-backup-or-move-them)
- [Profiles - Where Firefox stores your bookmarks, passwords and other user data \| Firefox Help][profile]

[Windows]:              https://www.microsoft.com/windows/
[Linux]:                https://www.kernel.org/linux.html
[Mozilla Firefox]:      {% post_url en/2019-07-20-20-apps-you-can-use-the-same-way-on-both-linux-and-windows-part-1 %}#1-mozilla-firefox
[Linux Kamarada 15.5]:  {% post_url en/2024-05-27-linux-kamarada-15-5-more-aligned-with-opensuse-leap-and-other-distributions %}
[sync]:                 https://support.mozilla.org/en-US/kb/sync
[Mozilla Account]:      https://www.mozilla.org/account/
[Google Chrome]:        {% post_url en/2019-07-20-20-apps-you-can-use-the-same-way-on-both-linux-and-windows-part-1 %}#2-google-chrome
[profile]:              https://support.mozilla.org/en-US/kb/profiles-where-firefox-stores-user-data
