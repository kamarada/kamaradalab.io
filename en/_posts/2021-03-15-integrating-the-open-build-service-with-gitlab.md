---
date: 2021-03-15 11:30:00 GMT-3
image: '/files/2021/03/obs-and-gitlab.jpg'
layout: post
nickname: 'obs-and-gitlab'
title: 'Integrating the Open Build Service with GitLab'
excerpt: 'Get your software built and available for download right after a git push!'
---

{% include image.html src="/files/2021/03/obs-and-gitlab.jpg" %}

As we've seen in the previous post, [Linux Kamarada is moving to GitLab][moving-to-gitlab]. If you follow me for some time, you know that the distribution development took place not only at [GitHub], but also at the [Open Build Service (OBS)][obs]. While [Git] manages the source code, OBS builds the packages and ISO images of the distribution. I already wrote a text explaining [how to integrate the Open Build Service with GitHub][obs-and-github]. With the migration from GitHub to GitLab, a question arises: is it possible to integrate the Open Build Service with GitLab?

Actually, before starting the migration, I had already done this test and the answer is: yes!

I write this text to share how to do this, just in case anyone else has that same question. I'm going to repeat some information from the [OBS and GitHub how-to][obs-and-github] to make this new how-to self-contained. If you haven't read the previous how-to, you don't need to. If you have, feel free to skip parts that looks familiar to you.

## Meet the Open Build Service (OBS)

If you develop software for [Linux], you should know the [**Open Build Service (OBS)**][obs]. What does it do that makes it such an interesting tool? Let me quote [its website][obs]:

> The Open Build Service (OBS) is a generic system to build and distribute binary packages from sources in an automatic, consistent and reproducible way. You can release packages as well as updates, add-ons, appliances and entire distributions for a wide range of operating systems and hardware architectures.

In practice, you software developer can upload your software souce to OBS and build ready to install packages for [openSUSE], [Fedora], [Debian], [Ubuntu] and other distributions, as well as packages for [i586], [x86_64], [ARM] and other architectures. Your software users can download those packages directly from OBS. It is also able to build [Live images][what-is-a-livecd-dvd-usb], so users can try your software on a clean and controlled system prior to actually installing anything.

OBS is [free software][free-software], so you can download, install and run it on a server of your own. Or (easier) you can use the reference server publicly and freely available at [build.opensuse.org][buildoo]. That server is used mainly for development of the openSUSE distribution, but it also hosts many community projects. Among them, the [Linux Kamarada][kamarada-15.2] distribution.

OBS offers its own [source code management][scm] system, analogous to Git, and you can store and manage your source code right in OBS. Or, if you already use Git with a service such as GitLab, you can set up OBS to get the source code from that service, so that your software is always built, packaged and made available for download right after a `git push`. Now you are going to see how to set up that integration.

## OBS basics

I'm not going to dive into OBS basics here, I'll write an entire post about them someday. By now, you can find how to get started with OBS here:

- [Documentation - Open Build Service][obs-help]
- [openSUSE:Build Service Tutorial - openSUSE Wiki][obs-tutorial]

I assume that you are able to login with your openSUSE account at [build.opensuse.org][buildoo] (if you don't have an account yet, click the **Sign Up** link at the top of the page to create one).

Create an empty package for your software at OBS. Here I'm going to use as example the Linux Kamarada's [patterns] GitLab repository, which by now has just a PNG image and a spec file.

If you don't know what a spec file is and/or are new to [RPM] packaging, read these:

- [Fedora RPM Guide][fedora-rpm-guide]
- [openSUSE:Packaging guidelines - openSUSE Wiki][opensuse-packaging]

I also assume that you have installed [**osc**][osc], the OBS command-line client, on your machine. If you are using openSUSE, you can do that running:

```
# zypper install osc
```

Checkout your OBS home project using:

```
$ osc checkout home:username
```

Enter the package directory:

```
$ cd home:username/packagename
```

Now let's integrate OBS with GitLab.

## OBS: retrieving sources from GitLab

Set up the [source service][obs-user-guide-3] to retrieve sources from GitLab using **osc**:

```
$ osc add https://gitlab.com/kamarada/patterns.git
```

Edit the generated `_service` file and exchange its contents with:

```xml
<services>
    <service name="obs_scm">
        <param name="scm">git</param>
        <param name="url">https://gitlab.com/kamarada/patterns.git</param>
        <param name="revision">15.3-dev</param>
        <param name="extract">pattern-kamarada-gnome.png</param>
        <param name="extract">patterns-kamarada-gnome.spec</param>
    </service>
</services>
```

Note that I'm using the `15.3-dev` branch. If you are going to use the `master` branch for your package, you can just omit the `revision` parameter.

Commit changes:

```
$ osc commit
```

Commiting to OBS automatically triggers the source service and the build process. If you go to [build.opensuse.org][buildoo], your project is already building:

{% include image.html src="/files/2021/03/obs-and-gitlab-1.jpg" %}

In order for GitLab to trigger your package build you need to generate an [authorization token][obs-user-guide-2] using **osc**:

```
$ osc token --create --operation runservice home:username packagename
```

The command will return the token and its id:

```xml
<status code="ok">
  <summary>Ok</summary>
  <data name="token">AcRaZyStRiNgWhIcHiSyOuRtOkeN</data>
  <data name="id">4321</data>
</status>
```

As a test, you can manually trigger your package build using the generated token:

```
$ osc token --trigger AcRaZyStRiNgWhIcHiSyOuRtOkeN
```

If you go to [build.opensuse.org][buildoo], your project is building.

## GitLab: alerting OBS of new commits

We want to update our package sources in OBS whenever they change in GitLab. We can have GitLab trigger that update automatically setting up a [GitLab webhook][gitlab-webhook].

To do this, execute the following steps.

Sign in to GitLab and go to your repository (in my case, [https://gitlab.com/kamarada/patterns](https://gitlab.com/kamarada/patterns)). Go to **Settings** > **Webhooks**:

{% include image.html src="/files/2021/03/obs-and-gitlab-2.jpg" %}

Provide the following parameters:

- **URL:** `https://build.opensuse.org/trigger/runservice?project=home:username&package=packagename`
- **Secret token:** `AcRaZyStRiNgWhIcHiSyOuRtOkeN`
- Under **Trigger**, enable **Push events** (actually, it is already enabled by default) and enter the _branch_ name `15.3-dev`

{% include image.html src="/files/2021/03/obs-and-gitlab-3.jpg" %}

Scroll down the page and, by the end, under **SSL verification**, check **Enable SSL verification** (actually, it is also already checked by default). To finish, click the **Add webhook** green button:

{% include image.html src="/files/2021/03/obs-and-gitlab-4.jpg" %}

Back to the previous page, you can see the added webhook:

{% include image.html src="/files/2021/03/obs-and-gitlab-5.jpg" %}

You can test the webhook by clicking **Test** and then **Push events**:

{% include image.html src="/files/2021/03/obs-and-gitlab-6.jpg" %}

If you go to [build.opensuse.org][buildoo], your project is building.

From now on, each `git push` to GitLab will automatically trigger a package rebuild in OBS. Your username will appear in the `osc log` source history.

Have a lot of fun!

## References

- [How to integrate external SCM sources - OBS User Guide][obs-user-guide-1]
- [Authorization - OBS User Guide][obs-user-guide-2]
- [Using Source Services - OBS User Guide][obs-user-guide-3]

[moving-to-gitlab]:         {% post_url en/2021-03-10-linux-kamarada-is-moving-to-gitlab %}
[github]:                   https://github.com/
[obs]:                      https://openbuildservice.org/
[git]:                      https://git-scm.com/
[obs-and-github]:           {% post_url en/2019-03-19-integrating-the-open-build-service-with-github %}
[linux]:                    https://www.kernel.org/linux.html
[opensuse]:                 https://www.opensuse.org/
[fedora]:                   https://getfedora.org/
[debian]:                   https://www.debian.org
[ubuntu]:                   https://www.ubuntu.com/
[i586]:                     https://en.wikipedia.org/wiki/X86
[x86_64]:                   https://en.wikipedia.org/wiki/X86-64
[arm]:                      https://en.wikipedia.org/wiki/ARM_architecture
[what-is-a-livecd-dvd-usb]: {% post_url en/2015-11-25-what-is-a-livecd-dvd-usb %}
[free-software]:            https://www.gnu.org/philosophy/free-sw.html
[buildoo]:                  https://build.opensuse.org
[kamarada-15.2]:            {% post_url en/2020-09-11-linux-kamarada-15.2-come-to-the-elegant-modern-green-side-of-the-force %}
[scm]:                      https://en.wikipedia.org/wiki/Source_code_management
[obs-help]:                 https://openbuildservice.org/help/
[obs-tutorial]:             https://en.opensuse.org/openSUSE:Build_Service_Tutorial
[patterns]:                 https://gitlab.com/kamarada/patterns/-/tree/15.3-dev
[rpm]:                      https://en.wikipedia.org/wiki/RPM_Package_Manager
[fedora-rpm-guide]:         http://docs.fedoraproject.org/en-US/Fedora_Draft_Documentation/0.1/html/RPM_Guide/index.html
[opensuse-packaging]:       https://en.opensuse.org/openSUSE:Packaging_guidelines
[osc]:                      https://linux.die.net/man/1/osc
[gitlab-webhook]:           https://docs.gitlab.com/ee/user/project/integrations/webhooks.html
[obs-user-guide-1]:         https://openbuildservice.org/help/manuals/obs-user-guide/cha.obs.best-practices.scm_integration.html
[obs-user-guide-2]:         https://openbuildservice.org/help/manuals/obs-user-guide/cha.obs.authorization.token.html
[obs-user-guide-3]:         https://openbuildservice.org/help/manuals/obs-user-guide/cha.obs.source_service.html
