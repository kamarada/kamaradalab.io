---
date: '2024-05-08 23:50:00 GMT-3'
image: '/files/2024/05/15.5-rc.jpg'
layout: post
published: true
nickname: 'kamarada-15.5-rc'
title: 'Linux Kamarada 15.5 enters Release Candidate (RC) phase'
---

{% include image.html src='/files/2024/05/15.5-rc.jpg' %}

The Linux Kamarada Project announces the 15.5 RC release of the homonym [Linux distribution][linux], based on [openSUSE Leap 15.5][leap-15.5]. It is now available for [download].

<!--more-->

The [Download] page has been updated and now offer mainly two releases for download:

- [15.4 Final][kamarada-15.4], which you can install on your home or work computer; and
- 15.5 RC, which you can test if you want to help in development.

Remember that the easiest way to test [Linux] is using a [VirtualBox] virtual machine. For more information on how to do this, read:

- [VirtualBox: the easiest way to try Linux without installing it][VirtualBox]

Or, if you want to test Linux Kamarada on your own machine, the easiest way to do this is using a USB stick that you can make bootable with [Ventoy]:

- [Ventoy: create a multiboot USB drive by simply copying ISO images to it][Ventoy]

If you want to install Linux Kamarada on your computer for daily use, it is recommended that you install the 15.4 Final release, which is already ready, and then, when the 15.5 release is ready, upgrade. For more information on Linux Kamarada 15.4 Final, see:

- [Linux Kamarada 15.4: more functional, beautiful, polished and elegant than ever][kamarada-15.4]

## What is a release candidate?

A [**release candidate**][rc] (**RC**) is a software version that is almost ready to be released to the market as a stable product. This release may become the final release, unless significant bugs are detected. At this stage of development, all features planned at the beginning are already present and no new features are added.

A release candidate is also a kind of [beta] version. Therefore, bugs are expected. If you find a bug, please report it. Take a look at the [Help] page to see how to get in touch.

Of course, bugs can be found and fixed anytime, but the sooner the better!

## What's new?

Compared to [15.5 Beta], the 15.5 RC release features updated packages. Other than that, there were no major changes.

## Upgrading to 15.5 RC

If you are already using Linux Kamarada 15.4 and trust your personal experience as an user, you may want to upgrade to Linux Kamarada 15.5 RC to help test the upgrade process. This tutorial shows how to do that:

- [Linux Kamarada 15.4 and openSUSE Leap 15.4: how to upgrade to 15.5][upgrade-howto]

If you are already using Linux Kamarada 15.5 Beta and want to upgrade to 15.5 RC, just update your system as usual:

- [How to get updates for openSUSE Linux][update-howto]

## Specs

To allow comparing Linux Kamarada releases, here is a summary of the software contained on Linux Kamarada 15.5 RC Build 5.69:

- Linux kernel 5.14.21
- X.Org display server 21.1.4 (with Wayland 1.21.0 enabled by default)
- GNOME desktop 41.9 including its core apps, such as Files (previously Nautilus), Calculator, Terminal, Text editor (gedit), Screenshot and others
- LibreOffice office suite 24.2.1.2
- Mozilla Firefox 115.10.0 ESR (default web browser)
- Chromium 122 web browser (alternative web browser)
- VLC media player 3.0.20
- Evolution email client 3.42.4
- YaST control center
- Brasero 3.12.3
- CUPS 2.2.7
- Drawing 1.0.1
- Firewalld 0.9.3
- GParted 1.4.0
- HPLIP 3.21.10
- Java (OpenJDK) 11.0.23
- KeePassXC 2.7.8
- Linphone 5.0.5
- PDFsam Basic 5.2.3
- Pidgin 2.14.8
- Python 3.6.15
- Samba 4.17.12
- Tor 0.4.8.10
- Transmission 3.00
- Vim 9.1.0330
- Wine 8.0
- Calamares installer 3.2.62
- Flatpak 1.14.5
- games: AisleRiot (Solitaire), Chess, Mahjongg, Mines (Minesweeper), Nibbles (Snake), Quadrapassel (Tetris), Reversi, Sudoku

That list is not exhaustive, but it gives a notion of what can be found in the distribution.

[linux]:            https://www.kernel.org/linux.html
[leap-15.5]:        {% post_url en/2023-06-07-leap-15-5-release-matures-sets-up-technological-transition %}
[download]:         /en/download
[kamarada-15.4]:    {% post_url en/2023-02-27-linux-kamarada-15-4-more-functional-beautiful-polished-and-elegant-than-ever %}
[VirtualBox]:       {% post_url en/2019-10-10-virtualbox-the-easiest-way-to-try-linux-without-installing-it %}
[Ventoy]:           {% post_url en/2020-07-29-ventoy-create-a-multiboot-usb-drive-by-simply-copying-iso-images-to-it %}
[rc]:               https://en.wikipedia.org/wiki/Software_release_life_cycle#Release_candidate
[beta]:             https://en.wikipedia.org/wiki/Software_release_life_cycle#BETA
[Help]:             /en/help
[15.5 Beta]:        {% post_url en/2023-11-01-linux-kamarada-15-5-beta-help-test-the-best-release-of-the-distribution %}
[upgrade-howto]:    {% post_url en/2023-11-27-linux-kamarada-15-4-and-opensuse-leap-15-4-how-to-upgrade-to-15-5 %}
[update-howto]:     {% post_url en/2019-05-20-how-to-get-updates-for-opensuse-linux %}
