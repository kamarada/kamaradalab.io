---
date: 2021-06-10 11:10:00 GMT-3
image: '/files/2021/04/grub-error.jpg'
layout: post
published: true
nickname: 'reinstall-grub'
title: 'How to reinstall GRUB'
---

{% include image.html src="/files/2021/04/grub.png" %}

[GRUB] is the default boot loader in [openSUSE]. A boot loader is the first program loaded from disk, responsible for loading the kernel and starting the operating system. GRUB is known for the menu it displays when the computer is turned on, especially useful on dual-boot computers, that is, computers on which two (or more) operating systems are installed (e.g. [Linux] and [Windows]) and, when turned on, their users need to choose which system to start.

In some situations, this menu may stop working. As it happened to me while testing [openSUSE Leap 15.3][leap-15.3] when it was [beta]. After [downloading system updates][how-to-update] with **zypper up** and restarting, the GRUB menu did not appear, with the following error message:

{% include image.html src="/files/2021/04/grub-error.jpg" %}

```
Welcome to GRUB!

error: symbol 'grub_verify_string' not found.
Entering rescue mode...
grub rescue>
```

That made me report the bug [boo#1183884][bugzillaoo] to the openSUSE Project.

That also made me search how to reinstall GRUB. And I found two possible solutions, which I share below, in case they are helpful to anyone else.

To try any of these solutions, since you can't access the operating system installed on your computer, you'll need a [live image][live] burned to a DVD or flashed to a USB stick. You can download [Linux Kamarada 15.2][kamarada-15.2] from the [Download] page.

Start by booting from the live system and identifying your disk partitions. You can do this, for example, with the **[fdisk]** command:

```
# fdisk -l
Disk /dev/sda: 447.1 GiB, 480103981056 bytes, 937703088 sectors
Disk model: KINGSTON SA400S3
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: C5E68EC3-1350-4E0C-A335-252FF0FDD675

Device         Start       End   Sectors  Size Type
/dev/sda1       2048    206847    204800  100M EFI System
/dev/sda2     206848    239615     32768   16M Microsoft reserved
/dev/sda3     239616 208864604 208624989 99.5G Microsoft basic data
/dev/sda4  208865280 209919999   1054720  515M Windows recovery environment
/dev/sda5  209920000 419635199 209715200  100G Linux filesystem
/dev/sda6  902049792 937701375  35651584   17G Linux filesystem
/dev/sda7  419635200 902049791 482414592  230G Linux filesystem

Partition table entries are not in disk order.
```

In my case, the information that matters is:

- the operating system is installed on `/dev/sda5` (this is the root partition)
- the EFI partition is `/dev/sda1`
- therefore, the disk I'm going to work on is `/dev/sda`

To illustrate the commands, I'm going to consider that information. Be careful to run different commands adjusted to your computer setup.

## 1) Installed and live systems coincide

The slightly less laborious solution (which requires the execution of fewer commands) should work when you have the live image for the system that is installed on your computer. For example, you installed Linux Kamarada 15.2 on your computer, and now GRUB is having a problem, and you have a Linux Kamarada 15.2 live image.

Create a mount point for the root partition and mount it:

```
# mkdir -p /mnt/opensuse
# mount /dev/sda5 /mnt/opensuse -o subvol=@
```

Then mount the EFI partition inside it, if you have one:

```
# mount /dev/sda1 /mnt/opensuse/boot/efi/
```

Run the following command to reinstall GRUB:

```
# grub2-install --root-directory=/mnt/opensuse /dev/sda
Installing for x86_64-efi platform.
Installation finished. No error reported.
```

(here, as usual, I'm talking about Linux Kamarada and openSUSE, if your scenario is different — for example, you are using an [Ubuntu] live image to reinstall GRUB into an Ubuntu installation — the above command may be different)

Finally, unmount the partitions and restart the computer:

```
# umount /mnt/opensuse/boot/efi/
# umount /mnt/opensuse/
# reboot
```

## 2) Different live system

This solution, at least in theory, should work even if different Linux distributions are being used. For example, if the system installed on your computer is Linux Kamarada 15.2, but now you only have an Ubuntu live image (or vice versa). This works because this solution uses the **[chroot]** command to enter the installed system. So the GRUB that is going to be reinstalled is the same GRUB of the installed distribution, regardless of the live image distribution.

Create a mount point for the root partition and mount it:

```
# mkdir -p /mnt/opensuse
# mount /dev/sda5 /mnt/opensuse -o subvol=@
```

Then mount the EFI partition inside it, if you have one:

```
# mount /dev/sda1 /mnt/opensuse/boot/efi/
```

Now let's make the installed system "borrow" some filesystems of the live system, using the **-B** (or **\--bind**) option of the **[mount]** command:

```
# mount -B /dev /mnt/opensuse/dev
# mount -B /dev/pts /mnt/opensuse/dev/pts
# mount -B /proc /mnt/opensuse/proc
# mount -B /run /mnt/opensuse/run
# mount -B /sys /mnt/opensuse/sys
```

Then enter the installed system using the **chroot** command:

```
# chroot /mnt/opensuse
```

From now on and until we exit the chroot environment, all commands will be run on the installed system, not on the live system. The partition you previously referred to as `/mnt/opensuse` is now the root partition (`/`) itself.

Run the following command to reinstall GRUB:

```
# grub2-install /dev/sda
Installing for x86_64-efi platform.
Installation finished. No error reported.
```

(here, I'm assuming that the installed system is Linux Kamarada or openSUSE, no matter which live system you are using, if your scenario is different — for example, you're using a Linux Kamarada live image to reinstall GRUB into an Ubuntu installation — the above command may be different)

Take the opportunity to recreate the initrd, which can be part of the problem as well:

```
# mkinitrd
Creating initrd: /boot/initrd-5.3.18-47-default
dracut: Executing: /usr/bin/dracut --logfile /var/log/YaST2/mkinitrd.log --force /boot/initrd-5.3.18-47-default 5.3.18-47-default

[...]

dracut: *** Store current command line parameters ***
dracut: Stored kernel commandline:
dracut:  root=UUID=d9dd0940-6312-456a-b769-ccbea177fdc2 rootfstype=btrfs rootflags=rw,relatime,ssd,space_cache,subvolid=256,subvol=/@,subvol=@
dracut: *** Creating image file '/boot/initrd-5.3.18-lp152.63-preempt' ***
dracut: *** Creating initramfs image file '/boot/initrd-5.3.18-lp152.63-preempt' done ***
```

(this command may take a few minutes)

Finally, exit the installed system, unmount the partitions and restart the computer:

```
# exit
# umount /mnt/opensuse/sys
# umount /mnt/opensuse/run
# umount /mnt/opensuse/proc
# umount /mnt/opensuse/dev/pts
# umount /mnt/opensuse/dev
# umount /mnt/opensuse/boot/efi/
# umount /mnt/opensuse/
# reboot
```

## Conclusion

Remove the live media so the computer boots from its own disk and _voilà_: the GRUB menu reappears.

## References

- [How to setup a chroot environment using the rescue system in order to re-create the initrd \| Support \| SUSE][suse1]
- [How to reinstall the GRUB boot loader \| Support \| SUSE][suse2]
- [Fix grub on btrfs partition \| panticz.de][panticz]
- [How to Create and Mount Btrfs Subvolumes &#8211; Linux Hint][linuxhint]
- [GRUB rescue on UEFI based systems \| by Arnab Satapathi \| Medium][medium]

[grub]:             https://www.gnu.org/software/grub/
[opensuse]:         https://www.opensuse.org/
[linux]:            https://www.kernel.org/linux.html
[windows]:          https://www.microsoft.com/windows/
[leap-15.3]:        {% post_url en/2021-06-02-opensuse-leap-153-bridges-path-to-enterprise %}
[beta]:             https://en.wikipedia.org/wiki/Software_release_life_cycle#Beta
[how-to-update]:    {% post_url en/2019-05-20-how-to-get-updates-for-opensuse-linux %}
[bugzillaoo]:       https://bugzilla.opensuse.org/show_bug.cgi?id=1183884
[live]:             {% post_url en/2015-11-25-what-is-a-livecd-dvd-usb %}
[kamarada-15.2]:    {% post_url en/2020-09-11-linux-kamarada-15.2-come-to-the-elegant-modern-green-side-of-the-force %}
[download]:         /en/download/
[fdisk]:            https://man7.org/linux/man-pages/man8/fdisk.8.html
[ubuntu]:           https://ubuntu.com/
[chroot]:           https://www.man7.org/linux/man-pages/man1/chroot.1.html
[mount]:            https://www.man7.org/linux/man-pages/man8/mount.8.html
[suse1]:            https://www.suse.com/support/kb/doc/?id=000018801
[suse2]:            https://www.suse.com/support/kb/doc/?id=000016528
[panticz]:          http://www.panticz.de/Fix-grub-on-btrfs-partition
[linuxhint]:        https://linuxhint.com/create-mount-btrfs-subvolumes/
[medium]:           https://medium.com/@Arnab_Sat/grub-rescue-on-uefi-based-systems-7bf520e414aa
