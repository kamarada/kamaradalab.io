---
date: '2024-05-27 23:58:00 GMT-3'
image: '/files/2024/05/kamarada-15.4-eol.jpg'
layout: post
nickname: 'kamarada-15.4-eol'
title: 'Linux Kamarada 15.4 and openSUSE Leap 15.4 reached end of life'
excerpt: 'Support for openSUSE Leap 15.4, released on Jun 8, 2022, ended on Dec 31, 2023, totaling 18 months of security and bugfix support. The openSUSE Project recommends that openSUSE Leap 15.4 users upgrade to openSUSE Leap 15.5, released on Jun 7, 2023 and expected to be maintained until the end of December 2024, according to the openSUSE wiki.'
---

{% include image.html src='/files/2024/05/kamarada-15.4-eol.jpg' %}

## openSUSE Leap 15.4

Support for [openSUSE Leap 15.4][leap-15.4], released on Jun 8, 2022, ended on Dec 31, 2023, totaling 18 months of security and bugfix support.

The [openSUSE Project][opensuse] recommends that openSUSE Leap 15.4 users upgrade to [openSUSE Leap 15.5][leap-15.5], released on Jun 7, 2023 and expected to be maintained until the end of December 2024, according to the [openSUSE wiki][lifetime].

By then, we will probably have openSUSE Leap 15.6 already, which is [scheduled][leap-15.6-roadmap] to be released on Jun 12, 2024. Therefore, when Leap 15.6 is released, Leap 15.5 users will have approximately 6 months to upgrade their installations.

## Linux Kamarada 15.4

Since Linux Kamarada is based on [openSUSE Leap][leap], Linux Kamarada users are by transitivity openSUSE Leap users and receive the same updates.

The end of support of all Linux Kamarada releases should normally occur alongside the end of support of the equivalent openSUSE Leap release.

As [Linux Kamarada 15.5][kamarada-15.5] was released today, support for [Linux Kamarada 15.4][kamarada-15.4], released on Feb 27, 2023, has been extended to today, but it ends today, May 27, 2024.

If you use Linux Kamarada 15.4, I recommend you to upgrade to [Linux Kamarada 15.5][kamarada-15.5], expected to be maintained for the same period of time as openSUSE Leap 15.5, that is, until the end of December 2024.

## How to upgrade

If you use openSUSE Leap (or Linux Kamarada) 15.4, you can find instructions on how to upgrade to 15.5 here:

- [Linux Kamarada and openSUSE Leap: how to upgrade from 15.4 to 15.5][upgrade]

Instructions on that how-to apply to both distributions.

If you have any questions, write in the comments for this text or the tutorial above. You can also check other support options on the [Help] page.

[leap-15.4]:            {% post_url en/2022-06-08-leap-15-4-offers-new-features-familiar-stability %}
[opensuse]:             https://www.opensuse.org/
[leap-15.5]:            {% post_url en/2023-06-07-leap-15-5-release-matures-sets-up-technological-transition %}
[lifetime]:             https://en.opensuse.org/Lifetime
[leap-15.6-roadmap]:    https://en.opensuse.org/openSUSE:Roadmap#Schedule_for_openSUSE_Leap_15.6
[leap]:                 {% post_url en/2020-12-07-opensuse-leap-vs-opensuse-tumbleweed-what-is-the-difference %}
[kamarada-15.5]:        {% post_url en/2024-05-27-linux-kamarada-15-5-more-aligned-with-opensuse-leap-and-other-distributions %}
[kamarada-15.4]:        {% post_url en/2023-02-27-linux-kamarada-15-4-more-functional-beautiful-polished-and-elegant-than-ever %}
[upgrade]:              {% post_url en/2023-11-27-linux-kamarada-15-4-and-opensuse-leap-15-4-how-to-upgrade-to-15-5 %}
[Help]:                 /en/help
