---
date: 2021-12-28 23:00:00 GMT-3
image: '/files/2021/12/15.3-beta.png'
layout: post
published: true
nickname: 'kamarada-15.3-rc'
title: 'Linux Kamarada 15.3 enters Release Candidate (RC) phase'
---

{% include image.html src='/files/2021/12/15.3-beta.png' %}

Development is almost finished: I'm happy to unveil the Release Candidate (RC) for Linux Kamarada 15.3! Based on [openSUSE Leap 15.3][leap-15.3], it is now available for [download].

Right now, the [Download] page offers two releases to download:

- [15.2 Final][kamarada-15.2], which you can install on your home or work computer; and
- 15.3 RC, which you can test if you want to help in development.

The easiest way to test [Linux] is using a [VirtualBox] virtual machine. Read about it here:

- [VirtualBox: the easiest way to try Linux without installing it][virtualbox]

Or, if you want to test Linux Kamarada on your own machine, the easiest way to do this is using a USB stick that you can make bootable with [Ventoy]:

- [Ventoy: create a multiboot USB drive by simply copying ISO images to it][ventoy]

If you want to install Linux Kamarada on your computer for daily use, it is recommended that you install the 15.2 Final release, which is already ready, and then, when the 15.3 release is ready, upgrade. For more information on Linux Kamarada 15.2 Final, see:

- [Linux Kamarada 15.2: come to the elegant modern green side of the force!][kamarada-15.2]

## What is a release candidate?

A [**release candidate**][rc] (**RC**) is a software version that is almost ready to be released to the market as a stable product. This release may become the final release, unless significant bugs are detected. At this stage of development, all features planned at the beginning are already present and no new features are added.

A release candidate is also a kind of [beta] version. Therefore, bugs are expected. If you find a bug, please report it. Take a look at the [Help] page to see how to get in touch.

Of course, bugs can be found and fixed anytime, but the sooner the better!

## What's new?

In addition to what was new on [15.3 Beta][kamarada-15.3-beta], on the new 15.3 RC you will find:

- [Flathub], the main [Flatpak] repository, comes preconfigured as system-wide remote

- Some useful utilities for diagnosing hardware and filesystem problems were added: **[inxi]**, **[lshw]**, **[lsof]**, **[lspci]** and **[smartctl]**

- different from [openSUSE Leap 15.2], which had only one main update repository (`repo-update`), openSUSE Leap 15.3 now has [three main update repositories][release-notes], two of which (the new ones: `repo-backports-update` and `repo-sle-update`) were not present on Linux Kamarada 15.3 Beta and were added on 15.3 RC

## Upgrading to 15.3 RC

If you are already using Linux Kamarada 15.2 and trust your personal experience as an user, you may want to upgrade to Linux Kamarada 15.3 RC to help test the upgrade process. This tutorial shows how to do that:

- [Linux Kamarada and openSUSE Leap: how to upgrade from 15.2 to 15.3][upgrade-to-15.3]

If you are already using Linux Kamarada 15.3 Beta and want to upgrade to 15.3 RC, just update your system as usual:

- [How to get updates for openSUSE Linux][howto-update]

In both cases, after upgrading/updating, to make sure everyone is on the same page, add the Flathub repository, if you intend to use Flatpak to install programs:

```
# flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
```

## Specs

To allow comparing Linux Kamarada releases, here is a summary of the software contained on Linux Kamarada 15.3 RC Build 4.3:

- Linux kernel 5.3.18
- X.Org display server 1.20.3 (without Wayland)
- GNOME 3.34.7 desktop including its core apps, such as Files (previously Nautilus), Calculator, Terminal, Text editor (gedit), Screenshot and others
- LibreOffice office suite 7.1.4.2
- Mozilla Firefox 91.4.0 ESR (default web browser)
- Chromium 96 web browser (alternative web browser)
- VLC media player 3.0.16
- Evolution email client
- YaST control center
- Brasero
- CUPS 2.2.7
- Firewalld 0.9.3
- GParted 0.31.0
- HPLIP 3.20.11
- Java (OpenJDK) 11.0.13
- KeePassXC 2.6.6
- KolourPaint 20.04.2
- Linphone 4.1.1
- PDFsam Basic 4.2.10
- Pidgin 2.13.0
- Python 2.7.18 and 3.6.15
- Samba 4.13.13
- Tor 0.4.6
- Transmission 2.94
- Vim 8.0
- Wine 6.0
- Calamares installer 3.2.36
- Flatpak 1.10.5
- games: Aisleriot (Solitaire), Chess, Hearts, Iagno (Reversi), Mahjongg, Mines (Minesweeper), Nibbles (Snake), Quadrapassel (Tetris), Sudoku

That list is not exhaustive, but it gives a notion of what can be found in the distribution.

[leap-15.3]:            {% post_url en/2021-06-02-opensuse-leap-153-bridges-path-to-enterprise %}
[download]:             /en/download
[kamarada-15.2]:        {% post_url en/2020-09-11-linux-kamarada-15.2-come-to-the-elegant-modern-green-side-of-the-force %}
[linux]:                https://www.kernel.org/linux.html
[virtualbox]:           {% post_url en/2019-10-10-virtualbox-the-easiest-way-to-try-linux-without-installing-it %}
[ventoy]:               {% post_url en/2020-07-29-ventoy-create-a-multiboot-usb-drive-by-simply-copying-iso-images-to-it %}
[rc]:                   https://en.wikipedia.org/wiki/Software_release_life_cycle#Release_candidate
[beta]:                 https://en.wikipedia.org/wiki/Software_release_life_cycle#BETA
[help]:                 /en/help
[kamarada-15.3-beta]:   {% post_url en/2021-12-11-linux-kamarada-15-3-beta-help-test-the-best-release-of-the-distribution %}
[Flathub]:              https://flathub.org/
[Flatpak]:              https://flatpak.org/
[inxi]:                 https://smxi.org/docs/inxi.htm
[lshw]:                 https://www.ezix.org/project/wiki/HardwareLiSter
[lsof]:                 https://man7.org/linux/man-pages/man8/lsof.8.html
[lspci]:                https://man7.org/linux/man-pages/man8/lspci.8.html
[smartctl]:             https://www.smartmontools.org/
[openSUSE Leap 15.2]:   {% post_url en/2020-07-02-opensuse-leap-15-2-release-brings-exciting-new-artificial-intelligence-ai-machine-learning-and-container-packages %}
[release-notes]:        https://doc.opensuse.org/release-notes/x86_64/openSUSE/Leap/15.3/#installation-new-update-repos
[upgrade-to-15.3]:      {% post_url en/2021-12-23-linux-kamarada-and-opensuse-leap-how-to-upgrade-from-152-to-153 %}
[howto-update]:         {% post_url en/2019-05-20-how-to-get-updates-for-opensuse-linux %}
