---
date: '2022-12-19 23:00:00 GMT-3'
image: '/files/2022/12/15.4-rc.jpg'
layout: post
published: true
nickname: 'kamarada-15.4-rc'
title: 'Linux Kamarada 15.4 enters Release Candidate (RC) phase'
---

{% include image.html src='/files/2022/12/15.4-rc.jpg' %}

The Linux Kamarada Project announces the 15.4 RC release of the homonym [Linux distribution][linux], based on [openSUSE Leap 15.4][leap-15.4]. It is now available for [download].

<!--more-->

The [Download] page has been updated and now offer mainly two releases for download:

- [15.3 Final][kamarada-15.3], which you can install on your home or work computer; and
- 15.4 RC, which you can test if you want to help in development.

Remember that the easiest way to test [Linux] is using a [VirtualBox] virtual machine. For more information on how to do this, read:

- [VirtualBox: the easiest way to try Linux without installing it][virtualbox]

Or, if you want to test Linux Kamarada on your own machine, the easiest way to do this is using a USB stick that you can make bootable with [Ventoy]:

- [Ventoy: create a multiboot USB drive by simply copying ISO images to it][ventoy]

If you want to install Linux Kamarada on your computer for daily use, it is recommended that you install the 15.3 Final release, which is already ready, and then, when the 15.4 release is ready, upgrade. For more information on Linux Kamarada 15.3 Final, see:

- [Linux Kamarada 15.3: new year, new release][kamarada-15.3]

## What is a release candidate?

A [**release candidate**][rc] (**RC**) is a software version that is almost ready to be released to the market as a stable product. This release may become the final release, unless significant bugs are detected. At this stage of development, all features planned at the beginning are already present and no new features are added.

A release candidate is also a kind of [beta] version. Therefore, bugs are expected. If you find a bug, please report it. Take a look at the [Help] page to see how to get in touch.

Of course, bugs can be found and fixed anytime, but the sooner the better!

## Where is the 15.4 Beta?

openSUSE Leap 15.4 was released in [June][leap-15.4] and has since received various updates and fixes. The previous release, [15.3][leap-15.3], released in [June last year][leap-15.3], is going to reach EOL (end of life) on the last day of 2022, according to the [openSUSE news website][opensuse-news]. As a result, Linux Kamarada 15.3, based on openSUSE Leap 15.3, will also no longer be supported.

Unfortunately, I was unable to release Linux Kamarada 15.4 earlier. As the deadline is tight, and also because most of what's new in this release comes from the apps themselves (not from changes in the software selection), I decided to skip the 15.4 Beta release and launch the 15.4 RC release. Also because openSUSE Leap 15.4 presents considerable stability and the same is expected from Linux Kamarada 15.4, although still in RC phase.

I want to release 15.4 Final this week or next, so users will have some time to upgrade from 15.3, while still covered by official support from the [openSUSE Project].

## What's new?

What's new in the 15.4 release is:

- most of the same apps as in 15.3, but updated with new features and bug fixes;

- the most notable update was to the [GNOME] desktop, which on [Linux Kamarada 15.2][kamarada-15.2] and 15.3 was at version 3.34, and is now at 41.2;

- GNOME extensions, which were previously managed via the **[Tweaks]** app, are now managed via the new **[Extensions]** app;

- the **[Games]** app is not a [GNOME core app] anymore and is being [renamed] to **[Highscore]**, it has not been packaged for openSUSE Leap 15.4 and therefore was also removed from Linux Kamarada 15.4 (it is still possible to install Games from [Flathub], if desired);

- **[Maps]** was included on Linux Kamarada 15.4;

- the **[KolourPaint]** painting app (which is more appropriate for the [KDE] desktop, but also works on GNOME) was replaced by **[Drawing]**, which is a [GNOME circle app];

- Linux Kamarada 15.4 brings new beautiful wallpapers (but you can keep using the previous ones or the openSUSE defaults, if you prefer); and

- the 15.3 default [GTK] theme, the [Materia] theme, has not been updated, so it was replaced by the [Orchis] theme, which is based on Materia and keeps alignment with [Google]'s [Material Design] (the design that empowers [Android]).

## Upgrading to 15.4 RC

If you are already using Linux Kamarada 15.3 and trust your personal experience as an user, you may want to upgrade to Linux Kamarada 15.4 RC to help test the upgrade process. Generally speaking, you should just need to follow this upgrade how-to, replacing `15.3` with `15.4`, where applicable:

- [Linux Kamarada and openSUSE Leap: how to upgrade from 15.2 to 15.3][upgrade-howto]

Since Linux Kamarada 15.3 repos are already setup with the `$releasever` variable, it should be pretty easy to upgrade to 15.4. Examples of commands from the tutorial above, already adapted for the new release:

```
# zypper --releasever=15.4 ref
# zypper --releasever=15.4 dup --download-only --allow-vendor-change
```

## Specs

To allow comparing Linux Kamarada releases, here is a summary of the software contained on Linux Kamarada 15.4 RC Build 4.2:

- Linux kernel 5.14.21
- X.Org display server 1.20.3 (without Wayland)
- GNOME desktop 41.2 including its core apps, such as Files (previously Nautilus), Calculator, Terminal, Text editor (gedit), Screenshot and others
- LibreOffice office suite 7.3.6.2
- Mozilla Firefox 102.6.0 ESR (default web browser)
- Chromium 108 web browser (alternative web browser)
- VLC media player 3.0.17
- Evolution email client 3.42.4
- YaST control center
- Brasero 3.12.3
- CUPS 2.2.7
- Drawing 1.0.1
- Firewalld 0.9.3
- GParted 1.3.1
- HPLIP 3.21.10
- Java (OpenJDK) 11.0.17
- KeePassXC 2.7.4
- Linphone 4.3.2
- PDFsam Basic 4.3.4
- Pidgin 2.14.8
- Python 2.7.18 and 3.6.15
- Samba 4.15.8
- Tor 0.4.7
- Transmission 3.00
- Vim 9.0
- Wine 7.0
- Calamares installer 3.2.36
- Flatpak 1.12.5
- games: Aisleriot (Solitaire), Chess, Hearts, Iagno (Reversi), Mahjongg, Mines (Minesweeper), Nibbles (Snake), Quadrapassel (Tetris), Sudoku

That list is not exhaustive, but it gives a notion of what can be found in the distribution.

[linux]:            https://www.kernel.org/linux.html
[leap-15.4]:        {% post_url en/2022-06-08-leap-15-4-offers-new-features-familiar-stability %}
[download]:         /en/download
[kamarada-15.3]:    {% post_url en/2021-12-30-linux-kamarada-15-3-new-year-new-release %}
[virtualbox]:       {% post_url en/2019-10-10-virtualbox-the-easiest-way-to-try-linux-without-installing-it %}
[ventoy]:           {% post_url en/2020-07-29-ventoy-create-a-multiboot-usb-drive-by-simply-copying-iso-images-to-it %}
[rc]:               https://en.wikipedia.org/wiki/Software_release_life_cycle#Release_candidate
[beta]:             https://en.wikipedia.org/wiki/Software_release_life_cycle#BETA
[Help]:             /en/help
[leap-15.3]:        {% post_url en/2021-06-02-opensuse-leap-153-bridges-path-to-enterprise %}
[opensuse-news]:    https://news.opensuse.org/2022/12/12/leap-153-to-reach-eol/
[openSUSE Project]: https://www.opensuse.org/
[GNOME]:            https://www.gnome.org/
[kamarada-15.2]:    {% post_url en/2020-09-11-linux-kamarada-15.2-come-to-the-elegant-modern-green-side-of-the-force %}
[Tweaks]:           https://wiki.gnome.org/Apps/Tweaks
[Extensions]:       https://apps.gnome.org/app/org.gnome.Extensions/
[Games]:            https://wiki.gnome.org/Apps/Games
[GNOME core app]:   https://apps.gnome.org/#core
[renamed]:          https://gitlab.gnome.org/Archive/gnome-games/-/issues/243
[Highscore]:        https://gitlab.gnome.org/World/highscore
[Flathub]:          https://flathub.org/apps/details/org.gnome.Games
[Maps]:             https://apps.gnome.org/app/org.gnome.Maps/
[KolourPaint]:      https://apps.kde.org/kolourpaint/
[KDE]:              https://kde.org/
[Drawing]:          https://apps.gnome.org/app/com.github.maoschanz.drawing/
[GNOME circle app]: https://apps.gnome.org/#circle
[GTK]:              https://www.gtk.org/
[Materia]:          https://github.com/nana-4/materia-theme
[Orchis]:           https://github.com/vinceliuice/Orchis-theme
[Material Design]:  https://material.io/
[Google]:           https://www.google.com/
[Android]:          https://www.android.com/
[upgrade-howto]:    {% post_url en/2021-12-23-linux-kamarada-and-opensuse-leap-how-to-upgrade-from-152-to-153 %}
