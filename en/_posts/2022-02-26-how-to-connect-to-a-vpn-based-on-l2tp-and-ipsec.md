---
date: '2022-02-26 01:30:00 GMT-3'
image: '/files/2022/02/l2tp-client.jpg'
layout: post
nickname: 'l2tp-client'
title: 'How to connect to a VPN based on L2TP and IPsec'
---

{% include image.html src='/files/2022/02/l2tp-client.jpg' %}

On my [personal blog][antoniomedeiros-1], I made a [tutorial][antoniomedeiros-1] (in Brazilian Portuguese) explaining how to set up a [VPN] on a [MikroTik home router][antoniomedeiros-2] using [L2TP] (Layer 2 Tunneling Protocol) and [IPsec] (IP Security Protocol). The purpose of that VPN was to allow outside computers (e.g. me with my laptop connected to a cafe's Wi-Fi network) to connect to the home network. But note that organizations (such as companies and universities) may also provide VPNs of that type to allow people (staff and students) to remotely connect to their networks.

If you use a [Linux] computer and need to connect it to a VPN based on L2TP and IPsec, here you are going to see how to do this, installing and configuring the necessary software.

As a reference, I'm going to use the [Linux Kamarada 15.3] distribution. The instructions below also apply to the [openSUSE Project] distributions ([Leap and Tumbleweed]). I use the [GNOME] desktop, but the settings should be similar on other desktops.

## Installing the needed packages

Install the needed packages using the **[zypper]** package manager:

```
# zypper in NetworkManager-l2tp-gnome NetworkManager-l2tp-lang NetworkManager-strongswan-gnome NetworkManager-strongswan-lang strongswan-ipsec
```

## Configuring the VPN

Open the **Settings** app. You can do this by opening the **system menu**, at the upper-right corner of the screen, and clicking the **gear** icon:

{% include image.html src='/files/2022/02/l2tp-client-01-en.jpg' %}

On the **Settings** app, by the left, select **Network**. By the right, next to **VPN**, click the **add** button:

{% include image.html src='/files/2022/02/l2tp-client-02-en.jpg' %}

Choose the **Layer 2 Tunneling Protocol (L2TP)** VPN type:

{% include image.html src='/files/2022/02/l2tp-client-03-en.jpg' %}

Fill in the VPN settings according to the following instructions, but also pay attention to the orientations given to you by the network administrator who set up the VPN (the examples below refer to my [tutorial on how to set up a VPN on the MikroTik router][vpn]):

{% include image.html src='/files/2022/02/l2tp-client-04-en.jpg' %}

- In the **Name** field, define a name that will allow you to identify the VPN, it can be whatever name you want (example: `Test VPN`)
- In **Gateway**, inform the IP address (e.g. `179.216.177.166`) or DNS name and domain (e.g. `6bxxxxxxxxc2.sn.mynetname.net`) of the VPN server (in this case, the MikroTik router)
- Enter your **Username** to connect to the VPN (e.g. `test`)
- Enter your **Password** to connect to the VPN (e.g. `testing`)

Click the **IPsec Settings** button.

On the dialog box that appears, check the **Enable IPsec tunnel to L2TP host** option:

{% include image.html src='/files/2022/02/l2tp-client-05-en.jpg' %}

Enter the **Pre-shared key** (e.g. `12345678`).

Click **OK** to return to the VPN settings, then click **PPP Settings**.

Disable the **PAP** authentication method and click **OK**:

{% include image.html src='/files/2022/02/l2tp-client-06-en.jpg' %}

To finish configuring the VPN, click **Add**.

Back to the **Network** settings, note that the VPN now appears in the list of VPNs:

{% include image.html src='/files/2022/02/l2tp-client-07-en.jpg' %}

Using the **Settings** app, you can enable or disable the VPN connection, as well as change its settings. But let's see a more practical way to do this on a daily basis, without having to open this app. You can now close it.

## Using the VPN

To connect to the VPN, open the **system menu**, expand the VPN submenu and click **Connect**:

{% include image.html src='/files/2022/02/l2tp-client-08-en.jpg' %}

If the connection is successful, you will see an icon indicating it:

{% include image.html src='/files/2022/02/l2tp-client-09.jpg' %}

You can test the VPN connection with the **[ping]** command, pinging to some address on the VPN's local network:

```
$ ping 10.0.0.2
```

(use **Ctrl + C** to stop the **ping** command)

If you know of any web servers on the VPN's local network, you can also test the VPN connection by opening your browser and accessing that server. In this example, I test the VPN connection by accessing my printer's web interface:

{% include image.html src='/files/2022/02/l2tp-client-10-en.jpg' %}

When you no longer need to use the VPN, disconnect by opening the **system menu**, then expanding the VPN submenu and finally clicking **Disconnect**:

{% include image.html src='/files/2022/02/l2tp-client-11-en.jpg' %}

[antoniomedeiros-1]:    https://antoniomedeiros.dev/blog/2021/04/09/mikrotik-como-criar-uma-vpn-com-l2tp-e-ipsec/
[VPN]:                  {% post_url en/2017-07-07-how-to-setup-an-openvpn-client-on-opensuse-linux %}
[antoniomedeiros-2]:    https://antoniomedeiros.dev/blog/2020/07/20/primeiros-passos-com-o-mikrotik-hap-ac-roteador-profissional-para-a-rede-de-casa/
[L2TP]:                 https://en.wikipedia.org/wiki/Layer_2_Tunneling_Protocol
[IPsec]:                https://en.wikipedia.org/wiki/IPsec
[Linux]:                https://www.kernel.org/linux.html
[Linux Kamarada 15.3]:  {% post_url en/2021-12-30-linux-kamarada-15-3-new-year-new-release %}
[openSUSE Project]:     https://www.opensuse.org/
[Leap and Tumbleweed]:  {% post_url en/2020-12-07-opensuse-leap-vs-opensuse-tumbleweed-what-is-the-difference %}
[GNOME]:                https://www.gnome.org/
[zypper]:               https://en.opensuse.org/Portal:Zypper
[ping]:                 https://man7.org/linux/man-pages/man8/ping.8.html
