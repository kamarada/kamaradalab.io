---
date: 2023-02-27 11:20:00 GMT-3
image: '/files/2023/02/kamarada-15.4.jpg'
layout: post
published: true
nickname: 'kamarada-15.4-final'
title: 'Linux Kamarada 15.4: more functional, beautiful, polished and elegant than ever'
---

{% include image.html src='/files/2023/02/kamarada-15.4.jpg' %}

I am proud to announce that **Linux Kamarada 15.4** is ready for everyone to use!&nbsp;

Linux Kamarada is a [Linux] distribution based on [openSUSE Leap] and is intended for use on desktops at home and at work, in both private companies and government entities. It features the essential software selection for any Linux installation and a nice looking modern desktop.

Linux Kamarada 15.4 is not created "[from scratch][lfs]", but based on a major distribution that is [openSUSE Leap 15.4]. The same version number (15.4) was adopted to emphasize the alignment between the distros. While openSUSE Leap is a general purpose Linux distro, offering a stable operating system for both personal computers and servers, as well as tools for developers and system administrators, Linux Kamarada is focused on personal computers and novice users.

Newcomers can find the new release on the [Download] page. If you are a Linux Kamarada user already, you can find directions on how to upgrade on this page.

Let's see what's changed on Linux Kamarada from the previous release ([15.3]) to the current release (15.4).

[Linux]:                https://www.kernel.org/linux.html
[openSUSE Leap]:        {% post_url en/2020-12-07-opensuse-leap-vs-opensuse-tumbleweed-what-is-the-difference %}
[lfs]:                  https://www.linuxfromscratch.org/
[openSUSE Leap 15.4]:   {% post_url en/2022-06-08-leap-15-4-offers-new-features-familiar-stability %}
[Download]:             /en/download
[15.3]:                 {% post_url en/2021-12-30-linux-kamarada-15-3-new-year-new-release %}

<div class="d-flex align-items-center my-3">
    <div class="flex-shrink-0">
        <img src="/files/2020/02/desktop-environment-gnome.svg" style="max-width: 64px;">
    </div>
    <div class="flex-grow-1 ms-3">
        <h2 class="mb-0">Improvements to GNOME</h2>
    </div>
</div>

The most notable update was to the [GNOME] desktop, which on [Linux Kamarada 15.2][kamarada-15.2] and 15.3 was at version 3.34, and is now at 41. The GNOME Project has adopted a new, simpler version numbering scheme. Following version 3.34 we had: [3.36][gnome-3.36], [3.38][gnome-3.38], [40][gnome-40] and [41][gnome-41].

Generally speaking, GNOME's visual style has got smoother and more polished.

The **Activities** overview has received visual and functional improvements. The previously split **Frequent** and **All** app grid has been replaced with a single customizable view that allows you to reorder apps and organize them into custom folders.

<div class="row">
    <div class="col-md">
        {% include image.html src='/files/2023/02/kamarada-15.4-activities-en.jpg' caption='Activities overview' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2023/02/kamarada-15.4-apps-en.jpg' caption='App grid' %}
    </div>
</div>

A **Do Not Disturb** switch was added to the notifications popover. While enabled, notifications are hidden. It is possible to manually check them, opening the notifications popover. For notifications to appear again, just turn this switch off.

<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        {% include image.html src='/files/2023/02/kamarada-15.4-do-not-disturb-en.jpg' caption='Do Not Disturb' %}
    </div>
    <div class="col-md-3"></div>
</div>

The log in/log out and lock/unlock experiences have been improved. The new lock screen is more functional, easier to use, and elegant in its simplicity.

<div class="row">
    <div class="col-md">
        {% include image.html src='/files/2023/02/kamarada-15.4-system-menu-en.jpg' caption='System menu' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2023/02/kamarada-15.4-lock-screen-en.jpg' caption='Lock screen' %}
    </div>
</div>

<div class="d-flex">
    <div class="flex-grow-1">
        {% markdown %}

GNOME extensions, which were previously managed via the **[Tweaks]** app, are now managed via the new **[Extensions]** app, able to update, configure, remove or disable extensions.

[Tweaks]:       https://wiki.gnome.org/Apps/Tweaks
[Extensions]:   https://apps.gnome.org/app/org.gnome.Extensions/

        {% endmarkdown %}
    </div>
    <div class="flex-shrink-0 ms-3">
        <img src="/files/2023/02/org.gnome.Extensions.svg" style="max-width: 48px;">
    </div>
</div>

The **[Weather]** app has been redesigned and now features separate views for the hourly forecast (for the next 24 hours) and the daily forecast (for the next 10 days). Other apps also have been redesigned to provide a more polished and elegant experience, such as **[Screenshot]**, **[Sound Recorder]** and **[Software]**.

<div class="row">
    <div class="col-md">
        {% include image.html src='/files/2023/02/kamarada-15.4-weather-en.jpg' caption='Weather' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2023/02/kamarada-15.4-screenshot-en.jpg' caption='Screenshot' %}
    </div>
</div>

These were just some of the changes that caught my attention the most. If you want to know more things that changed between GNOME 3.34 and 41, I recommend reading the release notes for each version on the way:

- [GNOME 3.36 Release Notes][gnome-3.36]
- [GNOME 3.38 Release Notes][gnome-3.38]
- [GNOME 40 Release Notes][gnome-40]
- [GNOME 41 Release Notes][gnome-41]

[GNOME]:            https://www.gnome.org/
[kamarada-15.2]:    {% post_url en/2020-09-11-linux-kamarada-15.2-come-to-the-elegant-modern-green-side-of-the-force %}
[gnome-3.36]:       https://help.gnome.org/misc/release-notes/3.36/
[gnome-3.38]:       https://help.gnome.org/misc/release-notes/3.38/
[gnome-40]:         https://help.gnome.org/misc/release-notes/40.0/
[gnome-41]:         https://help.gnome.org/misc/release-notes/41.0/
[Weather]:          https://apps.gnome.org/app/org.gnome.Weather/
[Screenshot]:       https://en.wikipedia.org/wiki/GNOME_Screenshot
[Sound Recorder]:   https://wiki.gnome.org/Apps/SoundRecorder
[Software]:         https://apps.gnome.org/app/org.gnome.Software/

<div class="d-flex align-items-center my-3">
    <div class="flex-shrink-0">
        <img src="/files/2023/02/system-software-install.svg" style="max-width: 64px;">
    </div>
    <div class="flex-grow-1 ms-3">
        <h2 class="mb-0">Changes to the software selection</h2>
    </div>
</div>

Linux Kamarada 15.4 features most of the same apps as in 15.3, but updated with new features and bug fixes.

The **[Games]** app is not a [GNOME core app] anymore. It has not been packaged for openSUSE Leap 15.4 and therefore was also removed from Linux Kamarada 15.4. It is still possible to install Games from [Flathub], if desired.

The **[KolourPaint]** painting app was replaced by **[Drawing]**, which better integrates with the GNOME desktop, and **[Maps]** was included.

<div class="row">
    <div class="col-md">
        {% include image.html src='/files/2023/02/kamarada-15.4-drawing-en.jpg' caption='Drawing' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2023/02/kamarada-15.4-maps-en.jpg' caption='Maps' %}
    </div>
</div>

[Games]:            https://wiki.gnome.org/Apps/Games
[GNOME core app]:   https://apps.gnome.org/#core
[Flathub]:          https://flathub.org/apps/details/org.gnome.Games
[KolourPaint]:      https://apps.kde.org/kolourpaint/
[Drawing]:          https://apps.gnome.org/app/com.github.maoschanz.drawing/
[Maps]:             https://apps.gnome.org/app/org.gnome.Maps/

<div class="d-flex align-items-center my-3">
    <div class="flex-shrink-0">
        <img src="/files/2023/02/preferences-desktop-theme.svg" style="max-width: 64px;">
    </div>
    <div class="flex-grow-1 ms-3">
        <h2 class="mb-0">New theme</h2>
    </div>
</div>

The Linux Kamarada 15.3 default [GTK] theme, [Materia], has not been updated, so it was replaced by the [Orchis] theme, which is based on Materia and keeps alignment with [Google]'s [Material Design] (the design that empowers [Android]).

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        {% include image.html src='/files/2023/02/kamarada-15.4-orchis-theme-en.jpg' caption='Orchis theme' %}
    </div>
    <div class="col-md-2"></div>
</div>

[GTK]:              https://www.gtk.org/
[Materia]:          https://github.com/nana-4/materia-theme
[Orchis]:           https://github.com/vinceliuice/Orchis-theme
[Material Design]:  https://material.io/
[Google]:           https://www.google.com.br/
[Android]:          https://www.android.com/

<div class="d-flex align-items-center my-3">
    <div class="flex-shrink-0">
        <img src="/files/2021/12/multimedia-photo-viewer.svg" style="max-width: 64px;">
    </div>
    <div class="flex-grow-1 ms-3">
        <h2 class="mb-0">New backgrounds</h2>
    </div>
</div>

As you may be used to by now, this new release comes with new wallpapers, featuring photos of beautiful beaches in [Florianópolis]:

<div class="row">
    <div class="col-md">
        {% include image.html src='/files/2023/02/kamarada-15.4-praia-do-meio-en.jpg' caption='Praia do Meio (default)' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2023/02/kamarada-15.4-lagoa-da-conceicao-en.jpg' caption='Lagoa da Conceição' %}
    </div>
</div>

In this new GNOME version, the lock screen uses the same background as the desktop. Nevertheless, to keep with the tradition of previous Linux Kamarada releases, 15.4 has got two new backgrounds.

If you prefer, you can also use wallpapers from previous Linux Kamarada releases or the openSUSE Leap default (or set your own images as backgrounds, of course).

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        {% include image.html src='/files/2023/02/kamarada-15.4-backgrounds-en.jpg' caption='Backgrounds included on Linux Kamarada 15.4' %}
    </div>
    <div class="col-md-2"></div>
</div>

[Florianópolis]:    https://en.wikipedia.org/wiki/Florian%C3%B3polis

<div class="d-flex align-items-center my-3">
    <div class="flex-shrink-0">
        <img src="/files/2023/02/network-server.svg" style="max-width: 64px;">
    </div>
    <div class="flex-grow-1 ms-3">
        <h2 class="mb-0">Changes to the infrastructure</h2>
    </div>
</div>

Linux Kamarada ISO images are now hosted on [SourceForge]. The [Download] page has been updated with the new links.

And the official Linux Kamarada package repository has been moved to [packages.linuxkamarada.com](https://packages.linuxkamarada.com/). The 15.4 Final Live media is already set up with the new repository address. New installations made from this media will also access the repository at the new address. But if you already use Linux Kamarada, you need to adjust your system configuration.

For more information, read:

- [Changes to the Linux Kamarada infrastructure][changes-infrastructure]

[SourceForge]:              https://sourceforge.net/projects/kamarada/
[changes-infrastructure]:   {% post_url en/2023-02-15-changes-to-the-linux-kamarada-infrastructure %}

## Where can I get Linux Kamarada?

The [Download] page has been updated with the download link for the 15.4 Final release.

**Warning:** it is not recommended to use Linux Kamarada on servers, although it is possible. openSUSE Leap is better suited for that use case, because it offers a server configuration during installation, which is going to set up a server with just a small set of packages and a text mode interface (without a desktop).

## What should I do next?

When the download is complete, verify the integrity of the ISO image by calculating its checksum. Compare it with the checksum that appears on the [Download] page. They must match. If they don't, download the ISO image again.

If you are not in a hurry, it is recommended to also verify the authenticity of the ISO image.

The fingerprint of the Linux Kamarada Project public key is:

```
6b18 52e7 764f b302 b805 a4a0 a575 bcce 1737 8ecc
```

To import that key, run the following commands:

```
$ wget https://build.opensuse.org/projects/home:kamarada:15.4/public_key
$ gpg --import public_key
```

For more information on those verifications, read:

- [Verifying data integrity and authenticity using SHA-256 and GPG][how-to-verify-iso]

Once the ISO image has been downloaded and those verifications have succeeded, you have 3 options:

<div class="d-flex">
    <div class="flex-shrink-0">
        <img src="/files/2020/02/disk-burner.svg" style="max-width: 48px;">
    </div>
    <div class="flex-grow-1 ms-3">
        <h6>1) Burn the ISO image to a DVD (thus generating a LiveDVD)</h6>

        <p>Use an application such as <a href="https://cdburnerxp.se/">CDBurnerXP</a> (on Windows), <a href="http://www.k3b.org/">K3b</a> (on a Linux system with KDE desktop) or <a href="https://wiki.gnome.org/Apps/Brasero">Brasero</a> (Linux with GNOME) to burn the ISO image to a DVD. Insert it on your computer DVD drive and reboot to start Linux Kamarada.</p>
    </div>
</div>

<div class="d-flex">
    <div class="flex-shrink-0">
        <img src="/files/2020/02/usb-creator.svg" style="max-width: 48px;">
    </div>
    <div class="flex-grow-1 ms-3">
        <h6>2) Write the ISO image to a USB flash drive (thus generating a LiveUSB)</h6>

        <p>Use <a href="https://www.ventoy.net/">Ventoy</a> (available for Windows and Linux) to prepare the flash drive and copy the ISO image to it. Plug it to a USB port and reboot your computer to start Linux Kamarada.</p>

        <p>This how-to can help you to prepare the LiveUSB:</p>

        <ul><li><a href="{% post_url en/2020-07-29-ventoy-create-a-multiboot-usb-drive-by-simply-copying-iso-images-to-it %}">Ventoy: create a multiboot USB drive by simply copying ISO images to it</a></li></ul>
    </div>
</div>

<div class="d-flex">
    <div class="flex-shrink-0">
        <img src="/files/2020/02/virtualbox.svg" style="max-width: 48px;">
    </div>
    <div class="flex-grow-1 ms-3">
        <h6>3) Create a virtual machine and use the ISO image to boot it</h6>

        <p>This option allows you to test Linux Kamarada without having to reboot your computer and leave the operating system you are familiar with.</p>

        <p>For more information, read:</p>

        <ul><li><a href="{% post_url en/2019-10-10-virtualbox-the-easiest-way-to-try-linux-without-installing-it %}">VirtualBox: the easiest way to try Linux without installing it</a></li></ul>
    </div>
</div>

<div class="d-flex mb-3">
    <div class="flex-grow-1">
        After testing Linux Kamarada, if you want to install it on your computer or virtual machine, start the installer by clicking this icon, located on the desktop.
    </div>
    <div class="flex-shrink-0 ms-3">
        <img src="/files/2021/12/calamares.svg" style="max-width: 48px;">
    </div>
</div>

The installer will do the partitioning, copy the system to the computer and set everything up (language, keyboard layout, time zone, username and password, etc.). At the end, reboot the computer to start using the installed system.

[how-to-verify-iso]:    {% post_url en/2018-11-08-verifying-data-integrity-and-authenticity-using-sha-256-and-gpg %}

## What if I already use Linux Kamarada?

The ISO image is not intended for upgrading, only for testing and installing.

If you already use Linux Kamarada 15.3, you can upgrade to 15.4 by following this tutorial:

- [Linux Kamarada and openSUSE Leap: how to upgrade from 15.3 to 15.4][upgrade-to-15.4]

If you already use [Linux Kamarada 15.4 RC][15.4-rc], be aware of the repository address change. You need to adjust your system configuration. If you haven't already, see how to do it at:

- [Changes to the Linux Kamarada infrastructure][changes-infrastructure]

Then, just update your system as usual:

- [How to get updates for openSUSE Linux][howto-update]

[upgrade-to-15.4]:  {% post_url en/2023-01-16-linux-kamarada-and-opensuse-leap-how-to-upgrade-from-153-to-154 %}
[15.4-rc]:          {% post_url en/2022-12-19-linux-kamarada-15-4-enters-release-candidate-rc-phase %}
[howto-update]:     {% post_url en/2019-05-20-how-to-get-updates-for-opensuse-linux %}

## What if I already use openSUSE Leap?

You already use openSUSE Leap and want to turn it into Linux Kamarada? It's simple!

Just add the Linux Kamarada repository and install the **[patterns-kamarada-gnome]** package.

There are two different methods for doing that: from the graphical interface, using 1-Click Install, or from the terminal, using the **zypper** package manager — choose whichever method you prefer.

To install Linux Kamarada using 1-Click Install, click the following button:

<p class='text-center'>
    <a class='btn btn-primary' href='/downloads/patterns-kamarada-gnome-en.ymp'>
        <i class='fas fa-bolt'></i> 1-Click Install
    </a>
</p>

To install Linux Kamarada using the terminal, first add its repository:

```
# zypper addrepo -f -K -n "Linux Kamarada" -p 95 "https://packages.linuxkamarada.com/\$releasever/openSUSE_Leap_\$releasever/" kamarada
```

Then, install the **patterns-kamarada-gnome** package:

```
# zypper in patterns-kamarada-gnome
```

If you already use the GNOME desktop, probably you will need to download just a few packages. If you use another desktop, the download size will be larger.

When the installation is finished, if you create a new user, you will notice that they will receive the Linux Kamarada default settings (such as theme, wallpaper, etc.). Existing users can continue to use their customizations or adjust the system appearance (for instance, using the **Tweaks** and **Settings** apps).

[patterns-kamarada-gnome]:  https://software.opensuse.org/package/patterns-kamarada-gnome

## Where can I get help?

The [Help] page suggests some places where you can get help with Linux Kamarada and [openSUSE].

The support channel preferred by users has been the [@LinuxKamaradaWW](https://t.me/LinuxKamaradaWW) group on [Telegram], which is a messaging service that you can access from an app or a web browser.

[Help]:     /en/help
[openSUSE]: https://www.opensuse.org/
[Telegram]: {% post_url en/2021-05-10-the-telegram-messenger %}

## How long will it receive support?

According to the [openSUSE wiki][lifetime], openSUSE Leap 15.4 is expected to be maintained until the end of November 2023.

Since Linux Kamarada is based on openSUSE Leap, Linux Kamarada users are by transitivity openSUSE Leap users and receive the same updates.

Therefore, Linux Kamarada 15.4 will also be supported until the end of November 2023.

[lifetime]: https://en.opensuse.org/Lifetime

## Where can I get the source code?

Like any free software project, Linux Kamarada makes its source code available to anyone who wants to study it, adapt it, or contribute to the project.

Linux Kamarada development takes place at [GitLab] and [Open Build Service][obs]. There you can get the source codes of the packages developed specifically for this project (even [the source code of this website][kamarada-website] you read is available).

Source codes of packages inherited from openSUSE Leap can be retrieved directly from that distribution. If you need help doing this, get in touch, I can help.

[GitLab]:           https://gitlab.com/kamarada
[obs]:              https://build.opensuse.org/project/subprojects/home:kamarada
[kamarada-website]: https://gitlab.com/kamarada/kamarada.gitlab.io

## About Linux Kamarada

The Linux Kamarada Project aims to spread and promote Linux as a robust, secure, versatile and easy to use operating system, suitable for everyday use be at home, at work or on the server. It started as a blog about openSUSE, which is the Linux distribution I've been using for 10 years (since [April 2012][antoniomedeiros], when I installed openSUSE 11.4 and then upgraded to 12.1). Now the project offers its own Linux distribution, which brings much of the software presented on the blog pre-installed and ready for use.

[antoniomedeiros]:  https://antoniomedeiros.dev/blog/2012/04/21/problemas-envolvendo-bootloaders-mbr-e-tabela-de-particoes/

## Specs

To allow comparing Linux Kamarada releases, here is a summary of the software contained on Linux Kamarada 15.4 Final Build 5.6:

- Linux kernel 5.14.21
- X.Org display server 1.20.3 (without Wayland)
- GNOME desktop 41.8 including its core apps, such as Files (previously Nautilus), Calculator, Terminal, Text editor (gedit), Screenshot and others
- LibreOffice office suite 7.4.3.2
- Mozilla Firefox 102.8.0 ESR (default web browser)
- Chromium 110 web browser (alternative web browser)
- VLC media player 3.0.18
- Evolution email client 3.42.4
- YaST control center
- Brasero 3.12.3
- CUPS 2.2.7
- Drawing 1.0.1
- Firewalld 0.9.3
- GParted 1.3.1
- HPLIP 3.21.10
- Java (OpenJDK) 11.0.17
- KeePassXC 2.7.4
- Linphone 4.3.2
- PDFsam Basic 4.3.4
- Pidgin 2.14.8
- Python 2.7.18 and 3.6.15
- Samba 4.15.13
- Tor 0.4.7
- Transmission 3.00
- Vim 9.0
- Wine 7.0
- Calamares installer 3.2.36
- Flatpak 1.12.5
- games: Aisleriot (Solitaire), Chess, Hearts, Iagno (Reversi), Mahjongg, Mines (Minesweeper), Nibbles (Snake), Quadrapassel (Tetris), Sudoku

That list is not exhaustive, but it gives a notion of what can be found in the distribution.
