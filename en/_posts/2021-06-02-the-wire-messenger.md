---
date: '2021-06-02 21:20:00 GMT-3'
image: '/files/2021/05/im-wire-pt.jpg'
layout: post
nickname: 'wire'
title: 'The Wire messenger'
---

{% include image.html src='/files/2021/05/im-wire-pt.jpg' %}

[Wire] is a secure, privacy-friendly fully end-to-end encrypted [messaging app]. Wire is different from messengers previously shown here because it is actually a corporate collaboration suite. As messengers commonly do, Wire allows you to send text, audio and video messages, photos, videos and files and also make voice and video calls. But Wire also has features for the enterprise, such as [screen sharing], [guest rooms], member role management, audio calls (with up to 25 participants), video conferencing (with up to 12 participants) and compliance with regulations (GDPR and CCPA) and certifications (ISO and SOX).

Like [Element], Wire is the name both of the app and the company behind it.

Wire sells plans for businesses: [Pro], [Enterprise] and [Red]. And there is also [Wire Personal][personal] for individuals, which is more simple, but completely free to use. It is curious that you will hardly find a link to Wire Personal on the [Wire website][wire], but if you search for it (e.g. on [DuckDuckGo] or [Google]) or [click here][personal] you will see it is there. And you can simply [download Wire][download]. All of these solutions are actually based on the same app, differing in available features and price.

Wire Personal shows no ads and makes no user profiling: it does not sell analytics or usage data to advertising companies or anyone else. And it does not need to, thanks to the paying customers of its corporate versions.

Like [WhatsApp], [Telegram] and [Signal], and different from other messengers previously shown here, Wire relies on centralized servers (although it allows for [self-hosting] and offers an [extensive documentation][self-hosting] on that). The practical benefit of this is that it allows Wire to work across multiple devices, like Telegram does. But different from Telegram, Wire is fully end-to-end encrypted: no messages or files are stored unencrypted on servers. Wire allows one account to be synced on up to 8 devices and messages are encrypted for each of them.

You need to provide either an email address or a phone number to create an account. [Wire does log some user metadata][metadata].

Like other messengers, Wire allows you to send [ephemeral messages and files][ephemeral]. Also, the mobile app offers an app lock
mechanism. If you enable it, you need to unlock the app with the same password, pattern or fingerprint that you normally use to unlock your device.

Wire has a [backup] feature that makes it easy for you to back up your conversation history.

Talking about the Wire company, [it was founded by some of the same people that founded Skype][skype] and today has over 1300 enterprise customers. About its location, information on their website is somehow confusing: [one page][location-1] says it is headquartered in Switzerland, while [other page][location-2] says it is headquartered in Germany with some offices around the world. Maybe they could explain us that and update their website as well.
Their [terms of use] says that the company follows the laws of the United States and the state of California (for users in the US) and the laws of Switzerland (for users outside the US).

Wire is completely open source: the source code for all of the clients and the server is available on [GitHub], allowing for independent auditing.

Wire uses the Proteus protocol to provide end-to-end encryption for messages. Proteus is an early fork from the code that went on to become the Signal Protocol. The [Proteus protocol][audit-1] and [all the Wire clients][audit-2] have been publicly audited by Kudelski Security and X41 D-Sec GmbH.

There is another in-development protocol for encrypting group messages. It is called [Messaging Layer Security][mls] (MLS) and is being developed under an IETF working group initiated by Wire in 2016, alongside established tech giants Mozilla and Cisco. Members of the working group now include the University of Oxford, Facebook, INRIA, Google and Twitter.

Last but not least, Wire features a beautiful, distinct user interface.

Wire was once recommended by the privacy expert [Edward Snowden]:

<blockquote class="twitter-tweet" data-conversation="none"><p lang="en" dir="ltr">I would not (and do not) use email, except as throwaways for registration. Email is a fundamentally insecure protocol that, in 2019, can and should be abandoned for the purposes of any meaningful communication. Email is unsafe. I&#39;d use <a href="https://twitter.com/signalapp?ref_src=twsrc%5Etfw">@Signalapp</a> or <a href="https://twitter.com/wire?ref_src=twsrc%5Etfw">@Wire</a> as a safer alternative.</p>&mdash; Edward Snowden (@Snowden) <a href="https://twitter.com/Snowden/status/1175437588129308672?ref_src=twsrc%5Etfw">September 21, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

But then a few months later, he showed concern about the acquisition of the Swiss-born Wire company by an American holding:

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">If you&#39;re a tech journalist, you should be digging into the story behind what&#39;s going on behind the curtain here. This is not appropriate for a company claiming to provide a secure messenger -- claims a large number of human rights defenders relied on -- and we need facts. <a href="https://t.co/iV4tRZwgDR">https://t.co/iV4tRZwgDR</a></p>&mdash; Edward Snowden (@Snowden) <a href="https://twitter.com/Snowden/status/1194396764293550080?ref_src=twsrc%5Etfw">November 12, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">“Wire was always for profit and planned to follow the typical venture backed route.&quot; [<a href="https://twitter.com/wire?ref_src=twsrc%5Etfw">@Wire</a> CEO] Brogger... describes individual consumers as “not part of our strategy.&quot;<br><br>This is a grim turn for a once-promising app, and a window for <a href="https://twitter.com/signalapp?ref_src=twsrc%5Etfw">@Signalapp</a> to exploit. <a href="https://t.co/HWuqwc9GIp">https://t.co/HWuqwc9GIp</a></p>&mdash; Edward Snowden (@Snowden) <a href="https://twitter.com/Snowden/status/1194805615023050752?ref_src=twsrc%5Etfw">November 14, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Anyway, if you are looking for an app to replace WhatsApp, Wire Personal could be it. Or, if you are running a company that is looking for compliance with regulations and certifications, such as GDPR, CCPA, ISO and SOX, you could hire Wire's services.

You can get the Wire app for Android from [Google Play] or from the [Wire website][download] (as an APK).

The Wire app for Linux in [AppImage] format can be downloaded from the [Wire website][download], which also has packages for Ubuntu and Debian. I found it easier to install Wire from [Flathub]:

```
# flatpak install com.wire.WireDesktop
```

Wire is also available for other systems. For more options, take a look at: [wire.com][download].

The Wire website has a comprehensive [support] section with many searchable articles. Wire also has [extensive documentation on the server side][self-hosting] for sysadmins looking for self-hosting Wire on their own datacenters or cloud, or people just curious to understand how it works.

This text is part of the series:

- [Open-source privacy-focused messaging apps alternatives to WhatsApp][messaging app]

[wire]:             https://wire.com/en/
[messaging app]:    {% post_url en/2021-05-10-open-source-privacy-focused-messaging-apps-alternatives-to-whatsapp %}
[screen sharing]:   https://wire.com/en/blog/encrypted-screen-sharing/
[guest rooms]:      https://wire.com/en/features/encrypted-guest-rooms/
[element]:          {% post_url en/2021-05-20-the-element-messenger-and-the-matrix-standard %}
[pro]:              https://wire.com/en/products/pro-secure-team-collaboration/
[enterprise]:       https://wire.com/en/products/technology/
[red]:              https://wire.com/en/products/red-crisis-communication-software/
[personal]:         https://wire.com/en/products/personal-secure-messenger/
[duckduckgo]:       https://duckduckgo.com/?q=wire+personal
[google]:           https://www.google.com/search?q=wire+personal
[download]:         https://wire.com/en/download/
[whatsapp]:         https://www.whatsapp.com/
[telegram]:         {% post_url en/2021-05-10-the-telegram-messenger %}
[signal]:           {% post_url en/2021-05-11-the-signal-messenger %}
[self-hosting]:     https://docs.wire.com/
[metadata]:         https://wire-docs.wire.com/download/Wire+Privacy+Whitepaper.pdf
[ephemeral]:        https://wire.com/en/blog/ephemeral-messaging-workplace/
[backup]:           https://support.wire.com/hc/en-us/articles/360000824805-Back-up-your-conversation-history
[skype]:            https://www.theguardian.com/technology/2014/dec/03/wire-communications-skype-janus-friis-app
[location-1]:       https://wire.com/en/security/
[location-2]:       https://wire.com/en/about/
[terms of use]:     https://wire.com/en/legal/#terms
[github]:           https://github.com/wireapp
[audit-1]:          https://wireapp.medium.com/wires-independent-security-review-61f37a1762a8
[audit-2]:          https://wireapp.medium.com/wire-application-level-security-audits-98324d1f211b
[mls]:              https://messaginglayersecurity.rocks/
[edward snowden]:   https://en.wikipedia.org/wiki/Edward_Snowden
[google play]:      https://play.google.com/store/apps/details?id=com.wire
[appimage]:         https://appimage.org/
[flathub]:          https://flathub.org/apps/details/com.wire.WireDesktop
[support]:          https://support.wire.com/hc/en-us
