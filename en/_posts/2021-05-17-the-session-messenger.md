---
date: '2021-05-17 19:10:00 GMT-3'
image: '/files/2021/05/im-session-pt.jpg'
layout: post
nickname: 'session'
title: 'The Session messenger'
---

{% include image.html src='/files/2021/05/im-session-pt.jpg' %}

[Session] is a [messaging app] forked from [Signal] that takes privacy a step further. Different from other more common instant messengers, Session does not require any personal information, such as a phone number or email address, to register and start using its service. While Signal collects almost no user metadata, Session does not collect any user metadata at all. Also, every message between Session users is end-to-end encrypted — a feature inherited from Signal. That means no data breaches, as there's nothing to leak.

Just for you to have an idea of how it is serious, Session's [privacy policy] starts like this:

> Session never knows who you are, who you’re talking to, or the contents of your messages.
>
> The Session app does not collect or share your information. That's our policy in a nutshell.

When you use Session for the first time, just choose to create an account and it will generate a random (unique) **Session ID** for you. It's a large hexadecimal number that acts as an address people can use to contact you on Session. No need to pass your phone number on to others. On [WhatsApp], Signal or [Telegram], you add contacts via their phone number. On Session, you add contacts via their Session ID.

One challenge with truly anonymous communication systems like Session is that you **do** need to verify the identity of the person you're talking to. Make sure you and your contact exchange Session IDs personally or using a secondary channel of communication both of you trust. To make things easier, Session provides a QR Code you can show each other to scan. You can also copy and paste the Session ID of each other.

Session is a project of the [Oxen Privacy Tech Foundation][optf] (OPTF, previously named Loki Foundation), an Australia-based not-for-profit organisation that supports the development of [free and open-source software][foss] that protects privacy and security in the digital world. Besides Session for private messaging, this foundation is also responsible for [Lokinet], a [Tor]-like decentralized onion routing network for private browsing, and [Oxen], the cryptocurrency that economically incentivises the **[service nodes]** that make up the Lokinet. At least in theory, anyone can [run a service node] and earn Oxen for that.

Different from other more common messengers, such as WhatsApp, Signal or Telegram, which have centralised servers, Session servers are decentralized: they are the Lokinet's service nodes. Session uses the Lokinet onion routing network to send messages securely and anonymously. An **onion routing network** is a network of nodes over which users can send anonymous encrypted messages. Onion networks encrypt messages with multiple layers of encryption, then send them through a number of nodes. Each node "unwraps" (decrypts) a layer of encryption. This way, no single node ever knows both the destination and origin of the message. That also ensures that a server which receives a message never knows the IP address of the sender. Please note that a disadvantage of the onion routing is that it may add a small delay to sending/receiving messages.

{% include image.html src='/files/2021/05/onion-routing.jpg' %}

In case you want to stop using your current Session ID (e.g. in the case where there are people sending you unwanted messages), just erase the app's data and generate a new Session ID. Remember to tell your contacts that your Session ID has changed. You can generate as many Session IDs as you want this way.

But if you want to keep your Session ID, as Session doesn't have a central server storing information about your identity, you are responsible for backing up your **recovery phrase** yourself. It is a mnemonic seed which can be used to restore your existing Session ID to a new device. You can write your recovery phrase on a piece of paper, then store it in a safe location that only you have access to.

Unfortunately, the recovery phrase is only for restoring the Session ID. Contacts and messages are stored only locally, so they cannot be retrieved once lost. And Session does not provide a way to backup contacts and messages yet (not even locally, as Signal does).

Also, Session does not offer voice nor video calls at the moment.

But all of that (backup, voice and video calls) are planned features for the future.

During my tests, I found that the same Session account cannot be used on multiple devices, as it is possible with Telegram and Signal. You can restore a Session ID on the computer, but then the mobile app stops working (and vice versa).

As Signal, Session allows you to send text and voice messages, photos, videos and files, in one-to-one or group chats. Session groups are always closed (private), fully end-to-end encrypted and support up to 100 members. Session supports open (public) groups, but they are handled very differently (you can find more information about them on the Session [FAQ]). Session also inherits the same [disappearing messages] and [lock system] from Signal.

Last but not least, Session offers a really beautiful dark mode for its users.

Session is completely open source: the source code for all of the clients and the server is available on [GitHub], allowing for independent auditing.

Session's desktop, [Android], and [iOS] clients have undergone a [security audit by Quarkslab][quarkslab].

I wouldn't recommend Session for users in general (especially the elderly), as it lacks some of the features present on other more common instant messengers. I believe that Telegram or Signal are already good enough for people looking for a simple alternative to WhatsApp. But if you are a tech-savvy user looking for absolute privacy and freedom from any form of surveillance, you should certainly give Session a try.

You can get the Session app for Android from [Google Play] or from [GitHub][session-android] (as an APK).

The Session app for Linux in [AppImage] format can be downloaded from the [Session website][session-appimage]. But I personally found it easier to install Session from [Flathub]:

```
# flatpak install network.loki.Session
```

Session is also available for other systems. For more options, take a look at: [getsession.org](https://getsession.org/download/).

Session has a very explanatory [FAQ] (frequently asked questions) page in its website.

## Comparison with Bitcoin

Just out of curiosity, I see some similarities in the way Session and [Bitcoin] were designed. Both are **decentralized** systems based on **cryptography**: Bitcoin is a decentralized currency, while Session is a decentralized messaging service. If you already know Bitcoin, I think you are going to understand some of the Session concepts more easily.

Remember from my [Bitcoin how-to][bitcoin] that a wallet has an **address** which is basically a large hexadecimal number. Just looking at a wallet, it's impossible to say to whom it belongs (unless the owner had left rich descriptions for each transaction and/or an investigation is performed). The same happens to a Session ID, which is also a large hexadecimal number: just looking at it, it's impossible to say to whom it belongs. Unlike bank account numbers and cellphone numbers, there is no central authority where one could ask who is behind a Bitcoin address or a Session ID. Also note that just like you write down a **seed** to be able to restore a Bitcoin wallet, you write down a recovery phrase to be able to restore a Session ID.

Bitcoin is **peer-to-peer** and does not rely on a central server. Even though Session isn't peer-to-peer, it does not rely on a central server as well, taking [a decentralized approach to route messages][session-blog-1]. This makes both Bitcoin and Session censorship resistant: with no central point of failure, it's harder to shut them down.

Also interesting is the use of a **cryptocurrency** — namely Oxen — to financially reward the servers that route Session messages. Session itself does not use a **blockchain** to store messages. But as well as the Bitcoin network has its own blockchain to register Bitcoin transactions, the Oxen network has its own blockchain to register Oxen transactions.

While Bitcoin is based on [**proof of work**][session-blog-2], Oxen relies on [**proof of stake**][session-blog-2]. I won't explain that difference here (for that, I recommend you to read [this][session-blog-2]), but that takes us to the next point.

Oxen has another key role on Session, which is [protecting the network from a Sybil attack][session-blog-3]. Attacking the Lokinet isn't that simple. To run a service node, someone has to "stake" (hold, temporarily lock up) a certain amount of Oxen (at the time of writing, [15,000 Oxen][oxen dashboard], equivalent to 19,000 dollars or 0,43 BTC). Only nodes that have met the staking requirement are allowed to participate in the network. If a node hasn't met that requirement, it cannot participate in building the blockchain, routing Session messages, or providing any of the other Lokinet services. That discourages misbehavior and makes it extremely expensive (not to say impossible) for an attacker to take control of the network by running various nodes.

Most of the time, getting enough Oxen to run a node will require purchasing from the [market].

Session is quite interesting not only because it ensures privacy to the conversations, but also because it proposes an economically viable way of supporting and protecting a decentralized network of servers that route the messages.

This text is part of the series:

- [Open-source privacy-focused messaging apps alternatives to WhatsApp][messaging app]

[session]:                  https://getsession.org/
[messaging app]:            {% post_url en/2021-05-10-open-source-privacy-focused-messaging-apps-alternatives-to-whatsapp %}
[signal]:                   {% post_url en/2021-05-11-the-signal-messenger %}
[privacy policy]:           https://getsession.org/privacy-policy/
[whatsapp]:                 https://www.whatsapp.com/
[telegram]:                 {% post_url en/2021-05-10-the-telegram-messenger %}
[optf]:                     https://optf.ngo/
[foss]:                     https://en.wikipedia.org/wiki/Free_and_open-source_software
[lokinet]:                  https://lokinet.org/
[tor]:                      https://www.torproject.org/
[oxen]:                     https://oxen.io/
[service nodes]:            https://docs.oxen.io/about-the-oxen-blockchain/oxen-service-nodes
[run a service node]:       https://docs.oxen.io/using-the-oxen-blockchain/oxen-service-node-guides/setting-up-an-oxen-service-node
[faq]:                      https://getsession.org/faq/
[disappearing messages]:    https://support.signal.org/hc/en-us/articles/360007320771-Set-and-manage-disappearing-messages
[lock system]:              https://support.signal.org/hc/en-us/articles/360007059572-Screen-Lock
[github]:                   https://github.com/oxen-io
[android]:                  https://www.android.com/
[ios]:                      https://www.apple.com/ios/
[quarkslab]:                https://blog.quarkslab.com/resources/2021-05-04_audit-of-session-secure-messaging-application/20-08-Oxen-REP-v1.4.pdf
[google play]:              https://play.google.com/store/apps/details?id=network.loki.messenger
[session-android]:          https://github.com/loki-project/session-android/releases
[appimage]:                 https://appimage.org/
[session-appimage]:         https://getsession.org/download/
[flathub]:                  https://flathub.org/apps/details/network.loki.Session

[bitcoin]:                  {% post_url en/2021-04-23-bitcoin-for-beginners-with-the-electrum-wallet-part-1-basic-concepts-and-setup %}
[session-blog-1]:           https://getsession.org/centralisation-vs-decentralisation-in-private-messaging/
[session-blog-2]:           https://loki.network/2020/01/24/blockchain-consensus-mechanisms/
[session-blog-3]:           https://getsession.org/how-session-protects-your-anonymity-with-blockchain-and-crypto/
[oxen dashboard]:           https://lokidashboard.com/
[market]:                   https://coinmarketcap.com/currencies/oxen/markets/
