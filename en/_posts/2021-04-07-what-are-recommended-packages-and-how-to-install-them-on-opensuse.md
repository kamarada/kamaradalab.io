---
date: '2021-04-07 00:00:00 UTC-3'
image: /files/2021/01/recommended.jpg
layout: post
nickname: 'recommended-packages'
title: 'What are recommended packages and how to install them on openSUSE'
excerpt: 'On Linux, packages can be understood as &quot;pieces&quot; of the software that make up the system. When you want to add a feature to your system, you usually look for the package that provides that feature and installs it. Packages can recommend the installation of other packages. In this text, you are going to see some tips on how to deal with recommended packages.'
---

{% include image.html src='/files/2021/01/recommended.jpg' %}

On [Linux], **packages** can be understood as "pieces" of the software that make up the system. When you want to add a feature to your system, you usually look for the package that provides that feature and installs it. Here are some examples of things provided by packages:

- **Applications:** the [Mozilla Firefox][firefox] web browser is in the [**MozillaFirefox**][softwareoo-1] package;
- **Translations:** the [**libreoffice-l10n-pt_BR**][softwareoo-2] package provides the [LibreOffice] translation for Brazilian Portuguese;
- **Documentation:** the [**man-pages**][softwareoo-3] package makes information about various commands available;
- **Customizations:** the [**MozillaFirefox-branding-openSUSE**][softwareoo-4] package provides [openSUSE] look and feel for Firefox;
- **Drivers:** the [**hplip**][softwareoo-5] package adds support for [HP printers][howto-hp-printer];
- **Plugins:** the [**evince-plugin-xpsdocument**][softwareoo-6] package makes the [GNOME Document Viewer (Evince)][evince] able to open XPS documents;
- **Libraries:** the [**libavcodec58**][softwareoo-7] package contains _codecs_ for multiple multimedia formats;
- **Fonts:** the [**google-roboto-fonts**][softwareoo-8] package installs the [Roboto] font family; and
- **Icons:** the [**papirus-icon-theme**][softwareoo-9] package provides the [Papirus] icon theme.

Probably there are more examples of package "categories", but I believe you already got the idea. All the examples listed above are packages from the [openSUSE distributions][leap-tumbleweed] and are already installed by default on [Linux Kamarada][kamarada-15.2].

## Required packages

There are cases where a package X brings a feature that only works if a package Y is installed too. In this case, we say that package X **depends on** (or **requires**) package Y or that package Y is a **dependency** (or **requirement**) for package X. In such cases, if you ask the system to install package X, package Y will also be installed. Or, if for some reason it is not possible to install package Y, package X will not be installed as well.

For example, the [**inkscape**][softwareoo-10] package, which is not installed by default on Linux Kamarada, depends on the [**ImageMagick**][softwareoo-11] package. If you ask to install the **inkscape** package, the **ImageMagick** package will be installed automatically, because it is a dependency for the **inkscape** package.

## Recommended packages

There are cases where a package X brings a feature that may work better if package Y is installed too. Package Y is not required for package X to work, but it adds or improves some functionality of package X. In this case, we say that package X **recommends** package Y or that package Y is **recommended** by package X.

Translation packages in general are recommended by the corresponding application packages. For example, the **inkscape** package recommends the [**inkscape-lang**][softwareoo-12] package. [Inkscape] works if translations are not installed, you can use it with the interface in English, not translated. But if you don't speak English, I imagine that it is more comfortable to use it translated to your language.

When installing a package, its recommended packages can be installed together or not.

Both openSUSE and Linux Kamarada are by default setup to install both required and recommended packages. This is probably the best setting for most users, as it improves the user experience.

But that is not always the case. Here are some scenarios where installing recommended packages may not be the best option:

- if you are using Linux Kamarada on a USB stick, you may prefer to install only the required packages to save space
- if you are using a [LiveDVD][what-is-a-livecd-dvd-usb], changes you make to the system are stored in RAM, in this scenario the installation of recommended packages may cause the system run out of memory and crash
- if you are using [openSUSE on a server][how-to-server], your priorities are disk space, performance and security, and installing only required packages gives you more control over what is actually installed and running on your server

Thinking on that, I have prepared some tips on how to deal with recommended packages.

## Changing the default setting

On openSUSE and Linux Kamarada, packages can be installed using the **[zypper]** package manager. For example, you could install Inkscape with the following command:

```
# zypper in inkscape
```

That `in` is the abbreviation for `install`. Alternatively, you could use:

```
# zypper install inkscape
```

Another option is to use the [YaST Control Center][yast]'s **Software Management** module:

{% include image.html src='/files/2021/04/recommended-01-en.jpg' %}

Note that when you mark the **inkscape** package for installation, several other packages, both required and recommended, are marked for installation as well.

Both **zypper** and YaST use the [libzypp] library behind the scenes. Its configuration file is located in `/etc/zypp/zypp.conf`. If you have not yet changed that file and you open it (as root), you will find something like this:

```ini
##
## Whether only required packages are installed.
##
## Recommended packages, will not be regarded.
##
## Valid values: boolean
## Default value: false
##
# solver.onlyRequires = false
```

You can use the `solver.onlyRequires` option to define whether only required packages should be installed. By default, it is commented out (disabled) and its value is `false`, that is, both required and recommended packages should be installed.

If you want to change this behavior, uncomment this line and set it to `true`, meaning that only required packages should be installed:

```ini
##
## Whether only required packages are installed.
##
## Recommended packages, will not be regarded.
##
## Valid values: boolean
## Default value: false
##
solver.onlyRequires = true
```

If you prefer to use YaST, you can enable or disable the installation of recommended packages by opening the **Dependencies** menu and checking or unchecking the **Install Recommended Packages** option:

{% include image.html src='/files/2021/04/recommended-02-en.jpg' %}

This setting applies to YaST only, it does not change the libzypp configuration.

## Changing the behavior on demand

If you don't want to modify **zypper**'s default behavior, you can use the `--recommends` or `--no-recommends` options to change the behavior for the current installation only.

For example, to install Inkscape along with both the required and recommended packages:

```
# zypper in inkscape --recommends
```

To install Inkscape along with the required but not the recommended packages:

```
# zypper in inkscape --no-recommends
```

Using these options, **zypper** ignores the `solver.onlyRequires` setting in `/etc/zypp/zypp.conf` and adopts the behavior requested by you on the fly.

These options can also be used with other **zypper** commands, such as the one that checks for package updates:

```
# zypper up --no-recommends
```

Regarding recommended packages, YaST always remembers the last setting used. So, you can change the behavior for the current installation using the same option described earlier, but if you don't want to make it the new default, you need to change it again later.

## Installing recommended packages by already installed packages

If at some point you have disabled the installation of recommended packages, installed some packages, then changed your mind and now want to install the recommended packages that have not been installed, you can use the command:

```
# zypper inr
```

Or, if you prefer:

```
# zypper install-new-recommends
```

This command can be useful for installing translations, if you have just changed the system language, or for installing drivers, if you have just added a hardware component.

On YaST, similar result can be achieved by opening the **Extras** menu and then clicking **Install All Matching Recommended Packages**:

{% include image.html src='/files/2021/04/recommended-03-en.jpg' %}

## Which packages are recommended by a package?

To look which packages are recommended by the **inkscape** package, for instance, using **zypper**, run:

```
# zypper info --recommends inkscape
```

It brings information about the **inkscape** package, showing the recommended packages at the end:

```
Information for package inkscape:
---------------------------------
Repository     : Main Update Repository
Name           : inkscape
Version        : 0.92.2-lp152.8.3.1
Arch           : x86_64
Vendor         : openSUSE
Installed Size : 94.0 MiB
Installed      : No
Status         : not installed
Source package : inkscape-0.92.2-lp152.8.3.1.src
Summary        : Vector Illustration Program
Description    :
    Inkscape is a vector illustration program for the GNOME desktop.
Recommends     : [2]
    inkscape-lang
    python-lxml
```

Using YaST, select the **inkscape** package and take a look at the **Dependencies** tab:

{% include image.html src='/files/2021/04/recommended-04-en.jpg' %}

[linux]:                    https://www.kernel.org/linux.html
[firefox]:                  https://www.mozilla.org/firefox/
[softwareoo-1]:             https://software.opensuse.org/package/MozillaFirefox
[softwareoo-2]:             https://software.opensuse.org/package/libreoffice-l10n-pt_BR
[libreoffice]:              https://www.libreoffice.org/
[softwareoo-3]:             https://software.opensuse.org/package/man-pages
[softwareoo-4]:             https://software.opensuse.org/package/MozillaFirefox-branding-openSUSE
[opensuse]:                 https://www.opensuse.org/
[softwareoo-5]:             https://software.opensuse.org/package/hplip
[howto-hp-printer]:         {% post_url en/2018-10-26-hp-printers-on-opensuse %}
[softwareoo-6]:             https://software.opensuse.org/package/evince-plugin-xpsdocument
[evince]:                   https://wiki.gnome.org/Apps/Evince
[softwareoo-7]:             https://software.opensuse.org/package/libavcodec58
[softwareoo-8]:             https://software.opensuse.org/package/google-roboto-fonts
[roboto]:                   https://fonts.google.com/specimen/Roboto
[softwareoo-9]:             https://software.opensuse.org/package/papirus-icon-theme
[papirus]:                  https://github.com/PapirusDevelopmentTeam/papirus-icon-theme/
[leap-tumbleweed]:          {% post_url en/2020-12-07-opensuse-leap-vs-opensuse-tumbleweed-what-is-the-difference %}
[kamarada-15.2]:            {% post_url en/2020-09-11-linux-kamarada-15.2-come-to-the-elegant-modern-green-side-of-the-force %}
[softwareoo-10]:            https://software.opensuse.org/package/inkscape
[softwareoo-11]:            https://software.opensuse.org/package/ImageMagick
[softwareoo-12]:            https://software.opensuse.org/package/inkscape-lang
[what-is-a-livecd-dvd-usb]: {% post_url en/2015-11-25-what-is-a-livecd-dvd-usb %}
[how-to-server]:            {% post_url en/2017-01-25-how-to-install-opensuse-leap-linux-on-a-server %}
[zypper]:                   https://en.opensuse.org/Portal:Zypper
[inkscape]:                 https://inkscape.org/
[yast]:                     http://yast.opensuse.org/
[libzypp]:                  https://en.opensuse.org/Portal:Libzypp
