---
date: '2023-08-21 19:40:00 GMT-3'
image: '/files/2023/08/rip-cd-en.jpg'
layout: post
nickname: 'sound-juicer'
title: 'How to rip CDs on Linux with Sound Juicer'
excerpt: 'What to do if you are holding a CD, this album is not available on any streaming service, and you want to listen to it in your car, whose multimedia player does not have a CD drive? In this case, extracting the songs from the CD to MP3 files – a process better known as &quot;ripping&quot; the CD – may be an option. On Linux, you can rip a CD in a few ways, and one of them is using the Sound Juicer app. In this how-to, you&apos;ll see how to install and use Sound Juicer on Linux Kamarada 15.4.'
---

{% include image.html src='/files/2023/08/rip-cd-en.jpg' %}

A stereo CD player is something rare nowadays. It's interesting to think how media evolved: in my childhood, I had [cassette tapes], but at the time there were already [CDs]. Afterwards, [MP3] music files became common, first stored on CDs, then on [USB sticks]. Until, finally, [Bluetooth]-enabled [smartphones] came along. Today, the scenario that seems more common to me is: the stereo only serves to produce the audio that we hear, while the songs themselves are stored on a smartphone, as MP3 files or streamed from a service such as [Spotify].

Streaming services made it very easy to search for and listen to music. It's not hard to find the music I'm looking for on Spotify or [YouTube].

But what to do if you are holding a CD, this album is not available on any streaming service, and you want to listen to it in your car, whose multimedia player does not have a CD drive?

In this case, extracting the songs from the CD to MP3 files -- a process better known as "ripping" the CD -- may be an option. You can then store these MP3 files on a USB stick or smartphone and listen to them with your car stereo.

On [Linux], you can rip a CD in a few ways, and one of them is using the **[Sound Juicer]** app. In this how-to, you'll see how to install and use Sound Juicer on [Linux Kamarada 15.4], which is the Linux distribution we are going to use as reference.

<div class='alert alert-danger' role='alert'>
    <i class='fa-solid fa-triangle-exclamation' aria-hidden='true'></i> <strong>Caution:</strong> depending on the use you make of these MP3 files, you may incur a copyright infringement. The purpose of this article is to present this technology and its possibilities, but the actual use you make of them is your responsibility. This article is technical, for informational purposes only and does not contain legal advice. If you need legal counsel, contact a lawyer.
</div>

## Installing Sound Juicer

You can install Sound Juicer from the [openSUSE official repositories][repos] using one of two ways: from the graphical interface using 1-Click Install or from the terminal using the **zypper** package manager -- choose whichever method you prefer.

To install Sound Juicer using 1-Click Install, click the following button:

<p class='text-center'>
    <a class='btn btn-primary' href='/downloads/sound-juicer.ymp'>
        <i class='fas fa-bolt'></i> 1-Click Install
    </a>
</p>

To install Sound Juicer using the terminal, run the following command:

```
# zypper in sound-juicer
```

If you use [FlatPak], another option is to install Sound Juicer from [Flathub]:

```
# flatpak install org.gnome.SoundJuicer
```

Right after installing, you should be able to launch Sound Juicer.

## Starting Sound Juicer

To start Sound Juicer, if you use the [GNOME] desktop, click **Activities**, by the upper-left corner of the screen, start typing `sound juicer` and then click its icon:

{% include image.html src='/files/2023/08/sound-juicer-01-en.jpg' %}

## Setting up Sound Juicer on first use

Sound Juicer saves the extracted songs as Ogg Vorbis files by default. [Ogg Vorbis] is an open compressed audio format. But the MP3 format is more common. Let's configure Sound Juicer to save songs as MP3 files.

Fortunately, we need to do this configuration only when using Sound Juicer for the first time. It remembers our settings in following uses.

Open the Sound Juicer **menu** (by clicking the three-dot icon by the upper-right corner of the window) and click **Preferences**:

{% include image.html src='/files/2023/08/sound-juicer-02-en.jpg' %}

Select **MPEG Layer 3 Audio** as **Output Format**:

{% include image.html src='/files/2023/08/sound-juicer-03-en.jpg' %}

Close the **Preferences** dialog.

## Ripping a CD with Sound Juicer

Insert the audio CD you want to rip into your computer's CD drive if you haven't already.

Sound Juicer reads the disc and tries to get information about it, either from the disc itself or from the [MusicBrainz] online music database:

{% include image.html src='/files/2023/08/sound-juicer-04-en.jpg' %}

Note that **Title**, **Artist**, **Genre**, **Year** and **Disc** are text fields and you can enter or change these data:

{% include image.html src='/files/2023/08/sound-juicer-05-en.jpg' %}

In this example, I set **Genre** to `Jazz` and **Year** to `2020`.

By double-clicking the track names, you can also rename them:

{% include image.html src='/files/2023/08/sound-juicer-06-en.jpg' %}

When you are done reviewing the CD data, make sure all the tracks are selected and click **Extract**:

{% include image.html src='/files/2023/08/sound-juicer-07-en.jpg' %}

A progress bar at the end shows the ripping progress:

{% include image.html src='/files/2023/08/sound-juicer-08-en.jpg' %}

When everything is finished, the MP3 files are going to be in the **Music** folder:

{% include image.html src='/files/2023/08/sound-juicer-09-en.jpg' %}

I won't share the MP3 files I created while writing this tutorial anywhere. Even because there is no need: if you are curious to hear this album, know that it's from the Brazilian band [GrOOfbOOgalOO] and is available on [Spotify][cd-spotify], [YouTube][cd-youtube] and other streaming services.

[cassette tapes]:       https://en.wikipedia.org/wiki/Cassette_tape
[CDs]:                  https://en.wikipedia.org/wiki/Compact_disc
[MP3]:                  https://en.wikipedia.org/wiki/MP3
[USB sticks]:           https://en.wikipedia.org/wiki/USB_flash_drive
[Bluetooth]:            https://en.wikipedia.org/wiki/Bluetooth
[smartphones]:          https://en.wikipedia.org/wiki/Smartphone
[Spotify]:              https://spotify.com/
[YouTube]:              https://music.youtube.com
[Linux]:                https://www.kernel.org/linux.html
[Sound Juicer]:         https://wiki.gnome.org/Apps/SoundJuicer
[Linux Kamarada 15.4]:  {% post_url en/2023-02-27-linux-kamarada-15-4-more-functional-beautiful-polished-and-elegant-than-ever %}
[repos]:                https://en.opensuse.org/Package_repositories#Official_Repositories
[FlatPak]:              {% post_url en/2022-01-31-flatpak-what-you-need-to-know-about-the-distro-agnostic-package-manager %}
[Flathub]:              https://flathub.org/pt-BR/apps/org.gnome.SoundJuicer
[GNOME]:                https://www.gnome.org/
[Ogg Vorbis]:           https://xiph.org/vorbis/
[MusicBrainz]:          https://musicbrainz.org/
[GrOOfbOOgalOO]:        https://www.groofboogaloo.com.br/
[cd-spotify]:           http://open.spotify.com/album/1hI3pGwo0z4jtQOEsquwvo
[cd-youtube]:           https://music.youtube.com/playlist?list=OLAK5uy_mX_xzseU96D7lBG-nZ-DrW_JsyGXTJ9Lg
