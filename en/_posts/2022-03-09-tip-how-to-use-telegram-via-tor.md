---
date: '2022-03-09 11:30:00 GMT-3'
image: '/files/2022/02/telegram-tor.png'
layout: post
nickname: 'telegram-tor'
title: 'Tip: How to use Telegram via Tor'
---

{% include image.html src='/files/2022/02/telegram-tor.png' %}

If for any reason you are prevented from using [Telegram] -- for example, if you are moving or traveling to one of the [countries that censor Telegram][telegram-censorship] (thankfully, Brazil is not on this list, and hopefully it's going to stay out of it) -- here's how you can still use it on [Linux] via the [Tor] Network. If you use the [Tor Browser], which I've already presented shortly [here][Tor Browser], you can also benefit from this tip.

As reference, I'm going to use [Linux Kamarada 15.3], which already comes with [Flatpak] and Tor installed out-of-the-box.

Telegram has a Linux app that can be installed on any distribution via Flatpak. If you already have Flatpak installed on your system, you can easily install the Telegram app from the app store, or by running the following command on a terminal:

```
# flatpak install org.telegram.desktop
```

If you need more information about Flatpak, take a look at:

- [Flatpak: what you need to know about the distro-agnostic package manager][Flatpak]

Also, if you need more information about Telegram, refer to:

- [The Telegram messenger][Telegram]

Make sure Tor is installed, active and running with the command:

```
# systemctl status tor
```

{% include image.html src='/files/2022/02/telegram-tor-01.jpg' %}

Look for the words **active (running)** in green. They should appear to you in green as well.

In case the above command gives you a different result, install, activate and start Tor by running the following commands:

```
# zypper in tor
# systemctl enable tor
# systemctl start tor
```

Now start Telegram and open the menu by clicking the hamburger icon, by the upper-left corner of the window:

{% include image.html src='/files/2022/03/telegram-tor-02-en.jpg' %}

On the menu, click **Settings**:

{% include image.html src='/files/2022/03/telegram-tor-03-en.jpg' %}

Click **Advanced**:

{% include image.html src='/files/2022/03/telegram-tor-04-en.jpg' %}

In the **Network and proxy** section, click **Connection type**:

{% include image.html src='/files/2022/03/telegram-tor-05-en.jpg' %}

Under **Proxy settings**, select **Use custom proxy**:

{% include image.html src='/files/2022/03/telegram-tor-06-en.jpg' %}

On the next screen, fill in the settings like this:

- Make sure the **SOCKS5** is selected
- In **Hostname**, enter `localhost`
- In **Port**:
  - If you are using Tor, enter `9050`; or
  - If you are using Tor Browser, enter `9150`.

{% include image.html src='/files/2022/03/telegram-tor-07-en.jpg' %}

Finally, click **Save**.

Back to the **Proxy settings**, make sure that Telegram was able to connect to the proxy (look for the word the **online**):

{% include image.html src='/files/2022/03/telegram-tor-08-en.jpg' %}

That's it. You can close all the settings and continue using Telegram as usual.

{% capture shield_icon %}

A shield icon by the lower-left corner of the window shows that Telegram is connected to the proxy:

{% include image.html src='/files/2022/03/telegram-tor-09.jpg' %}

By clicking it, you can easily access proxy settings.

{% endcapture %}

{% include update.html date="Mar 9, 2022" message=shield_icon %}

I hope this tip was helpful. If you have any doubts, don't hesitate to comment. Suggestions for other tips like this are also welcome.

[Telegram]:             {% post_url en/2021-05-10-the-telegram-messenger %}
[telegram-censorship]:  https://en.wikipedia.org/wiki/Government_censorship_of_Telegram_Messenger
[Linux]:                https://www.kernel.org/linux.html
[Tor]:                  https://www.torproject.org/
[Tor Browser]:          {% post_url en/2019-08-12-20-apps-you-can-use-the-same-way-on-both-linux-and-windows-part-2 %}#18-tor-browser
[Linux Kamarada 15.3]:  {% post_url en/2021-12-30-linux-kamarada-15-3-new-year-new-release %}
[Flatpak]:              {% post_url en/2022-01-31-flatpak-what-you-need-to-know-about-the-distro-agnostic-package-manager %}
