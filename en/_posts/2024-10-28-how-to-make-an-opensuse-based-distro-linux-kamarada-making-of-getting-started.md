---
date: '2024-10-28 00:50:00'
image: '/files/2021/04/kick-off.jpg'
layout: post
nickname: 'kick-off'
title: 'How to make an openSUSE-based distro (Linux Kamarada making of): getting started'
---

{% include image.html src='/files/2021/04/kick-off.jpg' %}

How to make a [Linux] distribution based on [openSUSE] is a tutorial that I've been thinking about doing for some time now. Now that Linux Kamarada has [some versions][download] released ([15.5] being the current) and another is on the way (15.6) I think I can explain how to do it. I decided to take the opportunity, since I've started working on the next version, to record the process and share how I do it, in case it's useful to someone else.

Not necessarily you want to create a distribution to share online. Maybe you just want to customize openSUSE (or -- why not? -- Linux Kamarada) to install on computers at work. Or maybe you develop software and want to create a [Live image] containing your app, so that it can be easily tested by potential users or demonstrated at events. A Live system is a controlled environment, ideal for testing.

Whatever your goal is, this post and the following ones can help you achieve it.

The tutorial will actually consist of a few posts. As I progress in the development of Linux Kamarada 15.6, I will share what I am doing and how.

I intend to do something practical, so I won't go into much detail about the tools -- mainly [Git], [Kiwi] and [Open Build Service (OBS)][obs] -- so reading this tutorial does not mean you won't need to consult other sources of information. In fact, much of what I learned was "on the fly": searching, trying, making mistakes, and asking for help on forums, groups, and mailing lists. So this tutorial will be more about showing the way and shortcutting the research, rather than being self-contained.

Today, we'll see how to start developing the distribution: how to create the project on the Open Build Service and how to create the first package. I'll show you how I kickstarted Linux Kamarada 15.6. Let's go!

## Ingredients: what we will need

As I mentioned in the [Linux Kamarada 15.5 release news][15.5], Linux Kamarada development takes place at [GitLab] and [OBS][obs-kamarada]. I prefer to use Git for storing and managing source code and OBS for building and packaging, although I could use just OBS to do all of that. Feel free to use [GitHub], GitLab, another Git service (like [Bitbucket]) or just OBS.

If you're not familiar with OBS yet, read the first section -- Meet the Open Build Service (OBS) -- of this text:

- [Integrating the Open Build Service with GitLab][obs-gitlab]

Open it in a new tab and keep it open because we will come back to it later.

If you prefer to use GitHub, read this text instead:

- [Integrating the Open Build Service with GitHub][obs-github]

I'm going to assume that you have an account on a Git service like GitLab or GitHub, that you can sign in to it, and that you're familiar with Git already.

To host your project's Git repositories, I recommend creating a group (in the case of GitLab) or an organization (in the case of GitHub), examples: [gitlab.com/kamarada][gitlab] or [github.com/kamarada](https://github.com/kamarada). But you can also use your personal profile, example: [gitlab.com/antoniomedeiros](https://gitlab.com/antoniomedeiros). For OBS, this makes no difference.

I'm also going to assume that you can already sign in with your openSUSE account to [build.opensuse.org][buildoo]. If you don't have an openSUSE account, create one by clicking the **Sign Up** link at the top of that page.

Finally, I'm also going to assume that you're using one of the openSUSE distributions (preferably [Leap or Tumbleweed][leap-tumbleweed], but [now there are many others][opensuse-distros]) or a distribution based on them (like Linux Kamarada). If we're talking about making a distro based on openSUSE, nothing seems more natural to me.

Install Git and **[osc]** (the OBS command-line client) on your computer:

```
# zypper in git osc
```

## The Linux Kamarada pattern

In case you don't know what patterns are in the openSUSE world, I explained that concept in this text:

- [openSUSE: what are patterns and how to install them][patterns]

That text presents the users point of view. Now we are going to look at the distribution developers point of view.

To define which packages make up Linux Kamarada, I created a pattern for it, taking other openSUSE patterns as reference:

{% include image.html src='/files/2024/10/kick-off-01.jpg' %}

That's why it's easy, as I said in the [Linux Kamarada 15.5 release news][15.5], to turn any openSUSE Leap installation into a Linux Kamarada installation: just add the distribution's repository and install the **[patterns-kamarada-gnome]** package.

## How to create patterns

If you want to see how patterns are defined in openSUSE, go to [software.opensuse.org][softwareoo] and search for `patterns`. Several packages whose name starts with `patterns` are listed. They all are patterns. Click one of them.

The next page lists the availability of this package for various distributions. Click the desired distribution (e.g. **openSUSE:Leap:15.6**) to view the source code for this package on OBS:

{% include image.html src='/files/2024/10/kick-off-02.jpg' %}

{% include image.html src='/files/2024/10/kick-off-03.jpg' %}

This page shows the files that make up the source code for the **patterns-xfce** package in the **openSUSE:Leap:15.6** project. Among these files, the most important is the spec file.

If you don't know what a spec file is and/or are new to [RPM] packaging, take a loot at:

- [Fedora RPM Guide][fedora-rpm-guide] (note that this links to the Wayback Machine since the Fedora Project for some reason removed the book, here's also [my own PDF copy][fedora-rpm-guide-pdf] I downloaded back in 2014)
- [RPM Packaging Guide][rpm-packaging-guide]
- [openSUSE:Packaging guidelines - openSUSE Wiki][opensuse-packaging]

In short, the spec file defines the RPM package. In the case of a pattern, this file gives the name and description of the pattern and its dependencies ([required, recommended][recommended] and suggested packages). For instance, this is the spec file for the XFCE pattern:

{% include image.html src='/files/2024/10/kick-off-04.jpg' %}

The source code for the Linux Kamarada pattern can be seen at:

- [gitlab.com/kamarada/patterns](https://gitlab.com/kamarada/patterns/-/tree/15.5) (link points to the 15.5 branch)

The main files are:

- [`patterns-kamarada-gnome.spec`](https://gitlab.com/kamarada/patterns/-/blob/15.5/patterns-kamarada-gnome.spec), which defines the pattern, and
- [`pattern-kamarada-gnome.png`](https://gitlab.com/kamarada/patterns/-/blob/15.5/pattern-kamarada-gnome.png), which is the pattern icon.

Opening the spec file, right at the beginning we have the `Name`, the description (`Summary`) and the `Version` of the pattern:

```
Name:           patterns-kamarada-gnome
Summary:        Linux Kamarada with GNOME desktop
Url:            https://gitlab.com/kamarada/patterns
Version:        15.5
Release:        0
Group:          Metapackages
License:        MIT
BuildRoot:      %{_tmppath}/%{name}-%{version}-build

```

Further on, we have the line that defines the pattern icon:

```
Source0:        pattern-kamarada-gnome.png
```

And throughout the file we have the pattern dependencies listed.

The Linux Kamarada pattern depends on other patterns:

```
Requires:       pattern() = apparmor
Requires:       pattern() = enhanced_base
Requires:       pattern() = x11_enhanced
Requires:       pattern() = gnome_basis
```

(these lines are not together in the file, they are spread out, I will not reproduce the entire file here so that the article does not get too long)

I thought it best not to create too many dependencies on other patterns, so that I could have more control over which packages are installed. Instead, I copied the dependencies I wanted from those patterns and kept their names commented out for reference:

```
#Requires:       pattern() = gnome

# kamarada/Linux-Kamarada-GNOME#62 - Wayland
Requires:       gnome-session-wayland
Requires:       xlsclients
Requires:       xwayland
```

My strategy for packages, in general, is:

- packages that I consider essential are required (listed with `Requires`) -- if you don't have these packages, I don't consider you to have a Linux Kamarada installation (note that removing these packages will cause the removal of the pattern as well); and
- translations or other packages that are useful but not for all computers and users are recommended (`Recommends`) or suggested (`Suggests`) -- for example, the **[hplip]** package is suggested, and comes installed by default, but depending on your printer, you can perfectly remove it (removing these packages won't cause the removal of the pattern, I consider that you still have a Linux Kamarada installation even if you remove these packages).

## Other examples of patterns

Here are examples of patterns that I look at to define mine and that may serve as reference for you as well:

- **[patterns-base]:** source code for the apparmor, base, console, enhanced_base, minimal_base, x11 and x11_enhanced patterns
- **[patterns-gnome]:** source code for the gnome_x11, gnome_basis, gnome_basic, gnome_games, gnome_imaging, gnome_internet, gnome_utilities and gnome_yast patterns
- **[patterns-yast]:** source code for the yast2_basis and yast2_install_wf patterns

Now that you understand the basic ideas behind patterns, let's get to work.

## Creating the pattern on Git

Note that I already have a Git repository for my pattern ([gitlab.com/kamarada/patterns](https://gitlab.com/kamarada/patterns)), so I'll use that as a starting point. You may need to create a repository yourself.

I usually create a branch for each release (`15.4`, `15.5`...) and a branch for development of each release (`15.4-dev`, `15.5-dev`...). This way, I can experiment on the development branch and merge it back into the release branch when it's ready.

I'm going to create the `15.6-dev` branch from the `15.5-dev`:

```
$ git checkout 15.5-dev
$ git checkout -b 15.6-dev
```

I changed the version in the spec file:

```
Version:        15.6
```

I committed the change and pushed it to the remote repository (since we are creating this branch, on the first push we need to define its remote):

```
$ git commit -a -m 'Upgrading from 15.5 to 15.6'
$ git push --set-upstream origin 15.6-dev
```

## Creating the projects on OBS

In the [openSUSE Build Service][buildoo], access your home project via the **Your Home Project** link in the OBS sidebar. Switch to the **Subprojects** tab. Then click **Create Subproject**:

{% include image.html src='/files/2024/10/kick-off-05.jpg' %}

I set `15.6` as the **Subproject Name** and `Linux Kamarada 15.6` as the **Title**, then clicked **Accept**:

{% include image.html src='/files/2024/10/kick-off-06.jpg' %}

Now we have an OBS project for Linux Kamarada 15.6 at:

- <https://build.opensuse.org/project/show/home:kamarada:15.6>

I entered the **home:kamarada:15.6** project and created another subproject, this one called `dev`, resulting in the **home:kamarada:15.6:dev** project:

- <https://build.opensuse.org/project/show/home:kamarada:15.6:dev>

If you go back to the subprojects list of your home project, you will see the two newly created projects:

{% include image.html src='/files/2024/10/kick-off-07.jpg' %}

## Setting up the repositories

Go to the development project (in my case, **home:kamarada:15.6:dev**), switch to the **Repositories** tab and click **Add from a Distribution**:

{% include image.html src='/files/2024/10/kick-off-08.jpg' %}

Check the **openSUSE Leap 15.6** repository:

{% include image.html src='/files/2024/10/kick-off-09.jpg' %}

Repositories are added to the project as soon as you check them. You don't need to click any buttons. Go back to the **Repositories** tab:

{% include image.html src='/files/2024/10/kick-off-10.jpg' %}

Now we have the **15.6** repo which will host the RPM packages.

For each repository of our project, we can configure which other repositories from other projects it will consider when obtaining packages during build time.

Let's add the openSUSE Leap 15.6 update repository. To do this, click the add icon.

Under **Project**, enter **openSUSE:Leap:15.6:Update**:

{% include image.html src='/files/2024/10/kick-off-11.jpg' %}

Under **Repositories**, keep the default **standard** selected and click **Accept**.

Since the **openSUSE:Leap:15.6:Update** repo already references the **openSUSE:Leap:15.6** repo, and OBS has intelligence to realize this, we can remove the **openSUSE:Leap:15.6** repo, to make our configuration leaner:

{% include image.html src='/files/2024/10/kick-off-12.jpg' %}

We need to create the **images** repo to host the ISO image and set it up according to the [KIWI documentation][kiwi-doc]. In earlier OBS versions, it was possible to do that via the GUI. Now, we need to specify it manually in the project meta configuration (**Meta** tab):

{% include image.html src='/files/2024/10/kick-off-13.jpg' %}

Just add:

```xml
<repository name="images">
    <path project="home:kamarada:15.6:dev" repository="openSUSE_Leap_15.6"/>
    <path project="Virtualization:Appliances:Builder" repository="openSUSE_Leap_15.6"/>
    <path project="openSUSE:Leap:15.6:Update" repository="standard"/>
    <arch>x86_64</arch>
</repository>
```

I also renamed the **15.6** repo to **openSUSE_Leap_15.6** to keep the standard of previous Linux Kamarada releases.

Note that the **images** repo references the other repo of the same project (in this case, **openSUSE_Leap_15.6**).

In the end, if you go back to the **Repositories** tab, it should look like this:

{% include image.html src='/files/2024/10/kick-off-14.jpg' %}

Once this repository configuration has been done in the development project (in my case, **home:kamarada:15.6:dev**), you can do a similar configuration in the production project ( **home:kamarada:15.6**).

## Creating the pattern on OBS

In the development project (in my case, **home:kamarada:15.6:dev**), switch to the **Overview** tab and click **Create Package**:

{% include image.html src='/files/2024/10/kick-off-15.jpg' %}

Give at least a **Name** to the package (in my case, `patterns-kamarada-gnome`) and click **Create**:

{% include image.html src='/files/2024/10/kick-off-16.jpg' %}

Once the pattern package has been created on OBS, let's check it out to our computer to work on it locally (similar to cloning a Git repository).

Check out the entire OBS project:

```
$ osc checkout home:kamarada:15.6:dev
```

Then enter the package folder:

```
$ cd home:kamarada:15.6:dev/patterns-kamarada-gnome
```

## Integrating OBS with Git

Now let's integrate OBS with Git, so that the package source code is pulled from Git and compiled on OBS. For instructions on how to do this, see one of the following tutorials, depending on the Git provider you use (GitLab or GitHub):

- [Integrating the Open Build Service with GitLab][obs-gitlab]
- [Integrating the Open Build Service with GitHub][obs-github]

## First package ready

Finally, going back to the development project overview, we have our first package successfully compiled on OBS:

{% include image.html src='/files/2024/10/kick-off-17.jpg' %}

There are a few more Linux Kamarada packages that are compiled by this duo OBS + Git. With the script presented here, I'll kickstart these packages as well.

Follow Linux Kamarada on your preferred social network to be notified of new episodes of this journey.

{% capture obs_rpm %}

Continue your reading:

- [Building RPM packages with the Open Build Service (OBS)]({% post_url en/2024-11-06-building-rpm-packages-with-the-open-build-service-obs %})

{% endcapture %}

{% include update.html date="Nov 6, 2024" message=obs_rpm %}

[Linux]:                    https://www.kernel.org/linux.html
[openSUSE]:                 https://www.opensuse.org/
[download]:                 /en/download
[15.5]:                     {% post_url en/2024-05-27-linux-kamarada-15-5-more-aligned-with-opensuse-leap-and-other-distributions %}
[Live image]:               {% post_url en/2015-11-25-what-is-a-livecd-dvd-usb %}
[Git]:                      https://git-scm.com/
[Kiwi]:                     https://osinside.github.io/kiwi/
[obs]:                      https://openbuildservice.org/

[GitLab]:                   https://gitlab.com/kamarada
[obs-kamarada]:             https://build.opensuse.org/project/subprojects/home:kamarada
[GitHub]:                   https://github.com/
[Bitbucket]:                https://bitbucket.org/
[obs-gitlab]:               {% post_url en/2021-03-15-integrating-the-open-build-service-with-gitlab %}
[obs-github]:               {% post_url en/2019-03-19-integrating-the-open-build-service-with-github %}
[buildoo]:                  https://build.opensuse.org
[leap-tumbleweed]:          {% post_url en/2020-12-07-opensuse-leap-vs-opensuse-tumbleweed-what-is-the-difference %}
[opensuse-distros]:         https://edworking.com/it/news/startups/is-opensuse-facing-a-major-identity-crisis#:~:text=openSUSE%20offers%20a%20rich%20array%20of%20operating%20systems%2C%20including%20Leap%20(stable)%2C%20Tumbleweed%20(rolling)%2C%20Slowroll%20(slower%20rolling)%2C%20and%20various%20modern%20combinations%20like%20MicroOS%2C%20Aeon%2C%20Kalpa%2C%20Argon%2C%20Krypton%2C%20and%20Leap%20Micro.
[osc]:                      https://linux.die.net/man/1/osc

[patterns]:                 {% post_url en/2024-08-14-opensuse-what-are-patterns-and-how-to-install-them %}
[patterns-kamarada-gnome]:  https://software.opensuse.org/package/patterns-kamarada-gnome
[softwareoo]:               https://software.opensuse.org/
[rpm]:                      https://en.wikipedia.org/wiki/RPM_Package_Manager
[fedora-rpm-guide]:         https://web.archive.org/web/20220628164331/docs.fedoraproject.org/en-US/Fedora_Draft_Documentation/0.1/html//RPM_Guide/index.html
[fedora-rpm-guide-pdf]:     /files/2024/10/fedora-rpm-guide.pdf
[rpm-packaging-guide]:      https://rpm-packaging-guide.github.io/
[opensuse-packaging]:       https://en.opensuse.org/openSUSE:Packaging_guidelines
[recommended]:              {% post_url en/2021-04-07-what-are-recommended-packages-and-how-to-install-them-on-opensuse %}
[hplip]:                    https://software.opensuse.org/package/hplip

[patterns-base]:            https://build.opensuse.org/package/view_file/openSUSE:Leap:15.6/patterns-base/patterns-base.spec
[patterns-gnome]:           https://build.opensuse.org/package/view_file/openSUSE:Leap:15.6/patterns-gnome/patterns-gnome.spec
[patterns-yast]:            https://build.opensuse.org/package/view_file/openSUSE:Leap:15.6/patterns-yast/patterns-yast.spec

[kiwi-doc]:                 https://osinside.github.io/kiwi/working_with_images/build_in_buildservice.html
