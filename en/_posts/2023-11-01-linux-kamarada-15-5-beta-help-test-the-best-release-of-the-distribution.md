---
date: '2023-11-01 10:00:00 GMT-3'
image: '/files/2023/11/15.5-beta.png'
layout: post
nickname: 'kamarada-15.5-beta'
title: 'Linux Kamarada 15.5 Beta: help test the best release of the distribution'
---

{% include image.html src='/files/2023/11/15.5-beta.png' %}

The Linux Kamarada Project announces the 15.5 Beta release of the homonym [Linux distribution][linux], based on [openSUSE Leap 15.5][leap-15.5]. It is now available for [download].

<!--more-->

The [Download] page has been updated and now offer mainly two releases for download:

- 15.4 Final, which you can install on your home or work desktop; and
- 15.5 Beta, which you can test if you want to help in development.

openSUSE Leap 15.5 was released in [June][leap-15.5] and has since received various updates and fixes. Therefore, Linux Kamarada 15.5, although still in [beta] phase, should present considerable stability already. Still, bugs are expected and this version may not be ready for daily use. If you find a bug, please report it. Take a look at the [Help] page to see how to get in touch. Of course, bugs can be found and fixed anytime, but the sooner the better!

The easiest way to test Linux is using [VirtualBox]. For more information, please read:

- [VirtualBox: the easiest way to try Linux without installing it][VirtualBox]

Or, if you want to try Linux Kamarada on your own machine, the easiest way to do this is using a USB stick that you can make bootable with [Ventoy]:

- [Ventoy: create a multiboot USB drive by simply copying ISO images to it][Ventoy]

If you want to install Linux Kamarada on your computer for daily use, it is recommended that you install the 15.4 Final release, which is already ready, and then, when the 15.5 release is ready, upgrade. For more information on Linux Kamarada 15.4 Final, see:

- [Linux Kamarada 15.4: more functional, beautiful, polished and elegant than ever][kamarada-15.4]

## What's new?

What's new in the 15.5 release is:

- most of the same apps as in 15.4, but updated with new features and bug fixes;

- [Wayland] is now the default display server (it is still possible to revert to [X.Org] if needed);

- the live image welcome screen is more polished and elegant, as a result of technical changes: the UI library it was based on was migrated from [GTK] 3 to GTK 4, and the underlying desktop it used was changed from [Openbox] to [GNOME Kiosk] (there were no changes to the features or how to use this screen, the changes were only internal, the user should not notice any difference except for the new look);

- the **Advanced Network Configuration** app was added, it is present in several other Linux distros that are also based on the [GNOME] desktop;

- the Linux Kamarada [pattern] ([`patterns-kamarada-gnome.spec`][patterns-spec]) has been revised in accordance with the [openSUSE patterns], as well as each package mentioned in the Linux Kamarada live image description ([`appliance.kiwi`][appliance.kiwi]) has been revised, this change is also just technical and internal, but it seeks for better software quality;

- translation packages were added, achieving an even more complete level of translation of the distro as a whole (this change may be noticed mainly by Brazilian users);

- the **[openSUSE-repos-Leap]** package is now used to add the official openSUSE repositories, thus making it easier to set up these repos (more information can be found [here][openSUSE-repos-Leap]);

{% capture update-repos %}

I wrote a post explaining this new feature in more detail:

- [Install the new openSUSE-repos-Leap package]({% post_url en/2023-11-13-install-the-new-opensuse-repos-leap-package %})

{% endcapture %}

{% include update.html date="Nov 13, 2023" message=update-repos %}

- [Python] 2 was removed from openSUSE Leap 15.5 and, therefore, from Linux Kamarada 15.5 as well, thus the **[Hearts]** game was also removed, as [it had not received updates since 2013][Hearts] and required Python 2;

- the [Papirus icon theme for LibreOffice][papirus-libreoffice], which [had not received updates since 2020][papirus-libreoffice], was removed and replaced with the default [LibreOffice] icon theme (Elementary); and

- new beautiful wallpapers (but you can keep using the previous ones or the openSUSE defaults, if you prefer).

## Upgrading to 15.5 Beta

If you are already using Linux Kamarada 15.4 and trust your personal experience as an user, you may want to upgrade to Linux Kamarada 15.5 Beta to help test the upgrade process. Generally speaking, you should just need to follow this upgrade how-to, replacing `15.4` with `15.5`, where applicable:

- [Linux Kamarada and openSUSE Leap: how to upgrade from 15.3 to 15.4][upgrade-howto]

Since Linux Kamarada 15.4 repos are already setup with the `$releasever` variable, it should be pretty easy to upgrade to 15.5. Examples of commands from the tutorial above, already adapted for the new release:

```
# zypper --releasever=15.5 ref
# zypper --releasever=15.5 dup --download-only --allow-vendor-change
```

At the end of the upgrade, as a new 15.5 feature, it is recommended (although not strictly necessary) that you install the **[openSUSE-repos-Leap]** package:

```
# zypper in openSUSE-repos-Leap
```

If you don't feel safe upgrading to 15.5 Beta with these tips, don't worry. In the next few days, I'm going to publish an updated version of the upgrade how-to.

{% capture update-upgrade %}

Here it is:

- [Linux Kamarada 15.4 and openSUSE Leap 15.4: how to upgrade to 15.5]({% post_url en/2023-11-27-linux-kamarada-15-4-and-opensuse-leap-15-4-how-to-upgrade-to-15-5 %})

{% endcapture %}

{% include update.html date="Nov 27, 2023" message=update-upgrade %}

## Specs

To allow comparison among Linux Kamarada releases, here is a summary of the software contained in Linux Kamarada 15.5 Beta Build 5.4:

- Linux kernel 5.14.21
- X.Org display server 21.1.4 (with Wayland 1.21.0 enabled by default)
- GNOME desktop 41.8 including its core apps, such as Files (previously Nautilus), Calculator, Terminal, Text editor (gedit), Screenshot and others
- LibreOffice office suite 7.5.4.1
- Mozilla Firefox 115.4.0 ESR (default web browser)
- Chromium 118 web browser (alternative web browser)
- VLC media player 3.0.18
- Evolution email client 3.42.4
- YaST control center
- Brasero 3.12.3
- CUPS 2.2.7
- Drawing 1.0.1
- Firewalld 0.9.3
- GParted 1.4.0
- HPLIP 3.21.10
- Java (OpenJDK) 11.0.21
- KeePassXC 2.7.6
- Linphone 5.0.5
- PDFsam Basic 5.1.3
- Pidgin 2.14.8
- Python 3.6.15
- Samba 4.17.9
- Tor 0.4.7.13
- Transmission 3.00
- Vim 9.0.1632
- Wine 8.0
- Calamares installer 3.2.62
- Flatpak 1.14.4
- games: AisleRiot (Solitaire), Chess, Mahjongg, Mines (Minesweeper), Nibbles (Snake), Quadrapassel (Tetris), Reversi, Sudoku

That list is not exhaustive, but it gives a notion of what can be found in the distribution.

[linux]:                https://www.kernel.org/linux.html
[leap-15.5]:            {% post_url en/2023-06-07-leap-15-5-release-matures-sets-up-technological-transition %}
[download]:             /en/download
[beta]:                 https://en.wikipedia.org/wiki/Software_release_life_cycle#Beta
[Help]:                 /en/help
[virtualbox]:           {% post_url en/2019-10-10-virtualbox-the-easiest-way-to-try-linux-without-installing-it %}
[ventoy]:               {% post_url en/2020-07-29-ventoy-create-a-multiboot-usb-drive-by-simply-copying-iso-images-to-it %}
[kamarada-15.4]:        {% post_url en/2023-02-27-linux-kamarada-15-4-more-functional-beautiful-polished-and-elegant-than-ever %}
[Wayland]:              https://wayland.freedesktop.org/
[X.Org]:                https://www.x.org/
[GTK]:                  https://www.gtk.org/
[Openbox]:              http://openbox.org/
[GNOME Kiosk]:          https://gitlab.gnome.org/GNOME/gnome-kiosk
[GNOME]:                https://www.gnome.org/
[pattern]:              {% post_url pt/2021-04-01-o-que-sao-padroes-e-como-instala-los-no-opensuse %}
[patterns-spec]:        https://gitlab.com/kamarada/patterns/-/blob/15.5/patterns-kamarada-gnome.spec
[openSUSE patterns]:    https://software.opensuse.org/search?baseproject=openSUSE%3ALeap%3A15.5&q=patterns
[appliance.kiwi]:       https://gitlab.com/kamarada/Linux-Kamarada-GNOME/-/blob/15.5/appliance.kiwi
[openSUSE-repos-Leap]:  {% post_url en/2023-11-13-install-the-new-opensuse-repos-leap-package %}
[Python]:               https://www.python.org/
[Hearts]:               https://www.jejik.com/gnome-hearts
[papirus-libreoffice]:  https://github.com/PapirusDevelopmentTeam/papirus-libreoffice-theme
[LibreOffice]:          https://www.libreoffice.org/
[upgrade-howto]:        {% post_url en/2023-01-16-linux-kamarada-and-opensuse-leap-how-to-upgrade-from-153-to-154 %}
