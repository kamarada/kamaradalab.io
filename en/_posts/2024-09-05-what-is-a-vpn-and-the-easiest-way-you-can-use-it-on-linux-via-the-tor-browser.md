---
date: '2024-09-05 11:00:00 GMT-3'
image: '/files/2024/09/tor-browser-en.jpg'
layout: post
nickname: 'tor-browser'
title: 'What is a VPN and the easiest way you can use it on Linux: via the Tor Browser'
excerpt: 'Understand what a VPN is, why you should use it, learn about the Tor Network and see how to install and use the Tor Browser on Linux.'
---

{% include image.html src='/files/2024/09/tor-browser-en.jpg' %}

[Search for VPNs has recently increased in Brazil][techradar], according to data from [Google Trends]. But after all, what is a VPN, and why would anyone want to use one?

The acronym **[VPN]** comes from _Virtual Private Network_. It is a technology that establishes a secure and encrypted connection over a less secure public network, such as the Internet. It serves to protect your data and privacy while you browse, ensuring that your online activities remain safe and confidential.

A VPN works like this: two computers are connected to different networks (they may be in different buildings, cities or even countries, for example), but both are connected to the Internet. They then establish an encrypted circuit on the world wide web through which they exchange information privately. It would be like a phone call that could not be tapped.

{% include image.html src='/files/2024/09/vpn-01.jpg' caption='Despite being connected to the Internet, two computers on a VPN communicate privately.' %}

For these computers users, it is imperceptible that they are far apart. It is as if they were directly connected by a cable, even though this cable does not actually exist (which is why this network is called **virtual**). And the communication, although occurring over the Internet, cannot be read by other computers on the network, because these two computers use encryption to communicate (hence, it is a **private** network). Because information travels through this encrypted circuit that cannot be intercepted, a VPN is also called a **tunnel**.

{% include image.html src='/files/2024/09/vpn-02.png' caption='A tunnel is just like that: only those inside it can see what passes through it.' %}

One practical use of VPNs is to bypass censorship. Typically, when we access a website, our connection to that website's server is routed through the carrier's network infrastructure, which may be blocking access to the website (by government order, for example). There are VPNs that allow the computer on the other end, which may be in another country, to do the work of the carrier's router, so we can access the censored website as if we were in another country.

{% include image.html src='/files/2024/09/vpn-03.png' %}

That kind of VPN is used mainly in [countries with authoritarian governments][vpn-statistics] where citizens are prohibited from accessing websites or apps considered inappropriate by the dictator in power. It is alarming that Brazilian people are increasingly looking for VPNs, as well as the country being mentioned in the [Westminster Declaration], signed by several freedom of speech activists, including [Julian Assange], [Edward Snowden], [Glenn Greenwald] and [Jordan Peterson].

If you feel the need to use a VPN, today I want to introduce you to one that is based on [free (_libre_) software][free-sw] and can be used for free by anyone anywhere in the world.

{% include image.html src='/files/2024/09/tor.svg' %}

The **[Tor Project]** (short for _The Onion Router_) maintains a free, worldwide, volunteer, decentralized network of tunnels -- the Tor Network -- through which private data from its users travels encrypted, anonymously, safely and free from censorship. The mention to the onion comes from the fact that information within this network circulates in a compartmentalized way. The Tor Network employs several layers of encryption to ensure that each computer along the way only knows enough information to pass the message on to the next:

{% include image.html src='/files/2024/09/how-tor-works.png' caption='Source: [manual do Navegador Tor](https://tb-manual.torproject.org/pt-BR/about/)' %}

The easiest way to connect to the Tor Network and use it on [Linux] is via the **[Tor Browser]**, which is a web browser based on [Mozilla Firefox] modified to route all its network traffic through the Tor Network. When using the Tor Browser, you browse without being identified or tracked; to websites, your internet traffic seems to come from a different IP address, often in a different country.

Below, you will see how to install and use Tor Browser on Linux. As a reference distribution, I'm going to use [Linux Kamarada 15.5], based on [openSUSE Leap]. If you use another Linux distribution, just the installation method should be different. For more information, take a look at your distribution's documentation.

## Installing Tor Browser

You can install Tor Browser from the [openSUSE official repositories][repos] using one of two ways: from the graphical interface using 1-Click Install or from the terminal using the **[zypper]** package manager -- choose whichever method you prefer.

To install Tor Browser using 1-Click Install, click the following button:

<p class='text-center'>
    <a class='btn btn-primary' href='/downloads/tor-browser.ymp'>
        <i class='fas fa-bolt'></i> 1-Click Install
    </a>
</p>

To install Tor Browser using the terminal, run the following command:

```
# zypper in torbrowser-launcher
```

## Starting Tor Browser

To start Tor Browser, if you use the [GNOME] desktop, click **Activities**, by the upper-left corner of the screen, type `tor` and then click the Tor Browser icon:

{% include image.html src='/files/2024/09/tor-browser-01-en.jpg' %}

The package we downloaded and installed actually contains a script that downloads and installs Tor Browser, as well as downloads and installs any updates to it, so that you are always using the latest version of Tor Browser. On the first use, it may take a while to download the browser:

{% include image.html src='/files/2024/09/tor-browser-02-en.jpg' %}

This is the initial screen of Tor Browser, not yet connected to the Tor Network and therefore not yet ready for use:

{% include image.html src='/files/2024/09/tor-browser-03-en.jpg' %}

Click **Connect**. Wait for the connection to the Tor Network to be established:

{% include image.html src='/files/2024/09/tor-browser-04-en.jpg' %}

The next screen indicates that Tor Browser is connected to the Tor Network and is therefore ready to use:

{% include image.html src='/files/2024/09/tor-browser-05-en.jpg' %}

## Testing the connection to the Tor Network

Before you start using Tor Browser, you should always test whether it is actually connected to the Tor Network. To do this, go to:

- <https://check.torproject.org/>

{% include image.html src='/files/2024/09/tor-browser-06-en.jpg' %}

The page should say: **"Congratulations. This browser is configured to use Tor."** If you see this message, it is now safe to use the Tor Browser to access the sites you need to access.

If you see a message other than this, Tor Browser is not properly connected to the Tor Network and is therefore not ready for use. Browsing this way is unsafe. For more information on what you can do to solve this, see the Tor Browser [manual] or [support].

## Recommended further reading

Knowledge is power. And what you just read in this article is the kind of knowledge you need to have before you actually need to use it. I recommend that you study the material on the following websites, which can help you browse the Internet with more security and privacy and less surveillance and censorship:

- [Security in-a-box - digital security tools and tactics](https://securityinabox.org/)
- [Privacy Tools Guide: Website for Encrypted Software & Apps](https://www.privacytools.io/)
- [Surveillance Self-Defense (SSD) guide by Electronic Frontier Foundation (EFF)](https://ssd.eff.org/)

[techradar]:                    https://www.techradar.com/pro/vpn/vpn-usage-soars-in-brazil-after-the-ban-on-x-but-people-now-risk-huge-fines
[Google Trends]:                https://trends.google.com/trends/explore?date=2024-08-25%202024-09-01&geo=BR&q=vpn
[VPN]:                          https://en.wikipedia.org/wiki/Virtual_private_network
[vpn-statistics]:               https://www.forbes.com/advisor/business/vpn-statistics/
[Westminster Declaration]:      https://westminsterdeclaration.org/
[Julian Assange]:               https://en.wikipedia.org/wiki/Julian_Assange
[Edward Snowden]:               https://en.wikipedia.org/wiki/Edward_Snowden
[Glenn Greenwald]:              https://en.wikipedia.org/wiki/Glenn_Greenwald
[Jordan Peterson]:              https://www.jordanbpeterson.com/about/
[free-sw]:                      https://www.gnu.org/philosophy/free-sw.html
[Tor Project]:                  https://www.torproject.org/
[Linux]:                        https://www.kernel.org/linux.html
[Tor Browser]:                  https://www.torproject.org/download/
[Mozilla Firefox]:              {% post_url en/2019-07-20-20-apps-you-can-use-the-same-way-on-both-linux-and-windows-part-1 %}#1-mozilla-firefox
[Linux Kamarada 15.5]:          {% post_url en/2024-05-27-linux-kamarada-15-5-more-aligned-with-opensuse-leap-and-other-distributions %}
[openSUSE Leap]:                {% post_url en/2020-12-07-opensuse-leap-vs-opensuse-tumbleweed-what-is-the-difference %}
[repos]:                        https://en.opensuse.org/Package_repositories#Official_Repositories
[zypper]:                       https://en.opensuse.org/Portal:Zypper
[GNOME]:                        https://www.gnome.org/
[manual]:                       https://tb-manual.torproject.org/running-tor-browser/
[support]:                      https://support.torproject.org/
