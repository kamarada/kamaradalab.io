---
date: '2022-01-31 23:59:00 GMT-3'
image: '/files/2022/01/flatpak.jpg'
layout: post
nickname: 'flatpak'
title: 'Flatpak: what you need to know about the distro-agnostic package manager'
excerpt: 'Flatpak is a distribution-agnostic package manager. It brought a simpler alternative to install programs on different distros: as long as Flatpak is installed on the system, the same Flatpak package can be installed on any distro. A Flatpak package contains not only the application, but also most of the libraries needed to run it. Applications are run by Flatpak in a sandbox environment that isolates applications from one another and from the system, increasing security as a whole.'
---

Until recently, different [Linux] distros used different package formats, often incompatible with each other. For example, [Debian] and [Ubuntu] use [DEB] packages, while [Red Hat], [Fedora], [openSUSE] and [Linux Kamarada][kamarada-15.3] use [RPM] packages. These traditional packaging formats presented some inconveniences for everyone -- users, software developers, and distro and package maintainers:

- **Duplicated work packaging apps:** as developers had to package the same application in different formats for different distributions, it was common to choose just one or a few distributions to support, while ignoring all the others;
- That takes us to the second point, **user limited to distributions that have the apps**: only a few distributions had a wide range of applications natively packaged and available for easy installation and use, so the distributions that could be considered for everyday use were limited;
- **User limited to apps that are packaged already:** not all applications are natively available in every Linux distribution, if the user needs a program that is not packaged for their distribution, they will need to download it, and often compile it manually;
- **Old and outdated packages:** distributions whose lifecycle is organized into versions, which are supported for a long time (LTS, Long Term Support), often have very old versions of applications. This is a problem mainly in distributions like [openSUSE Leap] or Debian, not so much in distributions with frequent updates (rolling release) like [openSUSE Tumbleweed] or [Arch Linux]; and
- **Difficulty to support:** as each distribution packages applications in a different way, and each distribution has different versions of programs and their dependencies, developers have little control over what is on the user's computer, making it difficult to troubleshoot bugs (or even to reduce the chance that they will occur, in the first place).

Striving to solve these problems, some alternative package formats emerged, such as [Snap], [Flatpak] and [AppImage]. Today, we are going to talk about Flatpak.

{% include image.html src='/files/2022/01/flatpak.jpg' %}

**[Flatpak]** is a distribution-agnostic package manager. It brought a simpler alternative to install programs on different distros: as long as Flatpak is installed on the system, the same Flatpak package can be installed on any distro. A Flatpak package contains not only the application, but also most of the libraries needed to run it. Applications are run by Flatpak in a sandbox environment that isolates applications from one another and from the system, increasing security as a whole.

Flatpak was originally created by [Alexander Larsson][alexl] while working on container technologies at Red Hat. Although it has received many contributions from Red Hat and [GNOME] developers, Flatpak is not tied to either this distribution or this desktop, having been developed as an independent [free software][free-sw] project from the beginning. [Formerly][flatpak-history] called xdg-app, it was first released in March 2015 and then renamed to Flatpak in May 2016. The following month, the competitor Snap, which [until then supported Ubuntu only][snap-wikipedia], was ported to other distributions.

Flatpak is currently supported by at least [33 major distributions][flatpak-setup].

Just as distributions have the concept of repositories, Flatpak also lets you get packages from one or more repositories, the main one being [Flathub] at [flathub.org][Flathub]. In [Flatpak terminology][flatpak-concepts], repositories are called **remotes**, very similarly to [Git] remotes.

Here's how to install and configure Flatpak on your system, how to use it to install apps, and other information that may be helpful.

## How to install Flatpak

[Linux Kamarada 15.3][kamarada-15.3] comes with Flatpak already installed out-of-the-box. Flathub is also pre-configured. If you are using a Linux Kamarada 15.3 [LiveDVD/USB] or a fresh installation made of one, you can skip this section. If you [upgraded from a previous release][how-to-upgrade], you may want to go through the steps below to make sure everything is in place.

Flatpak is available in the [default repositories][opensuse-repos] of all currently maintained openSUSE Leap and openSUSE Tumbleweed releases. On these distros, you can install Flatpak in two ways: from the graphical interface using 1-Click Install or from the terminal using the **[zypper]** package manager -- choose whichever method you prefer.

To install Flatpak using 1-Click Install, click the following button:

<p class='text-center'><a class='btn btn-sm btn-outline-primary' href='/downloads/flatpak.ymp'><i class='fas fa-bolt'></i> 1-Click Install</a></p>

To install Flatpak using the terminal, run the following command:

```
# zypper in flatpak
```
Then, add the Flathub repository by running:

```
# flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
```

Finally, restart your system (I know this is unusual for Linux, but it's the last step of the [Flatpak installation instructions for openSUSE][flatpak-setup-opensuse]).

If you use another distribution, see how to install Flatpak on the [Flatpak website][flatpak-setup].

From now on, I will focus on Linux Kamarada 15.3 and openSUSE.

## How to install apps with Flatpak

Now you have everything you need to install Flatpak apps on Linux Kamarada using GNOME's **Software** app, the terminal and/or the Flathub website.

### 1) Using GNOME Software

You can search for Flatpak apps to install using the **Software** app. To do this, open the **Activities** menu, type `software` and click the application icon:

{% include image.html src='/files/2022/01/flatpak-01-en.jpg' %}

On the **Software** app, search for the app you want to install, either by browsing the categories or using the button with a **magnifying glass** icon by the upper-left corner of the window:

{% include image.html src='/files/2022/01/flatpak-02-en.jpg' %}

When you find the app you are searching for, click it:

{% include image.html src='/files/2022/01/flatpak-03-en.jpg' %}

The next screen shows detailed info about the app. You can check it and click the **Install** button:

{% include image.html src='/files/2022/01/flatpak-04-en.jpg' %}

You may need to provide the administrator (root user) password to install the app.

You can check that the app comes from Flathub by looking at the **Source** field, further down the screen:

{% include image.html src='/files/2022/01/flatpak-05-en.jpg' %}

There are apps that can be installed either from Flathub or from a distro repository. In these cases, a **Source** menu by the top of the window lets you select where to get the app from:

{% include image.html src='/files/2022/01/flatpak-06-en.jpg' %}

As you select one source or another, the info about the app changes, you can compare them:

<div class="row">
    <div class="col-md">
        {% include image.html src='/files/2022/01/flatpak-07-en.jpg' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2022/01/flatpak-08-en.jpg' %}
    </div>
</div>

### 2) From the Flathub website

You can also search for Flatpak apps and install them using your favorite browser. Just go to the Flathub website at [flathub.org][Flathub] and browse the categories or search for the name of the app you want to install:

{% include image.html src='/files/2022/01/flatpak-09.jpg' %}

When you find the app, click it. The next page shows detailed info about it. You can check it and click the **Install** button:

{% include image.html src='/files/2022/01/flatpak-10-en.jpg' %}

The browser will download a `.flatpakref` file. Select to open it using the **Software** app, as in the image above.

On the **Software** app, click the **Install** button:

{% include image.html src='/files/2022/01/flatpak-11-en.jpg' %}

### 3) Using the terminal

[**flatpak**][flatpak-man] is the primary Flatpak command, to which specific commands are appended.

To search for an app, use the **flatpak search** command. For example:

```
$ flatpak search spotify
```

{% include image.html src='/files/2022/01/flatpak-12-en.jpg' %}

Search will return any applications matching the search terms. Each search result includes the **application ID**, which is used for the other operations with Flatpak. In this example, I'm looking for the `com.spotify.Client` app.

To install the application, use the **flatpak install** command. For example:

```
$ flatpak install com.spotify.Client
```

One tip is to use the Flathub website to search the app and find out its ID, which is the last part of the page address:

{% include image.html src='/files/2022/01/flatpak-13.jpg' %}

Or copy the command for installing the app, which appears at the bottom of the page:

{% include image.html src='/files/2022/01/flatpak-14.jpg' %}

## How to start a Flatpak app

Once installed, the app can be started from the **Activities** menu as usual.

You can also start a Flatpak app from the terminal, using the **flatpak run** command. For example:

```
$ flatpak run com.spotify.Client
```

## How to update Flatpak apps

You can update installed Flatpak apps using the **Software** app. To do this, switch to the **Updates** tab:

{% include image.html src='/files/2022/01/flatpak-15-en.jpg' %}

You can update all Flatpak apps at once by clicking **Update All**.

Or, if you want to update just one application, click its corresponding **Update** button.

If you want to use the terminal, to update all Flatpak apps at once, run:

```
$ flatpak update
```

To update just one app, inform its ID to the **flatpak update** command. For example:

```
$ flatpak update io.lbry.lbry-app
```

## How to uninstall a Flatpak app

You can uninstall a Flatpak app using the **Software** app. To do this, search for the app, click it in the list and, on the next screen, which shows its info, click **Remove**:

{% include image.html src='/files/2022/01/flatpak-16-en.jpg' %}

You can also uninstall a Flatpak app from the terminal, using the **flatpak uninstall** command. For example:

```
$ flatpak uninstall com.spotify.Client
```

## Limitations

Not to say that I only said great things about Flatpak, I list some of its issues below. When using Flatpak, be aware of them:

- Flatpak was not designed to be used on servers, only on Linux desktops;
- As a Flatpak package bundles the application and all of its dependencies, it consumes more disk space than traditional RPM packages; and
- As Flatpak apps run on isolated environments, this can limit some features of some apps. For example, Flatpak apps may appear with the default GNOME theme instead of your [custom GTK theme][gtk-themes], or they may have access only to certain folders such as Documents and Downloads.

## Conclusion

I believe that the practicality, the ease of using Flatpak and the convenience provided by the large number of apps available on Flathub more than outweigh its limitations. The balance is positive and Flatpak expands the possibilities for Linux Kamarada users.

I suggest using Flatpak the same way I do: I prefer to install applications from the distribution's own repositories. But if an app isn't in those repositories, or it is but I need a newer version that is available on Flathub, then I install that app using Flatpak.

I hope this guide has been helpful to you. Comment if you have any doubts or suggestions.

See you!

## References

- [Flatpak documentation][flatpak-docs]
- [How to Install Flatpak & Flathub on Ubuntu (Complete Guide) - OMG! Ubuntu!][omgubuntu]
- [openSUSE Quick Setup - Flatpak][flatpak-setup-opensuse]
- [Flatpak - openSUSE Wiki][opensuse-wiki]
- [What is Flatpak in Linux? - It's FOSS][itsfoss]

[Linux]:                    https://www.kernel.org/linux.html
[debian]:                   https://www.debian.org/
[ubuntu]:                   https://ubuntu.com/
[deb]:                      https://en.wikipedia.org/wiki/Deb_(file_format)
[Red Hat]:                  https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux
[fedora]:                   https://getfedora.org/
[opensuse]:                 https://www.opensuse.org/
[kamarada-15.3]:            {% post_url en/2021-12-30-linux-kamarada-15-3-new-year-new-release %}
[rpm]:                      https://en.wikipedia.org/wiki/RPM_Package_Manager
[openSUSE Leap]:            {% post_url en/2020-12-07-opensuse-leap-vs-opensuse-tumbleweed-what-is-the-difference %}
[openSUSE Tumbleweed]:      {% post_url en/2020-12-07-opensuse-leap-vs-opensuse-tumbleweed-what-is-the-difference %}
[Arch Linux]:               https://archlinux.org/
[Snap]:                     https://snapcraft.io/about
[Flatpak]:                  https://flatpak.org/
[AppImage]:                 https://appimage.org/
[alexl]:                    https://blogs.gnome.org/alexl/2018/06/20/flatpak-a-history/
[GNOME]:                    https://www.gnome.org/
[free-sw]:                  https://www.gnu.org/philosophy/free-sw.html
[flatpak-history]:          https://flatpak.org/about/
[snap-wikipedia]:           https://en.wikipedia.org/wiki/Snap_(package_manager)#Adoption
[flatpak-setup]:            https://flatpak.org/setup/
[Flathub]:                  https://flathub.org/
[flatpak-concepts]:         https://docs.flatpak.org/en/latest/basic-concepts.html#repositories
[Git]:                      https://git-scm.com/
[LiveDVD/USB]:              {% post_url en/2015-11-25-what-is-a-livecd-dvd-usb %}
[how-to-upgrade]:           {% post_url en/2021-12-23-linux-kamarada-and-opensuse-leap-how-to-upgrade-from-152-to-153 %}
[opensuse-repos]:           https://en.opensuse.org/Package_repositories
[zypper]:                   https://en.opensuse.org/Portal:Zypper
[flatpak-setup-opensuse]:   https://flatpak.org/setup/openSUSE
[flatpak-man]:              https://www.man7.org/linux/man-pages/man1/flatpak.1.html
[gtk-themes]:               {% post_url en/2019-05-09-customize-gnome-with-themes %}
[flatpak-docs]:             https://docs.flatpak.org/
[omgubuntu]:                https://www.omgubuntu.co.uk/how-to-install-flatpak-on-ubuntu
[opensuse-wiki]:            https://en.opensuse.org/Flatpak
[itsfoss]:                  https://itsfoss.com/what-is-flatpak/
