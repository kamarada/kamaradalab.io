---
date: '2021-05-10 11:00:00 GMT-3'
image: '/files/2021/05/im-telegram-pt.jpg'
layout: post
nickname: 'telegram'
title: 'The Telegram messenger'
---

{% include image.html src='/files/2021/05/im-telegram-pt.jpg' %}

[Telegram] is a [messaging app] focused on speed and security. It's WhatsApp's closest competitor in terms of both functionality and popularity. Telegram allows you to send text, audio and video messages, as well as photos, videos and files of any type, in addition to make voice and video calls. In January, Telegram surpassed [500 million monthly active users][durov-1] and is one of the [10 most downloaded apps in the world][10-most-downloaded]. It is known for its groups able to hold up to 200,000 members. There are also [channels] for broadcasting messages to people who subscribed to receive them, similar to an email newsletter or RSS feed.

You can use Telegram on multiple devices — computers, smartphones and tablets — at the same time, your messages sync seamlessly among them. You can even use Telegram from a web browser without the need for a device to be connected to the Internet. But that is only possible because messages are stored on Telegram's servers. Telegram only uses end-to-end encryption by default for voice and video calls. For text messages, end-to-end encryption must be enabled manually.

To send encrypted messages to a contact, you need to start a [secret chat][secret-chats], which is only stored and available on the sender and receiver mobile devices. It is also possible to send [self-destructing messages][self-destructing-messages] in any chat (not just secret chats).

{% include update.html date="May 17, 2021" message="Telegram offers an app-based lock system, which allows you to add an extra layer of security to the app [using a passcode or fingerprint](https://www.howtogeek.com/709870/how-to-protect-telegram-messages-with-a-passcode/). Please note that it is not synced between your devices and needs to be set up on each device individually." %}

You need a phone number to start using Telegram. But the service offers an interesting possibility if you don't want to pass your number on to others, which is creating a [username]. It then becomes possible for other users to find you by that username, even if they don't know your phone number. Telegram does not show your number to anyone who message you by your username (unless you allow that in your privacy settings).

Telegram client apps are all [open source][telegram-clients-source] and [reproducible builds][telegram-reproducible-builds]. The server's source code, however, [is not open][telegram-faq-1]. You also can not [run your own (self-hosted) Telegram server][telegram-faq-2]. One of the Telegram creators, Pavel Durov, [explains][durov-2] that they don't publish the server code because that would not bring additional security benefits, and that would allow an authoritarian regime to launch its own Telegram fork.

The Telegram protocol has been [formally verified] to be cryptographically sound.

Telegram was launched in 2013 by the Russian brothers Nikolai and Pavel Durov. Pavel supports Telegram financially and ideologically while Nikolai's input is technological. In 2018, they had to leave Russia due to changes in the local IT regulations and since then have passed by a number of locations including Berlin, London and Singapore. Currently, the Telegram development team is based in Dubai. But they [say][telegram-faq-3] they are ready to relocate again if local regulations change.

They also [say][telegram-faq-4] that to this day they haven't disclosed any user data to third parties, including governments. What is probably true, given that Telegram generated controversy because of its status as [the preferred messaging app of ISIS][isis]. This has further driven the debate about the extent to which messaging apps should cooperate with the police or keep user data in complete secrecy. The Telegram team has blocked content (bots and public channels) from those terrorist groups whenever they discover them or receive [reports][telegram-isis-watch].

Telegram is completely free and has been [funded][telegram-faq-5] by its founder and CEO Pavel Durov with his own resources. He [said][durov-3] that there are plans to start monetizing Telegram in 2021 to pay for the infrastructure and developer salaries. All the features that are currently free will stay free. The app won't display ads in chats, nor the service will sell user data. Instead, optional premium features will be offered for users willing to pay for them.

Among the options in the [list of free and open source instant messengers][messaging app], Telegram may not be the best one for privacy. However, it's a messenger that simply works, it's practical and it's already better than WhatsApp in several regards. For many users, the convenience of being cloud-based (not having to worry about backups), plus the possibility of secret chats for conversations that need more protection, can be a good deal.

To install the Telegram app on [openSUSE], you can retrieve it from the official repositories:

```
# zypper in telegram-desktop
```

If you are an [openSUSE Leap][leap-tumbleweed] user, you may get an out-of-date version from that repo. If you use [FlatPak], another option is to install Telegram from [Flathub]:

```
# flatpak install org.telegram.desktop
```

You can download Telegram for Android from [its own website][telegram-apk] (as APK) or from [Google Play][telegram-google-play].

To use Telegram from a web browser, without installing any app, go to: [web.telegram.org](https://web.telegram.org/).

Telegram is also available for other systems. For more options, take a look at: [telegram.org](https://telegram.org/apps).

Telegram has a very explanatory [FAQ] (frequently asked questions) page in its website.

If you use Telegram, subscribe to the [@LinuxKamaradaNews](https://t.me/LinuxKamaradaNews) channel to be notified of new texts, or join the [@LinuxKamaradaWW](https://t.me/LinuxKamaradaWW) user group, where, in addition to receiving the same notifications, you can chat with other people who use [Linux Kamarada][kamarada-15.2].

This text is part of the series:

- [Open-source privacy-focused messaging apps alternatives to WhatsApp][messaging app]

[telegram]:                     https://telegram.org/
[messaging app]:                {% post_url en/2021-05-10-open-source-privacy-focused-messaging-apps-alternatives-to-whatsapp %}
[durov-1]:                      https://t.me/durov/147
[10-most-downloaded]:           https://sensortower.com/blog/top-apps-worldwide-january-2021-by-downloads
[channels]:                     https://telegram.org/blog/channels
[secret-chats]:                 https://www.howtogeek.com/709484/how-to-start-an-encrypted-secret-chat-in-telegram/
[self-destructing-messages]:    https://www.howtogeek.com/709484/how-to-start-an-encrypted-secret-chat-in-telegram/
[username]:                     https://telegram.org/faq#usernames-and-t-me
[telegram-clients-source]:      https://telegram.org/apps#source-code
[telegram-reproducible-builds]: https://core.telegram.org/reproducible-builds
[telegram-faq-1]:               https://telegram.org/faq#q-can-i-get-telegram-39s-server-side-code
[telegram-faq-2]:               https://telegram.org/faq#q-can-i-run-telegram-using-my-own-server
[durov-2]:                      https://t.me/durovschat/515221
[formally verified]:            https://arxiv.org/abs/2012.03141
[telegram-faq-3]:               https://telegram.org/faq#q-where-is-telegram-based
[telegram-faq-4]:               https://telegram.org/faq#q-do-you-process-data-requests
[isis]:                         http://www.businessinsider.com/theresa-may-telegram-home-to-criminals-and-terrorists-2018-1
[telegram-isis-watch]:          https://t.me/isiswatch/2
[telegram-faq-5]:               https://telegram.org/faq#q-how-are-you-going-to-make-money-out-of-this
[durov-3]:                      https://t.me/durov/142
[opensuse]:                     https://www.opensuse.org/
[leap-tumbleweed]:              {% post_url en/2020-12-07-opensuse-leap-vs-opensuse-tumbleweed-what-is-the-difference %}
[flatpak]:                      https://flatpak.org/
[flathub]:                      https://www.flathub.org/apps/details/org.telegram.desktop
[telegram-apk]:                 https://telegram.org/android
[telegram-google-play]:         https://play.google.com/store/apps/details?id=org.telegram.messenger
[faq]:                          https://telegram.org/faq
[kamarada-15.2]:                {% post_url en/2020-09-11-linux-kamarada-15.2-come-to-the-elegant-modern-green-side-of-the-force %}
