---
date: 2024-05-27 23:59:00 GMT-3
image: '/files/2024/05/kamarada-15.5.jpg'
layout: post
published: true
nickname: 'kamarada-15.5-final'
title: 'Linux Kamarada 15.5: more aligned with openSUSE Leap and other distributions'
---

{% include image.html src='/files/2024/05/kamarada-15.5.jpg' %}

I am proud to announce that **Linux Kamarada 15.5** is ready for everyone to use!&nbsp;

Linux Kamarada is a [Linux] distribution based on [openSUSE Leap] and is intended for use on desktops at home and at work, in both private companies and government entities. It features the essential software selection for any Linux installation and a nice looking modern desktop.

Linux Kamarada 15.5 is not created "[from scratch][lfs]", but based on a major distribution that is [openSUSE Leap 15.5]. The same version number (15.5) was adopted to emphasize the alignment between the distros. While openSUSE Leap is a general purpose Linux distro, offering a stable operating system for both personal computers and servers, as well as tools for developers and system administrators, Linux Kamarada is focused on personal computers and novice users.

Newcomers can find the new release on the [Download] page. If you are a Linux Kamarada user already, you can find directions on how to upgrade on this page.

Let's see what's changed on Linux Kamarada from the previous release ([15.4]) to the current release (15.5).

[Linux]:                https://www.kernel.org/linux.html
[openSUSE Leap]:        {% post_url en/2020-12-07-opensuse-leap-vs-opensuse-tumbleweed-what-is-the-difference %}
[lfs]:                  https://www.linuxfromscratch.org/
[openSUSE Leap 15.5]:   {% post_url en/2023-06-07-leap-15-5-release-matures-sets-up-technological-transition %}
[Download]:             /en/download
[15.4]:                 {% post_url en/2023-02-27-linux-kamarada-15-4-more-functional-beautiful-polished-and-elegant-than-ever %}

<div class="d-flex align-items-center my-3">
    <div class="flex-shrink-0">
        <img src="/files/2024/05/wayland.svg" style="max-width: 64px;">
    </div>
    <div class="flex-grow-1 ms-3">
        <h2 class="mb-0">Wayland as default display server</h2>
    </div>
</div>

[Wayland] is a display server protocol aimed to become the successor of the [X Window System] currently implemented by [X.Org]. Roughly speaking, Linux uses either Wayland or X.Org to render graphical user interfaces for the applications on the screen. If your system had neither Wayland nor X.Org, you would be able to use only the terminal and command line utilities.

The Wayland Display Server project was started in 2008 by a [Red Hat] developer who had previously made several contributions to X.Org. Currently, [most Linux distributions][wayland-adoption] already support Wayland out of the box, they even default to it instead of X.Org. Some notable examples include: [Fedora] starting with version 25 (released in November 2016), [Red Hat Enterprise Linux][rhel] 8 (May 2019), [Debian] 10 (July 2019) and [Ubuntu] 21.04. openSUSE Leap uses Wayland as the default session for [GNOME] since [version 15][leap-15] (May 2018).

In the case of Linux Kamarada, I chose to postpone the adoption of Wayland as default because I was concerned about several compatibility issues here and there with apps (such as [TeamViewer]) and graphics cards (such as [NVIDIA]). But now it seems that Wayland support is very mature and it is safe to default to it, aligning Linux Kamarada 15.5 with other Linux distributions.

This change is internal and, for most users, should go unnoticed, except perhaps for improved graphics quality and performance.

To find out whether you are using Wayland or X.Org, open the GNOME **Settings** app, go to the **About** section and take a look at **Windowing System**:

{% include image.html src='/files/2024/05/kamarada-15.5-wayland-1-en.jpg' %}

Note that you can revert to X.Org if you need to. To do that, on the _login_ screen, click the gear icon and choose the **GNOME on Xorg** session:

{% include image.html src='/files/2024/05/kamarada-15.5-wayland-2-en.jpg' %}

[Wayland]:              https://wayland.freedesktop.org/
[X Window System]:      https://en.wikipedia.org/wiki/X_Window_System
[X.Org]:                https://www.x.org/

[Red Hat]:              https://www.redhat.com/
[wayland-adoption]:     https://en.wikipedia.org/wiki/Wayland_(protocol)#Desktop_Linux_distributions
[Fedora]:               https://fedoraproject.org/
[rhel]:                 https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux
[Debian]:               https://www.debian.org/
[Ubuntu]:               https://ubuntu.com/
[GNOME]:                https://www.gnome.org/
[leap-15]:              https://linuxkamarada.com/en/2018/05/25/based-on-enterprise-code-tested-millions-of-times-opensuse-leap-15-released/

[TeamViewer]:           https://linuxkamarada.com/en/2019/07/20/20-apps-you-can-use-the-same-way-on-both-linux-and-windows-part-1/#6-teamviewer
[NVIDIA]:               https://www.nvidia.com/

<div class="d-flex align-items-center my-3">
    <div class="flex-shrink-0">
        <img src="/files/2021/12/media-optical.svg" style="max-width: 64px;">
    </div>
    <div class="flex-grow-1 ms-3">
        <h2 class="mb-0">Updated welcome screen</h2>
    </div>
</div>

The live image welcome screen is more polished and elegant, as a result of technical changes: the UI library it was based on was migrated from [GTK] 3 to GTK 4, and the underlying desktop it used was changed from [Openbox] to [GNOME Kiosk] (there were no changes to the features or how to use this screen, the changes were only internal, the user should not notice any difference except for the new look).

<div class="row">
    <div class="col-md">
        {% include image.html src='/files/2024/05/kamarada-15.5-firstboot-1-en.jpg' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2024/05/kamarada-15.5-firstboot-2-en.jpg' %}
    </div>
</div>

[GTK]:                  https://www.gtk.org/
[Openbox]:              http://openbox.org/
[GNOME Kiosk]:          https://gitlab.gnome.org/GNOME/gnome-kiosk

<div class="d-flex align-items-center my-3">
    <div class="flex-shrink-0">
        <img src="/files/2023/02/system-software-install.svg" style="max-width: 64px;">
    </div>
    <div class="flex-grow-1 ms-3">
        <h2 class="mb-0">Changes to the software selection</h2>
    </div>
</div>

Linux Kamarada 15.5 features most of the same apps as in 15.4, but updated with new features and bug fixes.

The **Advanced Network Configuration** app was added, it is present in several other Linux distros that are also based on the GNOME desktop.

[Python] 2 was removed from openSUSE Leap 15.5 and, therefore, from Linux Kamarada 15.5 as well, thus the **[Hearts]** game was also removed, as [it had not received updates since 2013][Hearts] and required Python 2.

The [Papirus icon theme for LibreOffice][papirus-libreoffice], which [had not received updates since 2020][papirus-libreoffice], was removed and replaced with the default [LibreOffice] icon theme (Elementary).

Translation packages were added, achieving an even more complete level of translation of the distro as a whole (this change may be noticed mainly by Brazilian users).

<div class="row">
    <div class="col-md">
        {% include image.html src='/files/2024/05/kamarada-15.5-nm-connection-editor-en.jpg' caption='Advanced Network Configuration' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2024/05/kamarada-15.5-libreoffice-elementary-en.jpg' caption='LibreOffice with its default icon theme (Elementary)' %}
    </div>
</div>

Other technical and internal changes, aimed at better _software_ quality, include:

- the **[openSUSE-repos-Leap]** package is now used to add the official openSUSE repositories, thus making it easier to set up these repos (more information can be found [here][openSUSE-repos-Leap]); and

- the Linux Kamarada [pattern] ([`patterns-kamarada-gnome.spec`][patterns-spec]) has been revised in accordance with the [openSUSE patterns], as well as each package mentioned in the Linux Kamarada live image description ([`appliance.kiwi`][appliance.kiwi]) has been revised.

[Python]:               https://www.python.org/
[Hearts]:               https://www.jejik.com/gnome-hearts
[papirus-libreoffice]:  https://github.com/PapirusDevelopmentTeam/papirus-libreoffice-theme
[LibreOffice]:          https://www.libreoffice.org/
[openSUSE-repos-Leap]:  {% post_url en/2023-11-13-install-the-new-opensuse-repos-leap-package %}
[pattern]:              https://linuxkamarada.com/pt/2021/04/01/o-que-sao-padroes-e-como-instala-los-no-opensuse/
[patterns-spec]:        https://gitlab.com/kamarada/patterns/-/blob/15.5/patterns-kamarada-gnome.spec
[openSUSE patterns]:    https://software.opensuse.org/search?baseproject=openSUSE%3ALeap%3A15.5&q=patterns
[appliance.kiwi]:       https://gitlab.com/kamarada/Linux-Kamarada-GNOME/-/blob/15.5/appliance.kiwi

<div class="d-flex align-items-center my-3">
    <div class="flex-shrink-0">
        <img src="/files/2021/12/multimedia-photo-viewer.svg" style="max-width: 64px;">
    </div>
    <div class="flex-grow-1 ms-3">
        <h2 class="mb-0">New backgrounds</h2>
    </div>
</div>

As you may be used to by now, this new release comes with new wallpapers, featuring photos of beautiful beaches in [Florianópolis]:

<div class="row">
    <div class="col-md">
        {% include image.html src='/files/2024/05/kamarada-15.5-background-1-en.jpg' caption='Ribeirão Capivari (default)' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2024/05/kamarada-15.5-background-2-en.jpg' caption='Mirante Para Baía Sul' %}
    </div>
</div>

In newer versions of GNOME, the lock screen uses the same background as the desktop. Nevertheless, to keep with the tradition of previous Linux Kamarada releases, 15.5 has got two new backgrounds.

If you prefer, you can also use wallpapers from previous Linux Kamarada releases or the openSUSE Leap default (or set your own images as backgrounds, of course).

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        {% include image.html src='/files/2024/05/kamarada-15.5-backgrounds-en.jpg' caption='Backgrounds included on Linux Kamarada 15.5' %}
    </div>
    <div class="col-md-2"></div>
</div>

[Florianópolis]:    https://en.wikipedia.org/wiki/Florian%C3%B3polis

## Where can I get Linux Kamarada?

The [Download] page has been updated with the download link for the 15.5 Final release.

**Warning:** it is not recommended to use Linux Kamarada on servers, although it is possible. openSUSE Leap is better suited for that use case, because it offers a server configuration during installation, which is going to set up a server with just a small set of packages and a text mode interface (without a desktop).

## What should I do next?

When the download is complete, verify the integrity of the ISO image by calculating its checksum. Compare it with the checksum that appears on the [Download] page. They must match. If they don't, download the ISO image again.

If you are not in a hurry, it is recommended to also verify the authenticity of the ISO image.

The fingerprint of the Linux Kamarada Project public key is:

```
6b18 52e7 764f b302 b805 a4a0 a575 bcce 1737 8ecc
```

To import that key, run the following commands:

```
$ wget -O kamarada.gpg https://build.opensuse.org/projects/home:kamarada:15.5/signing_keys/download?kind=gpg
$ gpg --import kamarada.gpg
```

For more information on those verifications, read:

- [Verifying data integrity and authenticity using SHA-256 and GPG][how-to-verify-iso]

Once the ISO image has been downloaded and those verifications have succeeded, you have 3 options:

<div class="d-flex">
    <div class="flex-shrink-0">
        <img src="/files/2020/02/disk-burner.svg" style="max-width: 48px;">
    </div>
    <div class="flex-grow-1 ms-3">
        <h6>1) Burn the ISO image to a DVD (thus generating a LiveDVD)</h6>

        <p>Use an application such as <a href="https://cdburnerxp.se/">CDBurnerXP</a> (on Windows), <a href="https://apps.kde.org/k3b/">K3b</a> (on a Linux system with KDE desktop) or <a href="https://wiki.gnome.org/Apps/Brasero">Brasero</a> (Linux with GNOME) to burn the ISO image to a DVD. Insert it on your computer DVD drive and reboot to start Linux Kamarada.</p>
    </div>
</div>

<div class="d-flex">
    <div class="flex-shrink-0">
        <img src="/files/2020/02/usb-creator.svg" style="max-width: 48px;">
    </div>
    <div class="flex-grow-1 ms-3">
        <h6>2) Write the ISO image to a USB flash drive (thus generating a LiveUSB)</h6>

        <p>Use <a href="https://www.ventoy.net/">Ventoy</a> (available for Windows and Linux) to prepare the flash drive and copy the ISO image to it. Plug it to a USB port and reboot your computer to start Linux Kamarada.</p>

        <p>This how-to can help you to prepare the LiveUSB:</p>

        <ul><li><a href="{% post_url en/2020-07-29-ventoy-create-a-multiboot-usb-drive-by-simply-copying-iso-images-to-it %}">Ventoy: create a multiboot USB drive by simply copying ISO images to it</a></li></ul>
    </div>
</div>

<div class="d-flex">
    <div class="flex-shrink-0">
        <img src="/files/2020/02/virtualbox.svg" style="max-width: 48px;">
    </div>
    <div class="flex-grow-1 ms-3">
        <h6>3) Create a virtual machine and use the ISO image to boot it</h6>

        <p>This option allows you to test Linux Kamarada without having to reboot your computer and leave the operating system you are familiar with.</p>

        <p>For more information, read:</p>

        <ul><li><a href="{% post_url en/2019-10-10-virtualbox-the-easiest-way-to-try-linux-without-installing-it %}">VirtualBox: the easiest way to try Linux without installing it</a></li></ul>
    </div>
</div>

<div class="d-flex mb-3">
    <div class="flex-grow-1">
        After testing Linux Kamarada, if you want to install it on your computer or virtual machine, start the installer by clicking this icon, located on the desktop.
    </div>
    <div class="flex-shrink-0 ms-3">
        <img src="/files/2021/12/calamares.svg" style="max-width: 48px;">
    </div>
</div>

The installer will do the partitioning, copy the system to the computer and set everything up (language, keyboard layout, time zone, username and password, etc.). At the end, reboot the computer to start using the installed system.

[how-to-verify-iso]:    {% post_url en/2018-11-08-verifying-data-integrity-and-authenticity-using-sha-256-and-gpg %}

## What if I already use Linux Kamarada?

The ISO image is not intended for upgrading, only for testing and installing.

If you already use Linux Kamarada 15.4, you can upgrade to 15.5 by following this tutorial:

- [Linux Kamarada and openSUSE Leap: how to upgrade from 15.4 to 15.5][upgrade-to-15.5]

If you use a development version of Linux Kamarada 15.5 ([Beta] or [RC]), just update your system as usual:

- [How to get updates for openSUSE Linux][howto-update]

[upgrade-to-15.5]:  {% post_url en/2023-11-27-linux-kamarada-15-4-and-opensuse-leap-15-4-how-to-upgrade-to-15-5 %}
[Beta]:             {% post_url en/2023-11-01-linux-kamarada-15-5-beta-help-test-the-best-release-of-the-distribution %}
[RC]:               {% post_url en/2024-05-08-linux-kamarada-15-5-enters-release-candidate-rc-phase %}
[howto-update]:     {% post_url en/2019-05-20-how-to-get-updates-for-opensuse-linux %}

## What if I already use openSUSE Leap?

You already use openSUSE Leap and want to turn it into Linux Kamarada? It's simple!

Just add the Linux Kamarada repository and install the **[patterns-kamarada-gnome]** package.

There are two different methods for doing that: from the graphical interface, using 1-Click Install, or from the terminal, using the **zypper** package manager — choose whichever method you prefer.

To install Linux Kamarada using 1-Click Install, click the following button:

<p class='text-center'>
    <a class='btn btn-primary' href='/downloads/patterns-kamarada-gnome-en.ymp'>
        <i class='fas fa-bolt'></i> 1-Click Install
    </a>
</p>

To install Linux Kamarada using the terminal, first add its repository:

```
# zypper addrepo -f -K -n "Linux Kamarada" -p 95 "https://packages.linuxkamarada.com/\$releasever/openSUSE_Leap_\$releasever/" kamarada
```

Then, install the **patterns-kamarada-gnome** package:

```
# zypper in patterns-kamarada-gnome
```

If you already use the GNOME desktop, probably you will need to download just a few packages. If you use another desktop, the download size will be larger.

When the installation is finished, if you create a new user, you will notice that they will receive the Linux Kamarada default settings (such as theme, wallpaper, etc.). Existing users can continue to use their customizations or adjust the system appearance (for instance, using the **Tweaks** and **Settings** apps).

[patterns-kamarada-gnome]:  https://software.opensuse.org/package/patterns-kamarada-gnome

## Where can I get help?

The [Help] page suggests some places where you can get help with Linux Kamarada and [openSUSE].

The support channel preferred by users has been the [@LinuxKamaradaWW](https://t.me/LinuxKamaradaWW) group on [Telegram], which is a messaging service that you can access from an app or a web browser.

[Help]:     /en/help
[openSUSE]: https://www.opensuse.org/
[Telegram]: {% post_url en/2021-05-10-the-telegram-messenger %}

## How long will it receive support?

According to the [openSUSE wiki][lifetime], openSUSE Leap 15.5 is expected to be maintained until the end of December 2024.

Since Linux Kamarada is based on openSUSE Leap, Linux Kamarada users are by transitivity openSUSE Leap users and receive the same updates.

Therefore, Linux Kamarada 15.5 will also be supported until the end of December 2024.

[lifetime]: https://en.opensuse.org/Lifetime

## Where can I get the source code?

Like any free software project, Linux Kamarada makes its source code available to anyone who wants to study it, adapt it, or contribute to the project.

Linux Kamarada development takes place at [GitLab] and [Open Build Service][obs]. There you can get the source codes of the packages developed specifically for this project (even [the source code of this website][kamarada-website] you read is available).

Source codes of packages inherited from openSUSE Leap can be retrieved directly from that distribution. If you need help doing this, get in touch, I can help.

[GitLab]:           https://gitlab.com/kamarada
[obs]:              https://build.opensuse.org/project/subprojects/home:kamarada
[kamarada-website]: https://gitlab.com/kamarada/kamarada.gitlab.io

## About Linux Kamarada

The Linux Kamarada Project aims to spread and promote Linux as a robust, secure, versatile and easy to use operating system, suitable for everyday use be at home, at work or on the server. It started as a blog about openSUSE, which is the Linux distribution I've been using for 12 years (since [April 2012][antoniomedeiros], when I installed openSUSE 11.4 and then upgraded to 12.1). Now the project offers its own Linux distribution, which brings much of the software presented on the blog pre-installed and ready for use.

[antoniomedeiros]:  https://antoniomedeiros.dev/blog/2012/04/21/problemas-envolvendo-bootloaders-mbr-e-tabela-de-particoes/

## Specs

To allow comparing Linux Kamarada releases, here is a summary of the software contained on Linux Kamarada 15.5 Final Build 5.78:

- Linux kernel 5.14.21
- X.Org display server 21.1.4 (with Wayland 1.21.0 enabled by default)
- GNOME desktop 41.9 including its core apps, such as Files (previously Nautilus), Calculator, Terminal, Text editor (gedit), Screenshot and others
- LibreOffice office suite 24.2.1.2
- Mozilla Firefox 115.11.0 ESR (default web browser)
- Chromium 125 web browser (alternative web browser)
- VLC media player 3.0.20
- Evolution email client 3.42.4
- YaST control center
- Brasero 3.12.3
- CUPS 2.2.7
- Drawing 1.0.1
- Firewalld 0.9.3
- GParted 1.4.0
- HPLIP 3.21.10
- Java (OpenJDK) 11.0.23
- KeePassXC 2.7.8
- Linphone 5.0.5
- PDFsam Basic 5.2.3
- Pidgin 2.14.8
- Python 3.6.15
- Samba 4.17.12
- Tor 0.4.8.10
- Transmission 3.00
- Vim 9.1.0330
- Wine 8.0
- Calamares installer 3.2.62
- Flatpak 1.14.5
- games: AisleRiot (Solitaire), Chess, Mahjongg, Mines (Minesweeper), Nibbles (Snake), Quadrapassel (Tetris), Reversi, Sudoku

That list is not exhaustive, but it gives a notion of what can be found in the distribution.
