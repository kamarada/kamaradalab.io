---
date: '2021-12-11 19:50:00 GMT-3'
image: '/files/2021/12/15.3-beta.png'
layout: post
nickname: 'kamarada-15.3-beta'
title: 'Linux Kamarada 15.3 Beta: help test the best release of the distribution'
---

{% include image.html src='/files/2021/12/15.3-beta.png' %}

The Linux Kamarada Project announces the 15.3 Beta release of the homonym [Linux distribution][linux], based on [openSUSE Leap 15.3][leap-15.3]. It is now available for [download].

<!--more-->

The [Download] page has been updated and now offer mainly two releases for download:

- 15.2 Final, which you can install on your home or work desktop; and
- 15.3 Beta, which you can test if you want to help in development.

openSUSE Leap 15.3 was released in [June][leap-15.3] and has since received various updates and fixes. Therefore, Linux Kamarada 15.3, although still in [beta] phase, should present considerable stability already. Still, bugs are expected and this version may not be ready for daily use. If you find a bug, please report it. Take a look at the [Help] page to see how to get in touch. Of course, bugs can be found and fixed anytime, but the sooner the better!

The easiest way to test Linux is using [VirtualBox]. For more information, please read:

- [VirtualBox: the easiest way to try Linux without installing it][virtualbox]

Or, if you want to try Linux Kamarada on your own machine, the easiest way to do this is using a USB stick that you can make bootable with [Ventoy]:

- [Ventoy: create a multiboot USB drive by simply copying ISO images to it][ventoy]

If you want to install Linux Kamarada on your computer for daily use, it is recommended that you install the 15.2 Final release, which is already ready, and then, when the 15.3 release is ready, upgrade. For more information on Linux Kamarada 15.2 Final, see:

- [Linux Kamarada 15.2: come to the elegant modern green side of the force!][kamarada-15.2]

## What's new?

What's new in the 15.3 release is:

- same (but updated) applications as in 15.2, with new features and bug fixes;

- new beautiful wallpapers (but you can keep using the previous ones or the openSUSE defaults, if you prefer);

- native support for [Flatpak], a distribution-agnostic package manager (more on this later);

- more [X.Org] drivers were included in the Live image, which previously had only the most basic drivers, that was a problem mainly for owners of [NVIDIA] graphics cards, who can now benefit from the open source drivers **[nouveau]** and **nv** installed out-of-the-box;

- now Linux Kamarada has just one Live image, which can be used either in Brazilian Portuguese and International English (in previous releases, 15.1 and 15.2, two Live images were available, one for each language);

- when booting the Live image, a welcome screen allows choosing the language -- Brazilian Portuguese or International English -- as well as to try or to install Linux Kamarada, this screen was inspired by the [Ubuntu] Live image welcome screen;

- [YaST Firstboot], which was previously used in the international Live image and newly installed systems (from either images) to adjust the initial settings (language, keyboard layout, time zone, etc.), is no longer used because on many computers it didn't show up, and users saw a black screen instead;

- the [Calamares] installer now adjusts all the initial settings on the installed system, so that, after the installation ends, when the computer reboots, the system is completely ready for use;

- the installer icon was moved from the GNOME dock to the Desktop (thanks to [SlackJeff] for the suggestion, he made a [Linux Kamarada 15.2 review][SlackJeff] -- in Brazilian Portuguese);

- the [TopIcons Plus] GNOME extension, which has not been maintained, was replaced by Ubuntu's [AppIndicator Support] extension; and

- a L2TP VPN client was added.

Regarding the [Flatpak support][flatpak-opensuse], the [**flatpak**][flatpak-softwareoo] package comes installed out-of-the-box on Linux Kamarada 15.3. Therefore, Linux Kamarada 15.3 users do not need to perform the first step of the [Flatpak installation instructions for openSUSE][flatpak-setup]. However, the default [Flathub] repository does not come pre-configured, so that one needs to add it before installing software:

```
$ flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
```

I would like Linux Kamarada to be like [Linux Mint], which already brings the Flathub repo setup, so that the user, without having to set up anything else, is already able to install software from Flathub using the very software store app. I'm going to study what I need to do to achieve this still in this 15.3 release or in the next one, 15.4. Comments and ideas are welcome.

## Upgrading to 15.3 Beta

If you are already using Linux Kamarada 15.2 and trust your personal experience as an user, you may want to upgrade to Linux Kamarada 15.3 Beta to help test the upgrade process. Generally speaking, you should just need to follow this upgrade how-to, replacing `15.2` with `15.3`, where applicable:

- [Linux Kamarada and openSUSE Leap: how to upgrade from 15.1 to 15.2][upgrade-howto]

Since Linux Kamarada 15.2 repos are already setup with the `$releasever` variable, it should be pretty easy to upgrade to 15.3. Examples of commands from the tutorial above, already adapted for the new release:

```
# zypper --releasever=15.3 ref
# zypper --releasever=15.3 dup --download-only
```

There is a detail regarding openSUSE Leap 15.3, which shares RPM packages with [SUSE Linux Enterprise 15 SP3][sle-15-sp3] (not just the source code, as in previous releases, but even the binary, compiled packages): during upgrading, the **zypper** package manager is going to report many [vendor changes]. To accept them, use the `--allow-vendor-change` option. The last command would therefore look like this:

```
# zypper --releasever=15.3 dup --download-only --allow-vendor-change
```

For more information, take a look at the [openSUSE Leap 15.3 release notes][leap-15.3-release-notes].

If you don't feel safe upgrading to 15.3 Beta with these tips, don't worry. In the next few days, I'm going to publish an updated version of that how-to.

{% capture update %}

Here it is:

- [Linux Kamarada and openSUSE Leap: how to upgrade from 15.2 to 15.3]({% post_url en/2021-12-23-linux-kamarada-and-opensuse-leap-how-to-upgrade-from-152-to-153 %})

{% endcapture %}

{% include update.html date="Dec 23, 2021" message=update %}

## Specs

To allow comparison among Linux Kamarada releases, here is a summary of the software contained in Linux Kamarada 15.3 Beta Build 3.1:

- Linux kernel 5.3.18
- X.Org display server 1.20.3 (without Wayland)
- GNOME 3.34.7 desktop including its core apps, such as Files (previously Nautilus), Calculator, Terminal, Text editor (gedit), Screenshot and others
- LibreOffice office suite 7.1.4.2
- Mozilla Firefox 91.4.0 ESR (default web browser)
- Chromium 95 web browser (alternative web browser)
- VLC media player 3.0.16
- Evolution email client
- YaST control center
- Brasero
- CUPS 2.2.7
- Firewalld 0.9.3
- GParted 0.31.0
- HPLIP 3.20.11
- Java (OpenJDK) 11.0.13
- KeePassXC 2.6.6
- KolourPaint 20.04.2
- Linphone 4.1.1
- PDFsam Basic 4.2.7
- Pidgin 2.13.0
- Python 2.7.18 and 3.6.13
- Samba 4.13.13
- Tor 0.4.6
- Transmission 2.94
- Vim 8.0
- Wine 6.0
- Calamares installer 3.2.36
- Flatpak 1.10.5
- games: Aisleriot (Solitaire), Chess, Hearts, Iagno (Reversi), Mahjongg, Mines (Minesweeper), Nibbles (Snake), Quadrapassel (Tetris), Sudoku

That list is not exhaustive, but it gives a notion of what can be found in the distribution.

[linux]:                    https://www.kernel.org/linux.html
[leap-15.3]:                {% post_url en/2021-06-02-opensuse-leap-153-bridges-path-to-enterprise %}
[download]:                 /en/download
[beta]:                     https://en.wikipedia.org/wiki/Software_release_life_cycle#Beta
[Help]:                     /en/help
[virtualbox]:               {% post_url en/2019-10-10-virtualbox-the-easiest-way-to-try-linux-without-installing-it %}
[ventoy]:                   {% post_url en/2020-07-29-ventoy-create-a-multiboot-usb-drive-by-simply-copying-iso-images-to-it %}
[kamarada-15.2]:            {% post_url en/2020-09-11-linux-kamarada-15.2-come-to-the-elegant-modern-green-side-of-the-force %}
[Flatpak]:                  https://flatpak.org/
[X.Org]:                    https://www.x.org/wiki/
[NVIDIA]:                   https://www.nvidia.com/pt-br/
[nouveau]:                  https://nouveau.freedesktop.org/
[Ubuntu]:                   https://ubuntu.com/
[YaST Firstboot]:           https://en.opensuse.org/YaST_Firstboot
[Calamares]:                https://calamares.io/
[SlackJeff]:                https://www.youtube.com/watch?v=VlSCytf7Tns
[TopIcons Plus]:            https://github.com/phocean/TopIcons-plus
[GNOME]:                    https://www.gnome.org/
[AppIndicator Support]:     https://github.com/ubuntu/gnome-shell-extension-appindicator
[flatpak-opensuse]:         https://en.opensuse.org/Flatpak
[flatpak-softwareoo]:       https://software.opensuse.org/package/flatpak
[flatpak-setup]:            https://flatpak.org/setup/openSUSE/
[Flathub]:                  https://flathub.org/
[Linux Mint]:               https://linuxmint.com/
[upgrade-howto]:            {% post_url en/2020-09-01-linux-kamarada-and-opensuse-leap-how-to-upgrade-from-151-to-152 %}
[sle-15-sp3]:               https://www.suse.com/c/introducing-suse-linux-enterprise-15-sp3/
[vendor changes]:           https://en.opensuse.org/SDB:Vendor_change_update
[leap-15.3-release-notes]:  https://doc.opensuse.org/release-notes/x86_64/openSUSE/Leap/15.3/RELEASE-NOTES.html#sec.upgrade.152
