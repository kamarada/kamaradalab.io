---
date: 2021-12-30 18:00:00 GMT-3
image: '/files/2021/12/kamarada-15.3.jpg'
layout: post
published: true
nickname: 'kamarada-15.3-final'
title: 'Linux Kamarada 15.3: new year, new release'
---

{% include image.html src='/files/2021/12/kamarada-15.3.jpg' %}

I am proud to announce that **Linux Kamarada 15.3** is ready for everyone to use! Linux Kamarada 15.3 is a [Linux] distribution based on a bigger distribution that is [openSUSE Leap 15.3]. While [openSUSE Leap] is a general purpose Linux distro, offering a stable operating system for both personal computers and servers, as well as tools for developers and system administrators, Linux Kamarada is focused on personal computers and novice users.

Newcomers can find the new release on the [Download] page. If you are a Linux Kamarada user already, you can find directions on how to upgrade on this page.

Let's see what's changed on Linux Kamarada from the previous release ([15.2]) to the new release (15.2).

<div class="media mb-3">
    <img src="/files/2021/12/media-optical.svg" class="mr-3" style="max-width: 60px;">
    <div class="media-body align-self-center">
        <h2 class="mt-0 mb-0">Live media changes</h2>
    </div>
</div>

On the [Download] page you can already notice something new about Linux Kamarada 15.3: now Linux Kamarada only offers just one [Live image][live], which can be used either in Brazilian Portuguese or International English. In previous releases, [15.1] and 15.2, two Live images were available, one for each language.

When booting the Live image, a new welcome screen allows you to choose the desired language, as well as to try or install Linux Kamarada:

<div class="row">
    <div class="col-md">
        {% include image.html src='/files/2021/12/kamarada-15.3-welcome-01-en.jpg' caption='Portuguese or English?' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2021/12/kamarada-15.3-welcome-02-en.jpg' caption='Try or install?' %}
    </div>
</div>

Even if you choose to try Linux Kamarada, you can install it at any time by lauching the installer from the icon on Desktop. By the way, this is another difference from 15.3 to previous Linux Kamarada releases, in which the installer icon was on the [GNOME] dock.

{% include image.html src='/files/2021/12/kamarada-15.3-installer-icon-en.jpg' caption='Installer icon on Desktop' %}

And now the [Calamares] installer is solely responsible for setting everything up during installation:

<div class="row">
    <div class="col-md">
        {% include image.html src='/files/2021/12/kamarada-15.3-calamares-01-en.jpg' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2021/12/kamarada-15.3-calamares-02-en.jpg' %}
    </div>
</div>

So, after the installation ends, when the computer reboots, the system is ready for use.

<div class="media mb-3">
    <img src="/files/2021/12/multimedia-photo-viewer.svg" class="mr-3" style="max-width: 60px;">
    <div class="media-body align-self-center">
        <h2 class="mt-0 mb-0">New backgrounds</h2>
    </div>
</div>

As you may be used to by now, this new release comes with new wallpapers, featuring photos of beautiful beaches in [Florianópolis]:

<div class="row">
    <div class="col-md">
        {% include image.html src='/files/2021/12/kamarada-15.3-lock-en.jpg' caption='Lock screen' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2021/12/kamarada-15.3-desktop-en.jpg' caption='Desktop' %}
    </div>
</div>

If you prefer, you can also use wallpapers from previous Linux Kamarada releases or the openSUSE Leap default (or set your own images as backgrounds, of course).

<div class="media mb-3">
    <img src="/files/2021/12/application-vnd.flatpak.svg" class="mr-3" style="max-width: 60px;">
    <div class="media-body align-self-center">
        <h2 class="mt-0 mb-0">Native Flatpak support</h2>
    </div>
</div>

**[Flatpak]** is a distribution-agnostic package manager. Until recently, different Linux distros used different package formats, often incompatible with each other. For example, [Debian] and [Ubuntu] use [DEB] packages, while [RedHat], [Fedora] and [openSUSE] (and so Linux Kamarada) use [RPM] packages. Flatpak brought a simpler alternative to install programs on different distros: as long as Flatpak is installed, the same Flatpak package can be installed on any distro.

Linux Kamarada 15.3 comes with Flatpak already installed out-of-the-box. [Flathub], the main Flatpak repository, is also pre-configured. You can simply install applications using Flatpak, without having to [set it up][flatpak-setup] first. Even the GNOME software "store" (the **Software** app) supports Flatpak:

<div class="row">
    <div class="col-md">
        {% include image.html src='/files/2021/12/kamarada-15.3-flatpak-01-en.jpg' %}
    </div>
    <div class="col-md">
        {% include image.html src='/files/2021/12/kamarada-15.3-flatpak-02-en.jpg' %}
    </div>
</div>

If you upgraded from a previous Linux Kamarada release, you may need to add the Flathub repository. To do that, before using Flatpak for the first time, run the following command:

```
# flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
```

<div class="media mb-3">
    <img src="/files/2021/12/audio-card.svg" class="mr-3" style="max-width: 60px;">
    <div class="media-body align-self-center">
        <h2 class="mt-0 mb-0">Better hardware support</h2>
    </div>
</div>

Previous Linux Kamarada releases had only the most basic [X.Org] drivers included out-of-the-box. That was a problem mainly for owners of [NVIDIA] graphics cards. The Linux Kamarada 15.3 Live image comes with more drivers, including the open source **[nouveau]** and **nv**.

Some useful utilities for diagnosing hardware and filesystem problems were included as well: **[inxi]**, **[lshw]**, **[lsof]**, **[lspci]** and **[smartctl]**

## Where can I get Linux Kamarada?

The [Download] page has been updated with the download link for the 15.3 Final release.

**Warning:** it is not recommended to use Linux Kamarada on servers, although it is possible. openSUSE Leap is better suited for that use case, because it offers a server configuration during installation, which is going to set up a server with just a small set of packages and a text mode interface (without a desktop).

## What should I do next?

When the download is complete, verify the integrity of the ISO image by calculating its checksum. Compare it with the checksum that appears on the [Download] page. They must match. If they don't, download the ISO image again.

If you are not in a hurry, it is recommended to also verify the authenticity of the ISO image.

The fingerprint of the Linux Kamarada Project public key is:

```
6b18 52e7 764f b302 b805 a4a0 a575 bcce 1737 8ecc
```

To import that key, run the following commands:

```
$ wget https://build.opensuse.org/projects/home:kamarada:15.3/public_key
$ gpg --import public_key
```

For more information on those verifications, read:

- [Verifying data integrity and authenticity using SHA-256 and GPG][how-to-verify-iso]

Once the ISO image has been downloaded and those verifications have succeeded, you have 3 options:

<ul class="list-unstyled">
    <li class="media mb-3">
        <img src="/files/2020/02/disk-burner.svg" class="mr-3" style="max-width: 48px;">
        <div class="media-body">
            <h6 class="mt-0">1) Burn the ISO image to a DVD (thus generating a LiveDVD)</h6>

            Use an application such as <a href="https://cdburnerxp.se/">CDBurnerXP</a> (on Windows), <a href="http://www.k3b.org/">K3b</a> (on a Linux system with KDE desktop) or <a href="https://wiki.gnome.org/Apps/Brasero">Brasero</a> (Linux with GNOME) to burn the ISO image to a DVD. Insert it on your computer DVD drive and reboot to start Linux Kamarada.
        </div>
    </li>

    <li class="media mb-3">
        <img src="/files/2020/02/usb-creator.svg" class="mr-3" style="max-width: 48px;">
        <div class="media-body">
            <h6 class="mt-0">2) Write the ISO image to a USB flash drive (thus generating a LiveUSB)</h6>

            <p>Use <a href="https://www.ventoy.net/">Ventoy</a> (available for Windows and Linux) to prepare the flash drive and copy the ISO image to it. Plug it to a USB port and reboot your computer to start Linux Kamarada.</p>

            <p>This how-to can help you to prepare the LiveUSB:</p>

            <ul><li><a href="{% post_url en/2020-07-29-ventoy-create-a-multiboot-usb-drive-by-simply-copying-iso-images-to-it %}">Ventoy: create a multiboot USB drive by simply copying ISO images to it</a></li></ul>
        </div>
    </li>

    <li class="media mb-3">
        <img src="/files/2020/02/virtualbox.svg" class="mr-3" style="max-width: 48px;">
        <div class="media-body">
            <h6 class="mt-0">3) Create a virtual machine and use the ISO image to boot it</h6>

            <p>This option allows you to test Linux Kamarada without having to reboot your computer and leave the operating system you are familiar with.</p>

            <p>For more information, read:</p>

            <ul><li><a href="{% post_url en/2019-10-10-virtualbox-the-easiest-way-to-try-linux-without-installing-it %}">VirtualBox: the easiest way to try Linux without installing it</a></li></ul>
        </div>
    </li>

    <li class="media mb-3">
        <div class="media-body">
            After testing Linux Kamarada, if you want to install it on your computer or virtual machine, start the installer by clicking this icon, located on the desktop.
        </div>
        <img src="/files/2021/12/calamares.svg" class="ml-3" style="max-width: 48px;">
    </li>
</ul>

The installer will do the partitioning, copy the system to the computer and set everything up (language, keyboard layout, time zone, username and password, etc.). At the end, reboot the computer to start using the installed system.

## What if I already use Linux Kamarada?

The ISO image is not intended for upgrading, only for testing and installing.

If you already use Linux Kamarada 15.2, you can upgrade to 15.3 by following this tutorial:

- [Linux Kamarada and openSUSE Leap: how to upgrade from 15.2 to 15.3][upgrade-to-15.3]

If you use a development version of Linux Kamarada 15.3 ([Beta] or [RC]), just update your system as usual:

- [How to get updates for openSUSE Linux][howto-update]

## What if I already use openSUSE Leap?

You already use openSUSE Leap and want to turn it into Linux Kamarada? It's simple!

Just add the Linux Kamarada repository and install the **[patterns-kamarada-gnome]** package.

There are two different methods for doing that: from the graphical interface, using 1-Click Install, or from the terminal, using the **zypper** package manager — choose whichever method you prefer.

To install Linux Kamarada using 1-Click Install, click the following button:

<p class='text-center'><a class='btn btn-sm btn-outline-primary' href='/downloads/patterns-kamarada-gnome-en.ymp'><i class='fas fa-bolt'></i> 1-Click Install</a></p>

To install Linux Kamarada using the terminal, first add its repository:

```
# zypper addrepo -f -K -n "Linux Kamarada" -p 95 "http://c3sl.dl.osdn.jp/storage/g/k/ka/kamarada/\$releasever/openSUSE_Leap_\$releasever/" kamarada
```

The Linux Kamarada repository is gently hosted by [OSDN], which replicates the repository contents on some mirror servers around the world. I picked the Brazilian mirror hosted by [C3SL] because it is the one closest to me. You may want to take a look at the list of [Linux Kamarada mirrors][mirrors] and choose a mirror closer to you.

Then, install the **patterns-kamarada-gnome** package:

```
# zypper in patterns-kamarada-gnome
```

If you already use the GNOME desktop, probably you will need to download just a few packages. If you use another desktop, the download size will be larger.

When the installation is finished, if you create a new user, you will notice that they will receive the Linux Kamarada default settings (such as theme, wallpaper, etc.). Existing users can continue to use their customizations or adjust the system appearance (for instance, using the **Tweaks** and **Settings** apps).

## Where can I get help?

The [Help] page suggests some places where you can get help with Linux Kamarada and openSUSE.

The support channel preferred by users has been the [@LinuxKamaradaWW](https://t.me/LinuxKamaradaWW) group on [Telegram], which is a messaging service that you can access from an app or a web browser.

## How long will it receive support?

According to the [openSUSE wiki][lifetime], openSUSE Leap 15.3 is expected to be maintained until the end of November 2022.

Since Linux Kamarada is based on openSUSE Leap, Linux Kamarada users are by transitivity openSUSE Leap users and receive the same updates.

Therefore, Linux Kamarada 15.3 will also be supported until the end of November 2022.

## Where can I get the source code?

Like any free software project, Linux Kamarada makes its source code available to anyone who wants to study it, adapt it, or contribute to the project.

Linux Kamarada development takes place at [GitLab] and [Open Build Service][obs]. There you can get the source codes of the packages developed specifically for this project (even [the source code of this web site][kamarada-website] you read is available).

Source codes of packages inherited from openSUSE Leap can be retrieved directly from that distribution. If you need help doing this, get in touch, I can help.

## About Linux Kamarada

The Linux Kamarada Project aims to spread and promote Linux as a robust, secure, versatile and easy to use operating system, suitable for everyday use be at home, at work or on the server. It started as a blog about openSUSE, which is the Linux distribution I've been using for 9 years (since [April 2012][vinyanalista], when I installed openSUSE 11.4 and then upgraded to 12.1). Now the project offers its own Linux distribution, which brings much of the software presented on the blog pre-installed and ready for use.

The Kamarada Linux distribution is based on openSUSE Leap and is intended for use on desktops at home and at work, in both private companies and government entities. It features the essential software selection for any Linux installation and a nice looking modern desktop.

## Specs

To allow comparing Linux Kamarada releases, here is a summary of the software contained on Linux Kamarada 15.3 Final Build 4.3:

- Linux kernel 5.3.18
- X.Org display server 1.20.3 (without Wayland)
- GNOME 3.34.7 desktop including its core apps, such as Files (previously Nautilus), Calculator, Terminal, Text editor (gedit), Screenshot and others
- LibreOffice office suite 7.1.4.2
- Mozilla Firefox 91.4.0 ESR (default web browser)
- Chromium 96 web browser (alternative web browser)
- VLC media player 3.0.16
- Evolution email client
- YaST control center
- Brasero
- CUPS 2.2.7
- Firewalld 0.9.3
- GParted 0.31.0
- HPLIP 3.20.11
- Java (OpenJDK) 11.0.13
- KeePassXC 2.6.6
- KolourPaint 20.04.2
- Linphone 4.1.1
- PDFsam Basic 4.2.10
- Pidgin 2.13.0
- Python 2.7.18 and 3.6.15
- Samba 4.13.13
- Tor 0.4.6
- Transmission 2.94
- Vim 8.0
- Wine 6.0
- Calamares installer 3.2.36
- Flatpak 1.10.5
- games: Aisleriot (Solitaire), Chess, Hearts, Iagno (Reversi), Mahjongg, Mines (Minesweeper), Nibbles (Snake), Quadrapassel (Tetris), Sudoku

That list is not exhaustive, but it gives a notion of what can be found in the distribution.

[Linux]:                    https://www.kernel.org/linux.html
[openSUSE Leap 15.3]:       {% post_url en/2021-06-02-opensuse-leap-153-bridges-path-to-enterprise %}
[openSUSE Leap]:            {% post_url en/2020-12-07-opensuse-leap-vs-opensuse-tumbleweed-what-is-the-difference %}
[Download]:                 /en/download
[15.2]:                     {% post_url en/2020-09-11-linux-kamarada-15.2-come-to-the-elegant-modern-green-side-of-the-force %}
[live]:                     {% post_url en/2015-11-25-what-is-a-livecd-dvd-usb %}
[15.1]:                     {% post_url en/2020-02-27-kamarada-15.1-comes-with-everything-you-need-to-use-Linux-everyday %}
[GNOME]:                    https://www.gnome.org/
[Calamares]:                https://calamares.io/
[Florianópolis]:            https://en.wikipedia.org/wiki/Florianópolis
[flatpak]:                  https://flatpak.org/
[debian]:                   https://www.debian.org/
[ubuntu]:                   https://ubuntu.com/
[deb]:                      https://en.wikipedia.org/wiki/Deb_(file_format)
[redhat]:                   https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux
[fedora]:                   https://getfedora.org/
[opensuse]:                 https://www.opensuse.org/
[rpm]:                      https://en.wikipedia.org/wiki/RPM_Package_Manager
[Flathub]:                  https://flathub.org/
[flatpak-setup]:            https://flatpak.org/setup/openSUSE/
[X.Org]:                    https://www.x.org/wiki/
[NVIDIA]:                   https://www.nvidia.com/pt-br/
[nouveau]:                  https://nouveau.freedesktop.org/
[inxi]:                     https://smxi.org/docs/inxi.htm
[lshw]:                     https://www.ezix.org/project/wiki/HardwareLiSter
[lsof]:                     https://man7.org/linux/man-pages/man8/lsof.8.html
[lspci]:                    https://man7.org/linux/man-pages/man8/lspci.8.html
[smartctl]:                 https://www.smartmontools.org/
[how-to-verify-iso]:        {% post_url en/2018-11-08-verifying-data-integrity-and-authenticity-using-sha-256-and-gpg %}
[upgrade-to-15.3]:          {% post_url en/2021-12-23-linux-kamarada-and-opensuse-leap-how-to-upgrade-from-152-to-153 %}
[Beta]:                     {% post_url en/2021-12-11-linux-kamarada-15-3-beta-help-test-the-best-release-of-the-distribution %}
[RC]:                       {% post_url en/2021-12-28-linux-kamarada-153-enters-release-candidate-rc-phase %}
[howto-update]:             {% post_url en/2019-05-20-how-to-get-updates-for-opensuse-linux %}
[patterns-kamarada-gnome]:  https://software.opensuse.org/package/patterns-kamarada-gnome
[osdn]:                     https://osdn.net/projects/kamarada/
[c3sl]:                     https://www.c3sl.ufpr.br/
[mirrors]:                  https://gitlab.com/kamarada/Linux-Kamarada-GNOME/-/wikis/Mirrors
[help]:                     /en/help
[telegram]:                 {% post_url en/2021-05-10-the-telegram-messenger %}
[lifetime]:                 https://en.opensuse.org/Lifetime
[GitLab]:                   https://gitlab.com/kamarada
[obs]:                      https://build.opensuse.org/project/subprojects/home:kamarada
[kamarada-website]:         https://gitlab.com/kamarada/kamarada.gitlab.io
[vinyanalista]:             https://vinyanalista.github.io/blog/2012/04/21/problemas-envolvendo-bootloaders-mbr-e-tabela-de-particoes/
