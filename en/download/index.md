---
layout: download
nickname: download
title: Download

stable_title: 'Linux Kamarada 15.5'
stable_release: '15.5'
stable_desc: 'Stable version, ready to use on personal computers, be at home or at work, in private companies or government entities.'

dev_title: 'Linux Kamarada 15.6 Beta'
dev_release: '15.6'
dev_desc: 'Next version, under development, not recommended for daily use. As this version is in beta phase, bugs are expected. Download it if you want to test it and help the Linux Kamarada development. If you find a bug, please report it. Take a look at the [Help](/en/help) page to see how to get in touch.'

archive_title: 'Archive (old releases)'
archive_desc: 'Listed below are older Linux Kamarada releases, which are available for download in case you need them for any reason. Please note that these releases are no longer supported or updated.'
---
