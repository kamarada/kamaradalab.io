---
layout: download-release
nickname: download-15.1
permalink: /en/download/15.1/
title: Linux Kamarada 15.1 (old release)

intro: 'This is <strong>not</strong> the latest Linux Kamarada release, but it is available for download in case you need it for any reason. Newer versions can be found <a href="/en/download">here</a>.'

news_link: 'You can find more information about this release reading the <a href="/en/2020/02/27/kamarada-15.1-comes-with-everything-you-need-to-use-Linux-everyday/">release news</a>. Please note that this release has reached <a href="/en/2021/01/31/opensuse-leap-151-and-linux-kamarada-151-reach-end-of-life-today/">end of life</a> and <strong>is no longer supported or updated</strong>.'

free_sw: 'Linux Kamarada is completely <a href="https://www.gnu.org/philosophy/free-sw.en.html"><strong>free</strong></a> to download, use and share.'

based_on_opensuse: 'Powered by openSUSE'
based_on_opensuse_desc: 'It is a Linux distribution based on the great <a href="/en/2019/05/22/opensuse-community-releases-leap-15-1-version/">openSUSE Leap</a> — the hybrid enterprise-community version of <a href="https://www.opensuse.org/">openSUSE</a> — and contains customizations.'

language_label: 'Language'
language_icon: '/assets/icons/other/usa-24.png'
language_value: 'International English'
other_language: 'Se você fala Português Brasileiro, <a href="/pt/download/15.1">clique aqui</a>.'

filename_label: 'Filename'
filename_value: '{{ site.data.download151[page.lang].filename_value }}'
download_link: '{{ site.data.download151[page.lang].download_link }}'
download: 'Download'

date_label: 'Date'
date_value: '{{ site.data.download151[page.lang].date_value }}'

size_label: 'Size'
size_value: '{{ site.data.download151[page.lang].size_value }}'

sha256sum_label: 'SHA256 sum'
sha256sum_value: '{{ site.data.download151[page.lang].sha256sum_value }}'
sha256sum_link: '{{ site.data.download151[page.lang].sha256sum_link }}'

system_requirements_title: 'Recommended system requirements'
system_requirements_list: '<li>2 Ghz dual core processor or better</li>
<li>2 GB system memory</li>
<li>Over 40GB of free hard drive space</li>
<li>Either a DVD drive or a USB port for booting the Live media</li>'

hosted-by: 'The ISO images provided by the Linux Kamarada Project are gently hosted by'
---
